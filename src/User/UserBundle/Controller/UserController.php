<?php



namespace User\UserBundle\Controller;



use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use User\UserBundle\Entity\User;

use User\UserBundle\Form\UserType;

use Btob\HotelBundle\Entity\Hotelmarge;

use Btob\HotelBundle\Entity\HotelmargeRepository;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;



/**

 * User controller.

 *

 */

class UserController extends Controller

{
    /**
     * Show the user
     * /
    public function connexionAction() {  //->getUser()->getId()
      //  $isAuthenticated = $tokenInterface->isAuthenticated();
        if($this->get('request')->getMethod() == 'POST'){
      $x=$this->container->get('security.authorization_checker');
        $tokenInterface = $this->get('security.token_storage')->getToken();
        if(!is_object($tokenInterface->getUser())){
            return $this->redirectToRoute('fos_user_security_check');
            
        }else{exit(var_dump($this->get('request')->get('username')));
        
        $em = $this->getDoctrine()->getManager();
        $qb=$em->createQueryBuilder();
        $nb=$qb->select('h.nb_fois_errone')
            ->from('UserUserBundle:User','h')
            ->where('h.username = :user_name')
            ->setParameter('user_name', $_POST['username'])
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        $user = $em ->getRepository(User::class)
                        ->createQueryBuilder('u')
                        ->update()
                        ->set('u.nb_fois_errone',':nb')
                        ->where('u.id = :idusr')
                        ->setParameter('idusr', $id_user)
                        ->setParameter('nb', $nb+1)
                        ->getQuery()
                        ->execute();
                        if(($nb+1)==3){
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Authentification");
                        $hist->setMessage("Erreur de connexion 1 fois");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
                 return $this->render('UserUserBundle:Exception:error.html.twig',array('error'=>'Veuillez contacter l\'administrateur'));
                }
            }
        }
    return $this->render('UserUserBundle::layout.html.twig');

    }*/

    /**
     * Show the user
     */
    public function codeverificationAction() {
        
       $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $codeverif = $this->container->get('security.token_storage')->getToken()->getUser()->getCodeverification();
            if($codeverif == $request->get('codeverification'))
                return $this->redirectToRoute('btob_dash_homepage');
            else {
                return $this->redirectToRoute('codeverification');
            }
        }else{
            $dateexp = date_format($this->container->get('security.token_storage')->getToken()->getUser()->getDateexpiration(), 'Y-m-d');
            $datenow = date("Y-m-d");
           // $date = date_format($date, 'Y-m-d');
    if ($dateexp >= $datenow) {
        
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = @$_SERVER['REMOTE_ADDR'];
    $result  = array('country'=>'', 'city'=>'');
    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }else{
        $ip = $remote;
    }
    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));    
    if($ip_data && $ip_data->geoplugin_countryName != null){
        $result['country'] = $ip_data->geoplugin_countryCode;
        $result['city'] = $ip_data->geoplugin_countryName;
    } 
    if (($result['country'] == "TN") and ($result['city']=="Tunisia")) {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $mailto=$user->getEmail();
        $em = $this->getDoctrine()->getManager();
            $id_user = $em->getRepository("UserUserBundle:User")->find($user->getId());
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $code=substr(str_shuffle($permitted_chars), 0, 10);// Output: 54esmdr0qf
            $user = $em ->getRepository(User::class)
                        ->createQueryBuilder('u')
                        ->update()
                        ->set('u.codeverification',':code')
                        ->where('u.id = :idusr')
                        ->setParameter('idusr', $id_user)
                        ->setParameter('code', $code)
                        ->getQuery()
                        ->execute();
                        $to      = $mailto;
                        $subject = 'Code de verification';
                        $message = "voici votre code de verification ".$code."";

                        $mailfrom="contact@tuninfoforyou.com";
                        $headers  = "MIME-Version: 1.0\r\n";
                        $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                        $headers .= "From: {$mailto} <{$mailto}>\r\n";
                        $headers .= "Reply-To: <{$mailto}>\r\n";
                        $headers .= "Subject: {$subject}\r\n";
                        $headers .= "X-Mailer: PHP/".phpversion()."\r\n";
                        mail($to, $subject, $message, $headers);
            $em = $this->getDoctrine()->getManager();
        return $this->render('UserUserBundle:User:codeverification.html.twig');
          }  else {
                return $this->redirectToRoute('fos_user_security_check');
            } 
        }else {
                
             return $this->render('UserUserBundle:Exception:error.html.twig',array('error'=>'Date de validation de compte à été expiré'));
                
            }
        }
    }

    /**

     * Lists all User entities.

     *

     */

    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();



        $entities = $em->getRepository('UserUserBundle:User')->findAll();



        return $this->render('UserUserBundle:User:index.html.twig', array(

            'entities' => $entities,

        ));

    }



    /**

     * Creates a new User entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new User();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("User");
                        $hist->setMessage("créer un nouveau utilisateur" . $entity->getId()." - " . $entity->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

            $em->flush();



            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));

        }



        return $this->render('UserUserBundle:User:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a User entity.

     *

     * @param User $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(User $entity)

    {

        $form = $this->createForm(new UserType(), $entity, array(

            'action' => $this->generateUrl('user_create'),

            'method' => 'POST',

        ));



        $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new User entity.

     *

     */

    public function newAction()

    {

        $entity = new User();

        $form = $this->createCreateForm($entity);



        return $this->render('UserUserBundle:User:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Finds and displays a User entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('UserUserBundle:User')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find User entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('UserUserBundle:User:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }
    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserUserBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        return $this->render('UserUserBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        return $form;
    }
   /*     $margehotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($entity->getId());
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();
                foreach ($hotel as $value) {
                    $hotelmarge = new Hotelmarge();
                    $hotelmarge->setUser($entity);
                    $hotelmarge->setHotel($value);
                    $hotelmarge->setMarge($entity->getMarge());
                    $hotelmarge->setPrst($entity->getPrst());
                    $em->persist($hotelmarge);
                    $em->flush();
                }*/
        //    $form->add('submit', 'submit', array('label' => 'Update'));
      
    /**
    $hotel = $this->getDoctrine()
    ->getRepository('BtobHotelBundle:Hotelmarge')
    ->findByAgence($id);
    var_dump($hotel);
    die();
     */
    /**
     * Edits an existing User entity.

     *

     */

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserUserBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
        $margehotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($id);
    //    $count = count($margehotel);
       // for ($i=0; $i<$count; $i++) {
       $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find(8);
            if ($margehotel == null) {
                $margehotel = new Hotelmarge();
                $margehotel->setUser($entity);
                $margehotel->setHotel($hotel);
                $margehotel->setMarge($editForm->getData()->getMarge());
                $em->persist($margehotel);
                $em->flush();
            } else {
                $em = $this->getDoctrine()->getManager();
                
                $margehotel = $this->getDoctrine()
                                    ->getRepository('BtobHotelBundle:Hotelmarge')
                                    ->findByUser($id);   
                                    
            //    exit(var_dump($margehotel[0]->getId()));
            $div = $em->getRepository('BtobHotelBundle:Hotelmarge');
       $res = $div->createQueryBuilder('hm')
             ->update()
             ->set('hm.marge ',' :marge')
             ->set('hm.prst',':prst')
             ->where('hm.id = :idhm')
             ->setParameter('marge', $editForm->getData()->getMarge())
             ->setParameter('prst', $editForm->getData()->getPrst())
             ->setParameter('idhm', $margehotel[0]->getId())
             ->getQuery()
            ->execute();
          //      $margehotel->setMarge($editForm->getData()->getMarge());
        //        $margehotel->setPrst($editForm->getData()->getPrst());
         //       $em->flush();
            }
     //   }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("User");
                        $hist->setMessage("Modification: User" . $entity->getId()." - " . $entity->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            return $this->redirect($this->generateUrl('show_agence'));
        }
            
        return $this->render('UserUserBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }



    /**

     * Deletes a User entity.

     *

     */

    public function deleteAction(Request $request, $id)

    {

        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('UserUserBundle:User')->find($id);



            if (!$entity) {

                throw $this->createNotFoundException('Unable to find User entity.');

            }



            $em->remove($entity);

            $em->flush();

        }



        return $this->redirect($this->generateUrl('user'));

    }



    /**

     * Creates a form to delete a User entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('user_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm();

    }
    public function historiqueAction() {
        $em = $this->getDoctrine()->getManager();
     //   $entities = $em->getRepository('UserUserBundle:Historique')->findAll();
         $entities= $em->getRepository(Historique::class)
                        ->createQueryBuilder('h')
                        ->groupby('h.dcr')
                        ->orderby('h.id','DESC')
                        ->getQuery()
                        ->getResult();
                        
        $conn = $em->getConnection();
        $sql = 'select DISTINCT DATE_FORMAT(dcr, "%Y-%m-%d") AS date from Historique order by id desc';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $dcr= $stmt->fetchAll();
        return $this->render('UserUserBundle:Default:historique.html.twig', array(
            'entities' => $entities,
            'datecreation' => $dcr
            ));
    }

}

