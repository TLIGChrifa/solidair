<?php
/**
 * Created by PhpStorm.
 * User: poste 9
 * Date: 28/01/2015
 * Time: 09:28
 */

namespace User\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use User\UserBundle\Entity\Solde;
use User\UserBundle\Form\SoldeType;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;


class SoldeController extends Controller
{
    public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UserUserBundle:User')->findRole();
        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }

        return $this->render('UserUserBundle:Solde:home.html.twig', array(
            'entities' => $entities,
        ));
    }

public function agenceAction($userid,Request $request)
{
    $em = $this->getDoctrine()->getManager();
    $value = $request->get('_route_params');
    $x= (int)$value["userid"];
    $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($x);
         
    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($userid);

    if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
        $tab=array();
        foreach($solde as $value){
            if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                $tab[]=$value;
            }
        }
        $solde=$tab;

    }
    $solde=array_reverse($solde);
    $now = date("Y-m-d H:i:s");  
    //dump($nameagence->getSold()).die;
    $arrdate = array();
    //$oldesolde = $nameagence->getSold();
    foreach($solde as $s){
        // $newsolde = $oldesolde + $s->getSolde();
        
        //     if($now >= date_format($s->getDate(), 'Y-m-d H:i:s')){
        //     $nameagence->setsold($newsolde);
              
        // }
        
       
        $arrdate[]=date_format($s->getDate(), 'Y-m-d H:i:s');
        //dump(date_format($s->getDate(), 'Y-m-d H:i:s')).die;
      // $arrdate[]= $s->getDate();
    }
    //$em->flush();

    //dump($arrdate).die;
  
   
    return $this->render('UserUserBundle:Solde:agence.html.twig', array(
        'arraydate' =>$arrdate,
        'entitiessolde' => $solde,
        "nameagence" => $nameagence
    ));
}
public function getsoldeAction($userid,Request $request)
{
    $em = $this->getDoctrine()->getManager();
  
    $value = $request->get('_route_params');
    $x= (int)$value["userid"];
    $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($x);
         
    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($userid);

    if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
        $tab=array();
        foreach($solde as $value){
            if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                $tab[]=$value;
            }
        }
        $solde=$tab;

    }
    $solde=array_reverse($solde);
    $now = date("Y-m-d H:i:s");  
    //dump($nameagence->getSold()).die;
    $arrdate = array();
    //$oldesolde = $nameagence->getSold();
    foreach($solde as $s){
        // $newsolde = $oldesolde + $s->getSolde();
        
        //     if($now >= date_format($s->getDate(), 'Y-m-d H:i:s')){
        //     $nameagence->setsold($newsolde);
              
        // }
        
       
        $arrdate[]=date_format($s->getDate(), 'Y-m-d H:i:s');
        //dump(date_format($s->getDate(), 'Y-m-d H:i:s')).die;
      // $arrdate[]= $s->getDate();
    }
    //$em->flush();

  //  dump($solde).die;
  
   
    return $this->render('UserUserBundle:Solde:getsolde.html.twig', array(
        'arraydate' =>$arrdate,
        'entitiessolde' => $solde,
        "nameagence" => $nameagence
    ));
}


    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UserUserBundle:User')->findRole();
        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }

        return $this->render('UserUserBundle:Solde:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function addAction(Request $request,$userid)
    {
                $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($userid);
        $solde = new Solde();
        $form = $this->createForm(new SoldeType(), $solde);
        $request = $this->get('request');
    
//         function microtime_float()
// {
//     list($usec, $sec) = explode(" ", microtime());
//     return ((float)$usec + (float)$sec);
// }

// $time_start = microtime_float();

// // Attend pendant un moment
// usleep(100);

// $time_end = microtime_float();
// $time = $time_end - $time_start;

// echo "Ne rien faire pendant $time secondes\n";

        if ($request->getMethod() == 'POST') {
          
           // 
         
            $form->bind($request);
           // $date = $request->request->get("date");
        
          //  $datet= new \DateTime($date);
   // dump($solde->getDate()).die;
        // Tools::dump($datet,true);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $observation=$solde->getObservation();
                $oldesolde = $user->getSold();
                $newsolde = $oldesolde + $solde->getSolde();
                $solde->setObservation($observation);
                $solde->setSolde($solde->getSolde());
                
                $solde->setDate($solde->getDate());
                $solde->setAdminadd($user);
                $solde->setAgence($user);
                $em->persist($solde);
                
               // if($datet <= new \DateTime('now') ){
                   // var_dump('hjk');
                    //$user->setSold($newsolde);
               // }
                
                $em->persist($user);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("User");
                        $hist->setMessage("Ajouter " . $solde->getSolde() ." " .$user->getDevise()->getSymb(). " au solde d'agence " . $user->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                

           //     $userid =$solde->getAgence()->getId();
                return $this->redirect($this->generateUrl('user_solde_agence', array('userid' => $userid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('UserUserBundle:Solde:form.html.twig', array('form' => $form->createView(),'user'=>$user));
    }
}