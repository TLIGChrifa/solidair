<?php



namespace User\UserBundle\Controller;



use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\RedirectResponse;

use FOS\UserBundle\Model\UserInterface;

use FOS\UserBundle\FOSUserEvents;

use FOS\UserBundle\Event\FormEvent;

use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use User\UserBundle\Form\ResettingFormType;

use User\UserBundle\Entity\User;

use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\DashBundle\Entity\Devise;

use Btob\HotelBundle\Common\Tools;

use User\UserBundle\Form\Type\RegistrationFormType2;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;



class RegistrationController extends BaseController

{



    public function deleteAction($id)

    {

        $userManager = $this->container->get('fos_user.user_manager');

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));

        $om->remove($user);

        $om->flush();

        $url = $this->container->get('router')->generate('show_users');

        $response = new RedirectResponse($url);

        return $response;

    }

    public function deleteagenceAction($id)

    {

        $userManager = $this->container->get('fos_user.user_manager');

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));

        $om->remove($user);

        $om->flush();

        $url = $this->container->get('router')->generate('show_agence');

        $response = new RedirectResponse($url);

        return $response;

    }



    public function editAction(Request $request, $id){

        //Tools::dump($request->request, true);

        $userManager = $this->container->get('fos_user.user_manager');

//$user = new User();

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));
        $roles= $user->getRoles()[0];
        $user_role= $user->getRoles();
        $enabled= $user->isenabled();
//echo  Tools::dump($user->getRoles(), true); ;exit;

        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);


        if ( $request->getMethod() == 'POST' ) {

            $form->bind($request);

            if ($form->isValid()) {

                $user->setLogo($request->request->get('files'));
                if($roles == "ROLE_SUPER_ADMIN")
                {
                    $devise = $this->getDoctrine()
                        ->getRepository('BtobDashBundle:Devise')
                        ->find(1);
                    $user->setName("Admin");
                    $user->setDevise($devise);
                    $user->setIllimite(0);
                    
                    $user->setMarge(0);
                    $user->setPrst(0);
                    $user->setMargeomra(0);
                    $user->setActomra(0);
                    $user->setMargecircuit(0);
                    $user->setActcircuit(0);
                    $user->setMargesejour(0);
                    $user->setActsejour(0);
                    $user->setMargevole(0);
                    $user->setActvole(0);
                    $user->setMargecroissiere(0);
                    $user->setActcroissiere(0);
                    $user->setMargevoiture(0);
                    $user->setActvoiture(0);
                    $user->setActtransfert(0);
                    $user->setActbienetre(0);
                    $user->setActseminaire(0);
                    $user->setActgrandjeu(0);
                    $user->setActactivite(0);
                    $user->setMargeevenement(0);
                    $user->setActevenement(0);
                }
                if($request->request->get('fos_user_registration_form')['plainPassword']['first']!=null){
                $password=$request->request->get('fos_user_registration_form')['plainPassword']['first'];
                $newPasswordEncoded = $this->container->get('security.password_encoder')->encodePassword($user, $password);
                $user->setPassword($newPasswordEncoded);
                }
                if(in_array("ROLE_SUPER_ADMIN", $this->container->get('security.context')->getToken()->getUser()->getRoles())){

                    $role=$request->request->get('fos_user_registration_form')['roles'];
                    $enabled=$request->request->get('fos_user_registration_form')['enabled'];
                    $user->setRoles($role);
                    var_dump("bbbbbbbbbbb");
                }/* else { 
                    $data_user=array("0"=>$user_role[0]);
                    var_dump("aaaaaaaaaaaaaaa");
                    
                    $user->setRoles($data_user);
                }*/
               //     exit(var_dump($enabled));
                    $user->setEnabled($enabled);

                $userManager->updateUser($user);

        //            $user->getPlainPassword();
            


                $om->flush();



                if (in_array("AGENCEID", $user->getRoles()) || in_array("SALES", $user->getRoles())|| in_array("MONSITEWEB", $user->getRoles())) {

                    $em = $this->getDoctrine()->getManager();

                    foreach ($user->getHotelmarge() as $key => $value) {

                        $em->remove($value);

                        $em->flush();

                    }

                    $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();

                    foreach ($hotels as $key => $value) {

                    }

                }
//exit(var_dump($user = $this->container->get('security.context')->getToken()->getUser()->getRoles()));

                if(in_array("ROLE_SUPER_ADMIN", $this->container->get('security.context')->getToken()->getUser()->getRoles())){
                    if(in_array("AGENCEID", $user->getRoles())){
                        $url = $this->container->get('router')->generate('show_agence');
                    } else { 
                        $url = $this->container->get('router')->generate('show_users');
                    }
                } else {
                        $url = $this->container->get('router')->generate('btob_dash_homepage');
                }
                $response = new RedirectResponse($url);

                return $response;

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit.html.twig', array('form' => $form->createView(), 'id' => $id, "user" => $user,"roles" => $roles)

        );

    }

    public function editagenceAction(Request $request, $id){

        //Tools::dump($request->request, true);

        $userManager = $this->container->get('fos_user.user_manager');

//$user = new User();

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));
        $roles= $user->getRoles()[0];
        $user_role= $user->getRoles();
        $enabled= $user->isenabled();
//echo  Tools::dump($user->getRoles(), true); ;exit;

        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);


        if ('POST' === $request->getMethod()) {

            $form->bind($request);



            if ($form->isValid()) {

                $user->setLogo($request->request->get('files'));
                if($roles == "ROLE_SUPER_ADMIN")
                {
                    $devise = $this->getDoctrine()
                        ->getRepository('BtobDashBundle:Devise')
                        ->find(1);
                    $user->setName("Admin");
                    $user->setDevise($devise);
                    $user->setIllimite("NULL");
                    
                    $user->setMarge("NULL");
                    $user->setPrst("NULL");
                    $user->setMargeomra("NULL");
                    $user->setActomra("NULL");
                    $user->setMargecircuit("NULL");
                    $user->setActcircuit("NULL");
                    $user->setMargesejour("NULL");
                    $user->setActsejour("NULL");
                    $user->setMargevole("NULL");
                    $user->setActvole("NULL");
                    $user->setMargecroissiere("NULL");
                    $user->setActcroissiere("NULL");
                    $user->setMargevoiture("NULL");
                    $user->setActvoiture("NULL");
                    $user->setActtransfert("NULL");
                    $user->setActbienetre("NULL");
                    $user->setActseminaire("NULL");
                    $user->setActgrandjeu("NULL");
                    $user->setActactivite("NULL");
                    $user->setMargeevenement(0);
                    $user->setActevenement(0);
                    
                }

                $password=$request->request->get('fos_user_registration_form')['plainPassword']['first'];
                $newPasswordEncoded = $this->container->get('security.password_encoder')->encodePassword($user, $password);
                $user->setPassword($newPasswordEncoded);
                if(in_array("ROLE_SUPER_ADMIN", $this->container->get('security.context')->getToken()->getUser()->getRoles())){
                    $role=$request->request->get('fos_user_registration_form')['roles'];
                    $enabled=$request->request->get('fos_user_registration_form')['enabled'];
                    $user->setRoles($role);
                    $user->setEnabled($enabled);
                } else { 
                    $data_user=array("0"=>$user_role[0]);
              //      exit(var_dump($enabled));
                    $user->setRoles($data_user);
                    $user->setEnabled($enabled);
                }

                $userManager->updateUser($user);



                $om->flush();



                if (in_array("AGENCEID", $user->getRoles()) || in_array("SALES", $user->getRoles())|| in_array("MONSITEWEB", $user->getRoles())) {

                    $em = $this->getDoctrine()->getManager();

                    foreach ($user->getHotelmarge() as $key => $value) {

                        $em->remove($value);

                        $em->flush();

                    }

                    $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();

                    foreach ($hotels as $key => $value) {

                    }

                }



                if(in_array("ROLE_SUPER_ADMIN", $this->container->get('security.context')->getToken()->getUser()->getRoles())){
                    if(in_array("AGENCEID", $user->getRoles())){
                        $url = $this->container->get('router')->generate('show_agence');
                    } else { 
                        $url = $this->container->get('router')->generate('show_users');
                    }
                } else {
                        $url = $this->container->get('router')->generate('btob_dash_homepage');
                }

                $response = new RedirectResponse($url);

                return $response;

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:editagence.html.twig', array('form' => $form->createView(), 'id' => $id, "user" => $user,"roles" => $roles)

        );

    }



    public function edit2Action(Request $request, $id)

    {

        $userManager = $this->container->get('fos_user.user_manager');

//$user = new User();

        $om = $this->container->get('doctrine.orm.entity_manager');

        $user = $userManager->findUserBy(array('id' => $id));



        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);

        if ('POST' === $request->getMethod()) {

            $form->bind($request);



            if ($form->isValid()) {



                $userManager->updateUser($user);



                $om->persist($user);

                $om->flush();

                return $this->redirect($this->generateUrl('btob_dash_homepage'));

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit2.html.twig', array('form' => $form->createView(), 'id' => $id)

        );

    }



    public function registerAction(Request $request){

        $user = new User();

        $roles ="";

        $formFactory = $this->container->get('fos_user.registration.form.factory');
       
        $form = $formFactory->createForm();

        $form->setData($user);

        if ('POST' === $request->getMethod()) {

           // $form->bind($request);
           $form->handleRequest($request);
           $user->setEnabled(true);
          
          
       //    $image = $user->getImage();
           //dump($image).die;
           //var_dump($image);
            $om = $this->container->get('doctrine.orm.entity_manager');



           // if ($form->isValid()) {

                $userManager = $this->container->get('fos_user.user_manager');

                $event = new FormEvent($form, $request);



                // get default devise

                $datadevise = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findAll();

                $devise = null;

                foreach ($datadevise as $value) {

                    if ($value->getDef()) {

                        $devise = $value;

                    }

                }

                //$user->setDevise($devise);

             $userManager->createUser($user);

                 $login=$user->getUsername();
                
                $password=$user->getPlainPassword();
                //var_dump($user->getPlainPassword());

                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $user->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $user->getUsername().": votre compye à été créer " ;
                $headers = "From" .$user->getUsername().":" .$user->getEmail(). "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='

                 <img src="http://www.afritours.com.tn/front/images/logo.png" /> <br> <br>
				 MERCI DE VOTRE INSCRPTION! <br> <br>
				 Voici vos coordonnées de connexion au b2b:<br>

				' .'
				<b>Login :</b>'.$login.'<br>
				<b>Password :</b>'.$password.'




  </body>';


               // mail($to, $subject, $message1, $headers);
                    $devise = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->find(1);
                    $marcher = $this->getDoctrine()->getRepository('BtobHotelBundle:Marcher')->find(1);
                    $user->setDevise($devise);
                    $user->setMarcher($marcher);
                    $user->setMarge(0);
                    $user->setMargecircuit(0);
                    $user->setMargesejour(0);
                    $user->setMargevole(0);
                    $user->setMargeomra(0);
                    $user->setMargeparc(0);
                    $user->setMargebienetre(0);
                    $user->setMargecroissiere(0);
                    $user->setMargevoiture(0);
                    $user->upload();
                $om->persist($user);

                $om->flush();

                

                if($user->getRoles() !== 'AGENCEID')
                $url = $this->container->get('router')->generate('show_users');
                else
                $url = $this->container->get('router')->generate('show_agence');

                $response = new RedirectResponse($url);

                return $response;

          //  }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:edit.html.twig', array('form' => $form->createView(),"roles" => $roles)

        );

    }



    public function agenceregisterAction(Request $request){

        $user = new User();
        $roles ="";

        $formFactory = $this->container->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        $form->setData($user);

        if ('POST' === $request->getMethod()) {

            $form->bind($request);

            //Tools::dump($form,true);

            $om = $this->container->get('doctrine.orm.entity_manager');



            if ($form->isValid()) {

                $userManager = $this->container->get('fos_user.user_manager');

                $event = new FormEvent($form, $request);



                // get default devise

                $datadevise = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findAll();

                $devise = null;

                foreach ($datadevise as $value) {

                    if ($value->getDef()) {

                        $devise = $value;

                    }

                }

                //$user->setDevise($devise);

             $userManager->createUser($user);

                 $login=$user->getUsername();

                $password=$user->getPlainPassword();


                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $user->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $user->getUsername().": votre compye à été créer " ;
                $headers = "From" .$user->getUsername().":" .$user->getEmail(). "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='

                 <img src="http://www.afritours.com.tn/front/images/logo.png" /> <br> <br>
                 MERCI DE VOTRE INSCRPTION! <br> <br>
                 Voici vos coordonnées de connexion au b2b:<br>

                ' .'
                <b>Login :</b>'.$login.'<br>
                <b>Password :</b>'.$password.'




  </body>';


                mail($to, $subject, $message1, $headers);

                    $user->setMarge(0);
                    $user->setMargecircuit(0);
                    $user->setMargesejour(0);
                    $user->setMargevole(0);
                    $user->setMargeomra(0);
                    $user->setMargeparc(0);
                    $user->setMargebienetre(0);
                    $user->setMargecroissiere(0);
                    $user->setMargevoiture(0);
                $om->persist($user);

                $om->flush();                

                $url = $this->container->get('router')->generate('show_agence');

                $response = new RedirectResponse($url);

                return $response;

            }

        }



        return $this->container->get('templating')->renderResponse(

            'UserUserBundle:Registration:editagence.html.twig', array('form' => $form->createView(),"roles" => $roles)

        );

    }



}

