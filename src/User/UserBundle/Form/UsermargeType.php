<?php

namespace User\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsermargeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut de période",'format' => 'dd/MM/yyyy'))
            ->add('datef', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))
            ->add('marge',NULL, array('required' => true, 'label' => "Marge"))
            ->add('perst',NULL, array('required' => false, 'label' => 'Pourcentage'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'User\UserBundle\Entity\Usermarge'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_userbundle_usermarge';
    }
}
