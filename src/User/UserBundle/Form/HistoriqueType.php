<?php

namespace User\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class HistoriqueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip', 'text', array('label' => "ip adress", 'required' => false))
            ->add('type', 'text', array('label' => "Type", 'required' => false))
            ->add('bundle', 'text', array('label' => "Bundle", 'required' => false))
            ->add('message', 'text', array('label' => "Message", 'required' => false))
            ->add('dcr', 'datetime', array('widget' => 'single_text','data' => new \DateTime("now"), 'label' => "Date de creation"))
            ->add('user', EntityType::class, array('class'=>'UserUserBundle:User', 'choice_label'=>'id','label'=>'User'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'User\UserBundle\Entity\Historique'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_userbundle_historique';
    }
}
