<?php



namespace User\UserBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;



class UserType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('name', 'text', array('label' => "Nom de l'agence", "required" => false))
            ->add('codeverification', 'text', array('label' => "Code de verification", "required" => false))

            //->add('datenaiss')

            //->add('dcr')

            ->add('devise', 'entity', array(

                'class' => 'BtobDashBundle:Devise',

                'empty_value' => 'Choisissez une devise',

                'required' => true,

                'label' => "Devise",

            ))

            ->add('marcher', 'entity', array(

                'class' => 'BtobHotelBundle:Marcher',

                'empty_value' => 'Choisissez un marché',

                'required' => true,

                'label' => "Marché",

            ))

            ->add('responsable', 'text', array('label' => "Responsable", "required" => false))

            ->add('adresse', 'text', array('label' => "Adresse", "required" => false))

            ->add('cp', 'text', array('label' => "Code postal", "required" => false))

            ->add('tel', 'text', array('label' => "Téléphone", "required" => false))

            ->add('fax', 'text', array('label' => "Fax", "required" => false))

            ->add('matriculefiscal', 'text', array('label' => "Matricule Fiscal", "required" => false))

            ->add('observation', 'text', array('label' => "Observation", "required" => false))

            //->add('logo', 'text', array('label' => "logo", "required" => false))

            ->add('marge', 'text', array('label' => "Marge Hôtel", "required" => false))

            ->add('sold', 'text', array('label' => "Solde de l'agence", "required" => false))

            ->add('prst', null, array('label' => "Pourcentage de marge par hôtel", "required" => false))

            ->add('illimite', null, array('label' => "Solde illimité", "required" => false))
            ->add('etatsoldetolore', null, array('label' => "Etat solde toléré", "required" => false))
            ->add('montantsoldetolore', 'text', array('label' => "Montant solde toléré", "required" => false))
            ->add('dateexpiration','date', array('label' => "Date d'expiration","widget" => "single_text", "required" => false))

           // ->add('marque', 'text', array('label' => "Marge marque blanche", "required" => false))

            //->add('prstmarque', null, array('label' => "Pourcentage marque blanche", "required" => false))





            ///////////////////////////////////////////////////////////////////////////////////

            ->add('actomra', null, array('label' => "Activation module omra", "required" => false))
             ->add('margeomra', 'text', array('label' => "Marge omra", "required" => false))
            ->add('prstomra', null, array('label' => "Pourcentage", "required" => false))
            ->add('margecircuit', 'text', array('label' => "Marge circuit", "required" => false))
            ->add('prstcircuit', null, array('label' => "Pourcentage", "required" => false))
            ->add('actcircuit', null, array('label' => "Activation module circuit", "required" => false))

            ->add('margesejour', 'text', array('label' => "Marge séjour", "required" => false))
             ->add('prstsejour', null, array('label' => "Pourcentage", "required" => false))
            ->add('actsejour', null, array('label' => "Activation module séjour", "required" => false))
             ->add('margeevenement', 'text', array('label' => "Marge événement",'data' => '0', "required" => false))

             ->add('prstevenement', null, array('label' => "Pourcentage", "required" => false))

            ->add('actevenement', null, array('label' => "Activation module événement", "required" => false))

            ->add('margevole', 'text', array('label' => "Marge billetterie", "required" => false))

            ->add('actvole', null, array('label' => "Activation billetterie", "required" => false))

            //->add('logo', 'text', array('label' => "logo", "required" => false))

            ->add('margecroissiere', 'text', array('label' => "Marge croissiére", "required" => false))
             ->add('prstcroissiere', null, array('label' => "Pourcentage", "required" => false))
            ->add('actcroissiere', null, array('label' => "Activation croissiére", "required" => false))

            ->add('margevoiture', 'text', array('label' => "Marge voiture", "required" => false))
             ->add('prstvoiture', null, array('label' => "Pourcentage", "required" => false))
            ->add('actvoiture', null, array('label' => "Activation voiture", "required" => false))

             ->add('actparc', null, array('label' => "Activation parc", "required" => false))
             ->add('margeparc', 'text', array('label' => "Marge parc", "required" => false))
            ->add('prstparc', null, array('label' => "Pourcentage", "required" => false))    
                
            ->add('acttransfert', null, array('label' => "Activation transfert", "required" => false))

            ->add('actbienetre', null, array('label' => "Activation bien être", "required" => false))
             ->add('margebienetre', 'text', array('label' => "Marge Bien etre", "required" => false))
             ->add('prstbienetre', null, array('label' => "Pourcentage", "required" => false))
            
            ->add('actseminaire', null, array('label' => "Activation séminaire", "required" => false))

            ->add('actgrandjeu', null, array('label' => "Activation grand jeu", "required" => false))

            ->add('actactivite', null, array('label' => "Activation activité", "required" => false))

        ;

    }

    

    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'User\UserBundle\Entity\User'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'user_userbundle_user';

    }

}

