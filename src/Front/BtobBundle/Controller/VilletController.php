<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Villet;
use Symfony\Component\HttpFoundation\Request;
use Btob\TransfertBundle\Form\VilletType;
use Symfony\Component\HttpFoundation\JsonResponse;

class VilletController extends Controller {




    public function ajxPaystAction() {
        $request = $this->get('request');
        $id=$request->request->get("id");
        $villet = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Villet')
                ->findByPayst($id);
        
        return $this->render('FrontBtobBundle:Villet:optiont.html.twig', array('entity'=>$villet)
        );
    }

}
