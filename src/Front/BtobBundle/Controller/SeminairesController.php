<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ActiviteBundle\Entity\Activite;
use Btob\ActiviteBundle\Entity\Resevationact;
use Btob\HotelBundle\Common\Tools;


use Btob\HotelBundle\Form\ClientsType;


use Btob\HotelBundle\Entity\Clients;

use Btob\CuircuitBundle\Entity\Cuircuit;

use Btob\OmraBundle\Entity\Omra;
use User\UserBundle\Entity\User;
use Btob\HotelBundle\Entity\Hotel;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\HotelBundle\Entity\Hotelthemes;
use Btob\SiminaireBundle\Entity\Siminaire;
use Btob\SiminaireBundle\Entity\Reservationsim;
use Btob\SiminaireBundle\Entity\Tsalle;

class SeminairesController extends Controller
{
  

    public function reservationsiminairesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

       
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $capacite = $request->request->get('capacite');
                $type = $request->request->get('type');
                $nbsalle = $request->request->get('nbsalle');
                $categorie = $request->request->get('categorie');
                $gala = $request->request->get('gala');
                 if($gala=="on")
                {
                    $gala =true;
                }
                else{
                    
                    $gala =false;
                }
                $incentive = $request->request->get('incentive');
                
                 if($incentive=="on")
                {
                    $incentive =true;
                }
                else{
                    
                    $incentive =false;
                }
                $remarque = $request->request->get('remarque');
                
                
                $nbparticipant = $request->request->get('nbparticipant');
                
                $nbchsingle = $request->request->get('nbchsingle');
                
                $nbchdouble = $request->request->get('nbchdouble');
                
                $nbchtriple = $request->request->get('nbchtriple');
                
                $nbchquadtriple = $request->request->get('nbchquadtriple');
                
                $arrangement = $request->request->get('arrangement');
                
                $lieu = $request->request->get('lieu');
                
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                
                $dater = new \Datetime(Tools::explodedate($request->request->get('dater'),'/'));
                
                
                

                $reservation = new Reservationsim();
                $reservation->setAgent($User);
                $reservation->setClient($client);
               
               
                $reservation->setNbsalle($nbsalle);
                $reservation->setCategorie($categorie);
              
                $reservation->setGala($gala);
                
                
                $reservation->setIncentive($incentive);
               
               
                $reservation->setRemarque($remarque);
              
                $reservation->setNbparticipant($nbparticipant);
                
                $reservation->setNbchsingle($nbchsingle);
                
                $reservation->setNbchdouble($nbchdouble);
                
                $reservation->setNbchtriple($nbchtriple);
                
                $reservation->setNbchquadtriple($nbchquadtriple);
                
                $reservation->setArrangement($arrangement);
                $reservation->setLieu($lieu);
                
                $reservation->setDated($dated);
                
                $reservation->setDater($dater);
                
                
                $em->persist($reservation);
                $em->flush();
                
                
                
                
                 foreach( $capacite as $key=>$value){
                    $catg = new Tsalle();
                    $catg->setReservationsim($reservation);
                    $catg->setCapacite($capacite[$key]);
                    $catg->setType($type[$key]);
                   // $catg->setAgeadult(new \Datetime(Tools::explodedate($agead[$key],'/')));
                 
                    $em->persist($catg);
                    $em->flush();

                }
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
				
				
				                                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "Afritours: Réservtaion congrés & séminaires " ;
                $headers = "From:Afritours " .$reservation->getAgent()->getEmail(). "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                //mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservtaion congrés & séminaires" ;
                $header = "From" .$reservation->getAgent()->getName().":" .$reservation->getAgent()->getEmail(). "\n";
                $header .= "Reply-To:" .$reservation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 vous avez reçu une réservation congrés & séminaires , merci de consulter votre backoffice .

                              <a href="http://www.afritours.com.tn/b2b/'.$reservation->getId().'/siminaire/detail/reservation"> Cliquer Içi</a>';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';


                mail($too, $subjects, $message2, $header);
                //mail("afef.tuninfo@gmail.com", $subjects, $message2, $header);
                
                
                
                
                
                $request->getSession()->getFlashBag()->add('notiSiminaire', 'Votre message a été bien envoyé. Merci.');

                return $this->redirect($this->generateUrl('Front_btob_siminaire_reservation_homepage'));



            }
        }

        return $this->render('FrontBtobBundle:Siminaire:reservation.html.twig', array('form' => $form->createView()));
    }


}
