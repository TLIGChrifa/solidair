<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\SpaBundle\Entity\Spa;

use Btob\HotelBundle\Common\Tools;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use User\UserBundle\Entity\User;


use Btob\SpaBundle\Entity\Resapa;


use Btob\SpaBundle\Entity\Reservationspa;

use Btob\SpaBundle\Entity\Reservationspdetail;
use Symfony\Component\HttpFoundation\Request;



class SpaController extends Controller

{



    //  Spa !!!

    public function spaAction(Request $request)

    {
       
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->listPrice();
      
       
       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $spaprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        
      
        return $this->render('FrontBtobBundle:Spa:index.html.twig', array(

            'entities' => $entities,
            
            'spaprices' => $spaprices

  
            ));


    }



    public function detailAction(Spa $spa)

    {
       
        
       $spaperiode = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());
          
        return $this->render('FrontBtobBundle:Spa:detail.html.twig', array('entry' => $spa,'periods' => $spaperiode));

    }

    
     public function personalisationAction(Spa $spa)

    {

       $em = $this->getDoctrine()->getManager();
       
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $spaprice = $request->request->get("spaprice");
            $session->set('spaprice', $spaprice);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
          
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('front_inscrip_reservation_spa', array('id'=>$spa->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Spa:personalisation.html.twig', array('entry' => $spa,'spaprices' => $spaprices));

    
    }
    
    
    
    
    public function inscriptionAction(Spa $spa)

    {

       $em = $this->getDoctrine()->getManager();
       
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());
       $spasupp = $this->getDoctrine()->getRepository("BtobSpaBundle:Supplementsp")->findSupplementBySpa($spa->getId());
       
       $spareduc = $this->getDoctrine()->getRepository("BtobSpaBundle:Reductionsp")->findReductionBySpa($spa->getId());

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $spaprice = $session->get("spaprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
       
        $nbenf = $session->get("nbenf");
        
        $Array = array();
        
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                 if(isset($request->request->get("reducad")[$i][$j]))
                 {
                     $adad[$j]['reducad']= $request->request->get("reducad")[$i][$j];
                 }
                 else{
                   $adad[$j]['reducad']=null;  
                 }
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                 if(isset($request->request->get("reducenf")[$i][$k]))
                 {
                     $enf[$k]['reducenf']= $request->request->get("reducenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['reducenf']=null;  
                 }
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
              
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('spaprice', $spaprice);
            
          

          return $this->redirect($this->generateUrl('front_resa_reservation_spa', array('id'=>$spa->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Spa:inscription.html.twig', array('entry' => $spa,'spasupp' => $spasupp,'spareduc' => $spareduc,'spaprices' => $spaprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbenf' => $nbenf));

    
    }
    
    
    
    
    
    
    
    
    public function reservationAction(Spa $spa)

    {

        $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $spaperiode = $session->get("spaprice");
        
        
       $pricespa =   $this->getDoctrine()->getRepository('BtobSpaBundle:Spaprice')->find($spaperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
          
               
           $totalsuppad=0;
           $totalsuppenf=0;
           
           
           $totalreducad=0;
           $totalreducenf=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  $reducaddp =0;
               
                  if(isset($val1['reducad']))
                  {
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                  
                  $totalreducad+= $reducaddp;
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              //reduction
              
              
              $reducenfp=0;
                  if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
              $totalreducenf+= $reducenfp;
              
              
              
              
           }
           
           }
           
           
           
      
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobSpaBundle:Spaprice')
                        ->calcul($pricespa,$nbad,$nbenf,$totalsuppad,$totalsuppenf,$totalreducad,$totalreducenf);
         
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $spaprice = $session->get("spaprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationspa();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setSpa($spa);
                $resa->setSpaprice($pricespa);
                
                $resa->setTotal($total);
                  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
  
               
           $totalsuppad=0;
           $totalsuppenf=0;
           
           $totalreducad=0;
           $totalreducenf=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               $reducaddp =0; 
           
           $resadetail=new Reservationspdetail();
           $resadetail->setReservationspa($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  if(isset($val1['reducad']))
                  {
                      
                     

 
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                 
                  $resadetail->setReducad(json_encode($ArrBase[$key]['adult'][$k1]['reducad']));
                  $totalreducad+= $reducaddp;
                  
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               $reducenfp=0;
               
           $resadetails=new Reservationspdetail();
           $resadetails->setReservationspa($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
              //reduction
              
              if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
                  
              $resadetails->setReducenf(json_encode($ArrBase[$key]['enf'][$k1e]['reducenf']));
  
              $totalreducenf+= $reducenfp;
              
              
              
           
          
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
          
           
        
          $ArrBase[$key]['price'] = $this->getDoctrine()
                        ->getRepository('BtobSpaBundle:Spaprice')
                        ->calcul($pricespa,$nbad,$nbenf,$totalsuppad,$totalsuppenf,$totalreducad,$totalreducenf);
         
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $resa->getClient()->getEmail();
               


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "Afritours: Réservation Bienetre" ;
                $headers = "From:Afritours info@afritours.com.tn\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
				 <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservtaion Bienetre" ;
                $header = "From:Afritours <".$to.">\n";
                $header .= "Reply-To:Afritours info@afritours.com.tn\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 vous avez reçu une réservation Bienetre , merci de consulter votre backoffice .';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';


                mail($too, $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                
               

            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('notifspafront', 'Votre demande a été bien envoyée.');

                return $this->redirect($this->generateUrl('front_btob_spa_homepage'));



        }

        return $this->render('FrontBtobBundle:Spa:reservation.html.twig', array('form' => $form->createView(),'entry'=>$spa,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));

    }


}

