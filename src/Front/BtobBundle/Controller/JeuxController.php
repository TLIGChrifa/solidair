<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;

use Btob\JeuxBundle\Entity\Choix;
use Btob\JeuxBundle\Entity\Jeu;
use Btob\JeuxBundle\Entity\Reponse;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\AgenceBundle\Entity\Info;
use Symfony\Component\Validator\Constraints\DateTime;

class JeuxController extends Controller
{



    public function indexAction()
    {

        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $Question = $this->getDoctrine()->getRepository("BtobJeuxBundle:Jeu")->findBy( array(),array(),
            1 );

        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $User =  $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $question = $request->request->get('question');
                $optionsRadios = $request->request->get('optionsRadios');
                if($optionsRadios != null){
                    $Reponse = new Reponse();
                    $Reponse->setChoix($optionsRadios);
                    $Reponse->setQuestion($question);
                    $Reponse->setAgent($User);
                    $Reponse->setClient($client);
                    $em->persist($Reponse);
                    $em->flush();


                    $client = new Clients();

                    $form = $this->createForm(new ClientsType(), $client);





                    $request->getSession()->getFlashBag()->add('notiQuestion', 'Votre message a bien été envoyé. Merci.');


                }
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));


                $to = $Reponse->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $Reponse->getAgent()->getName().": Merci de votre réservation... " ;
                $headers = "From" .$Reponse->getAgent()->getName().":" .$admin->getEmail(). "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<img src="http://www.afritours.com.tn/front/images/logo.png" /><br>';
                $message1 .= '
				<br /> <b>Cher Madame/Monsieur,</b><br />
				 Merci pour votre réponse.<br />
				 <table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Votre Réponse :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getAgent()->getName().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Néf  Réponse</b></td>';
                $message1 .= '<td width="20%" height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getId().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Question</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getQuestion().' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Choix</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getChoix().' </td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getCiv().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getPname().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getName().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getEmail().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getTel().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getCin().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getAdresse().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getCp().'</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">'.$Reponse->getClient()->getPays()->getName().'</td>';
                $message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .='</body>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "  réservation  Jeux" ;
                $header = "From" .$Reponse->getAgent()->getName().":" .$admin->getEmail(). "\n";
                $header .= "Reply-To:" .$Reponse->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<img src="http://www.afritours.com.tn/front/images/logo.png" /><br>';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Sélection :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getAgent()->getName().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Réf réponse</b></td>';
                $message2 .= '<td width="20%" height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getId().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Réponse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getQuestion().' '.$Reponse->getChoix().'</td>';
                $message2 .= '</tr>';



                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getCiv().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getPname().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getName().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getEmail().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getTel().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getCin().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getAdresse().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getCp().'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$Reponse->getClient()->getPays()->getName().'</td>';
                $message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';

                mail($too, $subjects, $message2, $header);
               
            } else {
                echo $form->getErrors();
            }
			 return $this->redirect($this->generateUrl('front_jeuxx_homepage'));
        }
        return $this->render('FrontBtobBundle:Jeux:reservation.html.twig', array('form' => $form->createView() , 'Question'=>$Question,
            'info'=>$info

    ));
    }

}
