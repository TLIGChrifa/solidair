<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\AgenceBundle\Entity\Info;
use User\UserBundle\Entity\User;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\BienetreBundle\Entity\Tbien;
use Symfony\Component\HttpFoundation\Request;

class BienetreController extends Controller
{

    public function bienetreAction()
    {
        $biens = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->findby(array('act' => 1));
         $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $biens,
            $request->query->get('page', 1)/*page number*/,
            8/*limit per page*/
        );
         
         return $this->render('FrontBtobBundle:Bien:index.html.twig', array('entities' => $entities));
    }

    public function detailbienetreAction(Bienetre $Bienetre)
    {
        return $this->render('FrontBtobBundle:Bien:detail.html.twig', array('entry' => $Bienetre));
    }


    public function reservationbienbienetreAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->find($id);
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
             $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $reservation = new Reservationbienetre();
                $datea = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                $defaulta = $request->request->get('defaulta');
                

                $reservation->setAgent($User);
                $reservation->setClient($client);
                $reservation->setBienetre($entities);
                $reservation->setDatear($datea);
               
                $reservation->setHeur($defaulta);
                $em->persist($reservation);
                $em->flush();
                
                
                 foreach( $named as $key=>$value){
                    $catg = new Tbien();
                    $catg->setReservationbienetre($reservation);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
                 
                    $em->persist($catg);
                    $em->flush();

                }
				
                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));




                $to = $reservation->getClient()->getEmail();


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "Afritours: Réservation Bienetre" ;
                $headers = "From:Afritours info@afritours.com.tn\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
				 <img src="http://www.afritours.com.tn/front/images/logo.png" /><br><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.
				  </body>';



                mail($to, $subject, $message1, $headers);
                //mail('afef.tuninfo@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservtaion Bienetre" ;
                $header = "From:Afritours info@afritours.com.tn <".$to.">\n";
                $header .= "Reply-To:" .$reservation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                <img src="http://www.afritours.com.tn/front/images/logo.png" /><br><br>
				 Bonjour,<br>
				 vous avez reçu une réservation Bienetre , merci de consulter votre backoffice .

                 <a href="http://www.afritours.com.tn/b2b/'.$reservation->getId().'/bienetre/detail/reservation"> Cliquer Içi</a>



  </body>';


                mail($too, $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);

                $request->getSession()->getFlashBag()->add('notiBienetre', 'Votre message a bien été envoyé. Merci.');
                return $this->redirect($this->generateUrl('front_bienetre_homepage'));


            }
        }

        return $this->render('FrontBtobBundle:Bien:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }




}
