<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\HotelBundle\Entity\Hotel;
use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\HotelBundle\Entity\Hotelprice;
use Btob\AgenceBundle\Entity\Info;


use Btob\HotelBundle\Entity\Pricearr;
use Btob\HotelBundle\Entity\Pricesupplement;
use Btob\HotelBundle\Entity\Pricechild;
use Btob\HotelBundle\Entity\Priceroom;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Entity\Reservationdetail;
use Btob\HotelBundle\Entity\Notification;
use Btob\HotelBundle\Entity\Email;

class DefaultController extends Controller
{



public  function  hotelAction(){

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);

    $Hotel = $this->getDoctrine()->getRepository("BtobHotelBundle:Ville")->findBy(array(),array(),10);
    return $this->render('FrontBtobBundle:Default:hotel.html.twig', array(
        'hotel'=>$Hotel,'info'=>$info

    ));
}
 public function sejourxAction() 
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $sejour = $em->getRepository("BtobSejourBundle:Sejour")->findby(
            array('act' => 1),
            array('dcr' => 'desc')

        );
        return $this->render('FrontBtobBundle:Default:sejourindex.html.twig', array('sejourindex' => $sejour,'info'=>$info));


    }
    public  function  promoAction(){


            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $promo = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotel")->findBy(array(),array(),10);
        return $this->render('FrontBtobBundle:Default:promo.html.twig', array(
            'promo'=>$promo,'info'=>$info

        ));
    }


	 public function indexAction()
    {
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $hoetl=array();
        $idhotel=array();
        $hotelprice=array();
        $hoteldate=array();
        $ii=1;
        $coup = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotel")->findby(
            array(
                'act' => 1,

            )

        );
       

      
        $Sejour = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->listPrice();
        $rr=0;
        $ff=0;
        $Sejours=array();  $Sejours2=array();
        foreach($Sejour as $value){
            
                $Sejours[$rr]=$value;
                $rr++;
            
            
        }

        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if(!is_null($user))
        $marge = $user->getHotelmarge();
        $agence = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if(!is_null($agence))
        $agenceact =$agence->getId();



        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if(!is_null($user))
        $marge = $user->getHotelmarge();
        $agence = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if(!is_null($agence))
        $agenceact =$agence->getId();



        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();
        $value = $request->get('_route_params');
        // valeur de submit
        $nbpage = 5;
        $nbjour = 1;
        $paysid = "3";
        $ville = "";
        $nom = "";
        $trie = 1;
        $price_min = 0;
        $price_max = 3000;
        $star = 0;
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $datedd = $dt->format("Y-m-d");
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $page = 1;
        $ArrFinal = array();
        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();
       
        $totpage = 1;
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => 3);


        $villes = $this->getDoctrine()->getRepository('BtobHotelBundle:Ville')->findByPays(3);
        
        




        //Tools::dump($request->request);
        $paysid = $request->request->get("pays");

        $session->set('paysid', $paysid);
        $nom = $request->request->get("nom");
        $session->set('nom', $nom);
        $ville = $request->request->get("ville");
        $session->set('ville', $ville);
        $trie = $request->request->get("trie");
        $price_min = 0;
        $session->set('price_min', $price_min);
        $price_max = 3000;
        $session->set('price_max', $price_max);
        $star = 0;
        $session->set('star', $star);
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $datedd = $dt->format("Y-m-d");
        $session->set('dated', $dated);
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $session->set('datef', $datef);
        $page = 1;
        $session->set('page', $page);
        $ad = array(0 => 2);
        $session->set('ad', $ad);

        $enf = array(0 => 0);
        $session->set('enf', $enf);
        $arr = array(0 => 3);
        $session->set('arr', $arr);
        //Tools::dump($request->request, true);
        // recupération de hotel price
        $tab = explode("/", $dated);

        $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);

        $tab2 = explode("/", $datef);
        $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);

        // calculer la deference entre dated et dates
        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
      $promotion = array();
         if(!is_null($user)){
             
         
        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
        $i = 0;
       // $promotion = array();
        
      
        foreach ($price as $key => $value) {

            if ($value->getHotel()->getAct()) {

                $sstopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:Stopsales')->findbyhoteldate($value->getHotel(),$d1, $d2);
                if(count($sstopsales)<1){
                    $ath= array();


                    $hoteltheme = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelthemes')->listThemeHotel($value->getHotel());
                    foreach ($hoteltheme as $kth=>$valth)
                    {
                        array_push($ath, $valth->getThemes()->getName());
                    }

                    if(in_array("coup de coeur", $ath)||in_array("Voyages de noces", $ath)||in_array("affaire", $ath)||in_array("PROMO", $ath))

                    {


                        $surdemande = false;
                        if ($value->getNbnuit() > $nbjour) {
                            $surdemande = true;
                        }
                        //$filterhotel[] = $value->getHotel();

                        if(in_array("coup de coeur", $ath))
                        {
                            $ArrFinal[$i]["themescoup"] = "coup de coeur";
                        }
                        else{
                            $ArrFinal[$i]["themescoup"] = "";
                        }

                        if(in_array("affaire", $ath))
                        {
                            $ArrFinal[$i]["themesaffaire"] = "affaire";
                        }
                        else{
                            $ArrFinal[$i]["themesaffaire"] = "";
                        }

                        if(in_array("Voyages de noces", $ath))
                        {
                            $ArrFinal[$i]["themesnoce"] = "Voyages de noces";
                        }
                        else{
                            $ArrFinal[$i]["themesnoce"] = "";
                        }
						if(in_array("PROMO", $ath))
                        {
                            $ArrFinal[$i]["themespromo"] = "PROMO";
                        }
                        else{
                            $ArrFinal[$i]["themespromo"] = "";
                        }
						if(in_array("BON PLAN", $ath))
                        {
                            $ArrFinal[$i]["themesbon"] = "BON PLAN";
                        }
                        else{
                            $ArrFinal[$i]["themesbon"] = "";
                        }
                        $ArrFinal[$i]["hotel"] = $value->getHotel();
                        $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                        $ArrFinal[$i]['majminstay'] = $value->getMajminstay();
                        $ArrFinal[$i]['redminstay'] = $value->getRedminstay();
                        $ArrFinal[$i]['persmajminstay'] = $value->getPersmajminstay();
                        $ArrFinal[$i]['persredminstay'] = $value->getPersredminstay();

                        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(),$datedd);



                     
                        if($promotion)
                        {
                            $ArrFinal[$i]['promo'] = $promotion->getName();
                            $ArrFinal[$i]['datedpromo'] = $promotion->getDated();
                            $ArrFinal[$i]['datefpromo'] = $promotion->getDates();
                            $ArrFinal[$i]['valp'] = $promotion->getVal();
                            $ArrFinal[$i]['persp'] = $promotion->getPers();
                        }
                        else{
                            $ArrFinal[$i]['promo'] ="";
                            $ArrFinal[$i]['datedpromo'] ="";
                            $ArrFinal[$i]['datefpromo'] ="";
                            $ArrFinal[$i]['valp'] = "";
                            $ArrFinal[$i]['persp'] = "";
                            $promotion = array();

                        }
						
						if($ArrFinal[$i]["themespromo"] = "PROMO")
						{
						$ArrFinal[$i]['promot'] = "PROMO";
						}else{
						$ArrFinal[$i]['promot'] = "";
						}
                        // calcule variante de marge


                        $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                        $ArrFinal[$i]["hotelid"] =intval($value->getHotel()->getId());




                        $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                        $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                        $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                        // gestion des images
                        $dataimg = $value->getHotel()->getHotelimg();
                        $img = "/back/img/dummy_150x150.gif";
                        $j = 0;
                        foreach ($dataimg as $keyimg => $valimg) {
                            if ($j == 0)
                                $img = $valimg->getFile();
                            if ($valimg->getPriori())
                                $img = $valimg->getFile();
                            ++$j;
                        }
                        $ArrFinal[$i]["image"] = $img;
                        /*
                         * gestion des prix
                         */
                        // minimaume d'arrangement
                        $min = 0;
                        $j = 0;
                        $tab = array();
                        $name = "";
                        $pers = false;
                        $etat = false;
                        foreach ($value->getPricearr() as $keyarr => $valarr) {
                            if ($valarr->getEtat() == 1) {
                                if ($valarr->getMinstay() > $nbjour) {
                                    $surdemande = true;
                                }
                                if ($j == 0) {
                                    $min = $valarr->getPrice();
                                    $pers = $valarr->getPers();
                                    $etat = $valarr->getEtat();
                                    $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                    $margeprice = $valarr->getMarge();
                                    $persmprice = $valarr->getPersm();
                                } else {
                                    if ($min > $valarr->getPrice()) {
                                        $min = $valarr->getPrice();
                                        $pers = $valarr->getPers();
                                        $etat = $valarr->getEtat();
                                        $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                        $margeprice = $valarr->getMarge();
                                        $persmprice = $valarr->getPersm();

                                    }
                                }
                                ++$j;
                            }

                        }
                        if ($pers) {
                            $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

                        } else {
                            $ArrFinal[$i]["price"] = $min + $value->getPrice();
                        }
                        // calcule de la marge
                        $ArrFinal[$i]["price"] = $ArrFinal[$i]["price"];
                        $ArrFinal[$i]["name"] = $name;
                        $ArrFinal[$i]["margeprice"] = $margeprice;
                        $ArrFinal[$i]["persmprice"] = $persmprice;

                        // gestion des chambre
                        $k = 0;
                        //Tools::dump($ad);
                        foreach ($ad as $kad => $valad) {
                            $nbpersonne = $valad + $enf[$kad];
                            //if ($nbpersonne > 4) $nbpersonne = 4;
                            $room = null;
                            // test dispo room
                            $hotelroom = $value->getHotel()->getHotelroom();
                            $typepersonne = false;
                            foreach ($hotelroom as $khr => $vhr) {
                                if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                    $room = $vhr->getRoom();
                                    $typepersonne = $vhr->getPersonne();
                                }
                            }
                            //test dispo arrangement
                            $arrang = null;
                            $hotelarrangement = $value->getHotel()->getHotelarrangement();

                            foreach  ($value->getPricearr() as $keyarr => $valarr) {


                                if($valarr->getEtat()==1 && $valarr->getPrice()==0)
                                {
                                    $arrang =$valarr->getHotelarrangement()->getArrangement();
                                }




                            }
                            if ($room != null && $arrang != null) {
                                foreach ($hotelroom as $khr => $vhr) {
                                    if ($room->getId() == $vhr->getRoom()->getId()) {
                                        $idform = $vhr->getId();
                                    }
                                }
                                $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                                $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                                $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                                foreach ($value->getPricearr() as $kk => $vv) {
                                    if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                        $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                        $ArrFinal[$i]["allprice"][$k]["minstay"] = $vv->getMinstay();
                                        $ArrFinal[$i]["allprice"][$k]["etat"] = $vv->getEtat();
                                    }
                                }

                                //Disponiblité
                                $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                                $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($value->getId(),$room->getId());

                                $valres = $value->getRetro()-1;
                                $now =new \DateTime();
                                $res =$now->modify('+1 day');
                                $dres = $res->modify('+'.$valres. 'day');
                                $nows =new \DateTime();
                                $ress =$nows->modify('+1 day');

                                if($nbjour>=$ArrFinal[$i]["allprice"][$k]["minstay"] && $disroom[0]->getQte() > 0 && $dres->format("Ymd") <= $ress->format("Ymd") )
                                {
                                    $ArrFinal[$i]["allprice"][$k]["dispo"]="1";

                                }
                                else{
                                    $ArrFinal[$i]["allprice"][$k]["dispo"]="0";

                                }

                                //fin

                                $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                                $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                                $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;

                                //$calc=new Calculate();
                                $dtest = null;
                                $dtest = $d1;
                                // initialiser l'age des enfant a 6 s'il existe
                                // il faut les changer s'il y a un autre age d'enfant
                                $tabenf = array();
                                for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                                    $tabenf[] = $value->getHotel()->getMaxenfant();
                                }
                                $valp = $ArrFinal[$i]['valp'];
                                $vals =   $ArrFinal[$i]['persp'] ;

                                $prix = $this->getDoctrine()
                                    ->getRepository('BtobHotelBundle:Hotelprice')
                                    ->calcultate($valp,$vals,$user, $dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne);

                                $prix = $prix; // calcule de marge

                                $ArrFinal[$i]["allprice"][$k]["price"] = $prix;

                                if ($prix == 0) {
                                    $surdemande = true;
                                }
                                ++$k;
                            }
                        }
                        // Stop Sales
                        $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                        $ArrFinal[$i]["stopsales"] = count($stopsales);
                        if ($surdemande) {
                            $ArrFinal[$i]["stopsales"] = 1;
                        }
                        //Tools::dump($request, true);
                        ++$i;
                    }
                }

            }
        }

}

        
    
        
        $d1 = new \DateTime(Tools::explodedate($dated, '/'));
        $d2 = new \DateTime(Tools::explodedate($datef, '/'));
          if(!is_null($user)){
        $userid = $user->getId();

        $hotelmarges= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);
          }
        // dump($Sejours2);exit;

        $now = new \DateTime();

        $listhotelprice = $this->getDoctrine()->getRepository("BtobHotelBundle:hotelprice")->Testperiode();




        $listhotelimg = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelimg")->findAll();


       

     


       
		   $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findby(
            array('price' => 0, 'etat' => 1  )
        );
		
          $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->listPrice();
          $themes = $this->getDoctrine()->getRepository("BtobHotelBundle:Themes")->findActiv();
          $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();
        return $this->render('FrontBtobBundle:Default:index.html.twig', array(
            'coup' => $coup,
            'evenementprices' => $evenementprices,
	    'promotion' => $promotion,
            'hotelprice' => $hotelprice,
	    'pricearrs' => $pricearrs,
	    'Arrfinal' => $ArrFinal,
            'page' => $page,
            'sejours'=>$Sejours,
            'sejours2'=>$Sejours2,
			'circuitprices'=>$circuitprices,
	    'star' => $star,
	     'dated' => $dated,
            'datef' => $datef,
	    'price_min' => $price_min,
            'price_max' => $price_max,
            'listhotelimg' =>$listhotelimg,
            'ville' => $ville,
            'villes' => $villes,
            'user' => $user,
            'listhotelprice' =>$listhotelprice,
            
            'themes' => $themes,             			

            'pays' => $pays,

            'paysid' => $paysid,

            'nom' => $nom,

            'page' => $page,


            'arr' => $arrangement,


            'nbjour' => $nbjour,

            'frmad' => $ad,

            'frmenf' => $enf,


            'frmarr' => $arr,

            'd1' => $d1,

            'd2' => $d2,

            'info' => $info,

        ));
    }

    public function bannerAction()
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findby(array('act' => 1));
        return $this->render('FrontBtobBundle:Default:banner.html.twig', array('banner' => $banner,'info'=>$info));


    }
	 public function promoindexAction()
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $promo = $em->getRepository('BtobActiviteBundle:Activite')->findby(array('id' => '10'));
		//var_dump($promo);exit;
        return $this->render('FrontBtobBundle:Default:promoindex.html.twig', array('promo' => $promo,'info'=>$info));


    }
	
	
	public function sendNewsletterAction(){
        
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $alertexist = false;
		//dump("test");exit;
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if ($request->getMethod() == 'GET') {
			$emailnews = $request->query->get('email_news');
			
			//dump("result");
			//dump($formPost);exit;
            $oneemail = null;
            if ($emailnews !=null){
                $oneemail = $this->getDoctrine()->getRepository("BtobHotelBundle:Email")->findby(array('email_adress'=>$emailnews));
            }
            
			// to set the date in french
			
			/* Configure le script en français */
			setlocale (LC_TIME, 'fr_FR','fra');
			//Définit le décalage horaire par défaut de toutes les fonctions date/heure  
			date_default_timezone_set("Europe/Paris");
			//Definit l'encodage interne
			mb_internal_encoding("UTF-8");
			//Convertir une date US vers une date en français affichant le jour de la semaine
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
			$dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
			
            if ($oneemail == null){
                            
                if (! empty($emailnews)) {
                    $email = new Email();
                    $email->setEmail_adress($emailnews);
                    $em->persist($email);
                    $em->flush();
					 $to      = $emailnews;
					
					# -=-=-=- MIME BOUNDARY
					$mime_boundary = "----MSA Shipping----" . md5(time());
					# -=-=-=- MAIL HEADERS

					$subject = "Newsletter Afritours : Merci de votre inscription... " ;
					$headers = "From: '".$info->getNom()."' <'".$info->getEmail()."'> \n";
					$headers .= "Reply-To: '".$info->getNom()."' <'".$info->getEmail()."'> \n";

					$headers .= "MIME-Version: 1.0\n";
					$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";

					$message1 = "--$mime_boundary\n";
					$message1 .= "Content-Type: text/html; charset=UTF-8\n";
					$message1 .= "Content-Transfer-Encoding: 8bit\n\n";

					$message1 .= "<html>\n";
					$message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f7f2e4;" bgcolor="#f7f2e4" leftmargin="0">';

					$message1 .='
					<table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f7f2e4">
  <tr>
    <td>
	<!--top links-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="middle" align="center" height="45">
		<p style="font-size: 14px; line-height: 24px; font-family: Georgia,  Times, serif; color: #b0a08b; margin: 0px;">
	Ce mail est envoyé suite à votre inscription aux Newsletter de : <webversion style="color: #bc1f31; text-decoration: none;" href="#">www.afritours.tn</webversion></p></td>
	</tr>
</table>
   <!--header-->
   <table style="background:url(images/header-bg.jpg); background-repeat: no-repeat; background-position: center; background-color: #fffdf9;" width="684" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top" width="173">
		<!--ribbon-->
		<table border="0" cellspacing="0" cellpadding="0"> 
	<tr>
		<td height="120" width="45"></td>
		<td background="images/ribbon.jpg" valign="top" bgcolor="#c72439" height="120" width="80">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" align="center" height="35"> 
		<p style="font-size: 14px; font-family: Georgia,  Times, serif; color: #ffffff; margin-top: 0px; margin-bottom: 0px;">' .  
		 $daymonthyear . '
		
		</p>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
		<p style="font-size: 36px; font-family: Georgia,  Times, serif; color: #ffffff; margin-top: 0px; margin-bottom: 0px; text-shadow: 1px 1px 1px #333;">' .
		
		 $dayonly . '
		 
		</p>
		</td>
	</tr>
</table>
		</td>
	</tr>
	</table><!--ribbon-->
		</td>
		<td valign="middle" width="493"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="60"></td>
			</tr>
			<tr>
				<td>
				<h1 style=" color: #333; margin-top: 0px; margin-bottom: 0px; font-weight: normal; font-size: 48px; font-family: Georgia,  Times, serif">Afritours <em></em></h1>
				</td>
			</tr>
			
		</table>
		<!--date-->
		<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<!--td valign="top" align="center" border="1" bgcolor="#f7f2e4" background="images/date-bg.jpg" width="357" height="42">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td height="5"></td></tr></table>
        <p style="font-size: 24px; font-family: Georgia,  Times, serif; color: #000; margin-top: 0px; margin-bottom: 0px;">Félicitations, vous étes inscrit avec succés...</p>
		</td-->
	</tr>
	</table><!--/date-->
		</td>
		<td width="18"></td>
	</tr>
</table>
	</td>
    </tr>
</table><!--/header-->
    <!--email container-->
    <table bgcolor="#fffdf9" cellspacing="0" border="0" align="center" cellpadding="30" width="684">
  <tr>
    <td>
    <!--email content-->
    <table cellspacing="0" border="0" id="email-content" cellpadding="0" width="624">
  <tr>
    <td>
    <!--section 1-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td valign="top" align="center">
	
    <img src="'.$img_logo.'" alt="image dsc" style="border: solid 1px #FFF; box-shadow: 2px 2px 6px #333; -webkit-box-shadow: 2px 2px 6px #333; -khtml-box-shadow: 2px 2px 6px #333; -moz-box-shadow: 2px 2px 6px #333;" width="200" />
	<!--line break-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="bottom" height="50"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/line-break.jpg" width="622" height="27"></td>
	</tr>
</table><!--/line break-->
	<h1 style="font-size: 36px; font-weight: normal; color: #333333; font-family: Georgia,  Times, serif; margin-top: 0px; margin-bottom: 20px;">Chèr(e) internaute,<em></em></h1>
    <p style="font-size: 16px; line-height: 22px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;">Vous venez de vous inscrire à la newsletter de <a style="color: #bc1f31; text-decoration: none;" href="#">www.afritours.tn</a> et nous vous en remercions. Ainsi serez-vous informés des nouveautés et des offres que nous les mettons à votre disposition.. Vous recevrez désormais toutes les deux semaines nos actualités. Ce rendez-vous régulier s\'accompagnera de la découverte des détail de chaque offre.</p>
    </td>
  </tr>
</table><!--/section 1-->
<!--line break-->
  <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td height="72"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/line-break-2.jpg" width="622" height="72">
    </td>
  </tr>
</table><!--/line break-->
    <!--section 2-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td>
		<p style="font-size: 16px; line-height: 22px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;">
		Vous souhaitez suivre Afritours de plus près ? Nous vous invitons dès à présent à :
		</p>
    <table cellspacing="0" border="0" cellpadding="8" width="100%" style="margin-top: 10px;">
  <tr style="text-align:center;">
    <td valign="top">
	<a href="#" style="text-decoration: none;">
	<p style="font-size: 17px; line-height: 22px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/fb.png" height="70" alt="img2" style="border: solid 1px #FFF; box-shadow: 2px 2px 6px #333; -webkit-box-shadow: 2px 2px 6px #333; -khtml-box-shadow: 2px 2px 6px #333; -moz-box-shadow: 2px 2px 6px #333;" width="90" /></p>
    <p style="color: #333333; font-size: 18px; font-family: Georgia, Times, serif; margin: 12px 0px; font-weight: bold;">Facebook</p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;"></p>
    </a>
	</td>
    
    <td valign="top">
	<a href="#" style="text-decoration: none;">
	<p style="font-size: 17px; line-height: 22px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/tw.png" height="70" alt="img3" style="border: solid 1px #FFF; box-shadow: 2px 2px 6px #333; -webkit-box-shadow: 2px 2px 6px #333; -khtml-box-shadow: 2px 2px 6px #333; -moz-box-shadow: 2px 2px 6px #333;" width="90" /></p>
     <p style="font-size: 18px; font-family: Georgia,  Times, serif; color: #333333; margin: 12px 0px; font-weight: bold;">Twitter</p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia, Times, serif; color: #333; margin: 0px;"></p>
    </a>
	</td>
    
    <td valign="top">
	<a href="#" style="text-decoration: none;">
	<p style="font-size: 17px; line-height: 22px; font-family: Georgia, Times, serif; color: #333; margin: 0px;"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/sk.png" height="70" alt="img4" style="border: solid 1px #FFF; box-shadow: 2px 2px 6px #333; -webkit-box-shadow: 2px 2px 6px #333; -khtml-box-shadow: 2px 2px 6px #333; -moz-box-shadow: 2px 2px 6px #333;" width="90" /></p>
     <p style="font-size: 18px; font-family: Georgia, Times, serif; color: #333333; margin: 12px 0px; font-weight: bold;">Skype</p>
    <p style="font-size: 16px; line-height: 24px; font-family: Georgia,  Times, serif; color: #333; margin: 0px;"></p>
	</a>
    </td>
    
    
  </tr>
</table>
    </td>
  </tr>
</table><!--/section 2-->
    <!--section 3-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td>


<!--line break-->
  <table cellspacing="0" border="0" cellpadding="0" width="100%">
  <tr>
    <td height="72"><img src="http://www.tuninfo.tn/afritours2s/web/front/images/line-break-2.jpg" width="622" height="72">
    </td>
  </tr>
</table><!--/line break-->
    </td>
  </tr>
</table><!--/section 3-->
    </td>
  </tr>
</table><!--/email content-->
    </td>
  </tr>
</table><!--/email container-->
<!--footer-->
    <table width="680" border="0" align="center" cellpadding="30" cellspacing="0">
  <tr>
    <td valign="top">
    <p style="font-size: 14px; line-height: 24px; font-family: Georgia, Times, serif; color: #b0a08b; margin: 0px;">
	Vous recevez ce message car vous êtes abonné à notre lettre d\'information sur l\’adresse suivante :  <unsubscribe style="color: #bc1f31; text-decoration: none;" href="#">hamza.tlijani@gmail.com</unsubscribe></p>
    </td>
    <td valign="top"><p style="font-size: 14px; line-height: 24px; font-family: Georgia,  Times, serif; color: #b0a08b; margin: 0px;">61, Avenue Jean Jaurès 1000 Tunis Tunisie
	</td>
  </tr>
  <tr>
  	<td height="30"></td>
  	<td height="30"></td>
  	</tr>
	</table><!--/footer-->
    </td>
  </tr>
  

  </body>';

# -=-=-=- FINAL BOUNDARY
					
					
					
					
					mail($to, $subject, $message1, $headers);
					 
                } 
            }else {
                $alertexist = true;

            }
        }
        return $this->redirect($this->generateUrl('front_btob_homepage'));
    }

}