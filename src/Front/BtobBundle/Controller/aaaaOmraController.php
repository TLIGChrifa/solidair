<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;use Btob\OmraBundle\Entity\Omra;use Btob\OmraBundle\Entity\Reservationomra;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\CuircuitBundle\Entity\Resacircui;use Btob\BienetreBundle\Entity\Reservationbienetre;
use Btob\BienetreBundle\Entity\Bienetre;
use Btob\OmraBundle\Entity\Tomra;
use Symfony\Component\HttpFoundation\Request;

class OmraController extends Controller
{

    public function indexAction()
    {
        $ommras = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $ommras,
            $request->query->get('page', 1)/*page number*/,
            8/*limit per page*/
        );
        
        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Omra:index.html.twig', array(
            'entities' => $entities,
            'banner' => $banner,

        ));
    }
    // omra

    public function detailAction(Omra $omra)
    {
        $ommras = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();
        return $this->render('FrontBtobBundle:Omra:detail.html.twig', array('entry' => $omra,
            'ommras' => $ommras));
    }
    public function reservationAction(Omra $omra)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            
            
                         $namead = $request->request->get('namea');
			 $prenomad = $request->request->get('prenomaa');
			 $agead = $request->request->get('agea');
                         
                         
                         
                          $nameenf = $request->request->get('namee');
			 $prenomenf = $request->request->get('prenome');
			 $ageenf = $request->request->get('agee');
            
            
                          $nameb = $request->request->get('nameb');
			 $prenomb = $request->request->get('prenomb');
			 $ageb = $request->request->get('ageb');
                         
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $resa=new Reservationomra();

                $date = Tools::explodedate($request->request->get("dated"),'/');
                $resa->setDated(new \DateTime($date));
                $resa->setAgent($User);
                $resa->setClient($client);
                $resa->setEtat(1);
             
                $resa->setComment($request->request->get("body"));
                $resa->setOmra($omra);
                $em->persist($resa);
                $em->flush();

                  foreach( $namead as $key=>$value){
                    $catg = new Tomra();
                    $catg->setReservationomra($resa);
                    $catg->setNamead($namead[$key]);
                    $catg->setPrenomad($prenomad[$key]);
                    $catg->setAgeadult(new \Datetime(Tools::explodedate($agead[$key],'-')));
                 
                    $em->persist($catg);
                    $em->flush();

                }

                
                 foreach( $nameenf as $keys=>$values){
                    $catge = new Tomra();
                    $catge->setReservationomra($resa);
                    $catge->setNamee($nameenf[$keys]);
                    $catge->setPrenome($prenomenf[$keys]);
					 if($ageenf[$keys]!='')
                   {
                    $catge->setAgeenfant(new \Datetime(Tools::explodedate($ageenf[$keys],'-')));
                    }
                    $em->persist($catge);
                    $em->flush();

                }
                
                
                   foreach( $nameb as $keyb=>$valueb){
                    $catb = new Tomra();
                    $catb->setReservationomra($resa);
                    $catb->setNameb($nameb[$keyb]);
                    $catb->setPrenomb($prenomb[$keyb]);
					 if($ageb[$keyb]!='')
                   {
                    $catb->setAgeb(new \Datetime(Tools::explodedate($ageb[$keyb],'-')));
                   }
                    $em->persist($catb);
                    $em->flush();

                }
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $resa->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "Afritours: Réservation Omra" ;
                $headers = "From:Afritours info@afritours.com.tn\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
				 <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                //mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Omra " ;
                $header = "From:Afritours <".$to.">\n";
                $header .= "Reply-To:" .$resa->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                          <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation Omra  , merci de consulter votre backoffice .
                 <a href="http://www.afritours.com.tn/b2b/reservationomra/'.$resa->getId().'/detail"> Cliquer Içi</a>';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';



                mail($too, $subjects, $message2, $header);
                //mail("afef.tuninfo@gmail.com", $subjects, $message2, $header);
				
                $request->getSession()->getFlashBag()->add('notifomrafront', 'Votre demande a été bien envoyée.');
                return $this->redirect($this->generateUrl('front_btob_omra_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('FrontBtobBundle:Omra:reservation.html.twig', array('form' => $form->createView(),'entry'=>$omra));
    }



    public function validationAction(){
        return $this->render('BtobOmraBundle:Default:validation.html.twig', array());
    }


}
