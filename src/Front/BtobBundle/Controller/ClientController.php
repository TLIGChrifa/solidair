<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientController extends Controller
{

    public function ajxgetclientAction()
    {
        $request = $this->get('request');
        $cin = $request->request->get('cin');
        $client = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findOneBy(array('cin' => $cin));
        if ($client != null) {
            $tab["civ"] = $client->getCiv();
            $tab["pname"] = $client->getPname();
            $tab["name"] = $client->getName();
            $tab["email"] = $client->getEmail();
            $tab["tel"] = $client->getTel();
            $tab["adresse"] = $client->getAdresse();
            $tab["cp"] = $client->getCp();
            $tab["ville"] = $client->getVille();
            $tab["pays"] = $client->getPays()->getId();
			$tab["fid"] = $client->getFid();
            $tab["point"] = $client->getPoint();
        } else {
            $tab["civ"] = "";
            $tab["pname"] = "";
            $tab["name"] = "";
            $tab["email"] = "";
            $tab["tel"] = "";
            $tab["adresse"] = "";
            $tab["cp"] = "";
            $tab["ville"] = "";
            $tab["pays"] = 1;
			$tab["fid"] = "";
            $tab["point"] = "";
        }

//Tools::dump($request->request->get('cin'),true);
        return new JsonResponse($tab);
    }

}
