<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DescOmraController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobOmraBundle:DescOmra')->firstOmra();
        ;

        return $this->render('FrontBtobBundle:DescOmra:detail.html.twig', array(
            'entities' => $entities,
        ));
    }
    // omra


}
