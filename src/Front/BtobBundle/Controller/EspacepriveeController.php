<?php

namespace Front\BtobBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use Btob\SejourBundle\Entity\Tpsejour;
use Btob\CuircuitBundle\Entity\Tpcircuit;
use Btob\SejourBundle\Entity\Personalisationsejour;
use Btob\CuircuitBundle\Entity\Personalisationcircuit;
use Btob\VoleBundle\Entity\Vol;
use Btob\VoleBundle\Entity\Dvoles;
use Btob\VoleBundle\Entity\Dbvoles;
use Btob\VoleBundle\Entity\Reservationbateau;
use User\UserBundle\Entity\Solde;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\HotelBundle\Entity\Reservation;
use Btob\OmraBundle\Entity\Reservationomra;
use Btob\SejourBundle\Entity\Reservationsejour;
use Btob\HotelBundle\Entity\Reservationhotel;
use Btob\AgenceBundle\Entity\Info;
use Symfony\Component\Validator\Constraints\DateTime;

use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
class EspacepriveeController extends Controller
{
    public function loginAction()
    {
        $request = $this->get('request');
        if($request->request->get('emailpass') !=null){
        }else{
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $email = $request->request->get('email');
        $password =$request->request->get('password');
        $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
        if(!empty($client)){
        $options = ['cost' => 12, 'salt' => $client->getSalt()];
        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
            if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                $session->set('client', $client->getEmail());
                $fullname=$client->getPname();
                $session->set('fullname', $fullname);
                return $this->redirect($this->generateUrl('front_espace_privee_index_homepage'));
            }else{
                $request->getSession()->getFlashBag()->add('notiautherror', 'Adresse e-mail ou mot de passe incorrect.');
                return $this->redirect($this->generateUrl('front_btob_homepage'));
            }
        }else{
                $request->getSession()->getFlashBag()->add('notiautherror', 'Adresse e-mail ou mot de passe incorrect.');
                return $this->redirect($this->generateUrl('front_btob_homepage'));

        }
        }
    }
    public function logoutAction()
    {   $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $session->set('client', '');
        $session->clear();
        $session->invalidate();
        return $this->redirect($this->generateUrl('front_btob_homepage'));

    }
    public function indexAction()
    {

        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $session = $this->getRequest()->getSession();
      //  $client=new Clients();
        if($session->get('client') != null){
        $email=$session->get('client');
     //   var_dump($email);
        $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findByEmail($email);
        $circuit=$this->getDoctrine()->getRepository('BtobCuircuitBundle:Reservationcircuit')->findByClient($client[0]);
        $sejour=$this->getDoctrine()->getRepository('BtobSejourBundle:Reservationsejour')->findByClient($client[0]);
        $persocircuit=$this->getDoctrine()->getRepository('BtobCuircuitBundle:Personalisationcircuit')->findByClient($client[0]);
        $circuitpersonalise=$this->getDoctrine()->getRepository('BtobCuircuitBundle:Tpcircuit')->findByPersonalisationcircuit($persocircuit);
        $persosejour=$this->getDoctrine()->getRepository('BtobSejourBundle:Personalisationsejour')->findByClient($client[0]);
        $sejourpersonalise=$this->getDoctrine()->getRepository('BtobSejourBundle:Tpsejour')->findByPersonalisationsejour($persosejour);
        $hotel=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->findByClient($client[0]);
        $omra=$this->getDoctrine()->getRepository('BtobOmraBundle:Reservationomra')->findByClient($client[0]);
        $vol=$this->getDoctrine()->getRepository('BtobVoleBundle:Vol')->findByClient($client[0]);
        $dvol=$this->getDoctrine()->getRepository('BtobVoleBundle:Dvoles')->findByVold($vol);
        $bdvol=$this->getDoctrine()->getRepository('BtobVoleBundle:Reservationbateau')->findByClient($client[0]);
        $bateau=$this->getDoctrine()->getRepository('BtobVoleBundle:Dbvoles')->findByVolbd($bdvol);
        $facture=$this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findByClientId($client[0]);
$countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();
        $form = $this->createForm(new ClientsType(), $client[0]);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                $request = $this->get('request');
                $request->getSession()->getFlashBag()->add('notiomra', 'Votre demande a été bien envoyée.');
                $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randStringLen = 64;
                $salt = "";
                for ($i = 0; $i < $randStringLen; $i++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $password =$request->request->get('btob_hotelbundle_clients')['password'];
                $client[0]->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client[0]->setSalt($salt);
                $client[0]->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                $client[0]->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                $client[0]->setName($request->request->get('btob_hotelbundle_clients')['name']);
                $client[0]->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
                $client[0]->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                $client[0]->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                $client[0]->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                $client[0]->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                $client[0]->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                $client[0]->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                $client[0]->setPays($pays);
                $client[0]->setType(0);
                $client[0]->setAct(1);
                $em->flush();
                $session->set('client', $email);
                $fullname=$client[0]->getPname();
                $session->set('fullname', $fullname);
            $request->getSession()->getFlashBag()->add('notiprofil', 'Merci de modifier votre profil.');
            return $this->redirect($this->generateUrl('front_espace_privee_index_homepage'));
            } else {
            $request->getSession()->getFlashBag()->add('notiprofilerror', ' incorrect.');
                echo $form->getErrors();
            }
        }
                $session->set('client', $session->get('client'));
        return $this->render('FrontBtobBundle:Espaceprivee:index.html.twig', array(
            'circuit' => $circuit,
            'circuitpersonalise' => $circuitpersonalise,
            'sejourpersonalise' => $sejourpersonalise,
            'client' => $client[0],
            'sejour' => $sejour,
            'hotel' => $hotel,
            'omra' => $omra,
            'info' =>$info,
            'dvol' => $dvol,
            'bateau' => $bateau,
            'facture' => $facture,
            'form' => $form->createView(),
            'countries'=>$countries
            ));
        }else{
        $session->invalidate();
        return $this->redirect($this->generateUrl('front_btob_homepage'));
        }
    }
    
    public function resetpassAction()
    { 
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $request = $this->get('request');
    $name = $request->request->get('emailpasso');
                var_dump("azertyuiopqsdfghjklmwxcvbn");
   //     var_dump($name);
        exit;
                $email = $request->request->get('emailpasso');
                $em = $this->getDoctrine()->getManager();
        
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                
                var_dump($email);
                die();
                    $client= new Clients();
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
//exit(var_dump($salt));
                $em->flush();
                $to = $email;
               

                $nameagence="Explore voyage";
                $mailagence="contact@explorevoyage.com";
                $adresseagence="61, Avenue Jean Jaurès 1000 Tunis Tunisie";
                $urlagence="http://www.afritours.com.tn";

                $subject = $nameagence.": Re-initialisation mot de passe";
                $headers = "From: ".$nameagence."\n";
                $message1 ='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est :<br>'.$password;
             
                mail($to, $subject, $message1, $headers);
  // var_dump($message1);
                var_dump("success");
    }
    public function resetpasswordAction()//Request $request)
    { 
        
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $request = $this->get('request');
                $email = $request->get('pass');
                var_dump("azertyuiopqsdfghjklmwxcvbn");
                   exit(var_dump($email));
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client !=null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
//exit(var_dump($salt));
                $em->flush();
                $to = $request->request->get('emailpasso');
				
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = "Explore Voyage: Changement mot de passe";
                $headers = "From: Explore Voyage<contact@explorevoyage.com>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="https://www.digitravel-solution.com/Version_B2B_afef/public_html/front/images/logo.png" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#fcdb0d" style="color:#183961;">
                        Résidence LATINA, rue des Lacs de MAZURIE, appartement n°B1, Les Berges de Lac - <a style="color:#183961;">+216 71 862 263</a>
				</td>';               
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
			
                mail($to, $subject, $message1, $headers);
                $request->getSession()->getFlashBag()->add('notimailbase', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailbaseerror', 'Votre email n\'existe pas.');
                }
    }
    public function registerAction()
    { 
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
         
            $form->bind($request);
           
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                $request = $this->get('request');
                $request->getSession()->getFlashBag()->add('notiomra', 'Votre demande a été bien envoyée.');
                $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randStringLen = 64;
                $salt = "";
                for ($i = 0; $i < $randStringLen; $i++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client= new Clients();
                $password =$request->request->get('btob_hotelbundle_clients')['password'];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
                $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
                $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                $client->setPays($pays);
                $client->setType(0);
                $client->setAct(1);
                $em->persist($client);
                $em->flush();
                $session->set('client', $client->getEmail());
                $fullname=$client->getPname();
                $session->set('fullname', $fullname);
            return $this->redirect($this->generateUrl('front_espace_privee_index_homepage'));
            } else {
                echo $form->getErrors();
            }
        }

        return $this->render('FrontBtobBundle:Espaceprivee:register.html.twig', array(
            'form' => $form->createView(),
            'countries'=>$countries,'info'=>$info
            ));
    }
    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction($type,$id)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
     //   exit(var_dump($session->get('client')[0]));
        $email=$session->get('client');
        $clients=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findByEmail($email);
        if($type=='circuit')
        $entry=$this->getDoctrine()->getRepository('BtobCuircuitBundle:Reservationcircuit')->find($id);
        if($type=='sejour')
        $entry=$this->getDoctrine()->getRepository('BtobSejourBundle:Reservationsejour')->find($id);
        if($type=='omra')
        $entry=$this->getDoctrine()->getRepository('BtobOmraBundle:Reservationomra')->find($id);
     //   exit(var_dump($omra));
        if($type=='hotel'){
        $entry=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($id);
        
        $namead = json_decode($entry->getNamead(), true);
        $agead = json_decode($entry->getAgeadult(), true);
        $nameenf = json_decode($entry->getNameenf(), true);
        $ageenfant = json_decode($entry->getAgeenfant(), true);
        $frais = $this->getDoctrine()->getRepository('BtobHotelBundle:Frais')->fraisDossier();
        }
                $session->set('client', $session->get('client'));
                
        if($type!='hotel'){
        return $this->render('FrontBtobBundle:Espaceprivee:detail.html.twig', array('entry' => $entry,'type'=>$type));
        }else{
        return $this->render('FrontBtobBundle:Espaceprivee:detail.html.twig', array(
            'entry' => $entry,
            'type'=>$type,
            'namead' => $namead,
            'frais' => $frais,
            'agead' => $agead,
            'nameenf' => $nameenf,
            'ageenfant' => $ageenfant,
            'info'=>$info));
        }
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailvolAction($type,$id)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $email=$session->get('client');
        if($type=='bateau'){
            $vol=$this->getDoctrine()->getRepository('BtobVoleBundle:Reservationbateau')->find($id);
            $entry=$this->getDoctrine()->getRepository('BtobVoleBundle:Dbvoles')->findByVolbd($vol);
            $tvol=$this->getDoctrine()->getRepository('BtobVoleBundle:Bvoles')->findByReservationbateau($vol);
         //   $entry=$this->getDoctrine()->getRepository('BtobVoleBundle:Dbvoles')->find($id);
        }
        if($type=='vol'){
            $vol=$this->getDoctrine()->getRepository('BtobVoleBundle:Vol')->find($id);
            $entry=$this->getDoctrine()->getRepository('BtobVoleBundle:Dvoles')->findByVold($vol);
            $tvol=$this->getDoctrine()->getRepository('BtobVoleBundle:Tvoles')->findByVol($vol);
        }
        $session->set('client', $session->get('client'));
        return $this->render('FrontBtobBundle:Espaceprivee:detailvol.html.twig', array('entity' => $entry,'tvol' => $tvol,'vol' => $vol,'type'=>$type,'info'=>$info));
    }    
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailpersonaliseAction($type,$id)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
     //   exit(var_dump($session->get('client')[0]));
        $email=$session->get('client');
     //   var_dump($email);
        $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findByEmail($email);
        if($type=='sejourpersonalise')
        $entry=$this->getDoctrine()->getRepository('BtobSejourBundle:Tpsejour')->find($id);
   /*     {
        $persosejour=$this->getDoctrine()->getRepository('BtobSejourBundle:Personalisationsejour')->findByClient($client[0]);
         $entry = $this->getDoctrine()
                            ->getRepository(Tpsejour::class)
                            ->createQueryBuilder('s')
                            
                            ->leftjoin('s.personalisationsejour','ps')
                            ->andWhere('ps.id = :ps ')
                            ->setParameter('ps',$persosejour )
                            ->orderBy('s.id', 'DESC')
                            ->groupBy('s.personalisationsejour')
                            ->getQuery()
                            ->getResult();
            var_dump($entry);
        }*/
        if($type=='circuitpersonalise')
        $entry=$this->getDoctrine()->getRepository('BtobCuircuitBundle:Tpcircuit')->find($id);
     //   exit(var_dump($omra));
                $session->set('client', $session->get('client'));
        return $this->render('FrontBtobBundle:Espaceprivee:detailpersonalise.html.twig', array('entry' => $entry,'type'=>$type,'info'=>$info));
    }

    public function factureAction($id)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $facture = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Facture')
            ->find($id);
        $fact = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Fact')
            ->find(1);
        $tva = $em->createQueryBuilder()
             ->select('t.name')
             ->from('BtobHotelBundle:Tva','t')
             ->orderBy('t.name', 'ASC')
             ->getQuery()
             ->getResult();
        $counttva = $em->createQueryBuilder()
             ->select('count(t.name)')
             ->from('BtobHotelBundle:Tva','t')
             ->where('t.act = :act')
             ->setParameter('act', 1)
             ->getQuery()
             ->getSingleScalarResult();
             $s=array();
        foreach($tva as $sum['name']){
        $somme = $em->createQueryBuilder()
            ->select('SUM(t.quantite * t.prixUnitaire)')  //(t.quantite*t.prixUnitaire)-((t.quantite*t.prixUnitaire)/100)
             ->from('BtobHotelBundle:Tfacture','t')
             ->where('t.facture = :fact')
             ->setParameter('fact', $id)
             ->andWhere('t.tva = :tva')
             ->setParameter('tva', $sum['name'])
             ->groupBy('t.tva')
             ->orderBy('t.tva', 'ASC')
             ->getQuery()
             ->getResult();
             
             if($somme != null)
            $s[]=array($somme,);
            else 
            $s[]=array(0,);
        }
       //      exit(var_dump($s));
        $sum=array();
        for($i=0;$i<$counttva;$i++)
             if($s[$i][0] != 0)     $sum[]=$s[$i][0][0][1];
             else               $sum[]=0;
    //         exit(var_dump($sum));
        $tfacture = $this->getDoctrine()->getRepository('BtobHotelBundle:Tfacture')->findBy(array('facture' => $facture));
      //  exit(var_dump($facture->getMontant()*1000));
     //   $numtochar= $this->asLetters(2.500);
        $numtochar= $this->asLetters($facture->getMontant());
 //   exit(var_dump($tva));
    //    exit(var_dump($numtochar));
        $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('Facture N째 ' . $facture->getNum() . '/' . $facture->getDcr()->format("Y"));
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->AddPage();
        $pagefooter = "";
        $html = $this->renderView('FrontBtobBundle:Espaceprivee:pdf.html.twig', array(
            'facture' => $facture,
            'tfacture' => $tfacture,
            'fact' => $fact,
            'tva' => $tva,
            'sum' => $sum,
            'montantenlettre' => $numtochar,
            'pagefooter' => $pagefooter,
            'info'=>$info
        ));
        $pdf->writeHTML($html);
        $nompdf = 'facture_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
    
   public function asLetters($number) {
$convert = explode('.', $number);
$num[17] = array('zero', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit',
'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize');

$num[100] = array(20 => 'vingt', 30 => 'trente', 40 => 'quarante', 50 => 'cinquante',
60 => 'soixante', 70 => 'soixante-dix', 80 => 'quatre-vingt', 90 => 'quatre-vingt-dix');

if (isset($convert[1]) && $convert[1] != '') {
    if($convert[0]==0){
        if($convert[1]<10){
            return self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[1]).' millimes';
        }
    }else{
        if($convert[1]<10){
            return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]).' millimes';
        }
    }
}
if ($number < 0) return 'moins '.self::asLetters(-$number);
if ($number < 17) {
return $num[17][$number];
}
elseif ($number < 20) {
return 'dix-'.self::asLetters($number-10);
}
elseif ($number < 100) {
if ($number%10 == 0) {
return $num[100][$number];
}
elseif (substr($number, -1) == 1) {
if( ((int)($number/10)*10)<70 ){
return self::asLetters((int)($number/10)*10).'-et-un';
}
elseif ($number == 71) {
return 'soixante-et-onze';
}
elseif ($number == 81) {
return 'quatre-vingt-un';
}
elseif ($number == 91) {
return 'quatre-vingt-onze';
}
}
elseif ($number < 70) {
return self::asLetters($number-$number%10).'-'.self::asLetters($number%10);
}
elseif ($number < 80) {
return self::asLetters(60).'-'.self::asLetters($number%20);
}
else {
return self::asLetters(80).'-'.self::asLetters($number%20);
}
}
elseif ($number == 100) {
return 'cent';
}
elseif ($number < 200) {
return self::asLetters(100).' '.self::asLetters($number%100);
}
elseif ($number < 1000) {
return self::asLetters((int)($number/100)).' '.self::asLetters(100).($number%100 > 0 ? ' '.self::asLetters($number%100): '');
}
elseif ($number == 1000){
return 'mille';
}
elseif ($number < 2000) {
return self::asLetters(1000).' '.self::asLetters($number%1000).' ';
}
elseif ($number < 1000000) {
return self::asLetters((int)($number/1000)).' '.self::asLetters(1000).($number%1000 > 0 ? ' '.self::asLetters($number%1000): '');
}
elseif ($number == 1000000) {
return 'millions';
}
elseif ($number < 2000000) {
return 'un '.self::asLetters(1000000).' '.self::asLetters($number%1000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000)).' '.self::asLetters(1000000).($number%1000000 > 0 ? ' '.self::asLetters($number%1000000): '');
}
elseif ($number == 1000000000) {
return 'milliard';
}
elseif ($number < 2000000000) {
return 'un '.self::asLetters(1000000000).' '.self::asLetters($number%1000000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000000)).' '.self::asLetters(1000000000).($number%1000000000 > 0 ? ' '.self::asLetters($number%1000000000): '');
}
}
 public function vouchercircuitAction(Reservationcircuit $resacircui)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
            $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
		$em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
		$session->set('client', $session->get('client'));
	/* 		$user = $this->get('security.context')->getToken()->getUser();
                           $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();*/


        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('FrontBtobBundle:Espaceprivee:vouchercircuit.html.twig', array('entry' => $resacircui,'info'=>$info));
//exit(var_dump($html));
        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
 public function voucheromraAction(Reservationomra $resaomra)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
            $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
		$em = $this->getDoctrine()->getManager();
		        
        $session = $this->getRequest()->getSession();
		$session->set('client', $session->get('client'));
		 /*	$user = $this->get('security.context')->getToken()->getUser();
                           $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();*/

        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('FrontBtobBundle:Espaceprivee:voucheromra.html.twig', array('entry' => $resaomra,'info'=>$info));

        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
 public function vouchersejourAction(Reservationsejour $resasejour)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
            $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
		$em = $this->getDoctrine()->getManager();
		        
        $session = $this->getRequest()->getSession();
		$session->set('client', $session->get('client'));
	  /*		$user = $this->get('security.context')->getToken()->getUser();
                          $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();*/

        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('FrontBtobBundle:Espaceprivee:vouchersejour.html.twig', array('entry' => $resasejour,'info'=>$info));

        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
  public function voucherhotelAction(Reservation $reservation)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
            $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
        $em = $this->getDoctrine()->getManager();

        $agead = json_decode($reservation->getAgeadult(), true);
        // fin commentaire
        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
        $namead = json_decode($reservation->getNamead(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
        $ageenfant = json_decode($reservation->getAgeenfant(), true);
        $pagefooter = "";
        
		$dataimg = $reservation->getHotel()->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
					
        // echo $recaphtml;exit;
        //echo $reservation->getRecap();exit;
        $html = $this->renderView('FrontBtobBundle:Espaceprivee:voucherhotel.html.twig', array(
            'reservation' => $reservation,
            'agead' => $agead,
            'namead' => $namead,
            'nameenf' => $nameenf,
            'pagefooter' => $pagefooter,
            'ageenfant' => $ageenfant,
            'img' => $img,
			'info'=>$info
        ));
		$pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
		$pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
}

}
