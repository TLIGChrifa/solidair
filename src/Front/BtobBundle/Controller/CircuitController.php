<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\CuircuitBundle\Entity\Cuircuitprice;

use Btob\HotelBundle\Common\Tools;

use Btob\OmraBundle\Entity\Omra;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use User\UserBundle\Entity\User;

use Btob\AgenceBundle\Entity\Info;
use Btob\BienetreBundle\Entity\Reservationbienetre;

use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\CuircuitBundle\Entity\Tpcircuit;

use Btob\CuircuitBundle\Entity\Reservationcircuit;

use Btob\CuircuitBundle\Entity\Reservationcdetail;
use Btob\CuircuitBundle\Entity\Personalisationcircuit;
use Btob\BienetreBundle\Entity\Bienetre;
use Symfony\Component\HttpFoundation\Request;



class CircuitController extends Controller

{



    //  circuit !!!

    public function circuitAction(Request $request)

    {
       
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        //$circuits = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby(array('active'=>1));
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();
      
        $entity = $circuitprices;
       
       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $circuitprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        
      
        return $this->render('FrontBtobBundle:Circuit:index.html.twig', array(

            'entities' => $entities,
            'entity' => $entity,
            'info' => $info,
            
            'circuitprices' => $circuitprices

  
            ));


    }
    
    
    //  circuit !!!

    public function searchcircuitAction(Request $request)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $libelle=$request->request->get("libelle");
        $destination=$request->request->get("destination");
        $duree=$request->request->get("duree");
   
           $entityManager = $this->getDoctrine()->getManager();
    /*    $cp = $entityManager->createQueryBuilder('a')
            ->select('a')
            ->from(Cuircuitprice::class, 'a')
            ->leftJoin('a.cuircuit', 'u')
            ->andWhere('u.active =:act')
            ->andWhere('u.type =:type')
            ->andwhere('u.libelle = :libelle')
            ->andwhere('u.description LIKE :destination')
            ->andwhere('u.nbrjr = :duree')
            ->setParameter('act', true)
            ->setParameter('type', "Circuit")
            ->setParameter('libelle', $libelle)
            ->setParameter('destination', '%'.$destination.'%')
            ->setParameter('duree', $duree)
            ->groupBy('a.cuircuit')
            ->orderBy('u.dcr', 'DESC');*/
        $cp = $entityManager->createQueryBuilder('a')
            ->select('a')
            ->from(Cuircuitprice::class, 'a')
            ->leftJoin('a.cuircuit', 'u')
            ->andWhere('u.active =:act')
            ->andWhere('u.type =:type')
            ->setParameter('act', true)
            ->setParameter('type', "Circuit")
            ->groupBy('a.cuircuit')
            ->orderBy('u.dcr', 'DESC');
            if($libelle != "all"){
           $cp=$cp ->andwhere('u.libelle = :libelle')
            ->setParameter('libelle', $libelle);
            }
            if($destination != null){
           $cp=$cp ->andwhere('u.description LIKE :destination')
            ->setParameter('destination', '%'.$destination.'%');
            }
            if($duree != 0){
           $cp=$cp->andwhere('u.nbrjr = :duree')
            ->setParameter('duree', $duree);
            }
$circuitprices = $cp->getQuery()
                    ->getResult();
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $circuitprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );

        $entity =  $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();

         //   exit(var_dump($libelle));
        return $this->render('FrontBtobBundle:Circuit:search.html.twig', array(

            'entities' => $entities,
            'entity' => $entity,
            'circuitprices' => $circuitprices,
            'info'=>$info


        ));


    }

    public function excursionAction(Request $request)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
       
        //$circuits = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuit")->findby(array('active'=>1));
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrices();
      
       
       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $circuitprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        
      
        return $this->render('FrontBtobBundle:Circuit:excursion.html.twig', array(

            'entities' => $entities,
            
            'circuitprices' => $circuitprices,
            'info'=>$info

  
            ));


    }




    public function detailAction(Cuircuit $circuit)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
       
        $circuitperiode = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
        $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();
         
        return $this->render('FrontBtobBundle:Circuit:detail.html.twig', array('entry' => $circuit,'periods' => $circuitperiode,
            'circuitprices' => $circuitprices,
            'info'=>$info
          ));

    }

    
     public function personalisationAction(Cuircuit $circuit)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);

       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $circuitprice = $request->request->get("circuitprice");
            $session->set('circuitprice', $circuitprice);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('front_inscrip_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Circuit:personalisation.html.twig', array('entry' => $circuit,'circuitprices' => $circuitprices,
      'info'=>$info));

    
    }
    
    
    
    
    public function inscriptionAction(Cuircuit $circuit)

    {

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       $circuitsupp = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Supplementc")->findSupplementByCircuit($circuit->getId());
       
       $circuitreduc = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Reductionc")->findReductionByCircuit($circuit->getId());

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $circuitprice = $session->get("circuitprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $circuitpricesss = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->find($circuitprice);
		
        $Array = array();
		$aujourdhuib = new \DateTime($circuitpricesss->getDated()->format("Y-m-d"));
		$nbjourbebe = intval($circuit->getAgebmax())*365;
		$aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");
        
        $aujourdhuienf = new \DateTime($circuitpricesss->getDated()->format("Y-m-d"));
		$nbjourenf = intval($circuit->getAgeenfmax())*365;
		$aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
		

        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                 if(isset($request->request->get("reducad")[$i][$j]))
                 {
                     $adad[$j]['reducad']= $request->request->get("reducad")[$i][$j];
                 }
                 else{
                   $adad[$j]['reducad']=null;  
                 }
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                 if(isset($request->request->get("reducenf")[$i][$k]))
                 {
                     $enf[$k]['reducenf']= $request->request->get("reducenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['reducenf']=null;  
                 }
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 if(isset($request->request->get("reducb")[$i][$l]))
                 {
                     $bebe[$l]['reducb']= $request->request->get("reducb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['reducb']=null;  
                 }
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('circuitprice', $circuitprice);
            
          

          return $this->redirect($this->generateUrl('front_resa_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
       
       
       
		$datedbb = $aujourdhuib->format("Y-m-d");
        $datedenff = $aujourdhuienf->format("Y-m-d");
		$datedb_fin=date($datedbb);
		$datedenf_fin=date($datedenff);
		$auj_fin=date("Y-m-d");
		
		return $this->render('FrontBtobBundle:Circuit:inscription.html.twig', array('datedb' => $datedb,'datedb_fin' => $datedb_fin,'datedenf_fin' => $datedenf_fin,'datedenf' => $datedenf,'auj_fin' => $auj_fin,'entry' => $circuit,'circuitsupp' => $circuitsupp,'circuitreduc' => $circuitreduc,'circuitprices' => $circuitprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf,
      'info'=>$info));  
        
         

    
    }
    
    
    
    
    
    
    
    
    public function reservationAction(Cuircuit $circuit)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());

        $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $circuitperiode = $session->get("circuitprice");
        
        
       $pricecirc =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuitprice')->find($circuitperiode);   

       $mode = $this->getDoctrine()->getRepository('BtobHotelBundle:Payement')->find(1); 

        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  $reducaddp =0;
               
                  if(isset($val1['reducad']))
                  {
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                  
                  $totalreducad+= $reducaddp;
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              //reduction
              
              
              $reducenfp=0;
                  if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
              $totalreducenf+= $reducenfp;
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              $reducbp=0;
                  if(isset($valb['reducb']))
                  {
                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
              $totalreducb+= $reducbp;
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         $ArrBase[$key]['price'] = $prices+($prices*$circuit->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $circuitprice = $session->get("circuitprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

             if (($form->isSubmitted() && $form->isValid()) || $request->getSession()->get('client')!=null ) {
                if($request->request->get('emailpassc') != null){
                    $email=$request->request->get('emailpassc');
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client!=null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
                $em->flush();
                $to = $email;
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Changement mot de passe";
                $headers = "From: '".$info->getNom()."' <'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';             
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
				
                mail($to, $subject, $message, $headers);
                $request->getSession()->getFlashBag()->add('notimailcircuit', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailcircuiterror', 'Votre email n\'existe pas.');
                }
       //   return $this->redirect($this->generateUrl('front_omra_reservation_homepage', array('id'=>$omra->getId())));
                }else{
                $session = $this->getRequest()->getSession();
                if($request->request->get('btob_hotelbundle_clients')['email']!=null){
                    
                    $email=$request->request->get('btob_hotelbundle_clients')['email'];
                    $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                     if($client == null){
              //      $request->getSession()->getFlashBag()->add('noticircuit', 'Votre demande a été bien envoyée.');
                    $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                    $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                    $randStringLen = 64;
                    $salt = "";
                    for ($i = 0; $i < $randStringLen; $i++) {
                        $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                    }
                    $options = ['cost' => 12, 'salt' => $salt];
                    $client= new Clients();
                    $password =$request->request->get('btob_hotelbundle_clients')['password'];
              //      exit(var_dump(password_hash($password, PASSWORD_BCRYPT, $options)));
                    $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                    $client->setSalt($salt);
                    $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                    $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                    $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                    $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
             //       $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                    $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                    $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                    $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                    $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                    $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                    $client->setPays($pays);
                    $client->setType(0);
                    $client->setAct(1);
                    $em->persist($client);
                
                    }else{
                         $request->getSession()->getFlashBag()->add('notimailcircuitpersoerror', 'Votre email est deja existé.');
                    }
                }else{    
                    
                if($request->getSession()->get('client') == null){
                    $email = $request->request->get('email');
                }else{
                    $email=$request->getSession()->get('client');
                }
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                    
                    
                    
                    if($request->getSession()->get('client') != null){
                 //       $request->getSession()->getFlashBag()->add('noticircuit', 'Votre demande a été bien envoyée.');
                    }else{
                        $options = ['cost' => 12, 'salt' => $client->getSalt()];
                        $password =$request->request->get('password');
                        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
                        if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                     //       $request->getSession()->getFlashBag()->add('noticircuit', 'Votre demande a été bien envoyée.');
                        }else{
                            $request->getSession()->getFlashBag()->add('noticircuiterror', 'Adresse e-mail ou mot de passe incorrect.');
                        return $this->redirect($this->generateUrl('front_resa_reservation_cuircuit', array('id'=>$circuit->getId())));
                        }
                    }
                }

                $em->flush();
                

                $session = $this->getRequest()->getSession();
            $session->set('client', $client->getEmail());
            $session->set('fullname', $client->getPname());
                
                
                
                
                
                
                
                

                $resa=new Reservationcircuit();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setCuircuit($circuit);
                $resa->setCuircuitprice($pricecirc);
                
                $resa->setTotal($total);
                  
                

                $resa->setAvance($total*($circuit->getPrixavance()));
                $resa->setEtat(1);

                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               $reducaddp =0; 
           
           $resadetail=new Reservationcdetail();
           $resadetail->setReservationcircuit($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  if(isset($val1['reducad']))
                  {
                      
                     

 
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                 
                  $resadetail->setReducad(json_encode($ArrBase[$key]['adult'][$k1]['reducad']));
                  $totalreducad+= $reducaddp;
                  
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               $reducenfp=0;
               
           $resadetails=new Reservationcdetail();
           $resadetails->setReservationcircuit($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
              //reduction
              
              if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
                  
              $resadetails->setReducenf(json_encode($ArrBase[$key]['enf'][$k1e]['reducenf']));
  
              $totalreducenf+= $reducenfp;
              
              
              
           
          
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               $reducbp=0;
               
               $resadetailb=new Reservationcdetail();
           $resadetailb->setReservationcircuit($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              if(isset($valb['reducb']))
                  {

                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setReducb(json_encode($ArrBase[$key]['bebe'][$k1b]['reducb']));
              $totalreducb+= $reducbp;
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         $ArrBase[$key]['price'] = $prices+($prices*$circuit->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $resa->getClient()->getEmail();
               


                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "'".$info->getNom() ."' Réservation Circuit" ;
                $headers = "From:'".$info->getNom() ."'<'".$info->getEmail()."'> \n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
				 <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation Circuit" ;
                $header = "From:'".$info->getNom() ."' <".$to.">\n";
                $header .= "Reply-To:" .$resa->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
               <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
                Bonjour,<br>
				Vous avez reçu une réservation Circuit , merci de consulter votre backoffice .';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';

                mail($too, $subjects, $message2, $header);
                
                $request->getSession()->getFlashBag()->add('notifcirfront', 'Votre demande a été bien envoyée.');
            }

            } else {
                echo $form->getErrors();
            }

                $session->set('reservation', $resa);
                $session->set('ArrBase', $ArrBase);
                $session->set('paiement', $request->request->get('paiement'));

                $my_session_id = session_id();
        

                return $this->render('FrontBtobBundle:Circuit:validation.html.twig', array(
                        'paiement' => $request->request->get('paiement'),
                        'reservation' => $resa,
                        'my_session_id' => $my_session_id,
                        'entry'=>$circuit,
                        'recap'=>$recap,
                        'ArrBase'=>$ArrBase,
                        'total'=>$total,
                        'client' => $resa->getClient(),
                       
                    )
                );


        }

        return $this->render('FrontBtobBundle:Circuit:reservation.html.twig', array(
          'form' => $form->createView(),
          'entry'=>$circuit,
          'recap'=>$recap,
          'ArrBase'=>$ArrBase,
          'total'=>$total,
          'mode' => $mode,
          'info'=>$info
          ));

    }

public function personalizeAction(Cuircuit $circuit )
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
         $em = $this->getDoctrine()->getManager();
         $client = new Clients();
         $form = $this->createForm(new ClientsType(), $client);
         
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
           /* $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }*/
            $form->handleRequest($request);
             
            if (($form->isSubmitted() && $form->isValid()) || $request->getSession()->get('client')!=null ) {
                if($request->request->get('emailpasscp') != null){
                    $email=$request->request->get('emailpasscp');
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client != null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
                $em->flush();
                $to = $email;
                
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Changement mot de passe";
                $headers = "From: '".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';             
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
                mail($to, $subject, $message1, $headers);
				
                $request->getSession()->getFlashBag()->add('notimailcircuitperso', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailcircuitpersoerror', 'Votre email n\'existe pas.');
                }
                }else{
                $session = $this->getRequest()->getSession();
                if($request->request->get('btob_hotelbundle_clients')['email'] != null){
                    
                    $email=$request->request->get('btob_hotelbundle_clients')['email'];
                    $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client == null){
                    $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                    $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                    $randStringLen = 64;
                    $salt = "";
                    for ($i = 0; $i < $randStringLen; $i++) {
                        $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                    }
                    $options = ['cost' => 12, 'salt' => $salt];
                    $client= new Clients();
                    $password =$request->request->get('btob_hotelbundle_clients')['password'];
              //      exit(var_dump(password_hash($password, PASSWORD_BCRYPT, $options)));
                    $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                    $client->setSalt($salt);
                    $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                    $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                    $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                    $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
              //      $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                    $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                    $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                    $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                    $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                    $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                    $client->setPays($pays);
                    $client->setType(0);
                    $client->setAct(1);
                    $em->persist($client);
                    }
                }else{
                    $request->getSession()->getFlashBag()->add('notimailcircuitpersoerror', 'Votre email est deja existé.');
                }
                if($request->getSession()->get('client') == null){
                    $email = $request->request->get('email');
                }else{
                    $email=$request->getSession()->get('client');
                }
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                    
                    if($request->getSession()->get('client') != null){
                        
                    }else{
                        $options = ['cost' => 12, 'salt' => $client->getSalt()];
                        $password =$request->request->get('password');
                        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
                        if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                            
                        }else{
                            $request->getSession()->getFlashBag()->add('noticircuitpersoerror', 'Adresse e-mail ou mot de passe incorrect.');
                            return $this->redirect($this->generateUrl('front_cuircuit_personalize_homepage',array('id'=>$circuit->getId())));
                        }
                    }
                    
                }
                $em->flush();
                $session = $this->getRequest()->getSession();
            $session->set('client', $client->getEmail());
            $session->set('fullname', $client->getPname());
               
                $ville = $request->request->get('ville');
                $dated = $request->request->get('dated');
                
              
                $dater = $request->request->get('dater');
                
               
                $typec = $request->request->get('typec');
                $formule = $request->request->get('formule');
              
                $nbradult = $request->request->get('nbradult');
                $nbrenf = $request->request->get('nbrenf');
                $nbrbebe = $request->request->get('nbrbebe');
                $nbrch = $request->request->get('nbrch');
                $budgetmin = $request->request->get('budgetmin');
                $budgetmax = $request->request->get('budgetmax');
                $remarque = $request->request->get('remarque');

                $personalisation = new Personalisationcircuit();
                $personalisation->setClient($client);
                $personalisation->setCuircuit($circuit);
                $personalisation->setAgent($User);
                
                
                
                $personalisation->setTypec($typec);
                
                $personalisation->setFormule($formule);
                
                $personalisation->setNbradult($nbradult);
                $personalisation->setNbrenf($nbrenf);
                $personalisation->setNbrbebe($nbrbebe);
                $personalisation->setNbrch($nbrch);
                $personalisation->setBudgetmin($budgetmin);
                $personalisation->setBudgetmax($budgetmax);
                $personalisation->setRemarque($remarque);
                $em->persist($personalisation);
                $em->flush();

                foreach( $ville as $key=>$value){
                    $catg = new Tpcircuit();
                    $catg->setPersonalisationcircuit($personalisation);
                    $catg->setVille($ville[$key]);
                    
                    if($request->request->get('dateflex'.$key)=="on")
                {
                    $catg->setDateflex(true);
                }
                else{
                    
                    $catg->setDateflex(false);
                }
                    
                    $catg->setDated(new \Datetime(Tools::explodedate($dated[$key],'/')));
                    $catg->setDater(new \Datetime(Tools::explodedate($dater[$key],'/')));
                    $em->persist($catg);
                    $em->flush();

                }
                
                
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $personalisation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $personalisation->getAgent()->getName().": Réservataion circuit personnalisé" ;
                $headers = "From:'".$info->getNom()."'<" .$personalisation->getAgent()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   " Réservataion circuit personnalisé" ;
                $header = "From:'".$info->getNom()."'<" .$personalisation->getAgent()->getEmail(). ">\n";
                $header .= "Reply-To:" .$personalisation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 
				 Vous avez reçu une réservation circuit personnalisé, merci de consulter votre backoffice .
				</body>';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';

                mail($too, $subjects, $message2, $header); 
                
                $request->getSession()->getFlashBag()->add('notifcirfront', 'Votre demande a été bien envoyée.');

                return $this->redirect($this->generateUrl('front_btob_circuit_homepage'));

          $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client); 
            }
            }
        
        return $this->render('FrontBtobBundle:Circuit:personalize.html.twig', array('entry' => $circuit, 'form' => $form->createView(),
          'info'=>$info));
    }
 

}