<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Ville;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\VilleType;
use Symfony\Component\HttpFoundation\JsonResponse;

class VilleController extends Controller {




    public function ajxPaysAction() {
        $request = $this->get('request');
        $id=$request->request->get("id");
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->findByPays($id);
        
        return $this->render('FrontBtobBundle:Ville:option.html.twig', array('entity'=>$ville)
        );
    }

}
