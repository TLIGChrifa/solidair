<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\AgenceBundle\Entity\Info;
use Btob\VoleBundle\Entity\Vol;
use Btob\VoleBundle\Entity\Tvoles;
use Btob\VoleBundle\Entity\Dvoles;
use Btob\VoleBundle\Entity\Dbvoles;
use Btob\VoleBundle\Entity\Bvoles;
use Btob\VoleBundle\Entity\Reservationbateau;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class VoleController extends Controller
{
    public function indexAction()
    { 
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
			 $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
                         $age = $request->request->get('age');
                         $depart = $request->request->get('depart');
                         
                $arrive = $request->request->get('arrive');
			 
			 $dated = $request->request->get('dated');
			// exit(var_dump($dated));
                if($request->request->get('dater')!='')
                {
                $dater = $request->request->get('dater');
				
                }
			
                
          /*    
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }*/
            $form->handleRequest($request); 
            
            if (($form->isSubmitted() && $form->isValid()) || $request->getSession()->get('client')!=null ) {
                if($request->request->get('emailpassv') != null){
                    
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($request->request->get('emailpassv'));
                if($client !=null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
				//exit(var_dump($salt));
                $em->flush();
                $to = $request->request->get('emailpasso');
				
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Changement mot de passe";
                $headers = "From: '".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';               
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
			
                mail($to, $subject, $message1, $headers);
                $request->getSession()->getFlashBag()->add('notimailvol', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailvolerror', 'Votre email n\'existe pas.');
                }
                }else{
                $session = $this->getRequest()->getSession();
                if($request->request->get('btob_hotelbundle_clients')['email']!=null){
                    
                    $email=$request->request->get('btob_hotelbundle_clients')['email'];
                    $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                     if($client == null){
                    $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                    $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                    $randStringLen = 64;
                    $salt = "";
                    for ($i = 0; $i < $randStringLen; $i++) {
                        $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                    }
                    $options = ['cost' => 12, 'salt' => $salt];
                    $client= new Clients();
                    $password =$request->request->get('btob_hotelbundle_clients')['password'];
              //      exit(var_dump(password_hash($password, PASSWORD_BCRYPT, $options)));
                    $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                    $client->setSalt($salt);
                    $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                    $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                    $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                    $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
             //       $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                    $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                    $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                    $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                    $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                    $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                    $client->setPays($pays);
                    $client->setType(0);
                    $client->setAct(1);
                    $em->persist($client);
                    }else{
                         $request->getSession()->getFlashBag()->add('notimailhotelerror', 'Votre email est deja existé.');
                    }
                }else{
                if($request->getSession()->get('client') == null){
                    $email = $request->request->get('email');
                }else{
                    $email=$request->getSession()->get('client');
                }
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client != null){
                    
                    if($request->getSession()->get('client') != null){
                        
                    }else{
                        $options = ['cost' => 12, 'salt' => $client->getSalt()];
                        $password =$request->request->get('password');
                        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
                        if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                            
                        }else{
                            $request->getSession()->getFlashBag()->add('notivolerror', 'Adresse e-mail ou mot de passe incorrect.');
                        }
                    }
                    
                }
                $em->flush();
                $session->set('client',$email);
                $session->set('fullname',$client->getPname());
                $type = $request->request->get('type');
                //    echo 'test'.$type ; exit;
            
                $compagnie = $request->request->get('compagnie');
                $classes = $request->request->get('classes');
               

                $vol = new Vol();
                $vol->setClient($client);
                $vol->setAgent($User);
                $vol->setType($type);
                
               
                $vol->setCompagnie($compagnie);
                $vol->setClasses($classes);

              
                $vol->setType1("avion");
                $em->persist($vol);
                $em->flush();


				foreach( $named as $key=>$value){
                    $catg = new Tvoles();
                    $catg->setVol($vol);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
					$catg->setPassad($passad[$key]);
                    $catg->setAgeadult(new \DateTime(date('Y-m-d',strtotime(str_replace('/', '-',$age[$key])))));
                 
                    $em->persist($catg);
                    $em->flush();

                }
                
                 foreach( $depart as $keys=>$values){
                    $catgg = new Dvoles();
                    $catgg->setVold($vol);
                    $catgg->setDepart($depart[$keys]);
                    $catgg->setArrive($arrive[$keys]);
                    
                      $catgg->setDated(new \DateTime(date('Y-m-d',strtotime(str_replace('/', '-',$dated[$keys])))));
                      
					  
                      if($type=="Aller-Retour")
                      {
					 
                    $catgg->setDater(new \DateTime(date('Y-m-d',strtotime(str_replace('/', '-',$dater[$keys])))));
                      }
                    $em->persist($catgg);
                    $em->flush();

                }
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $vol->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation Vol" ;
                $headers = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
				 <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>';
				 

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom()."<'".$info->getEmail()."'>: Réservation  Vol " ;
                $header = "From:'".$info->getNom()."' <".$to.">\n";
                $header .= "Reply-To:" .$vol->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='
                             <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation Billetterie Vol , merci de consulter votre backoffice .';

                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';

                mail($too, $subjects, $message2, $header);
				
                $request->getSession()->getFlashBag()->add('noticvol', 'Votre demande a été bien envoyée.');
                $session->set('client',$session->get('client'));
                $session->set('fullname',$session->get('fullname'));
                return $this->redirect($this->generateUrl('front_vole_homepage'));
                
            }
                }
            } else {
                echo $form->getErrors();
            }
        }
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Vole:index.html.twig', array(
            'form' => $form->createView(),
            'banner' => $banner,
            'info'=>$info
            ));
    }


    public function indexbateauAction()
    {
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
       $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
        //    $cin = $post["cin"];
			 $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
                         $age = $request->request->get('age');
            // echo $defaultd .'/'.$defaulta ;exit;
           /* $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }*/
			
			$depart = $request->request->get('depart');
                         
                $arrive = $request->request->get('arrive');
			 
			 $dated = $request->request->get('dated');
                if($request->request->get('dater')!='')
                {
                $dater = $request->request->get('dater');
                }
       //     $form->bind($request->getParameter(new \DateTime(date('Y-m-d',strtotime(str_replace('/', '-',$request->request->get('datenaissance')))))));
		//	$form->setDatenaissance(new \DateTime(date('Y-m-d',strtotime(str_replace('/', '-',$dated[$keys])))));
        //  exit(var_dump($form));
       //     $form->bind($request);
            $form->handleRequest($request);
            
            
            if (($form->isSubmitted() && $form->isValid()) || $request->getSession()->get('client')!=null ) {
                if($request->request->get('emailpassb') != null){
                    
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($request->request->get('emailpassb'));
                if($client !=null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
				//exit(var_dump($salt));
                $em->flush();
                $to = $request->request->get('emailpassb');
				
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Changement mot de passe";
                $headers = "From: '".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';              
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
			
                mail($to, $subject, $message1, $headers);
                $request->getSession()->getFlashBag()->add('notimailbateau', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailbateauerror', 'Votre email n\'existe pas.');
                }
                }else{
                $session = $this->getRequest()->getSession();
                if($request->request->get('btob_hotelbundle_clients')['email']!=null){
                    
                    $email=$request->request->get('btob_hotelbundle_clients')['email'];
                    $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                     if($client == null){
                    $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                    $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                    $randStringLen = 64;
                    $salt = "";
                    for ($i = 0; $i < $randStringLen; $i++) {
                        $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                    }
                    $options = ['cost' => 12, 'salt' => $salt];
                    $client= new Clients();
                    $password =$request->request->get('btob_hotelbundle_clients')['password'];
              //      exit(var_dump(password_hash($password, PASSWORD_BCRYPT, $options)));
                    $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                    $client->setSalt($salt);
                    $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                    $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                    $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                    $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
             //       $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                    $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                    $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                    $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                    $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                    $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                    $client->setPays($pays);
                    $client->setType(0);
                    $client->setAct(1);
                    $em->persist($client);
                    }else{
                         $request->getSession()->getFlashBag()->add('notimailbateauerror', 'Votre email est deja existé.');
                    }
                }else{
                if($request->getSession()->get('client') == null){
                    $email = $request->request->get('email');
                }else{
                    $email=$request->getSession()->get('client');
                }
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client != null){
                    
                    if($request->getSession()->get('client') != null){
                        
                    }else{
                        $options = ['cost' => 12, 'salt' => $client->getSalt()];
                        $password =$request->request->get('password');
                        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
                        if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                            
                        }else{
                            $request->getSession()->getFlashBag()->add('notibateauerror', 'Adresse e-mail ou mot de passe incorrect.');
                        }
                    }
                    
                }
                $em->flush();
                
                $session->set('client',$email);
                $session->set('fullname',$client->getPname());
                $type = $request->request->get('type');
                
                 $contacte = $request->request->get('contacte');
                
                $couchetteint = $request->request->get('couchetteint');
                if($couchetteint=="on")
                {
                    $couchetteint =true;
                }
                else{
                    
                    $couchetteint =false;
                }
                $couchetteext = $request->request->get('couchetteext');
                 if($couchetteext=="on")
                {
                    $couchetteext =true;
                }
                else{
                    
                    $couchetteext =false;
                }
                $cabineanimal = $request->request->get('cabineanimal');
                 if($cabineanimal=="on")
                {
                    $cabineanimal =true;
                }
                else{
                    
                    $cabineanimal =false;
                }
                $singleint = $request->request->get('singleint');
                 if($singleint=="on")
                {
                    $singleint =true;
                }
                else{
                    
                    $singleint =false;
                }
                $singleext = $request->request->get('singleext');
                 if($singleext=="on")
                {
                    $singleext =true;
                }
                else{
                    
                    $singleext =false;
                }
                $suitesingle = $request->request->get('suitesingle');
                 if($suitesingle=="on")
                {
                    $suitesingle =true;
                }
                else{
                    
                    $suitesingle =false;
                }
                $doubleint = $request->request->get('doubleint');
                 if($doubleint=="on")
                {
                    $doubleint =true;
                }
                else{
                    
                    $doubleint =false;
                }
                $doubleext = $request->request->get('doubleext');
                 if($doubleext=="on")
                {
                    $doubleext =true;
                }
                else{
                    
                    $doubleext =false;
                }
                $suitematrimoniale = $request->request->get('suitematrimoniale');
                 if($suitematrimoniale=="on")
                {
                    $suitematrimoniale =true;
                }
                else{
                    
                    $suitematrimoniale =false;
                }
                $tripleint = $request->request->get('tripleint');
                 if($tripleint=="on")
                {
                    $tripleint =true;
                }
                else{
                    
                    $tripleint =false;
                }
                $tripleext = $request->request->get('tripleext');
                 if($tripleext=="on")
                {
                    $tripleext =true;
                }
                else{
                    
                    $tripleext =false;
                }
                $suitefamiliale = $request->request->get('suitefamiliale');
                 if($suitefamiliale=="on")
                {
                    $suitefamiliale =true;
                }
                else{
                    
                    $suitefamiliale =false;
                }
                $quadrupleint = $request->request->get('quadrupleint');
                 if($quadrupleint=="on")
                {
                    $quadrupleint =true;
                }
                else{
                    
                    $quadrupleint =false;
                }
                $quadrupleext = $request->request->get('quadrupleext');
                 if($quadrupleext=="on")
                {
                    $quadrupleext =true;
                }
                else{
                    
                    $quadrupleext =false;
                }
                $repas = $request->request->get('repas');
                
                 if($repas=="on")
                {
                    $repas =true;
                }
                else{
                    
                    $repas =false;
                }
				
				
				$fauteuil = $request->request->get('fauteuil');
                if($fauteuil=="on")
                {
                    $fauteuil =true;
                }
                else{
                    
                    $fauteuil =false;
                }
				
				$pont = $request->request->get('pont');
                if($pont=="on")
                {
                    $pont =true;
                }
                else{
                    
                    $pont =false;
                }
				
                $nbchien = $request->request->get('nbchien');
                
                
                $nbchat = $request->request->get('nbchat');
                
                
                $marque = $request->request->get('marque');
                
                
                $immatricule = $request->request->get('immatricule');
                
                $options = $request->request->get('options');
                
                $compagnie = $request->request->get('compagnie');
                
               $namead = $request->request->get('namead');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
			 $agead = $request->request->get('agead');
                
                
                
                $Reservationcroi = new Reservationbateau();
               
             
                $Reservationcroi->setClient($client);
                
                $Reservationcroi->setAgent($this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB'));
                $Reservationcroi->setType($type);
                $Reservationcroi->setContacte($contacte);
                
                $Reservationcroi->setCouchetteint($couchetteint);
                
                $Reservationcroi->setCouchetteext($couchetteext);
                
                
                $Reservationcroi->setCabineanimal($cabineanimal);
                
                
                $Reservationcroi->setSingleint($singleint);
                
                
                $Reservationcroi->setSingleext($singleext);
                
                
                $Reservationcroi->setSuitesingle($suitesingle);
                
                $Reservationcroi->setDoubleint($doubleint);
                
                $Reservationcroi->setDoubleext($doubleext);
                
                $Reservationcroi->setSuitematrimoniale($suitematrimoniale);
                
                $Reservationcroi->setTripleint($tripleint);
                
                $Reservationcroi->setTripleext($tripleext);
                
                $Reservationcroi->setSuitefamiliale($suitefamiliale);
                
                $Reservationcroi->setQuadrupleint($quadrupleint);
                
                $Reservationcroi->setQuadrupleext($quadrupleext);
                
                $Reservationcroi->setRepas($repas);
                
                $Reservationcroi->setNbchien($nbchien);
                
                
                $Reservationcroi->setNbchat($nbchat);
                
                $Reservationcroi->setMarque($marque);
                
                $Reservationcroi->setImmatricule($immatricule);
                
                $Reservationcroi->setOptions($options);
                
                $Reservationcroi->setCompagnie($compagnie);
        
                $Reservationcroi->setPont($pont);
                
                
                $Reservationcroi->setFauteuil($fauteuil);
                $em->persist($Reservationcroi);
                $em->flush();
                
                
                 foreach( $named as $key=>$value){
                    $catg = new Bvoles();
                    $catg->setReservationbateau($Reservationcroi);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
					$catg->setPassad($passad[$key]);
                    $catg->setAgeadult($agead[$key]);
                 
                    $em->persist($catg);
                    $em->flush();

                }
				
				 foreach( $depart as $keys=>$values){
                    $catgg = new Dbvoles();
                    $catgg->setVolbd($Reservationcroi);
                    $catgg->setDepart($depart[$keys]);
                    $catgg->setArrive($arrive[$keys]);
                      $catgg->setDated($dated[$keys]);
                      
                      if($type=="Aller-Retour")
                      {
                    $catgg->setDater($dater[$keys]);
                      }
                    $em->persist($catgg);
                    $em->flush();

                }
				
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);

                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $Reservationcroi->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation Bâteau" ;
                $headers = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation Bâteau " ;
                $header = "From:'".$info->getNom()."' <".$to.">\n";
                $header .= "Reply-To:" .$Reservationcroi->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
								<tr>
									<td><img src="'.$img_logo.'" /><br></td>
								</tr>
							  </table><br>
							  Bonjour,<br>
						      Vous avez reçu une réservation Billetterie Bateau , merci de consulter votre backoffice .';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';

                mail($too, $subjects, $message2, $header);

                $request->getSession()->getFlashBag()->add('notibateau', 'Votre demande a été bien envoyée.');

                $session->set('client',$session->get('client'));
                $session->set('fullname',$session->get('fullname'));
                return $this->redirect($this->generateUrl('front_vole_bateau_homepage'));


            }
            
        }
                }
            }
        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Vole:indexbateau.html.twig', array(
            'form' => $form->createView(),
            'banner' => $banner,
            'info'=>$info
        ));
    
    }


    
    
    public function voyagecarteAction(){

        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $session = $this->getRequest()->getSession();
            
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');
            $too = $admin->getEmail();
            $to = $request->request->get('email');
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects = $info->getNom().": Voyage à la carte";
            $header = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<table width="900"  cellspacing="1" border="0">
                           <tr>
                            <td><img src="'.$img_logo.'" /><br></td>
                           </tr>
                          </table>
              Bonjour,<br>
              <table width="90%"  cellspacing="1" border="0">';
              
            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;">Informations</b></td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="30%"><b style="padding-left:10px;">Civilité</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('civ').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Nom & Prénom</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('pname').' '.$request->request->get('name').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Téléphone</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('tel').'</td>';
            $message2 .= '</tr>';
            
            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">E-mail</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('email').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Adresse</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('adresse').' '.$request->request->get('ville').' '.$request->request->get('cp').'</td>';
            $message2 .= '</tr>';
              
            $message2 .= '<tr>';
            $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;">Voyages à la carte : </b></td>';
            $message2 .= '</tr>';
            
            $dated = $request->request->get('dated');
            $dater = $request->request->get('dater');
            $payss = $request->request->get('payss');
            $ville = $request->request->get('villes');
            
            foreach ($payss as $key => $value) {
                $message2 .= '<tr>';  
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$payss[$key].'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Ville</b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$ville[$key].'</td>';
                $message2 .= '</tr>';
                
                $message2 .= '<tr>';
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Du</b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$dated[$key].'</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Au</b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">'.$dater[$key].'</td>';
                $message2 .= '</tr>';
            }

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Type et catégorie hébergement</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('typec').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Formule</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('formule').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Transfert</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('transfert').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Excursions souhaitées</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('excursion').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Nbre adultes</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('nbradult').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Nbre enfants (plus de 2 ans)</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('nbrenf').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Nbre bébés (moins de 2 ans)</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('nbrbebe').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Nbre chambre</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('nbrch').'</td>';
            $message2 .= '</tr>';

            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Budget par personne</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">Min: '.$request->request->get('budgetmin').' - Max: '.$request->request->get('budgetmax').'</td>';
            $message2 .= '</tr>';
            
            $message2 .= '<tr>';
            $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Message</b></td>';
            $message2 .= '<td height="30" width="30"><b>:</b></td>';
            $message2 .= '<td height="30">'.$request->request->get('remarque').'</td>';
            $message2 .= '</tr>';
              
            $message2 .= '</table>';
              
            $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
            $message2 .= '<tr>';
            $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
            $message2 .= '</tr>';

            $message2 .= '</table>';
            $message2 .= '</body><br>';
            
            mail($too, $subjects, $message2, $header);

            $request->getSession()->getFlashBag()->add('noticarte', 'Votre demande a été bien envoyée.');
            return $this->redirect($this->generateUrl('front_produit_cartee_homepage'));
            
        }

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Vole:produit-carte.html.twig', array(
            'banner' => $banner,
            'info'=>$info
        ));
    
    }

    
    
    
    public function indexproduitAction(){

        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $session = $this->getRequest()->getSession();
            $cin = $request->request->get("cin");            
            $civ = $request->request->get("civ");           
            $nom = $request->request->get("name");
            $prenom = $request->request->get("pname");
            $tel = $request->request->get("tel");
            $email = $request->request->get("email");
            $adresse = $request->request->get("adresse");
            $cp=$request->request->get("cp");
            $ville=$request->request->get("ville");
            $pays = $request->request->get("pays");
            $villed = $request->request->get("villed");
            $dated = $request->request->get("dated");
            $heured = $request->request->get("heured");
            $villef = $request->request->get("villef");
            $datef = $request->request->get("datef");
            $heuref = $request->request->get("heuref");
            $vehicule = $request->request->get("vehicule");
            $type_transport = $request->request->get("type_transport");
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');
            $too = $admin->getEmail();
            $to = $email;
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $info->getNom().": Transfert";
            $headers = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .= '<img src="'.$img_logo.'" align="center" /><br>';
            $message1 .= '<table width="90%"  cellspacing="1" border="0">';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Coordonnées :</b></td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">C.I.N / Passeport</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $cin . '</td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $civ . '</td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $nom . '</td>';
            $message1 .= '</tr>';           
            $message1 .= '<tr>';            
            $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';           
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $prenom . '</td>';            
            $message1 .= '</tr>';       
            $message1 .= '<tr>';            
            $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';          
            $message1 .= '<td height="30"><b>:</b></td>';           
            $message1 .= '<td height="30">' . $tel . '</td>';           
            $message1 .= '</tr>';       
            $message1 .= '<tr>';            
            $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';       
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $email . '</td>';     
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $adresse . '</td>';
            $message1 .= '</tr>';       
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Code postale</b></td>'; 
            $message1 .= '<td height="30"><b>:</b></td>';   
            $message1 .= '<td height="30">' . $cp . '</td>';    
            $message1 .= '</tr>';   
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Ville</b></td>';    
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $ville . '</td>'; 
            $message1 .= '</tr>';   
            $message1 .= '<tr>';        
            $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';     
            $message1 .= '<td height="30"><b>:</b></td>';   
            $message1 .= '<td height="30">' . $pays . '</td>';  
            $message1 .= '</tr>';   
            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Transports :</b></td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Ville de départ</b></td>';  
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $villed . '</td>';    
            $message1 .= '</tr>';       
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Date de départ</b></td>';       
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $dated . '</td>'; 
            $message1 .= '</tr>';   
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Heure de départ</b></td>';  
            $message1 .= '<td height="30"><b>:</b></td>';   
            $message1 .= '<td height="30">' . $heured . '</td>';    
            $message1 .= '</tr>';   
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Ville d\'arrivée</b></td>'; 
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $villef . '</td>';    
            $message1 .= '</tr>';       
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Date d\'arrivée</b></td>';      
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $datef . '</td>'; 
            $message1 .= '</tr>';   
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Heure d\'arrivée</b></td>'; 
            $message1 .= '<td height="30"><b>:</b></td>';   
            $message1 .= '<td height="30">' . $heuref . '</td>';    
            $message1 .= '</tr>';   
            $message1 .= '<tr>';        
            $message1 .= '<td height="30"><b style="padding-left:10px;">vehicule</b></td>'; 
            $message1 .= '<td height="30"><b>:</b></td>';       
            $message1 .= '<td height="30">' . $vehicule . '</td>';  
            $message1 .= '</tr>';   
            $message1 .= '<tr>';    
            $message1 .= '<td height="30"><b style="padding-left:10px;">Type</b></td>'; 
            $message1 .= '<td height="30"><b>:</b></td>';   
            $message1 .= '<td height="30">' . $type_transport . '</td>';    
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
            $message1 .= '</tr>';
            $message1 .= '</table>
            </body>';
            mail($to, $subject, $message1, $headers);
			
            $request->getSession()->getFlashBag()->add('noticarte', 'Votre demande a été bien envoyée.');
            return $this->redirect($this->generateUrl('front_produit_carte_homepage'));
            
        }

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();
        return $this->render('FrontBtobBundle:Vole:transfert.html.twig', array(
            'banner' => $banner,
            'info'=>$info
        ));
    
    }
    
    
        

}
