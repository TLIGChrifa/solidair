<?php

namespace Front\BtobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Btob\VoitureBundle\Entity\Voiture;
use Btob\ResvoitureBundle\Entity\Reservationvoiture;
use Btob\VoitureBundle\Entity\Imgv;
use Btob\VoitureBundle\Form\VoitureType;
use Btob\HotelBundle\Entity\Clients;
use Btob\AgenceBundle\Entity\Info;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Common\Tools;


class VoitureController extends Controller
{


    public function indexAction()
    {

        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $voitures = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->findAll();
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $voitures,
            $request->query->get('page', 1)/*page number*/,
            8/*limit per page*/
        );
        return $this->render('FrontBtobBundle:Voiture:index.html.twig', array(
            'entities' => $entities,
            'info'=>$info
        ));
    }
    public function detailAction(Voiture $voiture)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        return $this->render('FrontBtobBundle:Voiture:detail.html.twig', array('entry' => $voiture,'info'=>$info));
    }
    public function reservationvoitureAction($id)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $datef = new \Datetime(Tools::explodedate($request->request->get('dater'), '/'));
                $message = $request->request->get('message');
                
                $nump = $request->request->get('nump');
                $numc = $request->request->get('numcin');
                $nationalite = $request->request->get('nationalite');
                $datep = new \Datetime(Tools::explodedate($request->request->get('datep'), '/'));
                $datec = new \Datetime(Tools::explodedate($request->request->get('datec'), '/'));
               
                
                $reservation = new Reservationvoiture();
                $reservation->setClient($client);
                $reservation->setVoiture($entities);
                $reservation->setAgent($this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB'));

                $reservation->setMessage($message);
                $reservation->setDated($dated);
                $reservation->setDatef($datef);
                
                
                $reservation->setNumc($numc);
                $reservation->setNationalite($nationalite);
                $reservation->setNump($nump);
                $reservation->setDatep($datep);
                $reservation->setDatec($datec);

                $em->persist($reservation);
                $em->flush();
                                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "Afritours: Réservation Voiture" ;
                $headers = "From:Afritours info@afritours.com.tn\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                //mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservtaion Voiture" ;
                $header = "From:Afritours <".$to.">\n";
                $header .= "Reply-To:" .$reservation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation voiture , merci de consulter votre backoffice .';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';


                mail($too, $subjects, $message2, $header);
                //mail("afef.tuninfo@gmail.com", $subjects, $message2, $header);

                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notivoiture', 'Votre demande a été bien envoyée.');

               // return $this->render('FrontBtobBundle:Voiture:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
                return $this->redirect($this->generateUrl('front_voiture'));

            }
        }

        return $this->render('FrontBtobBundle:Voiture:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView(),'info'=>$info));
    }



    public function listreservationvoitureAction()
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $entities = $this->getDoctrine()->getRepository("BtobResvoitureBundle:Reservationvoiture")->findAll();
        return $this->render('FrontBtobBundle:Voiture:listreservationvoiture.html.twig', array('entities' => $entities,'info'=>$info));
    }

}
