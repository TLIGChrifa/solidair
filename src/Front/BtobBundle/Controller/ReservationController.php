<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Tuncms\BannerBundle\Entity\Banner;
use Main\FrontBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Main\FrontBundle\Entity\Client;
use Btob\AgenceBundle\Entity\Info;


class ReservationController extends Controller
{

    public function indexAction($code = "")
    {
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        if ($session->has('client') || count($session->has('client')) != 1) {

            return $this->redirect($this->generateUrl('main_page_adresse'));
        }
        $idparentclient = Null;
        if ($code != "") {
            $idparentclient = Tools::decrypt($code);
        }
        $y = date('Y') - 10;
        $d = $y - 82;
        $client = new Client();
        $adresse = new Adresse();
        $msg = "";
        $tst = true;
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            //Tools::dump($request->request);
            if ($request->request->get('parent') > 0) {
                $idd = $request->request->get('parent');
                $parent = $this->getDoctrine()->getRepository('MainFrontBundle:Client')->find($idd);

                $client->setParent($parent);
                // envoie mail to parain
                $emailsender="rami@tuninfo.com";
                $message = \Swift_Message::newInstance()
                    ->setSubject('Confirmation d\'inscription d\'un ami: dmode.tn')
                    ->setFrom($emailsender)
                    ->setTo(array($parent->getEmail()))
                    ->setBody($this->renderView('MainFrontBundle:Reservation:mailtoparrin.html.twig', array('client' => $request->request->get("firstname") . " " . $request->request->get("lastname"))), 'text/html');
                $this->get('mailer')->send($message);
            } else {
                $client->setParent(null);
            }

            $testemail = $this->getDoctrine()->getRepository('MainFrontBundle:Client')->FindByMail($request->request->get("guest_email"));
            if ($testemail > 0) {
                $msg .= "<li><b>Adresse e-mail</b> : Vous avez déjà un compte</li>";
                $tst = false;
            } else if ($request->request->get("password") != $request->request->get("repassword")) {
                $msg .= "<li><b>Mot de passe</b> : Vérifiez votre mot de passe</li>";
                $tst = false;
            } else {
                $client->setAdresse($request->request->get("address1"));
                $client->setCiv($request->request->get("id_gender"));
                $client->setCompany($request->request->get("company"));
                $client->setCp($request->request->get("postcode"));
                $dt = $request->request->get("years") . "-" . $request->request->get("months") . "-" . $request->request->get("days");
                $client->setDatenaiss(new \DateTime($dt));
                $client->setEmail($request->request->get("guest_email"));
                $client->setName($request->request->get("firstname"));
                $client->setTel($request->request->get("phone"));
                $client->setVille($request->request->get("city"));
                $client->setPname($request->request->get("lastname"));
                $client->setPays($request->request->get("id_country"));
                $client->setPassword($request->request->get("password"));
                $cnfsender = "rami@tuninfo.com";

                // envoi d' email de confirmation
                $message = \Swift_Message::newInstance()
                    ->setSubject('Confirmation d\'inscription : dmode.tn')
                    ->setFrom($cnfsender)
                    ->setTo(array($client->getEmail(), $cnfsender))
                    ->setBody($this->renderView('MainFrontBundle:Reservation:mail.html.twig', array('client' => $client)), 'text/html');


                $this->get('mailer')->send($message);

                $em->persist($client);
                $em->flush();
                $adresse = new Adresse();
                $adresse->setAdresse($request->request->get("address1"));
                $adresse->setClient($client);
                $adresse->setRef("Mon adresse de livraison");
                $adresse->setName($request->request->get("firstname") . " " . $request->request->get("lastname"));
                $adresse->setCp($request->request->get("postcode"));
                $adresse->setPays($request->request->get("id_country"));
                $adresse->setVille($request->request->get("city"));
                $em->persist($adresse);
                $em->flush();
                // injection d'adresse de livraison
                $msg .= "<li><b>Vous êtes maintenant inscrit</b><br> Pour continuer la finalisation de votre commande merci de se connecter à votre compte</li>";
                //Tools::dump($client, true);
            }
        }
        return $this->render('MainFrontBundle:Reservation:inscription.html.twig', array(
            'y' => $y,
            'x' => $d,
            'msg' => $msg,
            'parent' => $idparentclient,
            'tst' => $tst,
            'info'=>$info
        ));
    }

   
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $id = $request->request->get("id");
            $commande = $this->getDoctrine()->getRepository('MainFrontBundle:Commande')->find($id);
            $commande->setEtatsps("Livraison");
            $em->persist($commande);
            $em->flush();
        }
        return $this->render('FrontBtobBundle:Reservation:recap.html.twig', array());
    }

}
