<?php

namespace Front\BtobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\AssuranceBundle\Entity\Assurance;
use Btob\AssuranceBundle\Form\AssuranceType;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\AgenceBundle\Entity\Info;
use Btob\HotelBundle\Common\Tools;

/**
 * Assurance controller.
 *
 */
class AssuranceController extends Controller
{

    public function reservationAction()
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
		$User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $daten = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                
                $datead = new \Datetime(Tools::explodedate($request->request->get('datead'),'/'));
                $dateaf = new \Datetime(Tools::explodedate($request->request->get('dateaf'),'/'));
                $duree =$request->request->get("duree");
                
                $assurance=new Assurance();
                $assurance->setDated($daten);
                $assurance->setFunction($request->request->get("function"));
                $assurance->setSituation($request->request->get("situation"));
                $assurance->setQuestion1($request->request->get("question1"));
                $assurance->setQuestion1rep($request->request->get("question1rep"));
                $assurance->setQuestion2($request->request->get("question2"));
                $assurance->setQuestion2rep($request->request->get("question2rep"));
                $assurance->setQuestion3($request->request->get("question3"));
                $assurance->setQuestion3rep($request->request->get("question3rep"));
                $assurance->setPass($request->request->get("pass"));
                $assurance->setClient($client);
		$assurance->setAgent($User);
                $assurance->setDuree($duree);
                $assurance->setDatead($datead);
                $assurance->setDateaf($dateaf);

                $em->persist($assurance);
                $em->flush();
				
				                                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $assurance->getClient()->getEmail();


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  "Afritours: Réservation Assurance" ;
                $headers = "From:Afritours info@afritours.com.tn\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                //mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservtaion Assurance" ;
                $header = "From:Afritours <".$to.">\n";
                $header .= "Reply-To:" .$assurance->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation assurance , merci de consulter votre backoffice .

                   <a href="http://www.afritours.com.tn/b2b/assurance/'.$assurance->getId().'/show"> Cliquer Içi</a>';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';


                mail($too, $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
				
				
				
                 $request->getSession()->getFlashBag()->add('notiassurancefront', 'Votre demande a été bien envoyée.');

                return $this->redirect($this->generateUrl('front_assurance_reservation_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('FrontBtobBundle:Assurance:reservation.html.twig', array('form' => $form->createView(),));
    }

   




}
