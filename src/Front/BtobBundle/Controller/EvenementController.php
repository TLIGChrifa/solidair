<?php
namespace Front\BtobBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\OmraBundle\Entity\Omra;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\EvenementBundle\Entity\Evenement;
use Btob\EvenementBundle\Entity\Evenementprice;
use Btob\AgenceBundle\Entity\Info;
use Symfony\Component\HttpFoundation\Request;

use Btob\EvenementBundle\Entity\Reservationevenement;

use Btob\EvenementBundle\Entity\Reservationevdetail;
class EvenementController extends Controller
{

    public function indexAction(Request $request)
    {

       
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);

       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->listPrice();

        $paginator = $this->get('knp_paginator');
		
        $entities = $paginator->paginate(
            $evenementprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        
        
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Evenement:index.html.twig', array(
            'entities' => $entities,
            'entity' => $entities,
            'info' => $info,
            'evenementprices' => $evenementprices,
            'banner' => $banner,
            ));
    }

 public function searchAction(Request $request)
    { 
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $titre=$request->request->get("titre");
        $villed=$request->request->get("villed");
     //   $duree=$request->request->get("duree");
     $entityManager = $this->getDoctrine()->getManager();
        $ep =$entityManager->createQueryBuilder('a')
            ->select('a')
            ->from(Evenementprice::class, 'a')
            ->leftJoin('a.evenement', 'u')
            ->andWhere('u.act =:act')
            ->setParameter('act', true)
            ->groupBy('a.evenement');

            if($titre != "all"){
           $ep=$ep ->andwhere('u.titre = :titre')
                   ->setParameter('titre', $titre);
            }
            if($villed != null){
           $ep=$ep ->andwhere('u.villed LIKE :villed')
                   ->setParameter('villed', '%'.$villed.'%');
            }
      /*      if($duree != 0){
           $ep=$ep->andwhere('u.nbrjr = :duree')
            ->setParameter('duree', $duree);
            }*/
       
        $evenementprices=$ep->getQuery()
                            ->getResult();

       $entity = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->listPrice();

        $paginator = $this->get('knp_paginator');
		
        $entities = $paginator->paginate(
            $evenementprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        
        
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Evenement:search.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
            'evenementprices' => $evenementprices,
            'banner' => $banner,
            'info'=>$info
            ));
    }
    public function detailAction(Evenement $Evenement)
    {
       
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        
       $evenementperiode = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($Evenement->getId());

        return $this->render('FrontBtobBundle:Evenement:detail.html.twig', array('entry' => $Evenement,'periods' => $evenementperiode,'info'=>$info));
    }


    
    
    public function personalisationAction(Evenement $evenement)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);

       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($evenement->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $evenementprices = $request->request->get("evenementprice");
            $session->set('evenementprice', $evenementprices);
            
           
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
            
           
            
          return $this->redirect($this->generateUrl('front_inscrip_reservation_evenement', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Evenement:personalisation.html.twig', array('entry' => $evenement,'evenementprices' => $evenementprices,'info'=>$info));

    
    }
    
    
    
    
    public function inscriptionAction(Evenement $evenement)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $evenementprice = $session->get("evenementprice");
       
       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->find($evenementprice);
      
       $evenementsupp = $this->getDoctrine()->getRepository("BtobEvenementBundle:Supplementev")->findSupplementByEvenementprice($evenementprices->getId());
       
      

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $Array = array();
         $aujourdhuib = new \DateTime();
       $nbjourbebe = intval($evenement->getAgebmax())*365;
       $aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");

        
        $aujourdhuienf = new \DateTime();
       $nbjourenf = intval($evenement->getAgeenfmax())*365;
       $aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('evenementprice', $evenementprice);
            
          

          return $this->redirect($this->generateUrl('front_evenement_reservation_homepage', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Evenement:inscription.html.twig', array('datedb' => $datedb,'datedenf' => $datedenf,'entry' => $evenement,'evenementsupp' => $evenementsupp,'evenementprices' => $evenementprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf,'info'=>$info));

    
    }
    
    
  
    
     public function reservationevenementAction(Evenement $evenement)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());

        $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $evenementperiode = $session->get("evenementprice");
        
        
       $priceev =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->find($evenementperiode);   

       $mode = $this->getDoctrine()->getRepository('BtobHotelBundle:Payement')->find(1); 

        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
          
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
             
                 
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              
              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         $ArrBase[$key]['price'] = $prices+($prices*$evenement->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $evenementprice = $session->get("evenementprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationevenement();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setEvenement($evenement);
                $resa->setEvenementprice($priceev);
                
                $resa->setTotal($total);

                $resa->setAvance($total*($evenement->getPrixavance()));
                $resa->setEtat(1);

                  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
       
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
           
           $resadetail=new Reservationevdetail();
           $resadetail->setReservationevenement($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                 
                  
                
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               
           $resadetails=new Reservationevdetail();
           $resadetails->setReservationevenement($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
            
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               
               $resadetailb=new Reservationevdetail();
           $resadetailb->setReservationevenement($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
            $em->persist($resadetailb);
            $em->flush();
           }
           }
           
          $prices = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         $ArrBase[$key]['price'] = $prices+($prices*$evenement->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

                $to = $resa->getClient()->getEmail();
               
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation évenement" ;
                $headers = "From:'".$info->getNom()."' <'".$info->getEmail()."'> \n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							    <td align="right"><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				  Cordialement.';
                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);
                mail('abdallah.dgperformance@gmail.com', $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Réservation évenement" ;
                $header = "From:'".$info->getNom()."' <".$to.">\n";
                $header .= "Reply-To:" .$resa->getUser()->getName()." " .$info->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation évenement , merci de consulter votre backoffice .';
                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';


                mail($too, $subjects, $message2, $header);
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);
                
               

            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('notievenementf', 'Votre demande a été bien envoyée. Merci.');

                $session->set('reservation', $resa);
                $session->set('ArrBase', $ArrBase);
                $session->set('paiement', $request->request->get('paiement'));

                $my_session_id = session_id();
        

                return $this->render('FrontBtobBundle:Evenement:validation.html.twig', array(
                        'paiement' => $request->request->get('paiement'),
                        'reservation' => $resa,
                        'my_session_id' => $my_session_id,
                        'entry'=>$evenement,
                        'recap'=>$recap,
                        'ArrBase'=>$ArrBase,
                        'total'=>$total,
                        'client' => $resa->getClient(),
                        'info'=>$info
                       
                    )
                );

        }

        return $this->render('FrontBtobBundle:Evenement:reservation.html.twig', array('form' => $form->createView(),'entry'=>$evenement,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total, 'mode' => $mode,'info'=>$info));

    }


}
