<?php

namespace Front\BtobBundle\Controller;

use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\TransfertBundle\Entity\Transfert;
use Btob\TransfertBundle\Entity\Transfertsperiode;
use Btob\TransfertBundle\Entity\Reservationtransfert;
use Btob\TransfertBundle\Entity\Tcategories;
use Btob\TransfertBundle\Form\TransfertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;


class TransfertsController extends Controller
{

    
    public function optionAction()
    {
       


       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

      
        $payst = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->findActiv();
        $paystid = "1";
        $villet = "";
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
      if ($request->getMethod() == 'POST') {
            
    
            $villet = $request->request->get("villet");
            
            $session->set('villet', $villet);
            $villetd = $request->request->get("villetd");
            $session->set('villetd', $villetd);
            
            $dated = $request->request->get("dated");
            $session->set('dated', $dated);
            
            $payst = $request->request->get("payst");
            $session->set('payst', $payst);
            
            $nbpersonne = $request->request->get("nbpersonne");
            $session->set('nbpersonne', $nbpersonne);
            
            
            $type = $request->request->get('type');
            $session->set('type', $type);
            
            
            $dater = $request->request->get("dater");
            $session->set('dater', $dater);
            
            $categorie = $request->request->get('categorie');
            $session->set('categorie', $categorie);
            
          return $this->redirect($this->generateUrl('front_search_transfert'));

            
        }

        return $this->render('FrontBtobBundle:Transferts:option.html.twig', array(
                'payst' => $payst,
                'paystid' => $paystid,
                'villet' => $villet,
                'dated' => $dated
               
            )
        );
    }
 public function searchAction()

    {
       $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $villet = $request->request->get("villet");
        $villetd = $request->request->get("villetd");
        $dated = $request->request->get("dated");
        $payst = $request->request->get("payst");
        $nbpersonne = $request->request->get("nbpersonne");
       $paystt = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->findActiv();
        
        $type = $request->request->get("type");
        
        
        
        $villedep = intval($villet);
        $villedes = intval($villetd);
        $paystres = intval($payst);
        $intnbpersonne = intval($nbpersonne);
        
        
        $tab = explode("/", $dated);

         $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);
           if($type=="Aller-Retour")
        {
         $categorie = $request->request->get("categorie");
        $dater = $request->request->get("dater");
           $tab1 = explode("/", $dater);

         $d2 = new \DateTime($tab1[2] . "-" . $tab1[1] . "-" . $tab1[0]);
           $nbjour = $d2->diff($d1);
            $nbjour = $nbjour->days+1; 
            
        }else{
            $dater = null;
            $categorie = null;
            $d2 = null;
            $nbjour = 1;
            
        }
         
             $villed = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($villedep);
             $villedess = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($villedes);
             $pays = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->find($paystres);
   
         
         
          $extransf = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->ListByVilledAndvillea($d1,$paystres,$villedep,$villedes);
       if($extransf)
       {
              $transport = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->ListTransport($extransf->getId());
       
              
              
       }else{
         $transport= array(); 
         
       }
          $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $transport,
            $request->query->get('page', 1)/*page number*/,
            5/*limit per page*/
        );   
     $session->set('villed', $villed);
     $session->set('villedess', $villedess);
     $session->set('paystres', $paystres);
     $session->set('nbpersonne', $nbpersonne);
     $session->set('type', $type);
     $session->set('d1', $d1);
     $session->set('d2', $d2);
     $session->set('nbjour', $nbjour);
     $session->set('categorie', $categorie);
     
     
     
        return $this->render('FrontBtobBundle:Transferts:search.html.twig', array(
                'villet' => $villet,
                'villetd' => $villetd,
                'villed' => $villed,
                'villedess' => $villedess,
                'pays' => $pays,
                'payst' => $payst,
                'paystt' => $paystt,
                 'type' => $type,
                 'dated' => $dated,
                 'dater' => $dater,
                'd1' => $d1,
                 'd2' => $d2,
                'nbjour' => $nbjour,
                'categorie' => $categorie,
                'nbpersonne' => $nbpersonne,
                'pagination' => $pagination,
                'intnbpersonne' => $intnbpersonne
            )
        );  
        
        
    }  
    
    public function inscriptionAction()

    {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        
        $villet = $session->get("villed");
        $villetd = $session->get("villedess");
        $dated = $session->get("d1");
        $payst = $session->get("paystres");
        $nbpersonne = $session->get("nbpersonne");
        
        $type = $session->get("type");
        $dater = $session->get("d2");
        $nbjour = $session->get("nbjour");
        $categorie = $session->get("categorie");
        
        $mode = $this->getDoctrine()->getRepository('BtobHotelBundle:Payement')->find(1); 
        
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
       $pays = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->find($payst);
        $value = $request->get('_route_params');
        
        $trans =$value["transportid"];
        $periode=$value["periode"];
        $qte=$value["qte"];
        
        
        $transp = intval($trans);
        $qantite= intval($qte);
        $perpric= intval($periode);
      $transport = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transport')->find($transp);
      $periodprice = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->find($perpric);

      
      
      if($type=="Aller-Simple")
      {
      $price =$qantite*$periodprice->getPrice();
      if($qantite<=$transport->getQte())
         {
          
          $msg= "Disponible";
         }else{
            $msg= "Sur Demande"; 
             
         }
       
      }else{
          if($nbjour==1)
          {
              if($qantite<=$transport->getQte())
                 {
          
               $msg= "Disponible";
               }else{
              $msg= "Sur Demande"; 
             
                }
                
            $price =$qantite*$periodprice->getPrice()*2;  
            
          }
          else{
              
            $price =$qantite*$periodprice->getPrice()*2;
            $msg= "Sur Demande"; 
          }
          
      }
      
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            
            $defaultd = $request->request->get('tp-defaultd');
            $pointd = $request->request->get('pointd');
            $pointa = $request->request->get('pointa');
            $bagage = $request->request->get('bagage');
            
            if($type=="Aller-Retour")
                
            {
              $defaultrd = $request->request->get('tp-defaultrd');
            $pointrd = $request->request->get('pointrd');
            $pointra = $request->request->get('pointra');
            $categorie= $categorie;
                
            }else{
             $defaultrd = null;
            $pointrd = null;
            $pointra = null; 
            $categorie= null;
                
            }
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {

                $em->persist($client);
                $em->flush();
                $session->set('namead', $request->request->get('namead'));
                 $session->set('ageadult', $request->request->get('ageadult'));
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                
                $nameadulte = $session->get('namead');
                $nbageadult = $session->get('ageadult');
                $reservation = new Reservationtransfert();
                $reservation->setClient($client);
               
                $reservation->setUser($User);
                
                
                $reservation->setDated($dated);
                
                $reservation->setVilledep($villet);
                $reservation->setVilledes($villetd);
                
                $reservation->setPays($pays->getName());
                
                $reservation->setTotal($price);
                $reservation->setQte($qte);
                 $reservation->setHeurd($defaultd);
                 $reservation->setPointd($pointd);
                 $reservation->setBag($bagage);
                $reservation->setPointa($pointa);
                
                 $reservation->setDater($dater);
                $reservation->setHeurr($defaultrd);
                 $reservation->setPointrd($pointrd);
                 $reservation->setPointra($pointra);
                 $reservation->setType($type);
                 $reservation->setCategorie($categorie);
                 $reservation->setMsg($msg);
                $reservation->setNbjour($nbjour);
                        
                $reservation->setTransport($transport);
                
                
                $reservation->setNamead(json_encode($nameadulte));
                $reservation->setAgeadult(json_encode($nbageadult));
                $em->persist($reservation);
                $em->flush();
               

              
            

                }
                
                
                setlocale(LC_TIME, 'fr_FR', 'fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%d %B %Y ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%A ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));


                $to = $reservation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $reservation->getUser()->getName() . ": réservation transfert ";
                $headers = "From" . $reservation->getUser()->getName() . ":" . $reservation->getUser()->getEmail() . "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table>';
                $message1 .= '
				<br /> <b>Bonjour,</b><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br />
				 <table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

               


                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
                $message1 .= '</tr>';
				
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
                $message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';

                mail($to, $subject, $message1, $headers);

                //mail("afef.tuninfo@gmail.com", $subject, $message1, $headers);
 
                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects = "Réservation transfert ";
                $header = "From" . $reservation->getUser()->getName() . ":" . $reservation->getUser()->getEmail() . "\n";
                $header .= "Reply-To:" . $reservation->getUser()->getName() . " " . $admin->getEmail() . "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="http://www.afritours.com.tn/front/images/logo.png" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table>';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Sélection :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                $message2 .= '<td width="20%" height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getId() . '</td>';
                $message2 .= '</tr>';

               

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
                $message2 .= '</tr>';

                

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#013f86"><b style="color:#fff; padding-left:5px;"> Coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
                $message2 .= '</tr>';
				
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#013f86" style="color:#fff;">
				 61, Avenue Jean Jaurès 1000 Tunis Tunisie - <a href="mailto:info@afritours.com.tn" style="color:#fff;">info@afritours.com.tn</a>
				</td>';
                $message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';

                mail($too, $subjects, $message2, $header);  

                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header);   
                
                
                $request->getSession()->getFlashBag()->add('notitransfrontf', 'Votre demande a été bien envoyée. Merci.');

                
                $reservation->setAvance($price);
                $reservation->setEtat(1);

                $em->persist($reservation);

                $em->flush();

                $session->set('reservation', $reservation);
                $session->set('paiement', $request->request->get('paiement'));

                $session->set('villed', $villet);
                $session->set('villea', $villetd);
                $session->set('dated', $dated);
                $session->set('qantite', $qantite);
                $session->set('transp', $transp);
                $session->set('price', $price);
                $session->set('type', $type);
                $session->set('msg', $msg);
                $session->set('transport', $transport);
                $session->set('pays', $pays);
                $session->set('nbpersonne', $nbpersonne);

            return $this->redirect($this->generateUrl('front_reservation_validation_transfert'));



            } else {
                echo $form->getErrors();
            }

        return $this->render('FrontBtobBundle:Transferts:inscription.html.twig', array(
            
                'villed' => $villet,
                'villea' => $villetd,
                'dated' => $dated,
                'qantite' => $qantite,
                'transp' => $transp,
                'price' => $price,
                'type' => $type,
                'msg' => $msg,
                'transport' => $transport,
                'periodprice' => $periodprice,
                'form' => $form->createView(),
                'pays' => $payst,
                'pays' => $pays,
                'nbpersonne' => $nbpersonne,
                'mode' => $mode
                

            )
        );  
    }


    public function validationAction()
    {
       


        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $paiement = $session->get('paiement');
        $reservation = $session->get('reservation');
        $my_session_id = session_id();

        $villed = $session->get('villed');
        $villea = $session->get('villea');
        $dated = $session->get('dated');
        $qantite = $session->get('qantite');
        $transp = $session->get('transp');
        $price = $session->get('price');
        $type = $session->get('type');
        $msg = $session->get('msg');
        $transport = $session->get('transport');
        $periodprice = $session->get('periodprice');
        $client = $reservation->getClient();
        $pays = $session->get('pays');
        $nbpersonne = $session->get('nbpersonne');
        

        return $this->render('FrontBtobBundle:Transferts:validation.html.twig', array(
                'paiement' => $paiement,
                'reservation' => $reservation,
                'my_session_id' => $my_session_id,

                'villed' => $villed,
                'villea' => $villea,
                'dated' => $dated,
                'qantite' => $qantite,
                'transp' => $transp,
                'price' => $price,
                'type' => $type,
                'msg' => $msg,
                'transport' => $transport,
                'periodprice' => $periodprice,
                'client' => $client,
                'pays' => $pays,
                'nbpersonne' => $nbpersonne,
               
            )
        );
    }
    
    
}