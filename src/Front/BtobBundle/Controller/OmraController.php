<?php
namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\User;
use Btob\OmraBundle\Entity\Omra;
use Btob\OmraBundle\Entity\Omraprice;
use Symfony\Component\HttpFoundation\Request;

use Btob\OmraBundle\Entity\Reservationomra;
use Btob\OmraBundle\Entity\Personalisationomra;

use Btob\OmraBundle\Entity\Tpomra;
use Btob\AgenceBundle\Entity\Info;

use Btob\OmraBundle\Entity\Reservationodetail;
class OmraController extends Controller
{

    public function indexAction(Request $request)
    {
        $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        
       $omraprices = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->listPrice();
        $entity = $omraprices;
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $omraprices,
            $request->query->get('page', 1)/*page number*/,
            9 /*limit per page*/
        );
   //     exit(var_dump($omraprices));
        
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Omra:index.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
            'omraprices' => $omraprices,
            'banner' => $banner,
            'info' =>$info
            ));
    }

    public function searchAction(Request $request)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $titre=$request->request->get("titre");
        $destination=$request->request->get("destination");
        $duree=$request->request->get("duree");
        $entityManager = $this->getDoctrine()->getManager();
        $sp = $entityManager->createQueryBuilder('a')
            ->select('a')
            ->from(Omraprice::class, 'a')
            ->leftJoin('a.omra', 'u')
            ->andWhere('u.act =:act')
            ->setParameter('act', true)
            ->groupBy('a.omra');
        if($titre != "all"){
           $sp=$sp ->andwhere('u.titre = :titre')
                   ->setParameter('titre', $titre);
        }
        if($destination != null){
          $sp=$sp ->andwhere('u.villea LIKE :destination')
                  ->setParameter('destination', '%'.$destination.'%');
        }
        if($duree != 0){
           $sp=$sp->andwhere('u.dureeomra = :duree')
                  ->setParameter('duree', $duree);
        }
        $omraprices=$sp->getQuery()
                         ->getResult();
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $omraprices,
            $request->query->get('page', 1)/*page number*/,
            9 /*limit per page*/
        );

        $entity = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->listPrice();

        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('FrontBtobBundle:Omra:search.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
            'omraprices' => $omraprices,
            'banner' => $banner,
            'info'=>$info
        ));
    }

    public function detailAction(Omra $omra)
    {
       
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
        $omraperiode = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->findByOmra($omra->getId());
        $omraprices = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->listPrice();
        return $this->render('FrontBtobBundle:Omra:detail.html.twig', array(
                'entry' => $omra,'periods' => $omraperiode,
                'omraprices' => $omraprices,
                'info'=>$info
               ));
    }


    
    
    public function personalisationAction(Omra $omra)

    {

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
       $em = $this->getDoctrine()->getManager();
       
       $omraprices = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->findByOmra($omra->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();
 
        if ($request->getMethod() == 'POST') {
            
            $omraprices = $request->request->get("omraprice");
            $session->set('omraprice', $omraprices);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
          return $this->redirect($this->generateUrl('front_inscrip_reservation_omra', array('id'=>$omra->getId())));

            
        }
       
       
       
       
    return $this->render('FrontBtobBundle:Omra:personalisation.html.twig', array('entry' => $omra,'omraprices' => $omraprices,
      'info'=>$info
  ));

    
    }
    
    
    
    
    public function inscriptionAction(Omra $omra)

    {

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
       $em = $this->getDoctrine()->getManager();
       
       $omraprices = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->findByOmra($omra->getId());
       $omrasupp = $this->getDoctrine()->getRepository("BtobOmraBundle:Supplemento")->findSupplementByOmra($omra->getId());
       

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $omraprice = $session->get("omraprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
       
		$omrapricess = $this->getDoctrine()->getRepository("BtobOmraBundle:Omraprice")->find($omraprice);
	   
        $Array = array();
        
        $aujourdhuib = new \DateTime($omrapricess->getDated()->format("Y-m-d"));
		$nbjourbebe = intval($omra->getAgebmax())*365;
		$aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");

        
        $aujourdhuienf = new \DateTime($omrapricess->getDated()->format("Y-m-d"));
		$nbjourenf = intval($omra->getAgeenfmax())*365;
		$aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('omraprice', $omraprice);
            
          return $this->redirect($this->generateUrl('front_omra_reservation_homepage', array('id'=>$omra->getId())));

            
        }
       
       
       
		$datedbb = $aujourdhuib->format("Y-m-d");
        $datedenff = $aujourdhuienf->format("Y-m-d");
		$datedb_fin=date($datedbb);
		$datedenf_fin=date($datedenff);
		$auj_fin=date("Y-m-d");
       
       
    return $this->render('FrontBtobBundle:Omra:inscription.html.twig', array('datedb' => $datedb,'datedb_fin' => $datedb_fin,'datedenf_fin' => $datedenf_fin,'datedenf' => $datedenf,'auj_fin' => $auj_fin,'datedenf' => $datedenf,'entry' => $omra,
        'omrasupp' => $omrasupp,'omraprices' => $omraprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf,'info'=>$info));

    
    }
    
    
    
  
    
     public function reservationomraAction(Omra $omra)

    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $omraperiode = $session->get("omraprice");
        
        $mode = $this->getDoctrine()->getRepository('BtobHotelBundle:Payement')->find(1);
       $priceomra =   $this->getDoctrine()->getRepository('BtobOmraBundle:Omraprice')->find($omraperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
          
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
             
                 
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
           }
           
           }
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              
              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobOmraBundle:Omraprice')
                        ->calculomra($priceomra,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         $ArrBase[$key]['price'] = $prices+($prices*$omra->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $omraprice = $session->get("omraprice");
        
        
       
       // $nbchambre = count($nbad);

       //         exit(var_dump($resa));
        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);
/*
            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
*/
            $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');

           /* if ($testclient != null) {

                $client = $testclient;

            }*/

            $form->bind($request);
     
         
            if (($form->isSubmitted() && $form->isValid()) || $request->getSession()->get('client')!=null ) {
                if($request->request->get('emailpasso') != null){
                    
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($request->request->get('emailpasso'));
                if($client !=null){
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                $randpasswordLen = 10;
                $password = "";
                for ($i = 0; $i < $randpasswordLen; $i++) {
                    $password .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $randStringLen = 64;
                $salt = "";
                for ($j = 0; $j < $randStringLen; $j++) {
                    $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                }
                $options = ['cost' => 12, 'salt' => $salt];
                $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                $client->setSalt($salt);
				//exit(var_dump($salt));
                $em->flush();
                $to = $request->request->get('emailpasso');
				
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Changement mot de passe";
                $headers = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre nouveau mot de passe est: <strong>'.$password.'</strong><br><br>
				 <table width="90%"  cellspacing="1" border="0">';
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';               
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';
			
                mail($to, $subject, $message1, $headers);
                $request->getSession()->getFlashBag()->add('notimailomra', 'Votre Mot de passe a été envoyée au adresse email.');
                }else{
                $request->getSession()->getFlashBag()->add('notimailomraerror', 'Votre email n\'existe pas.');
                }
                }else{
                $session = $this->getRequest()->getSession();
                if($request->request->get('btob_hotelbundle_clients')['email']!=null){
                    
                    $email=$request->request->get('btob_hotelbundle_clients')['email'];
                    $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                     if($client == null){
                    $request->getSession()->getFlashBag()->add('notiomra', 'Votre demande a été bien envoyée.');
                    $p=$request->request->get('btob_hotelbundle_clients')['pays'];
                    $pays=$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($p);
                    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';
                    $randStringLen = 64;
                    $salt = "";
                    for ($i = 0; $i < $randStringLen; $i++) {
                        $salt .= $charset[mt_rand(0, strlen($charset) - 1)];
                    }
                    $options = ['cost' => 12, 'salt' => $salt];
                    $client= new Clients();
                    $password =$request->request->get('btob_hotelbundle_clients')['password'];
              //      exit(var_dump(password_hash($password, PASSWORD_BCRYPT, $options)));
                    $client->setPassword(password_hash($password, PASSWORD_BCRYPT, $options));
                    $client->setSalt($salt);
                    $client->setCiv($request->request->get('btob_hotelbundle_clients')['civ']);
                    $client->setCin($request->request->get('btob_hotelbundle_clients')['cin']);
                    $client->setName($request->request->get('btob_hotelbundle_clients')['name']);
                    $client->setPname($request->request->get('btob_hotelbundle_clients')['pname']);
             //       $client->setDatenaissance(new \DateTime($request->request->get('btob_hotelbundle_clients')['datenaissance']));
                    $client->setEmail($request->request->get('btob_hotelbundle_clients')['email']);
                    $client->setTel($request->request->get('btob_hotelbundle_clients')['tel']);
                    $client->setAdresse($request->request->get('btob_hotelbundle_clients')['adresse']);
                    $client->setCp($request->request->get('btob_hotelbundle_clients')['cp']);
                    $client->setVille($request->request->get('btob_hotelbundle_clients')['ville']);
                    $client->setPays($pays);
                    $client->setType(0);
                    $client->setAct(1);
                    $em->persist($client);
                    }else{
                         $request->getSession()->getFlashBag()->add('notimailomraerror', 'Votre email est deja existé.');
                    }
                }else{
                if($request->getSession()->get('client') == null){
                    $email = $request->request->get('email');
                }else{
                    $email=$request->getSession()->get('client');
                }
                $client=$this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
                if($client != null){
                    
                    if($request->getSession()->get('client') != null){
                        $request->getSession()->getFlashBag()->add('notiomra', 'Votre demande a été bien envoyée.');
                    }else{
                        $options = ['cost' => 12, 'salt' => $client->getSalt()];
                        $password =$request->request->get('password');
                        $pass=password_hash($password, PASSWORD_BCRYPT, $options);
                        if(password_hash($password, PASSWORD_BCRYPT, $options) == $client->getPassword()){
                            $request->getSession()->getFlashBag()->add('notiomra', 'Votre demande a été bien envoyée.');
                        }else{
                            $request->getSession()->getFlashBag()->add('notiomraerror', 'Adresse e-mail ou mot de passe incorrect.');
                            return $this->redirect($this->generateUrl('front_omra_reservation_homepage', array('id'=>$omra->getId())));
                        }
                    }
                    
                }
                $em->flush();
                
                $session = $this->getRequest()->getSession();
            $session->set('client', $client->getEmail());
            $session->set('fullname', $client->getPname());
                $resa=new Reservationomra();
                $resa->setUser($User);
                $resa->setClient($client);
                $resa->setOmra($omra);
                $resa->setOmraprice($priceomra);
                $resa->setTotal($total);
                $resa->setAvance($total*($omra->getPrixavance()));
                $resa->setEtat(1);
                $em->persist($resa);
                $em->flush();
            $ArrBase = array();
            $total=0;

       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
           
           $resadetail=new Reservationodetail();
           $resadetail->setReservationomra($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                 
                  
                
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               
           $resadetails=new Reservationodetail();
           $resadetails->setReservationomra($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
            
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               
               $resadetailb=new Reservationodetail();
           $resadetailb->setReservationomra($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobOmraBundle:Supplemento')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
             
                 
                  
                  
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobOmraBundle:Omraprice')
                        ->calculomra($priceomra,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          
          $ArrBase[$key]['price'] = $prices+($prices*$omra->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
	   
	   
	   $reservationdetails = $this->getDoctrine()->getRepository("BtobOmraBundle:Reservationodetail")->findByRess($resa->getId());   
 
            
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $resa->getClient()->getEmail();
               


                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Réservation Omra";
                $headers = "From: '".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			      
                $message1 .='
				 Bonjour Madame/Monsieur,<br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				 <table width="90%"  cellspacing="1" border="0">';
				 $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" width="170"><b style="padding-left:10px;">Période omra</b></td>';
                $message1 .= '<td height="30" width="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $omra->getTitre().' : '.$resa->getOmraprice()->getDated()->format('Y-m-d').' - '. $resa->getOmraprice()->getDates()->format('Y-m-d') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3">';
				$message1 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left:10px;">';
				$message1 .= '<tr>';
				$message1 .= '<td><strong>Chambre</strong></td>';
				$message1 .= '<td><strong>Type</strong></td>';
				$message1 .= '<td><strong>Nom & prénom</strong></td>';
				$message1 .= '<td><strong>Age</strong></td>';
				$message1 .= '<td><strong>Suppléments</strong></td>';
				$message1 .= '</tr>';
                foreach( $reservationdetails as $kc=>$valuc){
          
                if($valuc->getNamead()!= NULL){
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Adulte</td>';
				$message1 .= '<td>'. $valuc->getNamead(). ' ' .$valuc->getPrenomad() .'</td>';
				$message1 .= '<td>'. $valuc->getAgead() .'</td>';
				if($valuc->getSuppad()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppad() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
				}
                
                if($valuc->getNameenf()!= NULL){
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Enfant</td>';
				$message1 .= '<td>'. $valuc->getNameenf(). ' ' .$valuc->getPrenomenf() .'</td>';
				$message1 .= '<td>'. $valuc->getAgeenf() .'</td>';
				if($valuc->getSuppenf()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppenf() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
				}
                 
                
                if($valuc->getNameb()!= NULL){      
				$message1 .= '<tr>';
				$message1 .= '<td>'. $valuc->getChambre() .'</td>';
				$message1 .= '<td>Bébé</td>';
				$message1 .= '<td>'. $valuc->getNameb(). ' ' .$valuc->getPrenomb() .'</td>';
				$message1 .= '<td>'. $valuc->getAgeb() .'</td>';
				if($valuc->getSuppb()!= 'null'){
				$message1 .= '<td>'. $valuc->getSuppb() .'</td>';
                }else{
				$message1 .= '<td></td>';
				}
				$message1 .= '</tr>';
                }
				
				}
				   
                $message1 .= '</table></td></tr>';
				
				$message1 .= '<tr>';                
				$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';              
				$message1 .= '</tr>';
                $message1 .= '</table>';
				$message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
				
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =  $info->getNom(). ": Réservation omra" ;
                $header = "From:'".$info->getNom()."'<'".$info->getEmail()."'>\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';            			

				$message2 .='
                 <table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Commande :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" width="170"><b style="padding-left:10px;">Période omra </b></td>';
                $message2 .= '<td height="30" width="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $omra->getTitre().' : '.$resa->getOmraprice()->getDated()->format('Y-m-d').' - '. $resa->getOmraprice()->getDates()->format('Y-m-d') . '</td>';
                $message2 .= '</tr>';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civilité</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' .$client->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getPays() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $client->getAdresse() . '</td>';
                $message2 .= '</tr>';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3">';
				$message2 .= '<table width="90%" border="0" cellpadding="0" cellspacing="0" style="margin-left:10px;">';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><strong>Chambre</strong></td>';
				$message2 .= '<td height="30"><strong>Type</strong></td>';
				$message2 .= '<td height="30"><strong>Nom & prénom</strong></td>';
				$message2 .= '<td height="30"><strong>Age</strong></td>';
				$message2 .= '<td height="30"><strong>Suppléments</strong></td>';
				$message2 .= '</tr>';
				
				foreach( $reservationdetails as $kc=>$valuc){
                       
                
                if($valuc->getNamead()!= NULL){
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Adulte</td>';
				$message2 .= '<td height="30">'. $valuc->getNamead(). ' ' .$valuc->getPrenomad() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgead() .'</td>';
				if($valuc->getSuppad()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppad() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
				}
                
                if($valuc->getNameenf()!= NULL){				
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Enfant</td>';
				$message2 .= '<td height="30">'. $valuc->getNameenf(). ' ' .$valuc->getPrenomenf() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgeenf() .'</td>';
				if($valuc->getSuppenf()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppenf() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
				}
                 
                
                if($valuc->getNameb()!= NULL){      
				$message2 .= '<tr>';
				$message2 .= '<td height="30">'. $valuc->getChambre() .'</td>';
				$message2 .= '<td height="30">Bébé</td>';
				$message2 .= '<td height="30">'. $valuc->getNameb(). ' ' .$valuc->getPrenomb() .'</td>';
				$message2 .= '<td height="30">'. $valuc->getAgeb() .'</td>';
				if($valuc->getSuppb()!= 'null'){
				$message2 .= '<td height="30">'. $valuc->getSuppb() .'</td>';
                }else{
				$message2 .= '<td height="30"></td>';
				}
				$message2 .= '</tr>';
                }
				 
                };
				   
                $message2 .= '</table></td></tr>';
				
				$message2 .= '<tr>';                
				$message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';               
				$message2 .= '</tr>';
                $message2 .= '</table>';
				$message2 .= '</body>';


                //mail($too, $subjects, $message2, $header);
                
               

            
                $request->getSession()->getFlashBag()->add('notiomraf', 'Votre demande a été bien envoyée.');

                $session->set('reservation', $resa);
                $session->set('ArrBase', $ArrBase);
                $session->set('paiement', $request->request->get('paiement'));

                $my_session_id = session_id();
        

                return $this->render('FrontBtobBundle:Omra:validation.html.twig', array(
                        'paiement' => $request->request->get('paiement'),
                        'reservation' => $resa,
                        'my_session_id' => $my_session_id,
                        'entry'=>$omra,
                        'recap'=>$recap,
                        'ArrBase'=>$ArrBase,
                        'total'=>$total,
                        'client' => $resa->getClient(),
                       
                    )
                );
            }
        }
} else { 
                echo $form->getErrors();
            }


        }

        return $this->render('FrontBtobBundle:Omra:reservation.html.twig', array(
            'form' => $form->createView(),
            'entry'=>$omra,
            'recap'=>$recap,
            'ArrBase'=>$ArrBase,
            'total'=>$total, 
            'mode' => $mode,
            'info'=>$info
            ));

    }




    public function personalizeAction(Omra $omra)
    {
            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
         $em = $this->getDoctrine()->getManager();
         $client = new Clients();
         $form = $this->createForm(new ClientsType(), $client);
         
        $User = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('MONSITEWEB');
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                
                 $dated = $request->request->get('dated');
                 
                $dater = $request->request->get('dater');
                $pays = $request->request->get('pays');
                

                $ville = $request->request->get('villes');
                
                                
                
                $typec = $request->request->get('typec');
                $formule = $request->request->get('formule');
                $transfert = $request->request->get('transfert');
                $excursion = $request->request->get('excursion');
                $nbradult = $request->request->get('nbradult');
                $nbrenf = $request->request->get('nbrenf');
                $nbrbebe = $request->request->get('nbrbebe');
                $nbrch = $request->request->get('nbrch');
                $budgetmin = $request->request->get('budgetmin');
                $budgetmax = $request->request->get('budgetmax');
                $remarque = $request->request->get('remarque');

                $personalisation = new Personalisationomra();
                $personalisation->setClient($client);
                $personalisation->setOmra($omra);
                $personalisation->setAgent($User);
                
                
    
                $personalisation->setTypec($typec);
                
                $personalisation->setFormule($formule);
                
                $personalisation->setTransfert($transfert);
                $personalisation->setExcursion($excursion);
                $personalisation->setNbradult($nbradult);
                $personalisation->setNbrenf($nbrenf);
                $personalisation->setNbrbebe($nbrbebe);
                $personalisation->setNbrch($nbrch);
                $personalisation->setBudgetmin($budgetmin);
                $personalisation->setBudgetmax($budgetmax);
                $personalisation->setRemarque($remarque);
                $em->persist($personalisation);
                $em->flush();

                
                 foreach( $pays as $key=>$value){
                    $catg = new Tpomra();
                    $catg->setPersonalisationomra($personalisation);
                    $catg->setVille($ville[$key]);
                    $catg->setPays($pays[$key]);
                    if($request->request->get('dateflex'.$key)=="on")
                {
                    $catg->setDateflex(true);
                }
                else{
                    
                    $catg->setDateflex(false);
                }
                    
                    $catg->setDated(new \Datetime(Tools::explodedate($dated[$key],'/')));
                    $catg->setDater(new \Datetime(Tools::explodedate($dater[$key],'/')));
                    $em->persist($catg);
                    $em->flush();

                }
                
                
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $personalisation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation Séjour à l'étranger personnalisé" ;
                $headers = "From:'".$info->getNom()."' <'".$info->getEmail()."'> \n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .='
				 <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							    <td align="right"><br></td>
							   </tr>
							  </table><br>
				  Bonjour,<br>
				  Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>';
                $message1 .= 'Cordialement.';

                $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation Séjour à l'étranger personnalisé " ;
                $header = "From:'".$info->getNom()."' <".$to.">\n";
                $header .= "Reply-To:" .$personalisation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';
                $message2 .='
                  <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							    <td align="right"><br></td>
							   </tr>
							  </table><br>
				 Bonjour,<br>
				 Vous avez reçu une réservation Voyages organisés  , merci de consulter votre backoffice .

                 <a href="http://www.afritours.com.tn/b2b/'.$personalisation->getId().'/detail/personalisation"> Cliquer Içi</a>';
				 

                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#fcdb0d" style="color:#fff;">"'.
				 $info->getAdresse().'" - <a href="mailto:'.$info->getEmail().'" style="color:#fff;">"'.$info->getEmail().'"</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body><br>';



                mail($too, $subjects, $message2, $header); 
                //mail('afef.tuninfo@gmail.com', $subjects, $message2, $header); 
                
                
                
                      $request->getSession()->getFlashBag()->add('notiomraf', 'Votre message a été bien envoyé. Merci.');


                return $this->redirect($this->generateUrl('front_omra_homepage'));

          $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client); 
            }
        }

        return $this->render('FrontBtobBundle:Omra:personalize.html.twig', array('entry' => $omra, 'form' => $form->createView(),
          'info'=>$info
      ));
    }
         

}
