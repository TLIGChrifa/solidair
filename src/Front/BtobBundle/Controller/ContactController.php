<?php

namespace Front\BtobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\AgenceBundle\Entity\Info;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\VoleBundle\Entity\Vol;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\HttpFoundation\Request;
class ContactController extends Controller 
{
    public function indexAction(){

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $request = $this->get('request');
        if ($request->getMethod() == 'POST')
        {

            $session = $this->getRequest()->getSession();
            $nom = $request->request->get("nom");
            $tel = $request->request->get("tel");
            $email = $request->request->get("email");
            $sujet = $request->request->get("sujet");
            $message = $request->request->get("Message");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');


            $to = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  " Contact:".$sujet ;
            $headers = "From:" .$info->getNom()."<" .$info->getEmail(). ">\n";
            $headers .= "To:" .$nom."<" .$email. ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='<img src="'.$img_logo.'" /><br><br>';
			$message1 .='<table width="90%"  cellspacing="1" border="0">';
              
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;">Contact</b></td>';
			$message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" width="30%"><b style="padding-left:10px;">Nom</b></td>';
            $message1 .= '<td height="30" width="30"><b>:</b></td>';
            $message1 .= '<td height="30">'.$nom.'</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" width="30%"><b style="padding-left:10px;">Tél</b></td>';
            $message1 .= '<td height="30" width="30"><b>:</b></td>';
            $message1 .= '<td height="30">'.$tel.'</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" width="30%"><b style="padding-left:10px;">E-mail</b></td>';
            $message1 .= '<td height="30" width="30"><b>:</b></td>';
            $message1 .= '<td height="30">'.$email.'</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" width="30%"><b style="padding-left:10px;">Message</b></td>';
            $message1 .= '<td height="30" width="30"><b>:</b></td>';
            $message1 .= '<td height="30">'.$message.'</td>';
            $message1 .= '</tr>';
              
            $message1 .= '</table>';
              
            $message1 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
            $message1 .= '</tr>';

            $message1 .= '</table>';
            $message1 .= '</body>';
		
			mail($to, $subject, $message1, $headers);
			mail('abdallah.dgperformance@gmail.com', $subject, $message1, $headers);

            $request->getSession()->getFlashBag()->add('noticcontact', 'Votre message a bien été envoyé. Merci.');
            return $this->redirect($this->generateUrl('front_contacts_homepage'));  
		}
        return $this->render('FrontBtobBundle:Contact:index.html.twig', array(
            'info' => $info));
    }
    public function demandeAction(){

            $info = $this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
            $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
        $request = $this->get('request');
        if ($request->getMethod() == 'POST')
        {

            $session = $this->getRequest()->getSession();
            $agence = $request->request->get("agence");
            $responsable = $request->request->get("responsable");
            $poste = $request->request->get("poste");
            $email = $request->request->get("email");
            $tel = $request->request->get("tel");
            $fax = $request->request->get("fax");
            $adresse = $request->request->get("adresse");
            $cp = $request->request->get("cp");
            $pays = $request->request->get("pays");


            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $to = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  " Demande d'affiliation:";
            $headers = "From" .$info->getNom().":" .$info->getEmail(). "\n";
            $headers = "To" .$agence.":" .$email. "\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='<img src="'.$img_logo.'" /><br><br>
						Bonjour,<br>
						Vous avez reçu une demande d\'affiliation<br><br>
						<b>Nom de l\'agence:</b> '.$agence.'<br>
						<b>Responsable:</b> '.$responsable.'<br>
						<b>Poste:</b> '.$poste.'<br>
						<b>E-mail:</b> '.$email.'<br>
						<b>Téléphone:</b>'.$tel.'<br>
						<b>Fax:</b> '.$fax.'<br>
						<b>Adresse:</b> '.$adresse.'<br>
						<b>Code postal:</b> '.$cp.'<br>
						<b>Pays:</b> '.$pays.'<br>


				</body>';

            //mail($to, $subject, $message1, $headers);
            mail($info->getEmail(), $subject, $message1, $headers);
            mail('abdallah.dgperformance@gmail.com', $subject, $message1, $headers);




            $request->getSession()->getFlashBag()->add('noticcontact', 'Votre demande a été bien envoyée. Merci.');
            return $this->redirect($this->generateUrl('front_inscription_homepage')); 
		}
        return $this->render('FrontBtobBundle:Contact:inscription.html.twig', array(
            'info' => $info)
        );
	}
}
