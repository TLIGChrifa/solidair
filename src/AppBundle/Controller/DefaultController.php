<?php

namespace AppBundle\Controller;
use Btob\CuircuitBundle\Entity\Coordonneescr;
use Btob\CuircuitBundle\Form\CoordonneescrType;
use Btob\CuircuitBundle\Entity\Coordonneesreservationcr;
use Btob\CuircuitBundle\Form\CoordonneesreservationcrType;
use Btob\CuircuitBundle\Entity\accompagnantsreservationciruit;
use Btob\SejourBundle\Entity\Accompagnantsreservationvo;
use Btob\HotelBundle\Entity\AccompagnantsChambreshotel;
use Btob\SejourBundle\Entity\coordonneesvoyageo;
use Btob\SejourBundle\Form\CoordonneesvoyageoType;
use Btob\SejourBundle\Entity\Reservationsejour;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\CuircuitBundle\Form\ReservationcircuitType;
use Btob\CuircuitBundle\Form\ReservationcircuitfrontType;
use Btob\SejourBundle\Entity\datessuplimentairesvo;
use Btob\HotelBundle\Entity\Week;
use Btob\AgenceBundle\Entity\Info;
use Btob\HotelBundle\Entity\Billetriecoordonnes;
use Btob\HotelBundle\Entity\chambre;
use Btob\HotelBundle\Entity\Coordonneesmaisondhote;
use Btob\HotelBundle\Entity\Coordonneesreservationhotel;
use Btob\HotelBundle\Entity\Coordonneesvo;
use Btob\HotelBundle\Entity\Destionationbilletrie;
use Btob\HotelBundle\Entity\Hotel;
use Btob\HotelBundle\Entity\Maisondhote;
use Btob\HotelBundle\Entity\Passagersbilletrie;
use Btob\HotelBundle\Entity\ReservationHotel;
use Btob\HotelBundle\Entity\Reservationmaisondhote;
use Btob\HotelBundle\Form\ReservationHotelpayerType;
use Btob\HotelBundle\Form\ReservationHotelType;
use Btob\HotelBundle\Form\CoordonneesreservationhotelType;
use Btob\HotelBundle\Form\CoordonneesvoType;
use Btob\SejourBundle\Entity\Sejour;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeZone;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
         $villes =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:Ville','v')
        ->getQuery()
        ->getResult();
            $bannaire =    $em->createQueryBuilder()
            ->select('b')
            ->from('BtobBannaireBundle:Bannaire','b')
            ->where('b.act = 1')
            ->getQuery()
            ->getResult();
            //dump($bannaire).die;
            $hotels =    $em->createQueryBuilder()
            ->select('h')
            ->from('BtobHotelBundle:Hotel','h')
            ->where('h.act = 1')
            //->setParameter('reservation', $reservation)
            ->getQuery()
            ->getResult();
        //     $paginator  = $this->get('knp_paginator');
        //     $pagination = $paginator->paginate(
        //      $hotels,
        //     $request->query->getInt('page', 1),
        //     $request->query->getInt('limit',8)
        // );
        $maisons = $this->getDoctrine()->getRepository("BtobHotelBundle:Maisondhote")->findAll();
        $maisonsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Maisonhoteimg','i')
            ->groupBy('i.maisondhote')  
            ->getQuery()
            ->getResult();
              $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $periodevalide = array();
            $contrats = array();
            $capacityrooms = array();
            $namebycapacity = array();
            $nameroomofhotel = array();
           // $alldates = array();    
            foreach($hotels as $hotel){
                //var_dump($hotel->getId()).die;
                foreach($hotel->getHotelroom() as $r){
                     $capacityrooms[] = $r->getRoom()->getCapacity();
                     $nameroomofhotel[] = $r->getRoom()->getName();
                     
                }
                foreach($hotel->getHotelroom() as $r){
                    if($r->getRoom()->getCapacity() == min($capacityrooms) and in_array($r->getRoom()->getName(), $nameroomofhotel))
                    $namebycapacity = $r->getRoom()->getName();
                    
                    
               }
                      $hotelprice =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $hotel)
                            // ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
                
                    
                    
             $alldates = array();            
            $arrayc = array();
           
          // var_dump(count($arrayc)).die;
                      foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
             }
         //  var_dump($arrayc).die;
            foreach ($hotelprice as $key=>$valuec) {
              //   var_dump($valuec->getHotel()->getId()).die;
           if(count($arrayc) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
                 if(!empty($datesvalides) ){
                    $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dates = :dates')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                              ->setparameter('dates', min($datesvalides))
                              ->getQuery()
                              ->getResult();      
                 }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                           //  ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
               
           }
            }
          
              
                 
             
        //  var_dump($arraymerge).die;
             if(!is_null($hotelpriceg) ){
                foreach ($hotelpriceg as $key=>$value) {
                if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
                  //  if( $value->getHotel()->getDated())
                      $periodevalide[] =  $value->getHotel()->getId();
                             //  $datesvalides[] = $value->getHotel()->getDated();
                           //  $datesvalides[] = $hotelprice->getdated();
                             $contrats[]= $value;
                }
                
                }
             }
               
               
            }
          
              
            
            
            
            
            
            
            
            $allhotelprice =array();
          //  dump($promos).die;
            $hotelsenpromos = array();
            $allhotelprice[] =  $em->createQueryBuilder()
            ->select('p')
            ->from('BtobHotelBundle:Hotelprice','p')
           // ->where('p.hotel = :hotel') 
            ->where('p.dates > :datedujour') 
           // ->setParameter('hotel', $h)
            ->setParameter('datedujour', new DateTime())
           // ->groupby('p.hotel')
            ->getQuery()
            ->getResult();
          //  dump($allhotelprice).die;
            foreach($allhotelprice as $p){
                foreach($p as $pr){
                    if(!is_null($pr->getPrice())){
                        $hotelsayantsdescontrats[] = $pr;
                    }
                     
                }
                
            }
             foreach ($hotels as $h){
       //     $allhotelprice[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $h));
         
               //  dump( $h->getPromotion()).die;
              // dump($allhotelprice).die;
             $promos[] =  $em->createQueryBuilder()
             ->select('p')
             ->from('BtobHotelBundle:Promotion','p')
             ->where('p.act = 1')
             ->andwhere('p.hotel = :hotel') 
             ->setParameter('hotel', $h)
             ->getQuery()
             ->getResult();
              
             }
             foreach($promos as $p){
                 foreach($p as $pr){
                      $hotelsenpromos[] = $pr->getHotel();
                 }
                 
             }
            
         // dump($hotelsenpromos).die;
            $hotelsimg =  $em->createQueryBuilder()
                ->select('i')
                ->from('BtobHotelBundle:Hotelimg','i')
                ->groupBy('i.hotel')  
                ->getQuery()
                ->getResult();
            $VO =    $em->createQueryBuilder()
                ->select('s')
                ->from('BtobSejourBundle:Sejour','s')
                ->where('s.act = 1')
                ->getQuery()
                ->getResult();
            // $paginatorvo  = $this->get('knp_paginator');
            // $paginationvo = $paginatorvo->paginate(
            //  $VO,
            // $request->query->getInt('page', 1),
            // $request->query->getInt('limit',4)
       // );
           // $VO = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findAll();
            $voimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Imgesej','i')
            ->groupBy('i.sejour')  
            ->getQuery()
            ->getResult();
            $circuit =   $em->createQueryBuilder()
            ->select('c')
            ->from('BtobCuircuitBundle:Cuircuit','c')
            ->where('c.active = 1')
            ->getQuery()
            ->getResult();
            $imgcuir = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobCuircuitBundle:Imgcuir','i')
            ->groupBy('i.cuircuit')  
            ->getQuery()
            ->getResult();
            $blog = $em->getRepository('BtobDashBundle:Blog')->findAll();
            $room =   $em->createQueryBuilder()
                ->select('r')
                ->from('BtobHotelBundle:Room','r')
                ->where('r.act = 1')
                ->getQuery()
                ->getResult();
            
              if ($request->getMethod() == 'POST') {
                //   $nbstars = 0;
                //   if($request->request->get('troiset') == "on" ){
                //       $nbstars = 3;
                      
                //   }
                //   if($request->request->get('quatret') == "on" ){
                //       $nbstars = 4;
                      
                //   }
                //   if($request->request->get('cinquet') == "on" ){
                //       $nbstars = 5;
                      
                //   }
                // var_dump($request->request->get("star")).die;
                 // var_dump($nbstars).die;
                     $dated = new DateTime($request->request->get('dated'));
                  //   var_dump($dated).die;
                     $datef = new DateTime($request->request->get('datef'));
//var_dump($request->request->get("star")).die;
                 return $this->redirect($this->generateUrl('rechercheht', array('destination' =>$request->request->get('destination'),'dated'=>$dated->format('Y-m-d'),'datef'=>$datef->format('Y-m-d'),'chambres'=>$request->request->get('chambres'),'occ' =>$request->request->get('occ'),'nbstars' =>$request->request->get("star"))));

                  
            // return $this->redirect($this->generateUrl('rechercheht', array('destination' =>$request->request->get('destination'),'dated'=>$request->request->get('dated'),'datef'=>$request->request->get('datef'),'chambres'=>$request->request->get('chambres'),'occ' =>$request->request->get('occ'),'troiset' =>$request->request->get('troiset'),'quatret' =>$request->request->get('quatret'),'cinquet' =>$request->request->get('cinquet'))));


         }
   // dump($hotelsayantsdescontrats).die;
        return $this->render('default/index.html.twig', [
           'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
         //  'hotels'=>$pagination,
         'maison'=>$maisons,
         'imgmaison'=>$maisonsimg,
          'hotels'=>$hotels,
           'contrats'=>$contrats,
           'img'=>$hotelsimg,
           'sejourimg'=>$voimg,'room'=>$room,
           'imgcuir'=>$imgcuir,
           'blog'=>$blog,
            'vo'=>$VO ,
            'villes'=>$villes,
            'circuit'=>$circuit,
            'bannaire'=>$bannaire,
            'hotelsenpromos'=>$hotelsenpromos,
            'hotelsprice'=>$hotelsayantsdescontrats,
        ]);
    }
    
     /**
     * @Route("/rechercheht/{destination}/{dated}/{datef}/{chambres}/{occ}/{{nbstars}}", name="rechercheht")
     */
    public function recherchehtAction($destination ,$nbstars, $duree,$dated,$occ,$datef,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       // $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));
//var_dump($nbstars).die;
        $hotels =    $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1')
        ->getQuery()
        ->getResult();
        $hotelsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Hotelimg','i')
            ->groupBy('i.hotel')  
            ->getQuery()
            ->getResult();
                       
            $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $periodevalide = array();
            $contrats = array();
            $capacityrooms = array();
            $namebycapacity = array();
            $nameroomofhotel = array();
           
            foreach($hotels as $hotel){
                foreach($hotel->getHotelroom() as $r){
                     $capacityrooms[] = $r->getRoom()->getCapacity();
                     $nameroomofhotel[] = $r->getRoom()->getName();
                     
                }
                foreach($hotel->getHotelroom() as $r){
                    if($r->getRoom()->getCapacity() == min($capacityrooms) and in_array($r->getRoom()->getName(), $nameroomofhotel))
                    $namebycapacity = $r->getRoom()->getName();
                    
                    
               }
             $hotelprice =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $hotel)
                            // ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
                
                    
                    
            $alldates = array();         
            $arrayc = array();
             foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
             }
         //  var_dump($arrayc).die;
            foreach ($hotelprice as $key=>$valuec) {
              //   var_dump($valuec->getHotel()->getId()).die;
           if(count($arrayc) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
                 if(!empty($datesvalides) ){
                    $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dates = :dates')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                              ->setparameter('dates', min($datesvalides))
                              ->getQuery()
                              ->getResult();      
                 }
                     
                // }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                           //  ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
               
           }
            }
          
              
                 
             
    

                foreach ($hotelpriceg as $key=>$value) {
                  
                if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
                  //  if( $value->getHotel()->getDated())
                      $periodevalide[] =  $value->getHotel()->getId();
                    //  var_dump($periodevalide).die;
                             //  $datesvalides[] = $value->getHotel()->getDated();
                           //  $datesvalides[] = $hotelprice->getdated();
                             $contrats[]= $value;
                }
                
                
                }
                
               
             
            }
               
               
       
        $villes =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:Ville','v')
        ->getQuery()
        ->getResult();
        $arragement =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:arrangement','v')
        ->getQuery()
        ->getResult();
         
        $room =   $em->createQueryBuilder()
        ->select('r')
        ->from('BtobHotelBundle:Room','r')
        ->getQuery()
        ->getResult();
           if ($request->getMethod() == 'POST') {

            $request->request->get("dated");
         //   dump( $request->request->get("ville")).die;
            $recherche =   $em->createQueryBuilder()
            ->select('h')
            ->from('BtobHotelBundle:Hotel','h')
            //->where('h.act = 1' )
           // ->where('h.ville = :ville')
           // ->andwhere('h.name = :name')
            ->andwhere('h.maxenfant >= :maxenfant')
            ->andwhere('h.star = :star')
          //  ->setParameter('ville', $request->request->get("ville"))
          //  ->setParameter('name', '%'.$request->request->get("hotels").'%')
            ->setParameter('maxenfant', $request->request->get("enfants"))
            ->setParameter('star', $request->request->get("star"))
            ->getQuery()
            ->getResult();
            $dated = new DateTime($request->request->get("dated"));
             $datef = new DateTime($request->request->get("datef"));
       // var_dump($request->request->get("star")).die;
            return $this->redirect($this->generateUrl('recherchehotel', array("nom" => $request->request->get("hotels"),"ville" => $request->request->get("ville"),"enfants" => $request->request->get("enfants"),"arr" => $request->request->get("arr"),"stars" => $request->request->get("star"),"dated"=>$dated->format('Y-m-d'),"datef"=>$datef->format('Y-m-d'))));

   
        }
         $hotelarrang =   $em->createQueryBuilder()
        ->select('a.id')
        ->from('BtobHotelBundle:hotelarrangement','a')
        ->where('a.arrangement = :arr')
         ->andwhere('a.hotel = :hotelid')
        ->setParameter('arr', $arr)
        ->setParameter('hotelid', $nom)
        ->getQuery()
        ->getOneOrNullResult();
        
        $arrang =   $em->createQueryBuilder()
        ->select('ar')
        ->from('BtobHotelBundle:hotelarrangement','ar')
        ->where('ar.arrangement = :arr')
        ->setParameter('arr', $arr)
        ->getQuery()
        ->getResult();
        $hotelswitharr = array();
        foreach($arrang as $arg){
           $hotelswitharr[]= $arg->getHotel()->getId();
        }
        
      //  var_dump($hotelswitharr).die;
             $dated = new DateTime($dated);
             $datef = new DateTime($datef);
            // var_dump($nom).die;
        foreach($contrats as $c){
        //  var_dump($c->getDates()).die;
        

        if( $c->getDated()<= $dated and $c->getDates()>= $datef  ){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
        ->andwhere('h.ville = :ville')
       // ->andwhere('h.id = :name')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
        ->setParameter('ville', $destination)
       // ->setParameter('name', $nom)
        ->setParameter('maxenfant', $occ)
        ->setParameter('star', $nbstars)
        ->getQuery()
        ->getResult();
             }
         
           
       
        }
    
          $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
             $recherche,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',5)
        );


        return $this->render('default/listehotelsrecherche.html.twig', array(
           'hotels' => $pagination,
              'listehotels' => $hotels,
               'dated' => $dated,
                'ville' => $ville,
                'enf'=>$enfants,
                'nom'=>$nom,
                'stars'=>$stars,
             'datef' => $datef,
             'img'=>$hotelsimg,
             'room'=>$room,
            'arr'=> $arr,
            'periodevalide'=>$periodevalide,
            'contrats'=>$contrats,
            'villes'=>$villes,
            'arragement'=>$arragement,
            'allarrangement'=>$allarrangement,
            'marcher' => $marcher,
          //  'datesvalides'=>min($datesvalides),
            'mincapacity'=>min($capacityrooms),
            'namebycapacity'=>$namebycapacity
        ));
    }
    
    /**
     * @Route("/locationvoiture", name="locationvoitures")
     */
    public function listevoituresAction(Request $request)
    {
      return $this->render('default/locationvoiture.html.twig', [
       
      
     ]);
    }


    /**
     * @Route("/croisier", name="croisier")
     */
    public function listecroisierAction(Request $request)
    {
      return $this->render('default/croisier.html.twig', [
       
      
     ]);
    }
    

/**
  
     * @Route("/detailshotel/{id}", name="detailshotel")
     */
    public function detailshotelAction(Hotel $hotel ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $hotels =    $em->createQueryBuilder()
            ->select('h')
            ->from('BtobHotelBundle:Hotel','h')
            ->where('h.act = 1')
            ->getQuery()
            ->getResult();

            $hotelsimg =  $em->createQueryBuilder()
                ->select('h')
                ->from('BtobHotelBundle:Hotelimg','h')
                ->where('h.hotel = :hotel')
                ->setParameter('hotel', $hotel)

               // ->groupBy('i.hotel')  
                ->getQuery()
                // ->getOneOrNullResult ();
                ->getResult();
    // dump($voimg).die;
        return $this->render('default/detailshotel.html.twig', [
           'hotels' => $hotels,
           'marche' => $marche,
           'img'=>$hotelsimg,
           'h' => $hotel,
         
        ]);
    }

        /**
     * @Route("/coordonnees/{id}/{{dated}}/{{datef}}", name="coordonnes")
     */
    public function coordonneesAction(ReservationHotel $reservation ,$dated,$datef,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
          $payement = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Payement')
            ->find(1);
            
        $chambre =  $em->createQueryBuilder()
            ->select('c')
            ->from('BtobHotelBundle:Chambre','c')
            ->where('c.reservationhotel = :reservation')
            ->setParameter('reservation', $reservation)
            ->getQuery()
            ->getResult();
        $hotelsimg =  $em->createQueryBuilder()
                ->select('i')
                ->from('BtobHotelBundle:Hotelimg','i')
                ->where('i.hotel = :hotel') 
                 ->setParameter('hotel', $reservation->getHotel())
                ->getQuery()
                ->getResult();
        $coordonnees = new Coordonneesreservationhotel();
        
        $form = $this->createForm('Btob\HotelBundle\Form\CoordonneesreservationhotelType', $coordonnees);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
//   var_dump($request->request->get('options')).die;
            $coordonnees->setReservationhotel($reservation);
            $reservation->setTypepayement($request->request->get('group'));
            $em->persist($coordonnees);
            $em->flush();
           // var_dump($request->request->get('nomadulte')).die;
           for ($i = 0; $i < count($chambre); $i++) {
                             
                         $accompagnant = new AccompagnantsChambreshotel();
                         $accompagnant->setNomadulte($request->request->get('nomadulte')[$i]);
                         $accompagnant->setAgeadulte($request->request->get('ageadulte')[$i]);
                         $accompagnant->setNomenfant($request->request->get('nomenf')[$i]);
                         $accompagnant->setAgeenfant($request->request->get('ageenf')[$i]);
                         $accompagnant->setChambre($request->request->get('idchambre'));
                         $accompagnant->setReservationhotel($reservation->getId());
                         $accompagnant->setRecommandations($request->request->get('options'));
                         $em->persist($accompagnant);
                          $em->flush();
                          } 
            
            
            
            
            
          $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation à '.$reservation->getHotel()->getName().', Référence:'.$reservation->getId())
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('reservation@rusticavoyages.com')
                        ->setBody("Nouvelle réservation à ".$reservation->getHotel()->getName()." à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()."  Le total de la réservation est de " .$reservation->getTotal()." TND . veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
           $this->addFlash("success", "Votre réservation à été faite avec succès ..");

                    return $this->redirect($this->generateUrl('hotelsentunise'));
      
        }
         
        
        
        
   
        return $this->render('default/coordonnees.html.twig', [
           
           'reservation'=>$reservation,
           'chambres' => $chambre,'payement' => $payement,'hotelsimg'=>$hotelsimg,
           'form' => $form->createView(), 'dated' => $dated, 'datef' => $datef,
           
         
        ]);
    }
    
     /**
     * @Route("/chambres/{id}/{marcheid}/{{dated}}/{{datef}}", name="chambres")
     */
    public function chambresAction(Hotel $hotel ,$marcheid ,$dated,$datef,Request $request)
    {
           $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
           $em = $this->getDoctrine()->getManager();
           $hotelsimg =  $em->createQueryBuilder()
                ->select('i')
                ->from('BtobHotelBundle:Hotelimg','i')
                ->where('i.hotel = :hotel') 
                 ->setParameter('hotel', $hotel)
                ->getQuery()
                ->getResult();
         
         //var_dump($hotel->getId()).die;
         
            $hotelprice =  $em->createQueryBuilder()
                 ->select('c')
                 ->from('BtobHotelBundle:Hotelprice','c')
                 ->where('c.marcher = :marche')
                 ->andwhere('c.hotel = :hotel')
                 ->setparameter('marche', 1)
                 ->setparameter('hotel', $hotel)
                 ->getQuery()
                 ->getResult();    
                
                    
                 
            $alldates = array();         
            $arrayc = array();
             foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
                 //var_dump($v->getDates()).die;
             }
            foreach ($hotelprice as $key=>$valuec) {
                //var_dump(count($arrayc)).die;
           if(count($hotelprice) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
               // var_dump(new \DateTime($dated), new \DateTime($datef)).die;
                //  if(!empty($datesvalides) ){
                    // $hotelpriceg =  $em->createQueryBuilder()
                    //          ->select('c')
                    //          ->from('BtobHotelBundle:Hotelprice','c')
                    //           ->where('c.marcher = :marche')
                    //           ->andwhere('c.hotel = :hotel')
                    //           ->andwhere('c.dates = :dates')
                    //           ->andwhere('c.price IS NOT NULL') 
                    //           ->andwhere('c.dates >= :d') 
                              
                    //          ->setparameter('marche', 1)
                    //          ->setparameter('hotel', $hotel)
                    //          ->setparameter('dates', min($datesvalides))
                    //          ->setparameter('d', new \DateTime('now'))
                    //          ->getQuery()
                    //          ->getOneOrNullResult(); 
                       $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dated <= :dated')
                              //->andwhere('c.dates >= :dated')
                              ->andwhere('c.dates >= :datef')
                             // ->andwhere('c.dated <= :datef')
                               ->andwhere('c.price IS NOT NULL') 
                               ->andwhere('c.dates >= :d') 
                              
                             ->setparameter('marche', 1)
                             ->setparameter('hotel', $hotel)
                             ->setparameter('dated', new \DateTime($dated))
                             ->setparameter('datef', new \DateTime($datef))
                             ->setparameter('d', new \DateTime('now'))
                             // ->setMaxResults(1)
                             ->getQuery()
                             ->getOneOrNullResult();  
                             
                //  }
                
                // }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                               ->andwhere('c.price IS NOT NULL') 
                               ->andwhere('c.dates >= :d') 
                             ->setparameter('marche', 1)
                             ->setparameter('hotel', $hotel)
                             ->setparameter('d', new \DateTime('now'))
                             ->groupBy('c.hotel')  
                             ->setMaxResults(1)
                             ->getQuery()
                             ->getOneOrNullResult(); 
               
           }
           //
            }
             
              
   //  var_dump($hotelpriceg->getDated(),$hotelpriceg->getDates()) .die;

        // foreach ($hotelpriceg as $key=>$value) {
        //           //var_dump($value).die;
        //      if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
        //   //   if(  (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){

        //           //  if( $value->getHotel()->getDated())
        //               $periodevalide[] =  $value->getHotel()->getId();
        //             //  var_dump($periodevalide).die;
        //                      //  $datesvalides[] = $value->getHotel()->getDated();
        //                   //  $datesvalides[] = $hotelprice->getdated();
        //                      $contrats[]= $value;
        //         }      
         
         
        //         }
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
   $price = array();
   $date = array();
   foreach ($hotel->getHotelprice() as $value) {
       if ($value->getMarcher()->getId() == $marche->getId()) {
           
           $date[] = $value->getDcr();
           $price[] = $value;
       }
   }
   
   
      
   
   
   $arraydater = array();
    $datert = new DateTime();
   $dr= $datert->modify('+'. $hotelpriceg->getRetro().' day');
 
   $period = new DatePeriod(
       new DateTime(),
       new DateInterval('P1D'),
       new DateTime($dr->format('m/d/Y'))
   );
   
   //var_dump($period).die;
   foreach ($period as $key => $value) {
      $arraydater[] = $value->format('d-m-y') ;      
   }
   
   
   //var_dump($arraydater).die;
//var_dump($arraydater).die;
           $allpriceroom = array();
           $allpricesupplement = array();
           $allpricechild = array();
           $allpricearr = array();
           $allhotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $hotel));
           $allroom = array();
           $allqterooms = array();
           $allsupplement = array();
           $allchild = array();
           $allarrangement = array();
           if (!empty($hotelpriceg)){
               $allpriceroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('hotelprice' => $allhotelprice[0]));
               $allpricesupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->findBy(array('hotelprice' => $hotelpriceg));
               $allpricechild = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findBy(array('hotelprice' => $allhotelprice[0]));
               $allpricearr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findBy(array('hotelprice' =>$hotelpriceg));
               $i = 0;
               foreach ($allpriceroom as $onepriceroom) {
                   $allroom[$i] = $onepriceroom->getRoom();
                   $allqterooms[$i] = $onepriceroom->getQte();
                   $allids[$i] = $onepriceroom->getid();
                   $onepriceroom->setQte($onepriceroom->getQte()-1);
                   $i = $i + 1;
               }
               //var_dump($allhotelprice[0]).die;  
               $i = 0;
               foreach ($allpricesupplement as $onepricesupplement) {
                   $allsupplement[$i] = $onepricesupplement;
                   $i = $i + 1;
               }
               $i = 0;
               foreach ($allpricechild as $onepricechild) {
                   if (! in_array($onepricechild->getHotelchild()->getChild(), $allchild)){
                       $allchild[$i] = $onepricechild->getHotelchild()->getChild();
                       $i = $i + 1;
                   }
               }
               $i = 0;
               foreach ($allpricearr as $onepricearr) {
                   if( $onepricearr->getEtat()==1){
                   $allarrangement[$i] = $onepricearr;
                   $i = $i + 1; 
                   }
                 
               }
              
           }
    
   
           
           $Reservation = new ReservationHotel();
           //$form = $this->createForm(new ReservationHotelType(), $Reservation);
           $form = $this->createForm('Btob\HotelBundle\Form\ReservationHotelType', $Reservation);

           if ($request->getMethod() == 'POST') {
   
                  $form->handleRequest($request);
              
                   $em = $this->getDoctrine()->getManager();
                   $Reservation->setHotel($hotel);
               
                   $datedebut = new \DateTime($request->request->get("datedebut"));
                  
                   $Reservation->setDatedebut($datedebut);
                   $datefin = new \DateTime($request->request->get("datefin"));
                   // var_dump($datedebut ,$datefin ).die;
                  // dump($datefin).die;
                   $Reservation->setDatefin($datefin);
                   $em->persist($Reservation);    
                  $em->flush();
                   $Pricearrangement = array();
                   foreach($hotelpriceg->getPricearr() as $p){
                      $Pricearrangement[] = $p->getPrice();
                      $Pers[] = $p->getPers();
                      $Marge[] = $p->getMarge();
                      $Persm[] = $p->getPersm();
                   }
                  // var_dump($Pers).die;
                   
                   $idreservation = $Reservation->getId();
                   $timestamp = strtotime($Reservation->getDatedebut()->format('Y-m-d'));
   
                   $day = date('l', $timestamp);
                // if(!in_array($day,["Sunday","Saturday"])){
                //         $total = end($price)->getPrice();
                //     }else{
                //         $total = end($price)->getPricew();
                //     }
                   
                   
                   $arraytypechambre = array();
                   $arraytypechambrehidden = array();
                   $arraynamechambre = array();
                   $arrayadultes = array();
                   $arrayenfants = array();
                   $arrayarrangement = array();
                   $arrayarrangementhidden= array();
                   $total = 0;
                   
                   
                    $interval = $datedebut->diff($datefin);
                //    var_dump((int) $interval->format('%R%a')).die ;
                   $arraysupp= array();
                   for ($i = 1; $i <= $Reservation->getNbchambres(); $i++) {
                     //  var_dump($request->request->get("supp".$i)).die;
                      
                       $arraysupphidden= array();
                       $arrayages = array();
                       $arraytypechambre[] = $request->request->get("type".$i);
                       $arraytypechambrehidden[]=$request->request->get("typehidden".$i);
                    //  var_dump().die;
                       $arraynamechambre[] = $request->request->get("hiddenselect".$i);
                       $arrayadultes[] = $request->request->get("adultes".$i);
                       $arrayenfants[] = $request->request->get("enfants".$i);
                      
                       foreach($allsupplement as $supp){
                           if(!is_null($request->request->get("supp".$i."_".$supp->getId()))){
                            $arraysupp[] = $request->request->get("supp".$i."_".$supp->getId()); 
                            $arraysupphidden[] = $supp->getHotelsupplement()->getSupplement()->getName(); 
                           }
                     
                       }
                       
                       
                      // var_dump($arraysupphidden).die;
                          
                       for($j=1; $j<= $request->request->get("enfants".$i); $j++){
                           $arrayages[] = $request->request->get("ages".$i."_".$j);
                       }
                     
                       $arrayarrangement[] = $request->request->get("logement".$i);
                       $arrayarrangementhidden[] = $request->request->get("arrhidden".$i);
                    //  var_dump((float) $request->request->get("logement".$i) ).die;
                      if((int) $request->request->get("type".$i) == 3){
                        $total =  $total +  (float) $request->request->get("logement".$i) * ((int) $request->request->get("type".$i) - 1);
                      //  var_dump($total).die;
                           $reduction3lit = $hotelpriceg->getRed3lit();
                           if($hotelpriceg->getPers3lit() == 1){
                               $prix3emeplace = (float) $request->request->get("logement".$i) -((float) $request->request->get("logement".$i) * $reduction3lit / 100) ;
                            //   var_dump((float) $request->request->get("logement".$i) * $reduction3lit / 100).die;
                           }else{
                               
                                $prix3emeplace = (float) $request->request->get("logement".$i) - $reduction3lit  ;
                           
                           }
                        //   var_dump($total).die;
                           $total = (int) $interval->format('%R%a') *($total + $prix3emeplace);
                     //
                      }
                    // var_dump( $total).die;

                     if((int) $request->request->get("type".$i) > 3){
                        $total =  $total +  (float) $request->request->get("logement".$i) * ((int) $request->request->get("type".$i) - 2);
                        if (!is_null($hotelpriceg->getRed3lit())){
                          //  var_dump($hotelpriceg->getRed3lit()).die;
                            
                           $reduction3lit = $hotelpriceg->getRed3lit();
                           if($hotelpriceg->getPers3lit() == 1){
                               $prix3emeplace = (float) $request->request->get("logement".$i) * $reduction3lit / 100 ;
                           }else{
                                $prix3emeplace = (float) $request->request->get("logement".$i) + $reduction3lit  ;
 
                           }
                          // $total = $total + $prix3emeplace;
                        }else{
                            $prix3emeplace = 0;
                        }

                      if (!is_null($hotelpriceg->getRed4lit())){
                          //  var_dump($hotelpriceg->getRed3lit()).die;
                            
                           $reduction4lit = $hotelpriceg->getRed4lit();
                           if($hotelpriceg->getPers4lit() == 1){
                               $prix4emeplace = (float) $request->request->get("logement".$i) * $reduction4lit / 100 ;
                           }else{
                                $prix4emeplace = (float) $request->request->get("logement".$i) + $reduction4lit  ;
 
                           }
                           //$total = $total + $prix4emeplace;
                        }else{
                            $prix4emeplace = 0;
                        }

                        $total = (int) $interval->format('%R%a') * ($total + $prix4emeplace + $prix3emeplace);
                      }
                      if((int) $request->request->get("type".$i) == 2){
                         
                           $total = (int) $interval->format('%R%a') *( $total +  (float) $request->request->get("logement".$i) * ((int) $request->request->get("type".$i)));
                     
                      }
                      
                      elseif((int) $request->request->get("type".$i) == 1){
                          
                          $weekofhotel =  $em->createQueryBuilder()
                             ->select('j.name')
                             ->from('BtobHotelBundle:Week','j')
                             ->where('j.hotel = :hotel')
                             ->setparameter('hotel', $hotel)
                             ->getQuery()
                             ->getResult();   
                           // var_dump($weekofhotel).die;
                           $total =  $total +  (float) $request->request->get("logement".$i) * ((int) $request->request->get("type".$i));
                           $periodresevation = new DatePeriod(
                           new DateTime( $Reservation->getDatedebut()->format('Y-m-d')),
                           new DateInterval('P1D'),
                           new DateTime( $Reservation->getDatefin()->format('Y-m-d'))
                           );
                         
                           foreach ($periodresevation as $key => $value) {
                              $timestamp = strtotime($value->format('Y-m-d'));
                              $day = date('l', $timestamp);
                              $datesdereservation[] = substr($day, 0, 3) ;
                              
                           }
                         //  var_dump($datesdereservation).Die;
                          if(!empty($weekofhotel)){
                            
                                 foreach($weekofhotel as $w){
                                   //  var_dump($w["name"]).die;
                                     if(in_array($w["name"], $datesdereservation)){
                                        foreach($datesdereservation as $d){
                                            if($d == $w){
                                              //  var_dump($w["name"]).die;
                                            if($hotelpriceg->getPerssupsinglew()==1){
                                              $suppliementprix =  (float) $request->request->get("logement".$i) * $hotelpriceg->getSupsinglew()/100;;
                                           }else{
                                              $suppliementprix = $hotelpriceg->getSupsinglew();
                
                                           }
                                           if($hotelpriceg->getPersssw() == 1){
                                                $prixsupplimentavecmarge = $suppliementprix + ($suppliementprix*$hotelpriceg->getMargessw()/100);
                                           }else{
                                              $prixsupplimentavecmarge = $suppliementprix + $hotelpriceg->getMargessw();
                                           }
                                           
                                        //   $total =  ($total + $prixsupplimentavecmarge);
                                                
                                                
                                                
                                                
                                                
                                            }else{
                                            if($hotelpriceg->getPerssupsingle()==1){
                                              $suppliementprix =  (float) $request->request->get("logement".$i) * $hotelpriceg->getSupsingle()/100;;
                                           }else{
                                              $suppliementprix = $hotelpriceg->getSupsingle();
                
                                           }
                                           if($hotelpriceg->getPersss() == 1){
                                                $prixsupplimentavecmarge = $suppliementprix + ($suppliementprix*$hotelpriceg->getMargess()/100);
                                           }else{
                                              $prixsupplimentavecmarge = $suppliementprix + $hotelpriceg->getMargess();
                                           }
                                         //  var_dump($prixsupplimentavecmarge).die;
                                         //  $total =  ($total + $prixsupplimentavecmarge);
                                            }
                                         //   var_dump($total).die;
                                        }
                                          $total= (int) $interval->format('%R%a') * ($total + $prixsupplimentavecmarge);

                                     }
                                 }
                          }else{
                              //var_dump('test').die;
                                           if($hotelpriceg->getPerssupsingle()==1){
                                              $suppliementprix =  (float) $request->request->get("logement".$i) * $hotelpriceg->getSupsingle()/100;;
                                           }else{
                                              $suppliementprix = $hotelpriceg->getSupsingle();
                                           }
                                        
                                           if($hotelpriceg->getPersss() == 1){
                                                $prixsupplimentavecmarge = $suppliementprix + ($suppliementprix*$hotelpriceg->getMargess()/100);
                                           }else{
                                              $prixsupplimentavecmarge = $suppliementprix + $hotelpriceg->getMargess();
                                           }
                                           //  var_dump($total).die;
                                   $total = (int) $interval->format('%R%a') * ($total + $prixsupplimentavecmarge);
                                  // var_dump($total).Die;
                          }
                         //  var_dump($datesdereservation).die;
                     
                      }
                    
                       
                       //var_dump(array_sum($arraysupp)).die;
                   
                       $roomsinarray = array();
                       $chambre = new chambre();
                       
                      // var_dump($arraytypechambre).die;
                      
                       $chambre->setAgeenfant($arrayages);
                       $chambre->setSupplements($arraysupphidden);
                       
                       foreach($arraytypechambrehidden as $ch)
                       $chambre->setType($ch);
                       foreach($arrayadultes as $ad)
                       $chambre->setNbadultes($ad);
                       foreach($arrayenfants as $en)
                       $chambre->setNbenfants($en);
                       foreach($arrayarrangementhidden as $arr)
                       $chambre->setArrangement($arr);
                       $chambre->setReservationhotel($Reservation);
                       $em->persist($chambre);
                      // $em->flush(); 
                    //var_dump(array_sum($arraysupp)).Die;
                      
                   } 
                //   var_dump(array_sum($arraysupp)).die;
                    $total = $total + ((int) $interval->format('%R%a') * array_sum($arraysupp));
                    //var_dump($total).die;
                   $Reservation->setTotal($total);
                 
                  foreach($arraynamechambre as $namech){
                   $roomsinarray[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->findBy(array('name' => $namech));
                   }
                   $roomchoice = array();
                   foreach($roomsinarray as $roo){
                   $roomchoice[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('room' => $roo,'hotelprice' => $allhotelprice[0]));
   
                   }
                   foreach($roomchoice as $rm){
                      foreach($rm as $ri)
                       $ri->setQte($ri->getQte() - 1);
                   }
                //  var_dump($roomsinarray).die;
                   $em->flush();
                 
                   
                   //var_dump($arraytypechambre,$arrayadultes,$arrayenfants,$arrayarrangement).die;
               
   
                  
                   return $this->redirect($this->generateUrl('coordonnes', array("id" => $Reservation->getId(),'datef' => $datef,'dated' => $dated)));
   
                 //  return $this->redirect($this->generateUrl('btob_hotel_homepage', array("hotelid" => $id)));
             //  } else {
                //   echo $form->getErrors();
            //   }
            
           }



        return $this->render('default/chambres.html.twig', ['form' =>$form->createView(),'h' => $hotel,"hotel" => $hotel,'allsupplement' => $allsupplement,'allarrangement' => $allarrangement,'datef'=>end($price)->getDates()->format('m/d/Y'),'arr'=>$arraydater,'retro'=>$datert->format('m/d/Y'),'hotelsimg'=>$hotelsimg, 'dated' => $dated,
             'datef' => $datef,
         
        ]);
    }


    /**
     * @Route("/payerreservationhotel/{id}/{coordonnees}", name="payerreservationhotel")
     */
    public function payerAction($id,$coordonnees,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:ReservationHotel')
            ->find($id);
            $Coordonneesreservationhotel=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Coordonneesreservationhotel')
            ->find($coordonnees);
             
            $hotelsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Hotelimg','i')
            ->where('i.hotel = :hotel') 
             ->setParameter('hotel', $reservation->getHotel())
            ->getQuery()
            ->getResult();
            $chambre =  $em->createQueryBuilder()
            ->select('c')
            ->from('BtobHotelBundle:Chambre','c')
            ->where('c.reservationhotel = :reservation')
            ->setParameter('reservation', $reservation)
            ->getQuery()
            ->getResult();
            $form = $this->createForm('Btob\HotelBundle\Form\ReservationHotelpayerType', $reservation);

            // $form = $this->createForm(new ReservationHotelpayerType(), $reservation);
         //   $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('hotelsentunise'));

            }
        
       

       return $this->render('default/payerreservationhotel.html.twig', array('form' =>$form->createView(),'reservation'=>$reservation,'chambres' => $chambre,'hotelsimg'=>$hotelsimg,'coordonnees'=>$Coordonneesreservationhotel));
    }
    
 /**
     * @Route("/deletereservationhotel/{id}", name="deletereservationhotel")
     */
    public function deletereservationhotelAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:ReservationHotel')
            ->find($id);

        $em->remove($reservation);
        $em->flush();
        $this->addFlash("success", "La réservation à été annulée avec succès ..");

        return $this->redirect($this->generateUrl('hotelsentunise'));
    }

 /**
     * @Route("/reserver/{id}/{marcheid}/{{dated}}/{{datef}}", name="reserver")
     */
    public function reserverAction(Hotel $hotelid ,$marcheid,$dated,$datef ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
            $payement = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Payement')
            ->find(1);
            
            
       
        $hotels =    $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1')
        ->getQuery()
        ->getResult();
        $hotelsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Hotelimg','i')
             ->where('i.hotel = :hotel')  
             ->setparameter('hotel', $hotelid)
            ->getQuery()
            ->getResult();
                       
            $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $periodevalide = array();
            $contrats = array();
            $capacityrooms = array();
            $namebycapacity = array();
            $nameroomofhotel = array();
           
            foreach($hotels as $hotel){
                foreach($hotel->getHotelroom() as $r){
                     $capacityrooms[] = $r->getRoom()->getCapacity();
                     $nameroomofhotel[] = $r->getRoom()->getName();
                     
                }
                foreach($hotel->getHotelroom() as $r){
                    if($r->getRoom()->getCapacity() == min($capacityrooms) and in_array($r->getRoom()->getName(), $nameroomofhotel))
                    $namebycapacity = $r->getRoom()->getName();
                    
                    
               }
             $hotelprice =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $hotel)
                            // ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
                
                    
                    
            $alldates = array();         
            $arrayc = array();
             foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
             }
         //  var_dump($arrayc).die;
            foreach ($hotelprice as $key=>$valuec) {
              //   var_dump($valuec->getHotel()->getId()).die;
           if(count($arrayc) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
                 if(!empty($datesvalides) ){
                    $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dates = :dates')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                              ->setparameter('dates', min($datesvalides))
                              ->getQuery()
                              ->getResult();      
                 }
                     
                // }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                           //  ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
               
           }
            }
          
              
                 
             
        //  var_dump($arraymerge).die;

                foreach ($hotelpriceg as $key=>$value) {
                  
                if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
                  //  if( $value->getHotel()->getDated())
                      $periodevalide[] =  $value->getHotel()->getId();
                    //  var_dump($periodevalide).die;
                             //  $datesvalides[] = $value->getHotel()->getDated();
                           //  $datesvalides[] = $hotelprice->getdated();
                             $contrats[]= $value;
                }
                
                
                if($value->getHotel() == $hotel){
                 $arrayprices=array();
                  foreach($value->getPricearr() as $arr){
                  
                      $totalprice = $arr->getPrice();
                   
                    if($arr->getEtat() != 0){
                        // {{arr.hotelarrangement.arrangement.name}}
                         $arrayprices[]= $arr;
                    }
                    
                     
                      
                  }
                 
                }
                }
                
               
             
            }
             
            
            
            
            
            
            
            
            
            
            
            
            
            

                
                $arragement =   $em->createQueryBuilder()
                ->select('v')
                ->from('BtobHotelBundle:arrangement','v')
                ->getQuery()
                ->getResult();
                

                $allhotelprice[] =  $em->createQueryBuilder()
                ->select('p')
                ->from('BtobHotelBundle:Hotelprice','p')
               //->where('p.hotel = :hotel') 
                ->where('p.dates > :datedujour') 
             //   ->setParameter('hotel', $hotel)
                ->setParameter('datedujour', new DateTime())
               // ->groupby('p.hotel')
                ->getQuery()
                ->getResult();
                foreach($allhotelprice as $p){
                    foreach($p as $pr){
                        if(!is_null($pr->getPrice())){
                            //dump('h').die;
                            $hotelsayantsdescontrats = $p;
                        }
                         
                    }
                    
                }
          
                    $coordonnees = new Coordonneesreservationhotel();
                    $form = $this->createForm('Btob\HotelBundle\Form\CoordonneesreservationhotelType', $coordonnees);
                    $form->handleRequest($request);
                    if ($request->getMethod() == 'POST') {
                    $Reservation = new ReservationHotel();
                    $Reservation->setHotel($hotel);
                    $currentDate = new DateTime();
                    $Reservation->setDatedebut(new DateTime($dated));
                    $Reservation->setDatefin(new DateTime($dated));
                    // $Reservation->setNbchambres(min($capacityrooms));
                    $Reservation->setNbchambres(2);
                    $Reservation->setNbadultes(2);
                    //var_dump($request->request->get('total')).die;
                    $Reservation->setTotal($request->request->get('total'));
                    $Reservation->setTypepayement($request->request->get('group'));
                    $em->persist($Reservation);
                  
                

                        $coordonnees->setReservationhotel($reservation);
                        $Reservation->setTypepayement($request->request->get('group'));
                        $em->persist($coordonnees);
                        $em->flush();
                    //   for ($i = 0; $i < 2; $i++) {
                             
                         $accompagnant = new AccompagnantsChambreshotel();
                         $accompagnant->setNomadulte($request->request->get('nomadulte'));
                         $accompagnant->setAgeadulte($request->request->get('ageadulte'));
                         $accompagnant->setNomenfant($request->request->get('nomenf'));
                         $accompagnant->setAgeenfant($request->request->get('ageenf'));
                         $accompagnant->setChambre($request->request->get('idchambre'));
                         $accompagnant->setReservationhotel($Reservation->getId());
                         $accompagnant->setRecommandations($request->request->get('options'));
                         $em->persist($accompagnant);
                          $em->flush();
                        //   } 
                    
                       
                     $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation à '.$hotel->getName().', Référence:'.$Reservation->getId())
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('test@rusticavoyages.com')
                        ->setBody("Nouvelle réservation à ".$hotel->getName()." à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()."  Le total de la réservation est de " .$request->request->get('total')." TND . veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
                    $this->addFlash("success", "Votre réservation à été faite avec succès ..");

                    return $this->redirect($this->generateUrl('hotelsentunise'));
                }

        return $this->render('default/reserver.html.twig', [
           'hotels' => $hotels,
           'hotel' => $hotelid,
            'dated' => $dated,
             'datef' => $datef,
           'img'=>$hotelsimg,
            'form' => $form->createView(),
           //'h' => $hotel,
           'marcher' => $marche,
           'periodevalide'=>$periodevalide,
           'contrats'=>$contrats,
           //'villes'=>$villes,
           'arragement'=>$arragement,
           'allarrangement'=>$allarrangement,
           'marcher' => $marcher,
           'mincapacity'=>min($capacityrooms),
           'namebycapacity'=>$namebycapacity,
           'hotelsprice'=>$hotelsayantsdescontrats,
           'payement'=>$payement

         
        ]);
    }



    /**
     * @Route("/hotelsentunisie", name="hotelsentunise")
     */
    public function hotelsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       // $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));

        $hotels =    $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1')
        ->getQuery()
        ->getResult();
        $hotelsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Hotelimg','i')
            ->groupBy('i.hotel')  
            ->getQuery()
            ->getResult();
                       
            $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $periodevalide = array();
            $contrats = array();
            $capacityrooms = array();
            $namebycapacity = array();
            $nameroomofhotel = array();
           
            foreach($hotels as $hotel){
                foreach($hotel->getHotelroom() as $r){
                     $capacityrooms[] = $r->getRoom()->getCapacity();
                     $nameroomofhotel[] = $r->getRoom()->getName();
                     
                }
                foreach($hotel->getHotelroom() as $r){
                    if($r->getRoom()->getCapacity() == min($capacityrooms) and in_array($r->getRoom()->getName(), $nameroomofhotel))
                    $namebycapacity = $r->getRoom()->getName();
                    
                    
               }
             $hotelprice =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $hotel)
                            // ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
                
                    
                    
            $alldates = array();         
            $arrayc = array();
             foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
             }
         //  var_dump($arrayc).die;
            foreach ($hotelprice as $key=>$valuec) {
              //   var_dump($valuec->getHotel()->getId()).die;
           if(count($arrayc) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
                 if(!empty($datesvalides) ){
                    $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dates = :dates')
                               ->andwhere('c.price IS NOT NULL') 
                               ->andwhere('c.dates >= :d') 
                             ->setparameter('d', new \DateTime('now'))
                              
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                              ->setparameter('dates', min($datesvalides))
                              ->getQuery()
                              ->getResult();      
                 }
                     
                // }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                               ->andwhere('c.price IS NOT NULL') 
                               ->andwhere('c.dates >= :d') 
                             ->setparameter('d', new \DateTime('now'))
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                           //  ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
               
           }
            }
          
              
                 
             
        //  var_dump($arraymerge).die;

                foreach ($hotelpriceg as $key=>$value) {
                  
                // if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
                  //  if( $value->getHotel()->getDated())
                      $periodevalide[] =  $value->getHotel()->getId();
                    //  var_dump($periodevalide).die;
                             //  $datesvalides[] = $value->getHotel()->getDated();
                           //  $datesvalides[] = $hotelprice->getdated();
                             $contrats[]= $value;
                // }
                
                
                if($value->getHotel() == $hotel){
                 $arrayprices=array();
                  foreach($value->getPricearr() as $arr){
                  
                      $totalprice = $arr->getPrice();
                   
                    if($arr->getEtat() != 0){
                        // {{arr.hotelarrangement.arrangement.name}}
                         $arrayprices[]= $arr;
                    }
                    
                     
                      
                  }
                 
                }
                }
                
               
             
            }
                //  var_dump($arrayprices).die;
               
                 // var_dump( $arrayprices).die;
               
     
  
        $villes =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:Ville','v')
        ->getQuery()
        ->getResult();
        $arragement =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:arrangement','v')
        ->getQuery()
        ->getResult();
         
        $room =   $em->createQueryBuilder()
        ->select('r')
        ->from('BtobHotelBundle:Room','r')
        ->getQuery()
        ->getResult();

        
        
        if ($request->getMethod() == 'POST') {

            $request->request->get("dated");
         //   dump( $request->request->get("ville")).die;
            $recherche =   $em->createQueryBuilder()
            ->select('h')
            ->from('BtobHotelBundle:Hotel','h')
            //->where('h.act = 1' )
           // ->where('h.ville = :ville')
           // ->andwhere('h.name = :name')
            ->andwhere('h.maxenfant >= :maxenfant')
            ->andwhere('h.star = :star')
          //  ->setParameter('ville', $request->request->get("ville"))
          //  ->setParameter('name', '%'.$request->request->get("hotels").'%')
            ->setParameter('maxenfant', $request->request->get("enfants"))
            ->setParameter('star', $request->request->get("star"))
            ->getQuery()
            ->getResult();
            $dated = new DateTime($request->request->get("dated"));
             $datef = new DateTime($request->request->get("datef"));
     //   var_dump($request->request->get("star")).die;
            return $this->redirect($this->generateUrl('recherchehotel', array("nom" => $request->request->get("hotels"),"ville" => $request->request->get("ville"),"enfants" => $request->request->get("enfants"),"arr" => $request->request->get("arr"),"stars" => $request->request->get("star"),"dated"=>$dated->format('Y-m-d'),"datef"=>$datef->format('Y-m-d'))));

   
        }
     $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
             $hotels,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',5)
        );
     //    var_dump($contrats).die;
        
        
        
        return $this->render('default/listehotels.html.twig', array(
            'hotels' => $pagination,
             'listehotels' => $hotels,
             'arrayprices'=>$arrayprices,
            'img'=>$hotelsimg, 'room'=>$room,
            'periodevalide'=>$periodevalide,
            'contrats'=>$contrats,
            'villes'=>$villes,
            'arragement'=>$arragement,
           // 'allarrangement'=>$allarrangement,
            'marcher' => $marcher,
          //  'datesvalides'=>min($datesvalides),
            'mincapacity'=>min($capacityrooms),
            'namebycapacity'=>$namebycapacity
        ));
    }



       /**
     * @Route("/recherchehotel/{nom}/{ville}/{enfants}/{arr}/{stars}/{dated}/{datef}", name="recherchehotel")
     */
    public function rechercheAction(Request $request,$nom,$ville,$enfants,$arr,$stars,$dated,$datef)
    {
       $em = $this->getDoctrine()->getManager();
       // $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));

        $hotels =    $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1')
        ->getQuery()
        ->getResult();
        $hotelsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Hotelimg','i')
            ->groupBy('i.hotel')  
            ->getQuery()
            ->getResult();
                       
            $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find(1);

            $periodevalide = array();
            $contrats = array();
            $capacityrooms = array();
            $namebycapacity = array();
            $nameroomofhotel = array();
           
            foreach($hotels as $hotel){
                foreach($hotel->getHotelroom() as $r){
                     $capacityrooms[] = $r->getRoom()->getCapacity();
                     $nameroomofhotel[] = $r->getRoom()->getName();
                     
                }
                foreach($hotel->getHotelroom() as $r){
                    if($r->getRoom()->getCapacity() == min($capacityrooms) and in_array($r->getRoom()->getName(), $nameroomofhotel))
                    $namebycapacity = $r->getRoom()->getName();
                    
                    
               }
             $hotelprice =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $hotel)
                            // ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
                
                    
                    
            $alldates = array();         
            $arrayc = array();
             foreach ($hotelprice as $key=>$v) {
                 $arrayc[] = $v->getHotel()->getId();
                 $alldates[] =  $v->getDates();
             }
         //  var_dump($arrayc).die;
            foreach ($hotelprice as $key=>$valuec) {
              //   var_dump($valuec->getHotel()->getId()).die;
           if(count($arrayc) > 1){
           $datesvalides =array();
                foreach($alldates as $d){
                if($d >= new \DateTime('now')){
                   
                    $datesvalides[] = $d;
                }

                }
                 if(!empty($datesvalides) ){
                    $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                              ->andwhere('c.dates = :dates')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                              ->setparameter('dates', min($datesvalides))
                              ->getQuery()
                              ->getResult();      
                 }
                     
                // }
           }else{
                $hotelpriceg =  $em->createQueryBuilder()
                             ->select('c')
                             ->from('BtobHotelBundle:Hotelprice','c')
                              ->where('c.marcher = :marche')
                              ->andwhere('c.hotel = :hotel')
                             ->setparameter('marche', $marcher)
                             ->setparameter('hotel', $valuec->getHotel())
                           //  ->groupBy('c.hotel')  
                             ->getQuery()
                             ->getResult();    
               
           }
            }
          
              
                 
             
    

                foreach ($hotelpriceg as $key=>$value) {
                  
                if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
                  //  if( $value->getHotel()->getDated())
                      $periodevalide[] =  $value->getHotel()->getId();
                    //  var_dump($periodevalide).die;
                             //  $datesvalides[] = $value->getHotel()->getDated();
                           //  $datesvalides[] = $hotelprice->getdated();
                             $contrats[]= $value;
                }
                
                
                }
                
               
             
            }
               
               
       
        $villes =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:Ville','v')
        ->getQuery()
        ->getResult();
         $villebyid = $em->getRepository('BtobHotelBundle:Ville')->find(1);
        $arragement =   $em->createQueryBuilder()
        ->select('v')
        ->from('BtobHotelBundle:arrangement','v')
        ->getQuery()
        ->getResult();
         
        $room =   $em->createQueryBuilder()
        ->select('r')
        ->from('BtobHotelBundle:Room','r')
        ->getQuery()
        ->getResult();
        
            if ($request->getMethod() == 'POST') {

            $request->request->get("dated");
         //   dump( $request->request->get("ville")).die;
            $recherche =   $em->createQueryBuilder()
            ->select('h')
            ->from('BtobHotelBundle:Hotel','h')
            //->where('h.act = 1' )
           // ->where('h.ville = :ville')
           // ->andwhere('h.name = :name')
            ->andwhere('h.maxenfant >= :maxenfant')
            ->andwhere('h.star = :star')
          //  ->setParameter('ville', $request->request->get("ville"))
          //  ->setParameter('name', '%'.$request->request->get("hotels").'%')
            ->setParameter('maxenfant', $request->request->get("enfants"))
            ->setParameter('star', $request->request->get("star"))
            ->getQuery()
            ->getResult();
            $dated = new DateTime($request->request->get("dated"));
             $datef = new DateTime($request->request->get("datef"));
      //  var_dump($_POST["star"]).die;
            return $this->redirect($this->generateUrl('recherchehotel', array("nom" => $request->request->get("hotels"),"ville" => $request->request->get("ville"),"enfants" => $request->request->get("enfants"),"arr" => $request->request->get("arr"),"stars" => $request->request->get("star"),"dated"=>$dated->format('Y-m-d'),"datef"=>$datef->format('Y-m-d'))));

   
        }
        
        
        
        
        
        
        
        
        
         $hotelarrang =   $em->createQueryBuilder()
        ->select('a.id')
        ->from('BtobHotelBundle:hotelarrangement','a')
        ->where('a.arrangement = :arr')
         ->andwhere('a.hotel = :hotelid')
        ->setParameter('arr', $arr)
        ->setParameter('hotelid', $nom)
        ->getQuery()
        ->getOneOrNullResult();
        
        $arrang =   $em->createQueryBuilder()
        ->select('ar')
        ->from('BtobHotelBundle:hotelarrangement','ar')
        ->where('ar.arrangement = :arr')
        ->setParameter('arr', $arr)
        ->getQuery()
        ->getResult();
        $hotelswitharr = array();
        foreach($arrang as $arg){
           $hotelswitharr[]= $arg->getHotel()->getId();
        }
        
      //  var_dump($hotelswitharr).die;
             $dated = new DateTime($dated);
             $datef = new DateTime($datef);
            // var_dump($nom).die;
        foreach($contrats as $c){
        //  var_dump($c->getDates()).die;
        
        if($nom != "Tous" and $arr != "Tous" and $ville != "Tous"){
        if($c->getHotel()->getId() == $nom and $c->getDated()<= $dated and $c->getDates()>= $datef and !is_null($hotelarrang) ){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
        ->andwhere('h.ville = :ville')
        ->andwhere('h.id = :name')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
        ->setParameter('ville', $ville)
        ->setParameter('name', $nom)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
         
           
        }elseif($nom == "Tous" and $arr == "Tous" and $ville == "Tous"){
         if( $c->getDated()<= $dated and $c->getDates()>= $datef ){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
      //  ->andwhere('h.ville = :ville')
        //->andwhere('h.id = :name')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
      //  ->setParameter('ville', $ville)
       // ->setParameter('name', $nom)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
            
        }
        elseif($nom != "Tous" and $arr == "Tous" and $ville == "Tous"){
          if(  $c->getDated()<= $dated and $c->getDates()>= $datef){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
        ->andwhere('h.id = :name')
       //  ->andwhere('h.ville = :ville')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star >= :star')
       //  ->setParameter('ville', $ville)
       ->setParameter('name', $nom)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
         }
        elseif($nom == "Tous" and $arr == "Tous" and $ville != "Tous"){
          if(  $c->getDated()<= $dated and $c->getDates()>= $datef){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
         ->andwhere('h.ville = :ville')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
         ->setParameter('ville', $ville)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
         }elseif($nom = "Tous" and $ville == "Tous" and $arr != "Tous"){
         if(  $c->getDated()<= $dated and $c->getDates()>= $datef ){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
        ->andwhere('h.id IN (:liste)')
       //  ->andwhere('h.ville = :ville')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
         ->setParameter('liste', $hotelswitharr)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
         }elseif($nom = "Tous" and $ville != "Tous" and $arr != "Tous"){
         if(  $c->getDated()<= $dated and $c->getDates()>= $datef ){
        $recherche =  $em->createQueryBuilder()
        ->select('h')
        ->from('BtobHotelBundle:Hotel','h')
        ->where('h.act = 1' )
        ->andwhere('h.id IN (:liste)')
         ->andwhere('h.ville = :ville')
        ->andwhere('h.maxenfant >= :maxenfant')
        ->andwhere('h.star = :star')
         ->setParameter('liste', $hotelswitharr)
         ->setParameter('ville', $ville)
        ->setParameter('maxenfant', $enfants)
        ->setParameter('star', $stars)
        ->getQuery()
        ->getResult();
             }
         }
        }
   // var_dump($arr).die;
          $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
             $recherche,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',5)
        );

        return $this->render('default/listehotelsrecherche.html.twig', array(
            'hotels' => $pagination,
              'listehotels' => $hotels,
               'dated' => $dated,
                'ville' => $ville,
                'enf'=>$enfants,
                'nom'=>$nom,
                'stars'=>$stars,
             'datef' => $datef,
             'img'=>$hotelsimg,
             'room'=>$room,
            'arr'=> $arr,
            'periodevalide'=>$periodevalide,
            'contrats'=>$contrats,
            'villes'=>$villes,
            'arragement'=>$arragement,
            'allarrangement'=>$allarrangement,
            'marcher' => $marcher,
          //  'datesvalides'=>min($datesvalides),
            'mincapacity'=>min($capacityrooms),
            'namebycapacity'=>$namebycapacity
            //"recherche" => $recherche
        ));
    }

// Voyages Organisees
   /**
     * @Route("/voyagesorganisees", name="voyagesorganisees")
     */
    public function listevoyagesorganiseesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       
        $VO = $em->createQueryBuilder()
        ->select('s')
        ->from('BtobSejourBundle:Sejour','s')
        ->where('s.act = 1')
       // ->distinct('s.villea')
        ->getQuery()
        ->getResult();
         $destinations = $em->createQueryBuilder()
        ->select('s.villea')
        ->from('BtobSejourBundle:Sejour','s')
        ->where('s.act = 1')
        ->distinct('s.villea')
        ->getQuery()
        ->getResult();
     // var_dump($destinations).die;
        //  $paginator  = $this->get('knp_paginator');
        //     $pagination = $paginator->paginate(
        //      $VO,
        //     $request->query->getInt('page', 1),
        //     $request->query->getInt('limit',3)
        // );
   // $VO = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findAll();
        $voimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Imgesej','i')
            ->groupBy('i.sejour')  
            ->getQuery()
            ->getResult();
        if ($request->getMethod() == 'POST') {
            return $this->redirect($this->generateUrl('recherchevo', array('destination' =>$request->request->get('destination'),'duree'=>$request->request->get('duree'),)));


         }
        return $this->render('default/voyagesorganiees.html.twig', [
           'vo' => $VO,
           'dest'=>$destinations,
           'sejourimg'=>$voimg,
        //    'h' => $hotel,
         
        ]);
    }

    /**
     * @Route("/detailsvo/{id}", name="detailsvo")
     */
    public function detailsvoAction(Sejour $sejour, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       
       $VO = $em->createQueryBuilder()
        ->select('s')
        ->from('BtobSejourBundle:Sejour','s')
        ->where('s.act = 1')
        ->getQuery()
        ->getResult();
        $datesvo = $em->createQueryBuilder()
        ->select('s')
        ->from('BtobSejourBundle:datessuplimentairesvo','s')
        ->where('s.vo = :vo')
        ->setParameter('vo',$sejour->getId())
        ->getQuery()
        ->getResult();
        
        $voimg =  $em->createQueryBuilder()
            ->select('v')
            ->from('BtobSejourBundle:Imgesej','v')
            ->where('v.sejour = :sejour')
            ->setParameter('sejour',$sejour)
            ->getQuery()
            ->getResult();
            
         $voimgall =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Imgesej','i')
            ->groupBy('i.sejour')  
            ->getQuery()
            ->getResult();
        // if ($request->getMethod() == 'POST') {
        //     return $this->redirect($this->generateUrl('recherchevo', array('destination' =>$request->request->get('destination'),'duree'=>$request->request->get('duree'),)));


        //  }
        return $this->render('default/detailsvo.html.twig', [
           'vo' => $sejour,
           'datesvo'=>$datesvo,
           'sejourimg'=>$voimg,
            'img'=>$voimgall,
           'voyages' => $VO,
           
        //    'h' => $hotel,
         
        ]);
    }


      /**
     * @Route("/recherchevo/{destination}/{duree}", name="recherchevo")
     */
    public function recherchevoAction($destination , $duree,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //dump($destination).die;
        $recherche =  $em->createQueryBuilder()
        ->select('s')
        ->from('BtobSejourBundle:Sejour','s')
        ->where('s.villea = :villea')
        ->orwhere('s.dureesj = :duree')
        ->setParameter('villea',$destination)
        ->setParameter('duree',$duree)
        ->getQuery()
        ->getResult();
           $destinations = $em->createQueryBuilder()
        ->select('s.villea')
        ->from('BtobSejourBundle:Sejour','s')
        ->where('s.act = 1')
        ->distinct('s.villea')
        ->getQuery()
        ->getResult();
     //   var_dump($VO).die;
        $voimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Imgesej','i')
            ->groupBy('i.sejour')  
            ->getQuery()
            ->getResult();
        //   if(!is_null($VO)) {
                
        //     $paginatorvo  = $this->get('knp_paginator');
        //     $pagination = $paginatorvo->paginate(
        //      $recherche,
        //     $request->query->getInt('page', 1),
        //     $request->query->getInt('limit',3)
        // );
        //   }
             if ($request->getMethod() == 'POST') {
                //  $this->addFlash("success", "voyages ont été trouvés ..");
            return $this->redirect($this->generateUrl('recherchevo', array('destination' =>$request->request->get('destination'),'duree'=>$request->request->get('duree'),)));


         }
        return $this->render('default/voyagesorganiees.html.twig', [
           'vo' => $recherche,
           'dest'=>$destinations,
           'sejourimg'=>$voimg,
         
        ]);
    }
    
      /**
     * @Route("/reservervo/{id}/{nblignes}", name="reservervo")
     */
    public function reservervoAction(Sejour $sejour , $nblignes, Request $request)
    {
    
            $em = $this->getDoctrine()->getManager();
            $listeacc = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Accompagnantsreservationvo','i')
            ->where('i.ciruit = :s')  
            ->setParameter('s', $sejour->getId())
             ->orderBy('i.id', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults($nblignes)
            ->getQuery()
            ->getResult();
        
            // foreach($listeacc as $liste){
            //     var_dump($liste->getId()).die;
            // }
       $total = 0;
       $nbvoyageurs =0;
        //   var_dump($listeacc).die;
            if ($request->getMethod() == 'POST') {
                     foreach($listeacc as $liste){ 
                         // var_dump($liste->getNbenfants()).die;
                        $nbvoyageurs = $nbvoyageurs + ($liste->getNbadultes() + $liste->getNbenfants()) ;
                       
                          for ($i = 0; $i < $liste->getNbadultes(); $i++) {
                             
                         $coordonnees = new coordonneesvoyageo();
                         $coordonnees->setNom($request->request->get('noma')[$i]);
                         $coordonnees->setPrenom($request->request->get('prenoma')[$i]);
                         $coordonnees->setDatenaissance($request->request->get('datea')[$i]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
                        for ($e = 0; $e < $liste->getNbenfants(); $e++) {
                             
                         $coordonnees = new coordonneesvoyageo();
                         $coordonnees->setNom($request->request->get('nome')[$e]);
                         $coordonnees->setPrenom($request->request->get('prenome')[$e]);
                         $coordonnees->setDatenaissance($request->request->get('datee')[$e]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
                            for ($b = 0; $b < $liste->getNbbebe(); $b++) {
                             
                         $coordonnees = new coordonneesvoyageo();
                         $coordonnees->setNom($request->request->get('nome')[$b]);
                         $coordonnees->setPrenom($request->request->get('prenome')[$b]);
                         $coordonnees->setDatenaissance($request->request->get('datee')[$b]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
               
                     }
            $prixavecmarge= $sejour->getPrix()+$sejour->getMarge();
           $total = $prixavecmarge * $nbvoyageurs;
       //    var_dump($total).die;

                return $this->redirect($this->generateUrl('payerreservationvoyageo', array("id" => $sejour->getId(),"total"=>$total,"nblignes"=>$nblignes)));
            }
       
            return $this->render('default/coordonneesvo.html.twig', [
               
               'c'=>$sejour,
               
               'nblignes'=>$nblignes,'listeacc'=>$listeacc
             //  'form' => $form->createView()
               
             
            ]);
        }

         
          /**
     * @Route("/payerreservationvoyageo/{id}/{total}/{nblignes}", name="payerreservationvoyageo")
     */
    public function payerreservationvoyageoAction(Sejour $sejour ,$nblignes, $total,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
          $payement = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Payement')
            ->find(1);
         $listeacc = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Accompagnantsreservationvo','i')
            ->where('i.ciruit = :s')  
            ->setParameter('s', $sejour->getId())
             ->orderBy('i.id', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults($nblignes)
            ->getQuery()
            ->getResult();
       
        $sejourimg =  $em->createQueryBuilder()
                ->select('i')
                ->from('BtobSejourBundle:Imgesej','i')
                ->where('i.sejour = :sejour') 
                 ->setParameter('sejour',$sejour)
                ->getQuery()
                ->getResult();
        
        $coordonnees = new coordonneesvo();
        
        $form = $this->createForm('Btob\HotelBundle\Form\CoordonneesvoType', $coordonnees);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            $reservationvo = new Reservationsejour();
            $reservationvo->setTotal($total);
            $reservationvo->setSejour($sejour);
            $reservationvo->setTypepayement($request->request->get('group'));

            
            $coordonnees->setVo($sejour);
            $em->persist($coordonnees);
            $em->persist($reservationvo);
            $em->flush();
          $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation à '.$sejour->getVillea().', Référence:'.$reservationvo->getId())
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('reservation@rusticavoyages.com')
                        ->setBody("Nouvelle réservation à ".$sejour->getVillea()." à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()."  Le total de la réservation est de " .$total." TND . veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
           $this->addFlash("success", "Votre réservation à été faite avec succès ..");

                    return $this->redirect($this->generateUrl('voyagesorganisees'));
      
        }
         
        
        
        
   
        return $this->render('default/totalreservationvo.html.twig', [
            'sejour'=>$sejour,'total'=>$total,
           'reservation'=>$reservationvo,
           'payement' => $payement,'sejourimg'=>$sejourimg,'listeacc'=>$listeacc,
           'form' => $form->createView()
           
         
        ]);
    }
       
         
         
         
         
         
         
            /**
     * @Route("/reservervoyageo/{id}", name="reservervoyageo")
     */
    public function reservervoyageoAction(Sejour $sejour ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       
          $vopardate =  $em->createQueryBuilder()
            ->select('v')
            ->from('BtobSejourBundle:Sejour','v')
            ->where('v.villea = :s')  
            ->setParameter('s', $sejour->getVillea())
            ->getQuery()
            ->getResult();
      //  var_dump($vopardate).die;
          $voimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobSejourBundle:Imgesej','i')
            ->where('i.sejour = :s')  
            ->setParameter('s', $sejour)
            ->getQuery()
            ->getResult();
         $datesvo = $em->createQueryBuilder()
        ->select('s')
        ->from('BtobSejourBundle:datessuplimentairesvo','s')
        ->where('s.vo = :vo')
        ->setParameter('vo',$sejour->getId())
        ->getQuery()
        ->getResult();
        $em = $this->getDoctrine()->getManager();
      //  $reservation = new Reservationcircuit();
         //$form = $this->createForm('Btob\SejourBundle\Form\ReservationcircuitfrontType', $reservation);
         //$form->handleRequest($request);
          if ($request->getMethod() == 'POST') {
           //  var_dump($request->request->get('nbad'),$request->request->get('nbenf')).die;
              for ($i = 0; $i < count($request->request->get('nbenf')); $i++) {
                $accompagnants = new Accompagnantsreservationvo();
                if(is_array($request->request->get('nbbebe'))){
                $accompagnants->setNbbebe($request->request->get('nbbebe')[$i]);
                }else{
                $accompagnants->setNbbebe($request->request->get('nbbebe'));
               
                }
                if(is_array($request->request->get('nbenf'))){
                    $accompagnants->setNbenfants($request->request->get('nbenf')[$i]);
                //    $reservation->setNbenfants($request->request->get('nbenf')[$i]);

                }else{
                    $accompagnants->setNbenfants($request->request->get('nbenf'));
                 //   $reservation->setNbenfants($request->request->get('nbenf'));

                }
                if(is_array($request->request->get('nbad'))){
                    $accompagnants->setNbadultes($request->request->get('nbad')[$i]);
                  //  $reservation->setNbadultes($request->request->get('nbad')[$i]);

                }else{
                    $accompagnants->setNbadultes($request->request->get('nbad'));
                 //   $reservation->setNbadultes($request->request->get('nbad'));

                }
             //   $reservation->setEtat(1);
             //   $reservation->setNbnuits(2);
             //   $em->persist($reservation);
            // var_dump($request->request->get('datesejour')).die;
             $accompagnants->setReservationcircuit($request->request->get('datesejour'));
                $accompagnants->setCiruit($request->request->get('datesejour'));
             //   $em->flush();

              //  dump($reservation->getId()).die;
              //  $accompagnants->setReservationcircuit($reservation->getId());
                $em->persist($accompagnants);
                $em->flush();
                }
            
            
           
             
                //  $this->get('session')->getFlashBag()->add(
                //      'success',
                //      'Votre reservation a  été faite avec succée! '
                //  );
            return $this->redirect($this->generateUrl('reservervo', array("id" => $sejour->getId()
               ,"nblignes"=>count($request->request->get('nbenf')))));

        
          }
      


        return $this->render('default/reservervo.html.twig', [
            'c' => $sejour,
            'img'    =>$voimg ,
            'dates'=>$vopardate,
             'datesvo'=>$datesvo,
            //'form' => $form->createView()

             ]);
   
       }

         
         
         
         
         
         
         
         
         
         


  /**
     * @Route("/listemaisondhote", name="listemaisondhote")
     */
    public function listemaisondhoteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));

        $maisons = $this->getDoctrine()->getRepository("BtobHotelBundle:Maisondhote")->findAll();
          $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
             $maisons,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',3)
        );
        $maisonsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Maisonhoteimg','i')
            ->groupBy('i.maisondhote')  
            ->getQuery()
            ->getResult();
        return $this->render('default/listemaisondhote.html.twig', [
               
            'maison'=>$pagination,
            'img'=>$maisonsimg,
                
              
             ]);
    }

      /**
     * @Route("/detailsmaisondhotes/{id}", name="detailsmaisondhote")
     */
    public function detailsmaisondhoteAction(Maisondhote $maison ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $maisonsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Maisonhoteimg','i')
            ->where('i.maisondhote = :m')  
            ->setParameter('m', $maison)
            ->getQuery()
            ->getResult();
          //  dump($maisonsimg).die;
        return $this->render('default/detailsmaisondhote.html.twig', [
               
            'maison'=>$maison,
            'img'=>$maisonsimg,
                
              
             ]);
    }



      /**
     * @Route("/reservermaisondhote/{id}", name="reservermaisondhote")
     */
    public function reservermaisondhoteAction(Maisondhote $maisondhote , Request $request)
    {
    
            $em = $this->getDoctrine()->getManager();
            $coordonnees = new Coordonneesmaisondhote();
            $maisonsimg =  $em->createQueryBuilder()
            ->select('i')
            ->from('BtobHotelBundle:Maisonhoteimg','i')
            ->where('i.maisondhote = :m')  
            ->setParameter('m', $maisondhote)
            ->getQuery()
            ->getResult();
            $form = $this->createForm('Btob\HotelBundle\Form\CoordonneesmaisondhoteType', $coordonnees);
            $form->handleRequest($request);
            if ($request->getMethod() == 'POST') {
                $Reservation = new Reservationmaisondhote();
                //dump($request->request->get('datedebut'), $request->request->get('datefin')).die;
                $Reservation->setDatedebut( new DateTime($request->request->get('datedebut')));
                $Reservation->setDatefin( new DateTime($request->request->get('datefin')));
                $Reservation->setNbadultes($form["adulte"]->getData());
                $Reservation->setNbenfants($form["adultes"]->getData());

                $tz = new DateTimeZone("America/New_York");
                $date_1 = new DateTime($request->request->get('datedebut'),$tz);
                $date_2 = new DateTime($request->request->get('datefin'),$tz);
               // var_dump(  $date_2->diff($date_1)).die;
               $nbpersonnes = $form["adulte"]->getData() + $form["adultes"]->getData();
               
                $Reservation->setTotal($maisondhote->getPrix() * ($date_2->diff($date_1)->format("%a") * $nbpersonnes));
                $Reservation->setNbnuits($date_2->diff($date_1)->format("%a"));
                $Reservation->setNom($form["nom"]->getData());
                $Reservation->setPrenom($form["prenom"]->getData());
                $Reservation->setMobile($form["mobile"]->getData());
                $Reservation->setMaisondhote($maisondhote);
                $em->persist($Reservation);
                $em->flush();

                $coordonnees->setReservationmaisondhote($Reservation->getId());

                $em->persist($coordonnees);
                $em->flush();
                
          
                $this->get('session')->getFlashBag()->add(
                   'success',
                   'Votre réservation  est faite avec succée! Le prix total de la réservation:'.$Reservation->getTotal().'TND.' 
               );
                $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation à la maison de  ' . $maisondhote->getVille().', Référence:'.$Reservation->getId())
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('reservation@rusticavoyages.com')
                        ->setBody("Nouvelle réservation à la maison de".$maisondhote->getVille()." à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()."  Le total de la réservation est de " .$Reservation->getTotal()." TND . veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
                return $this->redirect($this->generateUrl('reservermaisondhote', array("id" => $maisondhote->getId()
               ,)));
            }
       
            return $this->render('default/coordonneesmaisondhotes.html.twig', [
               
               'img'=>$maisonsimg,
               'form' => $form->createView(),
               
             
            ]);
        }

  /**
     * @Route("/billettrie", name="billettrie")
     */
    public function  billettrieAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
      $coordonnees = new Billetriecoordonnes();
        
     $form = $this->createForm('Btob\HotelBundle\Form\BilletriecoordonnesType', $coordonnees);
      $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
    //     $coordonnees->setCIN($request->request->get('cin'));
        $coordonnees->setCivilite($request->request->get('civ'));
    //     $coordonnees->setPrenom($request->request->get('pname'));
    //     $coordonnees->setNom($request->request->get('name'));
    //     $coordonnees->setEmail($request->request->get('email'));
    //     $coordonnees->setMobile($request->request->get('tel'));
    //     $coordonnees->setAdresee($request->request->get('adresse'));
    //     $coordonnees->setCodepostal($request->request->get('cp'));
    //     $coordonnees->setVille($request->request->get('ville'));
         $coordonnees->setPays($request->request->get('pays'));
        // $coordonnees->setVilledepart($request->request->get('depart'));
         //$coordonnees->setVillearrivee($request->request->get('arrive'));
      //   dump($request->request->get('dated')).die;
         $datear = str_replace("/","-", $request->request->get('dated')[0]);
         $dater = str_replace("/","-", $request->request->get('dater')[0]);
        $coordonnees->setDatedepart(new DateTime($datear));
         $coordonnees->setDatearrivee(new DateTime($dater));
         $coordonnees->setTypevol($request->request->get('type'));
         $coordonnees->setCompanie($request->request->get('compagnie'));
         $coordonnees->setClasse($request->request->get('classes'));
         

         if (!is_null($request->request->get('named')) or !is_null($request->request->get('prenomad')) or !is_null($request->request->get('passad'))){
            //dump($request->request->get('named')).die;
        //foreach ($request->request->get('named') as $key => $value) {
            for ($i = 0; $i < count($request->request->get('named')); $i++) {
                $passagers = new Passagersbilletrie();
                if(is_array($request->request->get('named'))){
                $passagers->setNom($request->request->get('named')[$i]);
                }else{
                $passagers->setNom($request->request->get('named'));

                }
                if(is_array($request->request->get('named'))){
                    $passagers->setPrenom($request->request->get('prenomad')[$i]);

                }else{
                    $passagers->setPrenom($request->request->get('prenomad'));
    
                    }

                $passagers->setNumpassport($request->request->get('passad')[$i]);
                $passagers->setBillettriecoordonnees($coordonnees);
                $em->persist($passagers);
     
           }

           
 
       }
       if (!is_null($request->request->get('depart')) or !is_null($request->request->get('arrive')) or !is_null($request->request->get('dated'))){

       for ($i = 0; $i < is_countable($request->request->get('depart')); $i++) {
        $destination = new Destionationbilletrie();
        if(is_array($request->request->get('datedep'))){
            $datedepart = str_replace("/","-", $request->request->get('datedep')[$i]);

        }else{
            $datedepart = str_replace("/","-", $request->request->get('datedep'));

        }
 //dump($request->request->get('datedep')[$i]).die;
        $destination->setDatedepart(new DateTime($datedepart));
        $destination->setVilled($request->request->get('depart')[$i]);
        $destination->setVillea($request->request->get('arrive')[$i]);
        $destination->setBillettriecoordonnees($coordonnees);

        $em->persist($destination);
           
         }
        }
       
        $em->persist($coordonnees);
       //var_dump($coordonnees).die;
        $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Votre demande  est envoyée avec succée! '
            );
             $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation de billets' )
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('reservation@rusticavoyages.com')
                        ->setBody("Nouvelle réservation de billet à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()." Veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
       return $this->redirect($this->generateUrl('billettrie'));

        }
        return $this->render('default/billettrie.html.twig', [
            
         'form' => $form->createView()
             
           
          ]);

    }


     /**
     * @Route("/circuit", name="circuit")
     */
    public function CircuitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $circuit =   $em->createQueryBuilder()
        ->select('c')
        ->from('BtobCuircuitBundle:Cuircuit','c')
        ->where('c.active = 1')
        ->getQuery()
        ->getResult();
         $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
             $circuit,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit',3)
        );
        $imgcuir = $em->createQueryBuilder()
        ->select('i')
        ->from('BtobCuircuitBundle:Imgcuir','i')
        ->groupBy('i.cuircuit')  
        ->getQuery()
        ->getResult();


        return $this->render('default/circuit.html.twig', [
            'circuit' => $pagination,
            'img'    =>$imgcuir ,
             ]);
   
       }

         /**
     * @Route("/detailscircuit/{id}", name="detailscircuit")
     */
    public function DetailscircuitAction(Cuircuit $circuit ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $listec =   $em->createQueryBuilder()
        ->select('c')
        ->from('BtobCuircuitBundle:Cuircuit','c')
        ->where('c.active = 1')
        ->getQuery()
        ->getResult();
        $imgcuir = $em->createQueryBuilder()
        ->select('c')
        ->from('BtobCuircuitBundle:Imgcuir','c')
        ->where('c.cuircuit = :c')  
        ->setParameter('c', $circuit)
        ->getQuery()
        ->getResult();
        $imgc = $em->createQueryBuilder()
        ->select('c')
        ->from('BtobCuircuitBundle:Imgcuir','c')
        ->groupBy('c.cuircuit')
        ->getQuery()
        ->getResult();

        return $this->render('default/detailscircuit.html.twig', [
            'listec'=>$listec,
            'c' => $circuit,
            'img'    =>$imgcuir ,
            'imgc'    =>$imgc ,
             ]);
   
       }
   /**
     * @Route("/reservercircuit/{id}", name="reservercircuit")
     */
    public function reservercircuitAction(Cuircuit $circuit ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $imgcuir = $em->createQueryBuilder()
        ->select('c')
        ->from('BtobCuircuitBundle:Imgcuir','c')
        ->where('c.cuircuit = :c')  
        ->setParameter('c', $circuit)
        ->getQuery()
        ->getResult();
        $em = $this->getDoctrine()->getManager();
     //   $reservation = new Reservationcircuit();
     //   $form = $this->createForm('Btob\CuircuitBundle\Form\ReservationcircuitfrontType', $reservation);
      //  $form->handleRequest($request);
          if ($request->getMethod() == 'POST') {
          //    dump($request->request->get('nbad'),$request->request->get('nbenf')).die;
              for ($i = 0; $i < count($request->request->get('nbenf')); $i++) {
                $accompagnants = new accompagnantsreservationciruit();
                if(is_array($request->request->get('nbbebe'))){
                $accompagnants->setNbbebe($request->request->get('nbbebe')[$i]);
                }else{
                $accompagnants->setNbbebe($request->request->get('nbbebe'));
               
                }
                if(is_array($request->request->get('nbenf'))){
                    $accompagnants->setNbenfants($request->request->get('nbenf')[$i]);
                  //  $reservation->setNbenfants($request->request->get('nbenf')[$i]);

                }else{
                    $accompagnants->setNbenfants($request->request->get('nbenf'));
                 //   $reservation->setNbenfants($request->request->get('nbenf'));

                }
                if(is_array($request->request->get('nbad'))){
                    $accompagnants->setNbadultes($request->request->get('nbad')[$i]);
                  //  $reservation->setNbadultes($request->request->get('nbad')[$i]);

                }else{
                    $accompagnants->setNbadultes($request->request->get('nbad'));
                //    $reservation->setNbadultes($request->request->get('nbad'));

                }
             //   $reservation->setEtat(1);
             //   $reservation->setNbnuits(2);
              //  $em->persist($reservation);
                $accompagnants->setCiruit($circuit->getId());
               // $em->flush();

              //  dump($reservation->getId()).die;
                $accompagnants->setReservationcircuit($circuit->getId());
                $em->persist($accompagnants);
                $em->flush();
                }
            

           
             
                 $this->get('session')->getFlashBag()->add(
                     'success',
                     'Votre reservation a  été faite avec succée! '
                 );
             return $this->redirect($this->generateUrl('reservercr', array("id" => $circuit->getId()
               ,"nblignes"=>count($request->request->get('nbenf')))));

        
          }
      


        return $this->render('default/reservercircuit.html.twig', [
            'c' => $circuit,
            'img'    =>$imgcuir ,
            //'form' => $form->createView()

             ]);
   
       }


  /**
     * @Route("/reservercr/{id}/{nblignes}", name="reservercr")
     */
    public function reservercrAction(Cuircuit $circuit , $nblignes, Request $request)
    {
    
            $em = $this->getDoctrine()->getManager();
            $listeacc = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobCuircuitBundle:accompagnantsreservationciruit','i')
            ->where('i.ciruit = :s')  
            ->setParameter('s', $circuit->getId())
             ->orderBy('i.id', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults($nblignes)
            ->getQuery()
            ->getResult(); 
        //    var_dump($listeacc).die;
            // foreach($listeacc as $liste){
            //     var_dump($liste->getId()).die;
            // }
       $total = 0;
       $nbvoyageurs =0;
        //   var_dump($listeacc).die;
            if ($request->getMethod() == 'POST') {
                     foreach($listeacc as $liste){ 
                      // var_dump($liste->getNbadultes()).die;
                        $nbvoyageurs = $nbvoyageurs + ($liste->getNbadultes() + $liste->getNbenfants()) ;
                       
                          for ($i = 1; $i <= $liste->getNbadultes(); $i++) {
                             
                         $coordonnees = new Coordonneescr();
                         $coordonnees->setNom($request->request->get('noma')[$i]);
                         $coordonnees->setPrenom($request->request->get('prenoma')[$i]);
                         $coordonnees->setDatenaissance($request->request->get('datea')[$i]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
                        for ($e = 1; $e <= $liste->getNbenfants(); $e++) {
                             
                         $coordonnees = new Coordonneescr();
                         $coordonnees->setNom($request->request->get('nome')[$e]);
                         $coordonnees->setPrenom($request->request->get('prenome')[$e]);
                         $coordonnees->setDatenaissance($request->request->get('datee')[$e]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
                            for ($b = 1; $b <= $liste->getNbbebe(); $b++) {
                             
                         $coordonnees = new Coordonneescr();
                         $coordonnees->setNom($request->request->get('nome')[$b]);
                         $coordonnees->setPrenom($request->request->get('prenome')[$b]);
                         $coordonnees->setDatenaissance($request->request->get('datee')[$b]);
                         $em->persist($coordonnees);
                          $em->flush();
                          }
               
                     }
           $total = $circuit->getPrix()*$nbvoyageurs;
          // var_dump($total).die;

                return $this->redirect($this->generateUrl('payerreservationcr', array("id" => $circuit->getId(),"total"=>$total,"nblignes"=>$nblignes)));
            }
       
            return $this->render('default/coordonneescr.html.twig', [
               
               'c'=>$circuit,
               'nblignes'=>$nblignes,'listeacc'=>$listeacc
             //  'form' => $form->createView()
               
             
            ]);
        }



   /**
     * @Route("/payerreservationcr/{id}/{total}/{nblignes}", name="payerreservationcr")
     */
    public function payerreservationcrAction(Cuircuit $circuit ,$nblignes, $total,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
          $payement = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Payement')
            ->find(1);
          $listeacc = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobCuircuitBundle:accompagnantsreservationciruit','i')
            ->where('i.ciruit = :s')  
            ->setParameter('s', $circuit->getId())
             ->orderBy('i.id', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults($nblignes)
            ->getQuery()
            ->getResult(); 
       
          $imgcuir = $em->createQueryBuilder()
            ->select('i')
            ->from('BtobCuircuitBundle:Imgcuir','i')
            ->where('i.cuircuit = :cuircuit') 
            ->setParameter('cuircuit',$circuit)
            ->getQuery()
            ->getResult();
        $coordonnees = new Coordonneesreservationcr();
        
        $form = $this->createForm('Btob\CuircuitBundle\Form\CoordonneesreservationcrType', $coordonnees);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            $reservationcr = new Reservationcircuit();
            $reservationcr->setTotal($total);
            $reservationcr->setCuircuit($circuit);
            $reservationcr->setTypepayement($request->request->get('group'));
            $reservationcr->setEtat(0);
            $datef = $circuit->getDatef();
            $dated = $circuit->getDated();
            $interval = $datef->diff($dated);
            $reservationcr->setNbnuits($interval->format('%R%a'));
            $reservationcr->setNbadultes(0);
             $reservationcr->setNbEnfants(0);
            
            $coordonnees->setCr($circuit->getId());
            $em->persist($coordonnees);
            $em->persist($reservationcr);
            $em->flush();
          $message = \Swift_Message::newInstance()
                        ->setSubject('Une nouvelle réservation '.$circuit->getType().' à '.$circuit->getVille().', Référence:'.$reservationcr->getId())
                        ->setFrom(array($form["email"]->getData()=>$form["nom"]->getData()))
                        ->setTo('test@rusticavoyages.com')
                        ->setBody("Nouvelle réservation à ".$circuit->getVille()." à été éffectuée de la part de ". $form["nom"]->getData(). $form["prenom"]->getData()."  Le total de la réservation est de " .$total." TND . veuillez le contactez sur le ".$form["mobile"]->getData());
                         $this->get('mailer')->send($message);
           $this->addFlash("success", "Votre réservation à été faite avec succès ..");

                    return $this->redirect($this->generateUrl('voyagesorganisees'));
      
        }
         
        
        
        
   
        return $this->render('default/totalreservationcr.html.twig', [
            'sejour'=>$circuit,'total'=>$total,
           'reservation'=>$reservationcr,
           'payement' => $payement,'sejourimg'=>$imgcuir,'listeacc'=>$listeacc,
           'form' => $form->createView()
           
         
        ]);
    }
       





















/**
     * @Route("/circuitpersonnalisee/{id}", name="circuitpersonnalisee")
     */
    public function reservercircuitpersonnaliseeAction(Cuircuit $circuit ,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation = new Reservationcircuit();
        $form = $this->createForm('Btob\CuircuitBundle\Form\ReservationcircuitfrontType', $reservation);
        $form->handleRequest($request);
          if ($request->getMethod() == 'POST') {





                 $this->get('session')->getFlashBag()->add(
                     'success',
                     'Votre reservation a  été faite avec succée! '
                 );
            return $this->redirect($this->generateUrl('circuit'));

        
          }
      


        return $this->render('default/circuitpersonnalise.html.twig', [
            'c' => $circuit,
            'form' => $form->createView()

             ]);
   
       }
       /**
     * @Route("/contact", name="contact")
     */
public function contactAction(Request $request){
    
//   $infoagence = $em->getRepository('BtobAgenceBundle:Info')->find(1);

    if ($request->getMethod() == 'POST') {
        //dump( $request->request->get('nom')).die;
    $message = \Swift_Message::newInstance()
    ->setSubject($request->request->get('sujet'))
    ->setFrom(array($request->request->get('email')=>$request->request->get('nom')))
    ->setTo('contact@rusticavoyages.com')
    ->setBody($request->request->get('message'));
        $this->get('mailer')->send($message);
        
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Votre message a  été envoyé avec succée! '
        );

        return $this->redirectToRoute('contact');
    }
        return $this->render('default/contact.html.twig', [
            // 'info'=>$infoagence,
    
     ]);


    }
    
}