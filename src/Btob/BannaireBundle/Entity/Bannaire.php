<?php

namespace Btob\BannaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Bannaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\BannaireBundle\Entity\BannaireRepository")
 */
class Bannaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="path" , type="string", length=255, nullable=true)
     */
    public $path;
    /**
     * @var boolean
     *
     */
    private $image;
    /**
     * @Assert\File(maxSize="6000000")
     */
    public $file;
    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean", nullable=true)
     */
    private $act;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", nullable=true)
     */
    private $lien;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string" , length=255, nullable=true)
     */
    private $description;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Bannaire
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set lien
     *
     * @param string $lien
     * @return Bannaire
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string 
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bannaire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }



    public function getWebPath() {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }
	
    protected function getUploadRootDir() {
        // le chemin absolu du r�pertoire o� les documents upload�s doivent �tre sauvegard�s
        return __DIR__ . '/../../../../public_html/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // on se d�barrasse de � __DIR__ � afin de ne pas avoir de probl�me lorsqu'on affiche
        // le document/image dans la vue.
        return 'uploads/barnier';
    }

    // propri�t� utilis� temporairement pour la suppression
    private $filenameForRemove;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->file) {
            $this->path = $this->file->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->file) {
            return;
        }
        $sizes = getimagesize($this->file);
        $old_w = $sizes[0];
        $old_h = $sizes[1];
        $new_w = 1024;
        $new_h = 348;
        if (($old_w < $new_w)&&($old_h < $new_h)) {
            $this->setImage(0);

            return;
        } else {
            $this->setImage(1);
            // vous devez lancer une exception ici si le fichier ne peut pas
            // �tre d�plac� afin que l'entit� ne soit pas persist�e dans la
            // base de donn�es comme le fait la m�thode move() de UploadedFile
            $this->file->move($this->getUploadRootDir(), $this->id . '.' . $this->file->getClientOriginalName());
            $this->path = $this->file->getClientOriginalName();
            unset($this->file);
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove() {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }
    }

    public function getAbsolutePath() {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->id . '.' . $this->path;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }
    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }



    /**
     * Set path
     *
     * @param string $path
     * @return Barnier
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
}
