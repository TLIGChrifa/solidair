<?php

namespace Btob\BannaireBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\BannaireBundle\Entity\Bannaire;
use Btob\BannaireBundle\Form\BannaireType;

/**
 * Bannaire controller.
 *
 */
class BannaireController extends Controller
{

    /**
     * Lists all Bannaire entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobBannaireBundle:Bannaire')->findAll();

        return $this->render('BtobBannaireBundle:Bannaire:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Bannaire entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Bannaire();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $entity->upload();
            $image = $entity->getImage();
            if ($image == 1) {
                $lien = $form->get("lien")->getData();
                if($lien == null){
                    $entity->setLien("#");
                }
                $em->merge($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('barnierjouteradmin', " insert success");

                return $this->redirect($this->generateUrl('bannaire'));
            } else {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('barnierprbtailleadmin', " probléme de taille d'image il faut que le minimum de width est 1024 pixel et pour height est 348 pixel ");
                return $this->redirect($this->generateUrl('bannaire_new'));
            }
           // return $this->redirect($this->generateUrl('bannaire_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobBannaireBundle:Bannaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Bannaire entity.
     *
     * @param Bannaire $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bannaire $entity)
    {
        $form = $this->createForm(new BannaireType(), $entity, array(
            'action' => $this->generateUrl('bannaire_create'),
            'method' => 'POST',
        ));

       // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Bannaire entity.
     *
     */
    public function newAction()
    {
        $entity = new Bannaire();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobBannaireBundle:Bannaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Bannaire entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Bannaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bannaire entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBannaireBundle:Bannaire:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Bannaire entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Bannaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bannaire entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobBannaireBundle:Bannaire:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Bannaire entity.
    *
    * @param Bannaire $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Bannaire $entity)
    {
        $form = $this->createForm(new BannaireType(), $entity, array(
            'action' => $this->generateUrl('bannaire_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Bannaire entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobBannaireBundle:Bannaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bannaire entity.');
        }

     //   $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
           $file = $editForm->get("file")->getData();



            if($file != null){
                $entity->upload();

                $image = $entity->getImage();

                if ($image == 1) {
                $lien = $editForm->get("lien")->getData();
                if($lien == null){
                    $entity->setLien("#");
                }
                $em->merge($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('barnierjouteradmin', " insert success");

                return $this->redirect($this->generateUrl('bannaire'));
            } else {
                $this->get('session')->getFlashBag()->add('barnierprbtailleadminedit', " probléme de taille d'image il faut que le minimum de width est 1024 pixel et pour height est 348 pixel ");
                return $this->redirect($this->generateUrl('bannaire_edit', array('id' => $id)));
            }

            }
           // return $this->redirect($this->generateUrl('bannaire_edit', array('id' => $id)));
        }
        return $this->redirect($this->generateUrl('bannaire'));

        /* return $this->render('BtobBannaireBundle:Bannaire:edit.html.twig', array(
             'entity'      => $entity,
             'edit_form'   => $editForm->createView(),
           //  'delete_form' => $deleteForm->createView(),
         ));*/
    }
    /**
     * Deletes a Bannaire entity.
     *
     */
    public function deleteAction( $id)
    {
          $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobBannaireBundle:Bannaire')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('bannaire'));
    }

    /**
     * Creates a form to delete a Bannaire entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bannaire_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
