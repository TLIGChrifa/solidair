<?php

namespace Btob\BannaireBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BannaireType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('file', NULL, array('label' =>'Image', 'required' => false))
            ->add('act', NULL, array('label' =>'Active', 'required' => false))
            ->add('lien', NULL, array('label' =>'Lien', 'required' => false))
            ->add('description', NULL, array('label' =>'Titre', 'required' => true))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\BannaireBundle\Entity\Bannaire'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_bannairebundle_bannaire';
    }
}
