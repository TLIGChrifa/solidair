<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Villet;
use Symfony\Component\HttpFoundation\Request;
use Btob\TransfertBundle\Form\VilletType;
use Symfony\Component\HttpFoundation\JsonResponse;

class VilletController extends Controller {

    public function indexAction() {
        $villet = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Villet')
                ->findAll();
        return $this->render('BtobTransfertBundle:Villet:index.html.twig', array('entities' => $villet));
    }

    public function addAction() {
        $villet = new Villet();
        $form = $this->createForm(new VilletType(), $villet);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($villet);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_villet_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Villet:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $villet = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Villet')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VilletType(), $villet);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_villet_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Villet:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Villet $villet) {
        $em = $this->getDoctrine()->getManager();

        if (!$villet) {
            throw new NotFoundHttpException("Ville non trouvée");
        }
        $em->remove($villet);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_villet_homepage'));
    }

    public function ajxPaystAction() {
        $request = $this->get('request');
        $id=$request->request->get("id");
        $villet = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Villet')
                ->findByPayst($id);
        
        return $this->render('BtobTransfertBundle:Villet:option.html.twig', array('entity'=>$villet)
        );
    }

}
