<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Transferts;
use Btob\TransfertBundle\Form\TransfertsType;
use Symfony\Component\HttpFoundation\JsonResponse;


class TransfertsController extends Controller
{
    public function dashstatAction()
    {
        $transferts = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transferts')->findBy(array('act' => 1));
        $tab = array();
        foreach ($transferts as $value) {
            $tab[$value->getVille()->getName()][] = 1;
        }
        $ArrFinal = array();
        foreach ($tab as $key => $value) {
            $ArrFinal[$key] = count($value);
        }
        //Tools::dump($ArrFinal);
        return new JsonResponse($ArrFinal);
    }

    public function indexAction()
    {
        $transferts = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Transferts')
            ->findAll();
        return $this->render('BtobTransfertBundle:Transferts:index.html.twig', array('entities' => $transferts));
    }

    public function addAction()
    {

        $transferts = new Transferts();
        $form = $this->createForm(new TransfertsType(), $transferts);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $villet = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($request->request->get("villet"));
                $villeta = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($request->request->get("villeta"));

                $transferts->setVillet($villet);
                $transferts->setVilleta($villeta);
                $em->persist($transferts);
                $em->flush();


                return $this->redirect($this->generateUrl('btob_transferts_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Transferts:form.html.twig', array(
            'form' => $form->createView(),
            'transferts' => $transferts
        ));
    }

    public function editAction($id)
    {

        $request = $this->get('request');
        $transferts = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Transferts')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TransfertsType(), $transferts);
        $form->handleRequest($request);

        if ($form->isValid()) {

         
            $villet = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($request->request->get("villet"));
            $villeta = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($request->request->get("villeta"));

            $transferts->setVillet($villet);
            $transferts->setVilleta($villeta);
            $em->flush();
           


            return $this->redirect($this->generateUrl('btob_transferts_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Transferts:edit.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
                'transferts' => $transferts
            )
        );
    }

    public function deleteAction(transferts $transferts)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$transferts) {
            throw new NotFoundHttpException("transferts non trouvée");
        }
        $em->remove($transferts);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_transferts_homepage'));
    }

}
