<?php

namespace Btob\TransfertBundle\Controller;

use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\TransfertBundle\Entity\Transfert;
use Btob\TransfertBundle\Entity\Transfertsperiode;
use Btob\TransfertBundle\Entity\Reservationtransfert;
use Btob\TransfertBundle\Entity\Tcategories;
use Btob\TransfertBundle\Form\TransfertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;

class DefaultController extends Controller
{

    
    public function optionAction()
    {
       


       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

      
        $payst = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->findActiv();
        $paystid = "1";
        $villet = "";
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
      if ($request->getMethod() == 'POST') {
            
    
            $villet = $request->request->get("villet");
            
            $session->set('villet', $villet);
            $villetd = $request->request->get("villetd");
            $session->set('villetd', $villetd);
            
            $dated = $request->request->get("dated");
            $session->set('dated', $dated);
            
            $payst = $request->request->get("payst");
            $session->set('payst', $payst);
            
            $nbpersonne = $request->request->get("nbpersonne");
            $session->set('nbpersonne', $nbpersonne);
            
            $type = $request->request->get('type');
            $session->set('type', $type);
            
            
            $dater = $request->request->get("dater");
            $session->set('dater', $dater);
            
            $categorie = $request->request->get('categorie');
            $session->set('categorie', $categorie);
           
            
          return $this->redirect($this->generateUrl('btob_search_transfert'));

            
        }

        return $this->render('BtobTransfertBundle:Default:option.html.twig', array(
                'payst' => $payst,
                'paystid' => $paystid,
                'villet' => $villet,
                'dated' => $dated
               
            )
        );
    }
 public function searchAction()

    {
       $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $villet = $request->request->get("villet");
        $villetd = $request->request->get("villetd");
        $dated = $request->request->get("dated");
        $payst = $request->request->get("payst");
        $nbpersonne = $request->request->get("nbpersonne");
    
        $type = $request->request->get("type");
        
        
        $villedep = intval($villet);
        $villedes = intval($villetd);
        $paystres = intval($payst);
        $intnbpersonne = intval($nbpersonne);
        
        
        $tab = explode("/", $dated);

         $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);
           
                   if($type=="Aller-Retour")
        {
         $categorie = $request->request->get("categorie");
        $dater = $request->request->get("dater");
           $tab1 = explode("/", $dater);

         $d2 = new \DateTime($tab1[2] . "-" . $tab1[1] . "-" . $tab1[0]);
           $nbjour = $d2->diff($d1);
            $nbjour = $nbjour->days+1; 
            
        }else{
            
            $categorie = null;
            $d2 = null;
            $nbjour = 1;
            
        } 
         
         
         
             $villed = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($villedep);
             $villedess = $this->getDoctrine()->getRepository('BtobTransfertBundle:Villet')->find($villedes);
             $pays = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->find($paystres);
   
         
         
          $extransf = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->ListByVilledAndvillea($d1,$paystres,$villedep,$villedes);
       if($extransf)
       {
              $transport = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->ListTransport($extransf->getId());
       
              
              
       }else{
         $transport= array(); 
         
       }
          $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $transport,
            $request->query->get('page', 1)/*page number*/,
            5/*limit per page*/
        );   
     $session->set('villed', $villed);
     $session->set('villedess', $villedess);
     $session->set('paystres', $paystres);
     $session->set('nbpersonne', $nbpersonne);
     $session->set('d1', $d1);
     
     $session->set('type', $type);
     $session->set('d2', $d2);
     $session->set('nbjour', $nbjour);
     $session->set('categorie', $categorie);
     
        return $this->render('BtobTransfertBundle:Default:search.html.twig', array(
                'villet' => $villet,
                'villetd' => $villetd,
                'villed' => $villed,
                'villedess' => $villedess,
                'pays' => $pays,
                'payst' => $payst,
                'type' => $type,
                'd1' => $d1,
                 'd2' => $d2,
                'nbjour' => $nbjour,
                'categorie' => $categorie,
                'nbpersonne' => $nbpersonne,
                'pagination' => $pagination,
                'intnbpersonne' => $intnbpersonne
            )
        );  
        
        
    }  
    
    public function inscriptionAction()

    {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        
        $villet = $session->get("villed");
        $villetd = $session->get("villedess");
        $dated = $session->get("d1");
        $payst = $session->get("paystres");
        $nbpersonne = $session->get("nbpersonne");
        
        
        $type = $session->get("type");
        $dater = $session->get("d2");
        $nbjour = $session->get("nbjour");
        $categorie = $session->get("categorie");
        
        $User = $this->get('security.context')->getToken()->getUser();
       $pays = $this->getDoctrine()->getRepository('BtobTransfertBundle:Payst')->find($payst);
        $value = $request->get('_route_params');
        
        $trans =$value["transportid"];
        $periode=$value["periode"];
        $qte=$value["qte"];
        
        
        $transp = intval($trans);
        $qantite= intval($qte);
        $perpric= intval($periode);
      $transport = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transport')->find($transp);
      $periodprice = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->find($perpric);

      
            if($type=="Aller-Simple")
      {
      $price =$qantite*$periodprice->getPrice();
      if($qantite<=$transport->getQte())
         {
          
          $msg= "Disponible";
         }else{
            $msg= "Sur Demande"; 
             
         }
       
      }else{
          if($nbjour==1)
          {
              if($qantite<=$transport->getQte())
                 {
          
               $msg= "Disponible";
               }else{
              $msg= "Sur Demande"; 
             
                }
                
            $price =$qantite*$periodprice->getPrice()*2;  
            
          }
          else{
              
            $price =$qantite*$periodprice->getPrice()*2;
            $msg= "Sur Demande"; 
          }
          
      }
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            
            $defaultd = $request->request->get('tp-defaultd');
            $pointd = $request->request->get('pointd');
            $pointa = $request->request->get('pointa');
            
            $bagage = $request->request->get('bagage');
            
            if($type=="Aller-Retour")
                
            {
              $defaultrd = $request->request->get('tp-defaultrd');
            $pointrd = $request->request->get('pointrd');
            $pointra = $request->request->get('pointra');
            $categorie= $categorie;
                
            }else{
             $defaultrd = null;
            $pointrd = null;
            $pointra = null; 
            $categorie= null;
                
            }
            
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {

                $em->persist($client);
                $em->flush();
                $session->set('namead', $request->request->get('namead'));
                 $session->set('ageadult', $request->request->get('ageadult'));
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                
                $nameadulte = $session->get('namead');
                $nbageadult = $session->get('ageadult');
                $reservation = new Reservationtransfert();
                $reservation->setClient($client);
               
                $reservation->setUser($User);
                
                
                $reservation->setDated($dated);
                
                $reservation->setVilledep($villet);
                $reservation->setVilledes($villetd);
                
                $reservation->setPays($pays->getName());
                
                $reservation->setTotal($price);
                $reservation->setQte($qte);
                 $reservation->setHeurd($defaultd);
                 $reservation->setPointd($pointd);
                 $reservation->setBag($bagage);
                $reservation->setPointa($pointa);
                
                $reservation->setDater($dater);
                $reservation->setHeurr($defaultrd);
                 $reservation->setPointrd($pointrd);
                 $reservation->setPointra($pointra);
                 $reservation->setType($type);
                 $reservation->setCategorie($categorie);
                 $reservation->setMsg($msg);
                $reservation->setNbjour($nbjour);
                
                $reservation->setTransport($transport);
                
                $reservation->setNamead(json_encode($nameadulte));
                $reservation->setAgeadult(json_encode($nbageadult));
                $em->persist($reservation);
                $em->flush();

                }

            return $this->redirect($this->generateUrl('btob_transfertt_homepage'));



            } else {
                echo $form->getErrors();
            }
           


           
        
       
        
        return $this->render('BtobTransfertBundle:Default:inscription.html.twig', array(
            
                'villed' => $villet,
                'villea' => $villetd,
                'dated' => $dated,
                'qantite' => $qantite,
                'transp' => $transp,
                'price' => $price,
                'type' => $type,
                'msg' => $msg,
                'transport' => $transport,
                'periodprice' => $periodprice,
                'form' => $form->createView(),
                'pays' => $payst,
                'nbpersonne' => $nbpersonne
                

            )
        );  
    }
}

