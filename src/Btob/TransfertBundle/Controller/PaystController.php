<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Payst;
use Btob\TransfertBundle\Form\PaystType;

class PaystController extends Controller {

    public function indexAction() {
        $payst = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Payst')
                ->findAll();
        return $this->render('BtobTransfertBundle:Payst:index.html.twig', array('entities' => $payst));
    }

    public function addAction() {
        $payst = new Payst();
        $form = $this->createForm(new PaystType(), $payst);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($payst);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_payst_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Payst:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $payst = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Payst')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PaystType(), $payst);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_payst_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Payst:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Payst $payst) {
        $em = $this->getDoctrine()->getManager();

        if (!$payst) {
            throw new NotFoundHttpException("Pays non trouvée");
        }
        $em->remove($payst);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_payst_homepage'));
    }

}
