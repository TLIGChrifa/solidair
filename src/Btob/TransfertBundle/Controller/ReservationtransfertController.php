<?php

namespace Btob\TransfertBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Btob\TransfertBundle\Entity\Reservationtransfert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReservationtransfertController extends Controller
{

    public function indexAction()
    {
       
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $client = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $client = trim($request->request->get('client'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();

       
        $resa = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Reservationtransfert')
            ->findAll();
        $entities = array();
        foreach ($resa as $value) {
            
                $entities[] = $value;
        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {
            if ($agence != "") {
                $id = $agence;
            } else {
                $id = $user->getId();
            }
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $id) {
                    $tabuser[] = $value;
                }
            }
            $entities = $tabuser;
        }
        
        
       
        
        
     
        // test client $client
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        return $this->render('BtobTransfertBundle:Reservationtransfert:index.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'client' => $client,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));
    }

    public function detailAction(Reservationtransfert $reservationtransfert)
    {
        $namead = json_decode($reservationtransfert->getNamead(), true);
        $agead = json_decode($reservationtransfert->getAgeadult(), true);
        return $this->render('BtobTransfertBundle:Reservationtransfert:detail.html.twig', array(
            'entry' => $reservationtransfert,
            'namead' => $namead,
            'agead' => $agead
        ));
    }

    public function deleteAction(Reservationtransfert $reservationtransfert)
    {
        
        $em = $this->getDoctrine()->getManager();

        if (!$reservationtransfert) {
            throw new NotFoundHttpException("Reservation non trouvée");
        }
        $em->remove($reservationtransfert);
        $em->flush();
        
        return $this->redirect($this->generateUrl('btob_reservationtransf_homepage'));
    }
  
}
