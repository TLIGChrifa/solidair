<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Periodeprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\TransfertBundle\Form\PeriodepriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class PeriodepriceController extends Controller
{

    public function indexAction($transfertsperiodeid)
    {
        $transfertsperiode = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Transfertsperiode')
            ->find($transfertsperiodeid);
        $periode = array();
        foreach ($transfertsperiode->getPeriodeprice() as $value) {
           
                $periode[] = $value;
           
        }
        return $this->render('BtobTransfertBundle:Periodeprice:index.html.twig', array(
            "entities" => $periode,
            "transfertsperiodeid" => $transfertsperiodeid
        ));
    }


    public function addAction($transfertsperiodeid)
    {
        $transfertsperiode = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->find($transfertsperiodeid);
      
        $alltransfertsperiode = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->findBy(array('transfertsperiode' => $transfertsperiode));
      
        

        $periodeprice = new Periodeprice();
        $form = $this->createForm(new PeriodepriceType(), $periodeprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $periodeprice->setTransfertsperiode($transfertsperiode);
                $em->persist($periodeprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_periodeprice_homepage', array("transfertsperiodeid" => $transfertsperiodeid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Periodeprice:form.html.twig', array(
            'form' => $form->createView(),
            "transfertsperiode" => $transfertsperiode

        ));
    }



    public function editAction($id, $transfertsperiodeid)
    {
        $transfertsperiode = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->find($transfertsperiodeid);
        $request = $this->get('request');
        $periodeprice = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Periodeprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PeriodepriceType(), $periodeprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allperiodeprice = $this->getDoctrine()->getRepository('BtobTransfertBundle:Periodeprice')->findBy(array('transfertsperiode' => $transfertsperiode));
      
    
       
        if ($form->isValid()) {

            $periodeprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_periodeprice_homepage', array("transfertsperiodeid" => $transfertsperiodeid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Periodeprice:edit.html.twig', array('form' => $form->createView(), 'price' => $periodeprice, 'id' => $id, "transfertsperiode" => $transfertsperiode)
        );
    }

  
    public function deleteAction(Periodeprice $periodeprice, $transfertsperiodeid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$periodeprice) {
            throw new NotFoundHttpException("Price non trouvée");
        }
        $em->remove($periodeprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_periodeprice_homepage', array("transfertsperiodeid" => $transfertsperiodeid)));
    }



}












