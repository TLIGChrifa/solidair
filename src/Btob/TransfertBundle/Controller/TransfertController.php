<?php
namespace Btob\TransfertBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\TransfertBundle\Entity\Transfert;

use Btob\TransfertBundle\Form\TransfertType;



/**
 * Transfert controller.
 *
 */

class TransfertController extends Controller
{

    /**
     * Lists all Transfert entities.
     *
     */

    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();


        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $em->getRepository('BtobTransfertBundle:Transfert')->findAll();

        }else{
            $entities = $em->getRepository('BtobTransfertBundle:Transfert')->findBy(array('agent' => $user));

        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobTransfertBundle:Transfert:index.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));

    }

    /**

     * Creates a new Transfert entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new Transfert();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();



            return $this->redirect($this->generateUrl('transfert_show', array('id' => $entity->getId())));

        }



        return $this->render('BtobTransfertBundle:Transfert:new.html.twig', array(

            'entity' => $entity,

            'form'   => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a Transfert entity.

     *

     * @param Transfert $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(Transfert $entity)

    {

        $form = $this->createForm(new TransfertType(), $entity, array(

            'action' => $this->generateUrl('transfert_create'),

            'method' => 'POST',

        ));



        $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new Transfert entity.

     *

     */

    public function newAction()

    {

        $entity = new Transfert();

        $form   = $this->createCreateForm($entity);



        return $this->render('BtobTransfertBundle:Transfert:new.html.twig', array(

            'entity' => $entity,

            'form'   => $form->createView(),

        ));

    }



    /**

     * Finds and displays a Transfert entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Transfert entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobTransfertBundle:Transfert:show.html.twig', array(

            'entity'      => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Displays a form to edit an existing Transfert entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Transfert entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobTransfertBundle:Transfert:edit.html.twig', array(

            'entity'      => $entity,

            'edit_form'   => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

    * Creates a form to edit a Transfert entity.

    *

    * @param Transfert $entity The entity

    *

    * @return \Symfony\Component\Form\Form The form

    */

    private function createEditForm(Transfert $entity)

    {

        $form = $this->createForm(new TransfertType(), $entity, array(

            'action' => $this->generateUrl('transfert_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));



        $form->add('submit', 'submit', array('label' => 'Update'));



        return $form;

    }

    /**

     * Edits an existing Transfert entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Transfert entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);



        if ($editForm->isValid()) {

            $em->flush();



            return $this->redirect($this->generateUrl('transfert_edit', array('id' => $id)));

        }



        return $this->render('BtobTransfertBundle:Transfert:edit.html.twig', array(

            'entity'      => $entity,

            'edit_form'   => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }

    /**

     * Deletes a Transfert entity.

     *

     */

    public function deleteAction( $id)

    {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('BtobTransfertBundle:Transfert')->find($id);





            $em->remove($entity);

            $em->flush();





        return $this->redirect($this->generateUrl('transfert'));

    }



    /**

     * Creates a form to delete a Transfert entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('transfert_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm()

        ;

    }

}

