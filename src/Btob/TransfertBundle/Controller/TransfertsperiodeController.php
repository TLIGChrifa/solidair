<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Transfertsperiode;
use Symfony\Component\HttpFoundation\Request;
use Btob\TransfertBundle\Form\TransfertsperiodeType;
use Symfony\Component\HttpFoundation\JsonResponse;



class TransfertsperiodeController extends Controller
{

    public function indexAction($transfertsid)
    {
        $transferts = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Transferts')
            ->find($transfertsid);
        $periode = array();
        foreach ($transferts->getTransfertsperiode() as $value) {
           
                $periode[] = $value;
           
        }
        return $this->render('BtobTransfertBundle:Transfertsperiode:index.html.twig', array(
            "entities" => $periode,
            "transfertsid" => $transfertsid
        ));
    }


    public function addAction($transfertsid)
    {
        $transferts = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transferts')->find($transfertsid);
      
        $alltransfertsperiode = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->findBy(array('transferts' => $transferts));
      
        

        $transfertsperiode = new Transfertsperiode();
        $form = $this->createForm(new TransfertsperiodeType(), $transfertsperiode);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $transfertsperiode->setTransferts($transferts);
                $em->persist($transfertsperiode);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_transfertsperiode_homepage', array("transfertsid" => $transfertsid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Transfertsperiode:form.html.twig', array(
            'form' => $form->createView(),
            "transferts" => $transferts

        ));
    }



    public function editAction($id, $transfertsid)
    {
        $transferts = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transferts')->find($transfertsid);
        $request = $this->get('request');
        $transfertsperiode = $this->getDoctrine()
            ->getRepository('BtobTransfertBundle:Transfertsperiode')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TransfertsperiodeType(), $transfertsperiode);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $alltransfertsperiode = $this->getDoctrine()->getRepository('BtobTransfertBundle:Transfertsperiode')->findBy(array('transferts' => $transferts));
      
    
       
        if ($form->isValid()) {

            $transfertsperiode->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_transfertsperiode_homepage', array("transfertsid" => $transfertsid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Transfertsperiode:edit.html.twig', array('form' => $form->createView(), 'periode' => $transfertsperiode, 'id' => $id, "transferts" => $transferts)
        );
    }

  
    public function deleteAction(Transfertsperiode $transfertsperiode, $transfertsid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$transfertsperiode) {
            throw new NotFoundHttpException("Periode non trouvée");
        }
        $em->remove($transfertsperiode);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_transfertsperiode_homepage', array("transfertsid" => $transfertsid)));
    }



}












