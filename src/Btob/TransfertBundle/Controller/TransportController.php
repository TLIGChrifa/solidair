<?php

namespace Btob\TransfertBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\TransfertBundle\Entity\Transport;
use Btob\TransfertBundle\Form\TransportType;
use Btob\TransfertBundle\Entity\Imgtrans;

class TransportController extends Controller {

    public function indexAction() {
        $transport = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Transport')
                ->findAll();
        return $this->render('BtobTransfertBundle:Transport:index.html.twig', array('entities' => $transport));
    }

    public function addAction() {
        $transport = new Transport();
        $form = $this->createForm(new TransportType(), $transport);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($transport);
                $em->flush();
                
                
                if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgtrans();
                        $img->setTransport($transport);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
                return $this->redirect($this->generateUrl('btob_transport_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobTransfertBundle:Transport:form.html.twig', array(
            'entity' => $transport,
            'form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $transport = $this->getDoctrine()
                ->getRepository('BtobTransfertBundle:Transport')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TransportType(), $transport);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgtrans();
                        $img->setTransport($transport);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('btob_transport_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobTransfertBundle:Transport:form.html.twig', array(
            'form' => $form->createView(), 'id' => $id,'entity' => $transport)
        );
    }

    public function deleteAction(Transport $transport) {
        $em = $this->getDoctrine()->getManager();

        if (!$transport) {
            throw new NotFoundHttpException("Transport non trouvée");
        }
        $em->remove($transport);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_transport_homepage'));
    }

}
