<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transport
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\TransportRepository")
 */
class Transport {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;


    /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer" , nullable=true)
     */
    private $capacite;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="qte", type="integer" , nullable=true)
     */
    private $qte;    
     /**
     * @ORM\OneToMany(targetEntity="Periodeprice", mappedBy="transport", cascade={"remove"})
     */
    protected $periodeprice;

    
    
    /**
     * @ORM\OneToMany(targetEntity="Imgtrans", mappedBy="transport")
     */
    protected $imgtrans;
    
 public  function  __construct(){

        $this->imgtrans =new \Doctrine\Common\Collections\ArrayCollection();
    }   
    
    public function __toString() {

        return "" . $this->name;
    }
       
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Transport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Transport
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     * @return Transport
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer 
     */
    public function getCapacite()
    {
        return $this->capacite;
    }
    
    
      /**
     * Add periodeprice
     *
     * @param \Btob\TransfertBundle\Entity\Periodeprice $periodeprice
     * @return Transport
     */
    public function addPeriodeprice(\Btob\TransfertBundle\Entity\Periodeprice $periodeprice)
    {
        $this->periodeprice[] = $periodeprice;

        return $this;
    }

    /**
     * Remove periodeprice
     *
     * @param \Btob\TransfertBundle\Entity\Periodeprice $periodeprice
     */
    public function removePeriodeprice(\Btob\TransfertBundle\Entity\periodeprice $periodeprice)
    {
        $this->periodeprice->removeElement($periodeprice);
    }

    /**
     * Get periodeprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeriodeprice()
    {
        return $this->periodeprice;
    }
    
    /**
     * Set qte
     *
     * @param integer $qte
     * @return Transport
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }    
    
    /**
     * Add imgtrans
     *
     * @param \Btob\TransfertBundle\Entity\Imgtrans $imgtrans
     * @return Cuircuit
     */

    public function addImgtrans(\Btob\TransfertBundle\Entity\Imgtrans $imgtrans)
    {
        $this->imgtrans[] = $imgtrans;
        return $this;
    }

    /**
     * Remove imgtrans
     *
     * @param \Btob\TransfertBundle\Entity\Imgtrans $imgtrans
     */
    public function removeImgtrans(\Btob\TransfertBundle\Entity\Imgtrans $imgtrans)
    {
        $this->imgtrans->removeElement($imgtrans);
    }

    /**
     * Get imgtrans
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgtrans()
    {
        return $this->imgtrans;
    }

}
