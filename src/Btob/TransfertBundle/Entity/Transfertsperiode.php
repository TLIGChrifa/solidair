<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transfertsperiode
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\TransfertsperiodeRepository")
 */
class Transfertsperiode {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Transferts", inversedBy="transfertsperiode")
     * @ORM\JoinColumn(name="transferts_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transferts;

     /**
     * @ORM\OneToMany(targetEntity="Periodeprice", mappedBy="transfertsperiode", cascade={"remove"})
     */
    protected $periodeprice;   

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
       $this->periodeprice = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Transfertsperiode
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Transfertsperiode
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }



    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Transfertsperiode
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Transfertsperiode
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transferts $transferts
     * @return Transfertsperiode
     */
    public function setTransferts(\Btob\TransfertBundle\Entity\Transferts $transferts = null)
    {
        $this->transferts = $transferts;

        return $this;
    }

    /**
     * Get transferts
     *
     * @return \Btob\TransfertBundle\Entity\Transferts 
     */
    public function getTransferts()
    {
        return $this->transferts;
    }
     /**
     * Add periodeprice
     *
     * @param \Btob\TransfertBundle\Entity\Periodeprice $periodeprice
     * @return Transfertsperiode
     */
    public function addPeriodeprice(\Btob\TransfertBundle\Entity\Periodeprice $periodeprice)
    {
        $this->periodeprice[] = $periodeprice;

        return $this;
    }

    /**
     * Remove periodeprice
     *
     * @param \Btob\TransfertBundle\Entity\Periodeprice $periodeprice
     */
    public function removePeriodeprice(\Btob\TransfertBundle\Entity\Periodeprice $periodeprice)
    {
        $this->periodeprice->removeElement($periodeprice);
    }

    /**
     * Get Periodeprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeriodeprice()
    {
        return $this->periodeprice;
    }   
}
