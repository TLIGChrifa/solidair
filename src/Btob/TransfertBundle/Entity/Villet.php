<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Villet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\VilletRepository")
 */
class Villet {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\ManyToOne(targetEntity="Payst", inversedBy="villet")
     * @ORM\JoinColumn(name="payst_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $payst;
    /**
     * @ORM\OneToMany(targetEntity="Transferts", mappedBy="villet", cascade={"remove"})
     */
    protected $transferts;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transferts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Villet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Villet
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set payst
     *
     * @param \Btob\TransfertBundle\Entity\Payst $payst
     * @return Villet
     */
    public function setPayst(\Btob\TransfertBundle\Entity\Payst $payst = null)
    {
        $this->payst = $payst;

        return $this;
    }

    /**
     * Get payst
     *
     * @return \Btob\TransfertBundle\Entity\Payst 
     */
    public function getPayst()
    {
        return $this->payst;
    }

    /**
     * Add transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transferts $transferts
     * @return Villet
     */
    public function addTransferts(\Btob\TransfertBundle\Entity\Transferts $transferts)
    {
        $this->transferts[] = $transferts;

        return $this;
    }

    /**
     * Remove transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transferts $transferts
     */
    public function removeTransferts(\Btob\TransfertBundle\Entity\Transferts $transferts)
    {
        $this->transferts->removeElement($transferts);
    }

    /**
     * Get transferts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransferts()
    {
        return $this->transferts;
    }
    public  function  __toString(){
        return $this->name;
    }
}
