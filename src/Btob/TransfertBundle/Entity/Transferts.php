<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transferts
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\TransfertsRepository")
 */
class Transferts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @ORM\ManyToOne(targetEntity="Payst", inversedBy="transferts")
     * @ORM\JoinColumn(name="payst_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $payst;
    
    /**
     * @ORM\ManyToOne(targetEntity="Villet", inversedBy="transferts")
     * @ORM\JoinColumn(name="villet_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $villet;
    
    
     /**
     * @ORM\ManyToOne(targetEntity="Villet", inversedBy="transferts")
     * @ORM\JoinColumn(name="villeta_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $villeta;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean" , nullable=true)
     */
    private $act;  
    
     /**
     * @ORM\OneToMany(targetEntity="Transfertsperiode", mappedBy="transferts", cascade={"remove"})
     */
    protected $transfertsperiode;   
    
    public function __toString() {

        return "" . $this->payst;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public  function  __construct(){
        $this->dcr = new \DateTime();
        $this->transfertsperiode = new \Doctrine\Common\Collections\ArrayCollection();


    }



    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Transferts
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Transferts
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set pays
     *
     * @param \Btob\TransfertBundle\Entity\Payst $payst
     * @return Transferts
     */
    public function setPayst(\Btob\TransfertBundle\Entity\Payst $payst = null)
    {
        $this->payst = $payst;

        return $this;
    }

    /**
     * Get payst
     *
     * @return \Btob\TransfertBundle\Entity\Payst 
     */
    public function getPayst()
    {
        return $this->payst;
    }

    /**
     * Set villet
     *
     * @param \Btob\TransfertBundle\Entity\Villet $villet
     * @return Transferts
     */
    public function setVillet(\Btob\TransfertBundle\Entity\Villet $villet = null)
    {
        $this->villet = $villet;

        return $this;
    }

    /**
     * Get villeta
     *
     * @return \Btob\TransfertBundle\Entity\Villet 
     */
    public function getVilleta()
    {
        return $this->villeta;
    }
    
    
    
        /**
     * Set villeta
     *
     * @param \Btob\TransfertBundle\Entity\Villet $villeta
     * @return Transferts
     */
    public function setVilleta(\Btob\TransfertBundle\Entity\Villet $villeta = null)
    {
        $this->villeta = $villeta;

        return $this;
    }

    /**
     * Get villet
     *
     * @return \Btob\TransfertBundle\Entity\Villet 
     */
    public function getVillet()
    {
        return $this->villet;
    }
    
     /**
     * Add transfertsperiode
     *
     * @param \Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode
     * @return Transferts
     */
    public function addTransfertsperiode(\Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode)
    {
        $this->transfertsperiode[] = $transfertsperiode;

        return $this;
    }

    /**
     * Remove transfertsperiode
     *
     * @param \Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode
     */
    public function removeTransfertsperiode(\Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode)
    {
        $this->transfertsperiode->removeElement($transfertsperiode);
    }

    /**
     * Get Transfertsperiode
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransfertsperiode()
    {
        return $this->transfertsperiode;
    }   
}
