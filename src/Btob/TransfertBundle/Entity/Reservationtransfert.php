<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationtransfert
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\ReservationtransfertRepository")
 */
class Reservationtransfert
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime")
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="datetime")
     */
    private $dater;
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float" ,nullable=true)
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(name="qte", type="float")
     */
    private $qte;
    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

 
    /**
     * @var array
     *
     * @ORM\Column(name="villedep", type="text",nullable=true)
     */
    private $villedep;
    
    /**
     * @var array
     *
     * @ORM\Column(name="villedes", type="text",nullable=true)
     */
    private $villedes;
    
    /**
     * @var array
     *
     * @ORM\Column(name="pays", type="text",nullable=true)
     */
    private $pays;    
    
    /**
     * @var array
     *
     * @ORM\Column(name="ageadult", type="text",nullable=true)
     */
    private $ageadult;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;
    
    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    
    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float")
     */
    private $avance;


    /**
     * @ORM\ManyToOne(targetEntity="Transport", inversedBy="reservationtransfert")
     * @ORM\JoinColumn(name="transport_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationtransfert")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationtransfert")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;


    /**
     * @var string
     *
     * @ORM\Column(name="pointd", type="string", length=255 , nullable=true)
     */
    private $pointd;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pointa", type="string", length=255 , nullable=true)
     */
    private $pointa;    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="heurd", type="string", length=255 , nullable=true)
     */
    private $heurd;    

    /**
     * @var float
     *
     * @ORM\Column(name="bag", type="float")
     */
    private $bag;  
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="pointrd", type="string", length=255 , nullable=true)
     */
    private $pointrd;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pointra", type="string", length=255 , nullable=true)
     */
    private $pointra;    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="heurr", type="string", length=255 , nullable=true)
     */
    private $heurr;  
    
    /**
     * @var array
     *
     * @ORM\Column(name="type", type="text",nullable=true)
     */
    private $type;
    
    /**
     * @var array
     *
     * @ORM\Column(name="categorie", type="text",nullable=true)
     */
    private $categorie;
    
    /**
     * @var array
     *
     * @ORM\Column(name="msg", type="text",nullable=true)
     */
    private $msg;  

    /**
     * @var float
     *
     * @ORM\Column(name="nbjour", type="float")
     */
    private $nbjour;    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
        $this->namead=array();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationtransfert
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservationtransfert
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }


    /**
     * Set total
     *
     * @param float $total
     * @return Reservationtransfert
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservationtransfert
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string 
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set transport
     *
     * @param \Btob\TransfertBundle\Entity\Transport $transport
     * @return Reservationtransfert
     */
    public function setTransport(\Btob\TransfertBundle\Entity\Transport $transport = null)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return \Btob\TransfertBundle\Entity\Transport 
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationtransfert
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationtransfert
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set ageadult
     *
     * @param string $ageadult
     * @return Reservationtransfert
     */
    public function setAgeadult($ageadult)
    {
        $this->ageadult = $ageadult;
        return $this;
    }

    /**
     * Get ageadult
     *
     * @return string
     */
    public function getAgeadult()
    {
        return $this->ageadult;
    }
    
    /**
     * Set qte
     *
     * @param float $qte
     * @return Reservationtransfert
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return float 
     */
    public function getQte()
    {
        return $this->qte;
    }
    
    /**
     * Set villedep
     *
     * @param string $villedep
     * @return Reservationtransfert
     */
    public function setVilledep($villedep)
    {
        $this->villedep = $villedep;

        return $this;
    }

    /**
     * Get villedep
     *
     * @return string 
     */
    public function getVilledep()
    {
        return $this->villedep;
    }    
    
    

    /**
     * Set villedes
     *
     * @param string $villedes
     * @return Reservationtransfert
     */
    public function setVilledes($villedes)
    {
        $this->villedes = $villedes;

        return $this;
    }

    /**
     * Get villedes
     *
     * @return string 
     */
    public function getVilledes()
    {
        return $this->villedes;
    }        
    
    /**
     * Set pays
     *
     * @param string $pays
     * @return Reservationtransfert
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }
    
     /**
     * Set pointrd
     *
     * @param string $pointrd
     * @return Reservationtransfert
     */
    public function setPointrd($pointrd)
    {
        $this->pointrd = $pointrd;

        return $this;
    }

    /**
     * Get pointrd
     *
     * @return string 
     */
    public function getPointrd()
    {
        return $this->pointrd;
    }   
    
     /**
     * Set pointra
     *
     * @param string $pointra
     * @return Reservationtransfert
     */
    public function setPointra($pointra)
    {
        $this->pointra = $pointra;

        return $this;
    }

    /**
     * Get pointra
     *
     * @return string 
     */
    public function getPointra()
    {
        return $this->pointra;
    }   
    
    /**
     * Set heurr
     *
     * @param string $heurr
     * @return Reservationtransfert
     */
    public function setHeurr($heurr)
    {
        $this->heurr = $heurr;

        return $this;
    }

    /**
     * Get heurr
     *
     * @return string 
     */
    public function getHeurr()
    {
        return $this->heurr;
    }    
    /**
     * Set bag
     *
     * @param float $bag
     * @return Reservationtransfert
     */
    public function setBag($bag)
    {
        $this->bag = $bag;

        return $this;
    }

    /**
     * Get bag
     *
     * @return float 
     */
    public function getBag()
    {
        return $this->bag;
    }
 
     /**
     * Set pointd
     *
     * @param string $pointd
     * @return Reservationtransfert
     */
    public function setPointd($pointd)
    {
        $this->pointd = $pointd;

        return $this;
    }

    /**
     * Get pointd
     *
     * @return string 
     */
    public function getPointd()
    {
        return $this->pointd;
    }   
    
     /**
     * Set pointa
     *
     * @param string $pointa
     * @return Reservationtransfert
     */
    public function setPointa($pointa)
    {
        $this->pointa = $pointa;

        return $this;
    }

    /**
     * Get pointa
     *
     * @return string 
     */
    public function getPointa()
    {
        return $this->pointa;
    }   
    
    /**
     * Set heurd
     *
     * @param string $heurd
     * @return Reservationtransfert
     */
    public function setHeurd($heurd)
    {
        $this->heurd = $heurd;

        return $this;
    }

    /**
     * Get heurd
     *
     * @return string 
     */
    public function getHeurd()
    {
        return $this->heurd;
    }        
 
    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Reservationtransfert
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }

    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }    

    
    /**
     * Set type
     *
     * @param string $type
     * @return Reservationtransfert
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }  
    
    
    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Reservationtransfert
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }    

    /**
     * Set msg
     *
     * @param string $msg
     * @return Reservationtransfert
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return string 
     */
    public function getMsg()
    {
        return $this->msg;
    }        
 
     /**
     * Set nbjour
     *
     * @param float $nbjour
     * @return Reservationtransfert
     */
    public function setNbjour($nbjour)
    {
        $this->nbjour = $nbjour;

        return $this;
    }

    /**
     * Get nbjour
     *
     * @return float 
     */
    public function getNbjour()
    {
        return $this->nbjour;
    }   

        /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }

        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }
    
    
}