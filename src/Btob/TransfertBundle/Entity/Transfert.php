<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transfert
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\TransfertRepository")
 */
class Transfert
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="Tcategories", mappedBy="transfert")
     */
    protected $categories;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="transfert")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="Omra")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=255 , nullable=true)
     */
    private $organisme;
    /**
     * @var string
     *
     * @ORM\Column(name="heurd", type="string", length=255 , nullable=true)
     */
    private $heurd;

   /**
     * @var string
     *
     * @ORM\Column(name="heura", type="string", length=255 , nullable=true)
     */
    private $heura;

    /**
     * @var string
     *
     * @ORM\Column(name="villed", type="string", length=255, nullable=true)
     */
    private $villed;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datea", type="datetime" , nullable=true)
     */
    private $datea;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime" , nullable=true)
     */
    private $dated;

    /**
     * @var string
     *
     * @ORM\Column(name="villea", type="string", length=255 , nullable=true)
     */
    private $villea;

    /**
     * @var string
     *
     * @ORM\Column(name="pointd", type="string", length=255 , nullable=true)
     */
    private $pointd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }



    /**
     * Set organisme
     *
     * @param string $organisme
     * @return Transfert
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme
     *
     * @return string 
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set heurd
     *
     * @param string $heurd
     * @return Transfert
     */
    public function setHeurd($heurd)
    {
        $this->heurd = $heurd;

        return $this;
    }

    /**
     * Get heurd
     *
     * @return string 
     */
    public function getHeurd()
    {
        return $this->heurd;
    }

    /**
     * Set heura
     *
     * @param string $heura
     * @return Transfert
     */
    public function setHeura($heura)
    {
        $this->heura = $heura;

        return $this;
    }

    /**
     * Get heura
     *
     * @return string 
     */
    public function getHeura()
    {
        return $this->heura;
    }

    /**
     * Set villed
     *
     * @param string $villed
     * @return Transfert
     */
    public function setVilled($villed)
    {
        $this->villed = $villed;

        return $this;
    }

    /**
     * Get villed
     *
     * @return string 
     */
    public function getVilled()
    {
        return $this->villed;
    }

    /**
     * Set datea
     *
     * @param \DateTime $datea
     * @return Transfert
     */
    public function setDatea($datea)
    {
        $this->datea = $datea;

        return $this;
    }

    /**
     * Get datea
     *
     * @return \DateTime 
     */
    public function getDatea()
    {
        return $this->datea;
    }

    /**
     * Set villea
     *
     * @param string $villea
     * @return Transfert
     */
    public function setVillea($villea)
    {
        $this->villea = $villea;

        return $this;
    }

    /**
     * Get villea
     *
     * @return string 
     */
    public function getVillea()
    {
        return $this->villea;
    }

    /**
     * Set pointd
     *
     * @param string $pointd
     * @return Transfert
     */
    public function setPointd($pointd)
    {
        $this->pointd = $pointd;

        return $this;
    }

    /**
     * Get pointd
     *
     * @return string 
     */
    public function getPointd()
    {
        return $this->pointd;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Transfert
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Add categories
     *
     * @param \Btob\TransfertBundle\Entity\Tcategories $categories
     * @return Transfert
     */
    public function addCategory(\Btob\TransfertBundle\Entity\Tcategories $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Btob\TransfertBundle\Entity\Tcategories $categories
     */
    public function removeCategory(\Btob\TransfertBundle\Entity\Tcategories $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Transfert
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Transfert
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Transfert
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
