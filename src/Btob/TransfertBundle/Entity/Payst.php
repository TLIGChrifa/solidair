<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payst
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\PaystRepository")
 */
class Payst {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\OneToMany(targetEntity="Villet", mappedBy="payst", cascade={"remove"})
     */
    protected $villet;

    /**
     * @ORM\OneToMany(targetEntity="Transferts", mappedBy="payst", cascade={"remove"})
     */
    protected $transferts;

    public function __toString() {

        return "" . $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->villet = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transferts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Payst
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Payst
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Add villet
     *
     * @param \Btob\TransfertBundle\Entity\Villet $villet
     * @return Payst
     */
    public function addVillet(\Btob\TransfertBundle\Entity\Villet $villet)
    {
        $this->villet[] = $villet;

        return $this;
    }

    /**
     * Remove villet
     *
     * @param \Btob\TransfertBundle\Entity\Villet $villet
     */
    public function removeVillet(\Btob\TransfertBundle\Entity\Villet $villet)
    {
        $this->villet->removeElement($villet);
    }

    /**
     * Get villet
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVillet()
    {
        return $this->villet;
    }

    /**
     * Add transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transferts $transferts
     * @return Payst
     */
    public function addTransferts(\Btob\TransfertBundle\Entity\Transferts $transferts)
    {
        $this->transferts[] = $transferts;

        return $this;
    }

    /**
     * Remove transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transferts $transferts
     */
    public function removeTransferts(\Btob\TransfertBundle\Entity\Transferts $transferts)
    {
        $this->transferts->removeElement($transferts);
    }

    /**
     * Get transferts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransferts()
    {
        return $this->transferts;
    }
}
