<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgtrans
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\ImgtransRepository")
 */
class Imgtrans
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Transport", inversedBy="imgtrans")
     * @ORM\JoinColumn(name="trans_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transport;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgtrans
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgtrans
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set transport
     *
     * @param \Btob\TransfertBundle\Entity\Transport $transport
     * @return Imgtrans
     */
    public function setTransport(\Btob\TransfertBundle\Entity\Transport $transport = null)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return \Btob\TransfertBundle\Entity\Transport 
     */
    public function getTransport()
    {
        return $this->transport;
    }
}
