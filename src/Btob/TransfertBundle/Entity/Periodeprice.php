<?php

namespace Btob\TransfertBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Periodeprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\TransfertBundle\Entity\PeriodepriceRepository")
 */
class Periodeprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="Transport", inversedBy="periodeprice")
     * @ORM\JoinColumn(name="transport_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transport;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Transfertsperiode", inversedBy="periodeprice")
     * @ORM\JoinColumn(name="transfertsperiode_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $transfertsperiode;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set price
     *
     * @param float $price
     * @return Periodeprice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }    

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Periodeprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Periodeprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set transferts
     *
     * @param \Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode
     * @return Periodeprice
     */
    public function setTransfertsperiode(\Btob\TransfertBundle\Entity\Transfertsperiode $transfertsperiode = null)
    {
        $this->transfertsperiode = $transfertsperiode;

        return $this;
    }

    /**
     * Get transfertsperiode
     *
     * @return \Btob\TransfertBundle\Entity\Transfertsperiode 
     */
    public function getTransfertsperiode()
    {
        return $this->transfertsperiode;
    }
    
    
    /**
     * Set transport
     *
     * @param \Btob\TransfertBundle\Entity\Transport $transport
     * @return Periodeprice
     */
    public function setTransport(\Btob\TransfertBundle\Entity\Transport $transport = null)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return \Btob\TransfertBundle\Entity\Transport 
     */
    public function getTransport()
    {
        return $this->transport;
    }    

}
