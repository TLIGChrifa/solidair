<?php 

namespace Btob\TransfertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransfertsperiodeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut de période",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TransfertBundle\Entity\Transfertsperiode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_transfertbundle_transfertsperiode';
    }
}
