<?php

namespace Btob\TransfertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => "Nom de Véhicule", 'required' => true))
            ->add('capacite', 'integer', array('label' => "Capacité Max", 'required' => true))
            ->add('qte', 'integer', array('label' => "Quantité", 'required' => true))        
            ->add('act', null, array('label' => "Active?", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TransfertBundle\Entity\Transport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_transfertbundle_transport';
    }
}
