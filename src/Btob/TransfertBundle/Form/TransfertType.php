<?php

namespace Btob\TransfertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransfertType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisme')
           // ->add('villed', 'text', array('label' => ' ' , 'required' => true))
            ->add('dated')
           // ->add('villea', 'text', array('label' => ' ' , 'required' => true))
            ->add('pointd')
            ->add('dcr')
            ->add('client')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TransfertBundle\Entity\Transfert'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_transfertbundle_transfert';
    }
}
