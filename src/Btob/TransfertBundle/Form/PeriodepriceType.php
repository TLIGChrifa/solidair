<?php 

namespace Btob\TransfertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PeriodepriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price',NULL, array('required' => true, 'label' => "Prix"))//,'data' => '0'
            ->add('transport','entity', array(
                    'class' => 'BtobTransfertBundle:Transport',
                    'empty_value' => 'Choisissez une véhicule',
                    'required' => true,
                    'label' => "Véhicule",
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TransfertBundle\Entity\Periodeprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_transfertbundle_periodeprice';
    }
}
