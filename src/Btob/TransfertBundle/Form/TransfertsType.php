<?php

namespace Btob\TransfertBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Btob\TransfertBundle\Entity\Villet;

class TransfertsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
            ->add('act', Null, array('label' => "Active?", 'required' => false))
            ->add('payst', 'entity', array(
                'class' => 'BtobTransfertBundle:Payst',
                'empty_value' => 'Choisissez un pays',
                'required' => true,
                'label' => "Pays",
            ));
            
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\TransfertBundle\Entity\Transferts'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_transfertbundle_transferts';
    }

}
