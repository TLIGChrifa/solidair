<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reductioncroi
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\ReductioncroiRepository")
 */
class Reductioncroi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    /**
     * @ORM\ManyToOne(targetEntity="Croissiere", inversedBy="reductioncroi")
     * @ORM\JoinColumn(name="croissiere_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiere;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Reductioncroi
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Reductioncroi
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set cuircuit
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiere $croissiere
     * @return Reductioncroi
     */
    public function setCroissiere(\Btob\CroissiereBundle\Entity\Croissiere $croissiere = null)
    {
        $this->croissiere = $croissiere;

        return $this;
    }

    /**
     * Get croissiere
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiere 
     */
    public function getCroissiere()
    {
        return $this->croissiere;
    }
}
