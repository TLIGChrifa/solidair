<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgecrs
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\ImgecrsRepository")
 */
class Imgecrs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Croissiere", inversedBy="imgecrs")
     * @ORM\JoinColumn(name="croissiere_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiere;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgecrs
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgecrs
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set croissiere
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiere $croissiere
     * @return Imgecrs
     */
    public function setCroissiere(\Btob\CroissiereBundle\Entity\Croissiere $croissiere = null)
    {
        $this->croissiere = $croissiere;

        return $this;
    }

    /**
     * Get croissiere
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiere 
     */
    public function getCroissiere()
    {
        return $this->croissiere;
    }
}
