<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tcroissiere
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\TcroissiereRepository")
 */
class Tcroissiere
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationcroi", inversedBy="tcroissiere")
     * @ORM\JoinColumn(name="reservationcroi_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationcroi;
    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

    /**
     * @var array
     *
     * @ORM\Column(name="prenomad", type="text",nullable=true)
     */
    private $prenomad;

    /**
     * @var array
     *
     * @ORM\Column(name="ageadult", type="date",nullable=true)
     */
    private $ageadult;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reservationcroi
     *
     * @param \Btob\CroissiereBundle\Entity\Reservationcroi $reservationcroi
     * @return Tcroissiere
     */
    public function setReservationcroi(\Btob\CroissiereBundle\Entity\Reservationcroi $reservationcroi = null)
    {
        $this->reservationcroi = $reservationcroi;

        return $this;
    }

    /**
     * Get reservationcroi
     *
     * @return \Btob\CroissiereBundle\Entity\Reservationcroi
     */
    public function getReservationcroi()
    {
        return $this->reservationcroi;
    }

    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservation
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservation
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }

    /**
     * Set ageadult
     *
     * @param \DateTime $ageadult
     * @return Reservationcroi
     */
    public function setAgeadult($ageadult)
    {
        $this->ageadult = $ageadult;

        return $this;
    }

    /**
     * Get ageadult
     *
     * @return \DateTime
     */
    public function getAgeadult()
    {
        return $this->ageadult;
    }
}
