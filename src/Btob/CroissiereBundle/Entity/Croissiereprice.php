<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Croissiereprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\CroissierepriceRepository")
 */
class Croissiereprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

     /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;

     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbnuit", type="integer" , nullable=true)
     */
    private $nbnuit;

   

    /**
     * @var float
     *
     * @ORM\Column(name="red3lit", type="float" , nullable=true)
     */
    private $red3lit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers3lit", type="boolean" , nullable=true)
     */
    private $pers3lit;

    /**
     * @var float
     *
     * @ORM\Column(name="red4lit", type="float" , nullable=true)
     */
    private $red4lit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers4lit", type="boolean" , nullable=true)
     */
    private $pers4lit;

    /**
     * @var float
     *
     * @ORM\Column(name="supsingle", type="float" , nullable=true)
     */
    private $supsingle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perssupsingle", type="boolean" , nullable=true)
     */

    private $perssupsingle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Croissiere", inversedBy="supplementcroi")
     * @ORM\JoinColumn(name="croissiere_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiere;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Croissiereprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Croissiereprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Croissiereprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Croissiereprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Croissiereprice
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Set nbnuit
     *
     * @param integer $nbnuit
     * @return Croissiereprice
     */
    public function setNbnuit($nbnuit)
    {
        $this->nbnuit = $nbnuit;

        return $this;
    }

    /**
     * Get nbnuit
     *
     * @return integer 
     */
    public function getNbnuit()
    {
        return $this->nbnuit;
    }

  

    /**
     * Set red3lit
     *
     * @param float $red3lit
     * @return Croissiereprice
     */
    public function setRed3lit($red3lit)
    {
        $this->red3lit = $red3lit;

        return $this;
    }

    /**
     * Get red3lit
     *
     * @return float 
     */
    public function getRed3lit()
    {
        return $this->red3lit;
    }

    /**
     * Set pers3lit
     *
     * @param boolean $pers3lit
     * @return Croissiereprice
     */
    public function setPers3lit($pers3lit)
    {
        $this->pers3lit = $pers3lit;

        return $this;
    }

    /**
     * Get pers3lit
     *
     * @return boolean 
     */
    public function getPers3lit()
    {
        return $this->pers3lit;
    }

    /**
     * Set red4lit
     *
     * @param float $red4lit
     * @return Croissiereprice
     */
    public function setRed4lit($red4lit)
    {
        $this->red4lit = $red4lit;

        return $this;
    }

    /**
     * Get red4lit
     *
     * @return float 
     */
    public function getRed4lit()
    {
        return $this->red4lit;
    }

    /**
     * Set pers4lit
     *
     * @param boolean $pers4lit
     * @return Croissiereprice
     */
    public function setPers4lit($pers4lit)
    {
        $this->pers4lit = $pers4lit;

        return $this;
    }

    /**
     * Get pers4lit
     *
     * @return boolean 
     */
    public function getPers4lit()
    {
        return $this->pers4lit;
    }

    /**
     * Set supsingle
     *
     * @param float $supsingle
     * @return Croissiereprice
     */
    public function setSupsingle($supsingle)
    {
        $this->supsingle = $supsingle;

        return $this;
    }

    /**
     * Get supsingle
     *
     * @return float 
     */
    public function getSupsingle()
    {
        return $this->supsingle;
    }

    /**
     * Set perssupsingle
     *
     * @param boolean $perssupsingle
     * @return Croissiereprice
     */
    public function setPerssupsingle($perssupsingle)
    {
        $this->perssupsingle = $perssupsingle;

        return $this;

    }

    /**
     * Get perssupsingle
     *
     * @return boolean
     */
    public function getPerssupsingle()
    {
        return $this->perssupsingle;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Croissiereprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Croissiereprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set cuircuit
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiere $croissiere
     * @return Croissiereprice
     */
    public function setCroissiere(\Btob\CroissiereBundle\Entity\Croissiere $croissiere = null)
    {
        $this->croissiere = $croissiere;

        return $this;
    }

    /**
     * Get croissiere
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiere 
     */
    public function getCroissiere()
    {
        return $this->croissiere;
    }

}
