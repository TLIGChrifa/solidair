<?php







namespace Btob\CroissiereBundle\Entity;







use Doctrine\ORM\Mapping as ORM;











/**



 * Croissiere



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\CroissiereRepository")



 */



class Croissiere



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;



    /**



     * @ORM\OneToMany(targetEntity="Imgecrs", mappedBy="croissiere")



     */

    protected $imgcr;



    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $act;



    /**



     * @var string



     *



     * @ORM\Column(name="region", type="string", length=255 , nullable=true)



     */



    private $region;







    /**



     * @var string



     *



     * @ORM\Column(name="titre", type="string", length=255 , nullable=true)



     */



    private $titre;







    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;







    /**



     * @var integer



     *



     * @ORM\Column(name="nbrjr", type="integer" , nullable=true)



     */



    private $nbrjr;




    /**



     * @var float



     *



     * @ORM\Column(name="prixavance", type="float" , nullable=true)



     */



    private $prixavance;    

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */

    private $marge;        
         /**
     * @ORM\OneToMany(targetEntity="Supplementcroi", mappedBy="croissiere", cascade={"remove"})
     */
    protected $supplementcroi;
    

     /**
     * @ORM\OneToMany(targetEntity="Reductioncroi", mappedBy="croissiere", cascade={"remove"})
     */
    protected $reductioncroi;    
    
    
        /**
     * @ORM\OneToMany(targetEntity="Croissiereprice", mappedBy="croissiere", cascade={"remove"})
     */
    protected $croissiereprice;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */

    private $ageenfmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */

    private $ageenfmax;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)
     */

    private $agebmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)
     */

    private $agebmax;


    /**



     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;



    /**



     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="croissiere")



     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")



     */



    protected $arrangement;







    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }




    /**



     * Set region



     *



     * @param string $region



     * @return Croissiere



     */



    public function setRegion($region)



    {



        $this->region = $region;







        return $this;



    }







    /**



     * Get region



     *



     * @return string 



     */



    public function getRegion()



    {



        return $this->region;



    }







    /**



     * Set titre



     *



     * @param string $titre



     * @return Croissiere



     */



    public function setTitre($titre)



    {



        $this->titre = $titre;







        return $this;



    }







    /**



     * Get titre



     *



     * @return string 



     */



    public function getTitre()



    {



        return $this->titre;



    }







    /**



     * Set prix



     *



     * @param float $prix



     * @return Croissiere



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }







    /**



     * Set nbrjr



     *



     * @param integer $nbrjr



     * @return Croissiere



     */



    public function setNbrjr($nbrjr)



    {



        $this->nbrjr = $nbrjr;







        return $this;



    }







    /**



     * Get nbrjr



     *



     * @return integer 



     */



    public function getNbrjr()



    {



        return $this->nbrjr;



    }







    /**



     * Set description



     *



     * @param string $description



     * @return Croissiere



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set arrangement



     *



     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement



     * @return Croissiere



     */



    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)



    {



        $this->arrangement = $arrangement;







        return $this;



    }







    /**



     * Get arrangement



     *



     * @return \Btob\HotelBundle\Entity\Arrangement 



     */



    public function getArrangement()



    {



        return $this->arrangement;



    }







    /**



     * Constructor



     */



    public function __construct()



    {



        $this->imgcr = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplementcroi = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reductioncroi = new \Doctrine\Common\Collections\ArrayCollection();
        $this->croissiereprice = new \Doctrine\Common\Collections\ArrayCollection();



    }







    /**



     * Add imgcr



     *



     * @param \Btob\CroissiereBundle\Entity\Imgecrs $imgcr



     * @return Croissiere



     */



    public function addImgcr(\Btob\CroissiereBundle\Entity\Imgecrs $imgcr)



    {



        $this->imgcr[] = $imgcr;







        return $this;



    }







    /**



     * Remove imgcr



     *



     * @param \Btob\CroissiereBundle\Entity\Imgecrs $imgcr



     */



    public function removeImgcr(\Btob\CroissiereBundle\Entity\Imgecrs $imgcr)



    {



        $this->imgcr->removeElement($imgcr);



    }







    /**



     * Get imgcr



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgcr()



    {



        return $this->imgcr;



    }







    /**



     * Set act



     *



     * @param boolean $act



     * @return Croissiere



     */



    public function setAct($act)



    {



        $this->act = $act;







        return $this;



    }







    /**



     * Get act



     *



     * @return boolean 



     */



    public function getAct()



    {



        return $this->act;



    }


     /**
     * Add supplementcroi
     *
     * @param \Btob\CroissiereBundle\Entity\Supplementcroi $supplementcroi
     * @return Croissiere
     */
    public function addSupplementcroi(\Btob\CroissiereBundle\Entity\Supplementcroi $supplementcroi)
    {
        $this->supplementcroi[] = $supplementcroi;

        return $this;
    }

    /**
     * Remove supplementcroi
     *
     * @param \Btob\CroissiereBundle\Entity\Supplementcroi $supplementcroi
     */
    public function removeSupplementcroi(\Btob\CroissiereBundle\Entity\Supplementcroi $supplementcroi)
    {
        $this->supplementcroi->removeElement($supplementcroi);
    }

    /**
     * Get supplementcroi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplementcroi()
    {
        return $this->supplementcroi;
    }
  
    
     /**
     * Add croissiereprice
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice
     * @return Croissiere
     */
    public function addCroissiereprice(\Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice)
    {
        $this->croissiereprice[] = $croissiereprice;

        return $this;
    }

    /**
     * Remove croissiereprice
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice
     */
    public function removeCroissiereprice(\Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice)
    {
        $this->croissiereprice->removeElement($croissiereprice);
    }

    /**
     * Get Croissiereprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCroissiereprice()
    {
        return $this->croissiereprice;
    }   
    
    
    
        /**
     * Add reductioncroi
     *
     * @param \Btob\CroissiereBundle\Entity\Reductioncroi $reductioncroi
     * @return Croissiere
     */
    public function addReductioncroi(\Btob\CroissiereBundle\Entity\Reductioncroi $reductioncroi)
    {
        $this->reductioncroi[] = $reductioncroi;

        return $this;
    }

    /**
     * Remove reductioncroi
     *
     * @param \Btob\CroissiereBundle\Entity\Reductioncroi $reductioncroi
     */
    public function removeReductioncroi(\Btob\CroissiereBundle\Entity\Reductioncroi $reductioncroi)
    {
        $this->reductioncroi->removeElement($reductioncroi);
    }

    /**
     * Get reductioncroi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReductioncroi()
    {
        return $this->reductioncroi;
    } 
    
    
    
    /**
     * Set prixavance
     *
     * @param float $prixavance
     * @return Croissiere
     */

    public function setPrixavance($prixavance)
    {
        $this->prixavance = $prixavance;
        return $this;
    }

    /**
     * Get prixavance
     *
     * @return float 
     */

    public function getPrixavance()
    {
        return $this->prixavance;

    }    
    
    
       
     /**
     * Set ageenfmin
     *
     * @param string $ageenfmin
     * @return Croissiere
     */

    public function setAgeenfmin($ageenfmin)

    {
        $this->ageenfmin = $ageenfmin;
        return $this;

    }



    /**
     * Get ageenfmin
     *
     * @return string 
     */

    public function getAgeenfmin()

    {
        return $this->ageenfmin;

    }
    
    
     /**
     * Set ageenfmax
     *
     * @param string $ageenfmax
     * @return Croissiere
     */

    public function setAgeenfmax($ageenfmax)

    {
        $this->ageenfmax = $ageenfmax;
        return $this;

    }



    /**
     * Get ageenfmax
     *
     * @return string 
     */

    public function getAgeenfmax()

    {
        return $this->ageenfmax;

    }
    
    
    
    
    
    
    /**
     * Set agebmin
     *
     * @param string $agebmin
     * @return Croissiere
     */

    public function setAgebmin($agebmin)

    {
        $this->agebmin = $agebmin;
        return $this;

    }



    /**
     * Get agebmin
     *
     * @return string 
     */

    public function getAgebmin()

    {
        return $this->agebmin;

    }
    
    
     /**
     * Set agebmax
     *
     * @param string $agebmax
     * @return Croissiere
     */

    public function setAgebmax($agebmax)

    {
        $this->agebmax = $agebmax;
        return $this;

    }



    /**
     * Get agebmax
     *
     * @return string 
     */

    public function getAgebmax()

    {
        return $this->agebmax;

    }
    
     /**
     * Set marge
     *
     * @param float $marge
     * @return Croissiere
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        

}



