<?php

namespace Btob\CroissiereBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationcroi
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CroissiereBundle\Entity\ReservationcroiRepository")
 */
class Reservationcroi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;


    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;

    
    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float")
     */
    private $avance;
    

    /**
     * @ORM\ManyToOne(targetEntity="Croissiere", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="croissiere_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiere;
    
    /**
     * @ORM\ManyToOne(targetEntity="Croissiereprice", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="croissiereprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $croissiereprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

   
    /**
     * @ORM\OneToMany(targetEntity="Reservationcroidetail", mappedBy="reservationcroi", cascade={"remove"})
     */
    protected $reservationcroidetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationcroi
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationcroi
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set croissiereprice
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice
     * @return Reservationcroi
     */
    public function setCroissiereprice(\Btob\CroissiereBundle\Entity\Croissiereprice $croissiereprice = null)
    {
        $this->croissiereprice = $croissiereprice;

        return $this;
    }

    /**
     * Get croissiereprice
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiereprice 
     */
    public function getCroissiereprice()
    {
        return $this->croissiereprice;
    }
    
       
    
    /**
     * Set croissiere
     *
     * @param \Btob\CroissiereBundle\Entity\Croissiere $croissiere
     * @return Reservationcroi
     */
    public function setCroissiere(\Btob\CroissiereBundle\Entity\Croissiere $croissiere = null)
    {
        $this->croissiere = $croissiere;

        return $this;
    }

    /**
     * Get croissiere
     *
     * @return \Btob\CroissiereBundle\Entity\Croissiere 
     */
    public function getCroissiere()
    {
        return $this->croissiere;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationcroi
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationcroi
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationcroidetail
     *
     * @param \Btob\CroissiereBundle\Entity\Reservationcroidetail $reservationcroidetail
     * @return Reservationcroi
     */
    public function addReservationcroidetail(\Btob\CroissiereBundle\Entity\Reservationcroidetail $reservationcroidetail)
    {
        $this->reservationcroidetail[] = $reservationcroidetail;

        return $this;
    }

    /**
     * Remove reservationcroidetail
     *
     * @param \Btob\CroissiereBundle\Entity\Reservationcroidetail $reservationcroidetail
     */
    public function removeReservationcroidetail(\Btob\CroissiereBundle\Entity\Reservationcroidetail $reservationcroidetail)
    {
        $this->reservationcroidetail->removeElement($reservationcroidetail);
    }

    /**
     * Get reservationcroidetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationcroidetail()
    {
        return $this->reservationcroidetail;
    }

        /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }
    


        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }




}
