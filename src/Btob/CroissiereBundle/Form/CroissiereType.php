<?php







namespace Btob\CroissiereBundle\Form;







use Symfony\Component\Form\AbstractType;



use Symfony\Component\Form\FormBuilderInterface;



use Symfony\Component\OptionsResolver\OptionsResolverInterface;







class CroissiereType extends AbstractType



{



    /**



     * @param FormBuilderInterface $builder



     * @param array $options



     */



    public function buildForm(FormBuilderInterface $builder, array $options)



    {



        $builder



            ->add('act', NULL, array('label' =>'active', 'required' => false))






           // ->add('region', 'text', array('label' => "Region", 'required' => true))



            ->add('titre', 'text', array('label' => "Titre", 'required' => true))



            ->add('prix', NULL, array('label' =>'Prix' , 'required' => true))
            ->add('marge', NULL, array('label' => 'Marge(En pourcentage) ', 'required' => true))
            ->add('prixavance', NULL, array('label' => 'Avance(En pourcentage) ', 'required' => true))


            ->add('nbrjr', NULL, array('label' =>'Nombre des jours' , 'required' => true))



            ->add('description', null, array('label' => "Description ", 'required' => false))

            ->add('ageenfmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Enfant  *',
            ))
            ->add('ageenfmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Enfant  *',
            ))
            ->add('agebmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Bébé  *',
            ))
            ->add('agebmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Bébé  *',
            ))      

            ->add('arrangement', NULL, array('label' => "Logement", 'required' => true))
            


        ;



    }



    



    /**



     * @param OptionsResolverInterface $resolver



     */



    public function setDefaultOptions(OptionsResolverInterface $resolver)



    {



        $resolver->setDefaults(array(



            'data_class' => 'Btob\CroissiereBundle\Entity\Croissiere'



        ));



    }







    /**



     * @return string



     */



    public function getName()



    {



        return 'btob_croissierebundle_croissiere';



    }



}



