<?php

namespace Btob\CroissiereBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReductioncroiType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array('label' => "Nom de Reduction", 'required' => true))
                ->add('price', null, array('label' => "Prix" , 'required' => true))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\CroissiereBundle\Entity\Reductioncroi'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'btob_croissierebundle_reductioncroi';
    }

}
