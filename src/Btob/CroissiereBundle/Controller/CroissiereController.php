<?php







namespace Btob\CroissiereBundle\Controller;







use Symfony\Component\HttpFoundation\Request;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;







use Btob\CroissiereBundle\Entity\Croissiere;



use Btob\CroissiereBundle\Entity\Reservationcroi;



use Btob\CroissiereBundle\Entity\Imgecrs;



use Btob\CroissiereBundle\Form\CroissiereType;







/**



 * Croissiere controller.



 *



 */



class CroissiereController extends Controller



{







    /**



     * Lists all Croissiere entities.



     *



     */



    public function indexAction()



    {



        $em = $this->getDoctrine()->getManager();







        $entities = $em->getRepository('BtobCroissiereBundle:Croissiere')->findAll();







        return $this->render('BtobCroissiereBundle:Croissiere:index.html.twig', array(



            'entities' => $entities,



        ));



    }





    /**



     * Creates a new Croissiere entity.



     *



     */



    public function createAction(Request $request)



    {



        $entity = new Croissiere();



        $form = $this->createCreateForm($entity);



        $form->handleRequest($request);







        if ($form->isValid()) {



            $em = $this->getDoctrine()->getManager();



            $em->persist($entity);



            $em->flush();



            if (is_array($request->request->get("files")))



                foreach ($request->request->get("files") as $key => $value) {



                    if ($value != "") {



                        $img = new Imgecrs();



                        $img->setCroissiere($entity);



                        $img->setImage($value);







                        $em->persist($img);



                        $em->flush();







                    }



                }



           // return $this->redirect($this->generateUrl('croissiere_show', array('id' => $entity->getId())));



            return $this->redirect($this->generateUrl('croissiere'));



        }







        return $this->render('BtobCroissiereBundle:Croissiere:new.html.twig', array(



            'entity' => $entity,



            'form'   => $form->createView(),



        ));



    }







    /**



     * Creates a form to create a Croissiere entity.



     *



     * @param Croissiere $entity The entity



     *



     * @return \Symfony\Component\Form\Form The form



     */



    private function createCreateForm(Croissiere $entity)



    {



        $form = $this->createForm(new CroissiereType(), $entity, array(



            'action' => $this->generateUrl('croissiere_create'),



            'method' => 'POST',



        ));







       // $form->add('submit', 'submit', array('label' => 'Create'));







        return $form;



    }







    /**



     * Displays a form to create a new Croissiere entity.



     *



     */



    public function newAction()



    {



        $entity = new Croissiere();



        $form   = $this->createCreateForm($entity);







        return $this->render('BtobCroissiereBundle:Croissiere:new.html.twig', array(



            'entity' => $entity,



            'form'   => $form->createView(),



        ));



    }







    /**



     * Finds and displays a Croissiere entity.



     *



     */



    public function showAction($id)



    {



        $em = $this->getDoctrine()->getManager();







        $entity = $em->getRepository('BtobCroissiereBundle:Croissiere')->find($id);







        if (!$entity) {



            throw $this->createNotFoundException('Unable to find Croissiere entity.');



        }







        $deleteForm = $this->createDeleteForm($id);







        return $this->render('BtobCroissiereBundle:Croissiere:show.html.twig', array(



            'entity'      => $entity,



            'delete_form' => $deleteForm->createView(),



        ));



    }







    /**



     * Displays a form to edit an existing Croissiere entity.



     *



     */



    public function editAction($id)



    {



        $em = $this->getDoctrine()->getManager();







        $entity = $em->getRepository('BtobCroissiereBundle:Croissiere')->find($id);







        if (!$entity) {



            throw $this->createNotFoundException('Unable to find Croissiere entity.');



        }







        $editForm = $this->createEditForm($entity);



        $deleteForm = $this->createDeleteForm($id);







        return $this->render('BtobCroissiereBundle:Croissiere:edit.html.twig', array(



            'entity'      => $entity,



            'edit_form'   => $editForm->createView(),



            'delete_form' => $deleteForm->createView(),



        ));



    }







    /**



    * Creates a form to edit a Croissiere entity.



    *



    * @param Croissiere $entity The entity



    *



    * @return \Symfony\Component\Form\Form The form



    */



    private function createEditForm(Croissiere $entity)



    {



        $form = $this->createForm(new CroissiereType(), $entity, array(



            'action' => $this->generateUrl('croissiere_update', array('id' => $entity->getId())),



            'method' => 'PUT',



        ));







      //  $form->add('submit', 'submit', array('label' => 'Update'));







        return $form;



    }



    /**



     * Edits an existing Croissiere entity.



     *



     */



    public function updateAction(Request $request, $id)



    {



        $em = $this->getDoctrine()->getManager();







        $entity = $em->getRepository('BtobCroissiereBundle:Croissiere')->find($id);


if($entity->getImgcr())
{
        foreach ($entity->getImgcr() as $key => $value) {



            $em->remove($value);



            $em->flush();



        }

}

        if (!$entity) {



            throw $this->createNotFoundException('Unable to find Croissiere entity.');



        }







        $deleteForm = $this->createDeleteForm($id);



        $editForm = $this->createEditForm($entity);



        $editForm->handleRequest($request);







        if ($editForm->isValid()) {



            $em->flush();



            if (is_array($request->request->get("files")))



                foreach ($request->request->get("files") as $key => $value) {



                    if ($value != "") {



                        $img = new Imgecrs();



                        $img->setCroissiere($entity);



                        $img->setImage($value);







                        $em->persist($img);



                        $em->flush();







                    }



                }



           // return $this->redirect($this->generateUrl('croissiere_edit', array('id' => $id)));



            return $this->redirect($this->generateUrl('croissiere'));



        }







        return $this->render('BtobCroissiereBundle:Croissiere:edit.html.twig', array(



            'entity'      => $entity,



            'edit_form'   => $editForm->createView(),



            'delete_form' => $deleteForm->createView(),



        ));



    }



    /**



     * Deletes a Croissiere entity.



     *



     */



    public function deleteAction( $id)



    {



           $em = $this->getDoctrine()->getManager();



            $entity = $em->getRepository('BtobCroissiereBundle:Croissiere')->find($id);











            $em->remove($entity);



            $em->flush();











        return $this->redirect($this->generateUrl('croissiere'));



    }






    /**



     * Creates a form to delete a Croissiere entity by id.



     *



     * @param mixed $id The entity id



     *



     * @return \Symfony\Component\Form\Form The form



     */



    private function createDeleteForm($id)



    {



        return $this->createFormBuilder()



            ->setAction($this->generateUrl('croissiere_delete', array('id' => $id)))



            ->setMethod('DELETE')



            ->add('submit', 'submit', array('label' => 'Delete'))



            ->getForm()



        ;


    }



}



