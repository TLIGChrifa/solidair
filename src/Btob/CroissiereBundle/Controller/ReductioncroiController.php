<?php

namespace Btob\CroissiereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CroissiereBundle\Entity\Reductioncroi;
use Symfony\Component\HttpFoundation\Request;
use Btob\CroissiereBundle\Form\ReductioncroiType;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReductioncroiController extends Controller {

    public function indexAction($croissiereid) {
        $croissiere = $this->getDoctrine()
                ->getRepository('BtobCroissiereBundle:Croissiere')
                ->find($croissiereid);
        return $this->render('BtobCroissiereBundle:Reductioncroi:index.html.twig', array('entities' => $croissiere->getReductioncroi(), "croissiereid" => $croissiereid));
    }

    public function addAction($croissiereid) {
        $croissiere = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->find($croissiereid);
        $Reductioncroi = new Reductioncroi();
        $form = $this->createForm(new ReductioncroiType(), $Reductioncroi);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Reductioncroi->setCroissiere($croissiere);
                $em->persist($Reductioncroi);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_reductioncroi_homepage', array("croissiereid" => $croissiereid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCroissiereBundle:Reductioncroi:form.html.twig', array('form' => $form->createView(), "croissiereid" => $croissiereid));
    }

    public function editAction($id, $croissiereid) {
        $request = $this->get('request');
        $Reductioncroi = $this->getDoctrine()
                ->getRepository('BtobCroissiereBundle:Reductioncroi')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReductioncroiType(), $Reductioncroi);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_reductioncroi_homepage', array("croissiereid" => $croissiereid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCroissiereBundle:Reductioncroi:form.html.twig', array('form' => $form->createView(), 'id' => $id, "croissiereid" => $croissiereid)
        );
    }

    public function deleteAction(Reductioncroi $Reductioncroi, $croissiereid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Reductioncroi) {
            throw new NotFoundHttpException("Reduction non trouvée");
        }
        $em->remove($Reductioncroi);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reductioncroi_homepage', array("croissiereid" => $croissiereid)));
    }

}
