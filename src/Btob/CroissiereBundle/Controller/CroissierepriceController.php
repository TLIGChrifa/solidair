<?php

namespace Btob\CroissiereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CroissiereBundle\Entity\Croissiereprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\CroissiereBundle\Form\CroissierepriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class CroissierepriceController extends Controller
{

    public function indexAction($croissiereid)
    {
        $croissiere = $this->getDoctrine()
            ->getRepository('BtobCroissiereBundle:Croissiere')
            ->find($croissiereid);
	    $name =  $croissiere->getTitre();
        $price = array();
        foreach ($croissiere->getCroissiereprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobCroissiereBundle:Croissiereprice:index.html.twig', array(
            "entities" => $price,
            "croissiereid" => $croissiereid,
	    "name" => $name,
        ));
    }


    public function addAction($croissiereid)
    {
        $croissiere = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->find($croissiereid);
      
        $allcroissiereprice = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiereprice')->findBy(array('croissiere' => $croissiere));
      
        

        $croissiereprice = new Croissiereprice();
        $form = $this->createForm(new CroissierepriceType(), $croissiereprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $croissiereprice->setCroissiere($croissiere);
                $em->persist($croissiereprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_croissiereprice_homepage', array("croissiereid" => $croissiereid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCroissiereBundle:Croissiereprice:form.html.twig', array(
            'form' => $form->createView(),
            "croissiere" => $croissiere

        ));
    }



    public function editAction($id, $croissiereid)
    {
        $croissiere = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->find($croissiereid);
        $request = $this->get('request');
        $croissiereprice = $this->getDoctrine()
            ->getRepository('BtobCroissiereBundle:Croissiereprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CroissierepriceType(), $croissiereprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allcroissiereprice = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiereprice')->findBy(array('croissiere' => $croissiere));
      
    
       
        if ($form->isValid()) {

            $croissiereprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_croissiereprice_homepage', array("croissiereid" => $croissiereid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCroissiereBundle:Croissiereprice:edit.html.twig', array('form' => $form->createView(), 'price' => $croissiereprice, 'id' => $id, "croissiere" => $croissiere)
        );
    }

  
    public function deleteAction(Croissiereprice $croissiereprice, $croissiereid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$croissiereprice) {
            throw new NotFoundHttpException("Croissiereprice non trouvée");
        }
        $em->remove($croissiereprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_croissiereprice_homepage', array("croissiereid" => $croissiereid)));
    }



}












