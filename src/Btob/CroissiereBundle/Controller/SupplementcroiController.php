<?php

namespace Btob\CroissiereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CroissiereBundle\Entity\Supplementcroi;
use Symfony\Component\HttpFoundation\Request;
use Btob\CroissiereBundle\Form\SupplementcroiType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementcroiController extends Controller {

    public function indexAction($croissiereid) {
        $croissiere = $this->getDoctrine()
                ->getRepository('BtobCroissiereBundle:Croissiere')
                ->find($croissiereid);
        return $this->render('BtobCroissiereBundle:Supplementcroi:index.html.twig', array('entities' => $croissiere->getSupplementcroi(), "croissiereid" => $croissiereid));
    }

    public function addAction($croissiereid) {
        $croissiere = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->find($croissiereid);
        $Supplementcroi = new Supplementcroi();
        $form = $this->createForm(new SupplementcroiType(), $Supplementcroi);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementcroi->setCroissiere($croissiere);
                $em->persist($Supplementcroi);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementcroi_homepage', array("croissiereid" => $croissiereid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCroissiereBundle:Supplementcroi:form.html.twig', array('form' => $form->createView(), "croissiereid" => $croissiereid));
    }

    public function editAction($id, $croissiereid) {
        $request = $this->get('request');
        $Supplementcroi = $this->getDoctrine()
                ->getRepository('BtobCroissiereBundle:Supplementcroi')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementcroiType(), $Supplementcroi);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementcroi_homepage', array("croissiereid" => $croissiereid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCroissiereBundle:Supplementcroi:form.html.twig', array('form' => $form->createView(), 'id' => $id, "croissiereid" => $croissiereid)
        );
    }

    public function deleteAction(Supplementcroi $Supplementcroi, $croissiereid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementcroi) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplementcroi);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementcroi_homepage', array("croissiereid" => $croissiereid)));
    }

}
