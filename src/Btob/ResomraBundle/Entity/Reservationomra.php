<?php

namespace Btob\ResomraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationomra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ResomraBundle\Entity\ReservationomraRepository")
 */
class Reservationomra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="adulte", type="integer",nullable=true)
     */
    private $adulte;
    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=20,nullable=true)
     */
    private $ref;

    /**
     * @var integer
     *
     * @ORM\Column(name="enfant", type="integer",nullable=true)
     */
    private $enfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="bebe", type="integer",nullable=true)
     */
    private $bebe;
    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer",nullable=true)
     */
    private $etat;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datevole", type="date")
     */
    private $datevole;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var integer
     *
     * @ORM\Column(name="flexibilite", type="integer",nullable=true)
     */
    private $flexibilite;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text",nullable=true)
     */
    private $body;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\OmraBundle\Entity\Omra", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;


    public  function  __construct(){
        $this->dcr = new \DateTime();
        $this->etat=0;

    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adulte
     *
     * @param integer $adulte
     * @return Omra
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return integer
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Omra
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set enfant
     *
     * @param integer $enfant
     * @return Omra
     */
    public function setEnfant($enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get enfant
     *
     * @return integer
     */
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set bebe
     *
     * @param integer $bebe
     * @return Omra
     */
    public function setBebe($bebe)
    {
        $this->bebe = $bebe;

        return $this;
    }

    /**
     * Get bebe
     *
     * @return integer
     */
    public function getBebe()
    {
        return $this->bebe;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Omra
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Omra
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set datevole
     *
     * @param \DateTime $datevole
     * @return Omra
     */
    public function setDatevole($datevole)
    {
        $this->datevole = $datevole;

        return $this;
    }

    /**
     * Get datevole
     *
     * @return \DateTime
     */
    public function getDatevole()
    {
        return $this->datevole;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Omra
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set flexibilite
     *
     * @param integer $flexibilite
     * @return Omra
     */
    public function setFlexibilite($flexibilite)
    {
        $this->flexibilite = $flexibilite;

        return $this;
    }

    /**
     * Get flexibilite
     *
     * @return integer
     */
    public function getFlexibilite()
    {
        return $this->flexibilite;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Omra
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Omra
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Omra
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }



    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\Omra $omra
     * @return Reservationomra
     */
    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)
    {
        $this->omra = $omra;

        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\Omra
     */
    public function getOmra()
    {
        return $this->omra;
    }
}
