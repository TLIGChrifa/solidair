<?php

namespace Btob\ResomraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;

use Btob\OmraBundle\Entity\Omra;
use Btob\ResomraBundle\Entity\Reservationomra;

use Btob\HotelBundle\Entity\Clients;
use Btob\OmraBundle\Form\OmraType;
use Btob\ResomraBundle\Form\ReservationomraType;
use Btob\HotelBundle\Form\ClientsType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();
        return $this->render('BtobResomraBundle:Default:index.html.twig', array('entities' => $entities,
            'i' => 4));    }

    public function detailAction(Omra $omra)
    {
        return $this->render('BtobResomraBundle:Default:detail.html.twig', array('entry' => $omra));
    }


    public function reservationomraAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
            //    $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '-'));
            //    $datef = new \Datetime(Tools::explodedate($request->request->get('datef'), '-'));
           //     $message = $request->request->get('message');

                $reservation = new Reservationomra();
                $reservation->setClient($client);
                $reservation->setOmra($entities);
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
////////////  les champs de saisir
                $reservation->setAdulte($request->request->get("adulte"));
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setEnfant($request->request->get("enfant"));
                $reservation->setBebe($request->request->get("bebe"));
                $reservation->setFlexibilite($request->request->get("felxible"));
                $reservation->setBody($request->request->get("body"));
                $date = Tools::explodedate($request->request->get("datevole"),'/');
                $reservation->setDatevole(new \DateTime($date));
////////////  les champs de saisir

                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a bien été envoyé. Merci.');

                return $this->render('BtobResomraBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobResomraBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }

    public function listreservationomraAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobResomraBundle:Reservationomra")->findAll();
        return $this->render('BtobResomraBundle:Default:listreservationomra.html.twig', array('entities' => $entities));
    }




}
