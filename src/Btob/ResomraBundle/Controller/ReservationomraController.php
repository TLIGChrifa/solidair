<?php

namespace Btob\ResomraBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\OmraBundle\Entity\Omra;
use Btob\OmraBundle\Form\OmraType;
use Btob\ResomraBundle\Entity\Reservationomra;
use Btob\ResomraBundle\Form\ReservationomraType;

/**
 * Reservationomra controller.
 *
 */
class ReservationomraController extends Controller
{

    /**
     * Lists all Reservationomra entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobResomraBundle:Reservationomra')->findAll();

        return $this->render('BtobResomraBundle:Reservationomra:index.html.twig', array(
            'entities' => $entities,
        ));
    }


    public function listreservationomraAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("BtobResomraBundle:Reservationomra")->findAll();
        return $this->render('BtobResomraBundle:Reservationomra:listreservationomra.html.twig', array('entities' => $entities));
    }


    /**
     * Creates a new Reservationomra entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reservationomra();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reservationomra_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobResomraBundle:Reservationomra:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Reservationomra entity.
     *
     * @param Reservationomra $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Reservationomra $entity)
    {
        $form = $this->createForm(new ReservationomraType(), $entity, array(
            'action' => $this->generateUrl('reservationomra_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Reservationomra entity.
     *
     */
    public function newAction()
    {
        $entity = new Reservationomra();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobResomraBundle:Reservationomra:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reservationomra entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobResomraBundle:Reservationomra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationomra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobResomraBundle:Reservationomra:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reservationomra entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobResomraBundle:Reservationomra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationomra entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobResomraBundle:Reservationomra:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Reservationomra entity.
    *
    * @param Reservationomra $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reservationomra $entity)
    {
        $form = $this->createForm(new ReservationomraType(), $entity, array(
            'action' => $this->generateUrl('reservationomra_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

     //   $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Reservationomra entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobResomraBundle:Reservationomra')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationomra entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('reservationomra'));
        }

        return $this->render('BtobResomraBundle:Reservationomra:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reservationomra entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobResomraBundle:Reservationomra')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Reservationomra entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('reservationomra'));
    }

    /**
     * Creates a form to delete a Reservationomra entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservationomra_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }


    public function delete1Action(Reservationomra $reservationomra) {
        $em = $this->getDoctrine()->getManager();

        if (!$reservationomra) {
            throw new NotFoundHttpException("omra non trouvée");
        }
        $em->remove($reservationomra);
        $em->flush();
        return $this->redirect($this->generateUrl('reservationomra'));
    }




}
