<?php

namespace Btob\VoleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class VolType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('depart')
            ->add('arrive')
            ->add('dated', 'date')
            ->add('dater', 'date')
            ->add('jeunes')
            ->add('etudiant')
            ->add('adulte')
            ->add('enfant')
      //      ->add('dcr')
            ->add('dcr', 'datetime',array('data' => new \DateTime()))
            ->add('bebe')
            ->add('client')
            ->add('agent')
			->add('naissancebebe')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\VoleBundle\Entity\Vol'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_volebundle_vol';
    }
}
