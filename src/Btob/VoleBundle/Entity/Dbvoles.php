<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dbvoles
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\DbvolesRepository")
 */
class Dbvoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationbateau", inversedBy="dbvoles")
     * @ORM\JoinColumn(name="reservationbateau_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $volbd;
    /**
     * @var array
     *
     * @ORM\Column(name="depart", type="text",nullable=true)
     */
    private $depart;

    /**
     * @var array
     *
     * @ORM\Column(name="arrive", type="text",nullable=true)
     */
    private $arrive;

   /**
     * @var array
     *
     * @ORM\Column(name="dated", type="text", nullable=true)
     */
    private $dated;

    /**
     * @var array
     *
     * @ORM\Column(name="dater", type="text", nullable=true)
     */
    private $dater;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vold
     *
     * @param \Btob\VoleBundle\Entity\Vol $vold
     * @return Dbvoles
     */
    public function setVolbd(\Btob\VoleBundle\Entity\Reservationbateau $volbd = null)
    {
        $this->volbd = $volbd;

        return $this;
    }

    /**
     * Get volbd
     *
     * @return \Btob\VoleBundle\Entity\Reservationbateau
     */
    public function getVolbd()
    {
        return $this->volbd;
    }

    /**
     * Set depart
     *
     * @param string $depart
     * @return Reservation
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;

        return $this;
    }

    /**
     * Get depart
     *
     * @return string
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * Set arrive
     *
     * @param string $arrive
     * @return Reservation
     */
    public function setArrive($arrive)
    {
        $this->arrive = $arrive;

        return $this;
    }

    /**
     * Get arrive
     *
     * @return string
     */
    public function getArrive()
    {
        return $this->arrive;
    }

    

     /**
     * Set dated
     *
     * @param string $dated
     * @return Dvoles
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return string
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dater
     *
     * @param string $dater
     * @return Dbvoles
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }

    /**
     * Get dater
     *
     * @return string
     */
    public function getDater()
    {
        return $this->dater;
    }
}
