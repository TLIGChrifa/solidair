<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vol
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\VolRepository")
 */
class Vol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="vol")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="vol")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="type1", type="string", length=255, nullable=true)
     */
    private $type1;

   
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="compagnie", type="string", length=255, nullable=true)
     */
    private $compagnie;
    
    /**
     * @var string
     *
     * @ORM\Column(name="classes", type="string", length=255, nullable=true)
     */
    private $classes;

   



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**

     * @ORM\OneToMany(targetEntity="Tvoles", mappedBy="vol")

     */

    protected $vol;
    
    
     /**

     * @ORM\OneToMany(targetEntity="Dvoles", mappedBy="vold")

     */

    protected $vold;
    
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Vol
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type1
     *
     * @return string 
     */
    public function getType1()
    {
        return $this->type1;
    }
    /**
     * Set type1
     *
     * @param string $type1
     * @return Vol
     */
    public function setType1($type1)
    {
        $this->type1 = $type1;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    

    
  /**
     * Set compagnie
     *
     * @param string $compagnie
     * @return Vol
     */
    public function setCompagnie($compagnie)
    {
        $this->compagnie = $compagnie;

        return $this;
    }

    /**
     * Get compagnie
     *
     * @return string 
     */
    public function getCompagnie()
    {
        return $this->compagnie;
    }   
    
    
    /**
     * Set classes
     *
     * @param string $classes
     * @return Vol
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;

        return $this;
    }

    /**
     * Get classes
     *
     * @return string 
     */
    public function getClasses()
    {
        return $this->classes;
    }   
   



  


    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Vol
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Vol
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

     /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Vol
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
	
	    /**

     * Add categories

     *

     * @param \Btob\VoleBundle\Entity\Tvoles $vol

     * @return Vol

     */

    public function addVol(\Btob\VoleBundle\Entity\Tvoles $vol)

    {

        $this->vol[] = $vol;



        return $this;

    }



    /**

     * Remove vol

     *

     * @param \Btob\VoleBundle\Entity\Tvoles $vol

     */

    public function removeVol(\Btob\VoleBundle\Entity\Tvoles $vol)

    {

        $this->vol->removeElement($vol);

    }



    /**

     * Get vol

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getVol()

    {

        return $this->vol;

    }
    
    
    
    
    /**

     * Add categories

     *

     * @param \Btob\VoleBundle\Entity\Dvoles $vold

     * @return Vol

     */

    public function addVold(\Btob\VoleBundle\Entity\Dvoles $vold)

    {

        $this->vold[] = $vold;



        return $this;

    }



    /**

     * Remove vold

     *

     * @param \Btob\VoleBundle\Entity\Dvoles $vold

     */

    public function removeVold(\Btob\VoleBundle\Entity\Dvoles $vold)

    {

        $this->vold->removeElement($vold);

    }



    /**

     * Get vold

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getVold()

    {

        return $this->vold;

    }
  
}
