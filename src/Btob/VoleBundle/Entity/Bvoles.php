<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tvoles
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\BvolesRepository")
 */
class Bvoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationbateau", inversedBy="bvoles")
     * @ORM\JoinColumn(name="reservationbateau_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationbateau;
    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

    /**
     * @var array
     *
     * @ORM\Column(name="prenomad", type="text",nullable=true)
     */
    private $prenomad;

    /**
     * @var array
     *
     * @ORM\Column(name="ageadult", type="text",nullable=true)
     */
    private $ageadult;
	
    /**
     * @var array
     *
     * @ORM\Column(name="passad", type="text",nullable=true)
     */
    private $passad;	
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vol
     *
     * @param \Btob\VoleBundle\Entity\Reservationbateau $reservationbateau
     * @return Bvoles
     */
    public function setReservationbateau(\Btob\VoleBundle\Entity\Reservationbateau $reservationbateau = null)
    {
        $this->reservationbateau = $reservationbateau;

        return $this;
    }

    /**
     * Get reservationbateau
     *
     * @return \Btob\VoleBundle\Entity\Reservationbateau
     */
    public function getReservationbateau()
    {
        return $this->reservationbateau;
    }

    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservation
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservation
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }

    /**
     * Set ageadult
     *
     * @param string $ageadult
     * @return Vol
     */
    public function setAgeadult($ageadult)
    {
        $this->ageadult = $ageadult;

        return $this;
    }

    /**
     * Get ageadult
     *
     * @return string
     */
    public function getAgeadult()
    {
        return $this->ageadult;
    }
	
    /**
     * Set passad
     *
     * @param string $passad
     * @return Bvoles
     */
    public function setPassad($passad)
    {
        $this->passad = $passad;

        return $this;
    }

    /**
     * Get passad
     *
     * @return string
     */
    public function getPassad()
    {
        return $this->passad;
    }
		
}
