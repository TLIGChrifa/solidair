<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationbateau
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\ReservationbateauRepository")
 */
class Reservationbateau
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationbateau")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationcroi")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
   
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="contacte", type="string", length=255 , nullable=true)
     */
    private $contacte;

  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var boolean
     *
     * @ORM\Column(name="couchetteint", type="boolean" , nullable=true)
     */
    private $couchetteint;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="fauteuil", type="boolean" , nullable=true)
     */
    private $fauteuil;
	
	/**
     * @var boolean
     *
     * @ORM\Column(name="pont", type="boolean" , nullable=true)
     */
    private $pont;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="couchetteext", type="boolean" , nullable=true)
     */
    private $couchetteext;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="cabineanimal", type="boolean" , nullable=true)
     */
    private $cabineanimal;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="singleint", type="boolean" , nullable=true)
     */
    private $singleint;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="singleext", type="boolean" , nullable=true)
     */
    private $singleext;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="suitesingle", type="boolean" , nullable=true)
     */
    private $suitesingle;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="doubleint", type="boolean" , nullable=true)
     */
    private $doubleint;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="doubleext", type="boolean" , nullable=true)
     */
    private $doubleext;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="suitematrimoniale", type="boolean" , nullable=true)
     */
    private $suitematrimoniale;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="tripleint", type="boolean" , nullable=true)
     */
    private $tripleint;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="tripleext", type="boolean" , nullable=true)
     */
    private $tripleext;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="suitefamiliale", type="boolean" , nullable=true)
     */
    private $suitefamiliale;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="quadrupleint", type="boolean" , nullable=true)
     */
    private $quadrupleint;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="quadrupleext", type="boolean" , nullable=true)
     */
    private $quadrupleext;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="repas ", type="boolean" , nullable=true)
     */
    private $repas ;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchien", type="integer" , nullable=true)
     */
    private $nbchien;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchat", type="integer" , nullable=true)
     */
    private $nbchat;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="marque", type="string", length=255 , nullable=true)
     */
    private $marque;
    
     /**
     * @var string
     *
     * @ORM\Column(name="immatricule", type="string", length=255 , nullable=true)
     */
    private $immatricule;
    
    /**
     * @var string
     *
     * @ORM\Column(name="options", type="string", length=255 , nullable=true)
     */
    private $options;
    
    /**
     * @var string
     *
     *
     * @ORM\Column(name="compagnie", type="string", length=255 , nullable=true)
     */
    private $compagnie;
    
    
    /**

     * @ORM\OneToMany(targetEntity="Bvoles", mappedBy="reservationbateau")

     */

    protected $reservationbateau;
    
	
     /**

     * @ORM\OneToMany(targetEntity="Dbvoles", mappedBy="volbd")

     */

    protected $volbd;	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }


    /**
     * Set type
     *
     * @param string $type
     * @return Reservationbateau
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
  
    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationbateau
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set contacte
     *
     * @param string $contacte
     * @return Reservationcroi
     */
    public function setContacte($contacte)
    {
        $this->contacte = $contacte;

        return $this;
    }

    /**
     * Get contacte
     *
     * @return string 
     */
    public function getContacte()
    {
        return $this->contacte;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationbateau
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationbateau
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    
    
    
    /**
     * Set couchetteint
     *
     * @param boolean $couchetteint
     * @return Reservationbateau
     */
    public function setCouchetteint($couchetteint)
    {
        $this->couchetteint = $couchetteint;

        return $this;
    }

    /**
     * Get couchetteint
     *
     * @return boolean 
     */
    public function getCouchetteint()
    {
        return $this->couchetteint;
    }
    
    
    /**
     * Set couchetteext
     *
     * @param boolean $couchetteext
     * @return Reservationbateau
     */
    public function setCouchetteext($couchetteext)
    {
        $this->couchetteext = $couchetteext;

        return $this;
    }

    /**
     * Get couchetteext
     *
     * @return boolean 
     */
    public function getCouchetteext()
    {
        return $this->couchetteext;
    }
    
    
    /**
     * Set cabineanimal
     *
     * @param boolean $cabineanimal
     * @return Reservationbateau
     */
    public function setCabineanimal($cabineanimal)
    {
        $this->cabineanimal = $cabineanimal;

        return $this;
    }

    /**
     * Get cabineanimal
     *
     * @return boolean 
     */
    public function getCabineanimal()
    {
        return $this->cabineanimal;
    }
    
    
    /**
     * Set singleint
     *
     * @param boolean $singleint
     * @return Reservationbateau
     */
    public function setSingleint($singleint)
    {
        $this->singleint = $singleint;

        return $this;
    }

    /**
     * Get singleint
     *
     * @return boolean 
     */
    public function getSingleint()
    {
        return $this->singleint;
    }
    
    /**
     * Set singleext
     *
     * @param boolean $singleext
     * @return Reservationbateau
     */
    public function setSingleext($singleext)
    {
        $this->singleext = $singleext;

        return $this;
    }

    /**
     * Get singleext
     *
     * @return boolean 
     */
    public function getSingleext()
    {
        return $this->singleext;
    }
    
    /**
     * Set suitesingle
     *
     * @param boolean $suitesingle
     * @return Reservationbateau
     */
    public function setSuitesingle($suitesingle)
    {
        $this->suitesingle = $suitesingle;

        return $this;
    }

    /**
     * Get suitesingle
     *
     * @return boolean 
     */
    public function getSuitesingle()
    {
        return $this->suitesingle;
    }
    
    /**
     * Set doubleint
     *
     * @param boolean $doubleint
     * @return Reservationbateau
     */
    public function setDoubleint($doubleint)
    {
        $this->doubleint = $doubleint;

        return $this;
    }

    /**
     * Get doubleint
     *
     * @return boolean 
     */
    public function getDoubleint()
    {
        return $this->doubleint;
    }
    
    /**
     * Set doubleext
     *
     * @param boolean $doubleext
     * @return Reservationbateau
     */
    public function setDoubleext($doubleext)
    {
        $this->doubleext = $doubleext;

        return $this;
    }

    /**
     * Get doubleext
     *
     * @return boolean 
     */
    public function getDoubleext()
    {
        return $this->doubleext;
    }
    
    /**
     * Set suitematrimoniale
     *
     * @param boolean $suitematrimoniale
     * @return Reservationbateau
     */
    public function setSuitematrimoniale($suitematrimoniale)
    {
        $this->suitematrimoniale = $suitematrimoniale;

        return $this;
    }

    /**
     * Get suitematrimoniale
     *
     * @return boolean 
     */
    public function getSuitematrimoniale()
    {
        return $this->suitematrimoniale;
    }
    
    /**
     * Set tripleint
     *
     * @param boolean $tripleint
     * @return Reservationbateau
     */
    public function setTripleint($tripleint)
    {
        $this->tripleint = $tripleint;

        return $this;
    }

    /**
     * Get tripleint
     *
     * @return boolean 
     */
    public function getTripleint()
    {
        return $this->tripleint;
    }
    
    /**
     * Set tripleext
     *
     * @param boolean $tripleext
     * @return Reservationbateau
     */
    public function setTripleext($tripleext)
    {
        $this->tripleext = $tripleext;

        return $this;
    }

    /**
     * Get tripleext
     *
     * @return boolean 
     */
    public function getTripleext()
    {
        return $this->tripleext;
    }
    
    /**
     * Set suitefamiliale
     *
     * @param boolean $suitefamiliale
     * @return Reservationbateau
     */
    public function setSuitefamiliale($suitefamiliale)
    {
        $this->suitefamiliale = $suitefamiliale;

        return $this;
    }

    /**
     * Get suitefamiliale
     *
     * @return boolean 
     */
    public function getSuitefamiliale()
    {
        return $this->suitefamiliale;
    }
    
    /**
     * Set quadrupleint
     *
     * @param boolean $quadrupleint
     * @return Reservationbateau
     */
    public function setQuadrupleint($quadrupleint)
    {
        $this->quadrupleint = $quadrupleint;

        return $this;
    }

    /**
     * Get quadrupleint
     *
     * @return boolean 
     */
    public function getQuadrupleint()
    {
        return $this->quadrupleint;
    }
    
    /**
     * Set quadrupleext
     *
     * @param boolean $quadrupleext
     * @return Reservationbateau
     */
    public function setQuadrupleext($quadrupleext)
    {
        $this->quadrupleext = $quadrupleext;

        return $this;
    }

    /**
     * Get quadrupleext
     *
     * @return boolean 
     */
    public function getQuadrupleext()
    {
        return $this->quadrupleext;
    }
    
    /**
     * Set repas
     *
     * @param boolean $repas
     * @return Reservationbateau
     */
    public function setRepas($repas)
    {
        $this->repas = $repas;

        return $this;
    }

    /**
     * Get repas
     *
     * @return boolean 
     */
    public function getRepas()
    {
        return $this->repas;
    }
    
    
    
   /**
     * Set nbchien
     *
     * @param integer $nbchien
     * @return Reservationbateau
     */
    public function setNbchien($nbchien)
    {
        $this->nbchien = $nbchien;

        return $this;
    }

    /**
     * Get nbchien
     *
     * @return integer 
     */
    public function getNbchien()
    {
        return $this->nbchien;
    }
    
    /**
     * Set nbchat
     *
     * @param integer $nbchat
     * @return Reservationbateau
     */
    public function setNbchat($nbchat)
    {
        $this->nbchat = $nbchat;

        return $this;
    }

    /**
     * Get nbchat
     *
     * @return integer 
     */
    public function getNbchat()
    {
        return $this->nbchat;
    }
    
    
    /**
     * Set marque
     *
     * @param string $marque
     * @return Reservationbateau
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return string 
     */
    public function getMarque()
    {
        return $this->marque;
    }
    
    
    /**
     * Set immatricule
     *
     * @param string $immatricule
     * @return Reservationbateau
     */
    public function setImmatricule($immatricule)
    {
        $this->immatricule = $immatricule;

        return $this;
    }

    /**
     * Get immatricule
     *
     * @return string 
     */
    public function getImmatricule()
    {
        return $this->immatricule;
    }
    
    
    /**
     * Set options
     *
     * @param string $options
     * @return Reservationbateau
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }
    
    
    /**
     * Set compagnie 
     *
     * @param string $compagnie
     * @return Reservationbateau
     */
    public function setCompagnie($compagnie)
    {
        $this->compagnie = $compagnie;

        return $this;
    }

    /**
     * Get compagnie
     *
     * @return string 
     */
    public function getCompagnie()
    {
        return $this->compagnie;
    }
    
    
    
     /**
     * Add reservationcroi
     *
     * @param \Btob\VoleBundle\Entity\Tvoles $reservationbateau
     * @return Reservationbateau
     */

    public function addReservationbateau(\Btob\VoleBundle\Entity\Tvoles $reservationbateau)

    {

        $this->reservationbateau[] = $reservationbateau;



        return $this;

    }



    /**

     * Remove reservationbateau

     *

     * @param \Btob\VoleBundle\Entity\Tvoles $reservationbateau

     */

    public function removeReservationbateau(\Btob\VoleBundle\Entity\Tvoles $reservationbateau)

    {

        $this->reservationbateau->removeElement($reservationbateau);

    }



    /**

     * Get reservationbateau

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getReservationbateau()

    {

        return $this->reservationbateau;

    }
  
     /**

     * Add categories

     *

     * @param \Btob\VoleBundle\Entity\Dbvoles $volbd

     * @return Reservationbateau

     */

    public function addVolbd(\Btob\VoleBundle\Entity\Dbvoles $vold)

    {

        $this->volbd[] = $volbd;



        return $this;

    }



    /**

     * Remove volbd

     *

     * @param \Btob\VoleBundle\Entity\Dbvoles $volbd

     */

    public function removeVolbd(\Btob\VoleBundle\Entity\Dbvoles $volbd)

    {

        $this->volbd->removeElement($volbd);

    }



    /**

     * Get volbd

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getVolbd()

    {

        return $this->volbd;

    }   
	
	
	 /**
     * Get fauteuil
     *
     * @return boolean 
     */
    public function getFauteuil()
    {
        return $this->fauteuil;
    }
    
    
    /**
     * Set fauteuil
     *
     * @param boolean $fauteuil
     * @return Reservationbateau
     */
    public function setFauteuil($fauteuil)
    {
        $this->fauteuil = $fauteuil;

        return $this;
    }

	 /**
     * Get pont
     *
     * @return boolean 
     */
    public function getPont()
    {
        return $this->pont;
    }
    
    
    /**
     * Set pont
     *
     * @param boolean $pont
     * @return Reservationbateau
     */
    public function setPont($pont)
    {
        $this->pont = $pont;

        return $this;
    }
}
