<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dvoles
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\DvolesRepository")
 */
class Dvoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Vol", inversedBy="dvoles")
     * @ORM\JoinColumn(name="vol_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $vold;
    /**
     * @var array
     *
     * @ORM\Column(name="depart", type="text",nullable=true)
     */
    private $depart;

    /**
     * @var array
     *
     * @ORM\Column(name="arrive", type="text",nullable=true)
     */
    private $arrive;

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date", nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="date", nullable=true)
     */
    private $dater;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vold
     *
     * @param \Btob\VoleBundle\Entity\Vol $vold
     * @return Dvoles
     */
    public function setVold(\Btob\VoleBundle\Entity\Vol $vold = null)
    {
        $this->vold = $vold;

        return $this;
    }

    /**
     * Get vold
     *
     * @return \Btob\VoleBundle\Entity\Vol
     */
    public function getVold()
    {
        return $this->vold;
    }

    /**
     * Set depart
     *
     * @param string $depart
     * @return Reservation
     */
    public function setDepart($depart)
    {
        $this->depart = $depart;

        return $this;
    }

    /**
     * Get depart
     *
     * @return string
     */
    public function getDepart()
    {
        return $this->depart;
    }

    /**
     * Set arrive
     *
     * @param string $arrive
     * @return Reservation
     */
    public function setArrive($arrive)
    {
        $this->arrive = $arrive;

        return $this;
    }

    /**
     * Get arrive
     *
     * @return string
     */
    public function getArrive()
    {
        return $this->arrive;
    }

    

     /**
     * Set dated
     *
     * @param \Date $dated
     * @return Dvoles
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \Date
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dater
     *
     * @param \Date $dater
     * @return Dvoles
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }

    /**
     * Get dater
     *
     * @return \Date
     */
    public function getDater()
    {
        return $this->dater;
    }
}
