<?php

namespace Btob\VoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tvoles
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoleBundle\Entity\TvolesRepository")
 */
class Tvoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Vol", inversedBy="tvoles")
     * @ORM\JoinColumn(name="vol_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $vol;
    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

    /**
     * @var array
     *
     * @ORM\Column(name="prenomad", type="text",nullable=true)
     */
    private $prenomad;
	
    /**
     * @var array
     *
     * @ORM\Column(name="passad", type="text",nullable=true)
     */
    private $passad;	
	

    /**
     * @var array
     *
     * @ORM\Column(name="ageadult", type="date",nullable=true)
     */
    private $ageadult;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vol
     *
     * @param \Btob\VoleBundle\Entity\Vole $vol
     * @return Tvoles
     */
    public function setVol(\Btob\VoleBundle\Entity\Vol $vol = null)
    {
        $this->vol = $vol;

        return $this;
    }

    /**
     * Get vol
     *
     * @return \Btob\VoleBundle\Entity\Vol
     */
    public function getVol()
    {
        return $this->vol;
    }

    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservation
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservation
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }

    /**
     * Set ageadult
     *
     * @param datetime $ageadult
     * @return Vol
     */
    public function setAgeadult($ageadult)
    {
        $this->ageadult = $ageadult;

        return $this;
    }

    /**
     * Get ageadult
     *
     * @return datetime
     */
    public function getAgeadult()
    {
        return $this->ageadult;
    }
	
    /**
     * Set passad
     *
     * @param string $passad
     * @return Tvoles
     */
    public function setPassad($passad)
    {
        $this->passad = $passad;

        return $this;
    }

    /**
     * Get passad
     *
     * @return string
     */
    public function getPassad()
    {
        return $this->passad;
    }
	
}
