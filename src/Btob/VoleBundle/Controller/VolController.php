<?php 

namespace Btob\VoleBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\VoleBundle\Entity\Vol;
use Btob\VoleBundle\Entity\Reservationbateau;

use Btob\VoleBundle\Form\VolType;


/**
 * Vol controller.
 *
 */

class VolController extends Controller
{
    
    /**
     * Lists all Vol entities.
     *
     */
    public function indexAction()

    {

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $em->getRepository('BtobVoleBundle:Vol')->findAll();


        }else{
            $entities = $em->getRepository('BtobVoleBundle:Vol')->findBy(array('agent' => $user));

        }


        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }

        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }







        return $this->render('BtobVoleBundle:Vol:index.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));





    }





    public function indexbateauAction()

    {

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $em->getRepository('BtobVoleBundle:Reservationbateau')->findAll();


        }else{
            $entities = $em->getRepository('BtobVoleBundle:Reservationbateau')->findBy(array('agent' => $user));

        }


        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }

        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }







        return $this->render('BtobVoleBundle:Vol:indexbateau.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));






    }

    /**

     * Creates a new Vol entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new Vol();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);



        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();



            return $this->redirect($this->generateUrl('vol_show', array('id' => $entity->getId())));

        }



        return $this->render('BtobVoleBundle:Vol:new.html.twig', array(

            'entity' => $entity,

            'form'   => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a Vol entity.

     *

     * @param Vol $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(Vol $entity)

    {

        $form = $this->createForm(new VolType(), $entity, array(

            'action' => $this->generateUrl('vol_create'),

            'method' => 'POST',

        ));



        $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new Vol entity.

     *

     */

    public function newAction()

    {

        $entity = new Vol();

        $form   = $this->createCreateForm($entity);



        return $this->render('BtobVoleBundle:Vol:new.html.twig', array(

            'entity' => $entity,

            'form'   => $form->createView(),

        ));

    }



    /**

     * Finds and displays a Vol entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();


           
        $entity = $em->getRepository('BtobVoleBundle:Vol')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Vol entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobVoleBundle:Vol:show.html.twig', array(

            'entity'      => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }
    public function voucherAction($id)

    {

        $em = $this->getDoctrine()->getManager();


           
        $entity = $em->getRepository('BtobVoleBundle:Vol')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Vol entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



		$pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('BtobVoleBundle:Vol:voucher.html.twig', array(

            'entity'      => $entity,

            'delete_form' => $deleteForm->createView(),

        ));
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
                $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
                $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->Image($img_logo, 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;

    }



    /**

     * Displays a form to edit an existing Vol entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobVoleBundle:Vol')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Vol entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobVoleBundle:Vol:edit.html.twig', array(

            'entity'      => $entity,

            'edit_form'   => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

    * Creates a form to edit a Vol entity.

    *

    * @param Vol $entity The entity

    *

    * @return \Symfony\Component\Form\Form The form

    */

    private function createEditForm(Vol $entity)

    {

        $form = $this->createForm(new VolType(), $entity, array(

            'action' => $this->generateUrl('vol_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));



        $form->add('submit', 'submit', array('label' => 'Update'));



        return $form;

    }

    /**

     * Edits an existing Vol entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobVoleBundle:Vol')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Vol entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);



        if ($editForm->isValid()) {

            $em->flush();



            return $this->redirect($this->generateUrl('vol_edit', array('id' => $id)));

        }



        return $this->render('BtobVoleBundle:Vol:edit.html.twig', array(

            'entity'      => $entity,

            'edit_form'   => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }

    /**

     * Deletes a Vol entity.

     *

     */

    public function deleteAction($id)

    {

           $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('BtobVoleBundle:Vol')->find($id);





            $em->remove($entity);

            $em->flush();





        return $this->redirect($this->generateUrl('vol'));

    }



    /**

     * Creates a form to delete a Vol entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('vol_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm()

        ;

    }
    
    
     public function deletebAction($id)

    {

           $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('BtobVoleBundle:Reservationbateau')->find($id);





            $em->remove($entity);

            $em->flush();





        return $this->redirect($this->generateUrl('volbateau'));

    }
    
    
     public function showbAction($id)

    {

        $em = $this->getDoctrine()->getManager();


           
        $entity = $em->getRepository('BtobVoleBundle:Reservationbateau')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Reservationbateau entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobVoleBundle:Vol:showb.html.twig', array(

            'entity'      => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



}

