<?php

namespace Btob\VoleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use Btob\VoleBundle\Entity\Vol;
use Btob\VoleBundle\Entity\Tvoles;
use Btob\VoleBundle\Entity\Dvoles;
use Btob\VoleBundle\Entity\Dbvoles;
use Btob\VoleBundle\Entity\Bvoles;
use Btob\VoleBundle\Entity\Reservationbateau;
use Btob\HotelBundle\Common\Tools;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DefaultController extends Controller
{

    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $email = $post["email"];

             $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
			 $age = $request->request->get('age');
			$depart = $request->request->get('depart');
                         
                $arrive = $request->request->get('arrive');
			 
			 $dated = $request->request->get('dated');
                if($request->request->get('dater')!='')
                {
                $dater = $request->request->get('dater');
                }
			
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $type = $request->request->get('type');
                //    echo 'test'.$type ; exit;
               
            
                $compagnie = $request->request->get('compagnie');
                $classes = $request->request->get('classes');
               

                $vol = new Vol();
                $vol->setClient($client);
                $vol->setAgent($this->get('security.context')->getToken()->getUser());
                $vol->setType($type);
               
                $vol->setCompagnie($compagnie);
                $vol->setClasses($classes);

               
                $vol->setType1("avion");
                $em->persist($vol);
                $em->flush();


				 foreach( $named as $key=>$value){
                    $catg = new Tvoles();
                    $catg->setVol($vol);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
                    $catg->setPassad($passad[$key]);
                    $catg->setAgeadult(new \Datetime($age[$key]));
            //        $catg->setAgeadult(date_format(date_create($age[$key]), 'Y-m-d'));
                 //   $catg->setAgeadult($age[$key]);
                    
                    $em->persist($catg);
                    $em->flush();

                }

 foreach( $depart as $keys=>$values){
                    $catgg = new Dvoles();
                    $catgg->setVold($vol);
                    $catgg->setDepart($depart[$keys]);
                    $catgg->setArrive($arrive[$keys]);
                      $catgg->setDated($dated[$keys]);
                      
                      if($dater[$keys]!="")
                      {
                    $catgg->setDater($dater[$keys]);
                      }
                    $em->persist($catgg);
                    $em->flush();

                }

                $request->getSession()->getFlashBag()->add('noticvol', 'Votre message a bien été envoyé. Merci.');



                return $this->redirect($this->generateUrl('btob_vole_homepage'));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('BtobVoleBundle:Default:index.html.twig', array('form' => $form->createView(),));

    }







    public function indexbateauAction()

    {
                $info =$this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
       $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
			 $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
             $age = $request->request->get('age');
            $email = $post["email"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
            if ($testclient != null) {
                $client = $testclient;
            }
			$depart = $request->request->get('depart');
                         
                $arrive = $request->request->get('arrive');
			 
			 $dated = $request->request->get('dated');
                if($request->request->get('dater')!='')
                {
                $dater = $request->request->get('dater');
                }
			
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $type = $request->request->get('type');
                
                 $contacte = $request->request->get('contacte');
                
                $couchetteint = $request->request->get('couchetteint');
                if($couchetteint=="on")
                {
                    $couchetteint =true;
                }
                else{
                    
                    $couchetteint =false;
                }
                $couchetteext = $request->request->get('couchetteext');
                 if($couchetteext=="on")
                {
                    $couchetteext =true;
                }
                else{
                    
                    $couchetteext =false;
                }
                $cabineanimal = $request->request->get('cabineanimal');
                 if($cabineanimal=="on")
                {
                    $cabineanimal =true;
                }
                else{
                    
                    $cabineanimal =false;
                }
                $singleint = $request->request->get('singleint');
                 if($singleint=="on")
                {
                    $singleint =true;
                }
                else{
                    
                    $singleint =false;
                }
                $singleext = $request->request->get('singleext');
                 if($singleext=="on")
                {
                    $singleext =true;
                }
                else{
                    
                    $singleext =false;
                }
                $suitesingle = $request->request->get('suitesingle');
                 if($suitesingle=="on")
                {
                    $suitesingle =true;
                }
                else{
                    
                    $suitesingle =false;
                }
                $doubleint = $request->request->get('doubleint');
                 if($doubleint=="on")
                {
                    $doubleint =true;
                }
                else{
                    
                    $doubleint =false;
                }
                $doubleext = $request->request->get('doubleext');
                 if($doubleext=="on")
                {
                    $doubleext =true;
                }
                else{
                    
                    $doubleext =false;
                }
                $suitematrimoniale = $request->request->get('suitematrimoniale');
                 if($suitematrimoniale=="on")
                {
                    $suitematrimoniale =true;
                }
                else{
                    
                    $suitematrimoniale =false;
                }
                $tripleint = $request->request->get('tripleint');
                 if($tripleint=="on")
                {
                    $tripleint =true;
                }
                else{
                    
                    $tripleint =false;
                }
                $tripleext = $request->request->get('tripleext');
                 if($tripleext=="on")
                {
                    $tripleext =true;
                }
                else{
                    
                    $tripleext =false;
                }
                $suitefamiliale = $request->request->get('suitefamiliale');
                 if($suitefamiliale=="on")
                {
                    $suitefamiliale =true;
                }
                else{
                    
                    $suitefamiliale =false;
                }
                $quadrupleint = $request->request->get('quadrupleint');
                 if($quadrupleint=="on")
                {
                    $quadrupleint =true;
                }
                else{
                    
                    $quadrupleint =false;
                }
                $quadrupleext = $request->request->get('quadrupleext');
                 if($quadrupleext=="on")
                {
                    $quadrupleext =true;
                }
                else{
                    
                    $quadrupleext =false;
                }
                $repas = $request->request->get('repas');
                
                 if($repas=="on")
                {
                    $repas =true;
                }
                else{
                    
                    $repas =false;
                }
				
				
				$fauteuil = $request->request->get('fauteuil');
                if($fauteuil=="on")
                {
                    $fauteuil =true;
                }
                else{
                    
                    $fauteuil =false;
                }
				
				$pont = $request->request->get('pont');
                if($pont=="on")
                {
                    $pont =true;
                }
                else{
                    
                    $pont =false;
                }
				
                $nbchien = $request->request->get('nbchien');
                
                
                $nbchat = $request->request->get('nbchat');
                
                
                $marque = $request->request->get('marque');
                
                
                $immatricule = $request->request->get('immatricule');
                
                $options = $request->request->get('options');
                
                $compagnie = $request->request->get('compagnie');
                
               $namead = $request->request->get('namead');
			 $prenomad = $request->request->get('prenomad');
			 $passad = $request->request->get('passad');
			 $agead = $request->request->get('agead');

                
                
                $Reservationcroi = new Reservationbateau();
               
             
                $Reservationcroi->setClient($client);
                
                $Reservationcroi->setAgent($this->get('security.context')->getToken()->getUser());
                $Reservationcroi->setType($type);
                $Reservationcroi->setContacte($contacte);
                
                $Reservationcroi->setCouchetteint($couchetteint);
                
                $Reservationcroi->setCouchetteext($couchetteext);
                
                
                $Reservationcroi->setCabineanimal($cabineanimal);
                
                
                $Reservationcroi->setSingleint($singleint);
                
                
                $Reservationcroi->setSingleext($singleext);
                
                
                $Reservationcroi->setSuitesingle($suitesingle);
                
                $Reservationcroi->setDoubleint($doubleint);
                
                $Reservationcroi->setDoubleext($doubleext);
                
                $Reservationcroi->setSuitematrimoniale($suitematrimoniale);
                
                $Reservationcroi->setTripleint($tripleint);
                
                $Reservationcroi->setTripleext($tripleext);
                
                $Reservationcroi->setSuitefamiliale($suitefamiliale);
                
                $Reservationcroi->setQuadrupleint($quadrupleint);
                
                $Reservationcroi->setQuadrupleext($quadrupleext);
                
                $Reservationcroi->setRepas($repas);
                
                $Reservationcroi->setNbchien($nbchien);
                
                
                $Reservationcroi->setNbchat($nbchat);
                
                $Reservationcroi->setMarque($marque);
                
                $Reservationcroi->setImmatricule($immatricule);
                
                $Reservationcroi->setOptions($options);
                
                $Reservationcroi->setCompagnie($compagnie);
        
                $Reservationcroi->setPont($pont);
                
                
                $Reservationcroi->setFauteuil($fauteuil);
                $em->persist($Reservationcroi);
                $em->flush();
                
                 foreach( $named as $key=>$value){
                    $catg = new Bvoles();
                    $catg->setReservationbateau($Reservationcroi);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
					$catg->setPassad($passad[$key]);
                    $catg->setAgeadult($agead[$key]);
                 
                    $em->persist($catg);
                    $em->flush();

                }
                
                 		 foreach( $depart as $keys=>$values){
                    $catgg = new Dbvoles();
                    $catgg->setVolbd($Reservationcroi);
                    $catgg->setDepart($depart[$keys]);
                    $catgg->setArrive($arrive[$keys]);
                      $catgg->setDated(new \Datetime(Tools::explodedate($dated[$keys],'/')));
                      
                      if($type=="Aller-Retour")
                      {
                    $catgg->setDater(new \Datetime(Tools::explodedate($dater[$keys],'/')));
                      }
                    $em->persist($catgg);
                    $em->flush();

                }
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);

                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $Reservationcroi->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": réservation Billetterie Bâteau " ;
                $headers = "From:" .$info->getNom()."<" .$info->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message1 .='Bonjour,<br /><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br />
				 Cordialement.';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation Billetterie Bâteau " ;
                $header = "From:" .$info->getNom()."<" .$info->getEmail(). ">\n";
                $header .= "Reply-To:" .$Reservationcroi->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$info_logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message2 .='<b>Bonjour,</b><br>
				 vous avez reçu une réservation vol  , merci de consulter votre backoffice .';

                $message2 .= '<br><br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';



                mail($too, $subjects, $message2, $header);

                $request->getSession()->getFlashBag()->add('noticvol', 'Votre message a été bien envoyé. Merci.');

                return $this->redirect($this->generateUrl('btob_volebateau_homepage'));


            }
        }

        return $this->render('BtobVoleBundle:Default:indexbateau.html.twig', array('form' => $form->createView(),));
    
    }

        

}
      
