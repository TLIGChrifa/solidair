<?php

namespace Btob\VoitureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voiture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoitureBundle\Entity\VoitureRepository")
 */
class Voiture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\OneToMany(targetEntity="Imgv", mappedBy="voiture")
     */
    protected $imgv;
    /**
     * @ORM\OneToMany(targetEntity="Prixvoiture", mappedBy="voiture")
     */
    protected $prixvoiture;
    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="equipement", type="string", length=255, nullable=true)
     */
    private $equipement;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255 , nullable=true, nullable=true)
     */
    private $class;

    /**
     * @var string
     *
     * @ORM\Column(name="optionvoiture", type="string", length=255, nullable=true)
     */
    private $optionvoiture;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleproms", type="string", length=255, nullable=true)
     */
    private $libelleproms;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Voiture
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Voiture
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set equipement
     *
     * @param string $equipement
     * @return Voiture
     */
    public function setEquipement($equipement)
    {
        $this->equipement = $equipement;

        return $this;
    }

    /**
     * Get equipement
     *
     * @return string 
     */
    public function getEquipement()
    {
        return $this->equipement;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Voiture
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Voiture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set libelleproms
     *
     * @param string $libelleproms
     * @return Voiture
     */
    public function setLibelleproms($libelleproms)
    {
        $this->libelleproms = $libelleproms;

        return $this;
    }

    /**
     * Get libelleproms
     *
     * @return string 
     */
    public function getLibelleproms()
    {
        return $this->libelleproms;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return Voiture
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set optionvoiture
     *
     * @param string $optionvoiture
     * @return Voiture
     */
    public function setOptionvoiture($optionvoiture)
    {
        $this->optionvoiture = $optionvoiture;

        return $this;
    }

    /**
     * Get optionvoiture
     *
     * @return string 
     */
    public function getOptionvoiture()
    {
        return $this->optionvoiture;
    }

    public function __toString() {
        return (string) $this->getClass();

    }




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->imgv = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add imgv
     *
     * @param \Btob\VoitureBundle\Entity\Imgv $imgv
     * @return voiture
     */
    public function addImgv(\Btob\VoitureBundle\Entity\Imgv $imgv)
    {
        $this->imgv[] = $imgv;

        return $this;
    }

    /**
     * Remove imgv
     *
     * @param \Btob\VoitureBundle\Entity\Imgv $imgv
     */
    public function removeImgv(\Btob\VoitureBundle\Entity\Imgv $imgv)
    {
        $this->imgv->removeElement($imgv);
    }

    /**
     * Get imgv
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImgv()
    {
        return $this->imgv;
    }

}
