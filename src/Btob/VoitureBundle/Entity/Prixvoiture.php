<?php

namespace Btob\VoitureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prixvoiture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\VoitureBundle\Entity\PrixvoitureRepository")
 */
class Prixvoiture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date")
     */
    private $dateFin;
    /**
     * @ORM\ManyToOne(targetEntity="Voiture", inversedBy="prixvoiture")
     * @ORM\JoinColumn(name="voiture_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $voiture;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Prixvoiture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    
        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Prixvoiture
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Prixvoiture
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    
        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
    /**
     * Set voiture
     *
     * @param \Btob\VoitureBundle\Entity\voiture $voiture
     * @return Imgv
     */
    public function setVoiture(\Btob\VoitureBundle\Entity\voiture $voiture = null)
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * Get voiture
     *
     * @return \Btob\VoitureBundle\Entity\voiture
     */
    public function getVoiture()
    {
        return $this->voiture;
    }
}
