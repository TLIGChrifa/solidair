<?php

namespace Btob\VoitureBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\VoitureBundle\Entity\Prixvoiture;
use Btob\VoitureBundle\Form\PrixvoitureType;
use Btob\VoitureBundle\Entity\Voiture;
use Btob\VoitureBundle\Form\VoitureType;

/**
 * Prixvoiture controller.
 *
 */
class PrixvoitureController extends Controller
{

    /**
     * Lists all Prixvoiture entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobVoitureBundle:Prixvoiture')->findAll();

        return $this->render('BtobVoitureBundle:Prixvoiture:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Prixvoiture entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Prixvoiture();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prixvoiture_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobVoitureBundle:Prixvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Prixvoiture entity.
     *
     * @param Prixvoiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Prixvoiture $entity)
    {
        $form = $this->createForm(new PrixvoitureType(), $entity, array(
            'action' => $this->generateUrl('prixvoiture_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Prixvoiture entity.
     *
     */
    public function newAction()
    {
        $entity = new Prixvoiture();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobVoitureBundle:Prixvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Prixvoiture entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Prixvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prixvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobVoitureBundle:Prixvoiture:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Prixvoiture entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Prixvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prixvoiture entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobVoitureBundle:Prixvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Prixvoiture entity.
    *
    * @param Prixvoiture $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Prixvoiture $entity)
    {
        $form = $this->createForm(new PrixvoitureType(), $entity, array(
            'action' => $this->generateUrl('prixvoiture_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Prixvoiture entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Prixvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prixvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prixvoiture_edit', array('id' => $id)));
        }

        return $this->render('BtobVoitureBundle:Prixvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Prixvoiture entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobVoitureBundle:Prixvoiture')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Prixvoiture entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prixvoiture'));
    }

    /**
     * Creates a form to delete a Prixvoiture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prixvoiture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }




    public function adAction($j) {
        $prixvoiture2 = new Prixvoiture();
        $form = $this->createForm(new PrixvoitureType(), $prixvoiture2);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($j);

        $prixvoiture2->setVoiture($entity);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            $em = $this->getDoctrine()->getManager();
            $em->persist($prixvoiture2);
            $em->flush();

            return $this->redirect($this->generateUrl('voiture'));

        }
        return $this->render('BtobVoitureBundle:Prixvoiture:form.html.twig', array('form' => $form->createView() , 'entity'=>$prixvoiture2));
    }



    public function adprixAction($j) {

        return $this->render('BtobVoitureBundle:Prixvoiture:form.html.twig', array('j'=>$j));
    }

}
