<?php 

namespace Btob\VoitureBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Btob\VoitureBundle\Entity\Voiture;

use Btob\VoitureBundle\Entity\Imgv;

use Btob\VoitureBundle\Entity\Prixvoiture;

use Btob\VoitureBundle\Form\VoitureType;

use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Common\Tools;

use Btob\ResvoitureBundle\Entity\Reservationvoiture;

/**
 * Voiture controller.
 *
 */
class VoitureController extends Controller
{
    /**
     * Lists all Voiture entities.
     *
     */
    public function indexxAction()

    {
        $voitures = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->findAll();
        
         $request = $this->get('request');
         $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $voitures,
            $request->query->get('page', 1)/*page number*/,
            8/*limit per page*/
        );
         $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobVoitureBundle:Default:index.html.twig', array('entities' => $entities,'user' => $user));

    }


    public function detailAction(Voiture $voiture)
    {
        return $this->render('BtobVoitureBundle:Default:detail.html.twig', array('entry' => $voiture));
    }


    public function reservationvoitureAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobVoitureBundle:Voiture")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '/'));
                $datef = new \Datetime(Tools::explodedate($request->request->get('dater'), '/'));
                $message = $request->request->get('message');

                $nump = $request->request->get('nump');
                $numc = $request->request->get('numcin');
                $nationalite = $request->request->get('nationalite');
                $datep = new \Datetime(Tools::explodedate($request->request->get('datep'), '/'));
                $datec = new \Datetime(Tools::explodedate($request->request->get('datec'), '/'));
                
                $reservation = new Reservationvoiture();
                $reservation->setClient($client);
                $reservation->setVoiture($entities);
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());

                $reservation->setMessage($message);
                $reservation->setDated($dated);
                $reservation->setDatef($datef);

                 $reservation->setNumc($numc);
                 $reservation->setNationalite($nationalite);
                $reservation->setNump($nump);
                 $reservation->setDatep($datep);
                $reservation->setDatec($datec);
                $em->persist($reservation);
                $em->flush();


                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notivoiture', 'Votre demande a bien été envoyé. Merci.');

                // return $this->render('FrontBtobBundle:Voiture:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
                return $this->redirect($this->generateUrl('btob_voiture_homepage'));

            }
        }

        return $this->render('BtobVoitureBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }


    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();


        $entities = $em->getRepository('BtobVoitureBundle:Voiture')->findAll();


        return $this->render('BtobVoitureBundle:Voiture:index.html.twig', array(

            'entities' => $entities,

        ));

    }

    /**
     * Creates a new Voiture entity.
     *

     */

    public function createAction(Request $request)

    {

        $entity = new Voiture();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();
            
            
             if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgv();
                        $img->setVoiture($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }


            return $this->redirect($this->generateUrl('voiture_show', array('id' => $entity->getId())));

        }


        return $this->render('BtobVoitureBundle:Voiture:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }


    /**
     * Creates a form to create a Voiture entity.
     *
     * @param Voiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createCreateForm(Voiture $entity)

    {

        $form = $this->createForm(new VoitureType(), $entity, array(

            'action' => $this->generateUrl('voiture_create'),

            'method' => 'POST',

        ));


        $form->add('submit', 'submit', array('label' => 'Create'));


        return $form;

    }


    /**
     * Displays a form to create a new Voiture entity.
     *

     */

    public function newAction()

    {

        $entity = new Voiture();

        $form = $this->createCreateForm($entity);


        return $this->render('BtobVoitureBundle:Voiture:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }


    /**
     * Finds and displays a Voiture entity.
     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Voiture entity.');

        }


        $deleteForm = $this->createDeleteForm($id);


        return $this->render('BtobVoitureBundle:Voiture:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }


    /**
     * Displays a form to edit an existing Voiture entity.
     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Voiture entity.');

        }


        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);


        return $this->render('BtobVoitureBundle:Voiture:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }


    /**
     * Creates a form to edit a Voiture entity.
     *
     * @param Voiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createEditForm(Voiture $entity)

    {

        $form = $this->createForm(new VoitureType(), $entity, array(

            'action' => $this->generateUrl('voiture_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));


        $form->add('submit', 'submit', array('label' => 'Update'));


        return $form;

    }

    /**
     * Edits an existing Voiture entity.
     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Voiture entity.');

        }


        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);


        if ($editForm->isValid()) {

            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgv();
                        $img->setVoiture($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            
            
            
            return $this->redirect($this->generateUrl('voiture_edit', array('id' => $id)));

        }


        return $this->render('BtobVoitureBundle:Voiture:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }

    /**
     * Deletes a Voiture entity.
     *

     */

    public function deleteAction(Request $request, $id)

    {

        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);


            if (!$entity) {

                throw $this->createNotFoundException('Unable to find Voiture entity.');

            }


            $em->remove($entity);

            $em->flush();

        }


        return $this->redirect($this->generateUrl('voiture'));

    }


    /**
     * Creates a form to delete a Voiture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('voiture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }


    public function delete1Action(Voiture $voiture)
    {

        $em = $this->getDoctrine()->getManager();


        if (!$voiture) {

            throw new NotFoundHttpException("voiture non trouvée");

        }

        $em->remove($voiture);

        $em->flush();

        return $this->redirect($this->generateUrl('voiture'));

    }


    public function adAction()
    {

        $voiture2 = new Voiture();

        $form = $this->createForm(new VoitureType(), $voiture2);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {


            $form->bind($request);

            $em = $this->getDoctrine()->getManager();

            $em->persist($voiture2);

            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgv();

                        $img->setVoiture($voiture2);

                        $img->setImage($value);

                        $em->persist($img);

                        $em->flush();


                        //Envoie des mails automatique


                    }

                }

            return $this->redirect($this->generateUrl('voiture'));


        }

        return $this->render('BtobVoitureBundle:Voiture:form.html.twig', array('form' => $form->createView(), 'entity' => $voiture2));

    }


    public function edit1Action($id)
    {

        $em = $this->getDoctrine()->getManager();


        $request = $this->get('request');

        $voiture = $em->getRepository('BtobVoitureBundle:Voiture')->find($id);


        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new VoitureType(), $voiture);

        $form->handleRequest($request);


        if ($form->isValid()) {

            foreach ($voiture->getImgv() as $key => $value) {

                $em->remove($value);

                $em->flush();

            }

            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgv();

                        $img->setVoiture($voiture);

                        $img->setImage($value);


                        $em->persist($img);

                        $em->flush();


                    }

                }

            return $this->redirect($this->generateUrl('voiture'));

        } else {

            echo $form->getErrors();

        }

        return $this->render('BtobVoitureBundle:Voiture:form.html.twig', array('form' => $form->createView(), 'id' => $id, 'entity' => $voiture)

        );

    }


}

