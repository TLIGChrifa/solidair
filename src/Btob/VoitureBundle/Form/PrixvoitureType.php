<?php

namespace Btob\VoitureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PrixvoitureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix')
            ->add('dateDebut','date')
            ->add('dateFin','date')
       //     ->add('voiture')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\VoitureBundle\Entity\Prixvoiture'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_voiturebundle_prixvoiture';
    }
}
