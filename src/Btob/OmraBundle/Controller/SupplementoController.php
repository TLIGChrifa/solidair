<?php

namespace Btob\OmraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\OmraBundle\Entity\Supplemento;
use Symfony\Component\HttpFoundation\Request;
use Btob\OmraBundle\Form\SupplementoType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class SupplementoController extends Controller {

    public function indexAction($omraid) {
        $omra = $this->getDoctrine()
                ->getRepository('BtobOmraBundle:Omra')
                ->find($omraid);
        return $this->render('BtobOmraBundle:Supplemento:index.html.twig', array('entities' => $omra->getSupplemento(), "omraid" => $omraid));
    }

    public function addAction($omraid) {
        $omra = $this->getDoctrine()->getRepository('BtobOmraBundle:Omra')->find($omraid);
        $supplemento = new Supplemento();
        $form = $this->createForm(new SupplementoType(), $supplemento);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $supplemento->setOmra($omra);
                $em->persist($supplemento);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Ajout: Nouveau supplément - ". $omra->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplemento_homepage', array("omraid" => $omraid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobOmraBundle:Supplemento:form.html.twig', array('form' => $form->createView(), "omraid" => $omraid));
    }

    public function editAction($id, $omraid) {
        $request = $this->get('request');
        $supplemento = $this->getDoctrine()
                ->getRepository('BtobOmraBundle:Supplemento')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementoType(), $supplemento);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Modification: Supplément du Omra (". $supplemento->getOmra()->getTitre(). ") n° " . $supplemento->getId()." - ". $supplemento->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplemento_homepage', array("omraid" => $omraid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobOmraBundle:Supplemento:form.html.twig', array('form' => $form->createView(), 'id' => $id, "omraid" => $omraid)
        );
    }

    public function deleteAction(Supplemento $supplemento, $omraid) {
        $em = $this->getDoctrine()->getManager();

        if (!$supplemento) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Suppression: Supplément du Omra (". $supplemento->getOmra()->getTitre(). ") n° " . $supplemento->getId()." - ". $supplemento->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($supplemento);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplemento_homepage', array("omraid" => $omraid)));
    }

}
