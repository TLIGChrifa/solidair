<?php



namespace Btob\OmraBundle\Controller;



use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

use Btob\HotelBundle\Common\Tools;

use Btob\OmraBundle\Entity\Omra;

use Btob\OmraBundle\Entity\Imgeomra;

use Btob\OmraBundle\Form\OmraType;



/**

 * Omra controller.

 *

 */

class OmraController extends Controller

{



    /**

     * Lists all Omra entities.

     *

     */



    public function indexAction()

    {

      //  $entities = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findby(array('act' => 1));

        $entities = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();



        return $this->render('BtobOmraBundle:Omra:index.html.twig', array('entities' => $entities));

    }



    /**

     * Creates a new Omra entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new Omra();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        $request = $this->get('request');

        if ($form->isValid()) {

           


            $em = $this->getDoctrine()->getManager();

           

            $em->persist($entity);
            
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Ajout: Nouvelle Omra");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgeomra();

                        $img->setOmra($entity);

                        $img->setImage($value);



                        $em->persist($img);

                        $em->flush();



                    }

                }

            return $this->redirect($this->generateUrl('omra'));

        }



        return $this->render('BtobOmraBundle:Omra:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a Omra entity.

     *

     * @param Omra $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(Omra $entity)

    {

        $form = $this->createForm(new OmraType(), $entity, array(

            'action' => $this->generateUrl('omra_create'),

            'method' => 'POST',

        ));



       // $form->add('submit', 'submit', array('label' => 'Create'));



        return $form;

    }



    /**

     * Displays a form to create a new Sejour entity.

     *

     */

    public function newAction()

    {

        $entity = new Omra();

        $form = $this->createCreateForm($entity);



        return $this->render('BtobOmraBundle:Omra:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Finds and displays a Omra entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Omra entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobOmraBundle:Omra:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Displays a form to edit an existing Sejour entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Omra entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobOmraBundle:Omra:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Creates a form to edit a Omra entity.

     *

     * @param Omra $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createEditForm(Omra $entity)

    {

        $form = $this->createForm(new OmraType(), $entity, array(

            'action' => $this->generateUrl('omra_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));



      //  $form->add('submit', 'submit', array('label' => 'Update'));



        return $form;

    }



    /**

     * Edits an existing Omra entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);

        foreach ($entity->getImgeomra() as $key => $value) {

            $em->remove($value);

            $em->flush();

        }

        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Omra entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);



        if ($editForm->isValid()) {

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Modification: Omra " . $entity->getId()." - " . $entity->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgeomra();

                        $img->setOmra($entity);

                        $img->setImage($value);



                        $em->persist($img);

                        $em->flush();



                    }

                }

            return $this->redirect($this->generateUrl('omra'));

        }



        return $this->render('BtobOmraBundle:Omra:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Deletes a Omra entity.

     *

     */

    public function deleteAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOmraBundle:Omra')->find($id);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Suppression: Omra " . $entity->getId()." - " . $entity->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('omra'));

    }



    /**

     * Creates a form to delete a Omra entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('omra_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm();

    }

}

