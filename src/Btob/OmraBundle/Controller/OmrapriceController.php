<?php

namespace Btob\OmraBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\OmraBundle\Entity\Omraprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\OmraBundle\Form\OmrapriceType;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class OmrapriceController extends Controller
{

    public function indexAction($omraid)
    {
        $omra = $this->getDoctrine()
            ->getRepository('BtobOmraBundle:Omra')
            ->find($omraid);
	    $name =  $omra->getLibelle();
        $price = array();
        foreach ($omra->getOmraprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobOmraBundle:Omraprice:index.html.twig', array(
            "entities" => $price,
            "omraid" => $omraid,
	    "name" => $name,
        ));
    }


    public function addAction($omraid)
    {
        $omra = $this->getDoctrine()->getRepository('BtobOmraBundle:Omra')->find($omraid);
      
        $allOmraprice = $this->getDoctrine()->getRepository('BtobOmraBundle:Omraprice')->findBy(array('omra' => $omra));
      
        

        $omraprice = new Omraprice();
        $form = $this->createForm(new OmrapriceType(), $omraprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $omraprice->setOmra($omra);
                $em->persist($omraprice);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Ajout: Nouvelle période - ". $omra->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_omraprice_homepage', array("omraid" => $omraid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobOmraBundle:Omraprice:form.html.twig', array(
            'form' => $form->createView(),
            "omra" => $omra

        ));
    }



    public function editAction($id, $omraid)
    {
        $omra = $this->getDoctrine()->getRepository('BtobOmraBundle:Omra')->find($omraid);
        $request = $this->get('request');
        $omraprice = $this->getDoctrine()
            ->getRepository('BtobOmraBundle:Omraprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OmrapriceType(), $omraprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allOmraprice = $this->getDoctrine()->getRepository('BtobOmraBundle:Omraprice')->findBy(array('omra' => $omra));
      
    
       
        if ($form->isValid()) {

            $omraprice->setDmj(new \DateTime());
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Modification: Période du Omra (".$omra->getTitre().") n° " . $omraprice->getId()." du ". date_format($omraprice->getDated(), 'Y-m-d')." au ". date_format($omraprice->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();


            return $this->redirect($this->generateUrl('btob_omraprice_homepage', array("omraid" => $omraid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobOmraBundle:Omraprice:edit.html.twig', array('form' => $form->createView(), 'price' => $omraprice, 'id' => $id, "omra" => $omra)
        );
    }

  
    public function deleteAction(Omraprice $omraprice, $omraid)
    {
        $em = $this->getDoctrine()->getManager();
        $omra = $this->getDoctrine()->getRepository('BtobOmraBundle:Omra')->find($omraid);

        if (!$omraprice) {
            throw new NotFoundHttpException("Omraprice non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Omra");
                        $hist->setMessage("Suppression: Période du Omra (".$omra->getTitre().") n° " . $omraprice->getId()." du ". date_format($omraprice->getDated(), 'Y-m-d')." au ". date_format($omraprice->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($omraprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_omraprice_homepage', array("omraid" => $omraid)));
    }



}












