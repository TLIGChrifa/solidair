<?php







namespace Btob\OmraBundle\Form;







use Symfony\Component\Form\AbstractType;



use Symfony\Component\Form\FormBuilderInterface;



use Symfony\Component\OptionsResolver\OptionsResolverInterface;







class OmraType extends AbstractType



{



    /**



     * @param FormBuilderInterface $builder



     * @param array $options



     */



    public function buildForm(FormBuilderInterface $builder, array $options)



    {



        $builder



            ->add('act', NULL, array('label' => 'active', 'required' => false))



            ->add('titre', 'text', array('label' => "Titre de omra", 'required' => true))



            ->add('libelle', 'text', array('label' => "Libellé promotion", 'required' => true))



            ->add('villed', 'text', array('label' => "Ville de départ ", 'required' => true))



            ->add('villea', 'text', array('label' => "Ville d'arrivée ", 'required' => true))



            //->add('voyagecart', 'text', array('label' => "Voyages a la carte", 'required' => false))



             ->add('ageenfmin', 'choice', array(

                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),

                'required' => true,

                'empty_value' => 'Choisissez ',

                'label'=>'Age Min Enfant  *',

            ))

            ->add('ageenfmax', 'choice', array(

                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),

                'required' => true,

                'empty_value' => 'Choisissez ',

                'label'=>'Age Max Enfant  *',

            ))

            ->add('agebmin', 'choice', array(

                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),

                'required' => true,

                'empty_value' => 'Choisissez ',

                'label'=>'Age Min Bébé  *',

            ))

            ->add('agebmax', 'choice', array(

                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),

                'required' => true,

                'empty_value' => 'Choisissez ',

                'label'=>'Age Max Bébé  *',

            ))     

            ->add('dureeomra', 'number', array('label' => "Durée du omra", 'required' => true))



            ->add('pindex', NULL, array('label' => "Inclure dans la liste des promos", 'required' => false))



            ->add('prix', NULL, array('label' => "Prix à partir de :", 'required' => true))

            ->add('marge', NULL, array('label' => 'Marge(En pourcentage) ', 'required' => true))

            ->add('prixavance', NULL, array('label' => 'Avance(En pourcentage) ', 'required' => true))

    





            ->add('pindex', NULL, array('label' => "Promo Index", 'required' => false))



            ->add('themes', NULL, array('label' => "Thèmes", 'required' => true))



            ->add('arrangement', NULL, array('label' => "Logement", 'required' => false))



            ->add('hotel', NULL, array('label' => "Hôtel : ", 'required' => false))



            ->add('nbretoile', NULL, array('label' => "Nombre d'étoiles :  ", 'required' => false))


            ->add('maps', NULL, array('label' => "Adresse Maps", 'required' => false))

            ->add('description', NULL, array('label' => "Description :  ", 'required' => false)) ;



    }







    /**



     * @param OptionsResolverInterface $resolver



     */



    public function setDefaultOptions(OptionsResolverInterface $resolver)



    {



        $resolver->setDefaults(array(



            'data_class' => 'Btob\OmraBundle\Entity\Omra'



        ));



    }







    /**



     * @return string



     */



    public function getName()



    {



        return 'btob_omrabundle_omra';



    }



}



