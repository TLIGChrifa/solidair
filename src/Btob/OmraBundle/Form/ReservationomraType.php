<?php

namespace Btob\OmraBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationomraType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          //  ->add('dcr')
        // ->add('dated', 'date', ['label' => "Date de Début :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
       //  ->add('datef', 'date', ['label' => "Date de Fin :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
          //  ->add('total')
         //   ->add('etat')
           // ->add('avance')
           // ->add('resultatfinal')
            //->add('numautoris')
            ->add('omra')
            //->add('omraprice')
            ->add('client')
            //->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\OmraBundle\Entity\Reservationomra'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_omrabundle_reservationomra';
    }
}
