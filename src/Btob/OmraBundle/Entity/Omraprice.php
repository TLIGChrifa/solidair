<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Omraprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\OmrapriceRepository")
 */
class Omraprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

    /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;
    
     /**
     * @var float
     *
     * @ORM\Column(name="priceenf11", type="float" , nullable=true)
     */
    private $priceenf11;
    
     /**
     * @var float
     *
     * @ORM\Column(name="priceenf12", type="float" , nullable=true)
     */
    private $priceenf12;

     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Omra", inversedBy="omraprice")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;

    /**
     * @var float
     *
     * @ORM\Column(name="supsingle", type="float" , nullable=true)
     */
    private $supsingle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="perssupsingle", type="boolean" , nullable=true)
     */

    private $perssupsingle;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Omraprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Omraprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
    /**
     * Set priceenf11
     *
     * @param float $priceenf11
     * @return Omraprice
     */
    public function setPriceenf11($priceenf11)
    {
        $this->priceenf11 = $priceenf11;

        return $this;
    }

    /**
     * Get priceenf11
     *
     * @return float 
     */
    public function getPriceenf11()
    {
        return $this->priceenf11;
    }
    
    /**
     * Set priceenf12
     *
     * @param float $priceenf12
     * @return Omraprice
     */
    public function setPriceenf12($priceenf12)
    {
        $this->priceenf12 = $priceenf12;

        return $this;
    }

    /**
     * Get priceenf12
     *
     * @return float 
     */
    public function getPriceenf12()
    {
        return $this->priceenf12;
    }   
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Omraprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Omraprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Omraprice
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Omraprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Omraprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\Omra $omra
     * @return Omraprice
     */
    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)
    {
        $this->omra = $omra;

        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\Omra 
     */
    public function getOmra()
    {
        return $this->omra;
    }
    
    
        /**
     * Set supsingle
     *
     * @param float $supsingle
     * @return Omraprice
     */
    public function setSupsingle($supsingle)
    {
        $this->supsingle = $supsingle;

        return $this;
    }

    /**
     * Get supsingle
     *
     * @return float 
     */
    public function getSupsingle()
    {
        return $this->supsingle;
    }

    /**
     * Set perssupsingle
     *
     * @param boolean $perssupsingle
     * @return Omraprice
     */
    public function setPerssupsingle($perssupsingle)
    {
        $this->perssupsingle = $perssupsingle;

        return $this;

    }

    /**
     * Get perssupsingle
     *
     * @return boolean
     */
    public function getPerssupsingle()
    {
        return $this->perssupsingle;
    }

}
