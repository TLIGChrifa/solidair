<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpomra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\TpomraRepository")
 */
class Tpomra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Personalisationomra", inversedBy="tpomra")
     * @ORM\JoinColumn(name="personalisationomra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $personalisationomra;
    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    private $pays;
    
     /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime" , nullable=true)
     */
    private $dated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="datetime" , nullable=true)
     */
    private $dater;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dateflex", type="boolean" , nullable=true)
     */
    private $dateflex;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personalisationomra
     *
     * @param \Btob\OmraBundle\Entity\Personalisationomra $personalisationomra
     * @return Tpomra
     */
    public function setPersonalisationomra(\Btob\OmraBundle\Entity\Personalisationomra $personalisationomra = null)
    {
        $this->personalisationomra = $personalisationomra;

        return $this;
    }

    /**
     * Get personalisationomra
     *
     * @return \Btob\OmraBundle\Entity\Personalisationomra
     */
    public function getPersonalisationomra()
    {
        return $this->personalisationomra;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Tpomra
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }
    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    
    
    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Tpomra
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }
    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }
    
    
     /**
     * Set dateflex
     *
     * @param boolean $dateflex
     * @return Tpomra
     */
    public function setDateflex($dateflex)
    {
        $this->dateflex = $dateflex;

        return $this;
    }

    /**
     * Get dateflex
     *
     * @return boolean 
     */
    public function getDateflex()
    {
        return $this->dateflex;
    }
     /**
     * Set pays
     *
     * @param string $pays
     * @return Tpomra
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }
    
    
    
    /**
     * Set ville
     *
     * @param string $ville
     * @return Tpomra
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }
    
       
	
}
