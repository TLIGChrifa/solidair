<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplemento
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\SupplementoRepository")
 */
class Supplemento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    /**
     * @ORM\ManyToOne(targetEntity="Omra", inversedBy="supplemento")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Supplemento
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Supplemento
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\Omra $omra
     * @return Supplemento
     */
    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)
    {
        $this->omra = $omra;

        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\Omra 
     */
    public function getOmra()
    {
        return $this->omra;
    }
}
