<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationomra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\ReservationomraRepository")
 */
class Reservationomra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date")
     */
    private $dated;
    
  /**
     * @var \DateTime
     *
     * @ORM\Column(name="datef", type="date")
     */
    private $datef;
   /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;


    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float")
     */
    private $avance;


    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    

    /**
     * @ORM\ManyToOne(targetEntity="Omra", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;
    
    /**
     * @ORM\ManyToOne(targetEntity="Omraprice", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="omraprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omraprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Resaomracomment", mappedBy="reservationomra", cascade={"remove"})
     */
    protected $resaomracomment;
   
    /**
     * @ORM\OneToMany(targetEntity="Reservationodetail", mappedBy="reservationomra", cascade={"remove"})
     */
    protected $reservationodetail;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationomra
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationomra
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set omraprice
     *
     * @param \Btob\OmraBundle\Entity\Omraprice $omraprice
     * @return Reservationomra
     */
    public function setOmraprice(\Btob\OmraBundle\Entity\Omraprice $omraprice = null)
    {
        $this->omraprice = $omraprice;

        return $this;
    }

    /**
     * Get omraprice
     *
     * @return \Btob\OmraBundle\Entity\Omraprice 
     */
    public function getOmraprice()
    {
        return $this->omraprice;
    }
    
       
    
    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\Omra $omra
     * @return Reservationomra
     */
    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)
    {
        $this->omra = $omra;

        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\Omra 
     */
    public function getOmra()
    {
        return $this->omra;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationomra
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationomra
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationodetail
     *
     * @param \Btob\OmraBundle\Entity\Reservationodetail $reservationodetail
     * @return Reservationomra
     */
    public function addReservationodetail(\Btob\OmraBundle\Entity\Reservationodetail $reservationodetail)
    {
        $this->reservationodetail[] = $reservationodetail;

        return $this;
    }

    /**
     * Remove reservationodetail
     *
     * @param \Btob\OmraBundle\Entity\Reservationodetail $reservationodetail
     */
    public function removeReservationodetail(\Btob\OmraBundle\Entity\Reservationodetail $reservationodetail)
    {
        $this->reservationodetail->removeElement($reservationodetail);
    }

    /**
     * Get reservationodetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationodetail()
    {
        return $this->reservationodetail;
    }

        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservationomra
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservationomra
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservationomra
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }
    
    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservationomra
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }
    
    
    /**
     * Add resaomracomment
     *
     * @param \Btob\OmraBundle\Entity\Resaomracomment $resaomracomment
     * @return Reservationomra
     */
    public function addResaomracomment(\Btob\OmraBundle\Entity\Resaomracomment $resaomracomment)
    {
        $this->resaomracomment[] = $resaomracomment;

        return $this;
    }

    /**
     * Remove resaomracomment
     *
     * @param \Btob\OmraBundle\Entity\Resaomracomment $resaomracomment
     */
    public function removeResaomracomment(\Btob\OmraBundle\Entity\Resaomracomment $resaomracomment)
    {
        $this->resaomracomment->removeElement($resaomracomment);
    }

    /**
     * Get resaomracomment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResaomracomment()
    {
        return $this->resaomracomment;
    }


    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Reservationomra
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Reservationomra
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return Reservationomra
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return Reservationomra
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return Reservationomra
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }
}
