<?php

namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personalisationomra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\PersonalisationomraRepository")
 */
class Personalisationomra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="personalisationomra")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Omra", inversedBy="personalisationomra")
     * @ORM\JoinColumn(name="omra_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $omra;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="personalisationomra")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
            
    /**
     * @var string
     *
     * @ORM\Column(name="typec", type="string", length=255, nullable=true)
     */
    private $typec;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="formule", type="string", length=255, nullable=true)
     */
    private $formule;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="transfert", type="string", length=255, nullable=true)
     */
    private $transfert;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="excursion", type="integer" , nullable=true)
     */
    private $excursion;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbradult", type="integer" , nullable=true)
     */
    private $nbradult;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrenf", type="integer" , nullable=true)
     */
    private $nbrenf;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrbebe", type="integer" , nullable=true)
     */
    private $nbrbebe;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrch", type="integer" , nullable=true)
     */
    private $nbrch;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="budgetmin", type="float" , nullable=true)
     */
    private $budgetmin;
    
    
     /**
     * @var float
     *
     * @ORM\Column(name="budgetmax", type="float" , nullable=true)
     */
    private $budgetmax;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime" , nullable=true)
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text" , nullable=true)
     */
    private $remarque;

    /**

     * @ORM\OneToMany(targetEntity="Tpomra", mappedBy="personalisationomra")

     */

    protected $personalisationomra;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    public function __construct(){
        $this->dcr=new \DateTime();
    }

    
    

     /**
     * Set typec
     *
     * @param string $typec
     * @return Personalisationomra
     */
    public function setTypec($typec)
    {
        $this->typec = $typec;

        return $this;
    }

    /**
     * Get typec
     *
     * @return string 
     */
    public function getTypec()
    {
        return $this->typec;
    }
    
    
    /**
     * Set formule
     *
     * @param string $formule
     * @return Personalisationomra
     */
    public function setFormule($formule)
    {
        $this->formule = $formule;

        return $this;
    }

    /**
     * Get formule
     *
     * @return string 
     */
    public function getFormule()
    {
        return $this->formule;
    }
    
    /**
     * Set transfert
     *
     * @param string $transfert
     * @return Personalisationomra
     */
    public function setTransfert($transfert)
    {
        $this->transfert = $transfert;

        return $this;
    }

    /**
     * Get transfert
     *
     * @return string 
     */
    public function getTransfert()
    {
        return $this->transfert;
    }
    
    

    
     /**
     * Set nbradult
     *
     * @param integer $nbradult
     * @return Personalisationomra
     */
    public function setNbradult($nbradult)
    {
        $this->nbradult = $nbradult;

        return $this;
    }
    
    
    /**
     * Get nbradult
     *
     * @return integer 
     */
    public function getNbradult()
    {
        return $this->nbradult;
    }

    /**
     * Set nbrenf
     *
     * @param integer $nbrenf
     * @return Personalisationomra
     */
    public function setNbrenf($nbrenf)
    {
        $this->nbrenf = $nbrenf;

        return $this;
    }

    /**
     * Get nbrenf
     *
     * @return integer 
     */
    public function getNbrenf()
    {
        return $this->nbrenf;
    }

     /**
     * Set nbrbebe
     *
     * @param integer $nbrbebe
     * @return Personalisationomra
     */
    public function setNbrbebe($nbrbebe)
    {
        $this->nbrbebe = $nbrbebe;

        return $this;
    }

    /**
     * Get nbrbebe
     *
     * @return integer 
     */
    public function getNbrbebe()
    {
        return $this->nbrbebe;
    }
    
    
    /**
     * Set budgetmin
     *
     * @param float $budgetmin
     * @return Personalisationomra
     */
    public function setBudgetmin($budgetmin)
    {
        $this->budgetmin = $budgetmin;

        return $this;
    }

    /**
     * Get budgetmin
     *
     * @return float 
     */
    public function getBudgetmin()
    {
        return $this->budgetmin;
    }
    
    
    
     /**
     * Set budgetmax
     *
     * @param float $budgetmax
     * @return Personalisationomra
     */
    public function setBudgetmax($budgetmax)
    {
        $this->budgetmax = $budgetmax;

        return $this;
    }

    /**
     * Get budgetmax
     *
     * @return float 
     */
    public function getBudgetmax()
    {
        return $this->budgetmax;
    }
    
    
    /**
     * Get excursion
     *
     * @return integer 
     */
    public function getExcursion()
    {
        return $this->excursion;
    }
    /**
     * Set excursion
     *
     * @param integer $excursion
     * @return Personalisationomra
     */
    public function setExcursion($excursion)
    {
        $this->excursion = $excursion;

        return $this;
    }

    /**
     * Get nbrch
     *
     * @return integer 
     */
    public function getNbrch()
    {
        return $this->nbrch;
    }
    
    /**
     * Set nbrch
     *
     * @param integer $nbrch
     * @return Personalisationomra
     */
    public function setNbrch($nbrch)
    {
        $this->nbrch = $nbrch;

        return $this;
    }
    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Personalisationomra
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Personalisationomra
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Personalisationomra
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set omra
     *
     * @param \Btob\OmraBundle\Entity\Omra $omra
     * @return Personalisationomra
     */
    public function setOmra(\Btob\OmraBundle\Entity\Omra $omra = null)
    {
        $this->omra = $omra;

        return $this;
    }

    /**
     * Get omra
     *
     * @return \Btob\OmraBundle\Entity\Omra 
     */
    public function getOmra()
    {
        return $this->omra;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Personalisationomra
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
    /**

     * Add categories

     *

     * @param \Btob\OmraBundle\Entity\Tpomra $personalisationomra

     * @return Personalisationomra

     */

    public function addPersonalisationomra(\Btob\OmraBundle\Entity\Tpomra $personalisationomra)

    {

        $this->personalisationomra[] = $personalisationomra;



        return $this;

    }



    /**

     * Remove personalisationomra

     *

     * @param \Btob\OmraBundle\Entity\Tpomra $personalisationomra

     */

    public function removePersonalisationomra(\Btob\OmraBundle\Entity\Tpomra $personalisationomra)

    {

        $this->personalisationomra->removeElement($personalisationomra);

    }



    /**

     * Get personalisationomra

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getPersonalisationomra()

    {

        return $this->personalisationomra;

    }
    
        
    
    
}
