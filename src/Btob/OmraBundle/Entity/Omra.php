<?php
namespace Btob\OmraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Omra
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\OmraBundle\Entity\OmraRepository")
 */
class Omra
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date", nullable=true)
     */
    private $dcr;
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean" , nullable=true)
     */
    private $act;
    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)
     */
    private $libelle;
    /**
     * @var string
     *
     * @ORM\Column(name="dureeomra", type="string", length=255 , nullable=true)
     */
    private $dureeomra;
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float" , nullable=true)
     */
    private $prix;
    /**
     * @ORM\OneToMany(targetEntity="Imgeomra", mappedBy="omra")
     */
    protected $imgeomra;
    /**
     * @var boolean
     *
     * @ORM\Column(name="pindex", type="boolean" , nullable=true)
     */
    private $pindex;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Themes", inversedBy="omra")
     * @ORM\JoinColumn(name="themes_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $themes;
    /**
     * @var string
     *
     * @ORM\Column(name="villed", type="string", length=255 , nullable=true)
     */
    private $villed;
    /**
     * @var string
     *
     * @ORM\Column(name="voyagecart", type="string", length=255 , nullable=true)
     */
    private $voyagecart;
    /**
     * @var string
     *
     * @ORM\Column(name="villea", type="string", length=255 , nullable=true)
     */
    private $villea;
    /**
     * @var string
     *
     * @ORM\Column(name="hotel", type="string", length=255 , nullable=true)
     */
    private $hotel;
    /**
     * @var integer
     *
     * @ORM\Column(name="nbretoile", type="integer" , nullable=true)
     */
    private $nbretoile;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */
    private $description;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="omra")
     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $arrangement;
     /**
     * @ORM\OneToMany(targetEntity="Supplemento", mappedBy="omra", cascade={"remove"})
     */
    protected $supplemento;
    /**
     * @ORM\OneToMany(targetEntity="Omraprice", mappedBy="omra", cascade={"remove"})
     */
    protected $omraprice;
    /**
     * @var float
     *
     * @ORM\Column(name="prixavance", type="float" , nullable=true)
     */
    private $prixavance;       
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */
    private $ageenfmin;
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */
    private $ageenfmax;
    /**
     * @var string
     *
     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)
     */
    private $agebmin;
    /**
     * @var string
     *
     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)
     */
    private $agebmax;
    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */
    private $marge; 
    /**
     * @var string
     *
     * @ORM\Column(name="maps", type="string" , nullable=true)
     */
    private $maps; 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set seotitle
     *
     * @param string $seotitle
     * @return Omra
     */
    public  function  __construct(){
        $this->dcr = new \DateTime();
        $this->supplemento = new \Doctrine\Common\Collections\ArrayCollection();
        $this->omraprice = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Omra
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Omra
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dureeomra
     *
     * @param string $dureeomra
     * @return Omra
     */
    public function setDureeomra($dureeomra)
    {
        $this->dureeomra = $dureeomra;
        return $this;
    }

    /**
     * Get dureeomra
     *
     * @return string 
     */
    public function getDureeomra()
    {
        return $this->dureeomra;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Omra
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set pindex
     *
     * @param boolean $pindex
     * @return Omra
     */
    public function setPindex($pindex)
    {
        $this->pindex = $pindex;
        return $this;
    }

    /**
     * Get pindex
     *
     * @return boolean 
     */
    public function getPindex()
    {
        return $this->pindex;
    }

    /**
     * Set villed
     *
     * @param string $villed
     * @return Omra
     */
    public function setVilled($villed)
    {
        $this->villed = $villed;
        return $this;
    }

    /**
     * Get villed
     *
     * @return string 
     */
    public function getVilled()
    {
        return $this->villed;
    }

    /**
     * Set voyagecart
     *
     * @param string $voyagecart
     * @return Omra
     */
    public function setVoyagecart($voyagecart)
    {
        $this->voyagecart = $voyagecart;
        return $this;
    }

    /**
     * Get voyagecart
     *
     * @return string 
     */
    public function getVoyagecart()
    {
        return $this->voyagecart;
    }

    /**
     * Set hotel
     *
     * @param string $hotel
     * @return Omra
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
        return $this;
    }

    /**
     * Get hotel
     *
     * @return string 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set nbretoile
     *
     * @param integer $nbretoile
     * @return Omra
     */
    public function setNbretoile($nbretoile)
    {
        $this->nbretoile = $nbretoile;
        return $this;
    }

    /**
     * Get nbretoile
     *
     * @return integer 
     */
    public function getNbretoile()
    {
        return $this->nbretoile;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Omra
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }







    /**



     * Set dcr



     *



     * @param \DateTime $dcr



     * @return Omra



     */



    public function setDcr($dcr)



    {



        $this->dcr = $dcr;







        return $this;



    }







    /**



     * Get dcr



     *



     * @return \DateTime 



     */



    public function getDcr()



    {



        return $this->dcr;



    }







    /**



     * Set act



     *



     * @param boolean $act



     * @return Omra



     */



    public function setAct($act)



    {



        $this->act = $act;







        return $this;



    }







    /**



     * Get act



     *



     * @return boolean 



     */



    public function getAct()



    {



        return $this->act;



    }















    /**



     * Add imgeomra



     *



     * @param \Btob\OmraBundle\Entity\Imgeomra $imgeomra



     * @return Omra



     */



    public function addImgeomra(\Btob\OmraBundle\Entity\Imgeomra $imgeomra)



    {



        $this->imgeomra[] = $imgeomra;







        return $this;



    }







    /**



     * Remove imgeomra



     *



     * @param \Btob\OmraBundle\Entity\Imgeomra $imgeomra



     */



    public function removeImgeomra(\Btob\OmraBundle\Entity\Imgeomra $imgeomra)



    {



        $this->imgeomra->removeElement($imgeomra);



    }







    /**



     * Get imgeomra



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgeomra()



    {



        return $this->imgeomra;



    }







    /**



     * Set villea



     *



     * @param string $villea



     * @return Omra



     */



    public function setVillea($villea)



    {



        $this->villea = $villea;







        return $this;



    }







    /**



     * Get villea



     *



     * @return string 



     */



    public function getVillea()



    {



        return $this->villea;



    }







    /**



     * Set themes



     *



     * @param \Btob\HotelBundle\Entity\Themes $themes



     * @return Omra



     */



    public function setThemes(\Btob\HotelBundle\Entity\Themes $themes = null)



    {



        $this->themes = $themes;







        return $this;



    }







    /**



     * Get themes



     *



     * @return \Btob\HotelBundle\Entity\Themes 



     */



    public function getThemes()



    {



        return $this->themes;



    }







    /**



     * Set arrangement



     *



     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement



     * @return Omra



     */



    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)



    {



        $this->arrangement = $arrangement;







        return $this;



    }







    /**



     * Get arrangement



     *



     * @return \Btob\HotelBundle\Entity\Arrangement 



     */



    public function getArrangement()



    {



        return $this->arrangement;



    }

    

    

     /**

     * Add supplemento

     *

     * @param \Btob\OmraBundle\Entity\Supplemento $supplemento

     * @return Omra

     */

    public function addSupplemento(\Btob\OmraBundle\Entity\Supplemento $supplemento)

    {

        $this->supplemento[] = $supplemento;



        return $this;

    }



    /**

     * Remove supplemento

     *

     * @param \Btob\OmraBundle\Entity\Supplemento $supplemento

     */

    public function removeSupplemento(\Btob\OmraBundle\Entity\Supplemento $supplemento)

    {

        $this->supplemento->removeElement($supplemento);

    }



    /**

     * Get supplemento

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getSupplemento()

    {

        return $this->supplemento;

    }

  

    

     /**

     * Add omraprice

     *

     * @param \Btob\OmraBundle\Entity\Omraprice $omraprice

     * @return Omra

     */

    public function addOmraprice(\Btob\OmraBundle\Entity\Omraprice $omraprice)

    {

        $this->omraprice[] = $omraprice;



        return $this;

    }



    /**

     * Remove omraprice

     *

     * @param \Btob\OmraBundle\Entity\Omraprice $omraprice

     */

    public function removeOmraprice(\Btob\OmraBundle\Entity\Omraprice $omraprice)

    {

        $this->omraprice->removeElement($omraprice);

    }



    /**

     * Get Omraprice

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getOmraprice()

    {

        return $this->omraprice;

    }   

    

    /**

     * Set prixavance

     *

     * @param float $prixavance

     * @return Omra

     */



    public function setPrixavance($prixavance)

    {

        $this->prixavance = $prixavance;

        return $this;

    }



    /**

     * Get prixavance

     *

     * @return float 

     */



    public function getPrixavance()

    {

        return $this->prixavance;



    }    

    

    

    

     /**

     * Set ageenfmin

     *

     * @param string $ageenfmin

     * @return Omra

     */



    public function setAgeenfmin($ageenfmin)



    {

        $this->ageenfmin = $ageenfmin;

        return $this;



    }







    /**

     * Get ageenfmin

     *

     * @return string 

     */



    public function getAgeenfmin()



    {

        return $this->ageenfmin;



    }

    

    

     /**

     * Set ageenfmax

     *

     * @param string $ageenfmax

     * @return Omra

     */



    public function setAgeenfmax($ageenfmax)



    {

        $this->ageenfmax = $ageenfmax;

        return $this;



    }







    /**

     * Get ageenfmax

     *

     * @return string 

     */



    public function getAgeenfmax()



    {

        return $this->ageenfmax;



    }

    

    

    

    

    

    

         /**

     * Set agebmin

     *

     * @param string $agebmin

     * @return Omra

     */



    public function setAgebmin($agebmin)



    {

        $this->agebmin = $agebmin;

        return $this;



    }







    /**

     * Get agebmin

     *

     * @return string 

     */



    public function getAgebmin()



    {

        return $this->agebmin;



    }

    

    

     /**

     * Set agebmax

     *

     * @param string $agebmax

     * @return Omra

     */



    public function setAgebmax($agebmax)



    {

        $this->agebmax = $agebmax;

        return $this;



    }







    /**

     * Get agebmax

     *

     * @return string 

     */



    public function getAgebmax()



    {

        return $this->agebmax;



    }

      /**
     * Set marge
     *
     * @param float $marge
     * @return Omra
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        
   

    /**
     * Set maps
     *
     * @param string $maps
     * @return Omra
     */

    public function setMaps($maps)

    {
        $this->maps = $maps;
        return $this;

    }


    /**
     * Get maps
     *
     * @return string
     */

    public function getMaps()

    {
        return $this->maps;

    }
    public function __toString() {
        return $this->titre;
     }

}



