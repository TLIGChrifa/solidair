<?php

namespace Btob\ActiviteBundle\Controller;

use Btob\ActiviteBundle\Form\ActiviteeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\ActiviteBundle\Entity\Activite;
use Btob\ActiviteBundle\Entity\Type;
use Btob\ActiviteBundle\Entity\Imgact;
use Btob\ActiviteBundle\Form\ActiviteType;

/**
 * Activite controller.
 *
 */
class ActiviteController extends Controller
{

    /**
     * Lists all Activite entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobActiviteBundle:Activite')->findAll();

        return $this->render('BtobActiviteBundle:Activite:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Activite entity.
     *
     */
    public function createAction(Request $request , $id)
    {
        $em = $this->getDoctrine()->getManager();

        $type = $em->getRepository('BtobActiviteBundle:Type')->find($id);


        $entity = new Activite();
        $entity->setType($type);
      $form = $this->createCreateForm($entity , $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgact();
                        $img->setActivite($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('type_show', array('id' => $id)));
        }

        return $this->render('BtobActiviteBundle:Activite:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Activite entity.
     *
     * @param Activite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Activite $entity, $id )
    {
        $form = $this->createForm(new ActiviteType(), $entity, array(
            'action' => $this->generateUrl('activite_create', array('id' => $id)),
                    'method' => 'POST',
        ));

     //   $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Activite entity.
     *
     */
    public function newAction($id)
    {   $em = $this->getDoctrine()->getManager();

        $type = $em->getRepository('BtobActiviteBundle:Type')->find($id);


        $entity = new Activite();
        $entity->setType($type);
        $form   = $this->createCreateForm($entity , $id);

        return $this->render('BtobActiviteBundle:Activite:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Activite entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobActiviteBundle:Activite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobActiviteBundle:Activite:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Activite entity.
     *
     */
    public function editAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobActiviteBundle:Activite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activite entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobActiviteBundle:Activite:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Activite entity.
    *
    * @param Activite $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Activite $entity)
    {
        $nametype = $entity->getType()->getType();
        if ($nametype == "Pages dynamiques"){
            $form = $this->createForm(new ActiviteeType(), $entity, array(
                'action' => $this->generateUrl('activite_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }else{
        $form = $this->createForm(new ActiviteType(), $entity, array(
            'action' => $this->generateUrl('activite_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        }
       // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Activite entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobActiviteBundle:Activite')->find($id);
        $idtype =$entity->getType()->getId();
        foreach ($entity->getImgact() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgact();
                        $img->setActivite($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('type_show', array('id' => $idtype)));

            //   return $this->redirect($this->generateUrl('activite_edit', array('id' => $id)));
        }

        return $this->render('BtobActiviteBundle:Activite:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Activite entity.
     *
     */
    public function deleteAction($id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobActiviteBundle:Activite')->find($id);
        $idtype =$entity->getType()->getId();


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('type_show', array('id' => $idtype)));

    }

    /**
     * Creates a form to delete a Activite entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('activite_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
