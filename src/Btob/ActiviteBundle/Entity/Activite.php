<?php

namespace Btob\ActiviteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ActiviteBundle\Entity\ActiviteRepository")
 */
class Activite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="Imgact", mappedBy="activite")
     */
    protected $imgact;
    /**
     * @ORM\OneToMany(targetEntity="Resevationact", mappedBy="activite")
     */
    protected $reservation;


    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="activite")
     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $arrangement;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="activite")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbj", type="integer", nullable=true)
     */
    private $nbj;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date", nullable=true)
     */
    private $dcr;



    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Activite
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Activite
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Activite
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Activite
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nbj
     *
     * @param integer $nbj
     * @return Activite
     */
    public function setNbj($nbj)
    {
        $this->nbj = $nbj;

        return $this;
    }

    /**
     * Get nbj
     *
     * @return integer 
     */
    public function getNbj()
    {
        return $this->nbj;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Activite
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Activite
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Activite
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set arrangement
     *
     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement
     * @return Activite
     */
    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return \Btob\HotelBundle\Entity\Arrangement 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Set type
     *
     * @param \Btob\ActiviteBundle\Entity\Type $type
     * @return Activite
     */
    public function setType(\Btob\ActiviteBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Btob\ActiviteBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add imgact
     *
     * @param \Btob\ActiviteBundle\Entity\Imgact $imgact
     * @return Activite
     */
    public function addImgact(\Btob\ActiviteBundle\Entity\Imgact $imgact)
    {
        $this->imgact[] = $imgact;

        return $this;
    }

    /**
     * Remove imgact
     *
     * @param \Btob\ActiviteBundle\Entity\Imgact $imgact
     */
    public function removeImgact(\Btob\ActiviteBundle\Entity\Imgact $imgact)
    {
        $this->imgact->removeElement($imgact);
    }

    /**
     * Get imgact
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImgact()
    {
        return $this->imgact;
    }

    /**
     * Add reservation
     *
     * @param \Btob\ActiviteBundle\Entity\Resevationact $reservation
     * @return Activite
     */
    public function addReservation(\Btob\ActiviteBundle\Entity\Resevationact $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \Btob\ActiviteBundle\Entity\Resevationact $reservation
     */
    public function removeReservation(\Btob\ActiviteBundle\Entity\Resevationact $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
