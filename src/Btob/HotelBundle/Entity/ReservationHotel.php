<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReservationHotel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ReservationHotelRepository")
 */
class ReservationHotel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedebut", type="date",nullable=true)
     */
    private $datedebut;
      /**
     * @var integer
     *
     * @ORM\Column(name="nbadultes", type="integer",nullable=true)
     */
    private $nbadultes;
          /**
     * @var integer
     *
     * @ORM\Column(name="nbenfants", type="integer",nullable=true)
     */
    private $nbenfants;
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefin", type="date",nullable=true)
     */
    private $datefin;
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime",nullable=true)
     */
    private $dcr;
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;
       /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchambres", type="integer")
     */
    private $nbchambres;
     /**
  * @ORM\OneToMany(targetEntity="chambre",mappedBy="reservationhotel",cascade={"persist", "remove"}, orphanRemoval=true)
  * @ORM\JoinColumn(nullable=true)
  */
  private $chambre;
   
    /**
      * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="reservationhotel")
      * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
      */
    protected $hotel;
    

//       /**
//    * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User",inversedBy="reservationhotel")
//    * @ORM\JoinColumn(nullable=true)
//    */
//     private $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Coordonneesreservationhotel",mappedBy="reservationhotel", cascade={"persist"})
    * @ORM\JoinColumn(nullable=true)
    */
    private $coordonnees;

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
        
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     *
     * @return ReservationHotel
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin
     *
     * @param \DateTime $datefin
     *
     * @return ReservationHotel
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     *
     * @return ReservationHotel
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return ReservationHotel
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return ReservationHotel
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

  



    

    

    /**
     * Set nbchambres
     *
     * @param integer $nbchambres
     *
     * @return ReservationHotel
     */
    public function setNbchambres($nbchambres)
    {
        $this->nbchambres = $nbchambres;

        return $this;
    }

    /**
     * Get nbchambres
     *
     * @return integer
     */
    public function getNbchambres()
    {
        return $this->nbchambres;
    }

  

     /**
      * Set hotel
      *
      * @param \Btob\HotelBundle\Entity\Hotel $hotel
      *
      * @return ReservationHotel
      */
     public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
     {
         $this->hotel = $hotel;

         return $this;
     }

     /**
      * Get hotel
      *
      * @return \Btob\HotelBundle\Entity\Hotel
      */
     public function getHotel()
     {
         return $this->hotel;
     }

    //  /**
    //   * Set user
    //   *
    //   * @param \User\UserBundle\Entity\User $user
    //   *
    //   * @return ReservationHotel
    //   */
    //  public function setUser(\User\UserBundle\Entity\User $user = null)
    //  {
    //      $this->user = $user;

    //      return $this;
    //  }

    //  /**
    //   * Get user
    //   *
    //   * @return \User\UserBundle\Entity\User
    //   */
    //  public function getUser()
    //  {
    //      return $this->user;
    //  }

    /**
     * Set nbadultes
     *
     * @param integer $nbadultes
     *
     * @return ReservationHotel
     */
    public function setNbadultes($nbadultes)
    {
        $this->nbadultes = $nbadultes;

        return $this;
    }

    /**
     * Get nbadultes
     *
     * @return integer
     */
    public function getNbadultes()
    {
        return $this->nbadultes;
    }

    /**
     * Set nbenfants
     *
     * @param integer $nbenfants
     *
     * @return ReservationHotel
     */
    public function setNbenfants($nbenfants)
    {
        $this->nbenfants = $nbenfants;

        return $this;
    }

    /**
     * Get nbenfants
     *
     * @return integer
     */
    public function getNbenfants()
    {
        return $this->nbenfants;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return ReservationHotel
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return ReservationHotel
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }

    /**
     * Add chambre
     *
     * @param \Btob\HotelBundle\Entity\chambre $chambre
     *
     * @return ReservationHotel
     */
    public function addChambre(\Btob\HotelBundle\Entity\chambre $chambre)
    {
        $this->chambre[] = $chambre;

        return $this;
    }

    /**
     * Remove chambre
     *
     * @param \Btob\HotelBundle\Entity\chambre $chambre
     */
    public function removeChambre(\Btob\HotelBundle\Entity\chambre $chambre)
    {
        $this->chambre->removeElement($chambre);
    }

    /**
     * Get chambre
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChambre()
    {
        return $this->chambre;
    }

    /**
     * Add coordonnee
     *
     * @param \AppBundle\Entity\Coordonnesreservationhotel $coordonnee
     *
     * @return ReservationHotel
     */
    public function addCoordonnee(\AppBundle\Entity\Coordonnesreservationhotel $coordonnee)
    {
        $this->coordonnees[] = $coordonnee;

        return $this;
    }

    /**
     * Remove coordonnee
     *
     * @param \AppBundle\Entity\Coordonnesreservationhotel $coordonnee
     */
    public function removeCoordonnee(\AppBundle\Entity\Coordonnesreservationhotel $coordonnee)
    {
        $this->coordonnees->removeElement($coordonnee);
    }

    /**
     * Get coordonnees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoordonnees()
    {
        return $this->coordonnees;
    }
}
