<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class PromotionController extends Controller {

    public function indexAction($hotelid) {
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Promotion:index.html.twig', array('entities' => $hotel->getPromotion(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Promotion = new Promotion();
        $form = $this->createForm(new PromotionType(), $Promotion);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Promotion->setHotel($hotel);
                $em->persist($Promotion);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajouter: Promotion");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Promotion:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $request = $this->get('request');
        $Promotion = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Promotion')
                ->find($id);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PromotionType(), $Promotion);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modification: Promotion n° " . $Promotion->getId()." - " . $Promotion->getName()." du " . date_format($Promotion->getDated(), 'Y-m-d'). " au " . date_format($Promotion->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Promotion:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid, "hotel" => $hotel)
        );
    }

    public function deleteAction(Promotion $Promotion, $hotelid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Promotion) {
            throw new NotFoundHttpException("Promotion non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Suppression: Promotion n° " . $Promotion->getId()." - " . $Promotion->getName()." du " . date_format($Promotion->getDated(), 'Y-m-d'). " au " . date_format($Promotion->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Promotion);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_promotion_homepage', array("hotelid" => $hotelid)));
    }

}
