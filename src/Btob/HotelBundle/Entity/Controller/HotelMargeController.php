<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Entity\HotelmargeRepository;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class HotelMargeController extends Controller {

   public function homeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entitiesagence = $em->getRepository('UserUserBundle:User')->findRole();
        $entitiessales = $em->getRepository('UserUserBundle:User')->findRoles();


        $entities = array_merge($entitiesagence, $entitiessales);

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }
        return $this->render('BtobHotelBundle:HotelMarge:home.html.twig', array(
            'entities' => $entities,
        ));
    }



    public function resetAction($userid,Request $request) {
        $em = $this->getDoctrine()->getManager();

        $value = $request->get('_route_params');
        $x= (int)$value["userid"];

        $nameagence= $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->find($x);
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();

        $marge = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);

        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {
            $count=count($marge);

            $occurence= $request->request->get("val");

            $pers = $request->request->get("prst");
            if($pers==null)
            {
                $perst = false;
            }else{
                $perst = true;
            }

            $hotel = $request->request->get("hotel");
            foreach ($occurence as $keys => $value) {
               $zzz= $value;
            }

            foreach ($marge as $key => $value) {

                if ($marge == null) {
                    $oneuser = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($x);
                    $hotelmarges[$key] = new Hotelmarge();
                    $hotelmarges[$key]->setUser($oneuser);
                    $hotelmarges[$key]->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find(1));
                    $hotelmarges[$key]->setMarge($occurence);

                    $hotelmarges[$key]->setPrst(true);
                    $em->persist($hotelmarges[$key]);
                    $em->flush();
                } else {
                    $em = $this->getDoctrine()->getManager();
                    $hotelmarges[$key] = $em->getRepository('BtobHotelBundle:Hotelmarge')->find($value->getId());
                    $hotelmarges[$key]->setMarge($zzz);
                    $hotelmarges[$key]->setPrst($perst);
                    $em->flush();

                }
            }





            return $this->redirect($this->generateUrl('btob_margagence_homepage'));
        }

        $zz=$hotel[0];
        return $this->render('BtobHotelBundle:HotelMarge:index.html.twig', array(
            'marge' => $marge,
            "userid" => $userid,
            "nameagence" => $nameagence,
            'hotels' => $hotel,
            'zz' => $zz
        ));
    }


    public function indexAction($userid,Request $request) {
        $em = $this->getDoctrine()->getManager();

        $value = $request->get('_route_params');
        $x= (int)$value["userid"];

        $nameagence= $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->find($x);
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();


        $marge = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);

        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {
            $count=count($hotel);

            $occurence= $request->request->get("val");
            $pers = $request->request->get("prst");
            $hotel = $request->request->get("hotel");


            for ($i=0; $i<$count; $i++) {
                for ($j=0; $j<count($count); $j++) {
                    if($marge[$j]!=null)
                    {
                        $em->remove($marge[$j]);
                        $em->flush();
                    }

                }
                foreach ($request->request->get("val") as $key => $value) {


                    $ff=$request->request->get("val");

                    //Tools::dump($request->request, TRUE);
                    $yy= (int)$hotel[$key];

                    $oneuser = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($x);
                    $hotelmarge[$i] = new Hotelmarge();
                    $hotelmarge[$i]->setUser($oneuser);
                    $hotelmarge[$i]->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($yy));
                    $hotelmarge[$i]->setMarge($value);
                    if (isset($pers[$key]))
                        $hotelmarge[$i]->setPrst(true);
                    $em->persist($hotelmarge[$i]);
                    $em->flush();
                }
                $i=$count;
            }
            return $this->redirect($this->generateUrl('btob_margagence_homepage'));
        }

        $zz=$hotel[0];
        return $this->render('BtobHotelBundle:HotelMarge:index.html.twig', array(
            'marge' => $marge,
            "userid" => $userid,
            "nameagence" => $nameagence,
            'hotels' => $hotel,
            'zz' => $zz
        ));
    }
}
