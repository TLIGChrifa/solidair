<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Localisation;
use Btob\HotelBundle\Form\LocalisationType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class LocalisationController extends Controller {

    public function indexAction() {
        $localisation = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Localisation')
                ->findAll();
        return $this->render('BtobHotelBundle:Localisation:index.html.twig', array('entities' => $localisation));
    }

    public function addAction() {
        $localisation = new Localisation();
        $form = $this->createForm(new LocalisationType(), $localisation);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($localisation);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Localisation");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_localisation_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Localisation:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $localisation = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Localisation')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new LocalisationType(), $localisation);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Localisation n° " . $localisation->getId()." - " . $localisation->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_localisation_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Localisation:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Localisation $localisation) {
        $em = $this->getDoctrine()->getManager();

        if (!$localisation) {
            throw new NotFoundHttpException("Localisation non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Localisation n° " . $localisation->getId()." - " . $localisation->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($localisation);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_localisation_homepage'));
    }

}
