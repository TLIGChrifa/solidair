<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Themes;
use Btob\HotelBundle\Form\ThemesType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class ThemeController extends Controller {

    public function indexAction() {
        $theme = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Themes')
                ->findAll();
        return $this->render('BtobHotelBundle:Theme:index.html.twig', array('entities' => $theme));
    }

    public function addAction() {
        $theme = new Themes();
        $form = $this->createForm(new ThemesType(), $theme);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($theme);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Thème");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_theme_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Theme:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $theme = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Themes')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ThemesType(), $theme);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Thème n° " . $theme->getId()." - " . $theme->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_theme_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Theme:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Themes $theme) {
        $em = $this->getDoctrine()->getManager();

        if (!$theme) {
            throw new NotFoundHttpException("Theme non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Thème n° " . $theme->getId()." - " . $theme->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($theme);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_theme_homepage'));
    }

}
