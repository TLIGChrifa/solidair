<?php

namespace Btob\HotelBundle\Controller;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Entity\Excelclient;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Form\ExcelclientType;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ClientController extends Controller
{

    public function indexAction(Request $request)
    {
        $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findBy(array('act' => 1),array('id' => 'desc'));

        // test agence connected
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findAll();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        $user = $this->get('security.context')->getToken()->getUser();

        if (in_array("AGENCEID", $user->getRoles())) {
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabuser[$value->getClient()->getId()] = $value->getClient();
                }
            }
            $clients = $tabuser;
        }
 $excelclient = new Excelclient();
 $form = $this->createForm(new ExcelclientType(), $excelclient);


 $form->handleRequest($request);

 if ($form->isSubmitted() && $form->isValid()) {
     $em = $this->getDoctrine()->getManager();
     $excelclient->upload();
     $em->persist($excelclient);
     $em->flush($excelclient);
     
//Open the file.
$fileHandle = fopen( __DIR__.'/../../../../public_html/user/logos/'.$excelclient->getPath(), "r");
$array= array();
//Loop through the CSV rows.
while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
    //Dump out the row for the sake of clarity.
  
  $array[]=  $row;
}
   //var_dump($array).die;
   $pieces = array();
     foreach($array as $a){
       
         if($a != $array[0]){
           // var_dump($a).die;
          
            foreach($a as $cl){
                $pieces[] = explode(";", $cl);
                
            }
         
            //$pieces = explode(";", $a[0]);
           // $c =str_replace('""', , $pieces[0]);
           // var_dump($c).die;
           
            
         }
         
     }
   //  var_dump(count($pieces)).die;
     for ($i = 0; $i < count($pieces); $i++) {
        $client = new Clients();

        $client->setAct(1);
        $client->setType(1);
       
        $client->setname($pieces[$i][0]);
        $client->setPname($pieces[$i][1]);
        $client->setTel($pieces[$i][2]);
        $client->setCin($pieces[$i][3]);
       // $client->setMp($mp);
       // $client->setDateNaissance($dates);
        $client->setEmail($pieces[$i][4]);
       
       // $client->setAdresse($adresse);
       // $client->setVille($ville);
       // $client->setCp($cp);
       // $client->setPays($payss);
       // $client->setTypeClient($typeclient);

        $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();

       // var_dump($pieces).die; 
     }
     return $this->redirect($this->generateUrl('btob_client_homepage'));


 }
        return $this->render('BtobHotelBundle:Client:index.html.twig', array('entities' => $clients,'form' => $form->createView()));
    }

    public function generateCsvAction(){
 

        $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findBy(array('act' => 1),array('id' => 'desc'));
        $response = new StreamedResponse();
        $response->setCallback(function() use($clients){
        
          $handle = fopen('php://output', 'w+');
           // Nom des colonnes du CSV 
          fputcsv($handle, array('Nom','Prenom',
                                   'Mobile',
                                  
                                   'cin',
                                   'email',                                  
                                  
                  ),';');
                  
          //Champs
          foreach($clients as $row)
             {
             
                  fputcsv($handle,array($row->getName(),
                  $row->getPname(),
                  $row->getTel(),
                
                  $row->getCin() ,
                  $row->getEmail() 
                         ),';');
             
             }
             
          fclose($handle);
          });
        
         $response->setStatusCode(200);
         $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
         $response->headers->set('Content-Disposition','attachment; filename="export.csv"');
            
         return $response;                             
                 
      }


    public function archiveAction()
    {
        $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findBy(array('act' => 0));

        // test agence connected
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findAll();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        $user = $this->get('security.context')->getToken()->getUser();

        if (in_array("AGENCEID", $user->getRoles())) {
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabuser[$value->getClient()->getId()] = $value->getClient();
                }
            }
            $clients = $tabuser;
        }
        return $this->render('BtobHotelBundle:Client:archive.html.twig', array('entities' => $clients));
    }


    public function detailAction(Clients $client)
    {
        return $this->render('BtobHotelBundle:Client:detail.html.twig', array('entry' => $client));
    }

    public function ajxgetclientAction()
    {
        $request = $this->get('request');
        $email = $request->request->get('email');
        $client = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findOneBy(array('email' => $email));
        if ($client != null) {
            $tab["cin"] = $client->getCin();
            $tab["civ"] = $client->getCiv();
            $tab["pname"] = $client->getPname();
            $tab["name"] = $client->getName();
            $tab["tel"] = $client->getTel();
            $tab["adresse"] = $client->getAdresse();
            $tab["cp"] = $client->getCp();
            $tab["ville"] = $client->getVille();
            $tab["pays"] = $client->getPays()->getId();
        } else {
            $tab["cin"] = "";
            $tab["civ"] = "";
            $tab["pname"] = "";
            $tab["name"] = "";
            $tab["tel"] = "";
            $tab["adresse"] = "";
            $tab["cp"] = "";
            $tab["ville"] = "";
            $tab["pays"] = 1;
        }

//Tools::dump($request->request->get('cin'),true);
        return new JsonResponse($tab);
    }


    public function addAction()
    {
         $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();

        $client = new Clients();
        $client->setAct(1);
        $client->setType(0);
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
         
            $form->bind($request);
            //dump("grf").die;
            if ($form->isValid()) {
              
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Client");
                        $hist->setMessage("Ajout: Nouveau client ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_client_homepage'));
            } else {
                echo $form->getErrors();
            }
        }

        return $this->render('BtobHotelBundle:Client:form.html.twig', array('form' => $form->createView(),'countries'=>$countries));
    }

public function addmoralAction(){
     $request = $this->get('request');
     $nom=$request->request->get('nom');
     $mp=$request->request->get('mf');
     $date=$request->request->get('date_naissance');
     $mail=$request->request->get('email');
     $tel=$request->request->get('tel');
     $adresse=$request->request->get('adresse');
     $ville=$request->request->get('ville');
     $cp=$request->request->get('cp');
     $pays=$request->request->get('pays');
     $mail=$request->request->get('email');

     $typeclient = $request->request->get('typeclient');

    $payss =$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($pays);
 $dates =  new \DateTime($date);
  $client = new Clients();
        $client->setAct(1);
        $client->setType(1);
        $client->setPname($nom);
        $client->setMp($mp);
        $client->setDateNaissance($dates);
        $client->setEmail($mail);
        $client->setTel($tel);
        $client->setAdresse($adresse);
        $client->setVille($ville);
        $client->setCp($cp);
        $client->setPays($payss);
        $client->setTypeClient($typeclient);

    $em = $this->getDoctrine()->getManager();
        $em->persist($client);
        $em->flush();
        
      return $this->redirect($this->generateUrl('btob_client_homepage'));

}



public function editmoralAction($id){
     $request = $this->get('request');
     $nom=$request->request->get('nom');
     $mp=$request->request->get('mf');

     $date=$request->request->get('date_naissance');
     $mail=$request->request->get('email');
     $tel=$request->request->get('tel');
     $adresse=$request->request->get('adresse');
     $ville=$request->request->get('ville');
     $cp=$request->request->get('cp');
     $pays=$request->request->get('pays');
    $payss =$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($pays);

 //$dates = strtotime($date);
 $dates =  new \DateTime($date);


  $client = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->find($id);
        //$client->setAct(1);
        //$client->setType(1);
        $client->setPname($nom);
        $client->setMp($mp);

        $client->setDateNaissance($dates);
        $client->setEmail($mail);
        $client->setTel($tel);
        $client->setAdresse($adresse);
        $client->setVille($ville);
        $client->setCp($cp);
        $client->setPays($payss);
    



    $em = $this->getDoctrine()->getManager();

       
        $em->flush();
        
      return $this->redirect($this->generateUrl('btob_client_homepage'));

}



    public function editAction($id)
    {
        $request = $this->get('request');
        $client = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->find($id);
        $type=$client->getType();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ClientsType(), $client);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Client");
                        $hist->setMessage("Modification: Client " . $client->getId()." - " . $client->getCiv()." " . $client->getPname()." " . $client->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_client_homepage'));
        } else {
            echo $form->getErrors();
        }
 $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();
        return $this->render('BtobHotelBundle:Client:form.html.twig', array('form' => $form->createView(), 'id' => $id,'type'=>$type,'client'=>$client,'countries'=>$countries)
        );
    }

    public function deleteAction(Clients $clients)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$clients) {
            throw new NotFoundHttpException("Arrangement non trouv�e");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Client");
                        $hist->setMessage("Archivage: Client " . $clients->getId()." - " . $clients->getCiv()." " . $clients->getPname()." " . $clients->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $clients->setAct(false);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_client_homepage'));
    }

    public function activeAction(Clients $clients)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$clients) {
            throw new NotFoundHttpException("Arrangement non trouv�e");
        }
		
		$hist = new Historique();
		$hist->setIp($_SERVER['REMOTE_ADDR']);
		$hist->setType("BO");
		$hist->setBundle("Client");
		$hist->setMessage("Restauration: Client " . $clients->getId()." - " . $clients->getCiv()." " . $clients->getPname()." " . $clients->getName());
		$hist->setUser($this->get('security.context')->getToken()->getUser());
		$em->persist($hist);
		
        $clients->setAct(true);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_client_archive_homepage'));
    }





}
