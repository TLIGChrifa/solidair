<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Hotelprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\HotelpriceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Pricearr;
use Btob\HotelBundle\Entity\Pricesupplement;
use Btob\HotelBundle\Entity\Pricechild;
use Btob\HotelBundle\Entity\Priceroom;
use Btob\HotelBundle\Entity\Hotelsupplement;
use Btob\HotelBundle\Entity\Hotelchild;
use Btob\HotelBundle\Entity\Hotelroom;
use Btob\HotelBundle\Entity\Hotelarrangement;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;



class HotelpriceController extends Controller
{

    public function indexAction($hotelid, $marcheid)
    {
        $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->find($hotelid);
	    $name =  $hotel->getName();
        $price = array();
        foreach ($hotel->getHotelprice() as $value) {
            if ($value->getMarcher()->getId() == $marche->getId()) {
                $price[] = $value;
            }
        }
        return $this->render('BtobHotelBundle:Hotelprice:index.html.twig', array(
            'entities' => $price,
            "hotelid" => $hotelid,
			"name" => $name,
            'marcheid' => $marcheid
        ));
    }


    public function addAction($hotelid, $marcheid)
    {
        $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
//*************** to show the real lists ************************* 
        $allpriceroom = array();
        $allpricesupplement = array();
        $allpricechild = array();
        $allpricearr = array();
        $allhotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $hotel));
        $allroom = array();
        $allsupplement = array();
        $allchild = array();
        $allarrangement = array();
        if (!empty($allhotelprice)){
            $allpriceroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricesupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricechild = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricearr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findBy(array('hotelprice' => $allhotelprice[0]));
            $i = 0;
            foreach ($allpriceroom as $onepriceroom) {
                $allroom[$i] = $onepriceroom->getRoom();
                $i = $i + 1;
            }
            $i = 0;
            foreach ($allpricesupplement as $onepricesupplement) {
                $allsupplement[$i] = $onepricesupplement->getHotelsupplement()->getSupplement();
                $i = $i + 1;
            }
            $i = 0;
            foreach ($allpricechild as $onepricechild) {
                if (! in_array($onepricechild->getHotelchild()->getChild(), $allchild)){
                    $allchild[$i] = $onepricechild->getHotelchild()->getChild();
                    $i = $i + 1;
                }
            }
            $i = 0;
            foreach ($allpricearr as $onepricearr) {
                $allarrangement[$i] = $onepricearr->getHotelarrangement()->getArrangement();
                $i = $i + 1;
            }
           
        }


//*************** end to show the real lists ************************* 
        $hotelprice = new Hotelprice();
        $form = $this->createForm(new HotelpriceType(), $hotelprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $hotelprice->setMarcher($marche);
                $hotelprice->setHotel($hotel);
                $em->persist($hotelprice);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Période d'hôtel ". $hotel->getName() );
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                // injection des arrangements
                $arrangement = $request->request->get("arrangement");
                $margearrangement = $request->request->get("margearrangement");
                $minstay = $request->request->get("minstay");
                $persarr = $request->request->get("persarr");
                $margepersarr = $request->request->get("margepersarr");
                $actarr = $request->request->get("actarr");
                foreach ($arrangement as $key => $value) {
                    $pricear = new Pricearr();
                    $pricear->setHotelprice($hotelprice);



                    $toshow = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelarrangement')->findBy(array('hotel' => $hotel,'arrangement' => $key));
                    if (!empty($toshow)){
                        $pricear->setHotelarrangement($toshow[0]);
                    }else{
                        $pricear->setHotelarrangement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelarrangement')->find($key));
                    }

                    if (isset($persarr[$key])){
                        $pricear->setPers(1);
                    }
                    else{$pricear->setPers(0);}
                    
                    
                     if (isset($margepersarr[$key])){
                        $pricear->setPersm(1);
                    }
                    else{$pricear->setPersm(0);}
                    
                    
                    if (isset($actarr[$key])){
                        $pricear->setEtat(1);
                    }
                    else{$pricear->setEtat(0);}
                    $pricear->setMinstay($minstay[$key]);
                    $pricear->setMarge($margearrangement[$key]);
                    $pricear->setPrice($value);
                    $em->persist($pricear);
                    $em->flush();
                }
                // injection de priceroom
                $room = $request->request->get('room');
                foreach ($room as $key => $value) {
                    $priceroom = new Priceroom();
                    $priceroom->setHotelprice($hotelprice);
                    $priceroom->setQte($value);
                    $priceroom->setRoom($this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($key));
                    $em->persist($priceroom);
                    $em->flush();
                }
                // injection des supplément
                $supplement = $request->request->get("supplement");
                $supplementpers = $request->request->get("supplementpers");
                $margesupplement = $request->request->get("margesupplement");
                $margesupplementpers = $request->request->get("margesupplementpers");

                if (is_array($supplement))
                    foreach ($supplement as $key => $value) {
                        $pricesup = new Pricesupplement();
                       // $pricesup->setHotelsupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->find($key));
                        $toshow = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->findBy(array('hotel' => $hotel,'supplement' => $key));
                        if (!empty($toshow)){
                            $pricesup->setHotelsupplement($toshow[0]);
                        }else{
                            $pricesup->setHotelsupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->find($key));
                        }
                        if (isset($supplementpers[$key])){
                            $pricesup->setPers(1);
                           
                        }
                        else{$pricesup->setPers(0);}
                        $pricesup->setHotelprice($hotelprice);
                        $pricesup->setPrice($value);
                        if (isset($margesupplementpers[$key])){
                        $pricesup->setPersm(1);
                        }else{
                           $pricesup->setPersm(0);  
                        }
                        $pricesup->setMarge($margesupplement[$key]);
                        $em->persist($pricesup);
                        $em->flush();
                    }
                // injection des réduction
                $redenf = $request->request->get("redenf");
                $i = 0;
                if(is_array($redenf))
                    foreach ($redenf as $key => $value) {
                        foreach ($value["val"] as $kx => $vx) {
                            $pricechild = new Pricechild();
                            $pricechild->setHotelprice($hotelprice);
                           // $pricechild->setHotelchild($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->find($kx));
                            $toshow = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->findBy(array('hotel' => $hotel,'child' => $kx));
                            if (!empty($toshow)){
                                $pricechild->setHotelchild($toshow[0]);
                            }else{
                                $pricechild->setHotelchild($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->find($kx));
                            }

                            $pricechild->setType($key);
                            $pricechild->setPrice($vx);
                            if (isset($value["pers"][$kx])){
                                $pricechild->setPers(1);
                            }
                            else{$pricechild->setPers(0);}
                            $em->persist($pricechild);
                            $em->flush();
                        }
                    }

                return $this->redirect($this->generateUrl('btob_hotelprice_homepage', array("hotelid" => $hotelid, 'marcheid' => $marcheid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Hotelprice:form.html.twig', array(
            'form' => $form->createView(),
            "hotel" => $hotel,
            'marcheid' => $marcheid,
            'allroom' => $allroom,
            'allsupplement' => $allsupplement,
            'allarrangement' => $allarrangement,
            'allchild' => $allchild

        ));
    }



    public function editAction($id, $hotelid, $marcheid)
    {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $request = $this->get('request');

        $hotelprice = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelprice')
            ->find($id);
//var_dump($id).die;
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new HotelpriceType(), $hotelprice);
       // Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
        $allpricechild = array();
        $allhotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $hotel));
        $allhotelchild = array();

        $allpricechild = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findBy(array('hotelprice' => $allhotelprice[0]));
        $i = 0;
        foreach ($allpricechild as $onepricechild) {
            if (! in_array($onepricechild->getHotelchild(), $allhotelchild)){
                $allhotelchild[$i] = $onepricechild->getHotelchild();
                $i = $i + 1;
            }
        }
        //if (empty($allhotelchild)){
        //   $allhotelchild = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->findBy(array('hotel' => $hotel));
        //}
//************** added by hamza **************
        if ($form->isValid()) {
            //if ($request->getMethod() == 'POST') {

            $hotelprice->setDmj(new \DateTime());
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Période du " . date_format($hotelprice->getDated(), 'Y-m-d')." au " . date_format($hotelprice->getDated(), 'Y-m-d') ." - Période n° " . $hotelprice->getId()." - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            foreach ($hotelprice->getPricearr() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPriceroom() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPricechild() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPricesupplement() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            // injection de priceroom
            $room = $request->request->get('room');
           // var_dump($room).die;
            foreach ($room as $key => $value) {
                $priceroom = new Priceroom();
                $priceroom->setHotelprice($hotelprice);
                $priceroom->setQte($value);
                $priceroom->setRoom($this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($key));
                $em->persist($priceroom);
                $em->flush();
            }
            // injection des arrangements
            $arrangement = $request->request->get('arrangement');
            $margearrangement = $request->request->get('margearrangement');
            $minstay = $request->request->get('minstay');
            $persarr = $request->request->get('persarr');
            $margepersarr = $request->request->get('margepersarr');
            $actarr = $request->request->get('actarr');
            //dump($persarr); 
            //dump($actarr);
            //exit;
      //      var_dump($arrangement);
      //      continue;
            foreach ($arrangement as $key => $value) {
                
                $pricear = new Pricearr();
                $pricear->setHotelprice($hotelprice);
                $pricear->setHotelarrangement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelarrangement')->find($key));
                if (isset($persarr[$key])){
                    $pricear->setPers(1);
                }
                else{$pricear->setPers(0);}
                
                
                 if (isset($margepersarr[$key])){
                    $pricear->setPersm(1);
                }
                else{$pricear->setPersm(0);}
                
                
                if (isset($actarr[$key])){
                    $pricear->setEtat(1);
                }
                else{$pricear->setEtat(0);}
                $pricear->setMinstay($minstay[$key]);
                
                 $pricear->setMarge($margearrangement[$key]);
                $pricear->setPrice($value);
                $em->persist($pricear);
                $em->flush();
            }

            // injection des supplément
            $supplement = $request->request->get("supplement");
            $supplementpers = $request->request->get("supplementpers");
            $margesupplement = $request->request->get("margesupplement");
            $margesupplementpers = $request->request->get("margesupplementpers");
            if(is_array($supplement))
                foreach ($supplement as $key => $value) {
                    $pricesup = new Pricesupplement();
                    $pricesup->setHotelsupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->find($key));
                    if (isset($supplementpers[$key])){
                        $pricesup->setPers(1);
                    }
                    else{$pricesup->setPers(0);
                    }
                    $pricesup->setHotelprice($hotelprice);
                    $pricesup->setPrice($value);
                    if (isset($margesupplementpers[$key])){
                    $pricesup->setPersm(1);
                    }else{
                      $pricesup->setPersm(0);  
                    }
                    $pricesup->setMarge($margesupplement[$key]);
                    $em->persist($pricesup);
                    $em->flush();
                }
            // injection des réduction
            $redenf = $request->request->get("redenf");
            $i = 0;
			if(!empty($redenf))
			{
            foreach ($redenf as $key => $value) {
                foreach ($value["val"] as $kx => $vx) {

                    $pricechild = new Pricechild();
                    $pricechild->setHotelprice($hotelprice);
                    $pricechild->setHotelchild($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->find($kx));
                    $pricechild->setType($key);
                    $pricechild->setPrice($vx);
                    if (isset($value["pers"][$kx])){
                        $pricechild->setPers(1);
                    }
                    else{$pricechild->setPers(0);}
                    $em->persist($pricechild);
                    $em->flush();
                }
            }
			}
            return $this->redirect($this->generateUrl('btob_hotelprice_homepage', array("hotelid" => $hotelid, 'marcheid' => $marcheid)));
        } else {
            echo $form->getErrors();
        }
   //     var_dump($hotelprice->getPricesupplement());
        return $this->render('BtobHotelBundle:Hotelprice:edit.html.twig', array('form' => $form->createView(), 'price' => $hotelprice, 'id' => $id, "hotel" => $hotel, 'allhotelchild' => $allhotelchild,
                'marcheid' => $marcheid)
        );
    }

    public function duplicateAction($id, $hotelid, $marcheid)
    {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $request = $this->get('request');
        $hotelprice = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelprice')
            ->find($id);
        $hotelprice->setDated(null);
        $hotelprice->setDates(null);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new HotelpriceType(), $hotelprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);

        if ($form->isValid()) {

            $hotelprice->setDmj(new \DateTime());
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Dupliquation: Période du " . date_format($hotelprice->getDated(), 'Y-m-d')." au " . date_format($hotelprice->getDated(), 'Y-m-d')." - Période n° " . $hotelprice->getId()." - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            foreach ($hotelprice->getPricearr() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPriceroom() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPricechild() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            foreach ($hotelprice->getPricesupplement() as $key => $value) {
                $em->remove($value);
                $em->flush();
            }
            // injection de priceroom
            $room = $request->request->get('room');
            foreach ($room as $key => $value) {
                $priceroom = new Priceroom();
                $priceroom->setHotelprice($hotelprice);
                $priceroom->setQte($value);
                $priceroom->setRoom($this->getDoctrine()->getRepository('BtobHotelBundle:Room')->find($key));
                $em->persist($priceroom);
                $em->flush();
            }
            // injection des arrangements
            $arrangement = $request->request->get("arrangement");
             $margearrangement = $request->request->get("margearrangement");
            $minstay = $request->request->get("minstay");
            $persarr = $request->request->get("persarr");
            $margepersarr = $request->request->get("margepersarr");
            foreach ($arrangement as $key => $value) {
                $pricear = new Pricearr();
                $pricear->setHotelprice($hotelprice);
                $pricear->setHotelarrangement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelarrangement')->find($key));
                if (isset($persarr[$key]))
                 $pricear->setPers(true);
                
                
                if (isset($margepersarr[$key]))
                 $pricear->setPersm(true);
                
                $pricear->setMinstay($minstay[$key]);
                $pricear->setMarge($margearrangement[$key]);
                $pricear->setPrice($value);
                $em->persist($pricear);
                $em->flush();
            }
            // injection des supplément
            $supplement = $request->request->get("supplement");
            $supplementpers = $request->request->get("supplementpers");
             $margesupplement = $request->request->get("supplement");
            $margesupplementpers = $request->request->get("supplementpers");
			    if(is_array($supplement))
				{
            foreach ($supplement as $key => $value) {
                $pricesup = new Pricesupplement();
                $pricesup->setHotelsupplement($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelsupplement')->find($key));
                if (isset($supplementpers[$key]))
                    $pricesup->setPers(true);
                $pricesup->setHotelprice($hotelprice);
                $pricesup->setPrice($value);
                if (isset($margesupplement[$key]))
                $pricesup->setMarge($margesupplement[$key]);
                if (isset($margesupplementpers[$key]))
                $pricesup->setPersm($margesupplementpers[$key]);
                $em->persist($pricesup);
                $em->flush();
                      }
			    }
            // injection des réduction
            $redenf = $request->request->get("redenf");
            $i = 0;
			 if(is_array($redenf))
				{
            foreach ($redenf as $key => $value) {
                foreach ($value["val"] as $kx => $vx) {

                    $pricechild = new Pricechild();
                    $pricechild->setHotelprice($hotelprice);
                    $pricechild->setHotelchild($this->getDoctrine()->getRepository('BtobHotelBundle:Hotelchild')->find($kx));
                    $pricechild->setType($key);
                    $pricechild->setPrice($vx);
                    if (isset($value["pers"][$kx]))
                        $pricechild->setPers(true);
                    $em->persist($pricechild);
                    $em->flush();
                }
            }
			}
            return $this->redirect($this->generateUrl('btob_hotelprice_homepage', array("hotelid" => $hotelid, 'marcheid' => $marcheid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Hotelprice:duplicate.html.twig', array('form' => $form->createView(), 'price' => $hotelprice, 'id' => $id, "hotel" => $hotel,
                'marcheid' => $marcheid)
        );
    }

    public function deleteAction(Hotelprice $hotelprice, $hotelid, $marcheid)
    {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $em = $this->getDoctrine()->getManager();

        if (!$hotelprice) {
            throw new NotFoundHttpException("Hotelprice non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Période du " . date_format($hotelprice->getDated(), 'Y-m-d')." au " . date_format($hotelprice->getDated(), 'Y-m-d')." - Période n° " . $hotelprice->getId()." - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($hotelprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_hotelprice_homepage', array("hotelid" => $hotelid, 'marcheid' => $marcheid)));
    }

    public function validdateaddAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {
            $id = $request->request->get("id");
            $dated = explode("/", $request->request->get("dated"));
            $dates = explode("/", $request->request->get("dates"));
            $marcheid = $request->request->get("marcheid");
            $dt = new \DateTime();
            $dt->setDate($dated[2], $dated[1], $dated[0]);
            $dt2 = new \DateTime();
            $dt2->setDate($dates[2], $dates[1], $dates[0]);
            $nbjour = $dt->diff($dt2)->days;
            if($nbjour<1){
                echo "false";
                exit;
            }

            $priceid = $request->request->get("priceid");
            //$marche=$this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
            //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->find($id);
            if (!is_numeric($priceid)) {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->ValidateInterval($id, $dt, $dt2, $marcheid);
            } else {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->ValidateInterval($id, $dt, $dt2, $marcheid, $priceid);
            }
            exit;

        }
        echo "false";
        exit;
    }

}












