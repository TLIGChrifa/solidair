<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Pays;
use Btob\HotelBundle\Form\PaysType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class PaysController extends Controller {

    public function indexAction() {
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Pays')
                ->findAll();
        return $this->render('BtobHotelBundle:Pays:index.html.twig', array('entities' => $pays));
    }

    public function addAction() {
        $pays = new Pays();
        $form = $this->createForm(new PaysType(), $pays);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($pays);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Pays");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_pays_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Pays:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $pays = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Pays')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PaysType(), $pays);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Pays n° " . $pays->getId()." - " . $pays->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_pays_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Pays:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Pays $pays) {
        $em = $this->getDoctrine()->getManager();

        if (!$pays) {
            throw new NotFoundHttpException("Pays non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Pays n° " . $pays->getId()." - " . $pays->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($pays);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_pays_homepage'));
    }

}
