<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Events;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\EventsType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class EventsController extends Controller {

    public function indexAction($hotelid) {
        $hotel = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotel')
                ->find($hotelid);
        return $this->render('BtobHotelBundle:Events:index.html.twig', array('entities' => $hotel->getEvents(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function addAction($hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $Events = new Events();
        $form = $this->createForm(new EventsType(), $Events);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Events->setHotel($hotel);
                $em->persist($Events);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Evénement - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Events:form.html.twig', array('form' => $form->createView(), "hotelid" => $hotelid, "hotel" => $hotel));
    }

    public function editAction($id, $hotelid) {
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
        $request = $this->get('request');
        $Events = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Events')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new EventsType(), $Events);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Evénement " . $Events->getName()." n° " . $Events->getId()." - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid, "hotel" => $hotel)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Events:form.html.twig', array('form' => $form->createView(), 'id' => $id, "hotelid" => $hotelid, "hotel" => $hotel)
        );
    }

    public function deleteAction(Events $Events, $hotelid) {
        $em = $this->getDoctrine()->getManager();
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        if (!$Events) {
            throw new NotFoundHttpException("Events non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Evénement " . $Events->getName()." n° " . $Events->getId()." - ". $hotel->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Events);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_events_homepage', array("hotelid" => $hotelid)));
    }

}
