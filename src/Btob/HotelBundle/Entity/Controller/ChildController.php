<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Child;
use Btob\HotelBundle\Form\ChildType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class ChildController extends Controller {

    public function indexAction() {
        $child = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Child')
                ->findAll();
        return $this->render('BtobHotelBundle:Child:index.html.twig', array('entities' => $child));
    }

    public function addAction() {
        $child = new Child();
        $form = $this->createForm(new ChildType(), $child);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            
            if ($form->isValid()) {
                $minimum = $form["min"]->getData();
                $maximum = $form["max"]->getData();
                
                if ($minimum < $maximum) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($child);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Catégorie enfant");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                    $em->flush();
                    return $this->redirect($this->generateUrl('btob_child_homepage'));
                } else {
                echo $form->getErrors();
                }
            }
             else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Child:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $child = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Child')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ChildType(), $child);
        $form->handleRequest($request);

        if ($form->isValid()) {

                $minimum = $form["min"]->getData();
                $maximum = $form["max"]->getData();
                
                if ($minimum < $maximum) {

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Catégorie enfant n° " . $child->getId()." - " . $child->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                    $em->flush();
                    return $this->redirect($this->generateUrl('btob_child_homepage'));
                } else {
                            echo $form->getErrors();
                        }






            

            
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Child:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Child $child) {
        $em = $this->getDoctrine()->getManager();

        if (!$child) {
            throw new NotFoundHttpException("Child non trouvée");
        }

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Catégorie enfant n° " . $child->getId()." - " . $child->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($child);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_child_homepage'));
    }

}
