<?php

namespace Btob\HotelBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Resacomment;
use Btob\HotelBundle\Entity\Notification;
use User\UserBundle\Entity\Solde;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class ReservationController extends Controller
{
    public function dashAction()
    {
        //echo 'dashmessage';
        $dt = new \DateTime();
        $dtweek = $dt->modify('-7 day');
        $tab = array();
        $tab = array();


        for ($i = 0; $i < 7; ++$i) {
            $dt = new \DateTime();
            $dt->modify("-$i day");
            $tab[$dt->format('Ymd')] = array();
        }
        $dtx = new \DateTime();
        $user = $this->get('security.context')->getToken()->getUser();
        $resaweek = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dtx, $dtweek);
       // if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaweek as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaweek = $tabx;
        //}
        foreach ($resaweek as $value) {
            $tab[$value->getDcr()->format('Ymd')][] = 1;
        }
        ksort($tab);
        $ArrFinal = array();
        $i = 1;
        foreach ($tab as $key => $value) {
            $ArrFinal[strtotime($key)] = count($value);
            ++$i;
        }


        return new JsonResponse($ArrFinal);

    }

    public function indexAction()
    {
		$user = $this->get('security.context')->getToken()->getUser();
        $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($user->getId());
         
    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($user->getId());

    if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
        $tab=array();
        foreach($solde as $value){
            if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                $tab[]=$value;
            }
        }
        $solde=$tab;

    }
    $solde=array_reverse($solde);
        //echo 'indexmessage';
        // filtre de recherch
        $hotelname = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "1000";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $hotelname = trim($request->request->get('hotelname'));
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        //var_dump($notification->getId());
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:ReservationHotel')
			->findAll();
			$resa =  $em->createQueryBuilder()
			->select('r')
			->from('BtobHotelBundle:ReservationHotel','r')
			->where('r.montantpaye IS NOT NULL')
			//->setParameter('reservation', $reservation)
			->getQuery()
			->getResult();
			//var_dump( $resa).die;
        $entities = array();
        foreach ($resa as $value) {
           // if ($value->getDel()) {
                $entities[] = $value;
           // }
        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {
            if ($agence != "") {
                $id = $agence;
            } else {
                $id = $user->getId();
            }
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $id) {
                    $tabuser[] = $value;
                }
            }
            $entities = $tabuser;
        }
        
        
        //filteretat
        if($etat!="1000")
        {
             $tabsearch = array();
             foreach ($entities as $value) {
                if ($value->getEtat() == $etat) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        
        
        
        // test nom d'hôtel
        if ($hotelname != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if ($value->getHotel()->getName() == $hotelname) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        // test client $client
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatedebut()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatefin()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        return $this->render('BtobHotelBundle:Reservation:index.html.twig', array(
            'entities' => $entities,
            'hotelname' => $hotelname,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
			'users' => $users,
			'entitiessolde'=>$solde,
        ));
    }

    public function detailAction(Reservation $reservation)
    {
        $namead = json_decode($reservation->getNamead(), true);
        $agead = json_decode($reservation->getAgeadult(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
        $ageenfant = json_decode($reservation->getAgeenfant(), true);
        $options = json_decode($reservation->getRemarque(), true);
        $frais = $this->getDoctrine()->getRepository('BtobHotelBundle:Frais')->fraisDossier();
        $optionHotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Options')->findAll();
        
        
        return $this->render('BtobHotelBundle:Reservation:detail.html.twig', array(
            'entry' => $reservation,
            'namead' => $namead,
            'frais' => $frais,
            'agead' => $agead,
            'nameenf' => $nameenf,
            'ageenfant' => $ageenfant,
             'options' => $options, 
            'optionHotel' => $optionHotel,
            
            ));
    }

    public function deleteAction(Reservation $reservation)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation->setDel(false);
        $em->persist($reservation);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Réservation hôtel n° " . $reservation->getId()." du " . date_format($reservation->getDated(), 'Y-m-d'). " au " . date_format($reservation->getDatef(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reservationback_homepage'));
    }

    public function ajxaddcommentAction()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $action = $request->request->get("action");
        $user = $this->get('security.context')->getToken()->getUser();
        $reservation = $resa = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($id);
        $comment = new Resacomment();
        $comment->setAction($action);
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        exit;
    }

    public function actiAction(Reservation $reservation, $action)
    {

                $info =$this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());

        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $frais = $this->getDoctrine()->getRepository('BtobHotelBundle:Frais')->fraisDossier();
        $namead = json_decode($reservation->getNamead(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
        $ageenfant = json_decode($reservation->getAgeenfant(), true);
        $agead = json_decode($reservation->getAgeadult(), true);
        $options = json_decode($reservation->getRemarque(), true);
        
      //  $em = $this->getDoctrine()->getManager();
        $reservation->setEtat($action);
        $em->persist($reservation);
        $em->flush();
        // action =2
        $agence=$reservation->getUser()->getUsername();

        $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
        $factures = array();
        $dr = new \DateTime();
        if(count($facts)>0)
        {
            foreach ($facts as $kef)
            {
                if($kef->getDcr()->format("Y")==$dr->format("Y"))
                {
                    array_push($factures,$kef);
                }
            }
        }

        if(count($factures)>0)
        {
            $num = $factures[0]->getNum()+1;
        }else{
            $num =1;
        }
        $comment = new Resacomment();
        $user = $this->get('security.context')->getToken()->getUser();
        if ($action == 2) {
            
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." à l'hôtel " . $reservation->getHotel()->getName()." a été traité");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
                        
            $comment->setAction("Réservation traitée");

            if($agence!='Site Digi-Travel'){
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $info->getNom().": Réservation traitée " ;
                $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 Bonjour Madame/Monsieur,<br>
							 Nous vous remercions pour la confiance renouvelée, votre  Réservation est traitée.
							 <table width="90%"  cellspacing="1" border="0">';
						 
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message1 .= '<td width="20%" height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message1 .= '</tr>';


				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message1 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message1 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message1 .= '' . $adx . ' - ';


					}
					$message1 .= '</td>';


					$message1 .= '</tr>';
					$cc++;
				}
				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message1 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message1 .= '</tr>';
				}
				$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#183961;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $reservation->getUser()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().":Réservation  traitée " ;
                $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>



						 '  . $reservation->getUser()->getName() .',<br>
						 Réservation est traitée .
						 <table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#183961;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);

            }
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject = $info->getNom().": Réservation traitée " ;
            $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
			$message1 .='
						<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>
						  
						 Bonjour Madame/Monsieur,<br>
						 Nous vous remercions pour la confiance renouvelée, votre  Réservation est traitée.
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message1 .= '<td width="20%" height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message1 .= '</tr>';


			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message1 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message1 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message1 .= '' . $adx . ' - ';


				}
				$message1 .= '</td>';


				$message1 .= '</tr>';
				$cc++;
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message1 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message1 .= '</tr>';
			}
			$message1 .= '</table>';

			$message1 .= '<table width="90%"  cellspacing="1" border="0">';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message1 .= '</tr>';

			$message1 .= '</table>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too      = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects =   $info->getNom().": Réservation traitée" ;
            $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
            $message2 .='<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>

						 '  . $reservation->getUser()->getName() .',<br>
						 Réservation est traitée .
						 
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message2 .= '<td width="20%" height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message2 .= '</tr>';


			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message2 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message2 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message2 .= '' . $adx . ' - ';


				}
				$message2 .= '</td>';


				$message2 .= '</tr>';
				$cc++;
			}
			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message2 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message2 .= '</tr>';
			}
			$message2 .= '</table>';
			$message2 .= '<table width="90%"  cellspacing="1" border="0">';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message2 .= '</tr>';

			$message2 .= '</table>';
			$message2 .= '</body>';


            mail($too, $subjects, $message2, $header);

        }elseif ($action == 31) {

           $idmfact  = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findByTypeIdm($reservation->getId());

                //            $reservation->setEtat(3);
               //             $em->flush();

            //decrimentation de stock de chambre
            $dated = $reservation->getDated();
            $hotel = $reservation->getHotel();
            $agence = $reservation->getUser();
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($dated, $hotel->getId(), $agence);
			
			
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." à l'hôtel " . $reservation->getHotel()->getName()." a été payée");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
			//point fid
            $client = $reservation->getClient();

       if($client->getFid())
        {
        $pointclient=  $client->getPoint();   
       $pointg = $this->getDoctrine()->getRepository('BtobHotelBundle:Point')->findbyPrice($reservation->getTotal());                   
        if(isset($pointg))
        {
            $pointgagner =$pointg->getPt();
        }else{
            $pointgagner =0;
            
        }
        $totalpt= $pointclient+ $pointgagner;
        
         $client->setPoint($totalpt);
         $em->persist($client);
         $em->flush();
       }
            
            //fin
			
            foreach ($price as $hotelprice) {

                $roomprice = $hotelprice->getPriceroom();
                foreach ($roomprice as $valroomprice) {
                    foreach ($reservation->getReservationdetail() as $valdetail) {
                        if ($valdetail->getRoomname() == $valroomprice->getRoom()->getName()) {
                            $html = "Le nombre de (" . $valdetail->getRoomname() . ") était " . $valroomprice->getQte() . " et maintenant est ";
                            $qte = $valroomprice->getQte() - 1;

                            if ($qte > 0) {
                                $valroomprice->setQte($qte);
                                $html = $html . $qte;
                            } else {
                                $valroomprice->setQte(0);
                                $html .= "0";
                            }

                            $em->persist($valroomprice);
                            $em->flush();
                            $comm = new Resacomment();
                            $comm->setReservation($reservation);
                            $comm->setUser($user);
                            $comm->setAction($html);
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
                        }
                    }

                }

            }


            // ajout de commentaire
            $comment->setAction("Réservation payée");
            if($agence!='Site Digi-Travel')
            {
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

                $to = $reservation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation payée " ;
                $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 Bonjour Madame/Monsieur,<br>
							 Nous vous remercions pour la confiance renouvelée, votre  Réservation est payée.
							 <table width="90%"  cellspacing="1" border="0">';
						 
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message1 .= '<td width="20%" height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message1 .= '</tr>';


				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message1 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message1 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message1 .= '' . $adx . ' - ';


					}
					$message1 .= '</td>';


					$message1 .= '</tr>';
					$cc++;
				}
				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message1 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message1 .= '</tr>';
				}
				$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';
                
                $message1 .= '</table>';
                $message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $reservation->getUser()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation  payée " ;
                $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';
                $message2 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 '  . $reservation->getUser()->getName() .',<br>
							 Réservation est payée .
							 
							<table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);

            }
            // envoie d'email de confirmation de paiement
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $info->getNom().": Réservation payée " ;
            $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>
						  
						 Bonjour Madame/Monsieur,<br>
						 Nous vous remercions pour la confiance renouvelée, votre  Réservation est payée.
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message1 .= '<td width="20%" height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message1 .= '</tr>';


			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message1 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message1 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message1 .= '' . $adx . ' - ';


				}
				$message1 .= '</td>';


				$message1 .= '</tr>';
				$cc++;
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message1 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message1 .= '</tr>';
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message1 .= '</tr>';

			$message1 .= '</table>';
			$message1 .= '</body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too      = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects =   $info->getNom().": Réservation  facturée " ;
            $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
            $message2 .='<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>
						  
						 '  . $reservation->getUser()->getName() .',<br>
						 Réservation est facturée .
						 
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message2 .= '<td width="20%" height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message2 .= '</tr>';


			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message2 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message2 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message2 .= '' . $adx . ' - ';


				}
				$message2 .= '</td>';


				$message2 .= '</tr>';
				$cc++;
			}
			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message2 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message2 .= '</tr>';
			}
			$message2 .= '</table>';

			$message2 .= '<table width="90%"  cellspacing="1" border="0">';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message2 .= '</tr>';

			$message2 .= '</table>';
			$message2 .= '</body>';


            mail($too, $subjects, $message2, $header);
            // déminuer le solde de l'agence
            $solde = new Solde();
            $solde->setObservation("Paiement de la commande " . Tools::RefResa($reservation->getRef(), $reservation->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($reservation->getUser());
            $solde->setSolde(($reservation->getTotal() * (-1)));
            $em->persist($solde);
            $newsolde = $reservation->getUser()->getSold() - $reservation->getTotal();
            $currenuser = $reservation->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);

        }
        elseif ($action == 3) {

           $idmfact  = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findByTypeIdm($reservation->getId());

            if(count($idmfact)==0||$reservation->getEtat()==3)
            {
//exit(var_dump($idmfact));
        $em = $this->getDoctrine()->getManager();
                $ffact = new Facture();
                $ffact->setNum($num);
                $ffact->setCode("FO");
                $ffact->setEtat(1);
                $ffact->setClient($reservation->getClient()->getName().' '.$reservation->getClient()->getPname());
                $ffact->setClientId($reservation->getClient());
                $ffact->setDcr($dr);
                $ffact->setDate(new \DateTime());
                $ffact->setIdm($reservation->getId());
                $ffact->setType("Hotel");
                $ffact->setMontant($reservation->getTotal());
                $em->persist($ffact);
                $em->flush();
                foreach ($reservation->getReservationdetail() as $ketf => $valtf) {

        $em = $this->getDoctrine()->getManager();
                    $tfac = new Tfacture();
                    $tfac->setFacture($ffact);
                    $tfac->setPrixUnitaire($reservation->getTotal());
                    $tfac->setQuantite(1);
                    $tfac->setRemise(0);
                    $tfac->setTva(20);
                    //$tfac->setMontant($valtf->getPrice());
                    $tfac->setType($reservation->getHotel()->getName().":".$valtf->getRoomname().":".$valtf->getArrangement()." de " .$reservation->getDated()->format('d-m-Y')." au ".$reservation->getDatef()->format('d-m-Y'));
                    $em->persist($tfac);
                    $em->flush();

                }
            }
            
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." à l'hôtel " . $reservation->getHotel()->getName()." a été facturé");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
            //decrimentation de stock de chambre
            $dated = $reservation->getDated();
            $hotel = $reservation->getHotel();
            $agence = $reservation->getUser();
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($dated, $hotel->getId(), $agence);
			
			//point fid
            $client = $reservation->getClient();

       if($client->getFid())
        {
        $pointclient=  $client->getPoint();   
       $pointg = $this->getDoctrine()->getRepository('BtobHotelBundle:Point')->findbyPrice($reservation->getTotal());                   
        if(isset($pointg))
        {
            $pointgagner =$pointg->getPt();
        }else{
            $pointgagner =0;
            
        }
        $totalpt= $pointclient+ $pointgagner;
        
         $client->setPoint($totalpt);
         $em->persist($client);
         $em->flush();
       }
            
            //fin
			
            foreach ($price as $hotelprice) {

                $roomprice = $hotelprice->getPriceroom();
                foreach ($roomprice as $valroomprice) {
                    foreach ($reservation->getReservationdetail() as $valdetail) {
                        if ($valdetail->getRoomname() == $valroomprice->getRoom()->getName()) {
                            $html = "Le nombre de (" . $valdetail->getRoomname() . ") était " . $valroomprice->getQte() . " et maintenant est ";
                            $qte = $valroomprice->getQte() - 1;

                            if ($qte > 0) {
                                $valroomprice->setQte($qte);
                                $html = $html . $qte;
                            } else {
                                $valroomprice->setQte(0);
                                $html .= "0";
                            }

                            $em->persist($valroomprice);
                            $em->flush();
                            $comm = new Resacomment();
                            $comm->setReservation($reservation);
                            $comm->setUser($user);
                            $comm->setAction($html);
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
                        }
                    }

                }

            }


            // ajout de commentaire
            $comment->setAction("Réservation facturée");
            if($agence!='Site Digi-Travel')
            {
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $reservation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation facturée " ;
                $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 Bonjour Madame/Monsieur,<br>
							 Nous vous remercions pour la confiance renouvelée, votre  Réservation est facturée.
							 <table width="90%"  cellspacing="1" border="0">';
						 
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message1 .= '<td width="20%" height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message1 .= '</tr>';


				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message1 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message1 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message1 .= '' . $adx . ' - ';


					}
					$message1 .= '</td>';


					$message1 .= '</tr>';
					$cc++;
				}
				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message1 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message1 .= '</tr>';
				}
				$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $reservation->getUser()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation  facturée " ;
                $header = "From: Explore Voyage<" .$reservation->getUser()->getEmail(). ">\n";
                $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';
                $message2 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 Bonjour,<br>
							 Réservation est facturée .
							 
						 <table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);

            }
            // envoie d'email de confirmation de paiement
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $info->getNom().": Réservation facturée " ;
            $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>
						  
						 Bonjour Madame/Monsieur,<br>
						 Nous vous remercions pour la confiance renouvelée, votre  Réservation est facturée.
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message1 .= '<td width="20%" height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message1 .= '</tr>';


			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message1 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message1 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message1 .= '' . $adx . ' - ';


				}
				$message1 .= '</td>';


				$message1 .= '</tr>';
				$cc++;
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message1 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message1 .= '</tr>';
			}
			$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too      = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects =   $info->getNom().": Réservation  Payeé " ;
            $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
            $message2 .='<table width="90%"  cellspacing="1" border="0">
						   <tr>
							<td><img src="'.$img_logo.'" /><br></td>
						   </tr>
						  </table><br>
						  
						 '  . $reservation->getUser()->getName() .',<br>
						 Réservation est Payée .
						 <table width="90%"  cellspacing="1" border="0">';
						 
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message2 .= '<td width="20%" height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message2 .= '</tr>';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message2 .= '</tr>';

			$message2 .= '<tr>';
			$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message2 .= '<td height="30"><b>:</b></td>';
			$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message2 .= '</tr>';


			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message2 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message2 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message2 .= '' . $adx . ' - ';


				}
				$message2 .= '</td>';


				$message2 .= '</tr>';
				$cc++;
			}
			$message2 .= '</table>';


			$message2 .= '<table width="90%"  cellspacing="1" border="0">';

			$message2 .= '<tr>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message2 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message2 .= '<tr>';
				$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message2 .= '</tr>';
			}
			$message2 .= '</table>';

			$message2 .= '<table width="90%"  cellspacing="1" border="0">';
			$message2 .= '<tr>';
			$message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message2 .= '</tr>';

			$message2 .= '</table>';
			$message2 .= '</body>';


            mail($too, $subjects, $message2, $header);
            // déminuer le solde de l'agence
            $solde = new Solde();
            $solde->setObservation("Paiement de la commande " . Tools::RefResa($reservation->getRef(), $reservation->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($reservation->getUser());
            $solde->setSolde(($reservation->getTotal() * (-1)));
            $em->persist($solde);
            $newsolde = $reservation->getUser()->getSold() - $reservation->getTotal();
            $currenuser = $reservation->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);

        } elseif ($action == 0) {
            
           $idmfact  = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findByTypeIdm($reservation->getId());

            if(count($idmfact)==1)
            {
                $em = $this->getDoctrine()->getManager();
                $ffact = new Facture();
                $ffact->setNum($num);
                $ffact->setCode("FO");
                $ffact->setEtat(1);
                $ffact->setClient($reservation->getClient()->getName().' '.$reservation->getClient()->getPname());
                $ffact->setClientId($reservation->getClient());
                $ffact->setDcr($dr);
                $ffact->setDate(new \DateTime());
                $ffact->setIdm($reservation->getId());
                $ffact->setType("Hotel");
                $ffact->setMontant((-1) * $reservation->getTotal());
                $em->persist($ffact);
                $em->flush();
            }
            
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." à l'hôtel " . $reservation->getHotel()->getName()." a été annulée");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
            $comment->setAction("Réservation annulée");
            if($agence!='Site Digi-Travel'){
                setlocale (LC_TIME, 'fr_FR','fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));

                $to = $reservation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $info->getNom().": Réservation Annulée " ;
                $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
							 Bonjour Madame/Monsieur,<br>
							 Nous vous remercions pour la confiance renouvelée, votre  Réservation est annulée.
							 <table width="90%"  cellspacing="1" border="0">';
						 
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message1 .= '<td width="20%" height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message1 .= '</tr>';
				$message1 .= '<tr>';
				$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message1 .= '</tr>';

				$message1 .= '<tr>';
				$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message1 .= '<td height="30"><b>:</b></td>';
				$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message1 .= '</tr>';


				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message1 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message1 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message1 .= '' . $adx . ' - ';


					}
					$message1 .= '</td>';


					$message1 .= '</tr>';
					$cc++;
				}
				$message1 .= '</table>';


				$message1 .= '<table width="90%"  cellspacing="1" border="0">';

				$message1 .= '<tr>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message1 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message1 .= '<tr>';
					$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message1 .= '</tr>';
				}
				$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $reservation->getUser()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   $info->getNom().": Réservation  traité " ;
                $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
                $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';    
                $message2 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
				'  . $reservation->getUser()->getName() .',<br>
				 Réservation est Annulée .
						 <table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header);

            }
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $info->getNom().": Réservation Annulée... " ;
            $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
				 Bonjour Madame/Monsieur,<br>
				 Nous vous remercions pour la confiance renouvelée, votre  Réservation est annulée.<table width="90%"  cellspacing="1" border="0">';
						 
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message1 .= '<td width="20%" height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message1 .= '</tr>';


			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message1 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message1 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message1 .= '' . $adx . ' - ';


				}
				$message1 .= '</td>';


				$message1 .= '</tr>';
				$cc++;
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message1 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message1 .= '</tr>';
			}
			$message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too      = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects =   $info->getNom().": Réservation  Annulée " ;
            $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
            $message2 .='
                            <table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>
							  
				'  . $reservation->getUser()->getName() .',<br>
				 Réservation est Annulée .
						 <table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


            mail($too, $subjects, $message2, $header);
        } elseif ($action == 6) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." à l'hôtel " . $reservation->getHotel()->getName()." a été demandée d'annulation");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
            $comment->setAction("Demande d'annulation");

        } elseif ($action == 1) {
            
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("La réservation numero ".$reservation->getId()." a l'hôtel " . $reservation->getHotel()->getName()." a été confirmée");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
            $comment->setAction('Réservation confirmée');
            setlocale (LC_TIME, 'fr_FR','fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject =  $info->getNom().": Réservation Confirmée " ;
            $headers = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $message1 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>

				' .'
				 Bonjour,<br /><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous confirmons votre réservation.<table width="90%"  cellspacing="1" border="0">';
						 
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
			$message1 .= '<td width="20%" height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getId() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
			$message1 .= '</tr>';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
			$message1 .= '</tr>';

			$message1 .= '<tr>';
			$message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
			$message1 .= '<td height="30"><b>:</b></td>';
			$message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
			$message1 .= '</tr>';


			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
			$message1 .= '</tr>';
			
			$cc = 1;
			foreach ($namead as $keyn => $valn) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
				$message1 .= '<td height="30">';
				foreach ($valn as $adv => $adx) {
					$message1 .= '' . $adx . ' - ';


				}
				$message1 .= '</td>';


				$message1 .= '</tr>';
				$cc++;
			}
			$message1 .= '</table>';


			$message1 .= '<table width="90%"  cellspacing="1" border="0">';

			$message1 .= '<tr>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
			$message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
			$message1 .= '</tr>';
			
			foreach ($reservation->getReservationdetail() as $keyy => $vals) {
				$message1 .= '<tr>';
				$message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
				$message1 .= '<td height="30">' . $vals->getAd() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
				$message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
				$message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
				$message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
				$message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
				$message1 .= '</tr>';
			}
			$message1 .= '</table>';

			$message1 .= '<table width="90%"  cellspacing="1" border="0">';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message1 .= '</tr>';

			$message1 .= '</table>';
			$message1 .= '</body>';

            mail($to, $subject, $message1, $headers);

            $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

            $too      = $admin->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subjects =   $info->getNom().": Réservation  Confirmée " ;
            $header = "From:".$info->getNom()."<" .$reservation->getUser()->getEmail(). ">\n";
            $header .= "Reply-To:" .$reservation->getUser()->getName()." " .$admin->getEmail(). "\n";
            $header .= "MIME-Version: 1.0\n";
            $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message2 = "--$mime_boundary\n";
            $message2 .= "Content-Type: text/html; charset=UTF-8\n";
            $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message2 .= "<html>\n";
            $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';
            $message2 .='<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							    <td align="right"><img src="http://www.afritours.com.tn/front/images/tel-mail.png" /><br></td>
							   </tr>
							  </table><br>

				 '  . $reservation->getUser()->getName() .',<br>
				 Réservation est Confirmée .
						 <table width="90%"  cellspacing="1" border="0">';
						 
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
				$message2 .= '<td width="20%" height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getId() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $frais->getPrix()+$reservation->getTotal() . ' DT</td>';
				$message2 .= '</tr>';
				$message2 .= '<tr>';
				$message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
				$message2 .= '</tr>';

				$message2 .= '<tr>';
				$message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
				$message2 .= '<td height="30"><b>:</b></td>';
				$message2 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
				$message2 .= '</tr>';


				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
				$message2 .= '</tr>';
				
				$cc = 1;
				foreach ($namead as $keyn => $valn) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
					$message2 .= '<td height="30">';
					foreach ($valn as $adv => $adx) {
						$message2 .= '' . $adx . ' - ';


					}
					$message2 .= '</td>';


					$message2 .= '</tr>';
					$cc++;
				}
				$message2 .= '</table>';


				$message2 .= '<table width="90%"  cellspacing="1" border="0">';

				$message2 .= '<tr>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
				$message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
				$message2 .= '</tr>';
				
				foreach ($reservation->getReservationdetail() as $keyy => $vals) {
					$message2 .= '<tr>';
					$message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
					$message2 .= '<td height="30">' . $vals->getAd() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
					$message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
					$message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
					$message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
					$message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
					$message2 .= '</tr>';
				}
				$message2 .= '</table>';

			$message1 .= '<table width="90%"  cellspacing="1" border="0">';
			$message1 .= '<tr>';
			$message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
			</td>';
			$message1 .= '</tr>';

			$message1 .= '</table>';
			$message1 .= '</body>';

            mail($too, $subjects, $message2, $header);
        }
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        // notification
        $notif = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findAll();
        $tst = false;
        $not = null;
        foreach ($notif as $value) {
            if ($reservation->getUser()->getId() == $value->getUser()->getId()) {
                $not = $value;
                $tst = true;
            }
        }
        if ($tst) {
            $note = $not->getNotif() + 1;
            $not->setNotif($note);
        } else {
            $not = new Notification();
            $not->setNotif(1);
            $not->setUser($reservation->getUser());
        }
        $em->persist($not);
        $em->flush();

        return $this->redirect($this->generateUrl('btob_reservationback_detail_homepage', array('id' => $reservation->getId())));
    }

    public function voucherAction(Reservation $reservation)
    {
        // Commentaire
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $comment = new Resacomment();
        $comment->setAction("Impression de Voucher");
        $comment->setUser($user);
        $comment->setReservation($reservation);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        $agead = json_decode($reservation->getAgeadult(), true);
        // fin commentaire
        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
        $namead = json_decode($reservation->getNamead(), true);
        $nameenf = json_decode($reservation->getNameenf(), true);
		$ageenfant = json_decode($reservation->getAgeenfant(), true);
		//dump($ageenfant).die;
        $pagefooter = "";
        
		$dataimg = $reservation->getHotel()->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
					
        // echo $recaphtml;exit;
        //echo $reservation->getRecap();exit;
        $html = $this->renderView('BtobHotelBundle:Reservation:pdf.html.twig', array(
            'reservation' => $reservation,
            'agead' => $agead,
            'namead' => $namead,
            'nameenf' => $nameenf,
            'pagefooter' => $pagefooter,
            'ageenfant' => $ageenfant,
            'user' => $user,
            'img' => $img,
			
        ));


        $pdf->Image($this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png'), 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image($this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png'), 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }

    public function ajxrefAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $ref = $request->request->get("ref");
        $resa = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($id);
        $resa->setRef($ref);
        $em->persist($resa);
        $em->flush();
        exit;
    }
    
    
     public function statsAction()
    {
        //echo 'indexmessage';
        // filtre de recherch
        $hotelname = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $hotelname = trim($request->request->get('hotelname'));
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        //var_dump($notification->getId());
        $resa = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findEtat();
        $entities = array();
        foreach ($resa as $value) {
            if ($value->getDel()) {
                $entities[] = $value;
            }
        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {
            if ($agence != "") {
                $id = $agence;
            } else {
                $id = $user->getId();
            }
            $tabuser = array();
            foreach ($entities as $value) {
                if ($value->getUser()->getId() == $id) {
                    $tabuser[] = $value;
                }
            }
            $entities = $tabuser;
        }
        // test nom d'hôtel
        if ($hotelname != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if ($value->getHotel()->getName() == $hotelname) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        // test client $client
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDated()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatef()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        return $this->render('BtobHotelBundle:Reservation:stats.html.twig', array(
            'entities' => $entities,
            'hotelname' => $hotelname,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));
    }
    
    public function mailsAction(Reservation $reservation)
    {
        $request = $this->get('request');
        $frais = $this->getDoctrine()->getRepository('BtobHotelBundle:Frais')->fraisDossier();

  //      if ($request->getMethod() == 'POST') {

            setlocale(LC_TIME, 'fr_FR', 'fra');
            date_default_timezone_set("Europe/Paris");
            mb_internal_encoding("UTF-8");
            $daymonthyear = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%d %B %Y ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));
            $dayonly = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%A ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));


            $namead = json_decode($reservation->getNamead(), true);
            $nameenf = json_decode($reservation->getNameenf(), true);
            $ageenfant = json_decode($reservation->getAgeenfant(), true);
            $agead = json_decode($reservation->getAgeadult(), true);
            $options = json_decode($reservation->getRemarque(), true);

            $to = $reservation->getClient()->getEmail();
            $mime_boundary = "----MSA Shipping----" . md5(time());
            $subject = $reservation->getUser()->getName() . ": Réservation Hôtel";
            $headers = "From:" . $reservation->getUser()->getName() . "<" . $reservation->getUser()->getEmail() . ">\n";
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
            $message1 = "--$mime_boundary\n";
            $message1 .= "Content-Type: text/html; charset=UTF-8\n";
            $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
            $message1 .= "<html>\n";
            $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
            $logo=$this->get('templating.helper.assets')->getUrl('front/images/logo.png');
            $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$logo.'" /><br></td>
							   </tr>
							  </table>';
            $message1 .= '
				<br /> <b>Bonjour,</b><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br>
				 <table width="90%"  cellspacing="1" border="0">';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#fa6202"><b style="color:#fff; padding-left:5px;"> Votre commande :</b></td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getUser()->getName() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
            $message1 .= '<td width="20%" height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getId() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getHotel()->getName() . ' ' . $reservation->getHotel()->getVille()->getName() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getDated()->format('Y-m-d H:i:s') . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getDatef()->format('Y-m-d H:i:s') . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Prix</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getTotal() . ' DT</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Frais de dossier</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $frais->getPrix() . ' DT</td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">total</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $frais->getPrix() + $reservation->getTotal() . ' DT</td>';
            $message1 .= '</tr>';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" colspan="3" bgcolor="#fa6202"><b style="color:#fff; padding-left:5px;"> Vos coordonnées :</b></td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getCiv() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getPname() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getName() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getEmail() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getTel() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getCin() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getAdresse() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getCp() . '</td>';
            $message1 .= '</tr>';

            $message1 .= '<tr>';
            $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
            $message1 .= '<td height="30"><b>:</b></td>';
            $message1 .= '<td height="30">' . $reservation->getClient()->getPays()->getName() . '</td>';
            $message1 .= '</tr>';


            $message1 .= '</table>';

            $message1 .= '<table cellpadding="5" width="90%">';
            $message1 .= ' <tr bgcolor="#eaeaea">';
            $message1 .= '  <td>';
            $rrm = 1;

            if (count($namead) > 0) {
                foreach ($namead as $kk => $valnad) {
                    $message1 .= '<table class="table table-bordered" width="100%">';
                    $message1 .= '<thead>';
                    $message1 .= '<tr>';
                    $message1 .= '<th width="25%">Chambres' . $rrm . '</th>';
                    $message1 .= '<th width="50%">Noms des occupants</th>';
                    $message1 .= '<th width="25%">Age</th>';
                    $message1 .= '</tr>';
                    $message1 .= '</thead>';
                    $message1 .= '<tbody>';
                    foreach ($valnad as $kvyn => $vvalnad) {

                        $message1 .= '<tr>';
                        $message1 .= '<td>Adulte</td>';
                        $message1 .= '<td>' . $vvalnad . '</td>';
                        $message1 .= '<td>';
                        if ($agead[$kk][$kvyn]) {
                            $agead[$kk][$kvyn];
                        }
                        $message1 .= '</td>';
                        $message1 .= '</tr>';
                    }
                    if (isset($ageenfant[$kk])) {

                        $message1 .= '<tr>';
                        $message1 .= '<td>Enfant</td>';
                        for ($vvr = 0; $vvr < count($ageenfant[$kk]); $vvr++) {
                            $message1 .= '<td>' . $nameenf[$kk][$vvr] . '</td>';
                            $message1 .= '<td>' . $ageenfant[$kk][$vvr] . '</td>';
                        }
                        $message1 .= '</tr>';

                    }
                    $message1 .= '</tbody>';
                    $message1 .= '</table>';
                    $rrm = $rrm + 1;
                }
            }
            $message1 .= '</td>';
            $message1 .= '</tr>';
            $message1 .= '</table>';

            $message1 .= '<table cellpadding="5">';
            $message1 .= '<tr bgcolor="#eaeaea">';
            $message1 .= '<td>';
            if (count($nameenf) > 0) {
                foreach ($nameenf as $kkf => $valnenf) {
                    if (isset($namead[$kkf])) {

                    } else {
                        $message1 .= '<table class="table table-bordered">';
                        $message1 .= '<thead>';
                        $message1 .= '<tr>';
                        $message1 .= '<th>Chambres' . $rrm . '</th>';
                        $message1 .= '<th>Noms des occupants</th>';
                        $message1 .= '<th>Age</th>';
                        $message1 .= '</tr>';
                        $message1 .= '</thead>';
                        $message1 .= '<tbody>';
                        if (isset($ageenfant[$kkf])) {

                            for ($v = 0; $v < count($ageenfant[$kkf]); $v++) {

                                $message1 .= '<tr>';
                                $message1 .= '<td>Enfant</td>';
                                $message1 .= '<td>' . $nameenf[$kkf][$v] . '</td>';
                                $message1 .= '<td>' . $ageenfant[$kkf][$v] . '</td>';
                                $message1 .= '</tr>';
                            }

                        }
                        $message1 .= '</tbody>';
                        $message1 .= '</table>';
                        $rrm = $rrm + 1;
                    }
                }
            }
            $message1 .= '</td>';
            $message1 .= '</tr>';
            $message1 .= '</table>';


            $message1 .= '<table width="90%"  cellspacing="1" border="0">';

            $message1 .= '<tr>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Chambres</b></td>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Adulte(s)</b></td>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Enfant(s)</b></td>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Arrangements</b></td>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Suppléménts</b></td>';
            $message1 .= '<td height="30" bgcolor="#fa6202"><b style="padding-left:10px; color:#fff;">Evenement</b></td>';

            $message1 .= '</tr>';

            foreach ($reservation->getReservationdetail() as $keyy => $vals) {
                $message1 .= '<tr>';
                $message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
                $message1 .= '<td height="30">' . $vals->getAd() . '</td>';
                $message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
                $message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
                $message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
                $message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
                $message1 .= '</tr>';
            }
            $message1 .= '</table>';

            $message1 .= '<table width="90%"  cellspacing="1" border="0">';
            $message1 .= '<tr>';
            $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#fa6202" style="color:#fff;">
                            Explore Voyage, Résidence LATINA, rue des Lacs de MAZURIE, appartement n°B1, Les Berges de Lac - <a style="color:#fff;"> Tél: +216 71 862 263</a>
				</td>';
            $message1 .= '</tr>';

            $message1 .= '</table>';
            $message1 .= '</body><br>';

            mail($to, $subject, $message1, $headers);

            return $this->redirect($this->generateUrl('btob_reservationback_detail_homepage', array('id' => $reservation->getId())));
    //    }

        /**
         * return $this->render('BtobHotelBundle:Reservation:detail.html.twig', array(
         * 'entry' => $reservation,
         * 'namead' => $namead,
         * 'frais' => $frais,
         * 'agead' => $agead,
         * 'nameenf' => $nameenf,
         * 'ageenfant' => $ageenfant,
         * 'options' => $options,
         * 'optionHotel' => $optionHotel,
         *
         * ));
         **/
    }

    
    
}
