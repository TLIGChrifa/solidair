<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Reservationmaisondhote;
use Btob\HotelBundle\Form\ReservationmaisondhotepayerType;
use Btob\HotelBundle\Form\ReservationmaisondhoteType;

/**
 * Reservationmaisondhote controller.
 *
 */
class ReservationmaisondhoteController extends Controller
{

    /**
     * Lists all Reservationmaisondhote entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->findAll();

        return $this->render('BtobHotelBundle:Reservationmaisondhote:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Reservationmaisondhote entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reservationmaisondhote();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
//var_dump('tt').die;
            $em->persist($entity);
            $em->flush();
           $total = 0;
           $prix_nuit = $entity->getMaisondhote()->getPrix();
           $nbnuits =  $entity->getNbnuits();
           $total = $prix_nuit * $nbnuits ;
           $entity->setTotal($total);
           $em->flush();
           return $this->redirect($this->generateUrl('reservationmaisondhote_payer', array("id" => $entity->getId())));

         
        }

        return $this->render('BtobHotelBundle:Reservationmaisondhote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Reservationmaisondhote entity.
     *
     * @param Reservationmaisondhote $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Reservationmaisondhote $entity)
    {
        $form = $this->createForm(new ReservationmaisondhoteType(), $entity, array(
            'action' => $this->generateUrl('reservationmaisondhote_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }


    public function payerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservationmaisondhote')
            ->find($id);
          
            $form = $this->createForm(new ReservationmaisondhotepayerType(), $reservation);
            $img = $em->getRepository('BtobHotelBundle:Maisonhoteimg')->findBy(array('maisondhote' => $reservation->getMaisondhote()));
           //  var_dump($img).die;
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('reservationmaisondhote'));

            }
            $deleteForm = $this->createDeleteForm($id);

            return $this->render('BtobHotelBundle:Reservationmaisondhote:payer.html.twig', array(
                'reservation' => $reservation,
                'img' => $img,
                'form'   => $form->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }

    /**
     * Displays a form to create a new Reservationmaisondhote entity.
     *
     */
    public function newAction()
    {
        $entity = new Reservationmaisondhote();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobHotelBundle:Reservationmaisondhote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            
        ));
    }

    /**
     * Finds and displays a Reservationmaisondhote entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationmaisondhote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Reservationmaisondhote:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Reservationmaisondhote entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationmaisondhote entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Reservationmaisondhote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Reservationmaisondhote entity.
    *
    * @param Reservationmaisondhote $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reservationmaisondhote $entity)
    {
        $form = $this->createForm(new ReservationmaisondhoteType(), $entity, array(
            'action' => $this->generateUrl('reservationmaisondhote_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Reservationmaisondhote entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Reservationmaisondhote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('reservationmaisondhote_edit', array('id' => $id)));
        }

        return $this->render('BtobHotelBundle:Reservationmaisondhote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reservationmaisondhote entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Reservationmaisondhote entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('reservationmaisondhote'));
    }

    /**
     * Creates a form to delete a Reservationmaisondhote entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservationmaisondhote_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function generervoucherAction($id){

        $em = $this->getDoctrine()->getManager();
      
       $reservation = $this->getDoctrine()
           ->getRepository('BtobHotelBundle:Reservationmaisondhote')
           ->find($id);

        // var_dump($chambre).die;
           $pdf = $this->get('white_october.tcpdf')->create();
           // set document information
           $pdf->SetCreator(PDF_CREATOR);
           $pdf->SetAuthor('');
           $pdf->SetTitle('');
           $pdf->SetSubject('');
           $pdf->SetKeywords('');
   
           // remove default header/footer
           $pdf->setPrintHeader(false);
           $pdf->setPrintFooter(false);
   
           // set default monospaced font
           $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
   
           // set margins
           $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
   
           // set auto page breaks
           $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
   
           $pdf->SetFont('helvetica', '', 10, '', true);
   
           $pdf->AddPage();
           
           $html = $this->renderView('BtobHotelBundle:Reservationmaisondhote:pdfvoucher.html.twig', array('reservation' => $reservation));
           $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
           $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           $pdf->writeHTML($html);
           $pdf->Image("images/footerrustica.PNG", 0, 246.56, 210, '', 'PNG', '', 'T', false, 500, '', false, false, 0, false, false, false);

          // $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
         //  $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           //$pdf->Output('/pnv.pdf', 'F');
           $nompdf = 'voucher_.pdf';
           $pdf->Output($nompdf);
           
           return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
           exit;
   }
     
   public function deletereservationAction($id)
   {
       $em = $this->getDoctrine()->getManager();
       $reservation=$this->getDoctrine()
           ->getRepository('BtobHotelBundle:Reservationmaisondhote')
           ->find($id);

       $em->remove($reservation);
       $em->flush();
       $this->addFlash("success", "La réservation à été supprimée avec succès ..");

       return $this->redirect($this->generateUrl('reservationmaisondhote'));
   }
}
