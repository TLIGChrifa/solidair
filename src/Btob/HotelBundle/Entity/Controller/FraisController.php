<?php

namespace Btob\HotelBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Frais;
use Btob\HotelBundle\Form\FraisType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class FraisController extends Controller
{
    /**
     * Lists all Frais entities.
     *
     */

    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobHotelBundle:Frais")->findAll();
        return $this->render('BtobHotelBundle:Frais:index.html.twig', array('entities' => $entities));
    }

    /**
     * Creates a new Frais entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Frais();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->get('request');
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter une frais ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('frais'));
        }

        return $this->render('BtobHotelBundle:Frais:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Frais entity.
     *
     * @param Frais $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Frais $entity)
    {
        $form = $this->createForm(new FraisType(), $entity, array(
            'action' => $this->generateUrl('frais_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new Frais entity.
     *
     */
    public function newAction()
    {
        $entity = new Frais();
        $form = $this->createCreateForm($entity);

        return $this->render('BtobHotelBundle:Frais:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Frais entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Frais')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Frais entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Frais:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Frais entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Frais')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Frais entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('BtobHotelBundle:Frais:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Frais entity.
     *
     * @param Frais $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Frais $entity)
    {
        $form = $this->createForm(new FraisType(), $entity, array(
            'action' => $this->generateUrl('frais_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Edits an existing Frais entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Frais')->find($id);
       
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Frais entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier une frais n� " . $entity->getId());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            
            return $this->redirect($this->generateUrl('frais'));
        }

        return $this->render('BtobHotelBundle:Frais:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));
    }


}
