<?php



namespace Btob\HotelBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Fact;

use Btob\HotelBundle\Form\FactType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;



class FactController extends Controller

{

    public function indexAction()

    {

        $Fact = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Fact')

            ->findAll();

        return $this->render('BtobHotelBundle:Fact:index.html.twig', array('entities' => $Fact));

    }





    public function editAction($id)

    {

        $request = $this->get('request');

        $Fact = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Fact')

            ->find($id);



        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new FactType(), $Fact);

        $form->handleRequest($request);



        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Modification: Paramétre de facture numero " . $Fact->getId()." : " . $Fact->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

            $em->flush();



            return $this->redirect($this->generateUrl('btob_fact_homepage'));

        } else {

            echo $form->getErrors();

        }

        return $this->render('BtobHotelBundle:Fact:form.html.twig', array('form' => $form->createView(), 'id' => $id,)

        );

    }





}

