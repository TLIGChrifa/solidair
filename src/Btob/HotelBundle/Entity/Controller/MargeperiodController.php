<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Promotion;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\PromotionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Margeperiod;
use Btob\HotelBundle\Entity\MargeperiodRepository;
use Btob\HotelBundle\Form\MargeperiodType;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Entity\HotelmargeRepository;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class MargeperiodController extends Controller {


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function indexAction()
    {
        $entities = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Margeperiod')

            ->findAll();
        return $this->render('BtobHotelBundle:Margeperiod:index.html.twig', array(
            'entities' => $entities

        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction()

    {

        $margeperiod = new Margeperiod();

        $form = $this->createForm(new MargeperiodType(), $margeperiod);

        $request = $this->get('request');





        if ($request->getMethod() == 'POST') {



            $form->bind($request);





            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                $em->persist($margeperiod);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter une marge du peride ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

                $em->flush();



                $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();

                $entitiesagence = $em->getRepository('UserUserBundle:User')->findRole();
                $entitiessales = $em->getRepository('UserUserBundle:User')->findRoles();
                $entitiessite = $em->getRepository('UserUserBundle:User')->findRolem();


                $entities = array_merge($entitiesagence, $entitiessales,$entitiessite);

                foreach($entities as $values) {
                    foreach ($hotel as $value) {


                        $hotelmarge = new Hotelmarge();

                        $hotelmarge->setUser($values);
                        $hotelmarge->setMargeperiod($margeperiod);

                        $hotelmarge->setHotel($value);

                        $hotelmarge->setMarge($values->getMarge());

                        $hotelmarge->setPrst($values->getPrst());

                        $em->persist($hotelmarge);

                        $em->flush();


                    }

                }

                return $this->redirect($this->generateUrl('btob_margeperiod_homepage'));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('BtobHotelBundle:Margeperiod:form.html.twig', array(

            'form' => $form->createView(),

        ));

    }


    public function editAction($id)
    {

        $request = $this->get('request');
        $margeperiod = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Margeperiod')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new MargeperiodType(), $margeperiod);

        $form->handleRequest($request);



        if ($form->isValid()) {

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier une marge du periode n° " . $margeperiod->getId()." du " . date_format($margeperiod->getDated(), 'Y-m-d')." au " . date_format($margeperiod->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

            $em->flush();




            return $this->redirect($this->generateUrl('btob_margeperiod_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Margeperiod:edit.html.twig', array('form' => $form->createView(),  'id' => $id)
        );
    }

    public function deleteAction(Margeperiod $margeperiod)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$margeperiod) {
            throw new NotFoundHttpException("Hotelprice non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete une marge du periode n° " . $margeperiod->getId(). date_format($margeperiod->getDated(), 'Y-m-d')." au " . date_format($margeperiod->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($margeperiod);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_margeperiod_homepage'));
    }

    public function validdateaddAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {
            $id = $request->request->get("id");
            $dated = explode("/", $request->request->get("dated"));
            $dates = explode("/", $request->request->get("dates"));

            $dt = new \DateTime();
            $dt->setDate($dated[2], $dated[1], $dated[0]);
            $dt2 = new \DateTime();
            $dt2->setDate($dates[2], $dates[1], $dates[0]);
            $nbjour = $dt->diff($dt2)->days;
            if($nbjour<1){
                echo "false";
                exit;
            }

            $margeid = $request->request->get("margeid");

            if (!is_numeric($margeid)) {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Margeperiod')->ValidateInterval($id, $dt, $dt2);
            } else {
                echo $this->getDoctrine()->getRepository('BtobHotelBundle:Margeperiod')->ValidateInterval($id, $dt, $dt2, $margeid);
            }
            exit;

        }
        echo "false";
        exit;
    }


    public function homeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $margeperiod = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Margeperiod')
            ->find($id);
        $entitiesagence = $em->getRepository('UserUserBundle:User')->findRole();
        $entitiessales = $em->getRepository('UserUserBundle:User')->findRoles();
        $entitiessite = $em->getRepository('UserUserBundle:User')->findRolem();


        $entities = array_merge($entitiesagence, $entitiessales,$entitiessite);

        if (!$entities) {
            throw $this->createNotFoundException('Unable to find Speaker entity.');
        }
        return $this->render('BtobHotelBundle:Margeperiod:home.html.twig', array(
            'entities' => $entities,
            'id' => $id,
        ));
    }


    public function listAction($userid,$margeperiodid,Request $request) {
        $em = $this->getDoctrine()->getManager();

        $value = $request->get('_route_params');
        $x= (int)$value["userid"];
        $p= (int)$value["margeperiodid"];

        $nameagence= $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->find($x);
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();


        $marge = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->listUserAndPeriod($margeperiodid,$userid);

        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {
            $count=count($hotel);

            $occurence= $request->request->get("val");
            $pers = $request->request->get("prst");
            $hotel = $request->request->get("hotel");


            for ($i=0; $i<$count; $i++) {
                for ($j=0; $j<count($count); $j++) {
                    if($marge[$j]!=null)
                    {
                        $em->remove($marge[$j]);
                        $em->flush();
                    }

                }
                foreach ($request->request->get("val") as $key => $value) {


                    $ff=$request->request->get("val");

                    //Tools::dump($request->request, TRUE);
                    $yy= (int)$hotel[$key];

                    $oneuser = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($x);
                    $period =  $this->getDoctrine()->getRepository('BtobHotelBundle:Margeperiod')->find($p);
                    $hotelmarge[$i] = new Hotelmarge();
                    $hotelmarge[$i]->setUser($oneuser);
                    $hotelmarge[$i]->setMargeperiod($period);
                    $hotelmarge[$i]->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($yy));
                    $hotelmarge[$i]->setMarge($value);
                    if (isset($pers[$key]))
                        $hotelmarge[$i]->setPrst(true);
                    $em->persist($hotelmarge[$i]);
                    $em->flush();
                }
                $i=$count;
            }
            return $this->redirect($this->generateUrl('btob_margeperiod_homepage'));
        }

        $zz=$hotel[0];
        return $this->render('BtobHotelBundle:Margeperiod:list.html.twig', array(
            'marge' => $marge,
            "userid" => $userid,
            "margeperiodid" => $margeperiodid,
            "nameagence" => $nameagence,
            'hotels' => $hotel,
            'zz' => $zz
        ));
    }

    public function resetAction($margeperiodid,$userid,Request $request) {
        $em = $this->getDoctrine()->getManager();

        $value = $request->get('_route_params');

        $x= (int)$value["userid"];
        $y= (int)$value["margeperiodid"];


        $nameagence= $this->getDoctrine()
            ->getRepository('UserUserBundle:User')
            ->find($x);
        $hotel = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();

        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($x);
        $period = $this->getDoctrine()->getRepository('BtobHotelBundle:Margeperiod')->find($y);
        $marge = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->listUserPeriod($user,$period);



        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {
            $count=count($marge);

            $occurence= $request->request->get("val");

            $pers = $request->request->get("prst");
            if($pers==null)
            {
                $perst = false;
            }else{
                $perst = true;
            }

            $hotel = $request->request->get("hotel");
            foreach ($occurence as $keys => $value) {
                $zzz= $value;
            }

            foreach ($marge as $key => $value) {

                if ($marge == null) {
                    $oneuser = $this->getDoctrine()->getRepository('UserUserBundle:User')->find($x);
                    $oneperiod = $this->getDoctrine()->getRepository('BtobHotelBundle:Margeperiod')->find($y);
                    $hotelmarges[$key] = new Hotelmarge();
                    $hotelmarges[$key]->setUser($oneuser);
                    $hotelmarges[$key]->setMargeperiod($oneperiod);
                    $hotelmarges[$key]->setHotel($this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find(1));
                    $hotelmarges[$key]->setMarge($occurence);

                    $hotelmarges[$key]->setPrst(true);
                    $em->persist($hotelmarges[$key]);
                } else {
                    $em = $this->getDoctrine()->getManager();
                    $hotelmarges[$key] = $em->getRepository('BtobHotelBundle:Hotelmarge')->find($value->getId());
                    $hotelmarges[$key]->setMarge($zzz);
                    $hotelmarges[$key]->setPrst($perst);

                }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("reset une marge du periode n° " . $period->getId()." du " . date_format($period->getDated(), 'Y-m-d')." au " . date_format($period->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                    $em->flush();
            }





            return $this->redirect($this->generateUrl('btob_margeperiod_homepage'));
        }

        $zz=$hotel[0];

        return $this->render('BtobHotelBundle:Margeperiod:list.html.twig', array(
            'marge' => $marge,
            "userid" => $userid,
            "margeperiodid" => $margeperiodid,
            "nameagence" => $nameagence,
            'hotels' => $hotel,
            'zz' => $zz
        ));
    }
}
