<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * chambre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\chambreRepository")
 */
class chambre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbadultes", type="integer")
     */
    private $nbadultes;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbenfants", type="integer")
     */
    private $nbenfants;
    
    /**
     * @var array
     * @ORM\Column(name="agesenfants", type="json_array")
     */
    private $ageenfant;
       /**
     * @var array
     * @ORM\Column(name="supplements", type="json_array")
     */
    private $supplements;

    /**
     * @var string
     *
     * @ORM\Column(name="arrangement", type="string", length=255)
     */
    private $arrangement;
      /**
  * @ORM\ManyToOne(targetEntity="ReservationHotel",inversedBy="chambre")
  * @ORM\JoinColumn(nullable=true)
  */
  private $reservationhotel;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return chambre
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nbadultes
     *
     * @param integer $nbadultes
     *
     * @return chambre
     */
    public function setNbadultes($nbadultes)
    {
        $this->nbadultes = $nbadultes;

        return $this;
    }

    /**
     * Get nbadultes
     *
     * @return integer
     */
    public function getNbadultes()
    {
        return $this->nbadultes;
    }

    /**
     * Set nbenfants
     *
     * @param integer $nbenfants
     *
     * @return chambre
     */
    public function setNbenfants($nbenfants)
    {
        $this->nbenfants = $nbenfants;

        return $this;
    }

    /**
     * Get nbenfants
     *
     * @return integer
     */
    public function getNbenfants()
    {
        return $this->nbenfants;
    }

    /**
     * Set arrangement
     *
     * @param string $arrangement
     *
     * @return chambre
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return string
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Set reservationhotel
     *
     * @param \Btob\HotelBundle\Entity\ReservationHotel $reservationhotel
     *
     * @return chambre
     */
    public function setReservationhotel(\Btob\HotelBundle\Entity\ReservationHotel $reservationhotel = null)
    {
        $this->reservationhotel = $reservationhotel;

        return $this;
    }

    /**
     * Get reservationhotel
     *
     * @return \Btob\HotelBundle\Entity\ReservationHotel
     */
    public function getReservationhotel()
    {
        return $this->reservationhotel;
    }

    /**
     * Set ageenfant
     *
     * @param array $ageenfant
     *
     * @return chambre
     */
    public function setAgeenfant($ageenfant)
    {
        $this->ageenfant = $ageenfant;

        return $this;
    }

    /**
     * Get ageenfant
     *
     * @return array
     */
    public function getAgeenfant()
    {
        return $this->ageenfant;
    }
    
    
        /**
     * Set supplements
     *
     * @param array $supplements
     *
     * @return chambre
     */
    public function setSupplements($supplements)
    {
        $this->supplements = $supplements;

        return $this;
    }

    /**
     * Get supplements
     *
     * @return array
     */
    public function getSupplements()
    {
        return $this->supplements;
    }
}
