<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotelarrangement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\HotelarrangementRepository")
 */
class Hotelarrangement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="hotelarrangement")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;
    /**
     * @ORM\ManyToOne(targetEntity="Arrangement", inversedBy="hotelarrangement")
     * @ORM\JoinColumn(name="arr_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $arrangement;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", length=11)
     */
    private $etat;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Hotelarrangement
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }



    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set arrangement
     *
     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement
     * @return Hotelarrangement
     */
    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return \Btob\HotelBundle\Entity\Arrangement 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }
    /**
     * Set etat
     *
     * @param integer $etat
     * @return Hotelarrangement
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }
}
