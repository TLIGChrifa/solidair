<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordonneesvo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\CoordonneesvoRepository")
 */
class Coordonneesvo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer")
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adulte", type="string", length=255)
     */
    private $adulte;

    /**
     * @var string
     *
     * @ORM\Column(name="adultes", type="string", length=255)
     */
    private $adultes;

    /**
     * @var string
     *
     * @ORM\Column(name="demandes", type="string", length=255)
     */
    private $demandes;
    /**
     * @var string
     *
     * @ORM\Column(name="vo", type="string", length=255)
     */
    private $vo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Coordonneesvo
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Coordonneesvo
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return Coordonneesvo
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Coordonneesvo
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adulte
     *
     * @param string $adulte
     *
     * @return Coordonneesvo
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return string
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set adultes
     *
     * @param string $adultes
     *
     * @return Coordonneesvo
     */
    public function setAdultes($adultes)
    {
        $this->adultes = $adultes;

        return $this;
    }

    /**
     * Get adultes
     *
     * @return string
     */
    public function getAdultes()
    {
        return $this->adultes;
    }

    /**
     * Set demandes
     *
     * @param string $demandes
     *
     * @return Coordonneesvo
     */
    public function setDemandes($demandes)
    {
        $this->demandes = $demandes;

        return $this;
    }

    /**
     * Get demandes
     *
     * @return string
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Set vo
     *
     * @param \Btob\SejourBundle\Entity\Sejour $vo
     *
     * @return Coordonneesvo
     */
    public function setVo(\Btob\SejourBundle\Entity\Sejour $vo = null)
    {
        $this->vo = $vo;

        return $this;
    }

    /**
     * Get vo
     *
     * @return \Btob\SejourBundle\Entity\Sejour
     */
    public function getVo()
    {
        return $this->vo;
    }
}
