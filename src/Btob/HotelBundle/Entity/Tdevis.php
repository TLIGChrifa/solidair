<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Tdevis
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\TdevisRepository")
 */
class Tdevis

{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Devis", inversedBy="tdevis")
     * @ORM\JoinColumn(name="devis_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $devis;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float" , nullable=true)
     */
    private $montant;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_p", type="float" , nullable=true)
     */
    private $montant_p;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */
    private $type;

/**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer" , nullable=true)
     */
    private $quantite;

/**
     * @var integer
     *
     * @ORM\Column(name="prix_unitaire", type="integer" , nullable=true)
     */
    private $prixUnitaire;

/**
     * @var integer
     *
     * @ORM\Column(name="remise", type="integer" , nullable=true)
     */
    private $remise;


    /**
     * @var integer
     *
     * @ORM\Column(name="tva", type="integer" , nullable=true)
     */
    private $tva;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set montant
     *
     * @param float $montant
     * @return Tdevis
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }


    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }
    
    /**
     * Set montant_p
     *
     * @param float $montant_p
     * @return Tdevis
     */
    public function setMontantP($montant_p)
    {
        $this->montant_p = $montant_p;

        return $this;
    }


    /**
     * Get montant_p
     *
     * @return float
     */
    public function getMontantP()
    {
        return $this->montant_p;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Tdevis
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;

    }


    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set devis
     *
     * @param \Btob\HotelBundle\Entity\Devis $devis
     * @return Tdevis
     */
    public function setDevis(\Btob\HotelBundle\Entity\Devis $devis = null)
    {
        $this->devis = $devis;

        return $this;
    }


    /**
     * Get devis
     *
     * @return \Btob\HotelBundle\Entity\Devis
     */
    public function getDevis()
    {
        return $this->devis;
    }
 /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get tva
     *
     * @return integer
     */
    public function getTva()
    {
        return $this->tva;
    }
    /**
     * Get remise
     *
     * @return integer
     */
    public function getRemise()
    {
        return $this->remise;
    }

     /**
     * Get prixUnitaire
     *
     * @return integer
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }


    


    /**
     * Set quantite
     *
     * @param float $quantite
     * @return Tdevis
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }


/**
     * Set tva
     *
     * @param float $tva
     * @return Tdevis
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

/**
     * Set remise
     *
     * @param float $remise
     * @return Tdevis
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * Set prixUnitaire
     *
     * @param float $prixUnitaire
     * @return Tdevis
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

}

