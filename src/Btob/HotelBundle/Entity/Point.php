<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Point
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PointRepository")
 */
class Point
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var float
     *
     * @ORM\Column(name="prixmin", type="float")
     */
    private $prixmin;
    
    /**
     * @var float
     *
     * @ORM\Column(name="prixmax", type="float")
     */
    private $prixmax;      
    
    /**
     * @var float
     *
     * @ORM\Column(name="pt", type="float")
     */
    private $pt;   
    
    
    public function __toString(){
        return $this->prixmin;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set prixmin
     *
     * @param float $prixmin
     * @return Point
     */
    public function setPrixmin($prixmin)
    {
        $this->prixmin = $prixmin;

        return $this;
    }

    /**
     * Get prixmin
     *
     * @return float 
     */
    public function getPrixmin()
    {
        return $this->prixmin;
    }
    
    /**
     * Set prixmax
     *
     * @param float $prixmax
     * @return Point
     */
    public function setPrixmax($prixmax)
    {
        $this->prixmax = $prixmax;

        return $this;
    }

    /**
     * Get prixmax
     *
     * @return float 
     */
    public function getPrixmax()
    {
        return $this->prixmax;
    }
    
    /**
     * Set pt
     *
     * @param float $pt
     * @return Point
     */
    public function setPt($pt)
    {
        $this->pt = $pt;

        return $this;
    }

    /**
     * Get pt
     *
     * @return float 
     */
    public function getPt()
    {
        return $this->pt;
    }
        
    
    
}
