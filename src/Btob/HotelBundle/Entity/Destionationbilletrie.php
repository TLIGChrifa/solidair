<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Destionationbilletrie
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\DestionationbilletrieRepository")
 */
class Destionationbilletrie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="villed", type="string", length=255)
     */
    private $villed;

    /**
     * @var string
     *
     * @ORM\Column(name="villea", type="string", length=255)
     */
    private $villea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedepart", type="date")
     */
    private $datedepart;

       /**
         * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Billetriecoordonnes",inversedBy="destinationbilletrie")
        * @ORM\JoinColumn(nullable=true)
        */
    private $billettriecoordonnees;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set villed
     *
     * @param string $villed
     *
     * @return Destionationbilletrie
     */
    public function setVilled($villed)
    {
        $this->villed = $villed;

        return $this;
    }

    /**
     * Get villed
     *
     * @return string
     */
    public function getVilled()
    {
        return $this->villed;
    }

    /**
     * Set villea
     *
     * @param string $villea
     *
     * @return Destionationbilletrie
     */
    public function setVillea($villea)
    {
        $this->villea = $villea;

        return $this;
    }

    /**
     * Get villea
     *
     * @return string
     */
    public function getVillea()
    {
        return $this->villea;
    }

    /**
     * Set datedepart
     *
     * @param \DateTime $datedepart
     *
     * @return Destionationbilletrie
     */
    public function setDatedepart($datedepart)
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    /**
     * Get datedepart
     *
     * @return \DateTime
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }

    /**
     * Set billettriecoordonnees
     *
     * @param \Btob\HotelBundle\Entity\Billetriecoordonnes $billettriecoordonnees
     *
     * @return Destionationbilletrie
     */
    public function setBillettriecoordonnees(\Btob\HotelBundle\Entity\Billetriecoordonnes $billettriecoordonnees = null)
    {
        $this->billettriecoordonnees = $billettriecoordonnees;

        return $this;
    }

    /**
     * Get billettriecoordonnees
     *
     * @return \Btob\HotelBundle\Entity\Billetriecoordonnes
     */
    public function getBillettriecoordonnees()
    {
        return $this->billettriecoordonnees;
    }
}
