<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * maisonhote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\maisonhoteRepository")
 */
class maisonhote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortdesc", type="string", length=255)
     */
    private $shortdesc;
        /**
     * @var decimal
     *
     * @ORM\Column(name="prix", type="decimal")
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="longdesc", type="string", length=255)
     */
    private $longdesc;
    //     /**
    //  * @ORM\ManyToOne(targetEntity="Pays", inversedBy="maisonhote")
    //  * @ORM\JoinColumn(name="pays_id", referencedColumnName="id",onDelete="CASCADE")
    //  */
    // protected $pays;

    // /**
    //  * @ORM\ManyToOne(targetEntity="Ville", inversedBy="maisonhote")
    //  * @ORM\JoinColumn(name="ville_id", referencedColumnName="id",onDelete="CASCADE")
    //  */
    // protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="nbchambres", type="string")
     */
    private $nbchambres;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortdesc
     *
     * @param string $shortdesc
     *
     * @return maisonhote
     */
    public function setShortdesc($shortdesc)
    {
        $this->shortdesc = $shortdesc;

        return $this;
    }

    /**
     * Get shortdesc
     *
     * @return string
     */
    public function getShortdesc()
    {
        return $this->shortdesc;
    }

  
    /**
     * Set nbchambres
     *
     * @param string $nbchambres
     *
     * @return maisonhote
     */
    public function setNbchambres($nbchambres)
    {
        $this->nbchambres = $nbchambres;

        return $this;
    }

    /**
     * Get nbchambres
     *
     * @return string
     */
    public function getNbchambres()
    {
        return $this->nbchambres;
    }

    // /**
    //  * Set pays
    //  *
    //  * @param \Btob\HotelBundle\Entity\Pays $pays
    //  *
    //  * @return maisonhote
    //  */
    // public function setPays(\Btob\HotelBundle\Entity\Pays $pays = null)
    // {
    //     $this->pays = $pays;

    //     return $this;
    // }

    // /**
    //  * Get pays
    //  *
    //  * @return \Btob\HotelBundle\Entity\Pays
    //  */
    // public function getPays()
    // {
    //     return $this->pays;
    // }

    // /**
    //  * Set ville
    //  *
    //  * @param \Btob\HotelBundle\Entity\Ville $ville
    //  *
    //  * @return maisonhote
    //  */
    // public function setVille(\Btob\HotelBundle\Entity\Ville $ville = null)
    // {
    //     $this->ville = $ville;

    //     return $this;
    // }

    // /**
    //  * Get ville
    //  *
    //  * @return \Btob\HotelBundle\Entity\Ville
    //  */
    // public function getVille()
    // {
    //     return $this->ville;
    // }

    /**
     * Set prix
     *
     * @param decimal $prix
     *
     * @return maisonhote
     */

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return maisonhote
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }
}
