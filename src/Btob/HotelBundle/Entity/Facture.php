<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Facture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\FactureRepository")
 */
class Facture

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=255 , nullable=true)
     */

    private $num;

    /**
     * @var integer
     *
     * @ORM\Column(name="idm", type="integer" , nullable=true)
     */

    private $idm;	
    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float" , nullable=true)
     */

    private $montant;

     /**
     * @var float
     *
     * @ORM\Column(name="montant_p", type="float" , nullable=true)
     */

    private $montantP;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=255 , nullable=true)
     */

    private $client;


    /**
     * @ORM\ManyToOne(targetEntity="Clients", inversedBy="facture")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */

    private $clientId;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */

    private $type;

/**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255 , nullable=true)
     */

    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */

    private $etat;

        /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */

private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */

private $dcr;
    /**
     * @var integer
     *
     * @ORM\Column(name="supp", type="integer" , nullable=true)
     */

    private $supp;	


    /**
     * Constructor
     */

    public function __construct()
    {
        $this->dcr = new \DateTime();
        $this->code="FO";
    }

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }

    public function __toString()

    {

        return $this->num;

    }

    /**
     * Set idm
     *
     * @param integer $idm
     * @return Facture
     */
    public function setIdm($idm)
    {
        $this->idm = $idm;
        return $this;
    }

    /**
     * Get idm
     *
     * @return integer
     */
    public function getIdm()
    {
        return $this->idm;
    }	
	
    /**
     * Set num
     *
     * @param string $num
     * @return Facture
     */

    public function setNum($num)

    {

        $this->num = $num;


        return $this;

    }


    /**
     * Get num
     *
     * @return string
     */

    public function getNum()

    {

        return $this->num;

    }

    /**
     * Set montant
     *
     * @param float $montant
     * @return Facture
     */

    public function setMontant($montant)

    {

        $this->montant = $montant;


        return $this->montant;

    }
    /**
     * Set montantP
     *
     * @param float $montantP
     * @return Facture
     */

    public function setMontantP($montantP)

    {

        $this->montantP = $montantP;


        return $this->montantP;

    }



    /**
     * Get montant
     *
     * @return float
     */

    public function getMontant()

    {

        return $this->montant;

    }

     /**
     * Get montantP
     *
     * @return float
     */

    public function getMontantP()

    {

        return $this->montantP;

    }


    /**
     * Set client
     *
     * @param string $client
     * @return Facture
     */

    public function setClient($client)

    {

        $this->client = $client;


        return $this;

    }


    /**
     * Set clientId
     *
     * @param \Btob\HotelBundle\Entity\Clients $clientId
     * @return Facture
     */

    public function setClientId($clientId)

    {

        $this->clientId = $clientId;


        return $this;

    }


    



    /**
     * Get client
     *
     * @return string
     */

    public function getClient()

    {

        return $this->client;

    }

     /**
     * Get clientId
     *
     * @return \Btob\HotelBundle\Entity\Clients
     */

    public function getClientId()

    {

        return $this->clientId;

    }




    /**
     * Set type
     *
     * @param string $type
     * @return Facture
     */

    public function setType($type)

    {

        $this->type = $type;


        return $this;

    }


    /**
     * Get type
     *
     * @return string
     */

    public function getType()

    {

        return $this->type;

    }
    /**
     * Set code
     *
     * @param string $code
     * @return Facture
     */

    public function setCode($code)

    {

        $this->code = $code;


        return $this;

    }


    /**
     * Get code
     *
     * @return string
     */

    public function getCode()

    {

        return $this->code;

    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Facture
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }


    /**
     * Set date
     *
     * @param \Date $date
     * @return Facture
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }


    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Facture
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

     /**
     * Get date
     *
     * @return \Date
     */

    public function getDate()
    {
        return $this->date;

    }


    /**
     * Get dcr
     *
     * @return \DateTime
     */

    public function getDcr()
    {
        return $this->dcr;

    }

    /**
     * Set supp
     *
     * @param integer $supp
     * @return Facture
     */
    public function setSupp($supp)
    {
        $this->supp = $supp;
        return $this;
    }

    /**
     * Get supp
     *
     * @return integer
     */
    public function getSupp()
    {
        return $this->supp;
    }
}

