<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordonneesmaisondhote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\CoordonneesmaisondhoteRepository")
 */
class Coordonneesmaisondhote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adultes", type="string", length=255)
     */
    private $adultes;

    /**
     * @var string
     *
     * @ORM\Column(name="adulte", type="string", length=255)
     */
    private $adulte;

    /**
     * @var string
     *
     * @ORM\Column(name="demande", type="string", length=255)
     */
    private $demande;
     /**
     * @var integer
     *
     * @ORM\Column(name="reservationmaisondhote", type="integer")
     */
    private $reservationmaisondhote;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Coordonneesmaisondhote
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Coordonneesmaisondhote
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Coordonneesmaisondhote
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Coordonneesmaisondhote
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adultes
     *
     * @param string $adultes
     *
     * @return Coordonneesmaisondhote
     */
    public function setAdultes($adultes)
    {
        $this->adultes = $adultes;

        return $this;
    }

    /**
     * Get adultes
     *
     * @return string
     */
    public function getAdultes()
    {
        return $this->adultes;
    }

    /**
     * Set adulte
     *
     * @param string $adulte
     *
     * @return Coordonneesmaisondhote
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return string
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set demande
     *
     * @param string $demande
     *
     * @return Coordonneesmaisondhote
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return string
     */
    public function getDemande()
    {
        return $this->demande;
    }

   

    /**
     * Set reservationmaisondhote
     *
     * @param integer $reservationmaisondhote
     *
     * @return Coordonneesmaisondhote
     */
    public function setReservationmaisondhote($reservationmaisondhote)
    {
        $this->reservationmaisondhote = $reservationmaisondhote;

        return $this;
    }

    /**
     * Get reservationmaisondhote
     *
     * @return integer
     */
    public function getReservationmaisondhote()
    {
        return $this->reservationmaisondhote;
    }
}
