<?php



namespace Btob\HotelBundle\Entity;



use Doctrine\ORM\Mapping as ORM;



/**

 * Margeperiod

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\MargeperiodRepository")

 */

class Margeperiod {



    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;




    /**

     * @var \DateTime

     *

     * @ORM\Column(name="dated", type="date" , nullable=true)

     */

    private $dated;



    /**

     * @var \DateTime

     *

     * @ORM\Column(name="dates", type="date" , nullable=true)

     */

    private $dates;




    /**

     * @var \DateTime

     *

     * @ORM\Column(name="dcr", type="datetime")

     */

    private $dcr;

    /**

     * @ORM\OneToMany(targetEntity="Btob\HotelBundle\Entity\Hotelmarge", mappedBy="margeperiod", cascade={"remove"})

     */

    protected $hotelmarge;

    /**

     * Constructor

     */

    public function __construct()

    {

        $this->dcr = new \DateTime();

    }



    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }


    /**

     * Set dated

     *

     * @param \DateTime $dated

     * @return Margeperiod

     */

    public function setDated($dated)

    {

        $this->dated = $dated;



        return $this;

    }



    /**

     * Get dated

     *

     * @return \DateTime 

     */

    public function getDated()

    {

        return $this->dated;

    }



    /**

     * Set dates

     *

     * @param \DateTime $dates

     * @return Margeperiod

     */

    public function setDates($dates)

    {

        $this->dates = $dates;



        return $this;

    }



    /**

     * Get dates

     *

     * @return \DateTime 

     */

    public function getDates()

    {

        return $this->dates;

    }


    /**

     * Set dcr

     *

     * @param \DateTime $dcr

     * @return Margeperiod

     */

    public function setDcr($dcr)

    {

        $this->dcr = $dcr;



        return $this;

    }



    /**

     * Get dcr

     *

     * @return \DateTime 

     */

    public function getDcr()

    {

        return $this->dcr;

    }


    /**

     * Add hotelmarge

     *

     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge

     * @return Margeperiod

     */

    public function addHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)

    {

        $this->hotelmarge[] = $hotelmarge;



        return $this;

    }



    /**

     * Remove hotelmarge

     *

     * @param \Btob\HotelBundle\Entity\Hotelmarge $hotelmarge

     */

    public function removeHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)

    {

        $this->hotelmarge->removeElement($hotelmarge);

    }



    /**

     * Get hotelmarge

     *

     * @return \Doctrine\Common\Collections\Collection

     */

    public function getHotelmarge()

    {

        return $this->hotelmarge;

    }




}

