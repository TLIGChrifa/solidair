<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\EventsRepository")
 */
class Events
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

    /**
     * @var float
     *
     * @ORM\Column(name="pricech", type="float" , nullable=true)
     */
    private $pricech;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers", type="boolean" , nullable=true)
     */
    private $pers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oblig", type="boolean" , nullable=true)
     */
    private $oblig;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="events")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotel;

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float")
     */
    private $marge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persm", type="boolean")
     */
    private $persm;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Events
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Events
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Events
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set pricech
     *
     * @param float $pricech
     * @return Events
     */
    public function setPricech($pricech)
    {
        $this->pricech = $pricech;

        return $this;
    }

    /**
     * Get pricech
     *
     * @return float 
     */
    public function getPricech()
    {
        return $this->pricech;
    }

    /**
     * Set pers
     *
     * @param boolean $pers
     * @return Events
     */
    public function setPers($pers)
    {
        $this->pers = $pers;

        return $this;
    }

    /**
     * Get pers
     *
     * @return boolean 
     */
    public function getPers()
    {
        return $this->pers;
    }

    /**
     * Set oblig
     *
     * @param boolean $oblig
     * @return Events
     */
    public function setOblig($oblig)
    {
        $this->oblig = $oblig;

        return $this;
    }

    /**
     * Get oblig
     *
     * @return boolean 
     */
    public function getOblig()
    {
        return $this->oblig;
    }

    /**
     * Set hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Events
     */
    public function setHotel(\Btob\HotelBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Btob\HotelBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }
    
    /**
     * Set marge
     *
     * @param float $marge
     * @return Events
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float
     */
    public function getMarge()
    {
        return $this->marge;
    }
    
    
    /**
     * Set persm
     *
     * @param boolean $persm
     * @return Events
     */
    public function setPersm($persm)
    {
        $this->persm = $persm;

        return $this;
    }

    /**
     * Get persm
     *
     * @return boolean
     */
    public function getPersm()
    {
        return $this->persm;
    }    
    
}
