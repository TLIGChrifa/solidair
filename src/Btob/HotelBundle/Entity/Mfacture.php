<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Mfacture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\MfactureRepository")
 */
class Mfacture

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string" , nullable=true)
     */
     private $mode;
 /**
     * @ORM\ManyToOne(targetEntity="Facture", inversedBy="Mfacture")
     * @ORM\JoinColumn(name="facture_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $facture;

   

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float" , nullable=true)
     */

    private $montant;	

    

    

    /**
     * @var date
     *
     * @ORM\Column(name="date_cheque", type="date" , nullable=true)
     */

    private $date_cheque;


    /**
     * @var string
     *
     * @ORM\Column(name="numero_cheque", type="string",nullable=true)
     */

    private $numero_cheque;

    /**
     * @var string
     *
     * @ORM\Column(name="rib", type="string",nullable=true)
     */
    private $rib;
     /**
     * @var string
     *
     * @ORM\Column(name="motif", type="string",nullable=true)
     */

    private $motif;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string",nullable=true)
    */

    private $etat;
     /**
     * @var string
     *
     * @ORM\Column(name="montantsolde", type="string",nullable=true)
     */

    private $montantsolde;
     /**
     * @var string
     *
     * @ORM\Column(name="banque", type="string",nullable=true)
     */

    private $banque;


    

    

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date",nullable=true)
     */

private $date;
    
    

    

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }

    public function __toString()

    {

        return $this->num;

    }

    
    /**
     * Set mode
     *
     * @param string $mode
     * @return Mfacture
     */

    public function setMode($mode)

    {

        $this->mode = $mode;


        return $this;

    }


    /**
     * Get mode
     *
     * @return string
     */

    public function getMode()

    {

        return $this->mode;

    }

    /**
     * Set montant
     *
     * @param float $montant
     * @return Facture
     */

    public function setMontant($montant)

    {

        $this->montant = $montant;


        return $this->montant;

    }
    /**
     * Set date_cheque
     *
     * @param date $date_cheque
     * @return Mfacture
     */

    public function setDate_cheque($date_cheque)

    {

        $this->date_cheque = $date_cheque;


        return $this->date_cheque;

    }

/**
     * Set banque
     *
     * @param string $banque
     * @return Mfacture
     */

    public function setBanque($banque)

    {

        $this->banque = $banque;


        return $this;

    }

    /**
     * Get montant
     *
     * @return float
     */

    public function getMontant()

    {

        return $this->montant;

    }

     /**
     * Get date_cheque
     *
     * @return date
     */

    public function getDate_cheque()

    {

        return $this->date_cheque;

    }


    


    /**
     * Set numero_cheque
     *
     * @param string $numero_cheque
     * @return Mfacture
     */

    public function setNumero_cheque($numero_cheque)

    {

        $this->numero_cheque = $numero_cheque;


        return $this;

    }


    


   

    /**
     * Get numero_cheque
     *
     * @return string
     */

    public function getNumero_cheque()

    {

        return $this->numero_cheque;

    }

    /**
     * Get banque
     *
     * @return float
     */

    public function getBanque()

    {

        return $this->banque;

    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Facture
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }


    /**
     * Get date
     *
     * @return \DateTime
     */

    public function getDate()
    {
        return $this->date;

    }
    public function setFacture(\Btob\HotelBundle\Entity\Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }


    /**
     * Set dateCheque
     *
     * @param \DateTime $dateCheque
     *
     * @return Mfacture
     */
    public function setDateCheque($dateCheque)
    {
        $this->date_cheque = $dateCheque;

        return $this;
    }

    /**
     * Get dateCheque
     *
     * @return \DateTime
     */
    public function getDateCheque()
    {
        return $this->date_cheque;
    }

    /**
     * Set numeroCheque
     *
     * @param string $numeroCheque
     *
     * @return Mfacture
     */
    public function setNumeroCheque($numeroCheque)
    {
        $this->numero_cheque = $numeroCheque;

        return $this;
    }

    /**
     * Get numeroCheque
     *
     * @return string
     */
    public function getNumeroCheque()
    {
        return $this->numero_cheque;
    }

    /**
     * Set rib
     *
     * @param string $rib
     *
     * @return Mfacture
     */
    public function setRib($rib)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib
     *
     * @return string
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * Set motif
     *
     * @param string $motif
     *
     * @return Mfacture
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;

        return $this;
    }

    /**
     * Get motif
     *
     * @return string
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return Mfacture
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set montantsolde
     *
     * @param string $montantsolde
     *
     * @return Mfacture
     */
    public function setMontantsolde($montantsolde)
    {
        $this->montantsolde = $montantsolde;

        return $this;
    }

    /**
     * Get montantsolde
     *
     * @return string
     */
    public function getMontantsolde()
    {
        return $this->montantsolde;
    }

    /**
     * Get facture
     *
     * @return \Btob\HotelBundle\Entity\Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }
}
