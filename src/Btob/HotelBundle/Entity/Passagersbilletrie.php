<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Passagersbilletrie
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PassagersbilletrieRepository")
 */
class Passagersbilletrie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="numpassport", type="string", length=255)
     */
    private $numpassport;
       /**
         * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Billetriecoordonnes",inversedBy="passagerbilletrie")
        * @ORM\JoinColumn(nullable=true)
        */
    private $billettriecoordonnees;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Passagersbilletrie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Passagersbilletrie
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set numpassport
     *
     * @param string $numpassport
     *
     * @return Passagersbilletrie
     */
    public function setNumpassport($numpassport)
    {
        $this->numpassport = $numpassport;

        return $this;
    }

    /**
     * Get numpassport
     *
     * @return string
     */
    public function getNumpassport()
    {
        return $this->numpassport;
    }

    /**
     * Set billettriecoordonnees
     *
     * @param \Btob\HotelBundle\Entity\Billetriecoordonnes $billettriecoordonnees
     *
     * @return Passagersbilletrie
     */
    public function setBillettriecoordonnees(\Btob\HotelBundle\Entity\Billetriecoordonnes $billettriecoordonnees = null)
    {
        $this->billettriecoordonnees = $billettriecoordonnees;

        return $this;
    }

    /**
     * Get billettriecoordonnees
     *
     * @return \Btob\HotelBundle\Entity\Billetriecoordonnes
     */
    public function getBillettriecoordonnees()
    {
        return $this->billettriecoordonnees;
    }
}
