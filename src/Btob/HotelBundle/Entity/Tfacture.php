<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Tfacture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\TfactureRepository")
 */
class Tfacture

{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Facture", inversedBy="tfacture")
     * @ORM\JoinColumn(name="facture_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $facture;

   


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */
    private $type;

/**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer" , nullable=true)
     */
    private $quantite;

/**
     * @var integer
     *
     * @ORM\Column(name="prix_unitaire", type="integer" , nullable=true)
     */
    private $prixUnitaire;

/**
     * @var integer
     *
     * @ORM\Column(name="remise", type="integer" , nullable=true)
     */
    private $remise;


    /**
     * @var integer
     *
     * @ORM\Column(name="tva", type="integer" , nullable=true)
     */
    private $tva;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

 /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get tva
     *
     * @return integer
     */
    public function getTva()
    {
        return $this->tva;
    }
    /**
     * Get remise
     *
     * @return integer
     */
    public function getRemise()
    {
        return $this->remise;
    }

     /**
     * Get prixUnitaire
     *
     * @return integer
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }


    


    /**
     * Set quantite
     *
     * @param float $quantite
     * @return Tfacture
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }


/**
     * Set tva
     *
     * @param float $tva
     * @return Tfacture
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

/**
     * Set remise
     *
     * @param float $remise
     * @return Tfacture
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * Set prixUnitaire
     *
     * @param float $prixUnitaire
     * @return Tfacture
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

   

    

   


    /**
     * Set type
     *
     * @param string $type
     * @return Tfacture
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;

    }


    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set facture
     *
     * @param \Btob\HotelBundle\Entity\Facture $facture
     * @return Tfacture
     */
    public function setFacture(\Btob\HotelBundle\Entity\Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }


    /**
     * Get facture
     *
     * @return \Btob\HotelBundle\Entity\Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }

}

