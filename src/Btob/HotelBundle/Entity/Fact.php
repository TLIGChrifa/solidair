<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Fact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\FactRepository")
 */
class Fact

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;


    /**
     * @var float
     *
     * @ORM\Column(name="tva", type="float" , nullable=true)
     */

    private $tva;

    /**
     * @var float
     *
     * @ORM\Column(name="timbre", type="float" , nullable=true)
     */

    private $timbre;

    /**
     * @var float
     *
     * @ORM\Column(name="frais", type="float" , nullable=true)
     */

    private $frais;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */

    private $name;

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }

    public function __toString()

    {

        return $this->name;

    }

    /**
     * Set tva
     *
     * @param float $tva
     * @return Fact
     */

    public function setTva($tva)

    {

        $this->tva = $tva;


        return $this;

    }


    /**
     * Get tva
     *
     * @return float
     */

    public function getTva()

    {

        return $this->tva;

    }

    /**
     * Set timbre
     *
     * @param float $timbre
     * @return Fact
     */

    public function setTimbre($timbre)

    {

        $this->timbre = $timbre;


        return $this;

    }


    /**
     * Get timbre
     *
     * @return float
     */

    public function getTimbre()

    {

        return $this->timbre;

    }

    /**
     * Set name
     *
     * @param string $name
     * @return Fact
     */

    public function setName($name)

    {

        $this->name = $name;


        return $this;

    }


    /**
     * Get name
     *
     * @return string
     */

    public function getName()

    {

        return $this->name;

    }

    /**
     * Set frais
     *
     * @param float $frais
     * @return Fact
     */

    public function setFrais($frais)

    {

        $this->frais = $frais;


        return $this;

    }


    /**
     * Get frais
     *
     * @return float
     */

    public function getFrais()

    {

        return $this->frais;

    }

}

