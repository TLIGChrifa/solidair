<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccompagnantsChambreshotel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\AccompagnantsChambreshotelRepository")
 */
class AccompagnantsChambreshotel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomadulte", type="string", length=255)
     */
    private $nomadulte;

    /**
     * @var integer
     *
     * @ORM\Column(name="ageadulte", type="integer")
     */
    private $ageadulte;

    /**
     * @var string
     *
     * @ORM\Column(name="nomenfant", type="string", length=255,nullable=true)
     */
    private $nomenfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="ageenfant", type="integer",nullable=true)
     */
    private $ageenfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="chambre", type="integer",nullable=true)
     */
    private $chambre;
     
     /**
     * @var integer
     *
     * @ORM\Column(name="reservationhotel", type="integer" )
     */
    private $reservationhotel;
         /**
     * @var array
     * @ORM\Column(name="recommandations", type="json_array")
     */
    private $Recommandations;
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomadulte
     *
     * @param string $nomadulte
     *
     * @return AccompagnantsChambreshotel
     */
    public function setNomadulte($nomadulte)
    {
        $this->nomadulte = $nomadulte;

        return $this;
    }

    /**
     * Get nomadulte
     *
     * @return string
     */
    public function getNomadulte()
    {
        return $this->nomadulte;
    }

    /**
     * Set ageadulte
     *
     * @param integer $ageadulte
     *
     * @return AccompagnantsChambreshotel
     */
    public function setAgeadulte($ageadulte)
    {
        $this->ageadulte = $ageadulte;

        return $this;
    }

    /**
     * Get ageadulte
     *
     * @return integer
     */
    public function getAgeadulte()
    {
        return $this->ageadulte;
    }

    /**
     * Set nomenfant
     *
     * @param string $nomenfant
     *
     * @return AccompagnantsChambreshotel
     */
    public function setNomenfant($nomenfant)
    {
        $this->nomenfant = $nomenfant;

        return $this;
    }

    /**
     * Get nomenfant
     *
     * @return string
     */
    public function getNomenfant()
    {
        return $this->nomenfant;
    }

    /**
     * Set ageenfant
     *
     * @param integer $ageenfant
     *
     * @return AccompagnantsChambreshotel
     */
    public function setAgeenfant($ageenfant)
    {
        $this->ageenfant = $ageenfant;

        return $this;
    }

    /**
     * Get ageenfant
     *
     * @return integer
     */
    public function getAgeenfant()
    {
        return $this->ageenfant;
    }

    /**
     * Set chambre
     *
     * @param integer $chambre
     *
     * @return AccompagnantsChambreshotel
     */
    public function setChambre($chambre)
    {
        $this->chambre = $chambre;

        return $this;
    }

    /**
     * Get chambre
     *
     * @return integer
     */
    public function getChambre()
    {
        return $this->chambre;
    }
    
     /**
     * Set reservationhotel
     *
     * @param integer $reservationhotel
     *
     * @return AccompagnantsChambreshotel
     */
    public function setReservationhotel($reservationhotel)
    {
        $this->reservationhotel = $reservationhotel;

        return $this;
    }
       /**
     * Get reservationhotel
     *
     * @return integer
     */
    public function getReservationhotel()
    {
        return $this->reservationhotel;
    }

    /**
     * Set recommandations
     *
     * @param array $recommandations
     *
     * @return AccompagnantsChambreshotel
     */
    public function setRecommandations($recommandations)
    {
        $this->Recommandations = $recommandations;

        return $this;
    }

    /**
     * Get recommandations
     *
     * @return array
     */
    public function getRecommandations()
    {
        return $this->Recommandations;
    }
}

