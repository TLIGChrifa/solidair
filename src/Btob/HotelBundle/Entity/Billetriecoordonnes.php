<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Billetriecoordonnes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\BilletriecoordonnesRepository")
 */
class Billetriecoordonnes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CIN", type="string", length=255)
     */
    private $cIN;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer")
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="adresee", type="string", length=255,nullable=true)
     */
    private $adresee;

    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=255,nullable=true)
     */
    private $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255,nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=255,nullable=true)
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="villedepart", type="string", length=255)
     */
    private $villedepart;

    /**
     * @var string
     *
     * @ORM\Column(name="villearrivee", type="string", length=255)
     */
    private $villearrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedepart", type="date")
     */
    private $datedepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datearrivee", type="date",nullable=true)
     */
    private $datearrivee;
    
   

    /**
     * @var string
     *
     * @ORM\Column(name="companie", type="string", length=255,nullable=true)
     */
    private $companie;

    /**
     * @var string
     *
     * @ORM\Column(name="classe", type="string", length=255,nullable=true)
     */
    private $classe;
     
    /**
     * @var string
     *
     * @ORM\Column(name="typevol", type="string", length=255)
     */
    private $typevol;
     /**
         * @ORM\OneToMany(targetEntity="Btob\HotelBundle\Entity\Passagersbilletrie",mappedBy="billettriecoordonnees",  cascade={"persist", "remove"}, orphanRemoval=true)
        * @ORM\JoinColumn(nullable=true)
        */
    private $passagerbilletrie;


         /**
         * @ORM\OneToMany(targetEntity="Btob\HotelBundle\Entity\Destionationbilletrie",mappedBy="billettriecoordonnees",  cascade={"persist", "remove"}, orphanRemoval=true)
        * @ORM\JoinColumn(nullable=true)
        */
        private $destinationbilletrie;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cIN
     *
     * @param string $cIN
     *
     * @return Billetriecoordonnes
     */
    public function setCIN($cIN)
    {
        $this->cIN = $cIN;

        return $this;
    }

    /**
     * Get cIN
     *
     * @return string
     */
    public function getCIN()
    {
        return $this->cIN;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return Billetriecoordonnes
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set adresee
     *
     * @param string $adresee
     *
     * @return Billetriecoordonnes
     */
    public function setAdresee($adresee)
    {
        $this->adresee = $adresee;

        return $this;
    }

    /**
     * Get adresee
     *
     * @return string
     */
    public function getAdresee()
    {
        return $this->adresee;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return Billetriecoordonnes
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Billetriecoordonnes
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Billetriecoordonnes
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Billetriecoordonnes
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set codepostal
     *
     * @param string $codepostal
     *
     * @return Billetriecoordonnes
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Billetriecoordonnes
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Billetriecoordonnes
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set villedepart
     *
     * @param string $villedepart
     *
     * @return Billetriecoordonnes
     */
    public function setVilledepart($villedepart)
    {
        $this->villedepart = $villedepart;

        return $this;
    }

    /**
     * Get villedepart
     *
     * @return string
     */
    public function getVilledepart()
    {
        return $this->villedepart;
    }

    /**
     * Set villearrivee
     *
     * @param string $villearrivee
     *
     * @return Billetriecoordonnes
     */
    public function setVillearrivee($villearrivee)
    {
        $this->villearrivee = $villearrivee;

        return $this;
    }

    /**
     * Get villearrivee
     *
     * @return string
     */
    public function getVillearrivee()
    {
        return $this->villearrivee;
    }

    /**
     * Set datedepart
     *
     * @param \DateTime $datedepart
     *
     * @return Billetriecoordonnes
     */
    public function setDatedepart($datedepart)
    {
        $this->datedepart = $datedepart;

        return $this;
    }

    /**
     * Get datedepart
     *
     * @return \DateTime
     */
    public function getDatedepart()
    {
        return $this->datedepart;
    }

    /**
     * Set datearrivee
     *
     * @param \DateTime $datearrivee
     *
     * @return Billetriecoordonnes
     */
    public function setDatearrivee($datearrivee)
    {
        $this->datearrivee = $datearrivee;

        return $this;
    }

    /**
     * Get datearrivee
     *
     * @return \DateTime
     */
    public function getDatearrivee()
    {
        return $this->datearrivee;
    }

    /**
     * Set companie
     *
     * @param string $companie
     *
     * @return Billetriecoordonnes
     */
    public function setCompanie($companie)
    {
        $this->companie = $companie;

        return $this;
    }

    /**
     * Get companie
     *
     * @return string
     */
    public function getCompanie()
    {
        return $this->companie;
    }

    /**
     * Set classe
     *
     * @param string $classe
     *
     * @return Billetriecoordonnes
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * Get classe
     *
     * @return string
     */
    public function getClasse()
    {
        return $this->classe;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->passagerbilletrie = new \Doctrine\Common\Collections\ArrayCollection();
        $this->destinationbilletrie = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set typevol
     *
     * @param string $typevol
     *
     * @return Billetriecoordonnes
     */
    public function setTypevol($typevol)
    {
        $this->typevol = $typevol;

        return $this;
    }

    /**
     * Get typevol
     *
     * @return string
     */
    public function getTypevol()
    {
        return $this->typevol;
    }

    /**
     * Add passagerbilletrie
     *
     * @param \Btob\HotelBundle\Entity\Passagersbilletrie $passagerbilletrie
     *
     * @return Billetriecoordonnes
     */
    public function addPassagerbilletrie(\Btob\HotelBundle\Entity\Passagersbilletrie $passagerbilletrie)
    {
        $this->passagerbilletrie[] = $passagerbilletrie;

        return $this;
    }

    /**
     * Remove passagerbilletrie
     *
     * @param \Btob\HotelBundle\Entity\Passagersbilletrie $passagerbilletrie
     */
    public function removePassagerbilletrie(\Btob\HotelBundle\Entity\Passagersbilletrie $passagerbilletrie)
    {
        $this->passagerbilletrie->removeElement($passagerbilletrie);
    }

    /**
     * Get passagerbilletrie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPassagerbilletrie()
    {
        return $this->passagerbilletrie;
    }

    /**
     * Add destinationbilletrie
     *
     * @param \Btob\HotelBundle\Entity\Destinationbilletrie $destinationbilletrie
     *
     * @return Billetriecoordonnes
     */
    public function addDestinationbilletrie(\Btob\HotelBundle\Entity\Destinationbilletrie $destinationbilletrie)
    {
        $this->destinationbilletrie[] = $destinationbilletrie;

        return $this;
    }

    /**
     * Remove destinationbilletrie
     *
     * @param \Btob\HotelBundle\Entity\Destinationbilletrie $destinationbilletrie
     */
    public function removeDestinationbilletrie(\Btob\HotelBundle\Entity\Destinationbilletrie $destinationbilletrie)
    {
        $this->destinationbilletrie->removeElement($destinationbilletrie);
    }

    /**
     * Get destinationbilletrie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDestinationbilletrie()
    {
        return $this->destinationbilletrie;
    }
}
