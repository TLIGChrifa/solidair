<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordonneesreservationhotel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\CoordonneesreservationhotelRepository")
 */
class Coordonneesreservationhotel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer")
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="adulte", type="string", length=255, nullable=true)
     */
    private $adulte;

    /**
     * @var string
     *
     * @ORM\Column(name="adultes", type="string", length=255, nullable=true)
     */
    private $adultes;

    /**
     * @var string
     *
     * @ORM\Column(name="demandes", type="string", length=255, nullable=true)
     */
    private $demandes;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\ReservationHotel",inversedBy="coordonnees")
    * @ORM\JoinColumn(nullable=true)
    */
    private $reservationhotel;

  /**
     * @var integer
     *
     * @ORM\Column(name="cin", type="integer")
     */
    private $cin;
      /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=255, nullable=true)
     */
    private $civilite;
     /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    private $pays;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Coordonneesreservationhotel
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Coordonneesreservationhotel
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return Coordonneesreservationhotel
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Coordonneesreservationhotel
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set adulte
     *
     * @param string $adulte
     *
     * @return Coordonneesreservationhotel
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return string
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set adultes
     *
     * @param string $adultes
     *
     * @return Coordonneesreservationhotel
     */
    public function setAdultes($adultes)
    {
        $this->adultes = $adultes;

        return $this;
    }

    /**
     * Get adultes
     *
     * @return string
     */
    public function getAdultes()
    {
        return $this->adultes;
    }

    /**
     * Set demandes
     *
     * @param string $demandes
     *
     * @return Coordonneesreservationhotel
     */
    public function setDemandes($demandes)
    {
        $this->demandes = $demandes;

        return $this;
    }

    /**
     * Get demandes
     *
     * @return string
     */
    public function getDemandes()
    {
        return $this->demandes;
    }

    /**
     * Set reservationhotel
     *
     * @param \Btob\HotelBundle\Entity\ReservationHotel $reservationhotel
     *
     * @return Coordonneesreservationhotel
     */
    public function setReservationhotel(\Btob\HotelBundle\Entity\ReservationHotel $reservationhotel = null)
    {
        $this->reservationhotel = $reservationhotel;

        return $this;
    }

    /**
     * Get reservationhotel
     *
     * @return \Btob\HotelBundle\Entity\ReservationHotel
     */
    public function getReservationhotel()
    {
        return $this->reservationhotel;
    }

    /**
     * Set cin
     *
     * @param integer $cin
     *
     * @return Coordonneesreservationhotel
     */
    public function setCin($cin)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return integer
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return Coordonneesreservationhotel
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Coordonneesreservationhotel
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }
}
