<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clients
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\ClientsRepository")
 */
class Clients
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="civ", type="string", length=10 , nullable=true)
     */
    private $civ;


    /**
     * @var string
     *
     * @ORM\Column(name="matricule_fiscal", type="string", length=255 , nullable=true)
     */
    private $mp;

    /**
     * @var string
     *
     * @ORM\Column(name="pname", type="string", length=50 , nullable=true)
     */
    private $pname;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50 , nullable=true)
     */
    private $name;

        /**
     * @var \Date
     *
     * @ORM\Column(name="datenaissance", type="date")
     */
    private $datenaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255 , nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=40 , nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="cin", type="string", length=40 , nullable=true)
     */
    private $cin;


    /**
     * @var string
     *
     * @ORM\Column(name="fid", type="string", length=40 , nullable=true)
     */
    private $fid;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255,nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=12,nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100,nullable=true)
     */
    private $ville;
    /**
     * @ORM\ManyToOne(targetEntity="Listpays", inversedBy="clients")
     * @ORM\JoinColumn(name="pays_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $pays;
    /**
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="Client", cascade={"remove"})
     */
    protected $reservation;


    /**
     * @var float
     *
     * @ORM\Column(name="point", type="float" , nullable=true)
     */
    private $point;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean" )
     */
    private $act;

     /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean" )
     */
    private $type;
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100,nullable=true)
     */
    private $password;
    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=100,nullable=true)
     */
    private $salt;
     /**
     * @var string
     *
     * @ORM\Column(name="typeclient", type="string", length=100,nullable=true)
     */
    private $typeclient;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reservation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->datenaissance=(new \DateTime());
        $this->type=0;
    }


    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set civ
     *
     * @param string $civ
     * @return Clients
     */
    public function setCiv($civ)
    {
        $this->civ = $civ;

        return $this;
    }

    /**
     * Get civ
     *
     * @return string
     */
    public function getCiv()
    {
        return $this->civ;
    }


    /**
     * Set mp
     *
     * @param string $mp
     * @return Clients
     */
    public function setMp($mp)
    {
        $this->mp = $mp;

        return $this;
    }

    /**
     * Get mp
     *
     * @return string
     */
    public function getMp()
    {
        return $this->mp;
    }


     /**
     * Get datenaissance
     *
     * @return DateTime
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }
    /**
     * Set datenaissance
     *
     * @param \DateTime $datenaissance
     * @return Clients
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Clients
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean
     */
    public function getAct()
    {
        return $this->act;
    }


    /**
     * Set type
     *
     * @param boolean $type
     * @return Clients
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pname
     *
     * @param string $pname
     * @return Clients
     */
    public function setPname($pname)
    {
        $this->pname = $pname;

        return $this;
    }

    /**
     * Get pname
     *
     * @return string
     */
    public function getPname()
    {
        return $this->pname;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Clients
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Clients
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Clients
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set cin
     *
     * @param string $cin
     * @return Clients
     */
    public function setCin($cin)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Clients
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Clients
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Clients
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param \Btob\HotelBundle\Entity\Listpays $pays
     * @return Clients
     */
    public function setPays(\Btob\HotelBundle\Entity\Listpays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Btob\HotelBundle\Entity\Listpays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Add reservation
     *
     * @param \Btob\HotelBundle\Entity\Reservation $reservation
     * @return Clients
     */
    public function addReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {
        $this->reservation[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \Btob\HotelBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {
        $this->reservation->removeElement($reservation);
    }

    /**
     * Get reservation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservation()
    {
        return $this->reservation;
    }


    /**
     * Set fid
     *
     * @param string $fid
     * @return Clients
     */
    public function setFid($fid)
    {
        $this->fid = $fid;

        return $this;
    }

    /**
     * Get fid
     *
     * @return string
     */
    public function getFid()
    {
        return $this->fid;
    }

    /**
     * Set point
     *
     * @param float $point
     * @return Clients
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return float
     */
    public function getPoint()
    {
        return $this->point;
    }


    /**
     * Set password
     *
     * @param string $password
     * @return Clients
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set salt
     *
     * @param string $salt
     * @return Clients
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }


    /**
     * Set typeclient
     *
     * @param string $typeclient
     *
     * @return Clients
     */
    public function setTypeclient($typeclient)
    {
        $this->typeclient = $typeclient;

        return $this;
    }

    /**
     * Get typeclient
     *
     * @return string
     */
    public function getTypeclient()
    {
        return $this->typeclient;
    }
   
    public function __toString() {
        if(is_null($this->name)) {
            return 'NULL';
        }
        return $this->name;
    }
}
