<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricearr
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PricearrRepository")
 */
class Pricearr {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelprice", inversedBy="Pricearr", cascade={"persist"})
     * @ORM\JoinColumn(name="pric_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelprice;

    /**
     * @ORM\ManyToOne(targetEntity="Hotelarrangement", inversedBy="Pricearr", cascade={"persist"})
     * @ORM\JoinColumn(name="hotarr_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $hotelarrangement;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="minstay", type="integer")
     */
    private $minstay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pers", type="boolean")
     */
    private $pers;
    
    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float")
     */
    private $marge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="persm", type="boolean")
     */
    private $persm;    

    public function __construct() {
        $this->pers = false;
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", length=11)
     */
    private $etat;


    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }
     /**
         * Set etat
         *
         * @param integer $etat
         * @return Hotelarrangement
         */
        public function setEtat($etat)
        {
            $this->etat = $etat;

            return $this;
        }







    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Pricearr
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set minstay
     *
     * @param integer $minstay
     * @return Pricearr
     */
    public function setMinstay($minstay)
    {
        $this->minstay = $minstay;

        return $this;
    }

    /**
     * Get minstay
     *
     * @return integer 
     */
    public function getMinstay()
    {
        return $this->minstay;
    }

    /**
     * Set pers
     *
     * @param boolean $pers
     * @return Pricearr
     */
    public function setPers($pers)
    {
        $this->pers = $pers;

        return $this;
    }

    /**
     * Get pers
     *
     * @return boolean 
     */
    public function getPers()
    {
        return $this->pers;
    }

    /**
     * Set hotelprice
     *
     * @param \Btob\HotelBundle\Entity\Hotelprice $hotelprice
     * @return Pricearr
     */
    public function setHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice = null)
    {
        $this->hotelprice = $hotelprice;

        return $this;
    }

    /**
     * Get hotelprice
     *
     * @return \Btob\HotelBundle\Entity\Hotelprice 
     */
    public function getHotelprice()
    {
        return $this->hotelprice;
    }

    /**
     * Set hotelarrangement
     *
     * @param \Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement
     * @return Pricearr
     */
    public function setHotelarrangement(\Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement = null)
    {
        $this->hotelarrangement = $hotelarrangement;

        return $this;
    }

    /**
     * Get hotelarrangement
     *
     * @return \Btob\HotelBundle\Entity\Hotelarrangement 
     */
    public function getHotelarrangement()
    {
        return $this->hotelarrangement;
    }
    
     
    /**
     * Set marge
     *
     * @param float $marge
     * @return Pricearr
     */
    public function setMarge($marge)
    {
        $this->marge = $marge;

        return $this;
    }

    /**
     * Get marge
     *
     * @return float
     */
    public function getMarge()
    {
        return $this->marge;
    }
    
    
    /**
     * Set persm
     *
     * @param boolean $persm
     * @return Pricearr
     */
    public function setPersm($persm)
    {
        $this->persm = $persm;

        return $this;
    }

    /**
     * Get persm
     *
     * @return boolean
     */
    public function getPersm()
    {
        return $this->persm;
    }
   
    
}
