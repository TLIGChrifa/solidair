<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Maisondhote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\MaisondhoteRepository")
 */
class Maisondhote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortdesc", type="string", length=255)
     */
    private $shortdesc;
      /**
     * @var text
     *
     * @ORM\Column(name="longdesc", type="text")
     */
    private $longdesc;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal")
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbchambres", type="integer")
     */
    private $nbchambres;
           /**
     * @ORM\ManyToOne(targetEntity="Pays", inversedBy="maisondhote")
     * @ORM\JoinColumn(name="pays_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $pays;

    /**
     * @ORM\ManyToOne(targetEntity="Ville", inversedBy="maisondhote")
     * @ORM\JoinColumn(name="ville_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $ville;
      /**
     * @ORM\OneToMany(targetEntity="Maisonhoteimg", mappedBy="maisondhote")
     */
    protected $Maisondhoteimg;
     
   /**
     * @ORM\OneToMany(targetEntity="Reservationmaisondhote", mappedBy="Maisondhote", cascade={"remove"})
     */
    private $reservationmaisondhote;

        /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime", nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datef", type="datetime", nullable=true)
     */
    private $datef;
     /**
    * @var string
    *
    * @ORM\Column(name="type", type="string")
    */
    private $type;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortdesc
     *
     * @param string $shortdesc
     *
     * @return Maisondhote
     */
    public function setShortdesc($shortdesc)
    {
        $this->shortdesc = $shortdesc;

        return $this;
    }

    /**
     * Get shortdesc
     *
     * @return string
     */
    public function getShortdesc()
    {
        return $this->shortdesc;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return Maisondhote
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nbchambres
     *
     * @param integer $nbchambres
     *
     * @return Maisondhote
     */
    public function setNbchambres($nbchambres)
    {
        $this->nbchambres = $nbchambres;

        return $this;
    }

    /**
     * Get nbchambres
     *
     * @return integer
     */
    public function getNbchambres()
    {
        return $this->nbchambres;
    }

    /**
     * Set pays
     *
     * @param \Btob\HotelBundle\Entity\Pays $pays
     *
     * @return Maisondhote
     */
    public function setPays(\Btob\HotelBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Btob\HotelBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param \Btob\HotelBundle\Entity\Ville $ville
     *
     * @return Maisondhote
     */
    public function setVille(\Btob\HotelBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \Btob\HotelBundle\Entity\Ville
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set longdesc
     *
     * @param string $longdesc
     *
     * @return Maisondhote
     */
    public function setLongdesc($longdesc)
    {
        $this->longdesc = $longdesc;

        return $this;
    }

    /**
     * Get longdesc
     *
     * @return string
     */
    public function getLongdesc()
    {
        return $this->longdesc;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Maisondhoteimg = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add maisondhoteimg
     *
     * @param \Btob\HotelBundle\Entity\Maisonhoteimg $maisondhoteimg
     *
     * @return Maisondhote
     */
    public function addMaisondhoteimg(\Btob\HotelBundle\Entity\Maisonhoteimg $maisondhoteimg)
    {
        $this->Maisondhoteimg[] = $maisondhoteimg;

        return $this;
    }

    /**
     * Remove maisondhoteimg
     *
     * @param \Btob\HotelBundle\Entity\Maisonhoteimg $maisondhoteimg
     */
    public function removeMaisondhoteimg(\Btob\HotelBundle\Entity\Maisonhoteimg $maisondhoteimg)
    {
        $this->Maisondhoteimg->removeElement($maisondhoteimg);
    }

    /**
     * Get maisondhoteimg
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaisondhoteimg()
    {
        return $this->Maisondhoteimg;
    }

    

    /**
     * Add reservationmaisondhote
     *
     * @param \Btob\HotelBundle\Entity\Reservationmaisondhote $reservationmaisondhote
     *
     * @return Maisondhote
     */
    public function addReservationmaisondhote(\Btob\HotelBundle\Entity\Reservationmaisondhote $reservationmaisondhote)
    {
        $this->reservationmaisondhote[] = $reservationmaisondhote;

        return $this;
    }

    /**
     * Remove reservationmaisondhote
     *
     * @param \Btob\HotelBundle\Entity\Reservationmaisondhote $reservationmaisondhote
     */
    public function removeReservationmaisondhote(\Btob\HotelBundle\Entity\Reservationmaisondhote $reservationmaisondhote)
    {
        $this->reservationmaisondhote->removeElement($reservationmaisondhote);
    }

    /**
     * Get reservationmaisondhote
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservationmaisondhote()
    {
        return $this->reservationmaisondhote;
    }
    public function __toString()
{
    return $this->shortdesc;
}

    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Maisondhote
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Maisondhote
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Maisondhote
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
