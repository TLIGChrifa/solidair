<?php


namespace Btob\HotelBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Devis
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\DevisRepository")
 */
class Devis

{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=255 , nullable=true)
     */

    private $num;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255 , nullable=true)
     */

    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="idm", type="integer" , nullable=true)
     */

    private $idm;	
    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float" , nullable=true)
     */

    private $montant;	
    /**
     * @var float
     *
     * @ORM\Column(name="montant_p", type="float" , nullable=true)
     */

    private $montant_p;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=255 , nullable=true)
     */

    private $client;


    /**
     * @ORM\ManyToOne(targetEntity="Clients", inversedBy="devis")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */

    private $client_id;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */

    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */

    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */

    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */

    private $dcr;
    
    /**
     * @ORM\ManyToOne(targetEntity="Facture", inversedBy="devis")
     * @ORM\JoinColumn(name="facture", referencedColumnName="id",onDelete="CASCADE")
     */
    private $facture;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string" , nullable=true)
     */

    private $observation;
    /**
     * Constructor
     */

    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }

    public function __toString()

    {

        return $this->num;

    }

    /**
     * Set idm
     *
     * @param integer $idm
     * @return Devis
     */
    public function setIdm($idm)
    {
        $this->idm = $idm;
        return $this;
    }

    /**
     * Get idm
     *
     * @return integer
     */
    public function getIdm()
    {
        return $this->idm;
    }	
	
    /**
     * Set num
     *
     * @param string $num
     * @return Devis
     */

    public function setNum($num)
    {
        $this->num = $num;
        return $this;
    }


    /**
     * Get num
     *
     * @return string
     */
    public function getNum()
    {
        return $this->num;
    }
	
    /**
     * Set code
     *
     * @param string $code
     * @return Devis
     */

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }


    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set montant
     *
     * @param float $montant
     * @return Devis
     */

    public function setMontant($montant)

    {

        $this->montant = $montant;


        return $this;

    }


    /**
     * Get montant
     *
     * @return float
     */

    public function getMontant()

    {

        return $this->montant;

    }
    
    /**
     * Set montant_p
     *
     * @param float $montant_p
     * @return Devis
     */

    public function setMontantP($montant_p)

    {

        $this->montant_p = $montant_p;


        return $this;

    }


    /**
     * Get montant_p
     *
     * @return float
     */

    public function getMontantP()

    {

        return $this->montant_p;

    }

    /**
     * Set client
     *
     * @param string $client
     * @return Devis
     */

    public function setClient($client)

    {

        $this->client = $client;


        return $this;

    }

     /**
     * Set client_id
     *
     * @param \Btob\HotelBundle\Entity\Clients $client_id
     * @return Devis
     */

    public function setClientId($client_id)

    {

        $this->client_id = $client_id;


        return $this;

    }


    /**
     * Get client
     *
     * @return string
     */

    public function getClient()

    {

        return $this->client;

    }

    /**
     * Get client_id
     *
     * @return \Btob\HotelBundle\Entity\Clients
     */

    public function getClientId()

    {

        return $this->client_id;

    }

    /**
     * Set type
     *
     * @param string $type
     * @return Devis
     */

    public function setType($type)

    {

        $this->type = $type;


        return $this;

    }


    /**
     * Get type
     *
     * @return string
     */

    public function getType()

    {

        return $this->type;

    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Devis
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Devis
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }


    /**
     * Get dcr
     *
     * @return \DateTime
     */

    public function getDcr()
    {
        return $this->dcr;

    }
    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Devis
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }


    /**
     * Get date
     *
     * @return \DateTime
     */

    public function getDate()
    {
        return $this->date;

    }


    /**
     * Set facture
     *
     * @param \Btob\HotelBundle\Entity\Facture $facture
     * @return Devis
     */
    public function setFacture(\Btob\HotelBundle\Entity\Facture $facture = null)
    {
        $this->facture = $facture;

        return $this;
    }


    /**
     * Get facture
     *
     * @return \Btob\HotelBundle\Entity\Facture
     */
    public function getFacture()
    {
        return $this->facture;
    }
    
    /**
     * Set observation
     *
     * @param string $observation
     * @return Devis
     */

    public function setObservation($observation)
    {
        $this->observation = $observation;
        return $this;
    }


    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }
}

