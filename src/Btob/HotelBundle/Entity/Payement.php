<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PayementPayement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PayementRepository")
 */
class Payement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var boolean
     *
     * @ORM\Column(name="actrib", type="boolean")
     */
    private $actrib;

    /**
     * @var string
     *
     * @ORM\Column(name="longdescrib", type="text" , nullable=true)
     */
    private $longdescrib;
    
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="actline", type="boolean")
     */
    private $actline;

    
    /**
     * @var boolean
     *
     * @ORM\Column(name="actagence", type="boolean")
     */
    private $actagence;
    
        /**
     * @var string
     *
     * @ORM\Column(name="longdescagence", type="text" , nullable=true)
     */
    private $longdescagence;
    /**
     * @var boolean
     *
     * @ORM\Column(name="actvers", type="boolean")
     */
    private $actvers;
    
        /**
     * @var string
     *
     * @ORM\Column(name="longdescvers", type="text" , nullable=true)
     */
    private $longdescvers;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="actmond", type="boolean")
     */
    private $actmond;   
    
        /**
     * @var string
     *
     * @ORM\Column(name="longdescmond", type="text" , nullable=true)
     */
    private $longdescmond;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

public  function  __toString(){
        return $this->longdescrib;
    }

    /**
     * Set longdescrib
     *
     * @param string $longdescrib
     * @return Payement
     */
    public function setLongdescrib($longdescrib)
    {
        $this->longdescrib = $longdescrib;

        return $this;
    }

    /**
     * Get longdescrib
     *
     * @return string 
     */
    public function getLongdescrib()
    {
        return $this->longdescrib;
    }

    
    /**
     * Set longdescagence
     *
     * @param string $longdescagence
     * @return Payement
     */
    public function setLongdescagence($longdescagence)
    {
        $this->longdescagence = $longdescagence;

        return $this;
    }

    /**
     * Get longdescagence
     *
     * @return string 
     */
    public function getLongdescagence()
    {
        return $this->longdescagence;
    }
        /**
     * Set longdescvers
     *
     * @param string $longdescvers
     * @return Payement
     */
    public function setLongdescvers($longdescvers)
    {
        $this->longdescvers = $longdescvers;

        return $this;
    }

    /**
     * Get longdescvers
     *
     * @return string 
     */
    public function getLongdescvers()
    {
        return $this->longdescvers;
    }
    
    /**
     * Set longdescmond
     *
     * @param string $longdescmond
     * @return Payement
     */
    public function setLongdescmond($longdescmond)
    {
        $this->longdescmond = $longdescmond;

        return $this;
    }

    /**
     * Get longdescmond
     *
     * @return string 
     */
    public function getLongdescmond()
    {
        return $this->longdescmond;
    }    
    
    /**
     * Set actrib
     *
     * @param boolean $actrib
     * @return Payement
     */
    public function setActrib($actrib)
    {
        $this->actrib = $actrib;

        return $this;
    }

    /**
     * Get actrib
     *
     * @return boolean 
     */
    public function getActrib()
    {
        return $this->actrib;
    }


    /**
     * Set actline
     *
     * @param boolean $actline
     * @return Payement
     */
    public function setActline($actline)
    {
        $this->actline = $actline;

        return $this;
    }

    /**
     * Get actline
     *
     * @return boolean
     */
    public function getActline()
    {
        return $this->actline;
    }


    /**
     * Set actagence
     *
     * @param boolean $actagence
     * @return Payement
     */
    public function setActagence($actagence)
    {
        $this->actagence = $actagence;

        return $this;
    }

    /**
     * Get actagence
     *
     * @return boolean
     */
    public function getActagence()
    {
        return $this->actagence;
    }
    
    /**
     * Set actvers
     *
     * @param boolean $actvers
     * @return Payement
     */
    public function setActvers($actvers)
    {
        $this->actvers = $actvers;

        return $this;
    }

    /**
     * Get actvers
     *
     * @return boolean 
     */
    public function getActvers()
    {
        return $this->actvers;
    }    
    /**
     * Set actmond
     *
     * @param boolean $actmond
     * @return Payement
     */
    public function setActmond($actmond)
    {
        $this->actmond = $actmond;

        return $this;
    }

    /**
     * Get actmond
     *
     * @return boolean 
     */
    public function getActmond()
    {
        return $this->actmond;
    }        
    
}
