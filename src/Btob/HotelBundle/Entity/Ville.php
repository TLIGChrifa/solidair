<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ville
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\VilleRepository")
 */
class Ville {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="act", type="boolean")
     */
    private $act;

    /**
     * @ORM\ManyToOne(targetEntity="Pays", inversedBy="ville")
     * @ORM\JoinColumn(name="pays_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $pays;
    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="ville", cascade={"remove"})
     */
    protected $hotel;

    /**
     * @ORM\OneToMany(targetEntity="Maisondhote", mappedBy="ville", cascade={"remove"})
     */
    protected $maisondhote;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hotel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Ville
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set act
     *
     * @param boolean $act
     * @return Ville
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return boolean 
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set pays
     *
     * @param \Btob\HotelBundle\Entity\Pays $pays
     * @return Ville
     */
    public function setPays(\Btob\HotelBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Btob\HotelBundle\Entity\Pays 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Add hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     * @return Ville
     */
    public function addHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel[] = $hotel;

        return $this;
    }

    /**
     * Remove hotel
     *
     * @param \Btob\HotelBundle\Entity\Hotel $hotel
     */
    public function removeHotel(\Btob\HotelBundle\Entity\Hotel $hotel)
    {
        $this->hotel->removeElement($hotel);
    }

    /**
     * Get hotel
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotel()
    {
        return $this->hotel;
    }
    public  function  __toString(){
        return $this->name;
    }

    /**
     * Add maisonhote
     *
     * @param \Btob\HotelBundle\Entity\maisonhote $maisonhote
     *
     * @return Ville
     */
    public function addMaisonhote(\Btob\HotelBundle\Entity\maisonhote $maisonhote)
    {
        $this->maisonhote[] = $maisonhote;

        return $this;
    }

    /**
     * Remove maisonhote
     *
     * @param \Btob\HotelBundle\Entity\maisonhote $maisonhote
     */
    public function removeMaisonhote(\Btob\HotelBundle\Entity\maisonhote $maisonhote)
    {
        $this->maisonhote->removeElement($maisonhote);
    }

    /**
     * Get maisonhote
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaisonhote()
    {
        return $this->maisonhote;
    }

    /**
     * Add maisondhote
     *
     * @param \Btob\HotelBundle\Entity\Maisondhote $maisondhote
     *
     * @return Ville
     */
    public function addMaisondhote(\Btob\HotelBundle\Entity\Maisondhote $maisondhote)
    {
        $this->maisondhote[] = $maisondhote;

        return $this;
    }

    /**
     * Remove maisondhote
     *
     * @param \Btob\HotelBundle\Entity\Maisondhote $maisondhote
     */
    public function removeMaisondhote(\Btob\HotelBundle\Entity\Maisondhote $maisondhote)
    {
        $this->maisondhote->removeElement($maisondhote);
    }

    /**
     * Get maisondhote
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaisondhote()
    {
        return $this->maisondhote;
    }
}
