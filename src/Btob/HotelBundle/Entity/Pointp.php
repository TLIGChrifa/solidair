<?php

namespace Btob\HotelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pointp
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\HotelBundle\Entity\PointpRepository")
 */
class Pointp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var float
     *
     * @ORM\Column(name="ptmin", type="float")
     */
    private $ptmin;
    
    /**
     * @var float
     *
     * @ORM\Column(name="ptmax", type="float")
     */
    private $ptmax;      
    
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;   
    
    
    public function __toString(){
        return $this->ptmin;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set ptmin
     *
     * @param float $ptmin
     * @return Pointp
     */
    public function setPtmin($ptmin)
    {
        $this->ptmin = $ptmin;

        return $this;
    }

    /**
     * Get ptmin
     *
     * @return float 
     */
    public function getPtmin()
    {
        return $this->ptmin;
    }
    
    /**
     * Set ptmax
     *
     * @param float $ptmax
     * @return Pointp
     */
    public function setPtmax($ptmax)
    {
        $this->ptmax = $ptmax;

        return $this;
    }

    /**
     * Get ptmax
     *
     * @return float 
     */
    public function getPtmax()
    {
        return $this->ptmax;
    }
    
    /**
     * Set prix
     *
     * @param float $prix
     * @return Pointp
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float 
     */
    public function getPrix()
    {
        return $this->prix;
    }
        
    
    
}
