<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationHotelreservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
      //  ->add('datedebut', 'date' ,['label' => "A partir du:", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
        //->add('datefin', 'date' ,['label' => "Jusqu'au :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
        ->add('nbchambres', 'choice', array('label' => false,
        'choices' => array('0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10',
        '11' => '11','12' => '12','13' => '13','14' => '14','15' => '15','16' => '16','17' => '17','18' => '18','19' => '19','20' => '20',
        ), 'required' => true, 'multiple' => false,
    ))
    // ->add('typepayement', 'choice', array('label' => false,
    // 'choices' => array('Espèce' => 'Espèce','Par chèque' => 'Par chèque',
    // ), 'required' => true, 'multiple' => false,
    //  ))
      ->add('hotel')
    //   ))
    //   ->add('numcheque', 'text', array('label' => false, 'required' => false,
    //   ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\ReservationHotel'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_reservationhotel';
    }
}
