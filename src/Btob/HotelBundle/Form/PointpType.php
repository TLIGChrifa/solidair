<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PointpType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('ptmin',NULL, array('required' => true, 'label' => "Point Transformation Min"))//,'data' => '0'
           ->add('ptmax',NULL, array('required' => true, 'label' => "Point Transformation Max"))//,'data' => '0'
          ->add('prix',NULL, array('required' => true, 'label' => "Prix"))//,'data' => '0'
                ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Pointp'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_clients';
    }
}
