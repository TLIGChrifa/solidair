<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoordonneesmaisondhoteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        //   ->add('datedebut', DateType::class ,['label' => "à partir du:", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
        // ->add('datefin',  DateType::class ,['label' => "Jusqu'au:", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])

        ->add('nom')->add('prenom')->add('mobile',IntegerType::class)->add('email',EmailType::class)->add('adultes', IntegerType::class)->add('adulte',IntegerType::class)->add('demande', TextType::class, array('label' => "", 'required' => true))       ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Coordonneesmaisondhote'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'btob_hotelbundle_coordonneesmaisondhote';
    }


}
