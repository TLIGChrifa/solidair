<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dated', 'date', array('widget' => 'single_text', 'label' => "Date", 'format' => 'dd/MM/yyyy'))
                ->add('name', 'text', array('label' => "Nom de l'événement", 'required' => true))
                ->add('pricead', null, array('label' => "Prix d'adulte", 'required' => true))
                ->add('pricech', null, array('label' => "Prix d'enfant", 'required' => true))
                ->add('pers', null, array('label' => "Porcentage?", 'required' => false))
                ->add('oblig', null, array('label' => "Obligatoire?", 'required' => false))
                ->add('marge', null, array('label' => "Marge", 'required' => true))
                ->add('persm', null, array('label' => "Porcentage?", 'required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Events'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'btob_hotelbundle_events';
    }

}
