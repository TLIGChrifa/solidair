<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PayementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('actrib', null, array('label' => "Virement bancaire?", 'required' => false))
            ->add('actline', null, array('label' => "Paiement en ligne?", 'required' => false))
            ->add('actagence', null, array('label' => "Paiement à l'agence?", 'required' => false))
            ->add('actvers', null, array('label' => "Versement", 'required' => false))
            ->add('actmond', null, array('label' => "Mondat", 'required' => false)) 
            ->add('longdescrib', null, array('label' => "Description Virement bancaire", 'required' => false))
            
             ->add('longdescagence', null, array('label' => "Description Paiement à l'agence", 'required' => false))
            ->add('longdescvers', null, array('label' => "Description Versement", 'required' => false))
            ->add('longdescmond', null, array('label' => "Description Mondat", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Payement'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_payement';
    }
}
