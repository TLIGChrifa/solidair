<?php



namespace Btob\HotelBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class DevisType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder
            ->add('num', 'text', array('label' => "", 'required' => true))
            ->add('montant', NULL, array('required' => true, 'label' => ""))//,'data' => '0'
             ->add('montant_p', NULL, array('required' => true, 'label' => ""))//,'data' => '0'
            ->add('client', NULL, array('required' => true, 'label' => ""))//,'data' => '0'
            ->add('type', NULL, array('required' => true, 'label' => ""))//,'data' => '0'
            ->add('etat', NULL, array('required' => true, 'label' => ""))//,'data' => '0'

        ;

    }



    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Btob\HotelBundle\Entity\Devis'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'btob_hotelbundle_devis';

    }

}

