<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BilletriecoordonnesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cIN',IntegerType::class,['label'=> false])
        ->add('mobile',IntegerType::class,['label'=> false])
        ->add('adresee',TextType::class,['label'=> false])
        // ->add('civilite',TextType::class,['label'=> false])
         ->add('nom',TextType::class,['label'=> false])
         ->add('prenom',TextType::class,['label'=> false])
         ->add('email',EmailType::class)
         ->add('codepostal',IntegerType::class,['label'=> false])
         ->add('ville',TextType::class,['label'=> false])
    //      ->add('villedepart', ChoiceType::class, array('label'=>false,
    //      'choices' => array(
    //      'ABIDJAN' => 'ABIDJAN',
    //      'AMMAN' => 'AMMAN',
    //      'ALGER' => 'ALGER',
    //      'AMSTERDAM' => 'AMSTERDAM',
    //      'ATHENES' => 'ATHENES','BAHREIN' => 'BAHREIN','BALE' => 'BALE','BAMAKO' => 'BAMAKO','BARCELONE' => 'BARCELONE','BELGRADE' => 'BELGRADE',
        
    //     )))
    //     ->add('villearrivee', ChoiceType::class, array('label'=>false,
    //     'choices' => array(
    //     'ABIDJAN' => 'ABIDJAN',
    //     'AMMAN' => 'AMMAN',
    //     'ALGER' => 'ALGER',
    //     'AMSTERDAM' => 'AMSTERDAM',
    //     'ATHENES' => 'ATHENES','BAHREIN' => 'BAHREIN','BALE' => 'BALE','BAMAKO' => 'BAMAKO','BARCELONE' => 'BARCELONE','BELGRADE' => 'BELGRADE',
       
    //    )))

       
         //->add('villearrivee',TextType::class,['label'=> false])
        // ->add('datedepart')
        // ->add('datearrivee')
         //->add('companie')->add('classe')
         ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Billetriecoordonnes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'btob_hotelbundle_billetriecoordonnes';
    }


}
