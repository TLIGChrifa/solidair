<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationmaisondhotepayerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('typepayement', 'choice', array('label' => false,
        'choices' => array('Espèce' => 'Espèce','Par chèque' => 'Par chèque',
        ), 'required' => true, 'multiple' => false,
         ))
         ->add('montantpaye', 'text', array('label' => "Montant à payer : *", 'required' => true,
          ))
          ->add('numcheque', 'text', array('label' => false, 'required' => false,
          ))
            ;
        }
        
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Reservationmaisondhote'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_reservationmaisondhote';
    }
}
