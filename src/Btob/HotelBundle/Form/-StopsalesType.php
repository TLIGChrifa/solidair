<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StopsalesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dated', 'date', array('widget' => 'single_text', 'label' => "Date debut", 'format' => 'dd/MM/yyyy'))
                ->add('dates', 'date', array('widget' => 'single_text', 'label' => "Date debut", 'format' => 'dd/MM/yyyy'))
                ->add('act', null, array('label' => "Active?", 'required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Stopsales'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'btob_hotelbundle_stopsales';
    }

}
