<?php



namespace Btob\HotelBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class FactType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder
            ->add('name', 'text', array('label' => "CODE TVA", 'required' => true))
            ->add('tva', NULL, array('required' => true, 'label' => "TVA(en Pourcentage)"))//,'data' => '0'
            ->add('timbre', NULL, array('required' => true, 'label' => "Timbre Fiscal"))//,'data' => '0'
            ->add('frais', NULL, array('required' => true, 'label' => "Frais de dossier"))//,'data' => '0'

        ;

    }



    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Btob\HotelBundle\Entity\Fact'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'btob_hotelbundle_fact';

    }

}

