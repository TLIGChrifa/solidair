<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AmgType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array('label' => "Nom d'aménagement", 'required' => true))
                ->add('act', null, array('label' => "Active?", 'required' => false))
                //->add('file', null, array('label' => "Active?", 'required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Amg'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'btob_hotelbundle_amg';
    }

}
