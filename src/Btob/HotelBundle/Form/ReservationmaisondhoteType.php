<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationmaisondhoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('datedebut', 'date' ,['label' => "A partir du:", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
        ->add('datefin', 'date' ,['label' => "Jusqu'au:", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
        ->add('nbadultes',Null, array('label' => "nbre des adultes"))
        ->add('nbenfants',Null, array('label' => "nbre des enfants"))
        ->add('nbnuits', 'choice', array('label' => "nbre de nuits",
            'choices' => array('0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10',
            '11' => '11','12' => '12','13' => '13','14' => '14','15' => '15',
            ), 'required' => true, 'multiple' => false,
        ))
        ->add('nom','text', array('label' => "Nom"))
        ->add('prenom','text', array('label' => "Prénom"))
        ->add('mobile',Null, array('label' => "Num de téléphone"))
        ->add('Maisondhote',Null, array('label' => "Maison d'hôte"))


        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Reservationmaisondhote'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_reservationmaisondhote';
    }
}
