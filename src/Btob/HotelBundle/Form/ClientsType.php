<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ClientsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civ', 'choice', array(
                'choices' => array('Mr.' => 'Mr.', 'Mme' => 'Mme', 'Mlle' => 'Mlle'),
                'required' => false,
                'empty_value' => 'Choisissez ',
                'label' => 'Civilité * :',
            ))
            ->add('pname', 'text', array('label' => "Prénom * :", 'required' => false))
            ->add('name', 'text', array('label' => "Nom * :", 'required' => false))
            ->add('email', 'email', array('label' => "E-mail * :", 'required' => true))
            // ->add('password', 'password', array('label' => "Mot de passe * :", 'required' => false))
            ->add('tel', 'text', array('label' => "Téléphone * :", 'required' => false))
            ->add('cin', 'text', array('label' => "C.I.N / Passeport * :", 'required' => false))
            ->add('adresse', 'textarea', array('label' => "Adresse :", 'required' => false))
            ->add('cp', 'text', array('label' => "Code postal :", 'required' => false))
            ->add('ville', 'text', array('label' => "Nom de ville :", 'required' => false))
            ->add('pays', 'entity', array(
                'class' => 'BtobHotelBundle:Listpays',
                'required' => false,
                'empty_value' => false,
                'label' => "Pays :",
            ))
            // ->add('typeclient', 'choice', array(
            //     'choices' => array('Client passager' => 'Client passager', 'Client fidèle' => 'Client fidèle'),
            //     'required' => false,
            //     'empty_value' => 'Choisissez ',
            //     'label' => 'Type du client * :',
            // ))
            ->add('fid', 'text', array('label' => "Num carte Fidelité", 'required' => false))
            ->add('point', 'text', array('label' => "Nombre de Point", 'required' => false))
            ->add('datenaissance', 'date', ['label' => "Date de Naissance :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd']);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Clients'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_clients';
    }
}
