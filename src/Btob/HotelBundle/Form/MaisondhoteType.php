<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MaisondhoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('longdesc',  'ckeditor', array('label' =>'Description détaillée', 'required' => false))
           ->add('shortdesc', 'text', array('label' => "Description", 'required' => false))
           ->add('prix')
           ->add('nbchambres', Null, array('label' => "Nbre de chambre", 'required' => false))
    //        ->add('nbimages', 'choice', array('label' => "Nbre des images",
    //        'choices' => array('0' => '0','1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10',
    //       ), 'required' => true, 'multiple' => false,
    //    ))
           ->add('pays')
           ->add('ville')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Maisondhote'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_maisondhote';
    }
}
