<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dcr')
            ->add('dated')
            ->add('datef')
            ->add('total')
            ->add('totalint')
            ->add('totalbase')
            ->add('nbnuit')
            ->add('namead')
            ->add('ageenfant')
            ->add('ageadult')
            ->add('nameenf')
            ->add('remarque')
            ->add('remarques')
            ->add('etat')
            ->add('avance')
            ->add('del')
            ->add('resultatfinal')
            ->add('numautoris')
            ->add('ref')
            ->add('hotel')
            ->add('client')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Reservation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_hotelbundle_reservation';
    }
}
