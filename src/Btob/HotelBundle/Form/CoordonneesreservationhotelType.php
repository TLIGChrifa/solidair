<?php

namespace Btob\HotelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CoordonneesreservationhotelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')->add('prenom')->add('mobile')->add('email')->add('adulte')->add('adultes',IntegerType::class)->add('demandes') ->add('pays') ->add('cin',IntegerType::class, array('label' => "C.I.N / Passeport * :", 'required' => false))  ->add('civilite', ChoiceType::class, array('label' => "Civilité",
        'choices' => array('Mr' => 'Mr','Mme' => 'Mme','Mlle' => 'Mlle',
        ), 'required' => true, 'multiple' => false,
    ))        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\HotelBundle\Entity\Coordonneesreservationhotel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'btob_hotelbundle_coordonneesreservationhotel';
    }


}
