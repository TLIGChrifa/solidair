<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Modepaiement;
use Btob\HotelBundle\Form\ModepaiementType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class ModepaiementController extends Controller {

    public function indexAction() {
        $modepaiement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Modepaiement')
                ->findAll();
        return $this->render('BtobHotelBundle:Modepaiement:index.html.twig', array('modepaiement' => $modepaiement));
    }


    public function addAction() {
        $modepaiement = new Modepaiement();
        $form = $this->createForm(new ModepaiementType(), $modepaiement);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($modepaiement);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter une mode de paiement");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Modepaiement:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $modepaiement = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Modepaiement')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ModepaiementType(), $modepaiement);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier une mode de paiement n° " . $modepaiement->getId()." : " . $modepaiement->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Modepaiement:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Modepaiement $modepaiement) {
        $em = $this->getDoctrine()->getManager();

        if (!$modepaiement) {
            throw new NotFoundHttpException("Mode non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete une mode de paiement n° " . $modepaiement->getId()." : " . $modepaiement->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($modepaiement);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
    }
    
    public function activateAction(Modepaiement $modepaiement) {

        $em = $this->getDoctrine()->getManager();
        if (!$modepaiement) {
            throw new NotFoundHttpException("Mode non trouvée");
        }else{
	        if ($modepaiement->getAct()==true) {
	            $modepaiement->setAct(false);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Desactiver une mode de paiement n° " . $modepaiement->getId()." : " . $modepaiement->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
	            $em->flush();
	        }else{
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Activer une mode de paiement n° " . $modepaiement->getId()." : " . $modepaiement->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
	            $modepaiement->setAct(true);
	            $em->flush();
	        }
        }
        
        
        
        return $this->redirect($this->generateUrl('btob_modepaiement_homepage'));
    }
    

}