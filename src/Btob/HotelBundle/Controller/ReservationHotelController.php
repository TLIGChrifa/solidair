<?php

namespace Btob\HotelBundle\Controller;
use Btob\HotelBundle\Entity\ReservationHotel;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Form\ReservationType;

use Btob\HotelBundle\Form\ReservationHotelType;
use Btob\HotelBundle\Form\ReservationHotelpayerType;

use Btob\HotelBundle\Entity\chambre;
use DateInterval;
use DatePeriod;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ReservationHotelController extends Controller
{
  
public function addreservationAction($id, $marcheid) {
        //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find(1);
        $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->find($marcheid);
        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($id);
//*************** to show the real lists ************************* 
$price = array();
$date = array();
foreach ($hotel->getHotelprice() as $value) {
    if ($value->getMarcher()->getId() == $marche->getId()) {
        
        $date[] = $value->getDcr();
        $price[] = $value;
    }
}
$arraydater = array();
 $datert = new DateTime();
// for ($i = 1; $i < end($price)->getRetro(); $i++) {
//      $datert->modify('+'.$i.' day');
//      $arraydater[] = $datert->format('m/d/Y');
// }
//var_dump($price).die;
$dr= $datert->modify('+'.end($price)->getRetro().' day');

$period = new DatePeriod(
    new DateTime(),
    new DateInterval('P1D'),
    new DateTime($dr->format('m/d/Y'))
);
//var_dump($period).die;
foreach ($period as $key => $value) {
   $arraydater[] = $value->format('m/d/Y') ;      
}
//var_dump($arraydater).die;
//var_dump(end($price)->getRetro()).die;
        // foreach(end($price) as $p){
        //       $p->getPrice();
             
        // }
        $allpriceroom = array();
        $allpricesupplement = array();
        $allpricechild = array();
        $allpricearr = array();
        $allhotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findBy(array('hotel' => $hotel));
        $allroom = array();
        $allqterooms = array();
        $allsupplement = array();
        $allchild = array();
        $allarrangement = array();
        if (!empty($allhotelprice)){
            $allpriceroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricesupplement = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricechild = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricechild')->findBy(array('hotelprice' => $allhotelprice[0]));
            $allpricearr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findBy(array('hotelprice' => $allhotelprice[0]));
            $i = 0;
            foreach ($allpriceroom as $onepriceroom) {
                $allroom[$i] = $onepriceroom->getRoom();
                $allqterooms[$i] = $onepriceroom->getQte();
                $allids[$i] = $onepriceroom->getid();
                $onepriceroom->setQte($onepriceroom->getQte()-1);
                $i = $i + 1;
            }
            //var_dump($allhotelprice[0]).die;  
            $i = 0;
            foreach ($allpricesupplement as $onepricesupplement) {
                $allsupplement[$i] = $onepricesupplement->getHotelsupplement()->getSupplement();
                $i = $i + 1;
            }
            $i = 0;
            foreach ($allpricechild as $onepricechild) {
                if (! in_array($onepricechild->getHotelchild()->getChild(), $allchild)){
                    $allchild[$i] = $onepricechild->getHotelchild()->getChild();
                    $i = $i + 1;
                }
            }
            $i = 0;
            foreach ($allpricearr as $onepricearr) {
                $allarrangement[$i] = $onepricearr->getHotelarrangement()->getArrangement();
                $i = $i + 1;
            }
           
        }
        
        $request = $this->get('request');

        
        $Reservation = new ReservationHotel();
        $form = $this->createForm(new ReservationHotelType(), $Reservation);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

               $form->bind($request);
            //echo "<pre>";print_r($page);exit;
           // if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Reservation->setHotel($hotel);
                $Reservation->setHotel($hotel);
              //  $Reservation->setUser($this->get('security.context')->getToken()->getUser());
                $datedebut = new \DateTime($request->request->get("textdatedebut"));
                $Reservation->setDatedebut($datedebut);
                $datefin = new \DateTime($request->request->get("textdatefin"));
               // dump($datefin).die;
                $Reservation->setDatefin($datefin);
                $em->persist($Reservation);    
                $em->flush();
                $Pricearrangement = array();
                foreach(end($price)->getPricearr() as $p){
                   $Pricearrangement[] = $p->getPrice();
                   $Pers[] = $p->getPers();
                   $Marge[] = $p->getMarge();
                   $Persm[] = $p->getPersm();
                }
               // var_dump($request->request->get("textdatedebut"),$request->request->get("textdatefin")).die;
                $idreservation = $Reservation->getId();
                $timestamp = strtotime($Reservation->getDatedebut()->format('Y-m-d'));

                $day = date('l', $timestamp);
               // $total = end($price)->getPrice();
             if(!in_array($day,["Sunday","Saturday"])){
                     $total = end($price)->getPrice();
                 }else{
                     $total = end($price)->getPricew();
                 }
    
                $arraytypechambre = array();
                $arraynamechambre = array();
                $arrayadultes = array();
                $arrayenfants = array();
                $arrayarrangement = array();
                // $arrayages= array();
                for ($i = 1; $i <= $Reservation->getNbchambres(); $i++) {
                    $arrayages = array();
                    $arraytypechambre[] = $request->request->get("type".$i);
                    $arraynamechambre[] = $request->request->get("hiddenselect".$i);
                    $arrayadultes[] = $request->request->get("adultes".$i);
                    $arrayenfants[] = $request->request->get("enfants".$i);
                    for($j=1; $j<= $request->request->get("enfants".$i); $j++){
                        $arrayages[] = $request->request->get("ages".$i."_".$j);
                    }
                  
                    $arrayarrangement[] = $request->request->get("logement".$i);
                    
                       if(isset($Pers[1]) and $Pers[1] == 1){
                           if(in_array("Logement Petit Déjeuner",$arrayarrangement)){
                             $total = $total + ($total*$Pricearrangement[1])/100;  
                             if($Persm[1] == 1){ 
                                $total = $total + $total*$Marge[1]/100;  
                            }else{
                                $total = $total + $Marge[1];  
                            }
                           }
                           
                           
                       }else{
                        if(in_array("Logement Petit Déjeuner",$arrayarrangement)){
                            if(isset($Pricearrangement[1]))
                            $total = $total + $Pricearrangement[1];
                            if(isset($Persm[1]) and $Persm[1] == 1){ 
                                $total = $total + $total*$Marge[1]/100;  
                            }else{
                                if(isset($Marge[1]))
                                $total = $total + $Marge[1];  
                            }
                           }
                       }
                        
                       if(isset($Pers[2]) and $Pers[2] == 1){
                        if(in_array("Logement Simple",$arrayarrangement)){
                            $total = $total + ($total * $Pricearrangement[2])/100;
                          //  var_dump($total).die;
                          if($Persm[2] == 1){ 
                            $total = $total + $total*$Marge[2]/100;  
                        }else{
                            $total = $total + $Marge[2];  
                        }
                        } 
                      }else{
                        if(in_array("Logement Simple",$arrayarrangement)){
                            $total = $total + $Pricearrangement[2];
                            if($Persm[2] == 1){ 
                                $total = $total + $total*$Marge[2]/100;  
                            }else{
                                $total = $total + $Marge[2];  
                            }
                          //  var_dump($total).die;
                        } 
                       }
                       if($Pers[0] == 1){
                        if(in_array("Demi Pension",$arrayarrangement)){
                            $total = $total + $total*$Pricearrangement[0]/100;
                            if($Persm[0] == 1){ 
                                $total = $total + $total*$Marge[0]/100;  
                            }else{
                                $total = $total + $Marge[0];  
                            }
                        }
                    }else{
                        if(in_array("Demi Pension",$arrayarrangement)){
                            $total = $total + $Pricearrangement[0];
                            if($Persm[0] == 1){ 
                                $total = $total + $total*$Marge[0]/100;  
                            }else{
                                $total = $total + $Marge[0];  
                            }
                        }   
                    }
                
                    $roomsinarray = array();
                    $chambre = new chambre();
                    
                   // var_dump($arraytypechambre).die;
                    $chambre->setAgeenfant($arrayages);
                   
                   
                    //var_dump($roomsinarray).die;
                    foreach($arraytypechambre as $ch){
                        $chambre->setType($ch);
                      //  $roomsinarray[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->findBy(array('capacity' => $ch));

                    }
                 //   var_dump($roomsinarray).die;
                    // foreach($allroom as $r){
                    //     if(in_array($r->getCapacity(),$arraytypechambre)) {
                    //     // var_dump($r->getId()).die;
                    //      //$idroom = $r->getId();
                    //         $roomchoice = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('room' => $r->getId(),'hotelprice' => $allhotelprice[0]));
                    //      //   var_dump($roomchoice[0]->getQte()).die;
                    //         $roomchoice[0]->setQte($roomchoice[0]->getQte()- 1);
                    //      //    foreach($roomchoice as $rm){
                    //      //        $rm->setQte($rm->getQte()-1);
                    //      //    }
                       
                    //      // foreach ($allpriceroom as $onepriceroom) {
                    //      //     $onepriceroom->setQte($onepriceroom->getQte()-1);
                    //      // }
                    //     }
                    //      }
                    
                    foreach($arrayadultes as $ad)
                    $chambre->setNbadultes($ad);
                    foreach($arrayenfants as $en)
                    $chambre->setNbenfants($en);
                    foreach($arrayarrangement as $arr)
                    $chambre->setArrangement($arr);
                    $chambre->setReservationhotel($Reservation);
                    $em->persist($chambre);
                    $em->flush();
                }
                $Reservation->setTotal($total);
               // var_dump($arrayages).die;
               foreach($arraynamechambre as $namech){
                $roomsinarray[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Room')->findBy(array('name' => $namech));
                }
                $roomchoice = array();
                foreach($roomsinarray as $roo){
                $roomchoice[] = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findBy(array('room' => $roo,'hotelprice' => $allhotelprice[0]));

                }
                foreach($roomchoice as $rm){
                   foreach($rm as $ri)
                    $ri->setQte($ri->getQte() - 1);
                }
             //  var_dump($roomsinarray).die;
                $em->flush();
             //  var_dump($total).die;
                
                //var_dump($arraytypechambre,$arrayadultes,$arrayenfants,$arrayarrangement).die;
            

               
                return $this->redirect($this->generateUrl('btob_reservation_hotel_payer', array("id" => $Reservation->getId())));

              //  return $this->redirect($this->generateUrl('btob_hotel_homepage', array("hotelid" => $id)));
          //  } else {
             //   echo $form->getErrors();
         //   }
         
        }
        return $this->render('BtobHotelBundle:ReservationHotel:addreservation.html.twig', array('form' =>$form->createView(),"hotel" => $hotel,'allarrangement' => $allarrangement,'datef'=>end($price)->getDates()->format('m/d/Y'),'arr'=>$arraydater,'retro'=>$datert->format('m/d/Y')));
    }



    
    public function deletereservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:ReservationHotel')
            ->find($id);

        $em->remove($reservation);
        $em->flush();
        $this->addFlash("success", "La réservation à été supprimée avec succès ..");

        return $this->redirect($this->generateUrl('btob_reservationback_homepage'));
    }
    public function payerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:ReservationHotel')
            ->find($id);
            $chambre =  $em->createQueryBuilder()
            ->select('c')
            ->from('BtobHotelBundle:Chambre','c')
            ->where('c.reservationhotel = :reservation')
            ->setParameter('reservation', $reservation)
            ->getQuery()
            ->getResult();
            $form = $this->createForm(new ReservationHotelpayerType(), $reservation);
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('btob_reservationback_homepage'));

            }
        
       

       return $this->render('BtobHotelBundle:ReservationHotel:payer.html.twig', array('form' =>$form->createView(),'reservation'=>$reservation,'chambres' => $chambre));
    }
    public function generervoucherAction($id){

        $em = $this->getDoctrine()->getManager();
      
       $reservation = $this->getDoctrine()
           ->getRepository('BtobHotelBundle:ReservationHotel')
           ->find($id);
        $chambre =  $em->createQueryBuilder()
        ->select('c')
        ->from('BtobHotelBundle:Chambre','c')
        ->where('c.reservationhotel = :reservation')
        ->setParameter('reservation', $reservation)
        ->getQuery()
        ->getResult();
        // var_dump($chambre).die;
           $pdf = $this->get('white_october.tcpdf')->create();
           // set document information
           $pdf->SetCreator(PDF_CREATOR);
           $pdf->SetAuthor('');
           $pdf->SetTitle('');
           $pdf->SetSubject('');
           $pdf->SetKeywords('');
   
           // remove default header/footer
           $pdf->setPrintHeader(false);
           $pdf->setPrintFooter(false);
   
           // set default monospaced font
           $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
   
           // set margins
           $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
   
           // set auto page breaks
           $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
   
           $pdf->SetFont('helvetica', '', 10, '', true);
   
           $pdf->AddPage();
           
           $html = $this->renderView('BtobHotelBundle:ReservationHotel:pdfvoucher.html.twig', array('entry' => $reservation,'chambres' => $chambre));
           $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
           $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           $pdf->writeHTML($html);
           $pdf->Image("images/footerrustica.PNG", 0, 246.56, 210, '', 'PNG', '', 'T', false, 500, '', false, false, 0, false, false, false);

        //   $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
        //   $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
           //$pdf->Output('/pnv.pdf', 'F');
           $nompdf = 'voucher_.pdf';
           $pdf->Output($nompdf);
           
           return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
           exit;
   }
   public function genererdevisAction($id){

    $em = $this->getDoctrine()->getManager();
  
   $reservation = $this->getDoctrine()
       ->getRepository('BtobHotelBundle:ReservationHotel')
       ->find($id);
    $chambre =  $em->createQueryBuilder()
    ->select('c')
    ->from('BtobHotelBundle:Chambre','c')
    ->where('c.reservationhotel = :reservation')
    ->setParameter('reservation', $reservation)
    ->getQuery()
    ->getResult();
    // var_dump($chambre).die;
       $pdf = $this->get('white_october.tcpdf')->create();
       // set document information
       $pdf->SetCreator(PDF_CREATOR);
       $pdf->SetAuthor('');
       $pdf->SetTitle('');
       $pdf->SetSubject('');
       $pdf->SetKeywords('');

       // remove default header/footer
       $pdf->setPrintHeader(false);
       $pdf->setPrintFooter(false);

       // set default monospaced font
       $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

       // set margins
       $pdf->SetMargins(PDF_MARGIN_LEFT,18, PDF_MARGIN_RIGHT,0);
       //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

       // set auto page breaks
       $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

       $pdf->SetFont('helvetica', '', 10, '', true);

       $pdf->AddPage();
       
       $html = $this->renderView('BtobHotelBundle:ReservationHotel:devis.html.twig', array('entry' => $reservation,'chambres' => $chambre));
       $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
       $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
       //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
       $pdf->writeHTML($html);
       $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
       $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
       //$pdf->Output('/pnv.pdf', 'F');
       $nompdf = 'voucher_.pdf';
       $pdf->Output($nompdf);
       
       return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
       exit;
}

public function addreservationhotelAction() {
    //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find(1);
  //  $marche = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findAll();
  //  $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findAll();
     

    $marcher = $this->getDoctrine()->getRepository("BtobHotelBundle:Marcher")->findBy(array('act' => 1));
    $hotels = $this->getDoctrine()
        ->getRepository('BtobHotelBundle:Hotel')
        ->findAll();
     

$periodevalide = array();
foreach($hotels as $h){
foreach($marcher as $m){
foreach ($h->getHotelprice() as $value) {
   // var_dump($value->getPrice()).die;
if ($value->getMarcher()->getId() == $m->getId()) {
    if(($value->getDates() >=  new \DateTime('now')) and (!is_null($value->getPricew())) and !is_null(!is_null($value->getPrice()))){
        $hotelprice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->find($value->getId());
         $periodevalide[] = $hotelprice->getHotel();
    }

}
}
}

}
//var_dump($periodevalide).die;
    $request = $this->get('request');
    if ($request->getMethod() == 'POST') {

         
       
            $em = $this->getDoctrine()->getManager();

        //var_dump($request->request->get('hotel')).die;
            return $this->redirect($this->generateUrl('btob_reservation_hotel_add', array('id'=>$request->request->get('hotel'),'marcheid' =>$request->request->get('marcher'))));

       }

    return $this->render('BtobHotelBundle:ReservationHotel:addreshotel.html.twig', array('hotel'=>$periodevalide,'marche' => $marcher));


}

public function hotelxmlAction(){


    return $this->render('BtobHotelBundle:ReservationHotel:hotelxml.html.twig');
}



}
