<?php

namespace Btob\HotelBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Pointp;
use Btob\HotelBundle\Form\PointpType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class PointpController extends Controller
{
    /**
     * Lists all Pointp entities.
     *
     */

    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobHotelBundle:Pointp")->findAll();
        return $this->render('BtobHotelBundle:Pointp:index.html.twig', array('entities' => $entities));
    }

    /**
     * Creates a new Point entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Pointp();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->get('request');
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter un pointp ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('pointp'));
        }

        return $this->render('BtobHotelBundle:Pointp:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Point entity.
     *
     * @param Point $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pointp $entity)
    {
        $form = $this->createForm(new PointpType(), $entity, array(
            'action' => $this->generateUrl('pointp_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new Point entity.
     *
     */
    public function newAction()
    {
        $entity = new Pointp();
        $form = $this->createCreateForm($entity);

        return $this->render('BtobHotelBundle:Pointp:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Point entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Pointp')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Pointp:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Point entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Pointp')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Pointp:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Point entity.
     *
     * @param Point $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pointp $entity)
    {
        $form = $this->createForm(new PointpType(), $entity, array(
            'action' => $this->generateUrl('pointp_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Edits an existing Point entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Pointp')->find($id);
       
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier un pointp n�� " . $entity->getId()." : " . $entity->getPtmin()." et ".$entity->getPtmax(). " point");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            
            return $this->redirect($this->generateUrl('pointp'));
        }

        return $this->render('BtobHotelBundle:Pointp:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Point entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobHotelBundle:Pointp')->find($id);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete un pointp n�� " . $entity->getId()." : " . $entity->getPtmin()." et ".$entity->getPtmax(). " point");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('pointp'));
    }

    /**
     * Creates a form to delete a Point entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pointp_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

}
