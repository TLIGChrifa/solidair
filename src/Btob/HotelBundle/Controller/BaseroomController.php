<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Baseroom;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\BaseroomType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class BaseroomController extends Controller {

    public function indexAction() {
        $Baseroom = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Baseroom')
                ->findAll();
        return $this->render('BtobHotelBundle:Baseroom:index.html.twig', array('entities' => $Baseroom));
    }

    public function addAction() {
        $Baseroom = new Baseroom();
        $form = $this->createForm(new BaseroomType(), $Baseroom);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($Baseroom);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter une baseroom");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Baseroom:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $Baseroom = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Baseroom')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new BaseroomType(), $Baseroom);
        $form->handleRequest($request);

        if ($form->isValid()) {

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier une baseroom n° " . $Baseroom->getId()." : " . $Baseroom->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Baseroom:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Baseroom $Baseroom) {
        $em = $this->getDoctrine()->getManager();

        if (!$Baseroom) {
            throw new NotFoundHttpException("enregistrement non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete une baseroom n° " . $Baseroom->getId()." : " . $Baseroom->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Baseroom);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_baseroom_homepage'));
    }



}
