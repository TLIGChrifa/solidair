<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Email;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class EmailController extends Controller {

    public function indexAction() {
        $emaillist = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Email')
                ->findAll();
        return $this->render('BtobHotelBundle:Email:index.html.twig', array('emaillist' => $emaillist));
    }



    public function deleteAction(Email $email) {
        $em = $this->getDoctrine()->getManager();

        if (!$email) {
            throw new NotFoundHttpException("Email non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete une email n° " . $email->getId()." : " . $email->getEmailAdress());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($email);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_email_homepage'));
    }
	
	public function exportAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $iterableResult = $em->getRepository('BtobHotelBundle:Email')->findAll();
        $handle = fopen('php://memory', 'r+');
        $header = array();
       
        fputcsv($handle, array('N°', 'Addresse e-mail', 'Dtae d\'inscription'),';');
        //while( false !== ($row = $iterableResult->next()) )
        foreach ($iterableResult as $row) {
        
            fputcsv($handle,array($row->getId(),
                $row->getEmail_adress(),
                $row->getDateinsc()->format('Y-m-d-H-i-s')
            ),';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);
        
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("export la liste de newsletter");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
        return new Response($content, 200, array(
            'Content-Type' => 'application/force-download',
            'Content-Disposition' => 'attachment; filename="export.csv"'
        ));
    }
}
