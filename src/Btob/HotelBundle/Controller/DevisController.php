<?php


namespace Btob\HotelBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Devis;

use Btob\HotelBundle\Entity\Tdevis;

use Btob\HotelBundle\Form\DevisType;

use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Mfacture;

use Btob\HotelBundle\Entity\Tfacture;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\HttpFoundation\JsonResponse;

class DevisController extends Controller

{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $devis = $em->createQueryBuilder()
             ->select('d')
             ->from('BtobHotelBundle:Devis','d')
             ->orderBy('d.id','DESC')
             ->getQuery()
             ->getResult();
             
$clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
        return $this->render('BtobHotelBundle:Devis:index.html.twig', array('entities' => $devis,'clients'=>$clients));

    }

    public function extraAction()
    {
        
        $libelles=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Libelle')
            ->findBy(array('act' => 1));
        $tva=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')
            ->findBy(array('act' => 1));
        $factp=$this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
        $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();

        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $date = new \DateTime();
        $client = new Clients();
        $client->setAct(1);
        $form = $this->createForm(new ClientsType(), $client);
        if ($request->getMethod() == 'POST') {
        
            $montant = $request->request->get("montant");
            
            $observation=$request->request->get("observation");
            $client=$request->request->get("client");
            $clientid=$request->request->get("client_id");
        $em = $this->getDoctrine()->getManager();
        $client_id = $em->createQueryBuilder()
             ->select('c')
             ->from('BtobHotelBundle:Clients','c')
             ->where('c.id = :cid')
             ->setParameter('cid', $clientid)
             ->getQuery()
             ->getResult();
            $type = $request->request->get("type");

            $nom=$request->request->get("client");
            $date=$request->request->get("date");
            $date= new \DateTime($date);


            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->finFacts();
            $deviss = array();
            $dr = new \DateTime();
            if (count($facts) > 0) {
                foreach ($facts as $kef) {
                    if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                        array_push($deviss, $kef);
                    }
                }
            }

            if (count($deviss) > 0) {
                $num = $deviss[0]->getNum() + 1;
            } else {
                $num = 1;
            }
    //  exit(var_dump($client));
            $ffact = new Devis();
            $ffact->setNum($num);
            $ffact->setCode("BO");
            $ffact->setEtat(1);
            $ffact->setClient($client);
            $ffact->setClientId($client_id[0]);
            $ffact->setDcr($dr);
            $ffact->setIdm($num);
            $ffact->setType($type[0]);
            $ffact->setMontant($montant);
           // $ffact->setMontantP(null);
            $ffact->setDate($date);
            $ffact->setObservation($observation);
            $em->persist($ffact);
            $em->flush();
    //  exit(var_dump($num));
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Devis");
                        $hist->setMessage("Ajout: Devis sous numéro " . $num ." du client " . $client);
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            

            $tva=$request->request->get("tva");
            $quantite=$request->request->get("quantite");
            $prix_unitaire=$request->request->get("prix_unitaire");
            $remise=$request->request->get("remise");
            
            foreach ($type as $key => $value) {
                $tdevis = new Tdevis();
                $tdevis->setDevis($ffact);
                $tdevis->setType($type[$key]);
                $tdevis->setTva($tva[$key]);
                $tdevis->setQuantite($quantite[$key]);
                $tdevis->setPrixUnitaire($prix_unitaire[$key]);
                $tdevis->setRemise($remise[$key]);

                $em->persist($tdevis);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('btob_devis_homepage'));
        }
       $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
        return $this->render('BtobHotelBundle:Devis:extra.html.twig',array('entities' => $clients,'form' =>$form->createView(),'countries'=>$countries,'factp'=>$factp,'libelles'=>$libelles,'tva'=>$tva));
    
    }

    public function actiAction(Devis $devis, $action)
    {
        $em = $this->getDoctrine()->getManager();

        $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
        $factures = array();
        $dr = new \DateTime();
        if (count($facts) > 0) {
            foreach ($facts as $kef) {
                if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                    array_push($factures, $kef);
                }
            }
        }

        if (count($factures) > 0) {
            $num = $factures[0]->getNum() + 1;
        } else {
            $num = 1;
        }

        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis_id' => $devis));
        if ($action == 2) {
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setCode("BO");
            $ffact->setEtat(2);
            $ffact->setClient($devis->getClient());
            $ffact->setDcr($dr);
            $ffact->setIdm($devis->getId());
            $ffact->setType("EXtra");
            $ffact->setMontant($devis->getMontant());
            $em->persist($ffact);
            $em->flush();

            foreach ($tdevis as $ketd => $valuetd) {

                $tfact = new Tfacture();
                $tfact->setFacture($ffact);
                $tfact->setType($valuetd->getType());
                /*$tfact->setMontant($valuetd->getMontant());*/
                $em->persist($tfact);
                $em->flush();

            }


            $devis->setEtat(2);
            $em->persist($devis);
            $em->flush();


        }
        if ($action == 3) {


            if ($devis->getEtat() == 2) {
                $ffact = new Facture();
                $ffact->setNum($num);
				$ffact->setCode("BO");
                $ffact->setEtat(2);
                $ffact->setClient($devis->getClient());
                $ffact->setDcr($dr);
                $ffact->setIdm($devis->getId());
                $ffact->setType("EXtra");
                $ffact->setMontant(-1 * ($devis->getMontant()));
                $em->persist($ffact);
                $em->flush();
                foreach ($tdevis as $ketd => $valuetd) {

                    $tfact = new Tfacture();
                    $tfact->setFacture($ffact);
                    $tfact->setType($valuetd->getType());
                    $tfact->setMontant(-1 * ($valuetd->getMontant()));
                    $em->persist($tfact);
                    $em->flush();
                }

                $devis->setEtat(3);
                $em->persist($devis);
                $em->flush();

            } else {
                $devis->setEtat(3);
                $em->persist($devis);
                $em->flush();

            }

        }
        return $this->redirect($this->generateUrl('btob_devis_homepage'));

    }

    public function detailAction(Devis $devis)
    {

        $em = $this->getDoctrine()->getManager();
        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $devis));

        return $this->render('BtobHotelBundle:Devis:detail.html.twig', array('entry' => $devis, 'tdevis' => $tdevis));
    }


   public function phpAction()
   {
       echo 'PHP7 est deux fois plus rapide que PHP 5';
       echo 'PHP7  utilise moins de ressources';
       echo 'Nouvel opérateur null ajouté';
       echo 'PHP 7 Scalar type Déclarations =Coercive Mode,Strict mode=>"strict_types = 1"';
       echo 'PHP 7 Opérateur Spaceship "<=>"';
       echo 'Les tableaux constants "define()"';
       echo 'une nouvelle classe de IntlChar "Cette classe définit un certain nombre de méthodes et constantes statiques, qui peuvent être utilisés pour manipuler des caractères unicode"';
       echo 'une seule déclaration d\utilisation peut être utilisé pour importer des classes "use com\tutorialspoint\{ClassA, ClassB, ClassC as C};"';
       echo 'PHP 7 introduit une nouvelle fonction  IntDiv "intdiv(10,3)"';
       echo 'Extensions et SAPI Supprimé: "Extensions=>ereg,mssql,mysql,sybase_ct" "SAPI=>apache,apache_hooks,thttpd,milter,roxen" ';
   }
   
   
   
   
    public function exporterAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $factp=$this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
        $devis = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->find($id);
        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $devis));
        $counttdevis = $em->createQueryBuilder()
             ->select('count(dd.id)')
             ->from('BtobHotelBundle:Tdevis','dd')
             ->where('dd.devis = :iddevis')
             ->setParameter('iddevis', $devis)
             ->getQuery()
             ->getResult();
    //         exit(var_dump($counttdevis[0][1]));
        $session = $this->getRequest()->getSession();
        
        $client_id = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findBy(array('id' => $devis->getClientId()->getId()));
        
            $montant = $devis->getMontant();
            $montant_p = $devis->getMontantP();
            $type = $devis->getType();

            $montant_f=0;
            if($montant_p != null){
            foreach ($montant_p as $key ) {
              $montant_f+=$key;
            }
            }

            
            $client=$devis->getClient();
          

            $nom=$client;
            $date= new \DateTime();

            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
            $factures = array();
            $dr = new \DateTime();
            if (count($facts) > 0) {
                foreach ($facts as $kef) {
                    if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                        array_push($factures, $kef);
                    }
                }
            }

            if (count($factures) > 0) {
                $num = $factures[0]->getNum() + 1;
            } else {
                $num = 1;
            }
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setCode("BO");
            $ffact->setEtat(1);
            $ffact->setClient($client);
            $ffact->setClientId($client_id[0]);
            $ffact->setDcr($dr);
            $ffact->setIdm($num);
            $ffact->setType('Extra');
            $ffact->setMontant($montant);
            $ffact->setMontantP($montant_f);
            $ffact->setDate($date);
            $em->persist($ffact);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Devis");
                        $hist->setMessage("Conversion un devis vers une facture sous numéro " . $ffact->getNum()." du client " . $ffact->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            
            // $tt = 0;
    $type=array();
$tva=array();
$quantite=array();
$prix_unitaire=array();
$remise=array();
for($i=0;$i<$counttdevis[0][1];$i++){
    $type[$i] = $tdevis[$i]->getType();
$tva[$i]=$tdevis[$i]->getTva();
$quantite[$i]=$tdevis[$i]->getQuantite();
$prix_unitaire[$i]=$tdevis[$i]->getPrixUnitaire();
$remise[$i]=$tdevis[$i]->getRemise();
}
    //        exit(var_dump($type));

            foreach ($type as $key => $value) {
                $tfacture = new Tfacture();
                $tfacture->setFacture($ffact);
                $tfacture->setType($type[$key]);
                $tfacture->setTva($tva[$key]);
                $tfacture->setQuantite($quantite[$key]);
                $tfacture->setPrixUnitaire($prix_unitaire[$key]);
                $tfacture->setRemise($remise[$key]);

                $em->persist($tfacture);
                $em->flush();
              //  $tt = $tt + $montant[$key];
            }
            $mode=null;
            $mnt=0;
           
            $date_cheque=new \DateTime();
            $numero_cheque=null;
            $banque=null;
            $dt=new \DateTime();

      //      foreach ($mode as $key => $value) {
                $mfacture = new Mfacture();
                $mfacture->setFacture($ffact);
                $mfacture->setMode($mode);
                $mfacture->setMontant($mnt);
                $mfacture->setDate($dt);

                $mfacture->setDate_cheque($date_cheque);
                $mfacture->setNumero_cheque($numero_cheque);
                $mfacture->setBanque($banque);
                $em->persist($mfacture);
                $em->flush();
     //       }
           
            $div = $em->getRepository(Devis::class);
       $res = $div->createQueryBuilder('d')
             ->update()
             ->set('d.facture',':numfact')
             ->where('d.id = :iddevis')
             ->setParameter('numfact', $ffact->getId())
             ->setParameter('iddevis', $devis->getId())
             ->getQuery()
            ->execute();
      
            return $this->redirect($this->generateUrl('btob_facture_facture_homepage',array('id'=>$ffact->getId())));
       ///////////////////////////// }
    
    }
    public function devisAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $devis = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->find($id);
       // die();
        $fact = $this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $devis));
        $numtochar= $this->asLetters($devis->getMontant());
        
        $tva = $em->createQueryBuilder()
             ->select('t.name')
             ->from('BtobHotelBundle:Tva','t')
             ->orderBy('t.name', 'ASC')
             ->getQuery()
             ->getResult();
        $counttva = $em->createQueryBuilder()
             ->select('count(t.name)')
             ->from('BtobHotelBundle:Tva','t')
             ->where('t.act = :act')
             ->setParameter('act', 1)
             ->getQuery()
             ->getSingleScalarResult();
             $s=array();
        foreach($tva as $sum['name']){
        $somme = $em->createQueryBuilder()
            ->select('SUM(t.quantite * t.prixUnitaire)') //(t.quantite*t.prixUnitaire)-((t.quantite*t.prixUnitaire)/100)
             ->from('BtobHotelBundle:Tdevis','t')
             ->where('t.devis = :devi')
             ->setParameter('devi', $id)
             ->andWhere('t.tva = :tva')
             ->setParameter('tva', $sum['name'])
             ->groupBy('t.tva')
             ->orderBy('t.tva', 'ASC')
             ->getQuery()
             ->getResult();
             if($somme != null)
            $s[]=array($somme,);
            else 
            $s[]=array(0,);
        }
        $sum=array();
        for($i=0;$i<$counttva;$i++)
             if($s[$i][0] != 0)     $sum[]=$s[$i][0][0][1];
             else               $sum[]=0;
     //        exit(var_dump($sum));
        $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('Devis N° ' . $devis->getNum() . '/' . $devis->getDcr()->format("Y"));
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set default monospaced font
      //  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(10,5, 10,0);
        $pdf->setCellPaddings(0,0,0,0);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->AddPage();
     //   $pagefooter = "";
     //   exit(var_dump($tdevis));
        $html = $this->renderView('BtobHotelBundle:Devis:pdf.html.twig', array(
            'devis' => $devis,
            'tdevis' => $tdevis,
            'fact' => $fact,
            'tva' => $tva,
            'sum' => $sum,
            'montantenlettre' => $numtochar,
            //'pagefooter' => $pagefooter,
        ));
        $pdf->writeHTML($html);
        $pdf->Image("images/footerrustica.PNG", 0, 246.56, 210, '', 'PNG', '', 'T', false, 500, '', false, false, 0, false, false, false);

        $nompdf = 'devis_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
    public function deleteAction($id){
   
        $em = $this->getDoctrine()->getManager();
        $devis = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->find($id);


        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $id));
  
      
            foreach ($tdevis as $key ) {
       $em->remove($key);
        $em->flush();
    }
        $em->remove($devis);
        $em->flush();
      

       return $this->redirect($this->generateUrl('btob_devis_homepage'));


    }
    public function editAction($id){
        $em = $this->getDoctrine()->getManager();
        $devis = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->find($id);
        $id=$devis->getId();
        
        $libelles=$this->getDoctrine()->getRepository('BtobHotelBundle:Libelle')->findAll();
        $tva=$this->getDoctrine()->getRepository('BtobHotelBundle:Tva')->findBy(array('act' => 1));
        $tdevis = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $id));

        $client = new Clients();
        $client->setAct(1);
        $form = $this->createForm(new ClientsType(), $client);
    $factp=$this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
  //      $form = $this->createForm(new DevisType(), $devis);
    $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();
$em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
         $code=$devis->getCode();
       
        
        if ($request->getMethod() == 'POST'){
    //        exit(var_dump("aaaaaaaaaaaaaaaaaa"));
            $montant = $request->request->get("montant");
            $date = new \DateTime($request->request->get("date"));
            $type = $request->request->get("type");
            $client=$request->request->get("client");
            $clientid=$request->request->get("client_id");
        $em = $this->getDoctrine()->getManager();
        $client_id = $em->createQueryBuilder()
             ->select('c')
             ->from('BtobHotelBundle:Clients','c')
             ->where('c.id = :cid')
             ->setParameter('cid', $clientid)
             ->getQuery()
             ->getResult();

            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->finFacts();

            $devis->setMontant($montant);
            $devis->setDate($date);
            $devis->setClient($client);
            $devis->setClientId($client_id[0]);
         //   $em->persist($devis);
            $em->flush();
            
        $count = $em->createQueryBuilder()
             ->select('count(td.id)')
             ->from('BtobHotelBundle:Tdevis','td')
             ->where('td.devis = :devis')
             ->setParameter('devis', $devis->getId())
             ->getQuery()
             ->getSingleScalarResult();
            $tva=$request->request->get("tva");
            $quantite=$request->request->get("quantite");
            $prix_unitaire=$request->request->get("prix_unitaire");
            $remise=$request->request->get("remise");
            $j=0;
         //   exit(var_dump($tdevis));
            foreach ($type as $key => $value) {
                if(($j+1)<=$count)
                $tdeviss = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->find($tdevis[$j]->getId());
                else
                $tdeviss = new Tdevis();
      
                $tdeviss->setDevis($devis);
                $tdeviss->setType($type[$key]);
                $tdeviss->setTva($tva[$key]);
                $tdeviss->setQuantite($quantite[$key]);
                $tdeviss->setPrixUnitaire($prix_unitaire[$key]);
                $tdeviss->setRemise($remise[$key]);

                if(($j+1)>$count){
                    $em->persist($tdeviss);
                }
                $em->flush();
                $j=$j+1;
            }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Devis");
                        $hist->setMessage("Modificatin: Devis  " . $devis->getId()." sous numéro " . $devis->getNum()." du client " . $devis->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
          
      
            return $this->redirect($this->generateUrl('btob_devis_homepage'));
        }
        $clients = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findAll(); 
           //  return $this->render('BtobHotelBundle:Devis:edit.html.twig', array('entity' => $devis,'tdevis'=>$tdevis,));
                         return $this->render('BtobHotelBundle:Devis:edit.html.twig',array(
                             'tdeviss'=>$tdevis,
                             'devis'=>$devis,
                             'entities' => $clients,
                             'form' =>$form->createView(),
                             'countries'=>$countries,
                             'factp'=>$factp,
                             'libelles'=>$libelles,
                             'tva'=>$tva
                             ));

    }


public function filtreAction(){
               
    $em = $this->getDoctrine()->getManager();
    $request = $this->get('request');
    $from=$request->request->get("from");
    $to=$request->request->get("to");
    $client_id=$request->request->get("client_id");

  
if (($client_id=="")&&($from=="")&&($to=="")) 
{
   $entities = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->findBy(array(),array('id'=>'DESC'));
}
elseif ($client_id=="") 
 {
      if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
        $entities = $em->createQueryBuilder()
             ->select('d')
             ->from('BtobHotelBundle:Devis','d')
             ->where('d.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->orderBy('d.id','DESC')
             ->getQuery()
             ->getResult();
    }
    else 
    {
        $entities = $em->createQueryBuilder()
             ->select('d')
             ->from('BtobHotelBundle:Devis','d')
             ->where('d.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->orderBy('d.id','DESC')
             ->getQuery()
             ->getResult();
    }
}

elseif ($client_id!="") 
{

    if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
    }
  
        $entities = $em->createQueryBuilder()
             ->select('d')
             ->from('BtobHotelBundle:Devis','d')
             ->where('d.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->leftjoin('d.client_id','c')
             ->andWhere('c.id =:clientid')
             ->setParameter('clientid', $client_id)
             ->orderBy('d.id','DESC')
             ->getQuery()
             ->getResult();
}
//exit(var_dump($entities));


$clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
  return $this->render('BtobHotelBundle:Devis:index.html.twig', array('entities' => $entities,'clients'=>$clients));


}
public function dubliquerAction($id){

 $em = $this->getDoctrine()->getManager();
        $devisa = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->find($id);
        
        //$id=$devis->getId();
        
        //var_dump($id);
        //die();
 $tdevisa = $this->getDoctrine()->getRepository('BtobHotelBundle:Tdevis')->findBy(array('devis' => $id));


            $num=$devisa->getNum();
            $code=$devisa->getCode();
            $etat=$devisa->getEtat();
            $client = $devisa->getClient();
            $clientid = $devisa->getClientId();
            $dr = new \DateTime();
            $type= $devisa->getType();
            $montant = $devisa->getMontant();
            $montant_p = $devisa->getMontantP();
            $date=$devisa->getDate();
            $observation=$devisa->getObservation();
        

            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Devis')->finFacts();
            $factures = array();
            if (count($facts) > 0) {
                foreach ($facts as $kef) {
                    if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                        array_push($factures, $kef);
                    }
                }
            }

            if (count($factures) > 0) {
                $num = $factures[0]->getNum() + 1;
            } else {
                $num = 1;
            }
            
            $ffact = new Devis();
            $ffact->setNum($num);
            $ffact->setCode($code);
            $ffact->setEtat($etat);
            $ffact->setClient($client);
            $ffact->setClientId($clientid);
            $ffact->setDcr($dr);
            $ffact->setIdm($num);
            $ffact->setType($type);
            $ffact->setMontant($montant);
            $ffact->setMontantP($montant_p);
            $ffact->setDate($date);
            $ffact->setObservation($observation);
            $em->persist($ffact);
            $em->flush();


            foreach ($tdevisa as $key ) {
                $tdevis = new Tdevis();
                $tdevis->setDevis($ffact);
                $tdevis->setMontant($key->getMontant());
                $tdevis->setMontantP($key->getMontantP());
                $tdevis->setType($key->getType());
                $tdevis->setQuantite($key->getQuantite());
                $tdevis->setPrixUnitaire($key->getPrixUnitaire());
                $tdevis->setRemise($key->getRemise());
                $tdevis->setTva($key->getTva());
                $em->persist($tdevis);
                $em->flush();
            }
            //$em->persist($ffact);
        //    $em->flush();
            return $this->redirect($this->generateUrl('btob_devis_homepage'));
    

}





   public function asLetters($number) {
$convert = explode('.', $number);
$num[17] = array('zero', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit',
'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize');

$num[100] = array(20 => 'vingt', 30 => 'trente', 40 => 'quarante', 50 => 'cinquante',
60 => 'soixante', 70 => 'soixante-dix', 80 => 'quatre-vingt', 90 => 'quatre-vingt-dix');

if (isset($convert[1]) && $convert[1] != '') {
    if($convert[0]==0){
        if($convert[1]<10){
            return self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[1]).' millimes';
        }
    }else{
        if($convert[1]<10){
            return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]).' millimes';
        }
    }
}
if ($number < 0) return 'moins '.self::asLetters(-$number);
if ($number < 17) {
return $num[17][$number];
}
elseif ($number < 20) {
return 'dix-'.self::asLetters($number-10);
}
elseif ($number < 100) {
if ($number%10 == 0) {
return $num[100][$number];
}
elseif (substr($number, -1) == 1) {
if( ((int)($number/10)*10)<70 ){
return self::asLetters((int)($number/10)*10).'-et-un';
}
elseif ($number == 71) {
return 'soixante-et-onze';
}
elseif ($number == 81) {
return 'quatre-vingt-un';
}
elseif ($number == 91) {
return 'quatre-vingt-onze';
}
}
elseif ($number < 70) {
return self::asLetters($number-$number%10).'-'.self::asLetters($number%10);
}
elseif ($number < 80) {
return self::asLetters(60).'-'.self::asLetters($number%20);
}
else {
return self::asLetters(80).'-'.self::asLetters($number%20);
}
}
elseif ($number == 100) {
return 'cent';
}
elseif ($number < 200) {
return self::asLetters(100).' '.self::asLetters($number%100);
}
elseif ($number < 1000) {
return self::asLetters((int)($number/100)).' '.self::asLetters(100).($number%100 > 0 ? ' '.self::asLetters($number%100): '');
}
elseif ($number == 1000){
return 'mille';
}
elseif ($number < 2000) {
return self::asLetters(1000).' '.self::asLetters($number%1000).' ';
}
elseif ($number < 1000000) {
return self::asLetters((int)($number/1000)).' '.self::asLetters(1000).($number%1000 > 0 ? ' '.self::asLetters($number%1000): '');
}
elseif ($number == 1000000) {
return 'millions';
}
elseif ($number < 2000000) {
return 'un '.self::asLetters(1000000).' '.self::asLetters($number%1000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000)).' '.self::asLetters(1000000).($number%1000000 > 0 ? ' '.self::asLetters($number%1000000): '');
}
elseif ($number == 1000000000) {
return 'milliard';
}
elseif ($number < 2000000000) {
return 'un '.self::asLetters(1000000000).' '.self::asLetters($number%1000000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000000)).' '.self::asLetters(1000000000).($number%1000000000 > 0 ? ' '.self::asLetters($number%1000000000): '');
}
}

}

