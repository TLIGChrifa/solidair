<?php

namespace Btob\HotelBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Point;
use Btob\HotelBundle\Form\PointType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class PointController extends Controller
{
    /**
     * Lists all Point entities.
     *
     */

    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobHotelBundle:Point")->findAll();
        return $this->render('BtobHotelBundle:Point:index.html.twig', array('entities' => $entities));
    }

    /**
     * Creates a new Point entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Point();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->get('request');
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Ajouter un point");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('point'));
        }

        return $this->render('BtobHotelBundle:Point:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Point entity.
     *
     * @param Point $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Point $entity)
    {
        $form = $this->createForm(new PointType(), $entity, array(
            'action' => $this->generateUrl('point_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new Point entity.
     *
     */
    public function newAction()
    {
        $entity = new Point();
        $form = $this->createCreateForm($entity);

        return $this->render('BtobHotelBundle:Point:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Point entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Point')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Point:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Point entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Point')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Point:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Point entity.
     *
     * @param Point $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Point $entity)
    {
        $form = $this->createForm(new PointType(), $entity, array(
            'action' => $this->generateUrl('point_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Edits an existing Point entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Point')->find($id);
       
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Point entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Modifier un point n�� " . $entity->getId()." : " . $entity->getPt(). " point");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            
            return $this->redirect($this->generateUrl('point'));
        }

        return $this->render('BtobHotelBundle:Point:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Point entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BtobHotelBundle:Point')->find($id);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hotel");
                        $hist->setMessage("Delete un point n�� " . $entity->getId()." : " . $entity->getPt(). " point");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('point'));
    }

    /**
     * Creates a form to delete a Point entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('point_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

}
