<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Entity\Maisondhote;
use Btob\HotelBundle\Entity\Maisonhoteimg;
use Btob\HotelBundle\Form\MaisondhoteType;
use Btob\HotelBundle\Form\MaisonhoteimgType;

/**
 * Maisondhote controller.
 *
 */
class MaisondhoteController extends Controller
{

    /**
     * Lists all Maisondhote entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobHotelBundle:Maisondhote')->findAll();
        
       // foreach($entities as $m)
        $maisondhoteimg = $this->getDoctrine()->getRepository('BtobHotelBundle:Maisonhoteimg')->findAll();

//var_dump($maisondhoteimg).die;
        return $this->render('BtobHotelBundle:Maisondhote:index.html.twig', array(
            'entities' => $entities,
            'maison'=> $maisondhoteimg,
        ));
    }
    /**
     * Creates a new Maisondhote entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Maisondhote();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
    //    $formimg = $this->createForm(new MaisonhoteimgType(),"");
        if ($form->isValid()) {
           
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
          //  var_dump($entity->getNbimages()).Die;
          if (is_array($request->request->get("files")))
          foreach ($request->request->get("files") as $key => $value) {
          //  for ($i = 1; $i <= $entity->getNbimages(); $i++) {
                 //  var_dump($i).Die;
                $img = new Maisonhoteimg();
                $formimg = $this->createForm(new MaisonhoteimgType(), $img);
                $img->setMaisondhote($entity);
                $img->setPath($value);
                $img->upload($value);
                $em = $this->getDoctrine()->getManager();
                $em->persist($img);
                $em->flush();
                
            }
           return $this->redirect($this->generateUrl('maisondhote'));
        }

        return $this->render('BtobHotelBundle:Maisondhote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
           
          
          
        ));
    }

    /**
     * Creates a form to create a Maisondhote entity.
     *
     * @param Maisondhote $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Maisondhote $entity)
    {
        $form = $this->createForm(new MaisondhoteType(), $entity, array(
            'action' => $this->generateUrl('maisondhote_create'),
            'method' => 'POST',
        ));

      //  $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Maisondhote entity.
     *
     */
    public function newAction()
    {
        $entity = new Maisondhote();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobHotelBundle:Maisondhote:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Maisondhote entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Maisondhote')->find($id);
       // $service = $repository->findBy(array('name' => 'Registration'),array('name' => 'ASC'),1 ,0)[0];
        $img = $em->getRepository('BtobHotelBundle:Maisonhoteimg')->findBy(array('maisondhote' => $entity));
       // var_dump($img).die;
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maisondhote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Maisondhote:show.html.twig', array(
            'entity'      => $entity,
            'img'      => $img,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Maisondhote entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Maisondhote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maisondhote entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobHotelBundle:Maisondhote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Maisondhote entity.
    *
    * @param Maisondhote $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Maisondhote $entity)
    {
        $form = $this->createForm(new MaisondhoteType(), $entity, array(
            'action' => $this->generateUrl('maisondhote_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Maisondhote entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobHotelBundle:Maisondhote')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maisondhote entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('maisondhote_edit', array('id' => $id)));
        }

        return $this->render('BtobHotelBundle:Maisondhote:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Maisondhote entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobHotelBundle:Maisondhote')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Maisondhote entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('maisondhote'));
    }

    /**
     * Creates a form to delete a Maisondhote entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('maisondhote_delete', array('id' => $id)))
            ->setMethod('DELETE')
           // ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
