<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Reservation;
use Btob\HotelBundle\Entity\Reservationdetail;
use Btob\HotelBundle\Entity\Notification;
use Btob\HotelBundle\Entity\Hotelmarge;
use Btob\HotelBundle\Entity\HotelmargeRepository;
use Symfony\Component\HttpFoundation\Request;

//        $agence = $this->get('security.context')->getToken()->getUser();
//BtobHotelBundle

class ListController extends Controller

{
    public function validationAction()

    {





        return $this->render('BtobHotelBundle:List:validation.html.twig'

        );

    }
    /*
     public function validationAction()

        {

            $em = $this->getDoctrine()->getManager();
            $session = $this->getRequest()->getSession();
            $paiement = $session->get('paiement');
            $id_res_paiement = $session->get('id_res_paiement');
            $my_session_id = session_id();

            $user = $this->get('security.context')->getToken()->getUser();
            $recap = $session->get('recap');
            $supp = $session->get('supp');
            $zz= $session->get('ArrFinal');
            $client = $session->get('client');
            $event = $session->get('event');
            $children = $recap['children'];
            $idclient = $client->getId();
            $hotelid = $recap["hotelid"];
            $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
            $nameadulte = $session->get('nameadulte');
            $nbageadult = $session->get('nbageadult');

            $nbageenfant = $session->get('nbageenfant');
            $namechildreen = $session->get('namechildreen');
            $options = $session->get('options');


            $d1 = new \DateTime($recap['dated']);
            $d2 = new \DateTime($recap['datef']);

            return $this->render('BtobHotelBundle:List:validation.html.twig', array(
                    "paiement" => $paiement,
                    "id_res_paiement" => $id_res_paiement,
                    "my_session_id" => $my_session_id,
                    "hotelid" => $hotelid,


                    'entities' => $hotel,
                    'recap' => $recap,
                    'client' => $client,
                    'options' => $options,


                    'ArrFinal' => $zz,
                    'user' => $user,

                    'img' => $session->get('img'),
                    'margess' => $session->get('margess'),
                    "prixfin" => $session->get('prixfin'),
                    "dated" => $d1,
                    "datef" => $d2,
                    'surdemande' => $session->get('surdemande'),
                    'msgsurdemande' => $session->get('msgsurdemande')
                )
            );


        }*/

    public function inscriptionAction()
    {


           /**
         * $marge = $this->getDoctrine()
         * ->getRepository('BtobHotelBundle:Hotelmarge')
         * ->findByUser($userid);*/
        $agence = $this->get('security.context')->getToken()->getUser();
        $agenceact = $agence->getId();


        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $paiement =0;
        $request = $this->get('request');

        $request = $request->request;

        $recap = $session->get('recap');
    

        $d1 = new \DateTime($recap['dated']);

        $d2 = new \DateTime($recap['datef']);

        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $supp = $session->get('supp');
        $hotelid = $recap["hotelid"];
        $idhotel = intval($hotelid);

        $zz= $session->get('ArrFinal');

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($idhotel);

        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriodeHotel($agence,$hotel,$datedd);

        $margess = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByHotelAgence($hotel,$agence);
 $frais = $this->getDoctrine()->getRepository('BtobHotelBundle:Frais')->fraisDossier();

        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotel,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "";
            $persp = "";
        }

        if($margess) {
            foreach ($margess as $margesp) {
                $margessprst = $margesp->getPrst();
                $margeesmarge = $margesp->getMarge();
            }
        }else{
            $margessprst =$agence->getPrst();
            $margeesmarge = $agence->getMarge();
        }

        if($periods)
        {
            foreach ($periods as $period) {
                $margessprst = $period->getPrst();
                $margeesmarge = $period->getMarge();
            }

        }
        if ($margessprst == true) {
            $margepers = 1;
        } else {
            $margepers = 0;
        }
        $margeval = $margeesmarge;


        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);



        $hotelPrice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $agence);

        $majminstay = $hotelPrice[0]->getMajminstay();
        $redminstay = $hotelPrice[0]->getRedminstay();
        $persmajminstay = $hotelPrice[0]->getPersmajminstay();
        $persredminstay = $hotelPrice[0]->getPersredminstay();


        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $ArrFinal = array();
        $ArrBase = array();
        $dispo = false ;
        $total = 0;
        $exist = true;

        foreach ($recap["adulte"] as $key => $value) {

            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
            $typepersonne = $dbroom->getPersonne();
            $room = $dbroom->getRoom();
            foreach ($value as $k1 => $val1) {
                foreach ($val1 as $k2 => $val2) {
                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                    $ArrFinal[$room->getId()][$k1]['enfant'] = $recap['enfant'][$key][$k1][$k2];
                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($recap['arrangement'][$key][$k1][$k2]);

                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                    $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();




                    //Disponiblité


                    $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($hotelPrice[0]->getId(),$room->getId());

                    $valres = $hotelPrice[0]->getRetro();




                    if($nbjour>=$ArrFinal[$room->getId()][$k1]['minstay'] && $disroom[0]->getQte() > 0  )
                    {
                        $dispo =true ;

                    }
                    else{
                        $dispo = false ;
                        $exist = false ;

                    }


                    //fin





                    $dtest = null;
                    $dtest = $d1;
                    $tabchild = array();
                    if ($k2 == 0) $kx = 1; else $kx = $k2;
                    $tabsupp = null;
                    // supplement
                    $ArrFinal[$room->getId()][$k1]['supplement'] = array();
                    if (isset($supp[$key][$kx])) {
                        //Tools::dump($supp,true);
                        $tabsupp = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($supp[$key][$kx][0]);
                        $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>+ " . $tabsupp->getHotelsupplement()->getSupplement()->getName();


                    }
                    // enfant
                    if (isset($children[$key][$k1])) {
                        $tabchild = $children[$key][$k1];
                    }

                    $dbev = array();
                    $ArrFinal[$room->getId()][$k1]['events'] = array();
                    if (isset($event[$key][$k1])) {
                        foreach ($event[$key][$k1] as $kevv => $valevv) {
                            $xx = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                            $ArrFinal[$room->getId()][$k1]['events'] = $xx->getName();
                            $dbev[] = $xx;
                        }
                    }

                    //event
                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                        ->getRepository('BtobHotelBundle:Hotelprice')
                        ->calcultatea($valp,$persp,$agenceact,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $margeval, $margepers, $tabsupp, $dbev);


                    $total += $ArrFinal[$room->getId()][$k1]['price'];

                    $prixfin=$ArrFinal[$room->getId()][$k1]['price']+$frais->getPrix();
                    
                }


            }

        }



        $d1 = new \DateTime($recap['dated']);
        $d2 = new \DateTime($recap['datef']);
        $dataimg = $hotel->getHotelimg();
        $img = "/back/img/dummy_150x150.gif";
        $j = 0;
        foreach ($dataimg as $keyimg => $valimg) {
            if ($j == 0)
                $img = $valimg->getFile();
            if ($valimg->getPriori())
                $img = $valimg->getFile();
            ++$j;
        }
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {

                $em->persist($client);
                $em->flush();
                $session->set('client', $client);
                $session->set('nameadulte', $request->request->get('childnamead'));
                $session->set('nbageenfant', $request->request->get('nbageenfant'));
                $session->set('nbageadult', $request->request->get('nbageadult'));
                $session->set('namechildreen', $request->request->get('childnameenf'));
                $session->set('options', $request->request->get('options'));
                $session->set('paiement', $request->request->get('paiement'));
                $marge = $this->getDoctrine()
                    ->getRepository('BtobHotelBundle:Hotelmarge');

                // ->getMargeByUser($this->get('security.context')->getToken()->getUser());
                $em = $this->getDoctrine()->getManager();
                $session = $this->getRequest()->getSession();
                $caddy = array();
                if (!$session->has('caddy')) {
                    $session->set('caddy', $caddy);
                } else {
                    $caddy = $session->get('caddy');
                }
                $recap = $session->get('recap');
                $client = $session->get('client');
                $supp = $session->get('supp');
                $event = $session->get('event');
                $children = $recap['children'];
                $idclient = $client->getId();
                $client = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->find($idclient);
                $hotelid = $recap["hotelid"];
                $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);
                $nameadulte = $session->get('nameadulte');
                $nbageenfant = $session->get('nbageenfant');
                $nbageadult = $session->get('nbageadult');
                $namechildreen = $session->get('namechildreen');
                // get variante de marge


                $d1 = new \DateTime($recap['dated']);
                $d2 = new \DateTime($recap['datef']);
                $nbjour = $d2->diff($d1);
                $nbjour = $nbjour->days;
                $reservation = new Reservation();
                $reservation->setClient($client);
                $reservation->setHotel($hotel);
                $User = $this->get('security.context')->getToken()->getUser();
                $reservation->setUser($User);
                $reservation->setRemarque(""); // a terminer ajouter le champs dans le formulaires
                $reservation->setDated($d1);
                $reservation->setDatef($d2);
                $reservation->setAvance($prixfin);
                $session->set('paiement', $request->request->get('paiement'));
                if ($session->get('surdemande')) {
                    $reservation->setEtat(5);
                } else {
                    $reservation->setEtat(1);
                }

                $reservation->setNbnuit($nbjour);
                $reservation->setNamead(json_encode($nameadulte));
                $reservation->setAgeenfant(json_encode($nbageenfant));
                $reservation->setAgeadult(json_encode($nbageadult));
                $reservation->setNameenf(json_encode($namechildreen));

                // calcule et génération de tablea
                // Tools::dump($recap,true);
                $ArrFinal = array();

                $total = 0;
                $totalbase = 0;
                foreach ($recap["adulte"] as $key => $value) {

                    $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);
                    $typepersonne = $dbroom->getPersonne();
                    $room = $dbroom->getRoom();
                    foreach ($value as $k1 => $val1) {
                        foreach ($val1 as $k2 => $val2) {
                            $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                            $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;
                            $ArrFinal[$room->getId()][$k1]['enfant'] = $recap['enfant'][$key][$k1][$k2];
                            $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($recap['arrangement'][$key][$k1][$k2]);
                            $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();
                            $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();

                            $dtest = null;
                            $dtest = $d1;
                            $tabchild = array();
                            if ($k2 == 0) $kx = 1; else $kx = $k2;
                            $tabsupp = null;
                            // supplement
                            $ArrFinal[$room->getId()][$k1]['supplement'] = array();

                            if (isset($supp[$key][$kx])) {

                                foreach ($supp[$key][$kx] as $ks => $vs) {
                                    $tabsupps = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($vs);

                                    $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $tabsupps->getHotelsupplement()->getSupplement()->getName();
                                    $tabsupp[] = $tabsupps;
                                }
                            }


                            // enfant
                            if (isset($children[$key][$k1])) {
                                $tabchild = $children[$key][$k1];
                            }

                            $dbev = array();
                            $ArrFinal[$room->getId()][$k1]['events'] = array();
                            if (isset($event[$key][$k1])) {
                                foreach ($event[$key][$k1] as $kevv => $valevv) {
                                    $xx = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);
                                    $ArrFinal[$room->getId()][$k1]['events'] = $xx->getName();
                                    $dbev[] = $xx;
                                }
                            }

                            //event
                            $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultatea($valp, $persp, $User, $dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $margeval, $margepers, $tabsupp, $dbev);

                            $total += $ArrFinal[$room->getId()][$k1]['price'];

                         $ArrBase[$room->getId()][$k1]['price'] = $this->getDoctrine()
                        ->getRepository('BtobHotelBundle:Hotelprice')
                        ->calcultatea($valp,$persp,$agenceact,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, 0, 0, $tabsupp, $dbev);

                       $totalbase += $ArrBase[$room->getId()][$k1]['price'];
                        }


                    }
                }
                $reservation->setTotalint($total);
                $reservation->setTotalbase($totalbase);
                $total2 = Tools::calculemarge($total, $margepers, $margeval); // calcule de marge


                $reservation->setTotal($total2);

                $em->persist($reservation);
                $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRoles("ROLE_SUPER_ADMIN");

                foreach ($users as $kuser => $valuser) {
                    //if (in_array('ROLE_SUPER_ADMIN', $valuser->getRoles()) || in_array('SALES', $valuser->getRoles())) {
                    $notif = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findAll();
                    $tst = false;
                    $not = null;
                    foreach ($notif as $value) {
                        if ($valuser->getId() == $value->getUser()->getId()) {
                            $not = $value;
                            $tst = true;
                        }
                    }
                    if ($tst) {
                        $note = $not->getNotif() + 1;
                        $not->setNotif($note);
                    } else {
                        $not = new Notification();
                        $not->setNotif(1);
                        $not->setUser($valuser);
                    }
                    $caddy['notif'][] = $not;
                    $em->persist($not);
                    $em->flush();
                    //}
                }
                $em->flush();
                // génération de tableau pour l'enregistrer dans la bdd
                foreach ($ArrFinal as $kx => $vx) {

                    foreach ($vx as $key => $value) {
                        //Tools::dump($vx,true);
                        $detail = new Reservationdetail();
                        $detail->setPrice(Tools::calculemarge($value['price'], $margepers, $margeval));
                        $detail->setReservation($reservation);
                        $detail->setRoomname($value['roomname']);
                        $detail->setAd($value['adulte']);
                        $detail->setEnf($value['enfant']);
                        $detail->setArrangement($value['arrangement']);
                        $html = "";
                        foreach ($value["supplement"] as $ks => $vs) {
                            $html .= $vs . "<br />";
                        }
                        $detail->setSupp($html);
                        $html = "";
                        //Tools::dump($vx[$key]["events"], true);

                        if (is_array($vx[$key]["events"])) {
                            if (isset($vx[$key]["events"])) {
                                foreach ($vx[$key]["events"] as $kev => $vev) {
                                    $html .= $vev . "<br />";
                                }
                            }
                        } else {
                            $html .= $vx[$key]["events"] . "<br />";
                        }

                        $detail->setEvent($html);
                        $reservation->addReservationdetail($detail);

                        $em->persist($detail);
                        $em->flush();
                    }
                }

                $caddy['resahotel'] = $reservation;
                $session->set('caddy', $caddy);
                $detailres = $this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->find($reservation->getId());

                setlocale(LC_TIME, 'fr_FR', 'fra');
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%d %B %Y ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9", "UTF-8", strftime(mb_convert_encoding('%A ', 'ISO-8859-9', 'UTF-8'), strtotime(date('Y M D'))));

                $namead = json_decode($detailres->getNamead(), true);
                $nameenf = json_decode($detailres->getNameenf(), true);
                $ageenfant = json_decode($detailres->getAgeenfant(), true);

                $info =$this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
                $to = $reservation->getClient()->getEmail();	
				$mime_boundary = "----MSA Shipping----" . md5(time());
                $subject = $reservation->getUser()->getName() . ": réservation hôtel ";
                $headers = "From:" . $reservation->getUser()->getName() . " " . $reservation->getUser()->getEmail() . "\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=\"ISO-8859-1\"";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message1 .= '
				<br /> <b>Bonjour,</b><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br />
				 <table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Votre commande :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                $message1 .= '<td width="20%" height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getId() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getTotal() . ' DT</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Vos coordonnées :</b></td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getCp() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '<tr>';
                $message1 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message1 .= '<td height="30"><b>:</b></td>';
                $message1 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                $message1 .= '</tr>';

                $message1 .= '</table>';


                    $message1 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message1 .= '<tr>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
                    $message1 .= '</tr>';
					
                    $cc = 1;
                    foreach ($namead as $keyn => $valn) {
                        $message1 .= '<tr>';
                        $message1 .= '<td height="30"> Chambre ' . $cc . '</td>';
                        $message1 .= '<td height="30">';
                        foreach ($valn as $adv => $adx) {
                            $message1 .= '' . $adx . ' - ';


                        }
                        $message1 .= '</td>';


                        $message1 .= '</tr>';
                        $cc++;
                    }
                    $message1 .= '</table>';


                    $message1 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message1 .= '<tr>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
                    $message1 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
                    $message1 .= '</tr>';
					
                    foreach ($detailres->getReservationdetail() as $keyy => $vals) {
                        $message1 .= '<tr>';
                        $message1 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getAd() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getEnf() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getArrangement() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getSupp() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getEvent() . '</td>';
                        $message1 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
                        $message1 .= '</tr>';
                    }
                    $message1 .= '</table>';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body><br>';
                $message1 .= 'Cordialement.';
                $message1 .= '</body>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects = "Explore Voyage: Réservation  hôtel ";
                $header = "From:" . $reservation->getUser()->getName() . " " . $reservation->getUser()->getEmail() . "\n";
                $header .= "Reply-To:" . $reservation->getUser()->getName() . " " . $admin->getEmail() . "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=\"ISO-8859-1\"";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table>';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Sélection :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                $message2 .= '<td width="20%" height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getId() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getTotal() . ' DT</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Coordonnées :</b></td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getCp() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '<tr>';
                $message2 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                $message2 .= '<td height="30"><b>:</b></td>';
                $message2 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                $message2 .= '</tr>';

                $message2 .= '</table>';


                    $message2 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message2 .= '<tr>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
                    $message2 .= '</tr>';
					
                    $cc = 1;
                    foreach ($namead as $keyn => $valn) {
                        $message2 .= '<tr>';
                        $message2 .= '<td height="30"> Chambre ' . $cc . '</td>';
                        $message2 .= '<td height="30">';
                        foreach ($valn as $adv => $adx) {
                            $message2 .= '' . $adx . ' - ';


                        }
                        $message2 .= '</td>';


                        $message2 .= '</tr>';
                        $cc++;
                    }
                    $message2 .= '</table>';


                    $message2 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message2 .= '<tr>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
                    $message2 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Prix</b></td>';
                    $message2 .= '</tr>';
					
                    foreach ($detailres->getReservationdetail() as $keyy => $vals) {
                        $message2 .= '<tr>';
                        $message2 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getAd() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getEnf() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getArrangement() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getSupp() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getEvent() . '</td>';
                        $message2 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
                        $message2 .= '</tr>';
                    }
                    $message2 .= '</table>';

                $message2 .= '<table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';

                mail($too, $subjects, $message2, $header);

                //mail responsable
                $namead = json_decode($detailres->getNamead(), true);
                $nameenf = json_decode($detailres->getNameenf(), true);
                $ageenfant = json_decode($detailres->getAgeenfant(), true);


                $responsable = $this->getDoctrine()->getRepository('BtobHotelBundle:Responsablehotel')->responsableHotel($hotel);


                if ($responsable) {

                    $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                    $toor = $responsable->getMail();
                    $mime_boundary = "----MSA Shipping----" . md5(time());
                    $subjectr = "Explore Voyage: Réservation  hôtel";
                    $headerr = "From:" . $reservation->getUser()->getName() . " " . $reservation->getUser()->getEmail() . "\n";
                    $headerr .= "Reply-To:" . $reservation->getUser()->getName() . " " . $admin->getEmail() . "\n";
                    $headerr .= "MIME-Version: 1.0\n";
                    $headerr .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                    $message3 = "--$mime_boundary\n";
                    $message3 .= "Content-Type: text/html; charset=\"ISO-8859-1\"";
                    $message3 .= "Content-Transfer-Encoding: 8bit\n\n";
                    $message3 .= "<html>\n";
                    $message3 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;" leftmargin="0">';
                    $message3 .= '<img src="'.$img_logo.'" /><br>';
                    $message3 .= '<table width="90%"  cellspacing="1" border="0">';
                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Sélection :</b></td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Nom agence</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getUser()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td width="25%" height="30"><b style="padding-left:10px;">Id commande</b></td>';
                    $message3 .= '<td width="20%" height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getId() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Hôtel</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getHotel()->getName() . ' ' . $detailres->getHotel()->getVille()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Date début</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getDated()->format('Y-m-d H:i:s') . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Date fin</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getDatef()->format('Y-m-d H:i:s') . '</td>';
                    $message3 .= '</tr>';

                    /*$message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Total</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getTotal() . ' DT</td>';
                    $message3 .= '</tr>';*/

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" colspan="3" bgcolor="#fcdb0d"><b style="color:#183961; padding-left:5px;"> Coordonnées :</b></td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Civ</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCiv() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Prénom</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getPname() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Nom</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getName() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">E-mail</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getEmail() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Tél</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getTel() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">CIN</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCin() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Adresse</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getAdresse() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Code postal</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getCp() . '</td>';
                    $message3 .= '</tr>';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30"><b style="padding-left:10px;">Pays</b></td>';
                    $message3 .= '<td height="30"><b>:</b></td>';
                    $message3 .= '<td height="30">' . $detailres->getClient()->getPays()->getName() . '</td>';
                    $message3 .= '</tr>';


                    $message3 .= '</table>';


                    $message3 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Noms des occupants</b></td>';
                    $message3 .= '</tr>';
					
                    $cc = 1;
                    foreach ($namead as $keyn => $valn) {
                        $message3 .= '<tr>';
                        $message3 .= '<td height="30"> Chambre ' . $cc . '</td>';
                        $message3 .= '<td height="30">';
                        foreach ($valn as $adv => $adx) {
                            $message3 .= '' . $adx . ' - ';


                        }
                        $message3 .= '</td>';


                        $message3 .= '</tr>';
                        $cc++;
                    }
                    $message3 .= '</table>';


                    $message3 .= '<table width="90%"  cellspacing="1" border="0">';

                    $message3 .= '<tr>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Chambres</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Adulte(s)</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Enfant(s)</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Arrangements</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Suppléménts</b></td>';
                    $message3 .= '<td height="30" bgcolor="#fcdb0d"><b style="padding-left:10px; color:#183961;">Evenement</b></td>';
                    $message3 .= '</tr>';
					
                    foreach ($detailres->getReservationdetail() as $keyy => $vals) {
                        $message3 .= '<tr>';
                        $message3 .= '<td height="30"> ' . $vals->getRoomname() . '</td>';
                        $message3 .= '<td height="30">' . $vals->getAd() . '</td>';
                        $message3 .= '<td height="30">' . $vals->getEnf() . '</td>';
                        $message3 .= '<td height="30">' . $vals->getArrangement() . '</td>';
                        $message3 .= '<td height="30">' . $vals->getSupp() . '</td>';
                        $message3 .= '<td height="30">' . $vals->getEvent() . '</td>';
                        //$message3 .= '<td height="30">' . $vals->getPrice() . ' DT</td>';
                        $message3 .= '</tr>';
                    }
                    $message3 .= '</table>';

                $message3 .= '<table width="90%"  cellspacing="1" border="0">';
                $message3 .= '<tr>';
                $message3 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message3 .= '</tr>';

                $message3 .= '</table>';

                    $message3 .= '</body>';

                    mail($toor, $subjectr, $message3, $headerr);

                }




            } else {
                echo $form->getErrors();
            }
            $request->getSession()->getFlashBag()->add('notifomrafront', 'Votre message a été bien envoyé. Merci.');
            $session->set('recap', $recap);
            $session->set('nameadulte', $request->request->get('childnamead'));
            $session->set('nbageenfant', $request->request->get('nbageenfant'));
            $session->set('nbageadult', $request->request->get('nbageadult'));
            $session->set('namechildreen', $request->request->get('childnameenf'));
            $session->set('options', $request->request->get('options'));
            $session->set('paiement', $request->request->get('paiement'));
            $session->set('img', $img);
            $session->set('total', $total2);
            $session->set('id_res_paiement', $reservation->getId());

          $request->getSession()->getFlashBag()->add('notifomrabtob', 'Votre message a bien été envoyé. Merci.');

            return $this->redirect($this->generateUrl('btob_hotel_validation_reservation'));


        }
        // traitement pour l'affichage de formulaire de rooming list
        $tabform = array();
        foreach ($recap['adulte'] as $key => $value) {
            foreach ($value as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $tabform[$key][$k2]["adulte"] = $v2;
                    $tabform[$key][$k2]["enfant"] = $recap['enfant'][$key][$k1][$k2];
                }
            }
        }
        $user = $this->get('security.context')->getToken()->getUser();
        //Tools::dump($recap,true);
        $ctr = count($recap['adulte']);

        
         $ax= array();


                            if(isset($recap['children']))
                             {
                                 
                                foreach ($recap['children'] as $keyss => $valuess) {
                                 foreach ($valuess as $keysss => $valuesss) {
                                     foreach ($valuesss as $keyssss => $valuessss) {
                                array_push($ax, $valuessss);
                                     }
                                 }
                            } 
                             }
                            
       
        return $this->render('BtobHotelBundle:List:inscription.html.twig', array(
                "hotelid" => $hotelid,
                "dated" => $d1,
                "datef" => $d2,
                "prixfin" => $prixfin,
                'entities' => $hotel,
                "frais" => $frais,
                'img' => $img,
                'ax' => $ax,
                'valres' => $valres,
                'margess' => $margess,
                'recap' => $recap,
                'form' => $form->createView(),
                'tabform' => $tabform,
                // affichage de recap de l'action recap
                'ArrFinal' => $zz,
                'exist' => $exist,
                'user' => $user,
                'surdemande' => $session->get('surdemande'),
                'msgsurdemande' => $session->get('msgsurdemande')
            )
        );
    }


//  $request->getSession()->getFlashBag()->add('notifomrabtob', 'Votre message a bien été envoyé. Merci.');

          //  return $this->redirect($this->generateUrl('btob_hotel_validation_reservation'));
    public function recapAction()

    {

        $User = $this->get('security.context')->getToken()->getUser();

          $marge=$User->getHotelmarge();

        $request = $this->get('request');

        $request = $request->request;

        $session = $this->getRequest()->getSession();

        $d1 = new \DateTime($request->get('dated'));

        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $d2 = new \DateTime($request->get('datef'));

        $hotelid = $request->get('hotelid');
        $idhot = intval($hotelid);
        $hotels =$this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($idhot);

        $user = $User;

        $hotelPrice = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $majminstay = $hotelPrice[0]->getMajminstay();
        $redminstay = $hotelPrice[0]->getRedminstay();
        $persmajminstay = $hotelPrice[0]->getPersmajminstay();
        $persredminstay = $hotelPrice[0]->getPersredminstay();

        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotels,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "";
            $persp = "";
        }
        $hotelPrice0=$hotelPrice[0];

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);


        $surdemande = false;

        $msgsurdemande = "";

        // test stopsales

        $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($hotel, $d1, $user);

        /*if (count($stopsales) > 0) {

            $surdemande = true;

            $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

        }*/
        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriodeHotel($user,$hotel,$datedd);


        $margess = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByHotelAgence($hotel,$user);

        if($margess) {
            foreach ($margess as $margesp) {
                $margessprst = $margesp->getPrst();
                $margeesmarge = $margesp->getMarge();
            }
        }else{
            $margessprst =$User->getPrst();
            $margeesmarge = $User->getMarge();
        }


        if($periods)
        {
            foreach ($periods as $period) {
                $margessprst = $period->getPrst();
                $margeesmarge = $period->getMarge();
            }

        }

        if ($margessprst == true) {
            $margepers = 1;
        } else {
            $margepers = 0;
        }
        $margeval = $margeesmarge;
        $price = $hotelPrice[0];

        $dataimg = $price->getHotel()->getHotelimg();

        $img = "/back/img/dummy_150x150.gif";

        $j = 0;

        foreach ($dataimg as $keyimg => $valimg) {

            if ($j == 0)

                $img = $valimg->getFile();

            if ($valimg->getPriori())

                $img = $valimg->getFile();

            ++$j;

        }


        $nbjour = $d2->diff($d1);

        $nbjour = $nbjour->days;

        $adulte = $request->get("adulte");

        $enfant = $request->get("enfant");


        $children = $request->get("children");

        $arrangement = $request->get("arrangement");




        $supp = $request->get("supp");



        $event = $request->get("event");//Tools::dump($supp,true);

        // set all recap to session

        /*foreach($enfant as $kk=>$vv){

            Tools::dump($vv);

        }exit;*/

        $recap["enfant"] = $enfant;

        $recap["adulte"] = $adulte;

        $recap["dated"] = $request->get('dated');

        $recap["datef"] = $request->get('datef');

        $recap["hotelid"] = $hotelid;

        //Tools::dump($request,true);

        $recap["children"] = $children;

        // Tools::dump($children,true);





        // exit;

        $recap["arrangement"] = $arrangement;


        $recap["supp"] = $supp;

        $recap["event"] = $event;



        $session->set('recap', $recap);

        $session->set('supp', $supp);

        $session->set('event', $event);

        $session->set('event', $event);

        // end set recap

        // test sur demande pour le dépassement de minstay général



        foreach ($hotelPrice as $vv) {

            if ($vv->getNbnuit() > $nbjour) {

                $surdemande = true;

                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $vv->getNbnuit() . " nuit&eacute;es ";

            }

        }

        // test de l'existance de toute la période

        if ($this->getDoctrine()->getRepository('BtobHotelBundle:HotelPrice')->Testallperiode($hotel, $d2, $user)) {

            $surdemande = true;

            $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

        }

        $ArrFinal = array();

        $totalchambre=array();

        foreach ($adulte as $key => $value) {

            $dbroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelroom')->find($key);



            $typepersonne = $dbroom->getPersonne();

            $typepersonne=0;

            $room = $dbroom->getRoom();



            foreach ($value as $k1 => $val1) {



                foreach ($val1 as $k2 => $val2) {

                    $ArrFinal[$room->getId()][$k1]['roomname'] = $room->getName();

                    $ArrFinal[$room->getId()][$k1]['adulte'] = $val2;

                    $ArrFinal[$room->getId()][$k1]['enfant'] = $enfant[$key][$k1][$k2];



                    $dbarr = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->find($arrangement[$key][$k1][$k2]);




                    // calcule de sur demande de ministay



                    if ($dbarr->getMinstay() > $nbjour) {

                        $surdemande = true;

                        $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Nombre de Nuit&eacute;es minimum est à " . $dbarr->getMinstay() . " nuit&eacute;es ";



                    }

                    $ArrFinal[$room->getId()][$k1]['arrangement'] = $dbarr->getHotelarrangement()->getArrangement()->getName();


                    $ArrFinal[$room->getId()][$k1]['minstay'] = $dbarr->getMinstay();

                    $dtest = null;

                    $dtest = $d1;

                    $tabchild = array();

                    if ($k2 == 0) $kx = 1; else $kx = $k2;

                    $tabsupp = array();

                    // supplement



                    if (isset($supp[$key][$k1])) {

                        foreach ($supp[$key][$k1] as $ks => $vs) {
                            $tabsupps = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricesupplement')->find($vs);

                            $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $tabsupps->getHotelsupplement()->getSupplement()->getName();
                            $tabsupp[] = $tabsupps;
                        }
                    }



                    $priceroom = $price->getPriceroom();

                    foreach ($priceroom as $kpr => $vpr) {

                        if ($room->getId() == $vpr->getRoom()->getId()) {

                            if ($vpr->getQte() == 0) {

                                $surdemande = true;

                                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>La chambre " . $room->getName() . " n'est pas disponible.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

                            }

                            if(!isset($totalchambre[$room->getName()]["qte"])){

                                $totalchambre[$room->getName()]["qte"]=1;



                            }else{

                                $totalchambre[$room->getName()]["qte"]=$totalchambre[$room->getName()]["qte"]+1;

                            }

                            $totalchambre[$room->getName()]["total"]=$vpr->getQte();

                        }

                    }

                    // enfant

                    if (isset($children[$key][$k1])) {

                        $tabchild = $children[$key][$k1];


                    }//echo "$k1-";


                    if (!isset($event[$key][$k1][$k2]) || count($event[$key][$k1][$k2]) == 0) {

                        $testeve = $hotel->getEvents();

                        foreach ($testeve as $vevtest) {

                            if($vevtest->getDated()->format('Ymd') >= $d1->format('Ymd')

                                && $vevtest->getDated()->format('Ymd')<= $d2->format('Ymd')  && $vevtest->getOblig())

                                $event[$key][$k1][$k2] = $vevtest->getId();

                        }



                        $session->set('event', $event);

                    }

                    $dbev = array();

                    if (isset($event[$key][$k1])) {

                        foreach ($event[$key][$k1] as $kevv => $valevv) {

                            $xevent = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->find($valevv);

                            $dbev[] = $xevent;

                            $ArrFinal[$room->getId()][$k1]['arrangement'] .= "<br>- " . $xevent->getName();

                        }

                    }





                    $ArrFinal[$room->getId()][$k1]['price'] = $this->getDoctrine()

                        ->getRepository('BtobHotelBundle:Hotelprice')

                        ->calcultatea($valp,$persp,$user,$dtest->format('Y-m-d'), $hotelid, $nbjour, $dbarr->getHotelarrangement()->getArrangement(), $ArrFinal[$room->getId()][$k1]['adulte'], $tabchild, $room, $typepersonne, $margeval, $margepers, $tabsupp, $dbev);


                    $ArrFinal[$room->getId()][$k1]['price'] = Tools::calculemarge($ArrFinal[$room->getId()][$k1]['price'], $margepers, $margeval); // calcule de marge


                    // stop sales tarif =0

                    if ($ArrFinal[$room->getId()][$k1]['price'] == 0) {

                        $surdemande = true;

                        $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

                    }

                }

            }

        }

        foreach($totalchambre as $key=>$value){

            if($value["total"]<$value["qte"]){

                $surdemande = true;

                $msgsurdemande = "Cet hôtel est disponible sur demande.<br>Vous pouvez n&eacute;anmoins faire une demande qui vous sera confirm&eacute;e dans les plus brefs d&eacute;lais en cliquant sur le bouton ";

            }

        }

        //Tools::dump($totalchambre,true);



        $session->set('ArrFinal', $ArrFinal);

        $session->set('surdemande', $surdemande);

        $session->set('msgsurdemande', $msgsurdemande);

        return $this->redirect($this->generateUrl('btob_list_clientform_homepage'));

        /*return $this->render('BtobHotelBundle:List:recap.html.twig', array(

                "hotelid" => $hotelid,

                "dated" => $d1,

                "datef" => $d2,

                'price' => $hotelPrice,

                'nbjour' => $nbjour,

                'entities' => $ArrFinal,

                'img' => $img,

                'entities' => $ArrFinal,

                'surdemande' => $surdemande,

                'msgsurdemande' => $msgsurdemande

            )

        );*/

    }




    public function selectionAction($hotelid, $dated, $datef)

    {

        $User = $this->get('security.context')->getToken()->getUser();

       $hotels = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);



        $d1 = new \DateTime($dated);


        $d1->modify('+0 day');
        $datedd = $d1->format("Y-m-d");

        $d2 = new \DateTime($datef);

        $marge=$User->getHotelmarge();

        /*$marge = $this->getDoctrine()

            ->getRepository('BtobHotelBundle:Hotelmarge')

            ->getMargeByUser(,$d1);*/

        $usersite = $this->get('security.context')->getToken()->getUser();


        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriodeHotel($usersite,$hotels,$datedd);

        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($hotels,$datedd);

        if($promotion)
        {
            $promo = $promotion->getName();
            $datedpromo = $promotion->getDated();
            $datefpromo = $promotion->getDates();
            $valp = $promotion->getVal();
            $persp = $promotion->getPers();
        }
        else{
            $promo ="";
            $datedpromo ="";
            $datefpromo ="";
            $valp = "";
            $persp = "";
        }


        $margess = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByHotelAgence($hotels,$usersite);
        if($margess) {
            foreach ($margess as $margesp) {
                $margessprst = $margesp->getPrst();
                $margeesmarge = $margesp->getMarge();
            }
        }else{
            $margessprst =$User->getPrst();
            $margeesmarge = $User->getMarge();
        }


        if($periods)
        {
            foreach ($periods as $period) {
                $margessprst = $period->getPrst();
                $margeesmarge = $period->getMarge();
            }

        }

        if ($margessprst == true) {
            $margepers = 1;
        } else {
            $margepers = 0;
        }


        $margeval = $margeesmarge;


        $request = $this->get('request');

        $session = $this->getRequest()->getSession();

        //$hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $ds = new \DateTime($request->get('dated'));


        $nbjour = $d2->diff($d1);

        $nbjour = $nbjour->days - 1;

        $ad = $session->get("ad");

        $enf = $session->get("enf");

        $arr = $session->get("arr");

        $child = $session->get("children");
        $user = $User;

        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);


        $i = 0;

        $tabactualroom = array();

        foreach ($price as $key => $value) {


            $ArrFinal["majminstay"] = $value->getMajminstay();
            $ArrFinal["redminstay"] = $value->getRedminstay();
            $ArrFinal["persmajminstay"] = $value->getPersmajminstay();
            $ArrFinal["persredminstay"]  = $value->getPersredminstay();



            // calcule variante de marge

            foreach ($margess as $valm) {

                $margeval = $valm->getMarge();

                $margepers = $valm->getPrst();



            }

            if($periods)
            {
                foreach ($periods as $period) {
                    $margeval = $period->getPrst();
                    $margepers = $period->getMarge();
                }

            }

            //$filterhotel[] = $value->getHotel();

            $ArrFinal["hotel"] = $value->getHotel();

            $ArrFinal["hotelname"] = trim($value->getHotel()->getName());

            $ArrFinal["star"] = $value->getHotel()->getStar();

            $ArrFinal["pays"] = $value->getHotel()->getPays()->getId();

            $ArrFinal["ville"] = $value->getHotel()->getVille()->getId();

            // gestion des images

            $dataimg = $value->getHotel()->getHotelimg();

            $img = "/back/img/dummy_150x150.gif";

            $j = 0;

            foreach ($dataimg as $keyimg => $valimg) {

                if ($j == 0)

                    $img = $valimg->getFile();

                if ($valimg->getPriori())

                    $img = $valimg->getFile();

                ++$j;

            }

            $ArrFinal["image"] = $img;


            $min = 0;

            $j = 0;

            $tab = array();

            $name = "";

            $pers = false;

            $pricearr = array();

            foreach ($value->getPricearr() as $keyarr => $valarr) {

                if($valarr->getEtat()==1)
                {

                    if ($j == 0) {

                        $min = $valarr->getPrice();

                        $pers = $valarr->getPers();

                        $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                        $minstay = $valarr->getMinstay();
                    } else {

                        if ($min > $valarr->getPrice()) {

                            $min = $valarr->getPrice();

                            $pers = $valarr->getPers();

                            $name = $valarr->getHotelarrangement()->getArrangement()->getName();

                        }

                    }

                    $pricearr[$j]["name"] = $valarr->getHotelarrangement()->getArrangement()->getName();
                    $pricearr[$j]["minstay"] = $valarr->getMinstay();

                    $pricearr[$j]["price"] = $valarr->getPrice();
					$pricearr[$j]["prstt"] = $valarr->getPers();

                    $pricearr[$j]["price"] = Tools::calculemarge($pricearr[$j]["price"], $margepers, $margeval); // calcule de marge

                    $pricearr[$j]["id"] = $valarr->getId();

                    $pricearr[$j]["idarr"] = $valarr->getHotelarrangement()->getArrangement()->getId();







                    ++$j;
                }
            }

            $pricearr = Tools::array_sort($pricearr, 'price', SORT_ASC);

            $ArrFinal["pricearr"] = $pricearr;

            if ($pers) {

                $ArrFinal["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

            } else {

                $ArrFinal["price"] = $min + $value->getPrice();

            }

            $ArrFinal["price"] = Tools::calculemarge($ArrFinal["price"], $margepers, $margeval); // calcule de marge

            $ArrFinal["name"] = $name;



            // gestion des chambre

            $k = 0;

            //Tools::dump($ad);

            foreach ($ad as $kad => $valad) {

                $nbpersonne = $valad + $enf[$kad];

                //if ($nbpersonne > 4) $nbpersonne = 4;

                $room = null;

                // test dispo room

                $hotelroom = $value->getHotel()->getHotelroom();

                $typepersonne = false;

                foreach ($hotelroom as $khr => $vhr) {

                    if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                        $room = $vhr->getRoom();

                        $tabactualroom[] = $room->getId();

                        $typepersonne = $vhr->getPersonne();



                    }

                }






                //test dispo arrangement

                $arrang = null;

                $hotelarrangement = $value->getHotel()->getHotelarrangement();
                $arr = $session->get("arr");

                $zz=array();
                foreach  ($value->getPricearr() as $keyarr => $valarr) {


                    if ($valarr->getHotelarrangement()->getArrangement()->getId() == $arr[$kad]) {
                        if($valarr->getEtat()==1)
                        {
                            $arrang =$valarr->getHotelarrangement()->getArrangement();

                        }


                    }

                }

                if($arrang==null)
                {
                    $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find($valarr->getHotelprice()->getId());
                    $pricecopc = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findFirstArrang($pricearrs);

                    foreach  ($pricecopc as $pricecopcar => $valpricecopc) {

                        $arrang =$valpricecopc->getHotelarrangement()->getArrangement();
                        $msg="*";

                    }
                }
                $taboneitem = array();

                if ($room != null && $arrang != null) {

                    $taboneitem = array(

                        "nbpersonne" => $nbpersonne,

                        "roomname" => $room->getName(),

                        "arrangement" => $arrang->getName(),

                        "ad" => $valad,

                        "enf" => $enf[$kad],

                        "arr" => $arr[$kad]

                    );



                    foreach ($hotelroom as $khr => $vhr) {

                        if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                            $taboneitem["id"] = $vhr->getId();

                            $taboneitem["nbad"] = $vhr->getAd();

                            $taboneitem["nbenf"] = $vhr->getEnf();

                            $taboneitem["typepersonne"] = $vhr->getPersonne();

                        }

                    }

                    // get supplement



                    $suppl = $value->getHotel()->getHotelroomsup();

                    $supplement = $value->getPricesupplement();



                    foreach ($suppl as $ksup => $vsup) {

                        if ($vsup->getRoom()->getId() == $room->getId()) {

                            $xtab = array();// ce tableau pour le croisement de price sup et hotel sup

                            foreach ($supplement as $valsupplement) {

                                if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

                                    $xtab[0] = $valsupplement->getPrice();

                                    if ($valsupplement->getPers()) {

                                        $xtab[0] = ($value->getPrice() * $valsupplement->getPrice()) / 100;

                                        $xtab[0] = Tools::calculemarge($xtab[0], $margepers, $margeval); // calcule de marge

                                    }

                                    $xtab[1] = $valsupplement->getId();

                                }

                            }

                            $taboneitem["supplement"][] = array($vsup, $xtab);

                        } else {

                            $taboneitem["supplement"][] = array();

                        }

                    }

                    // get events

                    $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

                    $tabev = array();

                    foreach ($events as $kev => $valev) {

                        $tabev[] = $valev;

                    }

                    $taboneitem['events'] = $tabev;

                    $ArrFinal["allprice"][$room->getId()][] = $taboneitem;

                    ++$k;



                }

                // affichage des autres chambres



                foreach ($value->getHotel()->getHotelroom() as $vrestroom) {

                    if (!in_array($vrestroom->getRoom()->getId(), $tabactualroom)) {

                        $roomother = $vrestroom->getRoom();

                        $nbpersonne = $roomother->getCapacity();

                        if ($roomother != null && $arrang != null) {

                            $taboneitem = array(

                                "nbpersonne" => $nbpersonne,

                                "roomname" => $roomother->getName(),

                                "arrangement" => $arrang->getName(),

                                "ad" => $valad,

                                "enf" => $enf[$kad],

                                "arr" => $arr[$kad]

                            );



                            foreach ($hotelroom as $khr => $vhr) {

                                if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

                                    $taboneitem["id"] = $vhr->getId();

                                    $taboneitem["nbad"] = $vhr->getAd();

                                    $taboneitem["nbenf"] = $vhr->getEnf();

                                }

                            }

                            // get supplement

                            $suppl = $value->getHotel()->getHotelroomsup();

                            $supplement = $value->getPricesupplement();

                            foreach ($suppl as $ksup => $vsup) {

                                if ($vsup->getRoom()->getId() == $roomother->getId()) {

                                    $xtab = array();// ce tableau pour le croisement de price sup et hotel sup

                                    foreach ($supplement as $valsupplement) {

                                        if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

                                            $xtab[0] = $valsupplement->getPrice();

                                            if ($valsupplement->getPers()) {

                                                $xtab[0] = ($value->getPrice() * $valsupplement->getPrice()) / 100;

                                                $xtab[0] = Tools::calculemarge($xtab[0], $margepers, $margeval); // calcule de marge

                                            }

                                            $xtab[1] = $valsupplement->getId();

                                        }

                                    }

                                    $taboneitem["supplement"][] = array($vsup, $xtab);

                                } else {

                                    $taboneitem["supplement"][] = array();

                                }

                            }

                            // get events

                            $events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

                            $tabev = array();

                            foreach ($events as $kev => $valev) {

                                $tabev[] = $valev;

                            }

                            $taboneitem['events'] = $tabev;

                            $ArrFinal["allotherprice"][$roomother->getId()][] = $taboneitem;

                            ++$k;



                        }

                    }

                }

                //Tools::dump($tabactualroom,true);

            }

            // Stop Sales

            $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);

            $ArrFinal["stopsales"] = count($stopsales);

            //Tools::dump($ArrFinal,true);



        }



        $idhotel=$ArrFinal['hotel']->getId();


        return $this->render('BtobHotelBundle:List:selection.html.twig', array(

               "hotelid" => $hotelid,

                "dated" => $d1,

                "datef" => $d2,

                'price' => $ArrFinal,

                'idhotel' => $idhotel,

                'nbjour' => $nbjour,

                'ad' => $ad,

                'enf' => $enf,

                'arr' => $arr,
            
                'child' => $child,

                'margeval' => $margeval,

                'margepers' => $margepers

            )

        );

    }



    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $marge = $user->getHotelmarge();
        $agence = $this->get('security.context')->getToken()->getUser();
        $agenceact =$agence->getId();
        $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($user->getId());
         
    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($user->getId());

    if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
        $tab=array();
        foreach($solde as $value){
            if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                $tab[]=$value;
            }
        }
        $solde=$tab;

    }
    $solde=array_reverse($solde);
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();
        $value = $request->get('_route_params');
        // valeur de submit
        $nbpage = 5;
        $nbjour = 1;
        $paysid = "3";
        $ville = "";
        $nom = "";
        $trie = 1;
        $price_min = 0;
        $price_max = 3000;
        $star = 0;
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $datedd = $dt->format("Y-m-d");
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $page = 1;
        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();
        $tri = $value["var"];
        if($value["var"]==null)
        {
            $tri = "nom" ;
        }
        $totpage = 1;
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => 3);


        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriode($agence,$datedd);

        $marges = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->listUser($agence);





        $margeval = $agence->getMarge();
        $margepers = $agence->getPrst();
        if($margepers==true)
        {
            $margepers = 1;
        }
        else{
            $margepers = 0;
        }




        
        $paysid = $request->request->get("pays");

        $session->set('paysid', $paysid);
        $nom = $request->request->get("nom");
        $session->set('nom', $nom);
        $ville = $request->request->get("ville");
        $session->set('ville', $ville);
        $trie = $request->request->get("trie");
        $session->set('trie', $trie);
        $tri = $tri;
        $session->set('tri', $tri);
        $price_min = 0;
        $session->set('price_min', $price_min);
        $price_max = 3000;
        $session->set('price_max', $price_max);
        $star = 0;
        $session->set('star', $star);
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $dt->format("d/m/Y");
        $datedd = $dt->format("Y-m-d");
        $session->set('dated', $dated);
        $dt->modify('+1 day');
        $datef = $dt->format("d/m/Y");
        $session->set('datef', $datef);
        $page = 1;
        $session->set('page', $page);
        $ad = array(0 => 2);
        $session->set('ad', $ad);

        $enf = array(0 => 0);
        $session->set('enf', $enf);
        $arr = array(0 => 3);
        $session->set('arr', $arr);
        //Tools::dump($request->request, true);
        // recupération de hotel price
        $tab = explode("/", $dated);

        $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);

        $tab2 = explode("/", $datef);
        $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);

        // calculer la deference entre dated et dates
        $nbjour = $d2->diff($d1);
        $nbjour = $nbjour->days;
        $user = $this->get('security.context')->getToken()->getUser();
        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
        $i = 0;
        $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriode($user,$datedd);

        $tabactualroom = array();

        foreach ($price as $key => $value) {

            if ($value->getHotel()->getAct()) {

                $sstopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:Stopsales')->findbyhoteldate($value->getHotel(),$d1, $d2);

                if(count($sstopsales)<1)
                {
                    $surdemande = false;
                    if ($value->getNbnuit() > $nbjour) {
                        $surdemande = true;
                    }
                    //$filterhotel[] = $value->getHotel();
                    $ArrFinal[$i]["hotel"] = $value->getHotel();
                    $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                    $ArrFinal[$i]['majminstay'] = $value->getMajminstay();
                    $ArrFinal[$i]['redminstay'] = $value->getRedminstay();
                    $ArrFinal[$i]['persmajminstay'] = $value->getPersmajminstay();
                    $ArrFinal[$i]['persredminstay'] = $value->getPersredminstay();

                    $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(),$datedd);
                    if($promotion)
                    {
                        $ArrFinal[$i]['promo'] = $promotion->getName();
                        $ArrFinal[$i]['datedpromo'] = $promotion->getDated();
                        $ArrFinal[$i]['datefpromo'] = $promotion->getDates();
                        $ArrFinal[$i]['valp'] = $promotion->getVal();
                        $ArrFinal[$i]['persp'] = $promotion->getPers();
                    }
                    else{
                        $ArrFinal[$i]['promo'] ="";
                        $ArrFinal[$i]['datedpromo'] ="";
                        $ArrFinal[$i]['datefpromo'] ="";
                        $ArrFinal[$i]['valp'] = "";
                        $ArrFinal[$i]['persp'] = "";
                    }
                    // calcule variante de marge
                    foreach ($marges as $valm) {
                        if($valm->getHotel()->getId()==$value->getHotel()->getId())
                        {
                            $margeval = $valm->getMarge();
                            $xx = $valm->getPrst();
                            if($xx==true)
                            {
                                $margepers = 1;
                            }
                            else{
                                $margepers = 0;
                            }

                        }

                    }
                    $exist = false;
                    foreach ($periods as $valp) {
                        if($valp->getHotel()->getId()==$value->getHotel()->getId())
                        {
                            $exist =true;
                            if($exist==true)
                            {
                                $margeval = $valp->getMarge();

                                $xx = $valp->getPrst();
                                if($xx==true)
                                {
                                    $margepers = 1;
                                }
                                else{
                                    $margepers = 0;
                                }
                            }
                        }

                    }
                    $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                    $ArrFinal[$i]["hotelid"] =intval($value->getHotel()->getId());

                    $ArrFinal[$i]["margepers"] =$margepers;
                    $ArrFinal[$i]["margeval"] =$margeval;


                    $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                    $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                    $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                    // gestion des images
                    $dataimg = $value->getHotel()->getHotelimg();
                    $img = "/back/img/dummy_150x150.gif";
                    $j = 0;
                    foreach ($dataimg as $keyimg => $valimg) {
                        if ($j == 0)
                            $img = $valimg->getFile();
                        if ($valimg->getPriori())
                            $img = $valimg->getFile();
                        ++$j;
                    }
                    $ArrFinal[$i]["image"] = $img;
                    /*
                     * gestion des prix
                     */
                    // minimaume d'arrangement
                    $min = 0;
                    $j = 0;
                    $tab = array();
                    $name = "";
                    $pers = false;
                    $etat = false;
                    foreach ($value->getPricearr() as $keyarr => $valarr) {
                        if ($valarr->getEtat() == 1) {
                            if ($valarr->getMinstay() > $nbjour) {
                                $surdemande = true;
                            }
                            if ($j == 0) {
                                $min = $valarr->getPrice();
                                $pers = $valarr->getPers();
                                $etat = $valarr->getEtat();
                                $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                $margeprice = $valarr->getMarge();
                                $persmprice = $valarr->getPersm();
                            } else {
                                if ($min > $valarr->getPrice()) {
                                    $min = $valarr->getPrice();
                                    $pers = $valarr->getPers();
                                    $etat = $valarr->getEtat();
                                    $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                    $margeprice = $valarr->getMarge();
                                    $persmprice = $valarr->getPersm();
                                }
                            }
							
							$pricearr[$j]["margeprice"] = $margeprice;
							$pricearr[$j]["persmprice"] = $persmprice;
							$pricearr[$j]["name"] = $valarr->getHotelarrangement()->getArrangement()->getName();
							$pricearr[$j]["minstay"] = $valarr->getMinstay();

							$pricearr[$j]["price"] = $valarr->getPrice();
							$pricearr[$j]["pers"] = $valarr->getPers();
							$pricearr[$j]["persm"] = $valarr->getPersm();
							 $pricearr[$j]["marge"] = $valarr->getMarge();

							$pricearr[$j]["price"] = $pricearr[$j]["price"]; // calcule de marge

							$pricearr[$j]["id"] = $valarr->getId();

							$pricearr[$j]["idarr"] = $valarr->getHotelarrangement()->getArrangement()->getId();
                            ++$j;
                        }

                    }

					$pricearr = Tools::array_sort($pricearr, 'price', SORT_ASC);

					$ArrFinal[$i]["pricearr"] = $pricearr;
					
                    if ($pers) {
                        $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

                    } else {
                        $ArrFinal[$i]["price"] = $min + $value->getPrice();
                    }
					
					
					
					
                    // calcule de la marge
                    $ArrFinal[$i]["price"] = Tools::calculemarge($ArrFinal[$i]["price"], $margepers, $margeval);
                    $ArrFinal[$i]["name"] = $name;
                    $ArrFinal[$i]["margeprice"] = $margeprice;
                    $ArrFinal[$i]["persmprice"] = $persmprice;
            
					$ArrFinal[$i]["sperssupsingle"] = $value->getPerssupsingle();
					$ArrFinal[$i]["ssupsingle"] = $value->getSupsingle();
					$ArrFinal[$i]["smargess"] = $value->getMargess();
					$ArrFinal[$i]["spersss"] = $value->getPersss();

                    // gestion des chambre
                    $k = 0;
                    //Tools::dump($ad);
                    foreach ($ad as $kad => $valad) {
                        $nbpersonne = $valad + $enf[$kad];
                        //if ($nbpersonne > 4) $nbpersonne = 4;
                        $room = null;
                        // test dispo room
                        $hotelroom = $value->getHotel()->getHotelroom();
                        $typepersonne = false;
                        foreach ($hotelroom as $khr => $vhr) {
                            if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                $room = $vhr->getRoom();
                                $typepersonne = $vhr->getPersonne();
                            }
                        }
                        //test dispo arrangement
                        $arrang = null;
                        $hotelarrangement = $value->getHotel()->getHotelarrangement();

                        foreach  ($value->getPricearr() as $keyarr => $valarr) {


                            if($valarr->getEtat()==1 && $valarr->getPrice()==0)
                            {
                                $arrang =$valarr->getHotelarrangement()->getArrangement();
                            }




                        }
                        if ($room != null && $arrang != null) {
                            foreach ($hotelroom as $khr => $vhr) {
                                if ($room->getId() == $vhr->getRoom()->getId()) {
                                    $idform = $vhr->getId();
                                }
                            }
                            $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                            $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                            $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                            foreach ($value->getPricearr() as $kk => $vv) {
                                if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                    $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                    $ArrFinal[$i]["allprice"][$k]["minstay"] = $vv->getMinstay();
                                    $ArrFinal[$i]["allprice"][$k]["etat"] = $vv->getEtat();
                                }
                            }

                            //Disponiblité
                            $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                            $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($value->getId(),$room->getId());

                            $valres = $value->getRetro()-1;
                            $now =new \DateTime();
                            $res =$now->modify('+1 day');
                            $dres = $res->modify('+'.$valres. 'day');
                            $nows =new \DateTime();
                            $ress =$nows->modify('+1 day');

                            if($nbjour>=$ArrFinal[$i]["allprice"][$k]["minstay"] && $disroom[0]->getQte() > 0 && $dres->format("Ymd") <= $ress->format("Ymd") )
                            {
                                $ArrFinal[$i]["allprice"][$k]["dispo"]="1";

                            }
                            else{
                                $ArrFinal[$i]["allprice"][$k]["dispo"]="0";

                            }

                            //fin

                            $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                            $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                            $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;
							
							foreach ($hotelroom as $khr => $vhr) {

								if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

									$ArrFinal[$i]["allprice"][$k]["nbad"] = $vhr->getAd();

									$ArrFinal[$i]["allprice"][$k]["nbenf"] = $vhr->getEnf();

									$ArrFinal[$i]["allprice"][$k]["typepersonne"] = $vhr->getPersonne();

									$ArrFinal[$i]["allprice"][$k]["arr"] = $arr[$kad];

								}

							}

                            //$calc=new Calculate();
                            $dtest = null;
                            $dtest = $d1;
                            // initialiser l'age des enfant a 6 s'il existe
                            // il faut les changer s'il y a un autre age d'enfant
                            $tabenf = array();
                            for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                                $tabenf[] = $value->getHotel()->getMaxenfant();
                            }
                            $valp = $ArrFinal[$i]['valp'];
                            $vals =   $ArrFinal[$i]['persp'] ;

                            $prix = $this->getDoctrine()
                                ->getRepository('BtobHotelBundle:Hotelprice')
                                ->calcultatea($valp,$vals,$user, $dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne, $margeval, $margepers);

                            $prix = Tools::calculemarge($prix, $margepers, $margeval); // calcule de marge

                            $ArrFinal[$i]["allprice"][$k]["price"] = $prix;

                            if ($prix == 0) {
                                $surdemande = true;
                            }
							
							// get supplement
							$suppl = $value->getHotel()->getHotelroomsup();

							$supplement = $value->getPricesupplement();


							foreach ($suppl as $ksup => $vsup) {

								if ($vsup->getRoom()->getId() == $room->getId()) {

									$xtab = array();// ce tableau pour le croisement de price sup et hotel sup

									foreach ($supplement as $valsupplement) {

										if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

											if ($valsupplement->getPers()) {
											 
												if($persp)
												{
																							 

												
												if($valsupplement->getPersm())
												  {
												 $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ 
														 (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);      
												  }else{
												   $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();    
												  }
												}
												else{
												if($valsupplement->getPersm())
												  {
												$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
														  (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
												  }else{
												$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
												  }
												}

												$xtab[0] = $xtab[0]; // calcule de marge

											}
											else{
											   
											   
											   if($valsupplement->getPersm())
												  {
													  $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100); 
												  }else{
													  $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge(); 
												  }
											}

											$xtab[1] = $valsupplement->getId();

										}

									}

									$ArrFinal[$i]["allprice"][$k]["supplement"][] = array($vsup, $xtab);

								} else {

									$ArrFinal[$i]["allprice"][$k]["supplement"][] = array();

								}

							}
							

							// get events
							$events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);
							$tabev = array();
							foreach ($events as $kev => $valev) {
								$tabev[] = $valev;
							}
							$ArrFinal[$i]["allprice"][$k]['events'] = $tabev;
                            ++$k;
                        }

						// affichage des autres chambres
						foreach ($value->getHotel()->getHotelroom() as $vrestroom) {

							if (!in_array($vrestroom->getRoom()->getId(), $tabactualroom)) {

								$roomother = $vrestroom->getRoom();

								$nbpersonne = $roomother->getCapacity();

								if ($roomother != null && $arrang != null) {

									$ArrFinal[$i]["allotherprice"][$k]["nbpersonne"] = $nbpersonne;
									$ArrFinal[$i]["allotherprice"][$k]["roomname"] = $roomother->getName();
									$ArrFinal[$i]["allotherprice"][$k]["arrangement"] = $arrang->getName();
									$ArrFinal[$i]["allotherprice"][$k]["ad"] = $valad;
									$ArrFinal[$i]["allotherprice"][$k]["enf"] = $enf[$kad];
									$ArrFinal[$i]["allotherprice"][$k]["arr"] = $arr[$kad];



									foreach ($hotelroom as $khr => $vhr) {

										if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

											$ArrFinal[$i]["allotherprice"][$k]["id"] = $vhr->getId();

											$ArrFinal[$i]["allotherprice"][$k]["nbad"] = $vhr->getAd();

											$ArrFinal[$i]["allotherprice"][$k]["nbenf"] = $vhr->getEnf();

										}

									}

									// get supplement

									$suppl = $value->getHotel()->getHotelroomsup();

									$supplement = $value->getPricesupplement();

									foreach ($suppl as $ksup => $vsup) {

										if ($vsup->getRoom()->getId() == $roomother->getId()) {

											$xtab = array();// ce tableau pour le croisement de price sup et hotel sup

											foreach ($supplement as $valsupplement) {

												if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

												   if ($valsupplement->getPers()) {
											 
												if($persp)
												{
																							 

												
												if($valsupplement->getPersm())
												  {
												 $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ 
														 (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);      
												  }else{
												   $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();    
												  }
												}
												else{
												if($valsupplement->getPersm())
												  {
												$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
														  (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
												  }else{
												$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
												  }
												}

												$xtab[0] = $xtab[0]; // calcule de marge

											}
											else{
											   
											   
											   if($valsupplement->getPersm())
												  {
													  $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100); 
												  }else{
													  $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge(); 
												  }
											}

													$xtab[1] = $valsupplement->getId();

												}

											}

											$ArrFinal[$i]["allotherprice"][$k]["supplement"][] = array($vsup, $xtab);

										} else {

											$ArrFinal[$i]["allotherprice"][$k]["supplement"][] = array();

										}

									}

									// get events

									$events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

									$tabev = array();

									foreach ($events as $kev => $valev) {

										$tabev[] = $valev;

									}

									$ArrFinal[$i]["allotherprice"][$k]['events'] = $tabev;

									++$k;



								}

							}

						}
						
                    }
                    // Stop Sales
                    $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                    $ArrFinal[$i]["stopsales"] = count($stopsales);
                    if ($surdemande) {
                        $ArrFinal[$i]["stopsales"] = 1;
                    }
                    //Tools::dump($request, true);
                    ++$i;
                }

            }
        }



        $ArrFinal1 = $ArrFinal;

        $ArrFinal = array();
        foreach ($ArrFinal1 as $key => $value) {
            if (isset($value["allprice"])) {
                $ArrFinal[] = $value;
            }
        }
        // filtre de prix


        // filter $star
        $ArrFinal1 = $ArrFinal;
        if ($tri == "nom") {
            $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
        } else if ($tri == "prix") {
            $ArrFinal = Tools::array_sort($ArrFinal, "price", SORT_ASC);
        } else if ($tri == "etoil") {
            $ArrFinal = Tools::array_sort($ArrFinal, "star", SORT_ASC);
        }

        // filter pays

        // filter ville

        // filter name hotel

        //Filter trie

        /*
         * pagination
         */
        $totpage = ceil(count($ArrFinal) / $nbpage);
        $ArrFinal1 = $ArrFinal;
        $ArrFinal = array();
        $i = 0;
        $start = $nbpage * ($page - 1);
        $end = $nbpage * $page;
        if ($start > count($ArrFinal1)) {
            $start = 0;
            $page = 1;
            $end = $nbpage;
        }
        foreach ($ArrFinal1 as $key => $value) {
            if ($i >= $start && $i < $end) {
                $ArrFinal[] = $value;
            }
            ++$i;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $ArrFinal1,
            $request->query->get('page', 1)/*page number*/,
            5
        );
        $d1 = new \DateTime(Tools::explodedate($dated, '/'));
        $d2 = new \DateTime(Tools::explodedate($datef, '/'));

        $userid = $user->getId();

        $hotelmarges= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);



        return $this->render('BtobHotelBundle:List:index.html.twig', array(
                'pays' => $pays,
                'paysid' => $paysid,
                'ville' => $ville,
                'trie' => $trie,
                'user' => $user,
                'pagination' => $pagination,
                'price_min' => $price_min,
                'price_max' => $price_max,
                'star' => $star,
                'dated' => $dated,
                'datef' => $datef,
                'nom' => $nom,
                'page' => $page,
                'price' => $ArrFinal,
                'filterhotel' => $filterhotel,
                'totpage' => $totpage,
                'arr' => $arrangement,
                'nbjour' => $nbjour,
                'frmad' => $ad,
                'frmenf' => $enf,
                'frmarr' => $arr,
                'd1' => $d1,
                'd2' => $d2,
                'margeval' => $margeval,
                'margepers' => $margepers,
                'hotelmarges' => $hotelmarges,
                'entitiessolde'=>$solde,
            )
        );
    }


    public function searchAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $marge = $user->getHotelmarge();
        $agence = $this->get('security.context')->getToken()->getUser();
        $agenceact =$agence->getId();



        $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        /*
         *  donner de formulaire
         */
        // list des arrangement
        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays
        $pays = $this->getDoctrine()->getRepository('BtobHotelBundle:Pays')->findActiv();

        // valeur de submit
        $nbpage = 5;
        $nbjour = 1;
        $paysid = $request->request->get("pays");
        $ville = $request->request->get("ville");
        $nom = $request->request->get("nom");
        $theme = intval($request->request->get("theme"));

        $trie = $request->request->get("ville");
        $try = $request->request->get("testinput");
        $price_min = $request->request->get("price_min");
        $price_max = $request->request->get("price_max");
        $star = $request->request->get("star");
        if($star==0)
        {
            $star =0;
        }
        $dt = new \DateTime();
        $dt->modify('+1 day');
        $dated = $request->request->get("dated");
        $dt->modify('+1 day');
        $datef = $request->request->get("datef");
        $page = 1;
        $ArrFinal = array();
        $filterhotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->findActiv();
        $totpage = 1;

        $ad = $request->request->get("ad");
        $enf = $request->request->get("enf");
        $arr = $request->request->get("arrr");

        $children = $request->request->get("children");


        //$periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriode($agence,$dated);

        $marges = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->listUser($agence);





        $margeval = $agence->getMarge();
        $margepers = $agence->getPrst();
        if($margepers==true)
        {
            $margepers = 1;
        }
        else{
            $margepers = 0;
        }

        if ($request->getMethod() == 'POST') {


            //Tools::dump($request->request);
            $paysid = $request->request->get("pays");
            $session->set('paysid', $paysid);
            $nom = $request->request->get("nom");
            $session->set('nom', $nom);
            $ville = $request->request->get("ville");
            $session->set('ville', $ville);
            $trie = $request->request->get("trie");
            $session->set('trie', $trie);
            $price_min = $request->request->get("price_min");
            $session->set('price_min', $price_min);
            $price_max = $request->request->get("price_max");
            $session->set('price_max', $price_max);
            $star = $request->request->get("star");
            if($star==0)
            {
                $star =0;
            }
            $session->set('star', $star);
            $dated = $request->request->get("dated");
            $session->set('dated', $dated);
            $datef = $request->request->get("datef");
            $session->set('datef', $datef);
            $page = $request->request->get("page");
            $session->set('page', $page);
            $ad = $request->request->get("ad");
            $session->set('ad', $ad);
            $enf = $request->request->get("enf");
            $session->set('enf', $enf);
            $children = $request->request->get("children");
            $session->set('children', $children);
            $arr = $request->request->get("arr");
            $try = $request->request->get("testinput");
            $session->set('testinput', $try);

            $session->set('arr', $arr);

            //Tools::dump($request->request, true);
            // recupération de hotel price
            $tab = explode("/", $dated);

            $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);
            $tab2 = explode("/", $datef);
            $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);
            $d1->modify('+0 day');
            $datedd = $d1->format("Y-m-d");



            $marges = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Hotelmarge')
                ->listUser($agence);

            $periods = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelmargeperiode')->listUsersPeriode($agence,$datedd);

            $nbjour = $d2->diff($d1);
            $nbjour = $nbjour->days;
            $user = $this->get('security.context')->getToken()->getUser();
            $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);
            $i = 0;
			$tabactualroom = array();

            foreach ($price as $key => $value) {

                if ($value->getHotel()->getAct()) {

                    $sstopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:Stopsales')->findbyhoteldate($value->getHotel(),$d1, $d2);

                    if(count($sstopsales)<1)
                    {


                        $surdemande = false;
                        if ($value->getNbnuit() > $nbjour) {
                            $surdemande = true;
                        }
                        //$filterhotel[] = $value->getHotel();
                        $ArrFinal[$i]["hotel"] = $value->getHotel();
                        $ArrFinal[$i]['hotelpriceid'] = $value->getId();
                        $ArrFinal[$i]['majminstay'] = $value->getMajminstay();
                        $ArrFinal[$i]['redminstay'] = $value->getRedminstay();
                        $ArrFinal[$i]['persmajminstay'] = $value->getPersmajminstay();
                        $ArrFinal[$i]['persredminstay'] = $value->getPersredminstay();
                        $ArrFinal[$i]['retro'] = $value->getRetro();
                        $promotion = $this->getDoctrine()->getRepository('BtobHotelBundle:Promotion')->listPromoHotel($value->getHotel(),$datedd);
                        if($promotion)
                        {
                            $ArrFinal[$i]['promo'] = $promotion->getName();
                            $ArrFinal[$i]['datedpromo'] = $promotion->getDated();
                            $ArrFinal[$i]['datefpromo'] = $promotion->getDates();
                            $ArrFinal[$i]['valp'] = $promotion->getVal();
                            $ArrFinal[$i]['persp'] = $promotion->getPers();
                        }
                        else{
                            $ArrFinal[$i]['promo'] ="";
                            $ArrFinal[$i]['datedpromo'] ="";
                            $ArrFinal[$i]['datefpromo'] ="";
                            $ArrFinal[$i]['valp'] = "";
                            $ArrFinal[$i]['persp'] = "";
                        }

                        // calcule variante de marge




                        foreach ($marges as $valm) {
                            if($valm->getHotel()->getId()==$value->getHotel()->getId())
                            {
                                $margeval = $valm->getMarge();

                                $xx = $valm->getPrst();
                                if($xx==true)
                                {
                                    $margepers = 1;
                                }
                                else{
                                    $margepers = 0;
                                }

                            }




                        }
                        $exist = false;
                        foreach ($periods as $valp) {
                            if($valp->getHotel()->getId()==$value->getHotel()->getId())
                            {
                                $exist =true;
                                if($exist==true)
                                {
                                    $margeval = $valp->getMarge();

                                    $xx = $valp->getPrst();
                                    if($xx==true)
                                    {
                                        $margepers = 1;
                                    }
                                    else{
                                        $margepers = 0;
                                    }
                                }
                            }

                        }


                        $ArrFinal[$i]["hotelname"] = trim($value->getHotel()->getName());
                        $ArrFinal[$i]["hotelid"] =intval($value->getHotel()->getId());

                        $ArrFinal[$i]["margepers"] =$margepers;
                        $ArrFinal[$i]["margeval"] =$margeval;

                        $ArrFinal[$i]["star"] = $value->getHotel()->getStar();
                        $ArrFinal[$i]["pays"] = $value->getHotel()->getPays()->getId();
                        $ArrFinal[$i]["ville"] = $value->getHotel()->getVille()->getId();
                        // gestion des images
                        $dataimg = $value->getHotel()->getHotelimg();
                        $img = "/back/img/dummy_150x150.gif";
                        $j = 0;
                        foreach ($dataimg as $keyimg => $valimg) {
                            if ($j == 0)
                                $img = $valimg->getFile();
                            if ($valimg->getPriori())
                                $img = $valimg->getFile();
                            ++$j;
                        }
                        $ArrFinal[$i]["image"] = $img;
                        /*
                         * gestion des prix
                         */
                        // minimaume d'arrangement
                        $min = 0;
                        $j = 0;
                        $tab = array();
                        $name = "";
                        $pers = false;
                        $etat = false;
                        foreach ($value->getPricearr() as $keyarr => $valarr) {
                            if ($valarr->getEtat() == 1) {
                                if ($valarr->getMinstay() > $nbjour) {
                                    $surdemande = true;
                                }
                                if ($j == 0) {
                                    $min = $valarr->getPrice();

                                    $pers = $valarr->getPers();
                                    $etat = $valarr->getEtat();
                                    $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                    $margeprice = $valarr->getMarge();
                                    $persmprice = $valarr->getPersm();
                                } else {
                                    if ($min > $valarr->getPrice()) {
                                        $min = $valarr->getPrice();
                                        $pers = $valarr->getPers();
                                        $etat = $valarr->getEtat();
                                        $name = $valarr->getHotelarrangement()->getArrangement()->getName();
                                        $margeprice = $valarr->getMarge();
                                        $persmprice = $valarr->getPersm();
                                    }
                                }
							
								$pricearr[$j]["margeprice"] = $margeprice;
								$pricearr[$j]["persmprice"] = $persmprice;
								$pricearr[$j]["name"] = $valarr->getHotelarrangement()->getArrangement()->getName();
								$pricearr[$j]["minstay"] = $valarr->getMinstay();

								$pricearr[$j]["price"] = $valarr->getPrice();
								$pricearr[$j]["pers"] = $valarr->getPers();
								$pricearr[$j]["persm"] = $valarr->getPersm();
								 $pricearr[$j]["marge"] = $valarr->getMarge();

								$pricearr[$j]["price"] = $pricearr[$j]["price"]; // calcule de marge

								$pricearr[$j]["id"] = $valarr->getId();

								$pricearr[$j]["idarr"] = $valarr->getHotelarrangement()->getArrangement()->getId();
                                ++$j;
                            }
                        }

						$pricearr = Tools::array_sort($pricearr, 'price', SORT_ASC);

						$ArrFinal[$i]["pricearr"] = $pricearr;
                        if ($pers) {
                            $ArrFinal[$i]["price"] = $value->getPrice() + (($value->getPrice() * $min) / 100);

                        } else {
                            $ArrFinal[$i]["price"] = $min + $value->getPrice();
                        }
                        // calcule de la marge
                        $ArrFinal[$i]["price"] = Tools::calculemarge($ArrFinal[$i]["price"], $margepers, $margeval);
                        $ArrFinal[$i]["name"] = $name;
                        $ArrFinal[$i]["margeprice"] = $margeprice;
                        $ArrFinal[$i]["persmprice"] = $persmprice;
						
						$ArrFinal[$i]["sperssupsingle"] = $value->getPerssupsingle();
						$ArrFinal[$i]["ssupsingle"] = $value->getSupsingle();
						$ArrFinal[$i]["smargess"] = $value->getMargess();
						$ArrFinal[$i]["spersss"] = $value->getPersss();

                        // gestion des chambre
                        $k = 0;
                        //Tools::dump($ad);
                        $iter =0;
                        foreach ($ad as $kad => $valad) {
                            $int= $iter;
                            $nbpersonne = $valad + $enf[$kad];
                            //if ($nbpersonne > 4) $nbpersonne = 4;
                            $room = null;
                            // test dispo room
                            $hotelroom = $value->getHotel()->getHotelroom();
                            $typepersonne = false;
                            foreach ($hotelroom as $khr => $vhr) {
                                if ($vhr->getRoom()->getCapacity() == $nbpersonne) {
                                    $room = $vhr->getRoom();
                                    $typepersonne = $vhr->getPersonne();
                                }
                            }
                            //test dispo arrangement
                            $arrang = null;
                            $hotelarrangement = $value->getHotel()->getHotelarrangement();

                            foreach  ($value->getPricearr() as $keyarr => $valarr) {

                                if ($valarr->getHotelarrangement()->getArrangement()->getId() == $arr[$kad]) {
                                    if($valarr->getEtat()==1)
                                    {
                                        $arrang =$valarr->getHotelarrangement()->getArrangement();
                                        $msg="";
                                    }


                                }

                            }
                            if($arrang==null)
                            {
                                $pricearrs = $this->getDoctrine()->getRepository("BtobHotelBundle:Hotelprice")->find(intval($ArrFinal[$i]['hotelpriceid']));
                                $pricecopc = $this->getDoctrine()->getRepository("BtobHotelBundle:Pricearr")->findFirstArrang($pricearrs);

                                foreach  ($pricecopc as $pricecopcar => $valpricecopc) {

                                    $arrang =$valpricecopc->getHotelarrangement()->getArrangement();
                                    $msg="*";

                                }
                            }
                            if ($room != null && $arrang != null) {
                                foreach ($hotelroom as $khr => $vhr) {
                                    if ($room->getId() == $vhr->getRoom()->getId()) {
                                        $idform = $vhr->getId();
                                    }
                                }
                                $ArrFinal[$i]["allprice"][$k]["nbpersonne"] = $nbpersonne;
                                $ArrFinal[$i]["allprice"][$k]["roomname"] = $room->getName();
                                $ArrFinal[$i]["allprice"][$k]["arrangement"] = $arrang->getName();
                                $ArrFinal[$i]["allprice"][$k]["msg"] = $msg;
                                foreach ($value->getPricearr() as $kk => $vv) {
                                    if ($vv->getHotelarrangement()->getArrangement()->getId() == $arrang->getId()) {
                                        $ArrFinal[$i]["allprice"][$k]["arrangementid"] = $vv->getId();
                                        $ArrFinal[$i]["allprice"][$k]["minstay"] = $vv->getMinstay();
                                        $ArrFinal[$i]["allprice"][$k]["etat"] = $vv->getEtat();
                                    }
                                }

                                $ArrFinal[$i]["allprice"][$k]["ad"] = $valad;
                                $ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                                $ArrFinal[$i]["allprice"][$k]["idform"] = $idform;
                                //$ArrFinal[$i]["allprice"][$k]["enf"] = $enf[$kad];
                                //$ArrFinal[$i]["allprice"][$k]["children"] = $children;
                                $ArrFinal[$i]["allprice"][$k]["enf"] = [$enf[$kad],$children];

                                //$calc=new Calculate();
                                $dtest = null;
                                $dtest = $d1;
                                // initialiser l'age des enfant a 6 s'il existe
                                // il faut les changer s'il y a un autre age d'enfant
                                $tabenf = array();
                                $tab = array();

                                // for ($xd = 0; $xd < $enf[$kad]; $xd++) {
                                $tab[] = $ArrFinal[$i]["allprice"][$k]["enf"];
                                // }
                                $iter+= $tab[0][0];

                                $a1= array();


                                for ($xv = $int; $xv < $iter; $xv++) {
                                    array_push($a1, $tab[0][1][$xv]);
                                }
                                $ArrFinal[$i]["allprice"][$k]["age"] = $a1;
                                $tabenf = $a1;


                                $valp = $ArrFinal[$i]['valp'];
                                $vals =   $ArrFinal[$i]['persp'] ;





                                //$dres = $res->modify('+'.$valres. 'day');


                                // $valres = $value->getRetro();
                                //$dres = $res->modify('+'.$valres. 'day');
                                //var_dump($valres);
                                //var_dump($d1);

                                //die();
                                //Disponiblité

                                $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                                $disroom = $this->getDoctrine()->getRepository('BtobHotelBundle:Priceroom')->findByPriceRoom($value->getId(),$room->getId());

                                $valres = $value->getRetro();




                                if($nbjour>=$ArrFinal[$i]["allprice"][$k]["minstay"] && $disroom[0]->getQte() > 0 )
                                {
                                    $ArrFinal[$i]["allprice"][$k]["dispo"]="1";
                                }
                                else{
                                    $ArrFinal[$i]["allprice"][$k]["dispo"]="0";
                                }

                                //fin
								
								foreach ($hotelroom as $khr => $vhr) {

									if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

										$ArrFinal[$i]["allprice"][$k]["nbad"] = $vhr->getAd();

										$ArrFinal[$i]["allprice"][$k]["nbenf"] = $vhr->getEnf();

										$ArrFinal[$i]["allprice"][$k]["typepersonne"] = $vhr->getPersonne();

										$ArrFinal[$i]["allprice"][$k]["arr"] = $arr[$kad];

									}

								}
                                $prix = $this->getDoctrine()
                                    ->getRepository('BtobHotelBundle:Hotelprice')
                                    ->calcultatea($valp,$vals,$user, $dtest->format('Y-m-d'), $value->getHotel()->getId(), $nbjour, $arrang, $valad, $tabenf, $room, $typepersonne, $margeval, $margepers);

                                $prix = Tools::calculemarge($prix, $margepers, $margeval); // calcule de marge

                                $ArrFinal[$i]["allprice"][$k]["price"] = $prix;

                                if ($prix == 0) {
                                    $surdemande = true;
                                }// get supplement
								$suppl = $value->getHotel()->getHotelroomsup();

								$supplement = $value->getPricesupplement();


								foreach ($suppl as $ksup => $vsup) {

									if ($vsup->getRoom()->getId() == $room->getId()) {

										$xtab = array();// ce tableau pour le croisement de price sup et hotel sup

										foreach ($supplement as $valsupplement) {

											if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

												if ($valsupplement->getPers()) {
												 
													if($persp)
													{
																								 

													
													if($valsupplement->getPersm())
													  {
													 $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ 
															 (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);      
													  }else{
													   $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();    
													  }
													}
													else{
													if($valsupplement->getPersm())
													  {
													$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
															  (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
													  }else{
													$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
													  }
													}

													$xtab[0] = $xtab[0]; // calcule de marge

												}
												else{
												   
												   
												   if($valsupplement->getPersm())
													  {
														  $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100); 
													  }else{
														  $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge(); 
													  }
												}

												$xtab[1] = $valsupplement->getId();

											}

										}

										$ArrFinal[$i]["allprice"][$k]["supplement"][] = array($vsup, $xtab);

									} else {

										$ArrFinal[$i]["allprice"][$k]["supplement"][] = array();

									}

								}
							

								// get events
								$events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);
								$tabev = array();
								foreach ($events as $kev => $valev) {
									$tabev[] = $valev;
								}
								$ArrFinal[$i]["allprice"][$k]['events'] = $tabev;
                                ++$k;
                            }

                        }
                        // Stop Sales
                        $stopsales = $this->getDoctrine()->getRepository('BtobHotelBundle:StopSales')->findbydate($value->getHotel(), $d1, $user);
                        $ArrFinal[$i]["stopsales"] = count($stopsales);
                        if ($surdemande) {
                            $ArrFinal[$i]["stopsales"] = 1;
                        }

							// affichage des autres chambres
							foreach ($value->getHotel()->getHotelroom() as $vrestroom) {

								if (!in_array($vrestroom->getRoom()->getId(), $tabactualroom)) {

									$roomother = $vrestroom->getRoom();

									$nbpersonne = $roomother->getCapacity();

									if ($roomother != null && $arrang != null) {

										$ArrFinal[$i]["allotherprice"][$k]["nbpersonne"] = $nbpersonne;
										$ArrFinal[$i]["allotherprice"][$k]["roomname"] = $roomother->getName();
										$ArrFinal[$i]["allotherprice"][$k]["arrangement"] = $arrang->getName();
										$ArrFinal[$i]["allotherprice"][$k]["ad"] = $valad;
										$ArrFinal[$i]["allotherprice"][$k]["enf"] = $enf[$kad];
										$ArrFinal[$i]["allotherprice"][$k]["arr"] = $arr[$kad];



										foreach ($hotelroom as $khr => $vhr) {

											if ($vhr->getRoom()->getCapacity() == $nbpersonne) {

												$ArrFinal[$i]["allotherprice"][$k]["id"] = $vhr->getId();

												$ArrFinal[$i]["allotherprice"][$k]["nbad"] = $vhr->getAd();

												$ArrFinal[$i]["allotherprice"][$k]["nbenf"] = $vhr->getEnf();

											}

										}

										// get supplement

										$suppl = $value->getHotel()->getHotelroomsup();

										$supplement = $value->getPricesupplement();

										foreach ($suppl as $ksup => $vsup) {

											if ($vsup->getRoom()->getId() == $roomother->getId()) {

												$xtab = array();// ce tableau pour le croisement de price sup et hotel sup

												foreach ($supplement as $valsupplement) {

													if ($vsup->getSupplement()->getId() == $valsupplement->getHotelsupplement()->getSupplement()->getId()) {

													   if ($valsupplement->getPers()) {
												 
													if($persp)
													{
																								 

													
													if($valsupplement->getPersm())
													  {
													 $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ 
															 (((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);      
													  }else{
													   $xtab[0]= ((($value->getPrice()-($value->getPrice()*$valp/100)) * $valsupplement->getPrice()) / 100)+ $valsupplement->getMarge();    
													  }
													}
													else{
													if($valsupplement->getPersm())
													  {
													$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+
															  (((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)*$valsupplement->getMarge()/100);
													  }else{
													$xtab[0] = ((($value->getPrice()-$valp) * $valsupplement->getPrice()) / 100)+$valsupplement->getMarge();
													  }
													}

													$xtab[0] = $xtab[0]; // calcule de marge

												}
												else{
												   
												   
												   if($valsupplement->getPersm())
													  {
														  $xtab[0] = $valsupplement->getPrice()+($valsupplement->getPrice()*$valsupplement->getMarge()/100); 
													  }else{
														  $xtab[0] = $valsupplement->getPrice()+$valsupplement->getMarge(); 
													  }
												}

														$xtab[1] = $valsupplement->getId();

													}

												}

												$ArrFinal[$i]["allotherprice"][$k]["supplement"][] = array($vsup, $xtab);

											} else {

												$ArrFinal[$i]["allotherprice"][$k]["supplement"][] = array();

											}

										}

										// get events

										$events = $this->getDoctrine()->getRepository('BtobHotelBundle:Events')->getEventHotel($value->getHotel()->getId(), $d1, $d2);

										$tabev = array();

										foreach ($events as $kev => $valev) {

											$tabev[] = $valev;

										}

										$ArrFinal[$i]["allotherprice"][$k]['events'] = $tabev;

										++$k;



									}

								}

							}
							//Tools::dump($tabactualroom,true);
                        //Tools::dump($request, true);
                        ++$i;
                    }

                }
            }

            if ($try == "nom") {
                $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
            } else if ($try == "prix") {
                $ArrFinal = Tools::array_sort($ArrFinal, "price", SORT_ASC);
            } else if ($try == "etoil") {
                $ArrFinal = Tools::array_sort($ArrFinal, "star", SORT_ASC);
            }
            else if ($try == "") {
                $ArrFinal = Tools::array_sort($ArrFinal, "hotelname", SORT_ASC);
            }
            /*
             * Appliquer les filtre de recheche
             */

            // pas de chambre disponible
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if (isset($value["allprice"])) {
                    $ArrFinal[] = $value;
                }
            }
            // filtre de prix
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            foreach ($ArrFinal1 as $key => $value) {
                if ($value["price"] >= $price_min && $value["price"] <= $price_max) {
                    $ArrFinal[] = $value;
                }
            }
            // filter $star
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            if($request->request->get("star")==0)
            {
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["star"] >= $star) {
                        $ArrFinal[] = $value;
                    }
                }
            }else{
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["star"] == $star) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter pays
            if ($paysid > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["pays"] == $paysid) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter ville
            if ($ville > 0) {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if ($value["ville"] == $ville) {
                        $ArrFinal[] = $value;
                    }
                }
            }
            // filter name hotel
            if ($nom != "") {
                $ArrFinal1 = $ArrFinal;
                $ArrFinal = array();
                foreach ($ArrFinal1 as $key => $value) {
                    if(strpos(htmlspecialchars($value["hotelname"]), $nom) !== false)
                    {
                        $ArrFinal[] = $value;
                    }
                }
            }
            //Filter trie

            /*
             * pagination
             */
            $totpage = ceil(count($ArrFinal) / $nbpage);
            $ArrFinal1 = $ArrFinal;
            $ArrFinal = array();
            $i = 0;
            $start = $nbpage * ($page - 1);
            $end = $nbpage * $page;
            if ($start > count($ArrFinal1)) {
                $start = 0;
                $page = 1;
                $end = $nbpage;
            }
            foreach ($ArrFinal1 as $key => $value) {
                if ($i >= $start && $i < $end) {
                    $ArrFinal[] = $value;
                }
                ++$i;
            }
        }

        $d1 = new \DateTime(Tools::explodedate($dated, '/'));
        $d2 = new \DateTime(Tools::explodedate($datef, '/'));

        $userid = $user->getId();

        $hotelmarges= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);



        return $this->render('BtobHotelBundle:List:search.html.twig', array(
                'pays' => $pays,
                'paysid' => $paysid,
                'ville' => $ville,
                'trie' => $trie,
                'try' => $try,
                'user' => $user,
                'price_min' => $price_min,
                'price_max' => $price_max,
                'star' => $star,
                'dated' => $dated,
                'datef' => $datef,
                'nom' => $nom,
                'page' => $page,
                'price' => $ArrFinal,
                'filterhotel' => $filterhotel,
                'totpage' => $totpage,
                'arr' => $arrangement,
                'nbjour' => $nbjour,
                'frmad' => $ad,
                'frmenf' => $enf,
                'frmchild' => $children,
                'frmarr' => $arr,
                'd1' => $d1,
                'd2' => $d2,
                'margeval' => $margeval,
                'margepers' => $margepers,
                'hotelmarges' => $hotelmarges,
            )
        );
    }

    public function detailAction($hotelid, $dated, $datef)

    {
        $user = $this->get('security.context')->getToken()->getUser();

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();


        $arrangement = $this->getDoctrine()->getRepository('BtobHotelBundle:Arrangement')->findActiv();
        // list des pays

        $dt = new \DateTime();
        $dt->modify('+1 day');
        $datedd = $dt->format("d/m/Y");
        $dt->modify('+1 day');
        $dateff = $dt->format("d/m/Y");




        $tab = explode("/", $datedd);

        $d1 = new \DateTime($tab[2] . "-" . $tab[1] . "-" . $tab[0]);

        $tab2 = explode("/", $dateff);

        $d2 = new \DateTime($tab2[2] . "-" . $tab2[1] . "-" . $tab2[0]);

        //calculer la deference entre dated et dates
        $nbjour = $d2->diff($d1);


        $nbjour = $nbjour->days;

        $user = $this->get('security.context')->getToken()->getUser();

        $price = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydate($d1, $user);

        $prices = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotelprice')->findbydateHotel($d1, $hotelid, $user);
        $pricarres = $this->getDoctrine()->getRepository('BtobHotelBundle:Pricearr')->findOneBy(
            array('hotelprice' => $prices[0]->getId(),'etat' => 1)


        );


        $id = $pricarres->getHotelarrangement()->getId();
        $arrg= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelarrangement')
            ->find($id);

        $ark=$arrg->getArrangement()->getId();
        $ad = array(0 => 2);
        $enf = array(0 => 0);
        $arr = array(0 => $ark);


        $ad = $session->get("ad");
        if($ad==NULL)
        {
            $ad = array(0 => 2);
        }

        $enf = $session->get("enf");
        if($enf==NULL)
        {
            $enf = array(0 => 0);
        }
		
		$arr=$session->get("arr");
        
        if($arr==NULL)
        {
            $arr = array(0 => $ark);
        }
        $d1 = new \DateTime(Tools::explodedate($datedd, '/'));
        $d2 = new \DateTime(Tools::explodedate($dateff, '/'));

        $userid = $user->getId();

        $hotelmarges= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotelmarge')
            ->findByUser($userid);


        $User = $this->get('security.context')->getToken()->getUser();

        $hotel = $this->getDoctrine()->getRepository('BtobHotelBundle:Hotel')->find($hotelid);

        $dataimg = $hotel->getHotelimg();

        $img = "/back/img/dummy_150x150.gif";

        $j = 0;

        foreach ($dataimg as $keyimg => $valimg) {

            if ($j == 0)

                $img = $valimg->getFile();

            if ($valimg->getPriori())

                $img = $valimg->getFile();

            ++$j;

        }

        $session->set('ad', $ad);
        $session->set('enf', $enf);
        $session->set('arr', $arr);

        return $this->render('BtobHotelBundle:List:detail.html.twig', array(

                 "hotelid" => $hotelid,

                "dated" => $dated,

                "datef" => $datef,

                "hotel" => $hotel,

                "img" => $img

            )

        );
    }




    /**

     * @param Request $request

     */

    public function sendMail()

    {


        $message = \Swift_Message::newInstance()

            ->setSubject('welcome')

            ->setFrom('zayenzeki@yahoo.fr')

            ->setTo('zayenzeki@gmail.com')

            ->setBody('hotel hotel');

        $this->get('mailer')->send($message);

    }



}