<?php


namespace Btob\HotelBundle\Controller;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Fact;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use Btob\HotelBundle\Entity\Client;
use Btob\HotelBundle\Entity\Mfacture;

use Symfony\Component\HttpFoundation\RedirectResponse;



use Btob\HotelBundle\Form\FactureType;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Entity\Tva;
use Btob\HotelBundle\Form\TvaType;
use Btob\HotelBundle\Entity\Libelle;
use Btob\HotelBundle\Form\LibelleType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;


class FactureController extends Controller

{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Facture = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->orderBy('f.id','DESC')
             ->getQuery()
             ->getResult();         
   /*     $nb = $em->createQueryBuilder()
             ->select('f.supp')
             ->from('BtobHotelBundle:Facture','f')
             ->groupBy('f.idm')
             ->having('count(f.idm) = 1')
             ->orderBy('f.id','DESC')
             ->getQuery()
             ->getResult();
*/
$clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
        return $this->render('BtobHotelBundle:Facture:index.html.twig', array('entities' => $Facture,'clients'=>$clients));

    }


    public function extraAction()
    {
        $libelles=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Libelle')
            ->findBy(array('act' => 1));
        $tva=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')
            ->findBy(array('act' => 1));
        $factp=$this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
        $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();

        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $date = new \DateTime();
        $client = new Clients();
        $client->setAct(1);
        $form = $this->createForm(new ClientsType(), $client);
        if ($request->getMethod() == 'POST') {
        
            $montant = $request->request->get("montant");
            $montant_p = $request->request->get("montant_p");


            $montant_f=0;
            if($montant_p != null){
                foreach ($montant_p as $key ) {
                    $montant_f+=$key;
                }
            }
            
            $client=$request->request->get("client");
            
            $clientid=$request->request->get("client_id");
        $client_id = $em->createQueryBuilder()
             ->select('c')
             ->from('BtobHotelBundle:Clients','c')
             ->where('c.id = :cid')
             ->setParameter('cid', $clientid)
             ->getQuery()
             ->getResult();
     //     exit(var_dump($clientid));
            $type = $request->request->get("type");

            $nom=$request->request->get("client");
            $date=$request->request->get("date");
            $date= new \DateTime($date);


            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
            $factures = array();
            $dr = new \DateTime();
            if (count($facts) > 0) {
                foreach ($facts as $kef) {
                    if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                        array_push($factures, $kef);
                    }
                }
            }

            if (count($factures) > 0) {
                $num = $factures[0]->getNum() + 1;
            } else {
                $num = 1;
            }
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setCode("BO");
            $ffact->setEtat(1);
            $ffact->setClient($client);
            $ffact->setClientId($client_id[0]);
            $ffact->setDcr($dr);
            $ffact->setIdm($num);
            $ffact->setType($type[0]);
            $ffact->setMontant($montant);
            $ffact->setMontantP($montant_f);
            $ffact->setDate($date);
            $em->persist($ffact);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Ajout: Facture sous numero " . $ffact->getNum()." du client " . $ffact->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
            

$tva=$request->request->get("tva");
$quantite=$request->request->get("quantite");
$prix_unitaire=$request->request->get("prix_unitaire");
$remise=$request->request->get("remise");
            // $tt = 0;
            foreach ($type as $key => $value) {
                $tfacture = new Tfacture();
                $tfacture->setFacture($ffact);
                $tfacture->setType($type[$key]);
                $tfacture->setTva($tva[$key]);
                $tfacture->setQuantite($quantite[$key]);
                $tfacture->setPrixUnitaire($prix_unitaire[$key]);
                $tfacture->setRemise($remise[$key]);

                $em->persist($tfacture);
                $em->flush();
              //  $tt = $tt + $montant[$key];
            }
            $mnt=$request->request->get("montant_p");
            if($mnt[0] != null){
            $mode=$request->request->get("mode");
           
            $date_cheque=$request->request->get("date_cheque");
            $numero_cheque=$request->request->get("numero_cheque");
            $banque=$request->request->get("banque");
            $date=$request->request->get("date_p");
            $etat=$request->request->get("etat");
            $rib=$request->request->get("rib");
            $motif=$request->request->get("motif");
          //  var_dump($motif).die;
              foreach ($mode as $key => $value) {
                $mfacture = new Mfacture();
                $mfacture->setFacture($ffact);
                $mfacture->setMode($mode[$key]);
                    $mfacture->setMontant($mnt[$key]);
                $mfacture->setDate(new \DateTime($date[$key]));
                $mfacture->setDate_cheque(new \DateTime($date_cheque[$key]));
                $mfacture->setNumero_cheque($numero_cheque[$key]);
                $mfacture->setBanque($banque[$key]);

                $mfacture->setRib($rib[$key]);
                $mfacture->setMotif($motif[$key]);
                $mfacture->setEtat($etat[$key]);
                $em->persist($mfacture);
                $em->flush();
              }
            }
      
            return $this->redirect($this->generateUrl('btob_facture_homepage'));
        }
       $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
        return $this->render('BtobHotelBundle:Facture:extra.html.twig',array('entities' => $clients,'form' =>$form->createView(),'countries'=>$countries,'factp'=>$factp,'libelles'=>$libelles,'tva'=>$tva));
    }


public function annulerAction($id){

 $em = $this->getDoctrine()->getManager();

        $facturea = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->find($id);
        
        //$id=$devis->getId();
        
        //var_dump($id);
        //die();
 $tfacturea = $this->getDoctrine()->getRepository('BtobHotelBundle:Tfacture')->findBy(array('facture' => $id));
 $mfacturea = $this->getDoctrine()->getRepository('BtobHotelBundle:Mfacture')->findBy(array('facture' => $id));


      //      $num=$facturea->getNum();
            $idm = $facturea->getIdm();
            $montant = $facturea->getMontant();
            $montant_p = $facturea->getMontantP();
            $client = $facturea->getClient();
            $clientid = $facturea->getClientId();
            $type= $facturea->getType();
            $code=$facturea->getCode();
            $etat=$facturea->getEtat();
            $dr = new \DateTime();
            $date=$facturea->getDate();
        

            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
            $factures = array();
            if (count($facts) > 0) {
                foreach ($facts as $kef) {
                    if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                        array_push($factures, $kef);
                    }
                }
            }

            if (count($factures) > 0) {
                $num = $factures[0]->getNum() + 1;
            } else {
                $num = 1;
            }
            
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setIdm($idm);
            if($montant != null)  $ffact->setMontant((-1) * $montant);
            if($montant_p != null)  $ffact->setMontantP((-1) * $montant_p);
            $ffact->setClient($client);
            $ffact->setClientId($clientid);
            $ffact->setType($type);
            $ffact->setCode($code);
            $ffact->setEtat($etat);
            $ffact->setDcr($dr);
            $ffact->setDate($date);
            $ffact->setSupp(1);
            $em->persist($ffact);
            $em->flush();


            foreach ($tfacturea as $key ) {
                $tfacture = new Tfacture();
                $tfacture->setFacture($ffact);
                $tfacture->setType($key->getType());
                $tfacture->setQuantite($key->getQuantite());
                $tfacture->setPrixUnitaire($key->getPrixUnitaire());
                $tfacture->setRemise($key->getRemise());
                $tfacture->setTva($key->getTva());
                $em->persist($tfacture);
                $em->flush();
            }
            foreach ($mfacturea as $key ) {
                $mfacture = new Mfacture();
                $mfacture->setFacture($ffact);
                $mfacture->setMode($key->getMode());
                if($key->getMontant() != null)  $mfacture->setMontant((-1) * $key->getMontant());
                $mfacture->setDate_cheque($key->getDate_cheque());
                $mfacture->setNumero_cheque($key->getNumero_cheque());
                $mfacture->setBanque($key->getBanque());
                $mfacture->setDate($key->getDate());
                $em->persist($tfacture);
                $em->flush();
            }
            //$em->persist($ffact);
        //    $em->flush();
            return $this->redirect($this->generateUrl('btob_facture_homepage'));
    

}


public function editfactureAction($id){
 $em = $this->getDoctrine()->getManager();
 $facture = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Facture')
            ->find($id);
  $tfacture = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tfacture')
            ->findBy(array('facture' => $facture));
  $mfacture = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Mfacture')
            ->findBy(array('facture' => $facture));

     $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll(); 
    $client = new Clients();
        $client->setAct(1);
    $factp=$this->getDoctrine()->getRepository('BtobHotelBundle:Fact')->find(1);
        $form = $this->createForm(new ClientsType(), $client);
    $countries = $this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->findAll();
$em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
         $code=$facture->getCode();


    if ($request->getMethod() == 'POST') 
    {
        if($code=="BO")
        {
            $montant = $request->request->get("montant");
            $montant_p = $request->request->get("montant_p");
            $oldmontant=$facture->getMontantP();
            $montant_f=0;
            foreach ($montant_p as $key ) 
            {
              $montant_f+=$key;
            }
            $newmontant=$montant_f+$oldmontant;

            $type = $request->request->get("type");

            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();

            $facture->setMontant($montant);
            $facture->setMontantP($newmontant);
            
            $em->persist($facture);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Modification: Facture " . $facture->getId()." sous numero " . $facture->getNum()." du client " . $facture->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            $mode=$request->request->get("mode");
            $montant=$request->request->get("montant_p");
           
            $date_cheque=$request->request->get("date_cheque");
            $numero_cheque=$request->request->get("numero_cheque");
            $banque=$request->request->get("banque");
            $date=$request->request->get("date_p");

            foreach ($mode as $key => $value) {
                $mfacture = new Mfacture();
                $mfacture->setFacture($facture);
                $mfacture->setMode($mode[$key]);
                $mfacture->setMontant($montant[$key]);
                $mfacture->setDate(new \DateTime($date[$key]));
                $mfacture->setDate_cheque(new \DateTime($date_cheque[$key]));
                $mfacture->setNumero_cheque($numero_cheque[$key]);
                $mfacture->setBanque($banque[$key]);
                $em->persist($mfacture);
                $em->flush();
            }
          
      
            return $this->redirect($this->generateUrl('btob_facture_homepage'));
        }
        else
        {
            $montant = $request->request->get("montant");
            $montant_p = $request->request->get("montant_p");
            $client_id = $request->request->get("client_id");
            $oldmontant=$facture->getMontantP();
            $montant_f=0;
            foreach ($montant_p as $key ) 
            {
              $montant_f+=$key;
            }

            $newmontant=$montant_f+$oldmontant;
            $type = $request->request->get("type");
            $tfact=$this->getDoctrine()->getRepository('BtobHotelBundle:Tfacture')->findBy(array('facture' => $facture));
            $verif_remise=$tfact[0]->getRemise();
            if (is_null($verif_remise)) {
            $tva=$request->request->get("tva");
            $remise=$request->request->get("remise");
            $prix_unitaire=$request->request->get("prix_unitaire");
            $tfact[0]->setTva($tva);
            $tfact[0]->setRemise($remise);
            $tfact[0]->setQuantite(1);
            $tfact[0]->setPrixUnitaire($prix_unitaire);
            $em->persist($tfact[0]);
            $em->flush();
            }

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Modification: Facture " . $facture->getId()." sous numero " . $facture->getNum()." du client " . $facture->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                        $em->flush();
            $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
            $facture->setMontant($montant);
            $facture->setMontantP($newmontant);
            //$facture->setClient_id($client_id);
            $em->persist($facture);
            $em->flush();
            $mode=$request->request->get("mode");
            $montant=$request->request->get("montant_p");
            $date_cheque=$request->request->get("date_cheque");
            $numero_cheque=$request->request->get("numero_cheque");
            $banque=$request->request->get("banque");
            $date=$request->request->get("date_p");

            foreach ($mode as $key => $value) {
                $mfacture = new Mfacture();
                $mfacture->setFacture($facture);
                $mfacture->setMode($mode[$key]);
                $mfacture->setMontant($montant[$key]);
                $mfacture->setDate(new \DateTime($date[$key]));
                $mfacture->setDate_cheque(new \DateTime($date_cheque[$key]));
                $mfacture->setNumero_cheque($numero_cheque[$key]);
                $mfacture->setBanque($banque[$key]);
                $em->persist($mfacture);
                $em->flush();
            }

          
      
            return $this->redirect($this->generateUrl('btob_facture_homepage'));

        }
       }
if ($code=="BO") 
        {

            return $this->render('BtobHotelBundle:Facture:edit.html.twig',array('mfactures'=>$mfacture,'tfactures'=>$tfacture,'facture'=>$facture,'entities' => $clients,'form' =>$form->createView(),'countries'=>$countries,'factp'=>$factp));
}else{
            $tva=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')
            ->findBy(array('act' => 1));
    return $this->render('BtobHotelBundle:Facture:edit2.html.twig',array('mfactures'=>$mfacture,'tfactures'=>$tfacture,'facture'=>$facture,'entities' => $clients,'form' =>$form->createView(),'countries'=>$countries,'factp'=>$factp,'tva'=>$tva));
}


}
    public function factureAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Facture')
            ->find($id);
        $fact = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Fact')
            ->find(1);
        $tva = $em->createQueryBuilder()
             ->select('t.name')
             ->from('BtobHotelBundle:Tva','t')
             ->orderBy('t.name', 'ASC')
             ->getQuery()
             ->getResult();
        $counttva = $em->createQueryBuilder()
             ->select('count(t.name)')
             ->from('BtobHotelBundle:Tva','t')
             ->where('t.act = :act')
             ->setParameter('act', 1)
             ->getQuery()
             ->getSingleScalarResult();
             $s=array();
        foreach($tva as $sum['name']){
        $somme = $em->createQueryBuilder()
            ->select('SUM(t.quantite * t.prixUnitaire)')  //(t.quantite*t.prixUnitaire)-((t.quantite*t.prixUnitaire)/100)
             ->from('BtobHotelBundle:Tfacture','t')
             ->where('t.facture = :fact')
             ->setParameter('fact', $id)
             ->andWhere('t.tva = :tva')
             ->setParameter('tva', $sum['name'])
             ->groupBy('t.tva')
             ->orderBy('t.tva', 'ASC')
             ->getQuery()
             ->getResult();
             
             if($somme != null)
            $s[]=array($somme,);
            else 
            $s[]=array(0,);
        }
       //      exit(var_dump($s));
        $sum=array();
        for($i=0;$i<$counttva;$i++)
             if($s[$i][0] != 0)     $sum[]=$s[$i][0][0][1];
             else               $sum[]=0;
    //         exit(var_dump($sum));
        $tfacture = $this->getDoctrine()->getRepository('BtobHotelBundle:Tfacture')->findBy(array('facture' => $facture));
      //  exit(var_dump($facture->getMontant()*1000));
     //   $numtochar= $this->asLetters(2.500);
        $numtochar= $this->asLetters($facture->getMontant());

 //   exit(var_dump($tva));
    //    exit(var_dump($numtochar));
        $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('Facture N�� ' . $facture->getNum() . '/' . $facture->getDcr()->format("Y"));
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
       // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->AddPage();
        $pdf->SetMargins(10,0, 10,0);
        $pdf->setCellPaddings(0,14,0,0);

      //  $pagefooter = "";
        $html = $this->renderView('BtobHotelBundle:Facture:pdf.html.twig', array(
            'facture' => $facture,
            'tfacture' => $tfacture,
            'fact' => $fact,
            'tva' => $tva,
            'sum' => $sum,
            'montantenlettre' => $numtochar,
          //  'pagefooter' => $pagefooter,
        ));
        $pdf->writeHTML($html);
        $pdf->Image("images/footerrustica.PNG", 0, 246.56, 210, '', 'PNG', '', 'T', false, 500, '', false, false, 0, false, false, false);

        $nompdf = 'facture_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }
    public function balanceAction() {
    //  exit(var_dump("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
      $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findtotalfact();
   //   exit(var_dump($facts));
      $fact = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findAll();
 //     exit(var_dump($fact));
   $clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
       $session = new Session();
      $session->set('facture', $facts);
      $session->set('tfacture', $fact);
      $session->set('from', "");
      $session->set('to', "");
return $this->render('BtobHotelBundle:Facture/balance:liste.html.twig',array('entities' => $facts,'facts'=>$fact,'clients'=>$clients));

    }

	public function balance_detailsAction($id){
		$factures = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findBy(array('clientId' => $id));
		return $this->render('BtobHotelBundle:Facture/balance:detail.html.twig',array('entities'=>$factures,'id'=>$id));
    }
     public function balance_editAction($id){
        $em = $this->getDoctrine()->getManager();
$factures = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findBy(array('id' => $id));
   $request = $this->get('request');
   
   
$type=$request->request->get("type");
        if ($request->getMethod() == 'POST') {
          $factures = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findBy(array('id' => $id));
            $montant_p=$factures[0]->getMontantP();
            $montant = $request->request->get("montant");
            $montant_f=$montant + $montant_p;
            $factures[0]->setMontantP($montant_f);
            $em->persist($factures[0]);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Modification: Balance de facture " . $factures->getId()." sous numero " . $factures->getNum()." du client " . $factures->getClient());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();
           $referer = $this->getRequest()->headers->get('referer');   
           return new RedirectResponse($referer);
         
        
            }else {
     return $this->render('BtobHotelBundle:Facture/balance:edit.html.twig',array('entities'=>$factures));
}
    
   }


public function addclientpAction(){
    

 $request = $this->get('request');

        $client = new Clients();
        $client->setAct(1);
        $client->setType(0);
        $form = $this->createForm(new ClientsType(), $client);
       
        $form->bind($request);
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Ajout: Personne physique " . $client->getCiv().". " . $client->getPname());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
     $client_name=$client->getPname(); 
  //    $session = new Session();
  //    $session->set('name', $client_name);

   
  $referer = $this->getRequest()->headers->get('referer');   
  return new RedirectResponse($referer);
          
}


public function addclientmAction(){
   $request = $this->get('request');
     $nom=$request->request->get('nom');
     $mp=$request->request->get('mf');
     $date=$request->request->get('date_naissance');
     $mail=$request->request->get('email');
     $tel=$request->request->get('tel');
     $adresse=$request->request->get('adresse');
     $ville=$request->request->get('ville');
     $cp=$request->request->get('cp');
     $pays=$request->request->get('pays');
    $payss =$this->getDoctrine()->getRepository('BtobHotelBundle:Listpays')->find($pays);
 $dates =  new \DateTime($date);
  $client = new Clients();
        $client->setAct(1);
        $client->setType(1);
        $client->setPname($nom);
        $client->setMp($mp);
        $client->setDateNaissance($dates);
        $client->setEmail($mail);
        $client->setTel($tel);
        $client->setAdresse($adresse);
        $client->setVille($ville);
        $client->setCp($cp);
        $client->setPays($payss);

    $em = $this->getDoctrine()->getManager();
        $em->persist($client);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Facture");
                        $hist->setMessage("Ajout: Personne morale " . $client->getPname());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->flush();
      $client_name=$client->getPname(); 
      $session = new Session();
      $session->set('name', $client_name);

   
  $referer = $this->getRequest()->headers->get('referer');   
  return new RedirectResponse($referer);
    
}

public function filtrebalanceAction(){
   
    $em = $this->getDoctrine()->getManager();
    $request = $this->get('request');
    $from=$request->request->get("from");
    $to=$request->request->get("to");
    $client_id=$request->request->get("client_id");
    $todt=$to;

  
if (($client_id=="")&&($from=="")&&($to=="")) 
{
   $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findallf();
   $fact = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findAll();
}
elseif ($client_id=="") 
 {
      if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
         $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findTD($from,$to);
        $fact = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->getQuery()
             ->getResult();
    }
    else 
    {
           $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findTD($from,$to);
        $fact = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
          //   ->HAVING('f.clientId =:clientid')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
          //   ->setParameter('clientid', $clientId)
             ->getQuery()
             ->getResult();
    }
}

elseif ($client_id!="") 
{

    if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
    }
  
    $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findTDC($from,$to,$client_id);
  
   //          $fact = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findfacturedateclient($from,$to,$client_id);
    
        $fact = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->leftjoin('f.clientId','c')
             ->andWhere('c.id =:clientid')
             ->setParameter('clientid', $client_id)
             ->getQuery()
             ->getResult();
//exit(var_dump($fact));
}
$session = new Session();
      $session->set('facture', $facts);
      $session->set('tfacture', $fact);
      $session->set('from', $from);
      $session->set('to', $todt);
$clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
   return $this->render('BtobHotelBundle:Facture/balance:liste.html.twig', array('entities'=>$facts,'facts'=>$fact,"clients"=>$clients));

}
public function testAction(){
   $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findtotalfact();
  $fact = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findAll();
   $session = new Session();
      $session->set('facture', $facts);
      $session->set('tfacture', $fact);
return $this->render('BtobHotelBundle:Facture:test.html.twig',array('entities' => $facts,'facts'=>$fact));
}
public function filtrefacturebalanceAction($id){
    $em = $this->getDoctrine()->getManager();
    $request = $this->get('request');
    $from=$request->request->get("from");
    $to=$request->request->get("to");

   $entities = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findclientdate($id,$from,$to);

   return $this->render('BtobHotelBundle:Facture:detail.html.twig', array('entities'=>$entities,'id'=>$id));
}

    public function filtrefactureAction(){
           
    $em = $this->getDoctrine()->getManager();
    $request = $this->get('request');
    $from=$request->request->get("from");
    $to=$request->request->get("to");
    $client_id=$request->request->get("client_id");

  
if (($client_id=="")&&($from=="")&&($to=="")) 
{
   $entities = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->findAll();
}
elseif ($client_id=="") 
 {
      if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
        $entities = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->getQuery()
             ->getResult();
    }
    else 
    {
        $entities = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->getQuery()
             ->getResult();
    }
}

elseif ($client_id!="") 
{

    if ($to=="") 
    {
        $to = new \DateTime(); 
       $to= date_format($to, 'Y-m-d');
    }
  
        $entities = $em->createQueryBuilder()
             ->select('f')
             ->from('BtobHotelBundle:Facture','f')
             ->where('f.dcr BETWEEN :from AND :to')
             ->setParameter('from', $from)
             ->setParameter('to', $to)
             ->leftjoin('f.clientId','c')
             ->andWhere('c.id =:clientid')
             ->setParameter('clientid', $client_id)
             ->getQuery()
             ->getResult();
//exit(var_dump($fact));
}


$clients = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Clients')
            ->findAll();
  return $this->render('BtobHotelBundle:Facture:index.html.twig', array('entities' => $entities,'clients'=>$clients));

}
public function listetvaAction(){
    $tva= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')->findAll();
    return $this->render('BtobHotelBundle:Facture/TVA:liste.html.twig',array('entities'=>$tva));

}
public function ajouttvaAction(){
        $tva = new Tva();
        $form = $this->createForm(new TvaType(), $tva);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST')
         {

            $form->bind($request);
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($tva);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_facture_TVA_homepage'));
            }
        }
         return $this->render('BtobHotelBundle:Facture/TVA:form.html.twig',array('form' => $form->createView()));

}

 public function edittvaAction($id)
    {
        $request = $this->get('request');
        $tva = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TvaType(), $tva);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_facture_TVA_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Facture/TVA:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deletetvaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $tva=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Tva')
            ->find($id);
        $em->remove($tva);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_facture_TVA_homepage'));
    }

public function listelibellesAction(){
     $libelles= $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Libelle')->findAll();
    return $this->render('BtobHotelBundle:Facture/libelles:liste.html.twig',array('entities'=>$libelles));

}
public function ajoutlibelleAction(){
        $lib = new Libelle();
        $form = $this->createForm(new LibelleType(), $lib);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST')
         {

            $form->bind($request);
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($lib);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_facture_libelles_homepage'));
            }
        }
         return $this->render('BtobHotelBundle:Facture/libelles:form.html.twig',array('form' => $form->createView()));

}

public function editlibelleAction($id)
    {
        $request = $this->get('request');
        $libelle = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Libelle')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new LibelleType(), $libelle);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_facture_libelles_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Facture/libelles:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deletelibelleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $libelles=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Libelle')
            ->find($id);

        $em->remove($libelles);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_facture_libelles_homepage'));
    }

    public function exporterbalanceAction(){

         $em = $this->getDoctrine()->getManager();
        $session = new Session();
        $facture = $session->get('facture');
        $tfacture = $session->get('tfacture');
        $fact = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Fact')
            ->find(1);
    //    exit(var_dump($tfacture[0]));

        $pdf = $this->get('white_october.tcpdf')->create();
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('Exportation');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->AddPage();
        $pagefooter = "";
        $html = $this->renderView('BtobHotelBundle:Facture/balance:pdf.html.twig', array(
            'facture' => $facture,
            'tfacture' => $tfacture,
            'fact' => $fact,
          
            'pagefooter' => $pagefooter,
        ));
        $pdf->writeHTML($html);
        $nompdf = 'facture_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;

    }





   public function asLetters($number) {
$convert = explode('.', $number);
$num[17] = array('zero', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit',
'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize');

$num[100] = array(20 => 'vingt', 30 => 'trente', 40 => 'quarante', 50 => 'cinquante',
60 => 'soixante', 70 => 'soixante-dix', 80 => 'quatre-vingt', 90 => 'quatre-vingt-dix');

if (isset($convert[1]) && $convert[1] != '') {
    if($convert[0]==0){
        if($convert[1]<10){
            return self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[1]).' millimes';
        }
    }else{
        if($convert[1]<10){
            return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*100).' millimes';
        }else{
            if($convert[1]<100)
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]*10).' millimes';
            else
                return self::asLetters($convert[0]).' dinars et '.self::asLetters($convert[1]).' millimes';
        }
    }
}
if ($number < 0) return 'moins '.self::asLetters(-$number);
if ($number < 17) {
return $num[17][$number];
}
elseif ($number < 20) {
return 'dix-'.self::asLetters($number-10);
}
elseif ($number < 100) {
if ($number%10 == 0) {
return $num[100][$number];
}
elseif (substr($number, -1) == 1) {
if( ((int)($number/10)*10)<70 ){
return self::asLetters((int)($number/10)*10).'-et-un';
}
elseif ($number == 71) {
return 'soixante-et-onze';
}
elseif ($number == 81) {
return 'quatre-vingt-un';
}
elseif ($number == 91) {
return 'quatre-vingt-onze';
}
}
elseif ($number < 70) {
return self::asLetters($number-$number%10).'-'.self::asLetters($number%10);
}
elseif ($number < 80) {
return self::asLetters(60).'-'.self::asLetters($number%20);
}
else {
return self::asLetters(80).'-'.self::asLetters($number%20);
}
}
elseif ($number == 100) {
return 'cent';
}
elseif ($number < 200) {
return self::asLetters(100).' '.self::asLetters($number%100);
}
elseif ($number < 1000) {
return self::asLetters((int)($number/100)).' '.self::asLetters(100).($number%100 > 0 ? ' '.self::asLetters($number%100): '');
}
elseif ($number == 1000){
return 'mille';
}
elseif ($number < 2000) {
return self::asLetters(1000).' '.self::asLetters($number%1000).' ';
}
elseif ($number < 1000000) {
return self::asLetters((int)($number/1000)).' '.self::asLetters(1000).($number%1000 > 0 ? ' '.self::asLetters($number%1000): '');
}
elseif ($number == 1000000) {
return 'millions';
}
elseif ($number < 2000000) {
return 'un '.self::asLetters(1000000).' '.self::asLetters($number%1000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000)).' '.self::asLetters(1000000).($number%1000000 > 0 ? ' '.self::asLetters($number%1000000): '');
}
elseif ($number == 1000000000) {
return 'milliard';
}
elseif ($number < 2000000000) {
return 'un '.self::asLetters(1000000000).' '.self::asLetters($number%1000000000);
}
elseif ($number < 1000000000) {
return self::asLetters((int)($number/1000000000)).' '.self::asLetters(1000000000).($number%1000000000 > 0 ? ' '.self::asLetters($number%1000000000): '');
}
}

}

