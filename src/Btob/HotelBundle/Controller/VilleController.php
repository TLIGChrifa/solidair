<?php

namespace Btob\HotelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Entity\Ville;
use Symfony\Component\HttpFoundation\Request;
use Btob\HotelBundle\Form\VilleType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class VilleController extends Controller {

    public function indexAction() {
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->findAll();
        return $this->render('BtobHotelBundle:Ville:index.html.twig', array('entities' => $ville));
    }

    public function addAction() {
        $ville = new Ville();
        $form = $this->createForm(new VilleType(), $ville);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($ville);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Ajout: Ville ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_ville_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobHotelBundle:Ville:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VilleType(), $ville);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Modification: Ville n° " . $ville->getId()." - " . $ville->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_ville_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobHotelBundle:Ville:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Ville $ville) {
        $em = $this->getDoctrine()->getManager();

        if (!$ville) {
            throw new NotFoundHttpException("Ville non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Hôtel");
                        $hist->setMessage("Suppression: Ville n° " . $ville->getId()." - " . $ville->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($ville);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_ville_homepage'));
    }

    public function ajxPaysAction() {
        $request = $this->get('request');
        $id=$request->request->get("id");
        $ville = $this->getDoctrine()
                ->getRepository('BtobHotelBundle:Ville')
                ->findByPays($id);
        
        return $this->render('BtobHotelBundle:Ville:option.html.twig', array('entity'=>$ville)
        );
    }

}
