<?php





namespace Btob\SpaBundle\Form;





use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\OptionsResolver\OptionsResolverInterface;


use Symfony\Component\Validator\Constraints\Null;





class SpaType extends AbstractType


{


    /**


     * @param FormBuilderInterface $builder


     * @param array $options


     */


    public function buildForm(FormBuilderInterface $builder, array $options)


    {


        $builder


            ->add('libelle', 'text', array('label' => "Libelle", 'required' => true))


            ->add('type', 'choice', array(


                'choices' => array('Soins' => 'Soins', 'Cures' => 'Cures'),


                'required' => true,


            


                'label' => 'Type',


            ))


      


            ->add('ville', 'text', array('label' => "Ville", 'required' => true))





            ->add('prix', NULL, array('label' => ' ', 'required' => true))
            ->add('marge', NULL, array('label' => 'Marge(En pourcentage) ', 'required' => true))
            ->add('prixpromo', NULL, array('label' => ' Prix Promo', 'required' => true))
            ->add('prixavance', NULL, array('label' => 'Avance(En pourcentage) ', 'required' => true))
            ->add('apartir', NULL, array('label' =>' ', 'required' => false))


            ->add('active', NULL, array('label' =>' ', 'required' => false))




            ->add('ageenfmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Enfant  *',
            ))
            ->add('ageenfmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Enfant  *',
            ))
           



            ->add('description', null, array('label' => "Description longue", 'required' => false))

 


        ;


    }


    


    /**


     * @param OptionsResolverInterface $resolver


     */


    public function setDefaultOptions(OptionsResolverInterface $resolver)


    {


        $resolver->setDefaults(array(


            'data_class' => 'Btob\SpaBundle\Entity\Spa'


        ));


    }





    /**


     * @return string


     */


    public function getName()


    {


        return 'btob_spabundle_spa';


    }


}


