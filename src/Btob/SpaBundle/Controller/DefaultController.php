<?php

namespace Btob\SpaBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SpaBundle\Entity\Spa;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\SpaBundle\Entity\Resaspa;
use Symfony\Component\Validator\Constraints\DateTime;
use Btob\SpaBundle\Entity\Reservationspa;

use Btob\SpaBundle\Entity\Reservationspdetail;

use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        
        
        
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->listPrice();

       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $spaprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );        
        
       
        
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobSpaBundle:Default:index.html.twig', array('entities' => $entities,'spaprices' => $spaprices, 'user' => $user));

    }
    
    
    
    

    public function detailAction(Spa $spa)
    {
        
       $spaperiode = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());

        return $this->render('BtobSpaBundle:Default:detail.html.twig', array('entry' => $spa,'periods' => $spaperiode));
    }
    
    
    
    
   public function personalisationAction(Spa $spa)

    {

       $em = $this->getDoctrine()->getManager();
       
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $spaprice = $request->request->get("dated");
            $session->set('spaprice', $spaprice);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
           
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('btob_inscrip_reservation_spa', array('id'=>$spa->getId())));

            
        }
          
       
    return $this->render('BtobSpaBundle:Default:personalisation.html.twig', array('entry' => $spa,'spaprices' => $spaprices));

    
    } 
    
    
    public function inscriptionAction(Spa $spa)

    {

       $em = $this->getDoctrine()->getManager();
       
       $spaprices = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findBySpa($spa->getId());
       $spasupp = $this->getDoctrine()->getRepository("BtobSpaBundle:Supplementsp")->findSupplementBySpa($spa->getId());
       
       $spareduc = $this->getDoctrine()->getRepository("BtobSpaBundle:Reductionsp")->findReductionBySpa($spa->getId());

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $spaprice = $session->get("spaprice");
         $date = $session->get("spaprice");
         
        $spaprice = $session->get("spaprice");
       $time = strtotime($spaprice);

          $newformat = date('Y-m-d',$time);
       $dispo = $this->getDoctrine()->getRepository("BtobSpaBundle:Spaprice")->findbydateSpa($spa->getId(),$newformat);
         
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbenf = $session->get("nbenf");
        
        $Array = array();
         $aujourdhuienf = new \DateTime();
       $nbjourenf = intval($spa->getAgeenfmax())*365;
       $aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        if($dispo){
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                 if(isset($request->request->get("reducad")[$i][$j]))
                 {
                     $adad[$j]['reducad']= $request->request->get("reducad")[$i][$j];
                 }
                 else{
                   $adad[$j]['reducad']=null;  
                 }
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                 if(isset($request->request->get("reducenf")[$i][$k]))
                 {
                     $enf[$k]['reducenf']= $request->request->get("reducenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['reducenf']=null;  
                 }
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
         
                
            }
            
            $session->set('array', $Array);
             $session->set('spaprice', $dispo->getId());
            $session->set('date', $date);
            
          

          return $this->redirect($this->generateUrl('btob_resa_reservation_spa', array('id'=>$spa->getId())));

            
        }
       
       
       
       
    return $this->render('BtobSpaBundle:Default:inscription.html.twig', array('datedenf' => $datedenf,'entry' => $spa,'spasupp' => $spasupp,'spareduc' => $spareduc,'spaprices' => $spaprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbenf' => $nbenf));
        }else{
        $request->getSession()->getFlashBag()->add('verifback', 'Aucun Spa trouvé à cet date .');
    return $this->render('BtobSpaBundle:Default:personalisation.html.twig', array('entry' => $spa));    
            
        }
    
    }
    
      
    
 
    public function reservationAction(Spa $spa)

    {

        $em = $this->getDoctrine()->getManager();
      
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $spaperiode = $session->get("spaprice");
        
        $date = $session->get("date");
       $pricespa =   $this->getDoctrine()->getRepository('BtobSpaBundle:Spaprice')->find($spaperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           
               
           $totalsuppad=0;
           $totalsuppenf=0;
           
           
           $totalreducad=0;
           $totalreducenf=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  $reducaddp =0;
               
                  if(isset($val1['reducad']))
                  {
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                  
                  $totalreducad+= $reducaddp;
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              //reduction
              
              
              $reducenfp=0;
                  if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
              $totalreducenf+= $reducenfp;
              
              
              
              
           }
           
           }
           
           
           
           
 
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobSpaBundle:Spaprice')
                        ->calcul($pricespa,$nbad,$nbenf,$totalsuppad,$totalsuppenf,$totalreducad,$totalreducenf);
         $ArrBase[$key]['price'] =$prices+($prices*$spa->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $spaprice = $session->get("spaprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            $User = $this->get('security.context')->getToken()->getUser();

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationspa();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setSpa($spa);
                $resa->setSpaprice($pricespa);
                
                $resa->setTotal($total);
                $resa->setJour($date);  
                $em->persist($resa);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           
               
           $totalsuppad=0;
           $totalsuppenf=0;
           
           $totalreducad=0;
           $totalreducenf=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               $reducaddp =0; 
           
           $resadetail=new Reservationspdetail();
           $resadetail->setReservationspa($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  if(isset($val1['reducad']))
                  {
                      
                     

 
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                 
                  $resadetail->setReducad(json_encode($ArrBase[$key]['adult'][$k1]['reducad']));
                  $totalreducad+= $reducaddp;
                  
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               $reducenfp=0;
               
           $resadetails=new Reservationspdetail();
           $resadetails->setReservationspa($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Supplementsp')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
              //reduction
              
              if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobSpaBundle:Reductionsp')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
                  
              $resadetails->setReducenf(json_encode($ArrBase[$key]['enf'][$k1e]['reducenf']));
  
              $totalreducenf+= $reducenfp;
              
              
              
           
          
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           

        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobSpaBundle:Spaprice')
                        ->calcul($pricespa,$nbad,$nbenf,$totalsuppad,$totalsuppenf,$totalreducad,$totalreducenf);
         $ArrBase[$key]['price'] = $prices+($prices*$spa->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
          
          

           
       }    
                
                
                
                
                
        
                
               

            } else {
                echo $form->getErrors();
            }

                return $this->redirect($this->generateUrl('btob_spa_validation'));



        }

        return $this->render('BtobSpaBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$spa,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));


    }


    
    public function validationAction(){
        return $this->render('BtobSpaBundle:Default:validation.html.twig', array());
    }
    
    
   
    
    
}
