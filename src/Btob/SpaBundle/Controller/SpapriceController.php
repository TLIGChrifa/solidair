<?php

namespace Btob\SpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SpaBundle\Entity\Spaprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\SpaBundle\Form\SpapriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class SpapriceController extends Controller
{

    public function indexAction($spaid)
    {
        $spa = $this->getDoctrine()
            ->getRepository('BtobSpaBundle:Spa')
            ->find($spaid);
	    $name =  $spa->getLibelle();
        $price = array();
        foreach ($spa->getSpaprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobSpaBundle:Spaprice:index.html.twig', array(
            "entities" => $price,
            "spaid" => $spaid,
	    "name" => $name,
        ));
    }


    public function addAction($spaid)
    {
        $spa = $this->getDoctrine()->getRepository('BtobSpaBundle:Spa')->find($spaid);
      
        $allspaprice = $this->getDoctrine()->getRepository('BtobSpaBundle:Spaprice')->findBy(array('spa' => $spa));
      
        

        $spaprice = new Spaprice();
        $form = $this->createForm(new SpapriceType(), $spaprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $spaprice->setSpa($spa);
                $em->persist($spaprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_spaprice_homepage', array("spaid" => $spaid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSpaBundle:Spaprice:form.html.twig', array(
            'form' => $form->createView(),
            "spa" => $spa

        ));
    }



    public function editAction($id, $spaid)
    {
        $spa = $this->getDoctrine()->getRepository('BtobSpaBundle:Spa')->find($spaid);
        $request = $this->get('request');
        $spaprice = $this->getDoctrine()
            ->getRepository('BtobSpaBundle:Spaprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SpapriceType(), $spaprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allspaprice = $this->getDoctrine()->getRepository('BtobSpaBundle:Spaprice')->findBy(array('spa' => $spa));
      
    
       
        if ($form->isValid()) {

            $spaprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_spaprice_homepage', array("spaid" => $spaid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSpaBundle:Spaprice:edit.html.twig', array('form' => $form->createView(), 'price' => $spaprice, 'id' => $id, "spa" => $spa)
        );
    }

  
    public function deleteAction(Spaprice $spaprice, $spaid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$spaprice) {
            throw new NotFoundHttpException("Spaprice non trouvée");
        }
        $em->remove($spaprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_spaprice_homepage', array("spaid" => $spaid)));
    }



}












