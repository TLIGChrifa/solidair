<?php 
namespace Btob\SpaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\SpaBundle\Entity\Spa;
use Btob\SpaBundle\Entity\Imgspa;
use Btob\SpaBundle\Form\SpaType;

/**
 * Spa controller.
 *
 */
class SpaController extends Controller
{

    /**
     * Lists all Spa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobSpaBundle:Spa')->findAll();
        $entities=array_reverse($entities);
        return $this->render('BtobSpaBundle:Spa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Spa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Spa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgspa();
                        $img->setSpa($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('spa'));
        }

        return $this->render('BtobSpaBundle:Spa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Spa entity.
     *
     * @param Spa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Spa $entity)
    {
        $form = $this->createForm(new SpaType(), $entity, array(
            'action' => $this->generateUrl('spa_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new Spa entity.
     *
     */
    public function newAction()
    {
        $entity = new Spa();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobSpaBundle:Spa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Spa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSpaBundle:Spa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Spa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobSpaBundle:Spa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Spa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSpaBundle:Spa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Spa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobSpaBundle:Spa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Spa entity.
    *
    * @param Spa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Spa $entity)
    {
        $form = $this->createForm(new SpaType(), $entity, array(
            'action' => $this->generateUrl('spa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }
    /**
     * Edits an existing Spa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSpaBundle:Spa')->find($id);
        foreach ($entity->getImgspa() as $key => $value) {
            $em->remove($value);
            $em->flush();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Spa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgspa();
                        $img->setSpa($entity);
                        $img->setImage($value);

                        $em->persist($img);
                        $em->flush();

                    }
                }
            return $this->redirect($this->generateUrl('spa'));
        }

        return $this->render('BtobSpaBundle:Spa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Spa entity.
     *
     */
    public function deleteAction($id)
    {

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobSpaBundle:Spa')->find($id);


            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('spa'));
    }

    /**
     * Creates a form to delete a Spa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('spa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
