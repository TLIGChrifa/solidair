<?php

namespace Btob\SpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SpaBundle\Entity\Supplementsp;
use Symfony\Component\HttpFoundation\Request;
use Btob\SpaBundle\Form\SupplementspType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementspController extends Controller {

    public function indexAction($spaid) {
        $spa = $this->getDoctrine()
                ->getRepository('BtobSpaBundle:Spa')
                ->find($spaid);
        return $this->render('BtobSpaBundle:Supplementsp:index.html.twig', array('entities' => $spa->getSupplementsp(), "spaid" => $spaid));
    }

    public function addAction($spaid) {
        $spa = $this->getDoctrine()->getRepository('BtobSpaBundle:Spa')->find($spaid);
        $Supplementsp = new Supplementsp();
        $form = $this->createForm(new SupplementspType(), $Supplementsp);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementsp->setSpa($spa);
                $em->persist($Supplementsp);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementsp_homepage', array("spaid" => $spaid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSpaBundle:Supplementsp:form.html.twig', array('form' => $form->createView(), "spaid" => $spaid));
    }

    public function editAction($id, $spaid) {
        $request = $this->get('request');
        $Supplementsp = $this->getDoctrine()
                ->getRepository('BtobSpaBundle:Supplementsp')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementspType(), $Supplementsp);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementsp_homepage', array("spaid" => $spaid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSpaBundle:Supplementsp:form.html.twig', array('form' => $form->createView(), 'id' => $id, "spaid" => $spaid)
        );
    }

    public function deleteAction(Supplementsp $Supplementsp, $spaid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementsp) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplementsp);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementsp_homepage', array("spaid" => $spaid)));
    }

}
