<?php

namespace Btob\SpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SpaBundle\Entity\Reductionsp;
use Symfony\Component\HttpFoundation\Request;
use Btob\SpaBundle\Form\ReductionspType;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReductionspController extends Controller {

    public function indexAction($spaid) {
        $spa = $this->getDoctrine()
                ->getRepository('BtobSpaBundle:Spa')
                ->find($spaid);
        return $this->render('BtobSpaBundle:Reductionsp:index.html.twig', array('entities' => $spa->getReductionsp(), "spaid" => $spaid));
    }

    public function addAction($spaid) {
        $spa = $this->getDoctrine()->getRepository('BtobSpaBundle:Spa')->find($spaid);
        $Reductionsp = new Reductionsp();
        $form = $this->createForm(new ReductionspType(), $Reductionsp);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Reductionsp->setSpa($spa);
                $em->persist($Reductionsp);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_reductionsp_homepage', array("spaid" => $spaid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSpaBundle:Reductionsp:form.html.twig', array('form' => $form->createView(), "spaid" => $spaid));
    }

    public function editAction($id, $spaid) {
        $request = $this->get('request');
        $Reductionsp = $this->getDoctrine()
                ->getRepository('BtobSpaBundle:Reductionsp')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReductionspType(), $Reductionsp);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_reductionsp_homepage', array("spaid" => $spaid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSpaBundle:Reductionsp:form.html.twig', array('form' => $form->createView(), 'id' => $id, "spaid" => $spaid)
        );
    }

    public function deleteAction(Reductionsp $Reductionsp, $spaid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Reductionsp) {
            throw new NotFoundHttpException("Reduction non trouvée");
        }
        $em->remove($Reductionsp);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reductionsp_homepage', array("spaid" => $spaid)));
    }

}
