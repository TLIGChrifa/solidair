<?php







namespace Btob\SpaBundle\Entity;







use Doctrine\ORM\Mapping as ORM;







/**



 * Spa



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\SpaBundle\Entity\SpaRepository")



 */



class Spa



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;







    /**



     * @var string



     *



     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)



     */



    private $libelle;



    /**



     * @var string



     *



     * @ORM\Column(name="type", type="string", length=255 , nullable=true)



     */



    private $type;



    /**



     * @var date



     *



     * @ORM\Column(name="dcr", type="date")



     */



    private $dcr;







    /**



     * @var string



     *



     * @ORM\Column(name="ville", type="string", length=255 , nullable=true)



     */



    private $ville;








    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;





    /**



     * @var float



     *



     * @ORM\Column(name="prixpromo", type="float" , nullable=true)



     */



    private $prixpromo;
    
    
    
    /**



     * @var float



     *



     * @ORM\Column(name="prixavance", type="float" , nullable=true)



     */



    private $prixavance;    




    /**



     * @var boolean



     *



     * @ORM\Column(name="apartir", type="boolean" , nullable=true)



     */



    private $apartir;



    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $active;





    /**



     * @ORM\OneToMany(targetEntity="Imgspa", mappedBy="spa")



     */



    protected $imgspa;



    /**



     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;

     /**
     * @ORM\OneToMany(targetEntity="Supplementsp", mappedBy="spa", cascade={"remove"})
     */
    protected $supplementsp;
    

     /**
     * @ORM\OneToMany(targetEntity="Reductionsp", mappedBy="spa", cascade={"remove"})
     */
    protected $reductionsp;    
    
    
     /**
     * @ORM\OneToMany(targetEntity="Spaprice", mappedBy="spa", cascade={"remove"})
     */
    protected $spaprice;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */

    private $ageenfmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */

    private $ageenfmax;
    
    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */

    private $marge;  


    public  function  __construct(){



        $this->dcr = new \DateTime();



        $this->imgspa =new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplementsp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reductionsp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->spaprice = new \Doctrine\Common\Collections\ArrayCollection();



    }







    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }


    /**



     * Set libelle



     *



     * @param string $libelle



     * @return Spa



     */



    public function setLibelle($libelle)



    {



        $this->libelle = $libelle;







        return $this;



    }







    /**



     * Get libelle



     *



     * @return string 



     */



    public function getLibelle()



    {



        return $this->libelle;



    }











    /**



     * Set type



     *



     * @param string $type



     * @return Spa



     */



    public function setType($type)



    {



        $this->type = $type;







        return $this;



    }







    /**



     * Get type



     *



     * @return string 



     */



    public function getType()



    {



        return $this->type;



    }







    /**



     * Set ville



     *



     * @param string $ville



     * @return Spa



     */



    public function setVille($ville)



    {



        $this->ville = $ville;







        return $this;



    }







    /**



     * Get ville



     *



     * @return string 



     */



    public function getVille()



    {



        return $this->ville;



    }








    /**



     * Set prix



     *



     * @param float $prix



     * @return Spa



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }




    /**



     * Set description



     *



     * @param string $description



     * @return Spa



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set dcr



     *



     * @param \DateTime $dcr



     * @return Spa



     */



    public function setDcr($dcr)



    {



        $this->dcr = $dcr;







        return $this;



    }







    /**



     * Get dcr



     *



     * @return \DateTime 



     */



    public function getDcr()



    {



        return $this->dcr;



    }



    /**



     * Set apartir



     *



     * @param boolean $apartir



     * @return Spa



     */



    public function setApartir($apartir)



    {



        $this->apartir = $apartir;







        return $this;



    }







    /**



     * Get apartir



     *



     * @return boolean 



     */



    public function getApartir()



    {



        return $this->apartir;



    }







    /**



     * Add imgspa



     *



     * @param \Btob\SpaBundle\Entity\Imgspa $imgspa



     * @return Spa



     */



    public function addImgspa(\Btob\SpaBundle\Entity\Imgspa $imgspa)



    {



        $this->imgspa[] = $imgspa;







        return $this;



    }







    /**



     * Remove imgspa



     *



     * @param \Btob\SpaBundle\Entity\Imgspa $imgspa



     */



    public function removeImgspa(\Btob\SpaBundle\Entity\Imgspa $imgspa)



    {



        $this->imgspa->removeElement($imgspa);



    }







    /**



     * Get imgspa



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgspa()



    {



        return $this->imgspa;



    }







    /**



     * Set active



     *



     * @param boolean $active



     * @return Spa



     */



    public function setActive($active)



    {



        $this->active = $active;







        return $this;



    }







    /**



     * Get active



     *



     * @return boolean 



     */



    public function getActive()



    {



        return $this->active;



    }


     /**
     * Add supplementsp
     *
     * @param \Btob\SpaBundle\Entity\Supplementsp $supplementsp
     * @return Spa
     */
    public function addSupplementsp(\Btob\SpaBundle\Entity\Supplementsp $supplementsp)
    {
        $this->supplementsp[] = $supplementsp;

        return $this;
    }

    /**
     * Remove supplementsp
     *
     * @param \Btob\SpaBundle\Entity\Supplementsp $supplementsp
     */
    public function removeSupplementsp(\Btob\SpaBundle\Entity\Supplementsp $supplementsp)
    {
        $this->supplementsp->removeElement($supplementsp);
    }

    /**
     * Get supplementsp
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplementsp()
    {
        return $this->supplementsp;
    }
  
    
     /**
     * Add spaprice
     *
     * @param \Btob\SpaBundle\Entity\Spaprice $spaprice
     * @return Spa
     */
    public function addSpaprice(\Btob\SpaBundle\Entity\Spaprice $spaprice)
    {
        $this->spaprice[] = $spaprice;

        return $this;
    }

    /**
     * Remove spaprice
     *
     * @param \Btob\SpaBundle\Entity\Spaprice $spaprice
     */
    public function removeSpaprice(\Btob\SpaBundle\Entity\Spaprice $spaprice)
    {
        $this->spaprice->removeElement($spaprice);
    }

    /**
     * Get Spaprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSpaprice()
    {
        return $this->spaprice;
    }   
    
    
    
     /**
     * Add reductionsp
     *
     * @param \Btob\SpaBundle\Entity\Reductionsp $reductionsp
     * @return Spa
     */
    public function addReductionsp(\Btob\SpaBundle\Entity\Reductionsp $reductionsp)
    {
        $this->reductionsp[] = $reductionsp;

        return $this;
    }

    /**
     * Remove reductionsp
     *
     * @param \Btob\SpaBundle\Entity\Reductionsp $reductionsp
     */
    public function removeReductionsp(\Btob\SpaBundle\Entity\Reductionsp $reductionsp)
    {
        $this->reductionsp->removeElement($reductionsp);
    }

    /**
     * Get reductionsp
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReductionsp()
    {
        return $this->reductionsp;
    } 
    
    
    
    /**
     * Set prixavance
     *
     * @param float $prixavance
     * @return Spa
     */

    public function setPrixavance($prixavance)
    {
        $this->prixavance = $prixavance;
        return $this;
    }

    /**
     * Get prixavance
     *
     * @return float 
     */

    public function getPrixavance()
    {
        return $this->prixavance;

    }    
    
    
       
     /**
     * Set ageenfmin
     *
     * @param string $ageenfmin
     * @return Spa
     */

    public function setAgeenfmin($ageenfmin)

    {
        $this->ageenfmin = $ageenfmin;
        return $this;

    }



    /**
     * Get ageenfmin
     *
     * @return string 
     */

    public function getAgeenfmin()

    {
        return $this->ageenfmin;

    }
    
    
     /**
     * Set ageenfmax
     *
     * @param string $ageenfmax
     * @return Spa
     */

    public function setAgeenfmax($ageenfmax)

    {
        $this->ageenfmax = $ageenfmax;
        return $this;

    }



    /**
     * Get ageenfmax
     *
     * @return string 
     */

    public function getAgeenfmax()

    {
        return $this->ageenfmax;

    }
    
    



    /**



     * Set prixpromo



     *



     * @param float $prixpromo



     * @return Spa



     */



    public function setPrixpromo($prixpromo)



    {



        $this->prixpromo = $prixpromo;







        return $this;



    }







    /**



     * Get prixpromo



     *



     * @return float 



     */



    public function getPrixpromo()



    {



        return $this->prixpromo;



    }

      /**
     * Set marge
     *
     * @param float $marge
     * @return Spa
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        
	   
    
}



