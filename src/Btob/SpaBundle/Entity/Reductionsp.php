<?php

namespace Btob\SpaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reductionsp
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SpaBundle\Entity\ReductionspRepository")
 */
class Reductionsp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    /**
     * @ORM\ManyToOne(targetEntity="Spa", inversedBy="reductionsp")
     * @ORM\JoinColumn(name="spa_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $spa;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Reductionsp
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Reductionsp
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set spa
     *
     * @param \Btob\SpaBundle\Entity\Spa $spa
     * @return Reductionsp
     */
    public function setSpa(\Btob\SpaBundle\Entity\Spa $spa = null)
    {
        $this->spa = $spa;

        return $this;
    }

    /**
     * Get spa
     *
     * @return \Btob\SpaBundle\Entity\Spa 
     */
    public function getSpa()
    {
        return $this->spa;
    }
}
