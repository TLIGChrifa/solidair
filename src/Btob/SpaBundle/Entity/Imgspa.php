<?php

namespace Btob\SpaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgspa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SpaBundle\Entity\ImgspaRepository")
 */
class Imgspa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Spa", inversedBy="imgspa")
     * @ORM\JoinColumn(name="spa_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $spa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgspa
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgspa
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }

    /**
     * Set spa
     *
     * @param \Btob\SpaBundle\Entity\Spa $spa
     * @return Imgspa
     */
    public function setSpa(\Btob\SpaBundle\Entity\Spa $spa = null)
    {
        $this->spa = $spa;

        return $this;
    }

    /**
     * Get spa
     *
     * @return \Btob\SpaBundle\Entity\Spa 
     */
    public function getSpa()
    {
        return $this->spa;
    }
}
