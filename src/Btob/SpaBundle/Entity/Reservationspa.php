<?php

namespace Btob\SpaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationspa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SpaBundle\Entity\ReservationspaRepository")
 */
class Reservationspa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

        /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;
    
    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float")
     */
    private $avance;


    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    

    /**
     * @ORM\ManyToOne(targetEntity="Spa", inversedBy="reservationspa")
     * @ORM\JoinColumn(name="spa_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $spa;
    
    /**
     * @ORM\ManyToOne(targetEntity="Spaprice", inversedBy="reservationspa")
     * @ORM\JoinColumn(name="spaprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $spaprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationspa")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationspa")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

   
    /**
     * @ORM\OneToMany(targetEntity="Reservationspdetail", mappedBy="reservationspa", cascade={"remove"})
     */
    protected $reservationspdetail;
   
    
     /**
     * @var string
     *
     * @ORM\Column(name="jour", type="string", nullable=true)
     */
    private $jour;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationspa
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationspa
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set spaprice
     *
     * @param \Btob\SpaBundle\Entity\Spaprice $spaprice
     * @return Reservationspa
     */
    public function setSpaprice(\Btob\SpaBundle\Entity\Spaprice $spaprice = null)
    {
        $this->spaprice = $spaprice;

        return $this;
    }

    /**
     * Get spaprice
     *
     * @return \Btob\SpaBundle\Entity\Spaprice 
     */
    public function getSpaprice()
    {
        return $this->spaprice;
    }
    
       
    
    /**
     * Set spa
     *
     * @param \Btob\SpaBundle\Entity\Spa $spa
     * @return Reservationspa
     */
    public function setSpa(\Btob\SpaBundle\Entity\Spa $spa = null)
    {
        $this->spa = $spa;

        return $this;
    }

    /**
     * Get spa
     *
     * @return \Btob\SpaBundle\Entity\Spa 
     */
    public function getSpa()
    {
        return $this->spa;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationspa
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationspa
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationspdetail
     *
     * @param \Btob\SpaBundle\Entity\Reservationspdetail $reservationspdetail
     * @return Reservationspa
     */
    public function addReservationspdetail(\Btob\SpaBundle\Entity\Reservationspdetail $reservationspdetail)
    {
        $this->reservationspdetail[] = $reservationspdetail;

        return $this;
    }

    /**
     * Remove reservationspdetail
     *
     * @param \Btob\SpaBundle\Entity\Reservationspdetail $reservationspdetail
     */
    public function removeReservationspdetail(\Btob\SpaBundle\Entity\Reservationspdetail $reservationspdetail)
    {
        $this->reservationspdetail->removeElement($reservationspdetail);
    }

    /**
     * Get reservationspdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationspdetail()
    {
        return $this->reservationspdetail;
    }

        /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }

        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }

 
    /**
     * Set jour
     *
     * @param string $jour
     * @return Reservation
     */
    public function setJour($jour)
    {
        $this->jour = $jour;
        return $this;
    }

    /**
     * Get jour
     *
     * @return string
     */
    public function getJour()
    {
        return $this->jour;
    }   
    
}
