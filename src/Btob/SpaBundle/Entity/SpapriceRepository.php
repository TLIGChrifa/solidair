<?php

namespace Btob\SpaBundle\Entity;

use Doctrine\ORM\EntityRepository;
/**
 * SpapriceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SpapriceRepository extends EntityRepository
{
  
   public function findbydateSpa($spaid,$dated) {
        $qb = $this->createQueryBuilder('r')
                ->leftJoin('r.spa', 'u')
                ->where("r.dated <='" . $dated . "'")
                ->andWhere("r.dates >= '" . $dated . "'")
               ->andWhere('u.id =:id')
               ->setParameter('id', $spaid)
                ->getQuery()
                ->getOneOrNullResult();
        //echo $qb->getQuery()->getSQL();exit;
        return $qb;
    }  
   public function listPrice()
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.spa', 'u')
            ->andWhere('u.active =:act')
            ->setParameter('act', true)
            ->groupBy('a.spa')    
           ;
        return $query->getQuery()->getResult();
    }
    
    
    
       public function listPriceIndex()
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.spa', 'u')
            ->andWhere('u.active =:act')
            ->andWhere('u.pindex =:pind')    
            ->setParameter('act', true)
            ->setParameter('pind', true)    
            ->groupBy('a.spa')
            ->orderBy('u.dcr', 'DESC')
            ->setMaxResults(9)    
           ;
        return $query->getQuery()->getResult();
    }
    
    
    public function findBySpa($spaid)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.spa', 'u')
            ->andWhere('u.id =:id')
            ->setParameter('id', $spaid);
        return $query->getQuery()->getResult();
    }
    
    
    public function calcul($pricespa,$nbad,$nbenf,$totalsupp,$totalsuppenf,$totalreduc,$totalreducenf)
    { 
        
      
            

            
       $price = ($nbad*$pricespa->getPricead())+$totalsupp-$totalreduc+($nbenf*$pricespa->getPriceenf())+$totalsuppenf-$totalreducenf;
    
            
        
        
        
        return $price;
    }
    
    
}
