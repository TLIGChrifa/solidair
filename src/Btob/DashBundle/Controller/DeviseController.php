<?php

namespace Btob\DashBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\DashBundle\Entity\Devise;
use Symfony\Component\HttpFoundation\Request;
use Btob\DashBundle\Form\DeviseType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Common\PriceCurr;

class DeviseController extends Controller {

    public function indexAction() {
        $devise = $this->getDoctrine()
                ->getRepository('BtobDashBundle:Devise')
                ->findAll();
        return $this->render('BtobDashBundle:Devise:index.html.twig', array('entities' => $devise));
    }

    public function addAction() {
        $devise = new Devise();
        $form = $this->createForm(new DeviseType(), $devise);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($devise);
                $em->flush();
                $id = $devise->getId();
                $data = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findAll();
                foreach ($data as $key => $value) {
                    if ($devise->getDef() && $value->getId() != $id) {
                        $value->setDef(false);
                        $em->persist($value);
                        $em->flush();
                    }
                    if ($devise->getDef() && $value->getId() != $id) {
                        $value->setCurrency(1);
                        $em->persist($value);
                        $em->flush();
                    }
                }
                return $this->redirect($this->generateUrl('btob_dashdevise_homepage'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobDashBundle:Devise:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id) {
        $request = $this->get('request');
        $devise = $this->getDoctrine()
                ->getRepository('BtobDashBundle:Devise')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DeviseType(), $devise);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            $id = $devise->getId();
            $data = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->findAll();
            foreach ($data as $key => $value) {
                if ($devise->getDef() && $value->getId() != $id) {
                    $value->setDef(false);
                    $em->persist($value);
                    $em->flush();
                }
                if ($devise->getDef() && $value->getId() != $id) {
                    $value->setCurrency(1);
                    $em->persist($value);
                    $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('btob_dashdevise_homepage'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobDashBundle:Devise:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }

    public function deleteAction(Devise $devise) {
        $em = $this->getDoctrine()->getManager();

        if (!$devise) {
            throw new NotFoundHttpException("Devise non trouvée");
        }
        $em->remove($devise);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_dashdevise_homepage'));
    }

    public function convertAction() {
        $devise = $this->getDoctrine()
                ->getRepository('BtobDashBundle:Devise')
                ->findAll();
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        if ($request->getMethod() == 'POST') {
            foreach ($request->request->get("tab") as $key => $value) {
                $data = $this->getDoctrine()->getRepository('BtobDashBundle:Devise')->find($key);
               
                $data->setCurrency($value);
                $em->persist($data);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('btob_dashdevise_convert'));
        }
        $symb = "";
        foreach ($devise as $key => $value) {
            if ($value->getDef()) {
                $symb = $value->getSymb();
            }
        }
        return $this->render('BtobDashBundle:Devise:convert.html.twig', array('entities' => $devise, 'sym' => $symb)
        );
    }

    public function convertxAction() {
        $request = $this->get('request');
        $currency_from = $request->request->get('def');
        $currency_to = $request->request->get('to');
        $chiffre = $request->request->get('chiffre');
        $amount_from = 1;
        $price = new PriceCurr();
        $pp=(float)$price->getPrice(1, $currency_from, $currency_to);
        echo number_format($pp,$chiffre,'.','');
        
        exit;
    }

}
