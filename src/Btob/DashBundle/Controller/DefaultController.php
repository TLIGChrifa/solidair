<?php

namespace Btob\DashBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\SejourBundle\Entity\Reservationsejour;
use Btob\EvenementBundle\Entity\Reservationevenement;
use Btob\OmraBundle\Entity\Reservationomra;
use Btob\VoitureBundle\Entity\Reservationvoiture;
use Btob\HotelBundle\Entity\Reservation;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DefaultController extends Controller
{
    public function deconnexionAction()
    {
            $em = $this->getDoctrine()->getManager();
            $hist = new Historique();
            $hist->setIp($_SERVER['REMOTE_ADDR']);
            $hist->setType("BO");
            $hist->setBundle("Deconnexion");
            $hist->setMessage("Deconnexion du compte");
            $hist->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($hist);
            $em->flush();
            $this->get('security.context')->setToken(null);
            $this->get('request')->getSession()->invalidate();
    //    return $this->redirectToRoute('fos_user_security_logout');
        return $this->redirectToRoute('front_btob_homepage');
    }
    public function indexAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $nameagence= $this->getDoctrine()
        ->getRepository('UserUserBundle:User')
        ->find($user->getId());
        
    $solde = $this->getDoctrine()
        ->getRepository('UserUserBundle:Solde')
        ->findByAgence($user->getId());

      //  dump($solde).die;
        if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
            $tab=array();
            foreach($solde as $value){
                if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                    $tab[]=$value;
                }
            }
            $solde=$tab;
    
        }
        $solde=array_reverse($solde);
       // dump($solde).die;
         // dump($user).die;
        // calcule des résa par interval de date
        $dt=new \DateTime();
        $dtday=$dt->modify('-1 day');
        $dt=new \DateTime();
        $dtweek=$dt->modify('-7 day');
        $dt=new \DateTime();
        $dtmonth=$dt->modify('-1 month');
        $dt=new \DateTime();
        $user = $this->get('security.context')->getToken()->getUser();
        $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');
        $resaday=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtday);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaday as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaday = $tabx;
        }
        $totalday=0;
        
        foreach($resaday as $value){
            $totalday += $value->getTotal();
        }
        
        $totalbaseday=$totalday;
        foreach($resaday as $value){
            $totalbaseday -= $value->getTotalbase();
        }
        
        
       
        $resaweek=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtweek);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resaweek as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resaweek = $tabx;
        }
        $totalweek=0;
        foreach($resaweek as $value){
            $totalweek += $value->getTotal();
        }
        
        
        $totalbaseweek=$totalweek;
         foreach($resaweek as $value){
            $totalbaseweek -= $value->getTotalbase();
        }
        
        
        
        $resamonth=$this->getDoctrine()->getRepository('BtobHotelBundle:Reservation')->getResaDash($dt,$dtmonth);
        if (in_array('AGENCEID', $user->getRoles()) || in_array('SALES', $user->getRoles())) {
            $tabx = array();
            foreach ($resamonth as $value) {
                if ($value->getUser()->getId() == $user->getId()) {
                    $tabx[] = $value;
                }
            }
            $resamonth = $tabx;
        }
        $totalmonth=0;
        foreach($resamonth as $value){
            $totalmonth += $value->getTotal();
        }
        $totalbasemonth=$totalmonth;
        
        foreach($resamonth as $value){
            $totalbasemonth -= $value->getTotalbase();
        }
        //hotels number
        $hotels=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findAll();
            $countH=count($hotels);

        $hotelsA=$this->getDoctrine()
            ->getRepository('BtobHotelBundle:Hotel')
            ->findBy(array('act' => 1));
            $countHA=count($hotelsA);

        //circuit number
    $circuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->findAll();
    $countC=count($circuit);
   $circuitA = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->findBy(array('active' => 1));
    $countCA=count($circuitA);
    //voyage number

    $voyage=$this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findAll();
    $countV=count($voyage);
    $voyageA = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(array('act' => 1));
    $countVA=count($voyageA);
    //soirées number

    $soirees = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->findAll();
    $countS= count($soirees);
    $soireesA = $this->getDoctrine()->getRepository('BtobCroissiereBundle:Croissiere')->findby(array('act' => 1));
    $countSA=count($soireesA);
    //omra number

    $omras = $this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findAll();
    $countO=count($omras);
    //$omrasA=$this->getDoctrine()->getRepository("BtobOmraBundle:Omra")->findby(array('active' => 1));
    //$countOA=count($omrasA);

    //events number
    $Evenement = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenement")->findAll();
    $countE=count($Evenement);

    $EvenementA = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenement")->findby(array('act' => 1));
    $countEA=count($EvenementA);
    //hotels reservation
    $resHR = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findBy(array('etat'=>1));
    $countRHR=count($resHR);

    $resHR = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findBy(array('etat'=>0));
    $countRHA=count($resHR);

    $resHR = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findBy(array('etat'=>2));
    $countRHT=count($resHR);
      $resHR = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findBy(array('etat'=>3));
    $countRHP=count($resHR);

      $resHR = $this->getDoctrine()
            ->getRepository('BtobHotelBundle:Reservation')
            ->findBy(array('etat'=>2));
    $countRHSD=count($resHR);

    //Circuit reservation

    $resRC = $this->getDoctrine()
    ->getRepository("BtobCuircuitBundle:Reservationcircuit")
    ->findBy(array('etat'=>0));
    $countRCA=count($resRC);

      $resRC = $this->getDoctrine()
    ->getRepository("BtobCuircuitBundle:Reservationcircuit")
    ->findBy(array('etat'=>1));
    $countRCEA=count($resRC);

    $resRC = $this->getDoctrine()
    ->getRepository("BtobCuircuitBundle:Reservationcircuit")
    ->findBy(array('etat'=>2));
    $countRCP=count($resRC);
// voyage reservation
    $resRV = $this->getDoctrine()
    ->getRepository('BtobSejourBundle:Reservationsejour')
    ->findBy(array('etat'=>0));
    $countRVA=count($resRV);

    $resRV = $this->getDoctrine()
    ->getRepository('BtobSejourBundle:Reservationsejour')
    ->findBy(array('etat'=>2));
    $countRVP=count($resRV);

    $resRV = $this->getDoctrine()
    ->getRepository('BtobSejourBundle:Reservationsejour')
    ->findBy(array('etat'=>1));
    $countRVEA=count($resRV);

// soirées reservation 
    $resRS = $this->getDoctrine()
    ->getRepository("BtobCroissiereBundle:Reservationcroi")
    ->findBy(array('etat'=>0));
    $countRSA=count($resRS);

        $resRS = $this->getDoctrine()
    ->getRepository("BtobCroissiereBundle:Reservationcroi")
    ->findBy(array('etat'=>1));
    $countRSEA=count($resRS);

    $resRS = $this->getDoctrine()
    ->getRepository("BtobCroissiereBundle:Reservationcroi")
    ->findBy(array('etat'=>2));
    $countRSP=count($resRS);
//events reservations
    $resRE = $this->getDoctrine()
    ->getRepository('BtobEvenementBundle:Reservationevenement')
    ->findBy(array('etat'=>0));
    $countREA=count($resRE);

    $resRE = $this->getDoctrine()
    ->getRepository('BtobEvenementBundle:Reservationevenement')
    ->findBy(array('etat'=>1));
    $countREP=count($resRE);

    $resRE = $this->getDoctrine()
    ->getRepository('BtobEvenementBundle:Reservationevenement')
    ->findBy(array('etat'=>2));
    $countREEA=count($resRE);
//     $reservations_hotel = $this->getDoctrine()
//             ->getRepository('BtobHotelBundle:ReservationHotel')
// 			->findAll();
    $em = $this->getDoctrine()->getManager();   
	$reservations_hotel = $em->createQueryBuilder()
			->select('r')
			->from('BtobHotelBundle:ReservationHotel','r')
			->where('r.montantpaye IS NOT NULL')
			->getQuery()
			->getResult();
    $reservations_maison = $em->getRepository('BtobHotelBundle:Reservationmaisondhote')->findAll();

    
    $reservation_Circuit = $this->getDoctrine()
            ->getRepository('BtobCuircuitBundle:Reservationcircuit')
            ->findAll();
    $reservation_Vo = $em->getRepository('BtobSejourBundle:Reservationsejour')->findAll();
   // var_dump($reservation_Circuit).Die;
        $em = $this->getDoctrine()->getManager();
        $qb=$em->createQueryBuilder();
        $entities=$qb->select('h.bundle')
            ->from('UserUserBundle:Historique','h')
            ->where('h.user = :user_id')
            ->andWhere('h.ip = :ip')
            ->setParameter('user_id', $this->get('security.context')->getToken()->getUser())
            ->setParameter('ip', $_SERVER['REMOTE_ADDR'])
            ->orderBy('h.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        if($entities != null and $entities[0]["bundle"]==="Deconnexion" ){
            $hist = new Historique();
            $hist->setIp($_SERVER['REMOTE_ADDR']);
            $hist->setType("BO");
            $hist->setBundle("Connexion");
            $hist->setMessage("Connexion du compte");
            $hist->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($hist);
            $em->flush();
        }
        if($entities == null){
            $hist = new Historique();
            $hist->setIp($_SERVER['REMOTE_ADDR']);
            $hist->setType("BO");
            $hist->setBundle("Connexion");
            $hist->setMessage("Connexion du compte");
            $hist->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($hist);
            $em->flush();
        }
        $em = $this->getDoctrine()->getManager();
        $cnx=$em->createQueryBuilder();
        $connexion=$cnx->select('h.dcr')
            ->from('UserUserBundle:Historique','h')
            ->where('h.user = :user_id')
            ->andwhere('h.bundle = :bndl')
            ->setParameter('user_id', $this->get('security.context')->getToken()->getUser())
            ->setParameter('bndl', "Connexion")
            ->orderBy('h.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        $dcnx=$em->createQueryBuilder();
        $deconnexion=$dcnx->select('h.dcr')
            ->from('UserUserBundle:Historique','h')
            ->where('h.user = :user_id')
            ->andwhere('h.bundle = :bndl')
            ->setParameter('user_id', $this->get('security.context')->getToken()->getUser())
            ->setParameter('bndl', "Deconnexion")
            ->orderBy('h.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
            
            if($connexion != null ){
                $con=date_format($connexion[0]["dcr"], 'Y-m-d H:i:s');
            }else{ 
                $con=$entities;
            }
            if($deconnexion != null ){
                $decon=date_format($deconnexion[0]["dcr"], 'Y-m-d H:i:s');
            }else{ 
                $decon='';
            }
        return $this->render('BtobDashBundle:Default:index.html.twig', array(
            'user' => $user,
            'reservation_Circuit'=> $reservation_Circuit,
            'admin' => $admin,
            'totalday' => $totalday,
            'totalbaseday' => $totalbaseday,
            'totalweek' => $totalweek,
            'totalbaseweek' => $totalbaseweek,
            'totalmonth' => $totalmonth,
            'totalbasemonth' => $totalbasemonth,
            'reservations_hotel' => $reservations_hotel,
            'countH'=>$countH,
            'countHA'=>$countHA,
            'countC'=>$countC,
            'countCA'=>$countCA,
            'countV'=>$countV,
            'countVA'=>$countVA,
            'countS'=>$countS,
            'countSA'=>$countSA,
            'countO'=>$countO,
            //'countOA'=>$countOA,
            'countE'=>$countE,
            'countEA'=>$countEA,
            'countRHR'=>$countRHR,
            'countRHA'=>$countRHA,
            'countRHT'=>$countRHT,
            'countRHP'=>$countRHP,
            'countRHSD'=>$countRHSD,
            'countRCA'=>$countRCA,
            'countRCP'=>$countRCP,
            'countRCEA'=>$countRCEA,
            'countRVA'=>$countRVA,
            'countRVP'=>$countRVP,
            'countRVEA'=>$countRVEA,
            'countRSA'=>$countRSA,
            'countRSP'=>$countRSP,
            'countRSEA'=>$countRSEA,
            'countREA'=>$countREA,
            'countREP'=>$countREP,
            'countREEA'=>$countREEA,
            'connexion'=>$con,
            'deconnexion'=>$decon,
            'entitiessolde' => $solde,
            'reservation_Vo'=>$reservation_Vo,
            'entities'=>  $reservations_maison
            
        ));
    }
    
    public function statistiqueAction(Request $request)
    {       
    if($this->get('security.context')->getToken()->getUser()->getRoles()[0]=='ROLE_SUPER_ADMIN'){
      //  var_dump('gh').die;
        $em = $this->getDoctrine()->getManager();
        $user_id = $this->get('security.context')->getToken()->getUser()->getId();
        $solde = $this->getDoctrine()
            ->getRepository('UserUserBundle:Solde')
            ->findByAgence($user_id);

        if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
            $tab=array();
            foreach($solde as $value){
                if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                    $tab[]=$value;
                }
            }
            $solde=$tab;

        }
        $solde=array_reverse($solde);
        $agence=$request->request->get('agence');
        $agent=$request->request->get('agent');
        $dateDeb = $request->request->get('debDate');
        $dateEnd = $request->request->get('endDate');
        if ($request->getMethod() == 'POST') {
            if($agence != null and $agent == null and $dateDeb == null and $dateEnd == null){
                $agc=$em->createQueryBuilder('u')->select('u')->from('UserUserBundle:User','u')->Where('u.id = :agence')->setParameter('agence', $agence)->getQuery()->getResult();
                $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->Where('c.user = :usr')->setParameter('usr', $agc)->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->Where('ev.user = :usr')->setParameter('usr', $agc)->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->Where('h.user = :usr')->setParameter('usr', $agc)->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->Where('o.user = :usr')->setParameter('usr', $agc)->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->Where('s.user = :usr')->setParameter('usr', $agc)->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->Where('v.agent = :usr')->setParameter('usr',$agc)->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->Where('cr.user = :usr')->setParameter('usr',$agc)->getQuery()->getSingleScalarResult();
            }
            if($agent != null and $agence == null and $dateDeb == null and $dateEnd == null){
                $agt=$em->createQueryBuilder('u')->select('u')->from('UserUserBundle:User','u')->Where('u.id = :agent')->setParameter('agent', $agent)->getQuery()->getResult();
                $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->Where('c.user = :usr')->setParameter('usr', $agt)->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->Where('ev.user = :usr')->setParameter('usr', $agt)->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->Where('h.user = :usr')->setParameter('usr', $agt)->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->Where('o.user = :usr')->setParameter('usr', $agt)->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->Where('s.user = :usr')->setParameter('usr', $agt)->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->Where('v.agent = :usr')->setParameter('usr',$agt)->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->Where('cr.user = :usr')->setParameter('usr',$agt)->getQuery()->getSingleScalarResult();
            }
            if($dateDeb != null and $dateEnd != null and $agent == null and $agence == null){
               $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->Where('c.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(c.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->Where('ev.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(ev.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->Where('h.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(h.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->Where('o.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(o.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->Where('s.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(s.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->Where('v.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(v.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->Where('cr.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(cr.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
            }
            if($dateDeb != null and $dateEnd != null and $agent != null and $agence == null){
                $agt=$em->createQueryBuilder('u')->select('u')->from('UserUserBundle:User','u')->Where('u.id = :agent')->setParameter('agent', $agent)->getQuery()->getResult();
               $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->Where('c.user = :usr')->setParameter('usr',$agt)->andWhere('c.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(c.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->Where('ev.user = :usr')->setParameter('usr',$agt)->andWhere('ev.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(ev.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->Where('h.user = :usr')->setParameter('usr',$agt)->andWhere('h.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(h.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->Where('o.user = :usr')->setParameter('usr',$agt)->andWhere('o.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(o.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->Where('s.user = :usr')->setParameter('usr',$agt)->andWhere('s.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(s.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->Where('v.agent = :usr')->setParameter('usr',$agt)->andWhere('v.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(v.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->Where('cr.user = :usr')->setParameter('usr',$agt)->andWhere('cr.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(cr.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
            }
            if($dateDeb != null and $dateEnd != null and $agence != null and $agent == null){
                $agc=$em->createQueryBuilder('u')->select('u')->from('UserUserBundle:User','u')->Where('u.id = :agence')->setParameter('agence', $agence)->getQuery()->getResult();
               $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->Where('c.user = :usr')->setParameter('usr',$agc)->andWhere('c.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(c.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->Where('ev.user = :usr')->setParameter('usr',$agc)->andWhere('ev.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(ev.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->Where('h.user = :usr')->setParameter('usr',$agc)->andWhere('h.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(h.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->Where('o.user = :usr')->setParameter('usr',$agc)->andWhere('o.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(o.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->Where('s.user = :usr')->setParameter('usr',$agc)->andWhere('s.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(s.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->Where('v.agent = :usr')->setParameter('usr',$agc)->andWhere('v.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(v.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->Where('cr.user = :usr')->setParameter('usr',$agc)->andWhere('cr.dcr >= :dd')->setParameter('dd',(new \DateTime($dateDeb))->format('Y-m-d') )->andWhere('(cr.dcr) <= :ds')->setParameter('ds',(new \DateTime($dateEnd))->format('Y-m-d') )->getQuery()->getSingleScalarResult();
            }

            if($agence == null and $agent == null and $dateDeb == null and $dateEnd == null){
                $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->getQuery()->getSingleScalarResult();
                $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->getQuery()->getSingleScalarResult();
                $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->getQuery()->getSingleScalarResult();
                $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->getQuery()->getSingleScalarResult();
                $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->getQuery()->getSingleScalarResult();
                $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->getQuery()->getSingleScalarResult();
                $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->getQuery()->getSingleScalarResult();
            }
            
        }else{
      
        $em = $this->getDoctrine()->getManager();         
        $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')->getQuery()->getSingleScalarResult();

        $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')->getQuery()->getSingleScalarResult();
        $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')->getQuery()->getSingleScalarResult();
        $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')->getQuery()->getSingleScalarResult();
        $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')->getQuery()->getSingleScalarResult();
        $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')->getQuery()->getSingleScalarResult();
        $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')->getQuery()->getSingleScalarResult();
    }
    } elseif($this->get('security.context')->getToken()->getUser()->getRoles()[0] =='AGENCEID'){
        
        $user_id = $this->get('security.context')->getToken()->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $circuit=$em->createQueryBuilder('c')->select('count(c.id)')->from('BtobCuircuitBundle:Reservationcircuit','c')
             ->leftjoin('c.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $evenement=$em->createQueryBuilder('ev')->select('count(ev.id)')->from('BtobEvenementBundle:Reservationevenement','ev')
             ->leftjoin('ev.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $hotel=$em->createQueryBuilder('h')->select('count(h.id)')->from('BtobHotelBundle:Reservation','h')
             ->leftjoin('h.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $omra=$em->createQueryBuilder('o')->select('count(o.id)')->from('BtobOmraBundle:Reservationomra','o')
             ->leftjoin('o.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $sejour=$em->createQueryBuilder('s')->select('count(s.id)')->from('BtobSejourBundle:Reservationsejour','s')
             ->leftjoin('s.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $vol=$em->createQueryBuilder('v')->select('count(v.id)')->from('BtobVoleBundle:Reservationbateau','v')
             ->leftjoin('v.agent','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
        $croi=$em->createQueryBuilder('cr')->select('count(cr.id)')->from('BtobCroissiereBundle:Reservationcroi','cr')
             ->leftjoin('cr.user','u')->andWhere('u.id = :userid')->setParameter('userid',$user_id)->getQuery()->getSingleScalarResult();
             $nameagence= $this->getDoctrine()
             ->getRepository('UserUserBundle:User')
             ->find($user_id);
              
         $solde = $this->getDoctrine()
             ->getRepository('UserUserBundle:Solde')
             ->findByAgence($user_id);
     
         if(in_array('AGENCEID',$this->get('security.context')->getToken()->getUser()->getRoles())){
             $tab=array();
             foreach($solde as $value){
                 if($value->getAgence()->getId()==$this->get('security.context')->getToken()->getUser()->getId()){
                     $tab[]=$value;
                 }
             }
             $solde=$tab;
     
         }
         $solde=array_reverse($solde);
    
            }
$data[]=array(
    "Circuit"=>$circuit,
    "Evénement"=>$evenement,
    "Hôtel"=>$hotel,
    "Omra"=>$omra,
    "Voyage organisé"=>$sejour,
    "Vol"=>$vol,
    "Croisière"=>$croi,
);
$nb=$circuit+$evenement+$hotel+$omra+$sejour;
        $userManager = $this->container->get('fos_user.user_manager');
        $agency = $userManager->findAgences();
        $user = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAllsauf();

            return $this->render('BtobDashBundle:Default:statistique.html.twig', array(
                'user' => $user,
                'agence' => $agency,
                'circuit' => $circuit,
                'evenement' => $evenement,
                'hotel' => $hotel,
            //    'voiture' => $voiture,
                'entitiessolde'=>$solde,
                'omra' => $omra,
                'sejour' => $sejour,
                'vol'=>$vol,
                'croissiere'=>$croi,
                'nb' => $nb,
                'data' => $data[0],
               
            ));


            return $this->render($view, $data);
    }
    
}
