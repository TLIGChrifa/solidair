<?php



namespace Btob\DashBundle\Menu;



use Knp\Menu\FactoryInterface;

use Symfony\Component\DependencyInjection\ContainerAware;



use Btob\ActiviteBundle\Entity\Type;

class Builder extends ContainerAware

{

    public function contractingMenu(FactoryInterface $factory, array $options)

    {

        $menu = $factory->createItem('root', array(

            'childrenAttributes' => array(

                'class' => 'clearfix',

                'id' => 'mobile-nav'

            ),

        ));

        $menu->addChild('Fichier', array('route' => 'btob_dash_homepage'));

        $menu['Fichier']->addChild('dashboard', array('route' => 'btob_dash_homepage', 'label' => 'Tableau de board'));

        $menu['Fichier']->addChild('Déconnexion', array('route' => 'fos_user_security_logout', 'label' => 'Déconnexion'));



        $menu->addChild('Hôtel', array('route' => 'btob_hotel_homepage'));

        $menu['Hôtel']->addChild('gotel', array('route' => 'btob_hotel_homepage', 'label' => 'Gestion des Hôtels'));

        $menu['Hôtel']->addChild('Pays', array('route' => 'btob_pays_homepage', 'label' => 'Gestion des Pays'));

        $menu['Hôtel']->addChild('Ville', array('route' => 'btob_ville_homepage', 'label' => 'Gestion des Villes'));

        $menu['Hôtel']->addChild('localisation', array('route' => 'btob_localisation_homepage', 'label' => 'Gestion des Localisations'));

        $menu['Hôtel']->addChild('Theme', array('route' => 'btob_theme_homepage', 'label' => 'Gestion des Themes'));

        $menu['Hôtel']->addChild('options', array('route' => 'btob_options_homepage', 'label' => 'Gestion des options'));

        $menu['Hôtel']->addChild('child', array('route' => 'btob_child_homepage', 'label' => 'Gestion catégorie d\'enfant'));

        $menu['Hôtel']->addChild('amenagement', array('route' => 'btob_amenagement_homepage', 'label' => 'Gestion des Aménagements'));

        $menu['Hôtel']->addChild('amrangement', array('route' => 'btob_arangement_homepage', 'label' => 'Gestion des arrangements'));

        $menu['Hôtel']->addChild('room', array('route' => 'btob_room_homepage', 'label' => 'Gestion des chambres'));

        $menu['Hôtel']->addChild('supplement', array('route' => 'btob_supplement_homepage', 'label' => 'Gestion des suppléments'));

        $menu['Hôtel']->addChild('basecalcule', array('route' => 'btob_baseroom_homepage', 'label' => 'Base de calcule pour les chambres'));

        return $menu;



    }



    public function agenceMenu(FactoryInterface $factory, array $options)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        // $entities = $em->getRepository('BtobActiviteBundle:Activite')->findAll();
        $entities = $em->getRepository("BtobActiviteBundle:Type")->findby(array('act' => 1));
        $menu = $factory->createItem('root', array(
            'childrenAttributes' => array(
                'class' => 'clearfix',
                'id' => 'mobile-nav'
            ),
        ));
        
        $menu->addChild('Fichier', array('route' => 'btob_dash_homepage'));
        $menu['Fichier']->addChild('dashboard', array('route' => 'btob_dash_homepage', 'label' => 'Tableau de board'));
        $menu['Fichier']->addChild('Déconnexion', array('route' => 'fos_user_security_logout', 'label' => 'Déconnexion'));
        $utilisateur= $this->container->get('security.context')->getToken()->getUser();

    if($utilisateur->getDevise())
    {
        $menu->addChild('Etat des Réservations', array('route' => 'btob_reservationback_homepage'));
        if($utilisateur->getActcircuit()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Circuit', array('route' => 'btob_resaback_cuircuit', 'label' => 'Réservations Circuit'));
            $menu['Etat des Réservations']->addChild('Réservations Circuits Personalisé', array('route' => 'circuits_personalisation', 'label' => 'Réservations Circuit Personalisé'));
        }
       if ($utilisateur->getActevenement() == 1) {
            $menu['Etat des Réservations']->addChild('Réservations Evenements', array('route' => 'evenements_reservation', 'label' => 'Réservations Evenements'));
        }
        if($utilisateur->getActvole()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Vol', array('route' => 'vol', 'label' => 'Réservations Vol'));
        }
        //    $menu['Etat des Réservations']->addChild('Réservations Croisière', array('route' => 'croissiere_reservation', 'label' => 'Réservations Croisière'));
        if($utilisateur->getActsejour()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Séjours', array('route' => 'sejours_reservation', 'label' => 'Réservations Séjours'));
            $menu['Etat des Réservations']->addChild('Réservations Séjours Personalisé', array('route' => 'sejours_personalisation', 'label' => 'Réservations Séjours Personalisé'));
        }
        if($utilisateur->getActbienetre()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Spa', array('route' => 'btob_resaback_spa', 'label' => 'Réservations Spa'));
        }
        if($utilisateur->getActomra()== 1)
        {
            $menu['Etat des Réservations']->addChild('Omra', array('route' => 'reservationomra', 'label' => 'Réservations Omra'));
        }
        if($utilisateur->getActseminaire()== 1)
        {
            $menu['Etat des Réservations']->addChild('Seminaire', array('route' => 'btob_siminaire_list_reservation_homepage', 'label' => 'Réservations seminaire'));
        }
        if ($utilisateur->getActcroissiere() == 1) {
            $menu['Etat des Réservations']->addChild('Réservations Croisière', array('route' => 'btob_resaback_croissiere', 'label' => 'Réservations Croisière'));
        }
        if($utilisateur->getActvoiture()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Voiture', array('route' => 'reservationvoituress', 'label' => 'Réservations Voiture'));
        }
        if($utilisateur->getActtransfert()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Transfert', array('route' => 'btob_reservationtransf_homepage', 'label' => 'Réservations Transfert'));
        }
         if($utilisateur->getActparc()== 1)
        {
            $menu['Etat des Réservations']->addChild('Réservations Parc', array('route' => 'btob_resaback_parc', 'label' => 'Réservations Parc'));
        }
        $menu['Etat des Réservations']->addChild('devise', array('route' => 'btob_reservationback_homepage', 'label' => 'Réservations Hôtels'));
        $menu['Etat des Réservations']->addChild('Historique de solde', array('route' => 'user_solde_agence' , 'routeParameters' => array('userid' => $utilisateur->getId()), 'label' => 'Historique de solde'));
        $menu['Etat des Réservations']->addChild('Staitistique Agence', array('route' => 'btob_stats_homepage', 'label' => 'Staitistique'));
    }
    if($utilisateur->getDevise())
        {
        $menu->addChild('Réservation', array('route' => 'btob_list_homepage'));
        $menu['Réservation']->addChild('devise', array('route' => 'btob_list_homepage', 'label' => 'Réservations Hôtels'));
      if($utilisateur->getActparc()== 1)
        {
        $menu['Réservation']->addChild('Parc', array('route' => 'btob_resa_parc', 'label' => 'Réservations Parc'));
        }
        if($utilisateur->getActomra()== 1)
        { $menu['Réservation']->addChild('Omra', array('route' => 'btob_omra_homepage', 'label' => 'Réservation Omra'));
            
        $menu['Réservation']['Omra']->setAttribute('icon', 'menu-icon fa fa-caret-right');
        }
        if($utilisateur->getActcircuit()== 1)
        { $menu['Réservation']->addChild('Circuit', array('route' => 'btob_resa_cuircuit', 'label' => 'Réservations Circuits'));}
       if ($utilisateur->getActevenement() == 1) {
            $menu['Réservation']->addChild('Réservations Evenements', array('route' => 'btob_evenement_evenement_homepage', 'label' => 'Réservations Evenements'));
        }
        if($utilisateur->getActsejour()== 1)
        { $menu['Réservation']->addChild('Réservations Séjours', array('route' => 'btob_sejour_sejour_homepage', 'label' => 'Réservations Séjours'));}
        if($utilisateur->getActvole()== 1)
        {$menu['Réservation']->addChild('Vol', array('route' => 'btob_vole_homepage', 'label' => 'Réservations Vol'));
         $menu['Réservation']->addChild('Bateau', array('route' => 'btob_volebateau_homepage', 'label' => 'Réservations Bateau'));
        }
        if($utilisateur->getActbienetre()== 1)
        { $menu['Réservation']->addChild('Bien être', array('route' => 'btob_resa_spa', 'label' => 'Réservations Bien être')); }
        if ($utilisateur->getActcroissiere() == 1) {
            $menu['Réservation']->addChild('Croissiere', array('route' => 'btob_croissiere_homepage', 'label' => 'croissiere'));
        }
        if($utilisateur->getActvoiture()== 1)
        { $menu['Réservation']->addChild('Voiture', array('route' => 'btob_voiture_homepage', 'label' => 'Réservations Voiture')); }
        if($utilisateur->getActtransfert()== 1)
        { $menu['Réservation']->addChild('Transfert', array('route' => 'btob_transfertt_homepage', 'label' => 'Réservations Transfert')); }
        if($utilisateur->getActseminaire()== 1)
        { $menu['Réservation']->addChild('Siminaire', array('route' => 'btob_siminaire_homepage', 'label' => 'Réservations Siminaire')); }
        }
        //$menu->addChild('Panier', array('route' => 'btob_caddy_homepage'));
        return $menu;
    }



    public function mainMenu(FactoryInterface $factory, array $options)

    {

        $em = $this->container->get('doctrine.orm.entity_manager');

        // $entities = $em->getRepository('BtobActiviteBundle:Activite')->findAll();

        $entities = $em->getRepository("BtobActiviteBundle:Type")->findby(array('act' => 1));



        $menu = $factory->createItem('root', array(

            'childrenAttributes' => array(

                'class' => 'clearfix',

                'id' => 'mobile-nav'

            ),

        ));

        $menu->addChild('Fichier', array('route' => 'btob_dash_homepage'));

        $menu['Fichier']->addChild('dashboard', array('route' => 'btob_dash_homepage', 'label' => 'Tableau de board'));

        $menu['Fichier']->addChild('Utilisateur', array('route' => 'show_users', 'label' => 'Gestion des Utilisateurs'));

        $menu['Fichier']->addChild('Déconnexion', array('route' => 'fos_user_security_logout', 'label' => 'Déconnexion'));



        $menu->addChild('Hôtel', array('route' => 'btob_hotel_homepage'));

        $menu['Hôtel']->addChild('gotel', array('route' => 'btob_hotel_homepage', 'label' => 'Gestion des Hôtels'));

        $menu['Hôtel']->addChild('Pays', array('route' => 'btob_pays_homepage', 'label' => 'Gestion des Pays'));

        $menu['Hôtel']->addChild('Ville', array('route' => 'btob_ville_homepage', 'label' => 'Gestion des Villes'));

        $menu['Hôtel']->addChild('localisation', array('route' => 'btob_localisation_homepage', 'label' => 'Gestion des Localisations'));

        $menu['Hôtel']->addChild('Theme', array('route' => 'btob_theme_homepage', 'label' => 'Gestion des Themes'));

        $menu['Hôtel']->addChild('options', array('route' => 'btob_options_homepage', 'label' => 'Gestion des options'));

        $menu['Hôtel']->addChild('child', array('route' => 'btob_child_homepage', 'label' => 'Gestion catégorie d\'enfant'));

        $menu['Hôtel']->addChild('amenagement', array('route' => 'btob_amenagement_homepage', 'label' => 'Gestion des Aménagements'));

        $menu['Hôtel']->addChild('amrangement', array('route' => 'btob_arangement_homepage', 'label' => 'Gestion des arrangements'));

        $menu['Hôtel']->addChild('room', array('route' => 'btob_room_homepage', 'label' => 'Gestion des chambres'));

        $menu['Hôtel']->addChild('supplement', array('route' => 'btob_supplement_homepage', 'label' => 'Gestion des suppléments'));

        $menu['Hôtel']->addChild('basecalcule', array('route' => 'btob_baseroom_homepage', 'label' => 'Base de calcule pour les chambres'));



        //



        $menu->addChild('Etat des Réservations', array('route' => 'btob_reservationback_homepage'));

        $menu['Etat des Réservations']->addChild('Réservations Hôtels', array('route' => 'btob_reservationback_homepage', 'label' => 'Réservations Hôtels'));

        //  $menu['Etat des Réservations']->addChild('Réservations Voîtres', array('route' => 'reservationvoituress', 'label' => 'Réservations Voitures'));



        $menu['Etat des Réservations']->addChild('Circuit&Excursion', array('route' => 'btob_resaback_cuircuit', 'label' => 'Circuit&Excursion'));
        $menu['Etat des Réservations']->addChild('Réservations Circuits Personalisé', array('route' => 'circuits_personalisation', 'label' => 'Réservations Circuits Personalisé'));
        $menu['Etat des Réservations']->addChild('Réservations Evenements', array('route' => 'evenements_reservation', 'label' => 'Réservations Evenements'));

        $menu['Etat des Réservations']->addChild('Omra', array('route' => 'reservationomra', 'label' => 'Réservation Omra'));

        $menu['Etat des Réservations']->addChild('Réservations Vol', array('route' => 'vol', 'label' => 'Réservations Vol'));

        $menu['Etat des Réservations']->addChild('Réservations bateau', array('route' => 'volbateau', 'label' => ' Réservation bâteau'));

        $menu['Etat des Réservations']->addChild('Réservations Assurance', array('route' => 'assurance', 'label' => ' Réservation assurance'));





        //    $menu['Etat des Réservations']->addChild('Réservations Croisière', array('route' => 'croissiere_reservation', 'label' => 'Réservations Croisière'));

        $menu['Etat des Réservations']->addChild('Réservations Séjours', array('route' => 'sejours_reservation', 'label' => 'Réservations Séjours'));

        $menu['Etat des Réservations']->addChild('Réservations Séjours Personalisé', array('route' => 'sejours_personalisation', 'label' => 'Réservations Séjours Personalisé'));

        $menu['Etat des Réservations']->addChild('Réservations Spa', array('route' => 'btob_resaback_spa', 'label' => 'Réservations Spa'));

        $menu['Etat des Réservations']->addChild('Seminaire', array('route' => 'btob_siminaire_list_reservation_homepage', 'label' => 'Réservations seminaire'));

        $menu['Etat des Réservations']->addChild('Réservations Croisière', array('route' => 'btob_resaback_croissiere', 'label' => 'Réservations Croisière'));

        $menu['Etat des Réservations']->addChild('Réservations Voiture', array('route' => 'reservationvoituress', 'label' => 'Réservations Voiture'));

        $menu['Etat des Réservations']->addChild('Réservations Jeux', array('route' => 'btob_jeux_list_homepage', 'label' => 'Réservations Jeux'));

        //$menu['Etat des Réservations']->addChild('Réservations Transfert', array('route' => 'btob_reservationtransf_homepage', 'label' => 'Réservations Transfert'));

        $menu['Etat des Réservations']->addChild('Réservations Parc', array('route' => 'btob_resaback_parc', 'label' => 'Réservations Parc'));







        $menu['Etat des Réservations']->addChild('Solde des Agences', array('route' => 'user_solde_homepage', 'label' => 'Solde des Agences'));

        $menu['Etat des Réservations']->addChild('Solde des Agences', array('route' => 'user_solde_homepage', 'label' => 'Solde des Agences'));

        $menu['Etat des Réservations']->addChild('Staitistique Agence', array('route' => 'btob_stats_homepage', 'label' => 'Staitistique'));
        //	$menu['Etat des Réservations']->addChild('Réservations séminaire', array('route' => 'btob_siminaire_list_reservation_homepage', 'label' => 'Réservations séminaire'));

        // $menu['Etat des Réservations']->addChild('Le Grand Jeu', array('route' => 'btob_jeux_list_homepage', 'label' => 'Le Grand Jeu'));





        if ($entities != null){

            $menu['Etat des Réservations']->addChild(

                'Réservations Activité' ,

                array(

                    'route' 	  => 'btob_activite_liste_reservation_homepage',

                    'label' => 'Activité',

                    'routeParameters' => array('id' =>$entities[0]->getID() ),

                ) );}

        foreach($entities as $value) {



            if($value->getType()!="Pages dynamiques")



            {

                $menu['Etat des Réservations']['Réservations Activité']->addChild(

                    'Articles-' .$value->getID(),

                    array(

                        'route' 	  => 'btob_activite_liste_reservation_homepage',

                        'label' => $value->getType(),

                        'routeParameters' => array('id' =>$value->getID() ),

                    ) );

            }

        }

        //



        $menu->addChild('Module', array('route' => 'cuircuit'));

        $menu['Module']->addChild('circuit', array('route' => 'cuircuit', 'label' => 'Circuit&Excursion'));

        $menu['Module']->addChild('Congrès et siminaires', array('route' => 'siminaire', 'label' => 'Congrès et siminaires'));

        $menu['Module']->addChild('Croisière', array('route' => 'croissiere', 'label' => 'Croisière'));

        $menu['Module']->addChild('Bien être', array('route' => 'spa', 'label' => 'Bien être'));

        $menu['Module']->addChild('Séjours', array('route' => 'sejour', 'label' => 'Séjours'));
        $menu['Module']->addChild('Evenements', array('route' => 'evenement', 'label' => 'Evenements'));

        $menu['Module']->addChild('voittures', array('route' => 'voiture', 'label' => ' Voitures'));

        $menu['Module']['voittures']->addChild('classes', array('route' => 'classe', 'label' => ' Classes Voitures'));

        $menu['Module']['voittures']->addChild('opvoitures', array('route' => 'opvoiture', 'label' => ' Options Voitures'));

        $menu['Module']->addChild('Le Grand Jeu', array('route' => 'jeu', 'label' => 'Le Grand Jeu'));

        $menu['Module']->addChild('parc', array('route' => 'parc', 'label' => 'parc'));

        $menu['Module']->addChild('Omra', array('route' => 'omra', 'label' => 'Omra'));





        $menu->addChild('Configuration', array('route' => 'show_users'));

        $menu['Configuration']->addChild('devise', array('route' => 'btob_dashdevise_homepage', 'label' => 'Gestion des devise'));
        $menu['Configuration']->addChild('mode payement', array('route' => 'btob_payement_homepage', 'label' => 'Mode de Payement'));
        $menu['Configuration']->addChild('Frais Dossier', array('route' => 'frais', 'label' => 'Frais de Dossier'));
        $menu['Configuration']->addChild('Bannaire', array('route' => 'bannaire', 'label' => 'Gestion de Bannière'));

        $menu['Configuration']->addChild('convert', array('route' => 'btob_dashdevise_convert', 'label' => 'Convertisseur de devise'));

        $menu['Configuration']->addChild('pays', array('route' => 'btob_listpays_homepage', 'label' => 'Liste des pays'));

        $menu['Configuration']->addChild('agence', array('route' => 'btob_margagence_homepage', 'label' => 'Marge par Agences & Saisons'));
        
        
        
        $menu['Configuration']->addChild('marcher', array('route' => 'btob_marcher_homepage', 'label' => 'Liste des marchés'));

		$menu['Configuration']->addChild('newsletter', array('route' => 'btob_email_homepage', 'label' => 'Gestion des newsletter'));

        /*        
       $menu->addChild('Transfert', array('route' => 'btob_transferts_homepage'));

        $menu['Transfert']->addChild('transferts', array('route' => 'btob_transferts_homepage', 'label' => 'Gestion des Transferts'));
        $menu['Transfert']->addChild('Véhicule', array('route' => 'btob_transport_homepage', 'label' => 'Gestion des Véhicules'));

        $menu['Transfert']->addChild('Pays', array('route' => 'btob_payst_homepage', 'label' => 'Gestion des Pays'));

        $menu['Transfert']->addChild('Ville', array('route' => 'btob_villet_homepage', 'label' => 'Gestion des Villes'));
        $menu->addChild('Fidelité', array('route' => 'btob_client_homepage'));

        $menu['Fidelité']->addChild('Gestion des Points', array('route' => 'point', 'label' => 'Gestion des Points'));
        $menu['Fidelité']->addChild('Transformation de Points', array('route' => 'pointp', 'label' => 'Transformation de Points'));

        $menu['Fidelité']->addChild('Liste des clients', array('route' => 'btob_client_homepage', 'label' => 'Liste des Clients'));

		*/







                
                
        //$menu->addChild('Activité', array('route' => 'type'));



        //$menu->addChild('Réservation', array('route' => 'btob_list_homepage'));

        return $menu;

    }//







}

