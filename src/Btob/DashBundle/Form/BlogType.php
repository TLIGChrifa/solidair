<?php

namespace Btob\DashBundle\Form;

//use egeloen\ckeditorBundle\Form\Type\CKEditorType;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class BlogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
           
            ->add('texte',  'ckeditor', array('label' =>'Texte', 'required' => false))
            //->add('datecreation')
            ->add('file', NULL, array('label' =>'Image', 'required' => false))
            //->add('utilisateur')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\DashBundle\Entity\Blog'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_dashbundle_blog';
    }
}
