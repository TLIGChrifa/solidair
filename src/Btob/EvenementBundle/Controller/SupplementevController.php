<?php

namespace Btob\EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\EvenementBundle\Entity\Supplementev;
use Symfony\Component\HttpFoundation\Request;
use Btob\EvenementBundle\Form\SupplementevType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class SupplementevController extends Controller {

    public function indexAction($evenementpriceid) {
        $evenementprice = $this->getDoctrine()
                ->getRepository('BtobEvenementBundle:Evenementprice')
                ->find($evenementpriceid);
        return $this->render('BtobEvenementBundle:Supplementev:index.html.twig', array('entities' => $evenementprice->getSupplementev(), "evenementpriceid" => $evenementpriceid));
    }

    public function addAction($evenementpriceid) {
        $evenementprice = $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->find($evenementpriceid);
        $Supplementev = new Supplementev();
        $form = $this->createForm(new SupplementevType(), $Supplementev);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementev->setEvenementprice($evenementprice);
                $em->persist($Supplementev);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Ajout: Nouveau supplément d'événement ");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementpriceid" => $evenementpriceid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobEvenementBundle:Supplementev:form.html.twig', array('form' => $form->createView(), "evenementpriceid" => $evenementpriceid));
    }

    public function editAction($id, $evenementpriceid) {
        $request = $this->get('request');
        $Supplementev = $this->getDoctrine()
                ->getRepository('BtobEvenementBundle:Supplementev')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementevType(), $Supplementev);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Modification: Supplément d'événement n° " . $Supplementev->getId()." - " . $Supplementev->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementpriceid" => $evenementpriceid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobEvenementBundle:Supplementev:form.html.twig', array('form' => $form->createView(), 'id' => $id, "evenementpriceid" => $evenementpriceid)
        );
    }

    public function deleteAction(Supplementev $Supplementev, $evenementpriceid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementev) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Suppression: Supplément d'événement n° " . $Supplementev->getId()." - " . $Supplementev->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Supplementev);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementev_homepage', array("evenementpriceid" => $evenementpriceid)));
    }

}
