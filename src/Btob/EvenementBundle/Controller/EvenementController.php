<?php



namespace Btob\EvenementBundle\Controller;



use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



use Btob\HotelBundle\Common\Tools;

use Btob\EvenementBundle\Entity\Evenement;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

use Btob\EvenementBundle\Entity\Imgeev;

use Btob\EvenementBundle\Form\EvenementType;



/**

 * Evenement controller.

 *

 */

class EvenementController extends Controller

{



    /**

     * Lists all Evenement entities.

     *

     */



    public function indexAction()

    {
        $entities = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenement")->findAll();
        return $this->render('BtobEvenementBundle:Evenement:index.html.twig', array('entities' => $entities));
    }



    /**

     * Creates a new Evenement entity.

     *

     */

    public function createAction(Request $request)

    {

        $entity = new Evenement();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $request = $this->get('request');
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Ajout: Nouvelle événement");
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            if (is_array($request->request->get("files")))
                foreach ($request->request->get("files") as $key => $value) {
                    if ($value != "") {
                        $img = new Imgeev();
                        $img->setEvenement($entity);
                        $img->setImage($value);
                        $em->persist($img);
                        $em->flush();
                    }

                }
        
            return $this->redirect($this->generateUrl('evenement'));

        }



        return $this->render('BtobEvenementBundle:Evenement:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Creates a form to create a Evenement entity.

     *

     * @param Evenement $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createCreateForm(Evenement $entity)

    {

        $form = $this->createForm(new EvenementType(), $entity, array(

            'action' => $this->generateUrl('evenement_create'),

            'method' => 'POST',

        ));






        return $form;

    }



    /**

     * Displays a form to create a new Evenement entity.

     *

     */

    public function newAction()

    {

        $entity = new Evenement();

        $form = $this->createCreateForm($entity);



        return $this->render('BtobEvenementBundle:Evenement:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }



    /**

     * Finds and displays a Evenement entity.

     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobEvenementBundle:Evenement')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Evenement entity.');

        }



        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobEvenementBundle:Evenement:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Displays a form to edit an existing Evenement entity.

     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobEvenementBundle:Evenement')->find($id);



        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Evenement entity.');

        }



        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);



        return $this->render('BtobEvenementBundle:Evenement:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Creates a form to edit a Evenement entity.

     *

     * @param Evenement $entity The entity

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createEditForm(Evenement $entity)

    {

        $form = $this->createForm(new EvenementType(), $entity, array(

            'action' => $this->generateUrl('evenement_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));






        return $form;

    }



    /**

     * Edits an existing Evenement entity.

     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobEvenementBundle:Evenement')->find($id);

        foreach ($entity->getImgeev() as $key => $value) {

            $em->remove($value);

            $em->flush();

        }

        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Evenement entity.');

        }



        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);



        if ($editForm->isValid()) {

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Modification: Evénement " . $entity->getId()." - " . $entity->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            if (is_array($request->request->get("files")))

                foreach ($request->request->get("files") as $key => $value) {

                    if ($value != "") {

                        $img = new Imgeev();

                        $img->setEvenement($entity);

                        $img->setImage($value);



                        $em->persist($img);

                        $em->flush();



                    }

                }

            return $this->redirect($this->generateUrl('evenement'));

        }



        return $this->render('BtobEvenementBundle:Evenement:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }



    /**

     * Deletes a Evenement entity.

     *

     */

    public function deleteAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobEvenementBundle:Evenement')->find($id);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Suppression: Evénement " . $entity->getId()." - " . $entity->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('evenement'));

    }



    /**

     * Creates a form to delete a Evenement entity by id.

     *

     * @param mixed $id The entity id

     *

     * @return \Symfony\Component\Form\Form The form

     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()

            ->setAction($this->generateUrl('evenement_delete', array('id' => $id)))

            ->setMethod('DELETE')

            ->add('submit', 'submit', array('label' => 'Delete'))

            ->getForm();

    }

}

