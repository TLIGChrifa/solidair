<?php
namespace Btob\EvenementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use User\UserBundle\Entity\Solde;
use Btob\EvenementBundle\Entity\Evenement;
use Btob\EvenementBundle\Entity\Reservationevenement;
use Btob\EvenementBundle\Entity\Imgeev;
use Btob\HotelBundle\Entity\Clients;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
use Btob\HotelBundle\Form\ClientsType;
use Symfony\Component\HttpFoundation\Request;
use Btob\EvenementBundle\Entity\Reservationevdetail;

class DefaultController extends Controller

{

    public function indexAction(Request $request)

    {

         $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->listPrice();

        $entities = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenement")->findby(array('act' => 1));
          $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $evenementprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );   
         $user = $this->get('security.context')->getToken()->getUser();

        return $this->render('BtobEvenementBundle:Default:index.html.twig', array('entities' => $entities,'evenementprices' => $evenementprices,'user' => $user));

    }



    public function detailAction(Evenement $Evenement)

    {
       $evenementperiode = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($Evenement->getId());

        return $this->render('BtobEvenementBundle:Default:detail.html.twig', array('entry' => $Evenement,'periods' => $evenementperiode));

    }

    
    
    public function personalisationAction(Evenement $evenement)

    {

       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->findByEvenement($evenement->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $evenementprices = $request->request->get("evenementprice");
            $session->set('evenementprice', $evenementprices);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('btob_inscrip_reservation_evenement', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('BtobEvenementBundle:Default:personalisation.html.twig', array('entry' => $evenement,'evenementprices' => $evenementprices));

    
    }
    
    
    public function inscriptionAction(Evenement $evenement)

    {

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $evenementprice = $session->get("evenementprice");
       $em = $this->getDoctrine()->getManager();
       
       $evenementprices = $this->getDoctrine()->getRepository("BtobEvenementBundle:Evenementprice")->find($evenementprice);
      
       $evenementsupp = $this->getDoctrine()->getRepository("BtobEvenementBundle:Supplementev")->findSupplementByEvenementprice($evenementprices->getId());
       
       

        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $Array = array();
        
         $aujourdhuib = new \DateTime();
       $nbjourbebe = intval($evenement->getAgebmax())*365;
       $aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");

        
        $aujourdhuienf = new \DateTime();
       $nbjourenf = intval($evenement->getAgeenfmax())*365;
       $aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('evenementprice', $evenementprice);
            
          

          return $this->redirect($this->generateUrl('btob_evenement_evenement_reservation_homepage', array('id'=>$evenement->getId())));

            
        }
       
       
       
       
    return $this->render('BtobEvenementBundle:Default:inscription.html.twig', array('datedb' => $datedb,'datedenf' => $datedenf,'entry' => $evenement,'evenementsupp' => $evenementsupp,'evenementprices' => $evenementprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));

    
    }
    
    
    
    public function detailreservationAction(Reservationevenement $Reservationevenement)

    {

        return $this->render('BtobEvenementBundle:Default:show.html.twig', array('entry' => $Reservationevenement));

    }
    
    
    
    public function voucherreservationAction(Reservationevenement $Reservationevenement)

    {
		

		$pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('BtobEvenementBundle:Default:voucher.html.twig', array('entry' => $Reservationevenement));

        $pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/header-vocher-explore-01.png', 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/footer-vocher-explore-01.png', 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;


    }

    public function deletereservationAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobEvenementBundle:Reservationevenement')->find($id);

        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('evenements_reservation'));

    }

    public function listreservationevenementAction()

    {



        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();

        $dt->modify('-2 month');

        $dcrfrom = $dt->format("d/m/Y");

        $dt->modify('+4 month');

        $dcrto = $dt->format("d/m/Y");

        $etat = "";

        $client = "";

        $numres = "";

        $dt = new \DateTime();

        $dt->modify('-2 month');

        $datedfrom = $dt->format("d/m/Y");

        $dt->modify('+4 month');

        $datefto = $dt->format("d/m/Y");

        $agence = "";

        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $dcrto = $request->request->get('dcrto');

            $dcrfrom = $request->request->get('dcrfrom');

            $etat = $request->request->get('etat');

            $client = trim($request->request->get('client'));

            $numres = trim($request->request->get('numres'));



            $agence = $request->request->get('agence');

        }

        $em = $this->getDoctrine()->getManager();

        // reset notification

        $user = $this->get('security.context')->getToken()->getUser();

        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));



        foreach ($notification as $value) {

            $value->setNotif(0);

            $em->persist($value);

            $em->flush();

        }

        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')

        {

            $entities = $em->getRepository('BtobEvenementBundle:Reservationevenement')->findAll();



        }else{

            $entities = $em->getRepository('BtobEvenementBundle:Reservationevenement')->findBy(array('user' => $user));



        }
        
               if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getUser()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {

            $tabsearch = array();

            foreach ($entities as $value) {

                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

        if ($dcrfrom != "") {

            $tabsearch = array();

            $tab = explode('/', $dcrfrom);

            $dtx = $tab[2] . $tab[1] . $tab[0];

            foreach ($entities as $value) {

                $dty = $value->getDcr()->format('Ymd');

                if (($dty - $dtx) >= 0) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

        if ($dcrto != "") {

            $tabsearch = array();

            $tab = explode('/', $dcrto);

            $dtx = $tab[2] . $tab[1] . $tab[0];

            foreach ($entities as $value) {

                $dty = $value->getDcr()->format('Ymd');

                if (($dtx - $dty) >= 0) {

                    $tabsearch[] = $value;

                }

            }

            $entities = $tabsearch;

        }

       

       


        return $this->render('BtobEvenementBundle:Default:listreservation.html.twig', array(

            'entities' => $entities,

            'dcrto' => $dcrto,

            'dcrfrom' => $dcrfrom,

            'etat' => $etat,

            'client' => $client,

            'numres' => $numres,

            'datefto' => $datefto,

            'datedfrom' => $datedfrom,

            'agence' => $agence,

            'users' => $users,

        ));





    }








    
  public function reservationevenementAction(Evenement $evenement)

    {

       $em = $this->getDoctrine()->getManager();
       
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $evenementperiode = $session->get("evenementprice");
        
        
       $priceev =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Evenementprice')->find($evenementperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
          
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
             
                 
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              
              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         $ArrBase[$key]['price'] = $prices+($prices*$evenement->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
           
       }
        
      
        $evenementprice = $session->get("evenementprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');


            $email = $post["email"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);

            $User = $this->get('security.context')->getToken()->getUser();

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                
                $resa=new Reservationevenement();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setEvenement($evenement);
                $resa->setEvenementprice($priceev);
                
                $resa->setTotal($total);
                  
                $em->persist($resa);
                $resa->setEtat(1);
                $resa->setAvance($total*($evenement->getPrixavance()));
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Evénement");
                        $hist->setMessage("Ajout: Réservation événement n° " . $resa->getEvenement()->getId()." - " . $resa->getEvenement()->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
       
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
           
           $resadetail=new Reservationevdetail();
           $resadetail->setReservationevenement($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                 
                  
                
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               
           $resadetails=new Reservationevdetail();
           $resadetails->setReservationevenement($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPriceenf(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
            
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               
               $resadetailb=new Reservationevdetail();
           $resadetailb->setReservationevenement($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobEvenementBundle:Supplementev')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPriceb();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
             
                 
                  
                  
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobEvenementBundle:Evenementprice')
                        ->calculev($priceev,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         $ArrBase[$key]['price'] =$prices+($prices*$evenement->getMarge()/100);
          $total+= $ArrBase[$key]['price']; 
       }    
            } else {
                echo $form->getErrors();
            }
                $request->getSession()->getFlashBag()->add('notievb', 'Votre réservation a bien été envoyé. Merci.');

                return $this->redirect($this->generateUrl('btob_evenement_evenement_homepage'));



        }

        return $this->render('BtobEvenementBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$evenement,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));

    }
   
   
   

    public function actiAction(Reservationevenement $resaevenement, $action)
    {
        $em = $this->getDoctrine()->getManager();
        $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
        $factures = array();
        $dr = new \DateTime();
        if (count($facts) > 0) {
            foreach ($facts as $kef) {
                if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                    array_push($factures, $kef);
                }
            }
        }

        if (count($factures) > 0) {
            $num = $factures[0]->getNum() + 1;
        } else {
            $num = 1;
        }

        if ($action == 2) {
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setCode("FO");
            $ffact->setEtat(1);
            $ffact->setClient($resaevenement->getClient()->getName() . ' ' . $resaevenement->getClient()->getPname());
            $ffact->setClientId($resaevenement->getClient());
            $ffact->setDcr($dr);
            $ffact->setDate(new \DateTime());
            $ffact->setIdm($resaevenement->getId());
            $ffact->setType("Evenement");
            $ffact->setMontant($resaevenement->getTotal());
            $em->persist($ffact);
            $em->flush();

            $tfact = new Tfacture();
            $tfact->setFacture($ffact);
            $tfact->setPrixUnitaire($resaevenement->getTotal());
            $tfact->setQuantite(1);
            $tfact->setRemise(0);
            $tfact->setTva(20);
           // $tfact->setMontant($resaevenement->getTotal());
           $tfact->setType("Evenement:".$resaevenement->getEvenement()->getTitre());
            $em->persist($tfact);
            $em->flush();


            $resaevenement->setEtat(2);
            $em->persist($resaevenement);
            $em->flush();
			
			$user = $this->get('security.context')->getToken()->getUser();			
			$solde = new Solde();
            $solde->setObservation("Paiement de la commande Evenement " . Tools::RefResa("", $resaevenement->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resaevenement->getUser());
            $solde->setSolde(($resaevenement->getTotal() * (-1)));
            $em->persist($solde);
			$em->flush();
            $newsolde = $resaevenement->getUser()->getSold() - $resaevenement->getTotal();
            $currenuser = $resaevenement->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();
            return $this->redirect($this->generateUrl('btob_evenement_detail_evenement_reservation', array('id' => $resaevenement->getId())));

        }
        if ($action == 0) {
		
		   if($resaevenement->getEtat()==2)
		   {
               $ffact = new Facture();
               $ffact->setNum($num);
               $ffact->setCode("FO");
               $ffact->setEtat(1);
               $ffact->setClient($resaevenement->getClient()->getName() . ' ' . $resaevenement->getClient()->getPname());
               $ffact->setClientId($resaevenement->getClient());
               $ffact->setDcr($dr);
               $ffact->setDate(new \DateTime());
               $ffact->setIdm($resaevenement->getId());
               $ffact->setType("Evenement");
               $ffact->setMontant((-1)*$resaevenement->getTotal());
               $em->persist($ffact);
               $em->flush();

               $tfact = new Tfacture();
               $tfact->setFacture($ffact);
            $tfact->setPrixUnitaire((-1)*$resaevenement->getTotal());
            $tfact->setQuantite(1);
            $tfact->setRemise(0);
            $tfact->setTva(20);
             //  $tfact->setMontant(-1*$resaevenement->getTotal());
               $tfact->setType("Evenement:".$resaevenement->getEvenement()->getTitre());
               $em->persist($tfact);
               $em->flush();

		   $user = $this->get('security.context')->getToken()->getUser();			
			$solde = new Solde();
            $solde->setObservation("Annuler de la commande Evenement " . Tools::RefResa("", $resaevenement->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resaevenement->getUser());
            $solde->setSolde($resaevenement->getTotal());
            $em->persist($solde);
			$em->flush();
            $newsolde = $resaevenement->getUser()->getSold() + $resaevenement->getTotal();
            $currenuser = $resaevenement->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();
		   }
		   
            $resaevenement->setEtat(0);
            $em->persist($resaevenement);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_evenement_detail_evenement_reservation', array('id' => $resaevenement->getId())));

        }
    }
   
}

