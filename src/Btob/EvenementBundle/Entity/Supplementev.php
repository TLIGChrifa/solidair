<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplementev
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\SupplementevRepository")
 */
class Supplementev
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Evenementprice", inversedBy="supplementev")
     * @ORM\JoinColumn(name="evenementprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenementprice;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Supplementev
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Supplementev
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set evenementprice
     *
     * @param \Btob\EvenementBundle\Entity\Evenementprice $evenementprice
     * @return Supplementev
     */
    public function setEvenementprice(\Btob\EvenementBundle\Entity\Evenementprice $evenementprice = null)
    {
        $this->evenementprice = $evenementprice;

        return $this;
    }

    /**
     * Get evenementprice
     *
     * @return \Btob\EvenementBundle\Entity\Evenementprice 
     */
    public function getEvenementprice()
    {
        return $this->evenementprice;
    }
    
 
}
