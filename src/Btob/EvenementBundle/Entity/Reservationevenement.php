<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationevenement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\ReservationevenementRepository")
 */
class Reservationevenement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;


        /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;
    
    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float")
     */
    private $avance;

    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="reservationevenement")
     * @ORM\JoinColumn(name="evenement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenement;
    
    /**
     * @ORM\ManyToOne(targetEntity="Evenementprice", inversedBy="reservationevenement")
     * @ORM\JoinColumn(name="evenementprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenementprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationevenement")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationevenement")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

   
    /**
     * @ORM\OneToMany(targetEntity="Reservationevdetail", mappedBy="reservationevenement", cascade={"remove"})
     */
    protected $reservationevdetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationevenement
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationevenement
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set evenementprice
     *
     * @param \Btob\EvenementBundle\Entity\Evenementprice $evenementprice
     * @return Reservationcircuit
     */
    public function setEvenementprice(\Btob\EvenementBundle\Entity\Evenementprice $evenementprice = null)
    {
        $this->evenementprice = $evenementprice;

        return $this;
    }

    /**
     * Get evenementprice
     *
     * @return \Btob\EvenementBundle\Entity\Evenementprice 
     */
    public function getEvenementprice()
    {
        return $this->evenementprice;
    }
    
       
    
    /**
     * Set evenement
     *
     * @param \Btob\EvenementBundle\Entity\Evenement $evenement
     * @return Reservationevenement
     */
    public function setEvenement(\Btob\EvenementBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \Btob\EvenementBundle\Entity\Evenement 
     */
    public function getEvenement()
    {
        return $this->evenement;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationevenement
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Reservationevenement
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

   


    /**
     * Add reservationevdetail
     *
     * @param \Btob\EvenementBundle\Entity\Reservationevdetail $reservationevdetail
     * @return Reservationevenement
     */
    public function addReservationevdetail(\Btob\EvenementBundle\Entity\Reservationevdetail $reservationevdetail)
    {
        $this->reservationevdetail[] = $reservationevdetail;

        return $this;
    }

    /**
     * Remove reservationevdetail
     *
     * @param \Btob\EvenementBundle\Entity\Reservationevdetail $reservationevdetail
     */
    public function removeReservationevdetail(\Btob\EvenementBundle\Entity\Reservationevdetail $reservationevdetail)
    {
        $this->reservationevdetail->removeElement($reservationevdetail);
    }

    /**
     * Get reservationevdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationevdetail()
    {
        return $this->reservationevdetail;
    }


        /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }


        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }



}
