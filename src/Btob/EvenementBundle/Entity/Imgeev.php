<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgeev
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\ImgeevRepository")
 */
class Imgeev
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255 , nullable=true)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="imgeev")
     * @ORM\JoinColumn(name="evenement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date" , nullable=true)
     */
    private $dcr;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public  function  __construct(){
        $this->dcr = new \DateTime();

    }


    /**
     * Set image
     *
     * @param string $image
     * @return Imgeev
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgeev
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set evenement
     *
     * @param \Btob\EvenementBundle\Entity\Evenement $evenement
     * @return Imgeev
     */
    public function setEvenement(\Btob\EvenementBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \Btob\EvenementBundle\Entity\Evenement 
     */
    public function getEvenement()
    {
        return $this->evenement;
    }
}
