<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationevdetail
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\ReservationevdetailRepository")
 */
class Reservationevdetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="chambre", type="text")
     */
    protected $chambre;
    
    /**
     * @ORM\ManyToOne(targetEntity="Reservationevenement", inversedBy="Reservationevdetail")
     * @ORM\JoinColumn(name="reservationevenement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationevenement;
    
    
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="namead", type="string",length=50 ,nullable=true)
     */
    private $namead;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomad", type="string",length=50 ,nullable=true)
     */
    private $prenomad;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="agead", type="string",length=50 ,nullable=true)
     */
    private $agead;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppad", type="text" ,nullable=true)
     */
    private $suppad;
    
    

    
    /**
     * @var string
     *
     * @ORM\Column(name="nameenf", type="string" ,length=50 ,nullable=true)
     */
    private $nameenf;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomenf", type="string" ,length=50 ,nullable=true)
     */
    private $prenomenf;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenf", type="string" ,length=50 ,nullable=true)
     */
    private $ageenf;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppenf", type="text" ,nullable=true)
     */
    private $suppenf;
    
    

    
    /**
     * @var string
     *
     * @ORM\Column(name="nameb", type="string" ,length=50 ,nullable=true)
     */
    private $nameb;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomb", type="string" ,length=50 ,nullable=true)
     */
    private $prenomb;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageb", type="string" ,length=50 ,nullable=true)
     */
    private $ageb;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppb", type="text", nullable=true)
     */
    private $suppb;
    


    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set chambre
     *
     * @param string $chambre
     * @return Reservationevdetail
     */
    public function setChambre($chambre)
    {
        $this->chambre = $chambre;

        return $this;
    }

    /**
     * Get chambre
     *
     * @return string 
     */
    public function getChambre()
    {
        return $this->chambre;
    }



    /**
     * Set reservationevenement
     *
     * @param \Btob\EvenementBundle\Entity\Reservationevenement $reservationevenement
     * @return Reservationcdetail
     */
    public function setReservationevenement(\Btob\EvenementBundle\Entity\Reservationevenement $reservationevenement = null)
    {
        $this->reservationevenement = $reservationevenement;

        return $this;
    }

    /**
     * Get reservationevenement
     *
     * @return \Btob\EvenementBundle\Entity\Reservationevenement
     */
    public function getReservationevenement()
    {
        return $this->reservationevenement;
    }
    
    
    
    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservationevdetail
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string 
     */
    public function getNamead()
    {
        return $this->namead;
    }
    
    
    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservationevdetail
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string 
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }    
    
    
    /**
     * Set agead
     *
     * @param string $agead
     * @return Reservationevdetail
     */
    public function setAgead($agead)
    {
        $this->agead = $agead;

        return $this;
    }

    /**
     * Get agead
     *
     * @return string 
     */
    public function getAgead()
    {
        return $this->agead;
    }
    

    /**
     * Set suppad
     *
     * @param string $suppad
     * @return Reservationevdetail
     */
    public function setSuppad($suppad)
    {
        $this->suppad = $suppad;

        return $this;
    }

    /**
     * Get suppad
     *
     * @return string 
     */
    public function getSuppad()
    {
        return $this->suppad;
    }    
    
 
    
    /**
     * Set nameenf
     *
     * @param string $nameenf
     * @return Reservationevdetail
     */
    public function setNameenf($nameenf)
    {
        $this->nameenf = $nameenf;

        return $this;
    }

    /**
     * Get nameenf
     *
     * @return string 
     */
    public function getNameenf()
    {
        return $this->nameenf;
    }
    
    
    /**
     * Set prenomenf
     *
     * @param string $prenomenf
     * @return Reservationevdetail
     */
    public function setPrenomenf($prenomenf)
    {
        $this->prenomenf = $prenomenf;

        return $this;
    }

    /**
     * Get prenomenf
     *
     * @return string 
     */
    public function getPrenomenf()
    {
        return $this->prenomenf;
    }  
    
    
    /**
     * Set ageenf
     *
     * @param string $ageenf
     * @return Reservationevdetail
     */
    public function setAgeenf($ageenf)
    {
        $this->ageenf = $ageenf;

        return $this;
    }

    /**
     * Get ageenf
     *
     * @return string 
     */
    public function getAgeenf()
    {
        return $this->ageenf;
    }    
    

    /**
     * Set suppenf
     *
     * @param string $suppenf
     * @return Reservationevdetail
     */
    public function setSuppenf($suppenf)
    {
        $this->suppenf = $suppenf;

        return $this;
    }

    /**
     * Get suppenf
     *
     * @return string 
     */
    public function getSuppenf()
    {
        return $this->suppenf;
    }        
    


    
    
    /**
     * Set nameb
     *
     * @param string $nameb
     * @return Reservationevdetail
     */
    public function setNameb($nameb)
    {
        $this->nameb = $nameb;

        return $this;
    }

    /**
     * Get nameb
     *
     * @return string 
     */
    public function getNameb()
    {
        return $this->nameb;
    } 
    
    /**
     * Set prenomb
     *
     * @param string $prenomb
     * @return Reservationevdetail
     */
    public function setPrenomb($prenomb)
    {
        $this->prenomb = $prenomb;

        return $this;
    }

    /**
     * Get prenomb
     *
     * @return string 
     */
    public function getPrenomb()
    {
        return $this->prenomb;
    }            
    
    /**
     * Set ageb
     *
     * @param string $ageb
     * @return Reservationevdetail
     */
    public function setAgeb($ageb)
    {
        $this->ageb = $ageb;

        return $this;
    }

    /**
     * Get ageb
     *
     * @return string 
     */
    public function getAgeb()
    {
        return $this->ageb;
    }
    
    /**
     * Set suppb
     *
     * @param string $suppb
     * @return Reservationevdetail
     */
    public function setSuppb($suppb)
    {
        $this->suppb = $suppb;

        return $this;
    }

    /**
     * Get suppb
     *
     * @return string 
     */
    public function getSuppb()
    {
        return $this->suppb;
    }            
    
      
    
}
