<?php

namespace Btob\EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evenementprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\EvenementpriceRepository")
 */
class Evenementprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */

    private $titre;    
    
    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

      /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;

      /**
     * @var float
     *
     * @ORM\Column(name="priceenf11", type="float" , nullable=true)
     */
    private $priceenf11;
    
      /**
     * @var float
     *
     * @ORM\Column(name="priceenf12", type="float" , nullable=true)
     */
    private $priceenf12;
     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var integer
     *
     * @ORM\Column(name="capacite", type="integer" , nullable=true)
     */
    private $capacite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Evenement", inversedBy="supplementev")
     * @ORM\JoinColumn(name="evenement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $evenement;

    

     /**
     * @ORM\OneToMany(targetEntity="Supplementev", mappedBy="evenementprice", cascade={"remove"})
     */
    protected $supplementev;    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
        $this->supplementev = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Evenementprice
     */

    public function setTitre($titre)
    {

        $this->titre = $titre;
        return $this;

    }



    /**
     * Get titre
     *
     * @return string 
     */

    public function getTitre()
    {
        return $this->titre;
    }    
    
    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Evenementprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

     /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Evenementprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
     /**
     * Set priceenf12
     *
     * @param float $priceenf12
     * @return Evenementprice
     */
    public function setPriceenf12($priceenf12)
    {
        $this->priceenf12 = $priceenf12;

        return $this;
    }

    /**
     * Get priceenf12
     *
     * @return float 
     */
    public function getPriceenf12()
    {
        return $this->priceenf12;
    }  
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Evenementprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
     /**
     * Set priceenf11
     *
     * @param float $priceenf11
     * @return Evenementprice
     */
    public function setPriceenf11($priceenf11)
    {
        $this->priceenf11 = $priceenf11;

        return $this;
    }

    /**
     * Get priceenf11
     *
     * @return float 
     */
    public function getPriceenf11()
    {
        return $this->priceenf11;
    }       
    
    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Evenementprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Evenementprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set evenement
     *
     * @param \Btob\EvenementBundle\Entity\Evenement $evenement
     * @return Evenementprice
     */
    public function setEvenement(\Btob\EvenementBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \Btob\EvenementBundle\Entity\Evenement 
     */
    public function getEvenement()
    {
        return $this->evenement;
    }
    /**
     * Set capacite
     *
     * @param integer $capacite
     * @return Evenementprice
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer 
     */
    public function getCapacite()
    {
        return $this->capacite;
    }
     /**
     * Add supplementev
     *
     * @param \Btob\EvenementBundle\Entity\Supplementev $supplementev
     * @return Evenementprice
     */
    public function addSupplementev(\Btob\EvenementBundle\Entity\Supplementev $supplementev)
    {
        $this->supplementev[] = $supplementev;

        return $this;
    }

    /**
     * Remove supplementev
     *
     * @param \Btob\EvenementBundle\Entity\Supplementev $supplementev
     */
    public function removeSupplementev(\Btob\EvenementBundle\Entity\Supplementev $supplementev)
    {
        $this->supplementev->removeElement($supplementev);
    }

    /**
     * Get supplementev
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplementev()
    {
        return $this->supplementev;
    }

}
