<?php



namespace Btob\EvenementBundle\Entity;



use Doctrine\ORM\Mapping as ORM;



/**

 * Evenement

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Btob\EvenementBundle\Entity\EvenementRepository")

 */

class Evenement

{

    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;
    
    /**

     * @var string

     *

     * @ORM\Column(name="titre", type="string", length=255, nullable=true)

     */

    private $titre;

    /**

     * @var \DateTime

     *

     * @ORM\Column(name="dcr", type="date", nullable=true)

     */

    private $dcr;

    /**

     * @var boolean

     *

     * @ORM\Column(name="active", type="boolean" , nullable=true)

     */

    private $act;



    /**

     * @var float

     *

     * @ORM\Column(name="prix", type="float" , nullable=true)

     */

    private $prix;

    /**

     * @ORM\OneToMany(targetEntity="Imgeev", mappedBy="evenement")

     */

    protected $imgeev;




    /**

     * @var boolean

     *

     * @ORM\Column(name="pindex", type="boolean" , nullable=true)

     */

    private $pindex;

    /**

     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Themes", inversedBy="evenement")

     * @ORM\JoinColumn(name="themes_id", referencedColumnName="id",onDelete="CASCADE")

     */

    protected $themes;



    /**

     * @var string

     *

     * @ORM\Column(name="villed", type="string", length=255 , nullable=true)

     */

    private $villed;



    /**

     * @var string

     *

     * @ORM\Column(name="voyagecart", type="string", length=255 , nullable=true)

     */

    private $voyagecart;


    /**

     * @var string

     *

     * @ORM\Column(name="hotel", type="string", length=255 , nullable=true)

     */

    private $hotel;



    /**

     * @var integer

     *

     * @ORM\Column(name="nbretoile", type="integer" , nullable=true)

     */

    private $nbretoile;





    /**

     * @var string

     *

     * @ORM\Column(name="description", type="text" , nullable=true)

     */

    private $description;

    /**

     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="evenement")

     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")

     */

    protected $arrangement;

        
    
        /**
     * @ORM\OneToMany(targetEntity="Evenementprice", mappedBy="evenement", cascade={"remove"})
     */
    protected $evenementprice;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="prixavance", type="float" , nullable=true)
     */




    private $prixavance;       
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */

    private $ageenfmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */

    private $ageenfmax;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)
     */

    private $agebmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)
     */

    private $agebmax;    

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */

    private $marge; 
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="maps", type="string" , nullable=true)
     */

    private $maps; 
 
    
    
    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }




    public  function  __construct(){

        $this->dcr = new \DateTime();
        $this->evenementprice = new \Doctrine\Common\Collections\ArrayCollection();


    }



    /**

     * Set titre

     *

     * @param string $titre

     * @return Evenement

     */

    public function setTitre($titre)

    {

        $this->titre = $titre;



        return $this;

    }



    /**

     * Get titre

     *

     * @return string 

     */

    public function getTitre()

    {

        return $this->titre;

    }


    /**

     * Set prix

     *

     * @param float $prix

     * @return Evenement

     */

    public function setPrix($prix)

    {

        $this->prix = $prix;



        return $this;

    }



    /**

     * Get prix

     *

     * @return float 

     */

    public function getPrix()

    {

        return $this->prix;

    }





    /**

     * Set pindex

     *

     * @param boolean $pindex

     * @return Evenement

     */

    public function setPindex($pindex)

    {

        $this->pindex = $pindex;



        return $this;

    }



    /**

     * Get pindex

     *

     * @return boolean 

     */

    public function getPindex()

    {

        return $this->pindex;

    }



    /**

     * Set villed

     *

     * @param string $villed

     * @return Evenement

     */

    public function setVilled($villed)

    {

        $this->villed = $villed;



        return $this;

    }



    /**

     * Get villed

     *

     * @return string 

     */

    public function getVilled()

    {

        return $this->villed;

    }



    /**

     * Set voyagecart

     *

     * @param string $voyagecart

     * @return Evenement

     */

    public function setVoyagecart($voyagecart)

    {

        $this->voyagecart = $voyagecart;



        return $this;

    }



    /**

     * Get voyagecart

     *

     * @return string 

     */

    public function getVoyagecart()

    {

        return $this->voyagecart;

    }



    /**

     * Set hotel

     *

     * @param string $hotel

     * @return Evenement

     */

    public function setHotel($hotel)

    {

        $this->hotel = $hotel;



        return $this;

    }



    /**

     * Get hotel

     *

     * @return string 

     */

    public function getHotel()

    {

        return $this->hotel;

    }



    /**

     * Set nbretoile

     *

     * @param integer $nbretoile

     * @return Evenement

     */

    public function setNbretoile($nbretoile)

    {

        $this->nbretoile = $nbretoile;



        return $this;

    }



    /**

     * Get nbretoile

     *

     * @return integer 

     */

    public function getNbretoile()

    {

        return $this->nbretoile;

    }





    /**

     * Set description

     *

     * @param string $description

     * @return Evenement

     */

    public function setDescription($description)

    {

        $this->description = $description;



        return $this;

    }



    /**

     * Get description

     *

     * @return string 

     */

    public function getDescription()

    {

        return $this->description;

    }



    /**

     * Set dcr

     *

     * @param \DateTime $dcr

     * @return Evenement

     */

    public function setDcr($dcr)

    {

        $this->dcr = $dcr;



        return $this;

    }



    /**

     * Get dcr

     *

     * @return \DateTime 

     */

    public function getDcr()

    {

        return $this->dcr;

    }



    /**

     * Set act

     *

     * @param boolean $act

     * @return Evenement

     */

    public function setAct($act)

    {

        $this->act = $act;



        return $this;

    }



    /**

     * Get act

     *

     * @return boolean 

     */

    public function getAct()

    {

        return $this->act;

    }







    /**

     * Add imgeev

     *

     * @param \Btob\EvenementBundle\Entity\Imgeev $imgeev

     * @return Evenement

     */

    public function addImgeev(\Btob\EvenementBundle\Entity\Imgeev $imgeev)

    {

        $this->imgeev[] = $imgeev;



        return $this;

    }



    /**

     * Remove imgeev

     *

     * @param \Btob\EvenementBundle\Entity\Imgeev $imgeev

     */

    public function removeImgeev(\Btob\EvenementBundle\Entity\Imgeev $imgeev)

    {

        $this->imgeev->removeElement($imgeev);

    }



    /**

     * Get imgeev

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getImgeev()

    {

        return $this->imgeev;

    }



    /**

     * Set themes

     *

     * @param \Btob\HotelBundle\Entity\Themes $themes

     * @return Evenement

     */

    public function setThemes(\Btob\HotelBundle\Entity\Themes $themes = null)

    {

        $this->themes = $themes;



        return $this;

    }



    /**

     * Get themes

     *

     * @return \Btob\HotelBundle\Entity\Themes 

     */

    public function getThemes()

    {

        return $this->themes;

    }



    /**

     * Set arrangement

     *

     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement

     * @return Evenement

     */

    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)

    {

        $this->arrangement = $arrangement;



        return $this;

    }



    /**

     * Get arrangement

     *

     * @return \Btob\HotelBundle\Entity\Arrangement 

     */

    public function getArrangement()

    {

        return $this->arrangement;

    }
    
      
    
     /**
     * Add evenementprice
     *
     * @param \Btob\EvenementBundle\Entity\Evenementprice $evenementprice
     * @return Evenement
     */
    public function addEvenementprice(\Btob\EvenementBundle\Entity\Evenementprice $evenementprice)
    {
        $this->evenementprice[] = $evenementprice;

        return $this;
    }

    /**
     * Remove evenementprice
     *
     * @param \Btob\EvenementBundle\Entity\Evenementprice $evenementprice
     */
    public function removeEvenementprice(\Btob\EvenementBundle\Entity\Evenementprice $evenementprice)
    {
        $this->evenementprice->removeElement($evenementprice);
    }

    /**
     * Get Evenementprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvenementprice()
    {
        return $this->evenementprice;
    }   
    
    /**
     * Set prixavance
     *
     * @param float $prixavance
     * @return Evenement
     */

    public function setPrixavance($prixavance)
    {
        $this->prixavance = $prixavance;
        return $this;
    }

    /**
     * Get prixavance
     *
     * @return float 
     */

    public function getPrixavance()
    {
        return $this->prixavance;

    }    
    
    
 
      /**
     * Set marge
     *
     * @param float $marge
     * @return Evenement
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        

    
     /**
     * Set ageenfmin
     *
     * @param string $ageenfmin
     * @return Evenement
     */

    public function setAgeenfmin($ageenfmin)

    {
        $this->ageenfmin = $ageenfmin;
        return $this;

    }



    /**
     * Get ageenfmin
     *
     * @return string 
     */

    public function getAgeenfmin()

    {
        return $this->ageenfmin;

    }
    
    
     /**
     * Set ageenfmax
     *
     * @param string $ageenfmax
     * @return Evenement
     */

    public function setAgeenfmax($ageenfmax)

    {
        $this->ageenfmax = $ageenfmax;
        return $this;

    }



    /**
     * Get ageenfmax
     *
     * @return string 
     */

    public function getAgeenfmax()

    {
        return $this->ageenfmax;

    }
    
    
    
    
    
    
     /**
     * Set agebmin
     *
     * @param string $agebmin
     * @return Evenement
     */

    public function setAgebmin($agebmin)

    {
        $this->agebmin = $agebmin;
        return $this;

    }



    /**
     * Get agebmin
     *
     * @return string 
     */

    public function getAgebmin()

    {
        return $this->agebmin;

    }
    
    
     /**
     * Set agebmax
     *
     * @param string $agebmax
     * @return Evenement
     */

    public function setAgebmax($agebmax)

    {
        $this->agebmax = $agebmax;
        return $this;

    }



    /**
     * Get agebmax
     *
     * @return string 
     */

    public function getAgebmax()

    {
        return $this->agebmax;

    }
     
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Evenement
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }   

    /**
     * Set maps
     *
     * @param string $maps
     * @return Evenement
     */

    public function setMaps($maps)

    {
        $this->maps = $maps;
        return $this;

    }


    /**
     * Get maps
     *
     * @return string
     */

    public function getMaps()

    {
        return $this->maps;

    }
}

