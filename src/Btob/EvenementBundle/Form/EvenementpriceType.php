<?php 

namespace Btob\EvenementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EvenementpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => "Titre de zone", 'required' => true))
            ->add('pricead',NULL, array('required' => true, 'label' => "Prix Adulte"))
            ->add('priceenf',NULL, array('required' => true, 'label' => "Prix Enfant"))
            ->add('priceenf12',NULL, array('required' => true, 'label' => "Prix Enfant(1 Enfant+2 adulte)"))
            ->add('priceenf11',NULL, array('required' => true, 'label' => "Prix Enfant(1 Enfant+1 adulte)"))
            ->add('priceb',NULL, array('required' => true, 'label' => "Prix Bébé"))
            ->add('capacite',NULL, array('required' => true, 'label' => "Capacité"))//,'data' => '1'
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\EvenementBundle\Entity\Evenementprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_evenementbundle_evenementprice';
    }
}
