<?php

namespace Btob\SejourBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationsejourType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('dcr')
            //->add('total')
           // ->add('etat')
           // ->add('avance')
          //  ->add('resultatfinal')
         //   ->add('numautoris')
            ->add('sejour')
            ->add('nbadultes','text', ['label' => "Nb. adultes :"])
            ->add('nbenfants','text', ['label' => "Nb. enfants :"])
            ->add('client')
            ->add('dated', 'date', ['label' => "Date de Début :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
            ->add('datef', 'date', ['label' => "Date de Fin :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
          //  ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\SejourBundle\Entity\Reservationsejour'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_sejourbundle_reservationsejour';
    }
}
