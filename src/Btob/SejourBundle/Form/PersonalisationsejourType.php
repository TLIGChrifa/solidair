<?php

namespace Btob\SejourBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalisationsejourType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('typec', 'choice', array('label' => 'Type',
        'choices' => array('Excursion' => 'Excursion','Circuit' => 'Circuit',
        ), 'required' => true, 'multiple' => false,
         ))
         ->add('formule', 'choice', array('label' => 'Formule',
         'choices' => array('Logement simple' => 'Logement simple','Logement petit déjeuner' => 'Logement petit déjeuner', 'Demi pension'=> 'Demi pension',
         ), 'required' => true, 'multiple' => false,
          ))
           
            ->add('nbradult' ,'text', ['label' => "Nombre des adultes :"])
            ->add('nbrenf' ,'text', ['label' => "Nombre des enfants :"])
            ->add('nbrbebe' ,'text', ['label' => "Nombre de bébés :"])
            ->add('nbrch' ,'text', ['label' => "Nombre de chambres :"])
            ->add('budgetmin' ,'text', ['label' => "Budget min :"])
            ->add('budgetmax' ,'text', ['label' => "Budget max :"])
            //->add('dcr')
            ->add('remarque')
            ->add('client')
            ->add('sejour')
           // ->add('agent')
           // ->add('dated', 'date', ['label' => "Date de Début :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
           // ->add('datef', 'date', ['label' => "Date de Fin :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
       



        //     ->add('typec')
        //     ->add('formule')
        //     ->add('transfert')
        //     ->add('excursion')
        //     ->add('nbradult')
        //     ->add('nbrenf')
        //     ->add('nbrbebe')
        //     ->add('nbrch')
        //     ->add('budgetmin')
        //     ->add('budgetmax')
        //   //  ->add('dcr')
        //     ->add('remarque')
        //     ->add('client')
        //     ->add('sejour')
        //     ->add('agent')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\SejourBundle\Entity\Personalisationsejour'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_sejourbundle_personalisationsejour';
    }
}
