<?php 

namespace Btob\SejourBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SejourpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pricead',NULL, array('required' => true, 'label' => "Prix Adulte"))
            ->add('priceenf',NULL, array('required' => true, 'label' => "Prix Enfant"))//,'data' => '0'
            ->add('priceenf11',NULL, array('required' => true, 'label' => "Prix Enfant(1enfant+1 adult)"))//,'data' => '0'
            ->add('priceenf12',NULL, array('required' => true, 'label' => "Prix Enfant(1 enfant+2adult)"))//,'data' => '0'
            ->add('priceb',NULL, array('required' => true, 'label' => "Prix Bébé"))//,'data' => '0'
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date début de période",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))
            ->add('supsingle',null, array('required' => true, 'label' => "Supplément Single"))
            ->add('perssupsingle',NULL, array('required' => false, 'label' => "Pourcentage"))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\SejourBundle\Entity\Sejourprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_sejourbundle_sejourprice';
    }
}
