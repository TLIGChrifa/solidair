<?php
namespace Btob\SejourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\HotelBundle\Common\Tools;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use User\UserBundle\Entity\Solde;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
use Btob\SejourBundle\Entity\Sejour;
use Btob\SejourBundle\Entity\Reservationsejour;
use Btob\SejourBundle\Entity\Resasejourcomment;
use Btob\SejourBundle\Entity\Imgesej;
use Btob\HotelBundle\Entity\Clients;
use Btob\SejourBundle\Entity\Personalisationsejour;
use Btob\SejourBundle\Entity\Tpsejour;
use Btob\HotelBundle\Form\ClientsType;
use Symfony\Component\HttpFoundation\Request;
use Btob\SejourBundle\Entity\Reservationsdetail;
use Btob\SejourBundle\Form\PersonalisationsejourType;
use Btob\SejourBundle\Form\PersonalisationsejourpayerType;
use Btob\SejourBundle\Form\ReservationsejourType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
         $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->listPrice();

        $entities = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejour")->findby(array('act' => 1));

          $paginator = $this->get('knp_paginator');

        $entities = $paginator->paginate(

            $sejourprices,

            $request->query->get('page', 1)/*page number*/,

            6/*limit per page*/

        );   

         $user = $this->get('security.context')->getToken()->getUser();



        return $this->render('BtobSejourBundle:Default:index.html.twig', array('entities' => $entities,'sejourprices' => $sejourprices,'user' => $user));



    }

    public function detailAction(Sejour $Sejour)
    {

       $sejourperiode = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($Sejour->getId());

        return $this->render('BtobSejourBundle:Default:detail.html.twig', array('entry' => $Sejour,'periods' => $sejourperiode));

    }

    public function personalisationAction(Sejour $sejour)
    {
       $em = $this->getDoctrine()->getManager();

       $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($sejour->getId());

       $request = $this->get('request');

        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {

            $sejourprices = $request->request->get("sejourprice");

            $session->set('sejourprice', $sejourprices);

            $nbad = $request->request->get("nbad");

            $session->set('nbad', $nbad);

            $nbbebe = $request->request->get("nbbebe");

            $session->set('nbbebe', $nbbebe);

            $nbenf = $request->request->get("nbenf");

            $session->set('nbenf', $nbenf);

          return $this->redirect($this->generateUrl('btob_inscrip_reservation_sejour', array('id'=>$sejour->getId())));

        }

    return $this->render('BtobSejourBundle:Default:personalisation.html.twig', array('entry' => $sejour,'sejourprices' => $sejourprices));

    }

    public function inscriptionAction(Sejour $sejour)
    {
       $em = $this->getDoctrine()->getManager();

       $sejourprices = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->findBySejour($sejour->getId());

       $sejoursupp = $this->getDoctrine()->getRepository("BtobSejourBundle:Supplements")->findSupplementBySejour($sejour->getId());

       



       $request = $this->get('request');

        $session = $this->getRequest()->getSession();



        $request = $this->get('request');

        $sejourprice = $session->get("sejourprice");

        $nbad = $session->get("nbad");

        $nbchambre = count($nbad);

        $nbbebe = $session->get("nbbebe");

        $nbenf = $session->get("nbenf");
        
        $sejourpricess = $this->getDoctrine()->getRepository("BtobSejourBundle:Sejourprice")->find($sejourprice);

        

        $Array = array();

        
     $aujourdhuib = new \DateTime($sejourpricess->getDated()->format("Y-m-d"));
       $nbjourbebe = intval($sejour->getAgebmax())*365;
       $aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");

        
        $aujourdhuienf = new \DateTime($sejourpricess->getDated()->format("Y-m-d"));
       $nbjourenf = intval($sejour->getAgeenfmax())*365;
       $aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        

        if ($request->getMethod() == 'POST') {

            

             

            for($i=0;$i<$nbchambre;$i++)

            {

               

               //adult

                if(isset($request->request->get("namead")[$i]))

                {

                    $adad=array();

                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)

                 {

                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];

                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];

                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];

                

                 if(isset($request->request->get("suppad")[$i][$j]))

                 {

                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];

                 }

                 else{

                   $adad[$j]['suppad']=null;  

                 }

                 

                 

                

                

                

                

                   

                 }

                

                 $Array[$i]['adult']=$adad;

                 

               

                }

                 //enf

                if(isset($request->request->get("nameenf")[$i]))

                {

                     $enf=array();

                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)

                 {

                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];

                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];

                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];

                

                

               

                

                

                if(isset($request->request->get("suppenf")[$i][$k]))

                 {

                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];

                 }

                 else{

                   $enf[$k]['suppenf']=null;  

                 }

                

                

                 

                

                 

                 }

                 

                 $Array[$i]['enf']=$enf;

                }

                

               

                

                //bebe

                if(isset($request->request->get("nameb")[$i]))

                {

                     $bebe=array();

                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)

                 {

                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];

                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];

                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];

                

                

                if(isset($request->request->get("suppb")[$i][$l]))

                 {

                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];

                 }

                 else{

                   $bebe[$l]['suppb']=null;  

                 }


                 }

                 

                 $Array[$i]['bebe']=$bebe;

                }


            }

            

            $session->set('array', $Array);

             $session->set('sejourprice', $sejourprice);

          return $this->redirect($this->generateUrl('btob_sejour_sejour_reservation_homepage', array('id'=>$sejour->getId())));

        }
	
	
       
       
		$datedbb = $aujourdhuib->format("Y-m-d");
        $datedenff = $aujourdhuienf->format("Y-m-d");
		$datedb_fin=date($datedbb);
		$datedenf_fin=date($datedenff);
		$auj_fin=date("Y-m-d");
	
    return $this->render('BtobSejourBundle:Default:inscription.html.twig', array('datedb' => $datedb,'datedb_fin' => $datedb_fin,'datedenf_fin' => $datedenf_fin,'auj_fin' => $auj_fin,'datedenf' => $datedenf,'entry' => $sejour,'sejoursupp' => $sejoursupp,'sejourprices' => $sejourprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));



    

    }

    

    

    

    

    public function detailreservationAction(Reservationsejour $Reservationsejour)



    {



        return $this->render('BtobSejourBundle:Default:show.html.twig', array('entry' => $Reservationsejour));



    }
    

    public function voucherreservationAction(Reservationsejour $reservationsejour)
    {
        $em = $this->getDoctrine()->getManager();
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resasejourcomment();
                            $comm->setReservationsejour($reservationsejour);
                            $comm->setUser($user);
                            $comm->setAction("Impression Voucher");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
		$pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('BtobSejourBundle:Default:voucher.html.twig', array('entry' => $reservationsejour));
        $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;


    }



    public function deletereservationAction($id)



    {



        $em = $this->getDoctrine()->getManager();



        $entity = $em->getRepository('BtobSejourBundle:Reservationsejour')->find($id);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Suppression: Réservation du voyage organisée " . $entity->getSejour()->getTitre()."du". date_format($entity->getSejourprice()->getDated(), 'Y-m-d')."au". date_format($entity->getSejourprice()->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);


        $em->remove($entity);



        $em->flush();











        return $this->redirect($this->generateUrl('sejours_reservation'));



    }



    public function listreservationsejourAction()



    {







        $user = $this->get('security.context')->getToken()->getUser();



        $dt = new \DateTime();



        $dt->modify('-2 month');



        $dcrfrom = $dt->format("d/m/Y");



        $dt->modify('+4 month');



        $dcrto = $dt->format("d/m/Y");



        $etat = "";



        $client = "";



        $numres = "";



        $dt = new \DateTime();



        $dt->modify('-2 month');



        $datedfrom = $dt->format("d/m/Y");



        $dt->modify('+4 month');



        $datefto = $dt->format("d/m/Y");



        $agence = "";



        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();



        $request = $this->get('request');



        if ($request->getMethod() == 'POST') {



            $dcrto = $request->request->get('dcrto');



            $dcrfrom = $request->request->get('dcrfrom');



            $etat = $request->request->get('etat');



            $client = trim($request->request->get('client'));



            $numres = trim($request->request->get('numres'));







            $agence = $request->request->get('agence');



        }



        $em = $this->getDoctrine()->getManager();



        // reset notification



        $user = $this->get('security.context')->getToken()->getUser();



        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));







        foreach ($notification as $value) {



            $value->setNotif(0);



            $em->persist($value);



            $em->flush();



        }



        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')



        {



            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findAll();







        }else{



            $entities = $em->getRepository('BtobSejourBundle:Reservationsejour')->findBy(array('user' => $user), array('id' => 'DESC'));







        }

        

               if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {



            if ($agence != "") {



                $id = $agence;



            } else {



                $id = $user->getId();



            }



            $tabuser = array();



            foreach ($entities as $value) {



                if ($value->getUser()->getId() == $id) {



                    $tabuser[] = $value;



                }



            }



            $entities = $tabuser;



        }





        if ($client != "") {



            $tabsearch = array();



            foreach ($entities as $value) {



                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {



                    $tabsearch[] = $value;



                }



            }



            $entities = $tabsearch;



        }



        if ($dcrfrom != "") {



            $tabsearch = array();



            $tab = explode('/', $dcrfrom);



            $dtx = $tab[2] . $tab[1] . $tab[0];



            foreach ($entities as $value) {



                $dty = $value->getDcr()->format('Ymd');



                if (($dty - $dtx) >= 0) {



                    $tabsearch[] = $value;



                }



            }



            $entities = $tabsearch;



        }



        if ($dcrto != "") {



            $tabsearch = array();



            $tab = explode('/', $dcrto);



            $dtx = $tab[2] . $tab[1] . $tab[0];



            foreach ($entities as $value) {



                $dty = $value->getDcr()->format('Ymd');



                if (($dtx - $dty) >= 0) {



                    $tabsearch[] = $value;



                }



            }



            $entities = $tabsearch;



        }



       



       





        return $this->render('BtobSejourBundle:Default:listreservation.html.twig', array(



            'entities' => $entities,



            'dcrto' => $dcrto,



            'dcrfrom' => $dcrfrom,



            'etat' => $etat,



            'client' => $client,



            'numres' => $numres,



            'datefto' => $datefto,



            'datedfrom' => $datedfrom,



            'agence' => $agence,



            'users' => $users,



        ));











    }



    public function ajoutreservationAction(Request $request){
        $reservation = new Reservationsejour();
         $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReservationsejourType() , $reservation);
        $form->handleRequest($request);
        $user =  $this->get('security.token_storage')->getToken()->getUser();
       // $reservationsejour = $em->getRepository('BtobSejourBundle:Reservationsejour')->findAll();
   
        if ($form->isSubmitted() && $form->isValid()) {
           
            $reservation->setUser($user);
            $reservation->setEtat(1);
            $request = $this->get('request');
           // $request->request->get("dureecircuit");
            //var_dump( ).die;
          //  $circuitbyid = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($request->request->get("dureecircuit"));
        //     $reservation->setCuircuit($circuitbyid);
             $montant = $reservation->getSejour()->getPrix();
             $marge = ($reservation->getSejour()->getPrix() * $reservation->getSejour()->getMarge()) / 100 ;
             if(!is_null($marge))
             $montant = $montant + $marge;     
             $reservation->setTotal($montant);
            // var_dump($montant).die; 
            $em->persist($reservation);
            $em->flush();
            return $this->redirect($this->generateUrl('sejours_reservation'));

        }

        return $this->render('BtobSejourBundle:Default:ajoutreservation.html.twig', array(
            'reservation' => $reservation,
            'form' => $form->createView(),       
           // 'arr'=>$datenotchecked,
       
        ));
    }














    

  public function reservationsejourAction(Sejour $sejour)



    {



        $em = $this->getDoctrine()->getManager();

       

        $session = $this->getRequest()->getSession();



        $client = new Clients();



        $form = $this->createForm(new ClientsType(), $client);



        $request = $this->get('request');

        

        $recap = $session->get("array");

        $sejourperiode = $session->get("sejourprice");

        

        

       $pricesej =   $this->getDoctrine()->getRepository('BtobSejourBundle:Sejourprice')->find($sejourperiode);   



       

        $ArrBase = array();

        $total=0;

        

       foreach ($recap as $key => $value) {

           

           //adult

           if(isset($value['adult']))

           {

             $nbad = count($value['adult']);   

           }else{

               

             $nbad=0;  

           }

           

          if(isset($value['enf']))

          {

            $nbenf = count($value['enf']);  

          }else{

              

            $nbenf=0;  

          }

           if(isset($value['bebe']))

           {

               $nbbebe = count($value['bebe']);

           }else{

              $nbbebe = 0; 

           }

               

           $totalsuppad=0;

           $totalsuppenf=0;

           $totalsuppb=0;

           

           

          

           

           

           

           if(isset($value['adult']))

           {

           foreach ($value['adult'] as $k1 => $val1) {

               

               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];

               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];

               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];

               

               $suppaddp =0;

               

                  if(isset($val1['suppad']))

                  {

                     $suppadd=array(); 

                      

                     foreach ($val1['suppad'] as $k2 => $val2) {

                         

                    

                    $supplementad =   $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2);   

                    $suppadd[]= $supplementad->getName();

                    $suppaddp+= $supplementad->getPrice();  

                  } 

                  

                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;

                  

                 

                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;

                    

                  }else{

                      

                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 

                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;

                  }

                  

                  

                  $totalsuppad+= $suppaddp;

                  

                  

             

                 

                  

              

           }

           }

         

          //enf

           if(isset($value['enf']))

           {

           foreach ($value['enf'] as $k1e => $vale) {

               

               

               

               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];

               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];

               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];

               $suppenfp=0;

                  if(isset($vale['suppenf']))

                  {

                      $suppenf= array();

                     foreach ($vale['suppenf'] as $k2e => $val2e) {

                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2e);  

                    $suppenf[]=$supplementenf->getName(); 

                    $suppenfp+=$supplementenf->getPrice(); 

                  } 

                  

                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;

                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;

                  }else{

                      

                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;

                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;

                  }

                 

              $totalsuppenf+= $suppenfp;

              

              

              

              

              

              

              

           }

           

           }

           

           

           

           

           //bebe

           

           if(isset($value['bebe']))

           {

           foreach ($value['bebe'] as $k1b => $valb) {

               

               

               

               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];

               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];

               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];

               $suppbp=0;

                  if(isset($valb['suppb']))

                  {

                      $suppb=array();

                     foreach ($valb['suppb'] as $k2b => $val2b) {

                     $supplementb = $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2b);      

                    $suppb[]= $supplementb->getName();

                    $suppbp+= $supplementb->getPrice();  

                  } 

                  

                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;

                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;

                  }else{

                      

                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;

                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;

                  }

                 

              $totalsuppb+= $suppbp;

              

              

              

              

           }

           }

           

        

          $prices = $this->getDoctrine()

                        ->getRepository('BtobSejourBundle:Sejourprice')

                        ->calculsej($pricesej,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);

         $ArrBase[$key]['price'] = $prices; // + ($prices*$sejour->getMarge()/100);

         $user = $this->get('security.context')->getToken()->getUser();
    //      $total+= $ArrBase[$key]['price']; 
        if($user->getPrstsejour()==0){
          $total+= $ArrBase[$key]['price']+$user->getMargesejour(); 
         }else {
          $total+= $ArrBase[$key]['price']+(($ArrBase[$key]['price']*$user->getMargesejour())/100); 
         }
           

       }

        

      

        $sejourprice = $session->get("sejourprice");

        

        

       

       // $nbchambre = count($nbad);



        if ($request->getMethod() == 'POST') {



            $post = $request->request->get('btob_hotelbundle_clients');



            //Tools::dump($post["cin"],true);


            $email = $post["email"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
            if ($testclient != null) {
                $client = $testclient;
            }

            $User = $this->get('security.context')->getToken()->getUser();



            $form->bind($request);



            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();

                $resa=new Reservationsejour();

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setSejour($sejour);

                $resa->setSejourprice($pricesej);

				$resa->setEtat(1);
                $resa->setTotal($total);

                  

                $em->persist($resa);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Ajouter une reservation du voyage organisée " . $sejour->getTitre()."du". date_format($pricesej->getDated(), 'Y-m-d')."au". date_format($pricesej->getDates(), 'Y-m-d'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);


                $em->flush();

                
   

            $ArrBase = array();

            $total=0;

        

       foreach ($recap as $key => $value) {

           

           //adult

           if(isset($value['adult']))

           {

             $nbad = count($value['adult']);   

           }else{

               

             $nbad=0;  

           }

           

          if(isset($value['enf']))

          {

            $nbenf = count($value['enf']);  

          }else{

              

            $nbenf=0;  

          }

           if(isset($value['bebe']))

           {

               $nbbebe = count($value['bebe']);

           }else{

              $nbbebe = 0; 

           }

               

           $totalsuppad=0;

           $totalsuppenf=0;

           $totalsuppb=0;

           

       

           

           

           if(isset($value['adult']))

           {

           foreach ($value['adult'] as $k1 => $val1) {

               

               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];

               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];

               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];

               

               $suppaddp =0;

           

           $resadetail=new Reservationsdetail();

           $resadetail->setReservationsejour($resa);

           $resadetail->setChambre($key+1);

           

           

           $resadetail->setNamead($val1['namead']);

           $resadetail->setPrenomad($val1['prenomad']);

           $resadetail->setAgead($val1['agead']);

               

           

          

                  if(isset($val1['suppad']))

                  {

                      

                     



 

                     $suppadd=array(); 

                      

                     foreach ($val1['suppad'] as $k2 => $val2) {

                         

                    

                    $supplementad =   $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2);   

                    $suppadd[]= $supplementad->getName();

                    $suppaddp+= $supplementad->getPrice();  

                  } 

                  

                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;

                  

                 

                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;

                    

                  }else{

                      

                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 

                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;

                  }

                  

                 

                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));

                  $totalsuppad+= $suppaddp;


                  //reduction

                  



           $em->persist($resadetail);

           $em->flush();

           }

           }

         

          //enf

           if(isset($value['enf']))

           {

           foreach ($value['enf'] as $k1e => $vale) {

               

               

               

               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];

               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];

               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];

               $suppenfp=0;

               

           $resadetails=new Reservationsdetail();

           $resadetails->setReservationsejour($resa);

           $resadetails->setChambre($key+1);

           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);

           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);

           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);

                  if(isset($vale['suppenf']))

                  {

          // $resadetails->setSuppenf(json_encode($vale['suppenf']));

                      $suppenf= array();

                     foreach ($vale['suppenf'] as $k2e => $val2e) {

                     $supplementenf =  $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2e);  

                    $suppenf[]=$supplementenf->getName(); 

                    $suppenfp+=$supplementenf->getPrice(); 

                  } 

                  

                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;

                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;

                  }else{

                      

                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;

                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;

                  }

                 

                  

              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));

  

              $totalsuppenf+= $suppenfp;

              

            

 

           $em->persist($resadetails);

            $em->flush();



              

           }

           

           }

           

           

           

           

           //bebe

           

           if(isset($value['bebe']))

           {

           foreach ($value['bebe'] as $k1b => $valb) {

               

               

               

               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];

               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];

               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];

               $suppbp=0;

               

               

               $resadetailb=new Reservationsdetail();

           $resadetailb->setReservationsejour($resa);

           $resadetailb->setChambre($key+1);

           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);

          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);

          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);

                  if(isset($valb['suppb']))

                  {



                      $suppb=array();

                     foreach ($valb['suppb'] as $k2b => $val2b) {

                     $supplementb = $this->getDoctrine()->getRepository('BtobSejourBundle:Supplements')->find($val2b);      

                    $suppb[]= $supplementb->getName();

                    $suppbp+= $supplementb->getPrice();  

                  } 

                  

                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;

                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;

                  }else{

                      

                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;

                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;

                  }


              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));

              $totalsuppb+= $suppbp;

              


            $em->persist($resadetailb);

            $em->flush();





              

           }

           }

           
        

          $prices = $this->getDoctrine()

                        ->getRepository('BtobSejourBundle:Sejourprice')

                        ->calculsej($pricesej,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb);
         
          $ArrBase[$key]['price'] = $prices; //+($prices*$sejour->getMarge()/100);
         

  //        $total+= $ArrBase[$key]['price']; 

         $user = $this->get('security.context')->getToken()->getUser();
        if($user->getPrstsejour()==0){
          $total+= $ArrBase[$key]['price']+$user->getMargesejour(); 
         }else {
          $total+= $ArrBase[$key]['price']+(($ArrBase[$key]['price']*$user->getMargesejour())/100); 
         }
           

       }    

     

            } else {

                echo $form->getErrors();

            }

                $request->getSession()->getFlashBag()->add('notisejourb', 'Votre réservation a bien été envoyé. Merci.');



                return $this->redirect($this->generateUrl('btob_sejour_sejour_homepage'));




        }



        return $this->render('BtobSejourBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$sejour,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));



    }

public function listpersonalisationsejourAction()
    {

        $em = $this->getDoctrine()->getManager();





        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();


        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $this->getDoctrine()->getRepository("BtobSejourBundle:Personalisationsejour")->findAll();

        }else{
            $entities =$this->getDoctrine()->getRepository("BtobSejourBundle:Personalisationsejour")->findBy(array('agent' => $user));

        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        
       

        return $this->render('BtobSejourBundle:Default:listpersonalisation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));






    }   
    
    
    public function detailpersonalisationAction(Personalisationsejour $Personalisationsejour)
    {

        return $this->render('BtobSejourBundle:Default:showPersonalisation.html.twig', array('entry' => $Personalisationsejour));

    }

    public function deletepersonalisationAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobSejourBundle:Personalisationsejour')->find($id);

                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Delete une reservation du personalisation voyage organisée " . $entity->getSejour()->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('sejours_personalisation'));

    }

public function personalizeAction(Sejour $Sejour)
    {
         $em = $this->getDoctrine()->getManager();
         $client = new Clients();
         $form = $this->createForm(new ClientsType(), $client);
         
        $User = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $email = $post["email"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('"email"' => $email));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                 $dated = $request->request->get('dated');
                 
                $dater = $request->request->get('dater');
                $pays = $request->request->get('pays');
                

                $ville = $request->request->get('villes');
                
                
                $typec = $request->request->get('typec');
                $formule = $request->request->get('formule');
                $transfert = $request->request->get('transfert');
                $excursion = $request->request->get('excursion');
                $nbradult = $request->request->get('nbradult');
                $nbrenf = $request->request->get('nbrenf');
                $nbrbebe = $request->request->get('nbrbebe');
                $nbrch = $request->request->get('nbrch');
                $budgetmin = $request->request->get('budgetmin');
                $budgetmax = $request->request->get('budgetmax');
                $remarque = $request->request->get('remarque');

                $personalisation = new Personalisationsejour();
                $personalisation->setClient($client);
                $personalisation->setSejour($Sejour);
                $personalisation->setAgent($User);
                
                
                
                $personalisation->setTypec($typec);
                
                $personalisation->setFormule($formule);
                
                $personalisation->setTransfert($transfert);
                $personalisation->setExcursion($excursion);
                $personalisation->setNbradult($nbradult);
                $personalisation->setNbrenf($nbrenf);
                $personalisation->setNbrbebe($nbrbebe);
                $personalisation->setNbrch($nbrch);
                $personalisation->setBudgetmin($budgetmin);
                $personalisation->setBudgetmax($budgetmax);
                $personalisation->setRemarque($remarque);
                $em->persist($personalisation);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Ajouter une reservation du personalisation voyage organisée " . $Sejour->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();

                foreach( $pays as $key=>$value){
                    $catg = new Tpsejour();
                    $catg->setPersonalisationsejour($personalisation);
                    $catg->setVille($ville[$key]);
                    $catg->setPays($pays[$key]);
                    if($request->request->get('dateflex'.$key)=="on")
                {
                    $catg->setDateflex(true);
                }
                else{
                    
                    $catg->setDateflex(false);
                }
                    
                    $catg->setDated(new \Datetime(Tools::explodedate($dated[$key],'/')));
                    $catg->setDater(new \Datetime(Tools::explodedate($dater[$key],'/')));
                    $em->persist($catg);
                    $em->flush();

                }
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $to = $personalisation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $personalisation->getAgent()->getName().": réservation séjour personnalisé " ;
                $headers = "From:" .$personalisation->getAgent()->getName()."<" .$personalisation->getAgent()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';

                $logo=$this->get('templating.helper.assets')->getUrl('front/images/logo.png');
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message1 .='
				 <b>Bonjour,</b><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br />
				 Cordialement.';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">
				 Résidence LATINA, rue des Lacs de MAZURIE, appartement n°B1, Les Berges de Lac - <a style="color:#fff;">+216 71 862 263</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';

                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Explore Voyage: réservation  Séjour personnalisé  " ;
                $header = "From:" .$personalisation->getAgent()->getName()."<" .$personalisation->getAgent()->getEmail(). ">\n";
                $header .= "Reply-To:" .$personalisation->getAgent()->getName()." " .$admin->getEmail(). "\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $logo=$this->get('templating.helper.assets')->getUrl('front/images/logo.png');
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message2 .='<b>Bonjour,</b><br>
				 vous avez reçu une réservation Séjours  , merci de consulter votre backoffice .';

                $message2 .= '<br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">
				 Résidence LATINA, rue des Lacs de MAZURIE, appartement n°B1, Les Berges de Lac - <a style="color:#fff;">+216 71 862 263</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';


                mail($too, $subjects, $message2, $header); 
                
                
                
         $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a été bien envoyée.');

                return $this->redirect($this->generateUrl('btob_sejour_sejour_homepage'));

          $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client); 
            }
        }

        return $this->render('BtobSejourBundle:Default:personalize.html.twig', array('entry' => $Sejour, 'form' => $form->createView()));
    }   



    public function actiAction(Reservationsejour $resasejour, $action)
    {
        $em = $this->getDoctrine()->getManager();
        $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
        $factures = array();
        $dr = new \DateTime();
        if (count($facts) > 0) {
            foreach ($facts as $kef) {
                if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                    array_push($factures, $kef);
                }
            }
        }

        if (count($factures) > 0) {
            $num = $factures[0]->getNum() + 1;
        } else {
            $num = 1;
        }

        if ($action == 2) {
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setCode("FO");
            $ffact->setEtat(1);
            $ffact->setClient($resasejour->getClient()->getName() . ' ' . $resasejour->getClient()->getPname());
            $ffact->setClientId($resasejour->getClient());
            $ffact->setDcr($dr);
            $ffact->setDate(new \DateTime());
            $ffact->setIdm($resasejour->getId());
            $ffact->setType("Voyage organisée");
            $ffact->setMontant($resasejour->getTotal());
            $em->persist($ffact);
            $em->flush();

            $tfact = new Tfacture();
            $tfact->setFacture($ffact);
            $tfact->setPrixUnitaire($resasejour->getTotal());
            $tfact->setQuantite(1);
            $tfact->setRemise(0);
            $tfact->setTva(20);
           // $tfact->setMontant($resasejour->getTotal());
           $tfact->setType("Voyage organisée:".$resasejour->getSejour()->getTitre());
            $em->persist($tfact);
            $em->flush();


			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resasejourcomment();
                            $comm->setReservationsejour($resasejour);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resasejour->setEtat(2);
            $em->persist($resasejour);
            $em->flush();
			
            return $this->redirect($this->generateUrl('btob_sejour_detail_sejour_reservation', array('id' => $resasejour->getId())));

        }

        if ($action == 3) {

            $resasejour->setEtat(3);
            $em->persist($resasejour);
            $em->flush();
			
			$user = $this->get('security.context')->getToken()->getUser();			
			$solde = new Solde();
            $solde->setObservation("Paiement de la commande du Voyage organisée " . Tools::RefResa("", $resasejour->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resasejour->getUser());
            $solde->setSolde(($resasejour->getTotal() * (-1)));
            $em->persist($solde);
			$em->flush();
            $newsolde = $resasejour->getUser()->getSold() - $resasejour->getTotal();
            $currenuser = $resasejour->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();
			
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resasejourcomment();
                            $comm->setReservationsejour($resasejour);
                            $comm->setUser($user);
                            $comm->setAction("Réservation payée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            return $this->redirect($this->generateUrl('btob_sejour_detail_sejour_reservation', array('id' => $resasejour->getId())));

        }
        if ($action == 0) {
		
		   if($resasejour->getEtat()==2){
               $ffact = new Facture();
               $ffact->setNum($num);
               $ffact->setCode("FO");
               $ffact->setEtat(1);
               $ffact->setClient($resasejour->getClient()->getName() . ' ' . $resasejour->getClient()->getPname());
               $ffact->setClientId($resasejour->getClient());
               $ffact->setDcr($dr);
               $ffact->setDate(new \DateTime());
               $ffact->setIdm($resasejour->getId());
               $ffact->setType("Voyage organiée");
               $ffact->setMontant(-1*$resasejour->getTotal());
               $em->persist($ffact);
               $em->flush();

               $tfact = new Tfacture();
               $tfact->setFacture($ffact);
				$tfact->setPrixUnitaire((-1)*$resasejour->getTotal());
				$tfact->setQuantite(1);
				$tfact->setRemise(0);
				$tfact->setTva(20);
             //  $tfact->setMontant(-1*$resasejour->getTotal());
               $tfact->setType("Voyage organisée:".$resasejour->getSejour()->getTitre());
               $em->persist($tfact);
               $em->flush();

		   }
		   
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resasejourcomment();
                            $comm->setReservationsejour($resasejour);
                            $comm->setUser($user);
                            $comm->setAction("Réservation annulée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resasejour->setEtat(0);
            $em->persist($resasejour);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_sejour_detail_sejour_reservation', array('id' => $resasejour->getId())));

        }
        if ($action == 4) {
		   
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resasejourcomment();
                            $comm->setReservationsejour($resasejour);
                            $comm->setUser($user);
                            $comm->setAction("Demande d'annulation du réservation");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resasejour->setEtat(4);
            $em->persist($resasejour);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_sejour_detail_sejour_reservation', array('id' => $resasejour->getId())));
		}
    }

    
    public function ajxaddsejourcommentAction()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $action = $request->request->get("action");
        $user = $this->get('security.context')->getToken()->getUser();
        $resasejour = $this->getDoctrine()->getRepository('BtobSejourBundle:Reservationsejour')->find($id);
        $comment = new Resasejourcomment();
        $comment->setAction($action);
        $comment->setUser($user);
        $comment->setReservationsejour($resasejour);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        exit;
    }

    public function addreservationpersonnaliseeAction(Request $request){
       $reservation = new Personalisationsejour();
       $em = $this->getDoctrine()->getManager();
       $form = $this->createForm(new PersonalisationsejourType() , $reservation);
       $form->handleRequest($request);
       $user =  $this->get('security.token_storage')->getToken()->getUser();
      // $circuit = $em->getRepository('BtobCuircuitBundle:Cuircuit')->findAll();

       if ($form->isSubmitted() && $form->isValid()) {
          
            $reservation->setAgent($user);
           // $reservation->setEtat(1);
           $request = $this->get('request');
          // $request->request->get("dureecircuit");
        // //  var_dump($circuitbyid).die;
           $montant = $reservation->getSejour()->getPrix();
         //  var_dump($circuitbyid).die;
           $marge = ($montant * $reservation->getSejour()->getMarge()) / 100 ;
            if(!is_null($marge))
            $montant = $montant + $marge;     
           $reservation->setTotal($montant);
           ///var_dump($montant).die;
           $em->persist($reservation);
           $em->flush();
           return $this->redirect($this->generateUrl('sejours_payer_reservation',array('id'=>$reservation->getId())));

       }


       return $this->render('BtobSejourBundle:Default:ajoutreservationpersonnalisee.html.twig', array(
           'reservation' => $reservation,
          // 'arraydater'=> $arraydater,
           'form' => $form->createView(),
          // 'datef'=> max($arraydatefin),
          // 'arr'=>$datenotchecked,
         //  'circuit'=>$circuit,
       ));
    }


    public function payerreservationpersonnaliseeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobSejourBundle:Personalisationsejour')
            ->find($id);
      
        
      //  
            $form = $this->createForm(new PersonalisationsejourpayerType(), $reservation);
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
              //  if($reservation->getTotal() == $form->get('montantpaye')->getData())
               // $reservation->setEtat(3);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('sejours_personalisation'));

            }
        
       

       return $this->render('BtobSejourBundle:Default:payersejourpersonnalisee.html.twig', array('form' =>$form->createView(),'reservation'=>$reservation));
    }


}



