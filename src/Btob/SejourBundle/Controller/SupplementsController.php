<?php

namespace Btob\SejourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SejourBundle\Entity\Supplements;
use Symfony\Component\HttpFoundation\Request;
use Btob\SejourBundle\Form\SupplementsType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class SupplementsController extends Controller {

    public function indexAction($sejourid) {
        $sejour = $this->getDoctrine()
                ->getRepository('BtobSejourBundle:Sejour')
                ->find($sejourid);
        return $this->render('BtobSejourBundle:Supplements:index.html.twig', array('entities' => $sejour->getSupplements(), "sejourid" => $sejourid));
    }

    public function addAction($sejourid) {
        $sejour = $this->getDoctrine()->getRepository('BtobSejourBundle:Sejour')->find($sejourid);
        $Supplements = new Supplements();
        $form = $this->createForm(new SupplementsType(), $Supplements);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplements->setSejour($sejour);
                $em->persist($Supplements);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Ajout: Nouveau supplément - ". $sejour->getTitre());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobSejourBundle:Supplements:form.html.twig', array('form' => $form->createView(), "sejourid" => $sejourid));
    }

    public function editAction($id, $sejourid) {
        $request = $this->get('request');
        $Supplements = $this->getDoctrine()
                ->getRepository('BtobSejourBundle:Supplements')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementsType(), $Supplements);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Modification: Supplément du voyage organisée (". $Supplements->getSejour()->getTitre(). ") n° " . $Supplements->getId()." - ". $Supplements->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobSejourBundle:Supplements:form.html.twig', array('form' => $form->createView(), 'id' => $id, "sejourid" => $sejourid)
        );
    }

    public function deleteAction(Supplements $Supplements, $sejourid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplements) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Voyage organisée");
                        $hist->setMessage("Suppression: Supplément du voyage organisée (". $Supplements->getSejour()->getTitre(). ") n° " . $Supplements->getId()." - ". $Supplements->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Supplements);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplements_homepage', array("sejourid" => $sejourid)));
    }

}
