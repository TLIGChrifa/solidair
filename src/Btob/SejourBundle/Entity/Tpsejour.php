<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpsejour
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\TpsejourRepository")
 */
class Tpsejour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Personalisationsejour", inversedBy="tpsejour")
     * @ORM\JoinColumn(name="personalisationsejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $personalisationsejour;
    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    private $pays;
    
     /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime" , nullable=true)
     */
    private $dated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="datetime" , nullable=true)
     */
    private $dater;
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dateflex", type="boolean" , nullable=true)
     */
    private $dateflex;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personalisationsejour
     *
     * @param \Btob\SejourBundle\Entity\Personalisationsejour $personalisationsejour
     * @return Tpsejour
     */
    public function setPersonalisationsejour(\Btob\SejourBundle\Entity\Personalisationsejour $personalisationsejour = null)
    {
        $this->personalisationsejour = $personalisationsejour;

        return $this;
    }

    /**
     * Get personalisationsejour
     *
     * @return \Btob\SejourBundle\Entity\Personalisationsejour
     */
    public function getPersonalisationsejour()
    {
        return $this->personalisationsejour;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Tpsejour
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }
    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    
    
    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Tpsejour
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }
    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }
    
    
     /**
     * Set dateflex
     *
     * @param boolean $dateflex
     * @return Tpsejour
     */
    public function setDateflex($dateflex)
    {
        $this->dateflex = $dateflex;

        return $this;
    }

    /**
     * Get dateflex
     *
     * @return boolean 
     */
    public function getDateflex()
    {
        return $this->dateflex;
    }
     /**
     * Set pays
     *
     * @param string $pays
     * @return Tpsejour
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }
    
    
    
    /**
     * Set ville
     *
     * @param string $ville
     * @return Tpsejour
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }
    
       
	
}
