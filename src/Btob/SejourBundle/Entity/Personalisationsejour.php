<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personalisationsejour
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\PersonalisationsejourRepository")
 */
class Personalisationsejour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="personalisationsejour")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Sejour", inversedBy="personalisationsejour")
     * @ORM\JoinColumn(name="sejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejour;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="personalisationsejour")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
            
    /**
     * @var string
     *
     * @ORM\Column(name="typec", type="string", length=255, nullable=true)
     */
    private $typec;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="formule", type="string", length=255, nullable=true)
     */
    private $formule;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="transfert", type="string", length=255, nullable=true)
     */
    private $transfert;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="excursion", type="integer" , nullable=true)
     */
    private $excursion;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbradult", type="integer" , nullable=true)
     */
    private $nbradult;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrenf", type="integer" , nullable=true)
     */
    private $nbrenf;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrbebe", type="integer" , nullable=true)
     */
    private $nbrbebe;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrch", type="integer" , nullable=true)
     */
    private $nbrch;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="budgetmin", type="float" , nullable=true)
     */
    private $budgetmin;
    
    
     /**
     * @var float
     *
     * @ORM\Column(name="budgetmax", type="float" , nullable=true)
     */
    private $budgetmax;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime" , nullable=true)
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text" , nullable=true)
     */
    private $remarque;

    /**

     * @ORM\OneToMany(targetEntity="Tpsejour", mappedBy="personalisationsejour")

     */

    protected $personalisationsejour;


    
       /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
        /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;
      
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    public function __construct(){
        $this->dcr=new \DateTime();
    }

    
    

     /**
     * Set typec
     *
     * @param string $typec
     * @return Personalisationsejour
     */
    public function setTypec($typec)
    {
        $this->typec = $typec;

        return $this;
    }

    /**
     * Get typec
     *
     * @return string 
     */
    public function getTypec()
    {
        return $this->typec;
    }
    
    
    /**
     * Set formule
     *
     * @param string $formule
     * @return Personalisationsejour
     */
    public function setFormule($formule)
    {
        $this->formule = $formule;

        return $this;
    }

    /**
     * Get formule
     *
     * @return string 
     */
    public function getFormule()
    {
        return $this->formule;
    }
    
    /**
     * Set transfert
     *
     * @param string $transfert
     * @return Personalisationsejour
     */
    public function setTransfert($transfert)
    {
        $this->transfert = $transfert;

        return $this;
    }

    /**
     * Get transfert
     *
     * @return string 
     */
    public function getTransfert()
    {
        return $this->transfert;
    }
    
    

    
     /**
     * Set nbradult
     *
     * @param integer $nbradult
     * @return Personalisationsejour
     */
    public function setNbradult($nbradult)
    {
        $this->nbradult = $nbradult;

        return $this;
    }
    
    
    /**
     * Get nbradult
     *
     * @return integer 
     */
    public function getNbradult()
    {
        return $this->nbradult;
    }

    /**
     * Set nbrenf
     *
     * @param integer $nbrenf
     * @return Personalisationsejour
     */
    public function setNbrenf($nbrenf)
    {
        $this->nbrenf = $nbrenf;

        return $this;
    }

    /**
     * Get nbrenf
     *
     * @return integer 
     */
    public function getNbrenf()
    {
        return $this->nbrenf;
    }

     /**
     * Set nbrbebe
     *
     * @param integer $nbrbebe
     * @return Personalisationsejour
     */
    public function setNbrbebe($nbrbebe)
    {
        $this->nbrbebe = $nbrbebe;

        return $this;
    }

    /**
     * Get nbrbebe
     *
     * @return integer 
     */
    public function getNbrbebe()
    {
        return $this->nbrbebe;
    }
    
    
    /**
     * Set budgetmin
     *
     * @param float $budgetmin
     * @return Personalisationsejour
     */
    public function setBudgetmin($budgetmin)
    {
        $this->budgetmin = $budgetmin;

        return $this;
    }

    /**
     * Get budgetmin
     *
     * @return float 
     */
    public function getBudgetmin()
    {
        return $this->budgetmin;
    }
    
    
    
     /**
     * Set budgetmax
     *
     * @param float $budgetmax
     * @return Personalisationsejour
     */
    public function setBudgetmax($budgetmax)
    {
        $this->budgetmax = $budgetmax;

        return $this;
    }

    /**
     * Get budgetmax
     *
     * @return float 
     */
    public function getBudgetmax()
    {
        return $this->budgetmax;
    }
    
    
    /**
     * Get excursion
     *
     * @return integer 
     */
    public function getExcursion()
    {
        return $this->excursion;
    }
    /**
     * Set excursion
     *
     * @param integer $excursion
     * @return Personalisationsejour
     */
    public function setExcursion($excursion)
    {
        $this->excursion = $excursion;

        return $this;
    }

    /**
     * Get nbrch
     *
     * @return integer 
     */
    public function getNbrch()
    {
        return $this->nbrch;
    }
    
    /**
     * Set nbrch
     *
     * @param integer $nbrch
     * @return Personalisationsejour
     */
    public function setNbrch($nbrch)
    {
        $this->nbrch = $nbrch;

        return $this;
    }
    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Personalisationsejour
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Personalisationsejour
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Personalisationsejour
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set sejour
     *
     * @param \Btob\SejourBundle\Entity\Sejour $sejour
     * @return Personalisationsejour
     */
    public function setSejour(\Btob\SejourBundle\Entity\Sejour $sejour = null)
    {
        $this->sejour = $sejour;

        return $this;
    }

    /**
     * Get sejour
     *
     * @return \Btob\SejourBundle\Entity\Sejour 
     */
    public function getSejour()
    {
        return $this->sejour;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Personalisationsejour
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
    /**

     * Add categories

     *

     * @param \Btob\SejourBundle\Entity\Tpsejour $personalisationsejour

     * @return Personalisationsejour

     */

    public function addPersonalisationsejour(\Btob\SejourBundle\Entity\Tpsejour $personalisationsejour)

    {

        $this->personalisationsejour[] = $personalisationsejour;



        return $this;

    }



    /**

     * Remove personalisationsejour

     *

     * @param \Btob\SejourBundle\Entity\Tpsejour $personalisationsejour

     */

    public function removePersonalisationsejour(\Btob\SejourBundle\Entity\Tpsejour $personalisationsejour)

    {

        $this->personalisationsejour->removeElement($personalisationsejour);

    }



    /**

     * Get personalisationsejour

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getPersonalisationsejour()

    {

        return $this->personalisationsejour;

    }
    
        
    
    

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return Personalisationsejour
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Personalisationsejour
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return Personalisationsejour
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return Personalisationsejour
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }
}
