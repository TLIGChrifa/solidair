<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resasejourcomment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\ResasejourcommentRepository")
 */
class Resasejourcomment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=30)
     */
    private $ip;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationsejour", inversedBy="resasejourcomment")
     * @ORM\JoinColumn(name="res_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationsejour;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="resasejourcomment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Resasejourcomment
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Resasejourcomment
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Resasejourcomment
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set reservationsejour
     *
     * @param \Btob\SejourBundle\Entity\Reservationsejour $reservationsejour
     * @return Resasejourcomment
     */
    public function setReservationsejour(\Btob\SejourBundle\Entity\Reservationsejour $reservationsejour = null)
    {
        $this->reservationsejour = $reservationsejour;

        return $this;
    }

    /**
     * Get reservationsejour
     *
     * @return \Btob\SejourBundle\Entity\Reservationsejour
     */
    public function getReservationsejour()
    {
        return $this->reservationsejour;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Resasejourcomment
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
