<?php







namespace Btob\SejourBundle\Entity;







use Doctrine\ORM\Mapping as ORM;







/**



 * Sejour



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\SejourRepository")



 */



class Sejour



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;







    /**



     * @var string



     *



     * @ORM\Column(name="titre", type="string", length=255, nullable=true)



     */



    private $titre;



    /**



     * @var \DateTime



     *



     * @ORM\Column(name="dcr", type="date", nullable=true)



     */



    private $dcr;



    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $act;











    /**



     * @var string



     *



     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)



     */



    private $libelle;







    /**



     * @var string



     *



     * @ORM\Column(name="dureesj", type="string", length=255 , nullable=true)



     */



    private $dureesj;







    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;



    /**



     * @ORM\OneToMany(targetEntity="Imgesej", mappedBy="sejour")



     */



    protected $imgesej;









    /**



     * @var boolean



     *



     * @ORM\Column(name="pindex", type="boolean" , nullable=true)



     */



    private $pindex;



    /**



     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Themes", inversedBy="sejour")



     * @ORM\JoinColumn(name="themes_id", referencedColumnName="id",onDelete="CASCADE")



     */



    protected $themes;







    /**



     * @var string



     *



     * @ORM\Column(name="villed", type="string", length=255 , nullable=true)



     */



    private $villed;







    /**



     * @var string



     *



     * @ORM\Column(name="voyagecart", type="string", length=255 , nullable=true)



     */



    private $voyagecart;











    /**



     * @var string



     *



     * @ORM\Column(name="villea", type="string", length=255 , nullable=true)



     */



    private $villea;







    /**



     * @var string



     *



     * @ORM\Column(name="hotel", type="string", length=255 , nullable=true)



     */



    private $hotel;







    /**



     * @var integer



     *



     * @ORM\Column(name="nbretoile", type="integer" , nullable=true)



     */



    private $nbretoile;


    /**
     * @var integer
     *
     * @ORM\Column(name="nbrplaces", type="integer" , nullable=true)
     */

    private $nbrplaces;


    /**

     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;



    /**



     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Arrangement", inversedBy="sejour")



     * @ORM\JoinColumn(name="arg_id", referencedColumnName="id",onDelete="CASCADE")



     */



    protected $arrangement;





     /**

     * @ORM\OneToMany(targetEntity="Supplements", mappedBy="sejour", cascade={"remove"})

     */

    protected $supplements;

    



    

    

        /**

     * @ORM\OneToMany(targetEntity="Sejourprice", mappedBy="sejour", cascade={"remove"})

     */

    protected $sejourprice;

    

    

    /**

     * @var float

     *

     * @ORM\Column(name="prixavance", type="float" , nullable=true)

     */









    private $prixavance;       

    

    

    

    

    /**

     * @var string

     *

     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)

     */



    private $ageenfmin;

    

    /**

     * @var string

     *

     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)

     */



    private $ageenfmax;

    

    

        /**

     * @var string

     *

     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)

     */



    private $agebmin;

    

    /**

     * @var string

     *

     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)

     */



    private $agebmax;


    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */

    private $marge; 



    /**
     * @var string
     *
     * @ORM\Column(name="maps", type="string" , nullable=true)
     */

    private $maps; 

      /**
     * @var \Date
     *
     * @ORM\Column(name="dated", type="date", nullable =true)
     */
    private $dated;
      /**
     * @var date
     *
     * @ORM\Column(name="datef", type="date" , nullable = true)
     */
    private $datef;
  


    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }





    /**



     * Set seotitle



     *



     * @param string $seotitle



     * @return Sejour



     */



    public  function  __construct(){



        $this->dcr = new \DateTime();

        $this->supplements = new \Doctrine\Common\Collections\ArrayCollection();

        $this->sejourprice = new \Doctrine\Common\Collections\ArrayCollection();





    }







    /**



     * Set titre



     *



     * @param string $titre



     * @return Sejour



     */



    public function setTitre($titre)



    {



        $this->titre = $titre;







        return $this;



    }







    /**



     * Get titre



     *



     * @return string 



     */



    public function getTitre()



    {



        return $this->titre;



    }







    /**



     * Set libelle



     *



     * @param string $libelle



     * @return Sejour



     */



    public function setLibelle($libelle)



    {



        $this->libelle = $libelle;







        return $this;



    }







    /**



     * Get libelle



     *



     * @return string 



     */



    public function getLibelle()



    {



        return $this->libelle;



    }







    /**



     * Set dureesj



     *



     * @param string $dureesj



     * @return Sejour



     */



    public function setDureesj($dureesj)



    {



        $this->dureesj = $dureesj;







        return $this;



    }







    /**



     * Get dureesj



     *



     * @return string 



     */



    public function getDureesj()



    {



        return $this->dureesj;



    }







    /**



     * Set prix



     *



     * @param float $prix



     * @return Sejour



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }











    /**



     * Set pindex



     *



     * @param boolean $pindex



     * @return Sejour



     */



    public function setPindex($pindex)



    {



        $this->pindex = $pindex;







        return $this;



    }







    /**



     * Get pindex



     *



     * @return boolean 



     */



    public function getPindex()



    {



        return $this->pindex;



    }







    /**



     * Set villed



     *



     * @param string $villed



     * @return Sejour



     */



    public function setVilled($villed)



    {



        $this->villed = $villed;







        return $this;



    }







    /**



     * Get villed



     *



     * @return string 



     */



    public function getVilled()



    {



        return $this->villed;



    }







    /**



     * Set voyagecart



     *



     * @param string $voyagecart



     * @return Sejour



     */



    public function setVoyagecart($voyagecart)



    {



        $this->voyagecart = $voyagecart;







        return $this;



    }







    /**



     * Get voyagecart



     *



     * @return string 



     */



    public function getVoyagecart()



    {



        return $this->voyagecart;



    }







    /**



     * Set hotel



     *



     * @param string $hotel



     * @return Sejour



     */



    public function setHotel($hotel)



    {



        $this->hotel = $hotel;







        return $this;



    }







    /**



     * Get hotel



     *



     * @return string 



     */



    public function getHotel()



    {



        return $this->hotel;



    }







    /**



     * Set nbretoile



     *



     * @param integer $nbretoile



     * @return Sejour



     */



    public function setNbretoile($nbretoile)



    {



        $this->nbretoile = $nbretoile;







        return $this;



    }







    /**



     * Get nbretoile



     *



     * @return integer 



     */



    public function getNbretoile()



    {



        return $this->nbretoile;



    }











    /**



     * Set description



     *



     * @param string $description



     * @return Sejour



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set dcr



     *



     * @param \DateTime $dcr



     * @return Sejour



     */



    public function setDcr($dcr)



    {



        $this->dcr = $dcr;







        return $this;



    }







    /**



     * Get dcr



     *



     * @return \DateTime 



     */



    public function getDcr()



    {



        return $this->dcr;



    }







    /**



     * Set act



     *



     * @param boolean $act



     * @return Sejour



     */



    public function setAct($act)



    {



        $this->act = $act;







        return $this;



    }







    /**



     * Get act



     *



     * @return boolean 



     */



    public function getAct()



    {



        return $this->act;



    }















    /**



     * Add imgesej



     *



     * @param \Btob\SejourBundle\Entity\Imgesej $imgesej



     * @return Sejour



     */



    public function addImgesej(\Btob\SejourBundle\Entity\Imgesej $imgesej)



    {



        $this->imgesej[] = $imgesej;







        return $this;



    }







    /**



     * Remove imgesej



     *



     * @param \Btob\SejourBundle\Entity\Imgesej $imgesej



     */



    public function removeImgesej(\Btob\SejourBundle\Entity\Imgesej $imgesej)



    {



        $this->imgesej->removeElement($imgesej);



    }







    /**



     * Get imgesej



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgesej()



    {



        return $this->imgesej;



    }







    /**



     * Set villea



     *



     * @param string $villea



     * @return Sejour



     */



    public function setVillea($villea)



    {



        $this->villea = $villea;







        return $this;



    }







    /**



     * Get villea



     *



     * @return string 



     */



    public function getVillea()



    {



        return $this->villea;



    }







    /**



     * Set themes



     *



     * @param \Btob\HotelBundle\Entity\Themes $themes



     * @return Sejour



     */



    public function setThemes(\Btob\HotelBundle\Entity\Themes $themes = null)



    {



        $this->themes = $themes;







        return $this;



    }







    /**



     * Get themes



     *



     * @return \Btob\HotelBundle\Entity\Themes 



     */



    public function getThemes()



    {



        return $this->themes;



    }







    /**



     * Set arrangement



     *



     * @param \Btob\HotelBundle\Entity\Arrangement $arrangement



     * @return Sejour



     */



    public function setArrangement(\Btob\HotelBundle\Entity\Arrangement $arrangement = null)



    {



        $this->arrangement = $arrangement;







        return $this;



    }







    /**



     * Get arrangement



     *



     * @return \Btob\HotelBundle\Entity\Arrangement 



     */



    public function getArrangement()



    {



        return $this->arrangement;



    }

    

    

     /**

     * Add supplements

     *

     * @param \Btob\SejourBundle\Entity\Supplements $supplements

     * @return Sejour

     */

    public function addSupplements(\Btob\SejourBundle\Entity\Supplements $supplements)

    {

        $this->supplements[] = $supplements;



        return $this;

    }



    /**

     * Remove supplements

     *

     * @param \Btob\SejourBundle\Entity\Supplements $supplements

     */

    public function removeSupplements(\Btob\SejourBundle\Entity\Supplements $supplements)

    {

        $this->supplements->removeElement($supplements);

    }



    /**

     * Get supplements

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getSupplements()

    {

        return $this->supplements;

    }

  

    

     /**

     * Add sejourprice

     *

     * @param \Btob\SejourBundle\Entity\Sejourprice $sejourprice

     * @return Sejour

     */

    public function addSejourprice(\Btob\SejourBundle\Entity\Sejourprice $sejourprice)

    {

        $this->sejourprice[] = $sejourprice;



        return $this;

    }



    /**

     * Remove sejourprice

     *

     * @param \Btob\SejourBundle\Entity\Sejourprice $sejourprice

     */

    public function removeSejourprice(\Btob\SejourBundle\Entity\Sejourprice $sejourprice)

    {

        $this->sejourprice->removeElement($sejourprice);

    }



    /**

     * Get Sejourprice

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getSejourprice()

    {

        return $this->sejourprice;

    }   

    

    /**

     * Set prixavance

     *

     * @param float $prixavance

     * @return Sejour

     */



    public function setPrixavance($prixavance)

    {

        $this->prixavance = $prixavance;

        return $this;

    }



    /**

     * Get prixavance

     *

     * @return float 

     */



    public function getPrixavance()

    {

        return $this->prixavance;



    }    

    

    

    

     /**

     * Set ageenfmin

     *

     * @param string $ageenfmin

     * @return Sejour

     */



    public function setAgeenfmin($ageenfmin)



    {

        $this->ageenfmin = $ageenfmin;

        return $this;



    }







    /**

     * Get ageenfmin

     *

     * @return string 

     */



    public function getAgeenfmin()



    {

        return $this->ageenfmin;



    }

    

    

     /**

     * Set ageenfmax

     *

     * @param string $ageenfmax

     * @return Sejour

     */



    public function setAgeenfmax($ageenfmax)



    {

        $this->ageenfmax = $ageenfmax;

        return $this;



    }







    /**

     * Get ageenfmax

     *

     * @return string 

     */



    public function getAgeenfmax()



    {

        return $this->ageenfmax;



    }

    

    

    

    

    

    

         /**

     * Set agebmin

     *

     * @param string $agebmin

     * @return Sejour

     */



    public function setAgebmin($agebmin)



    {

        $this->agebmin = $agebmin;

        return $this;



    }







    /**

     * Get agebmin

     *

     * @return string 

     */



    public function getAgebmin()



    {

        return $this->agebmin;



    }

    

    

     /**

     * Set agebmax

     *

     * @param string $agebmax

     * @return Sejour

     */



    public function setAgebmax($agebmax)



    {

        $this->agebmax = $agebmax;

        return $this;



    }







    /**

     * Get agebmax

     *

     * @return string 

     */



    public function getAgebmax()



    {

        return $this->agebmax;



    }

      /**
     * Set marge
     *
     * @param float $marge
     * @return Sejour
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        
   

    /**
     * Set maps
     *
     * @param string $maps
     * @return Sejour
     */

    public function setMaps($maps)

    {
        $this->maps = $maps;
        return $this;

    }


    /**
     * Get maps
     *
     * @return string
     */

    public function getMaps()

    {
        return $this->maps;

    }


    /**
     * Set nbrplaces
     *
     * @param integer $nbrplaces
     *
     * @return Sejour
     */
    public function setNbrplaces($nbrplaces)
    {
        $this->nbrplaces = $nbrplaces;

        return $this;
    }

    /**
     * Get nbrplaces
     *
     * @return integer
     */
    public function getNbrplaces()
    {
        return $this->nbrplaces;
    }

    /**
     * Add supplement
     *
     * @param \Btob\SejourBundle\Entity\Supplements $supplement
     *
     * @return Sejour
     */
    public function addSupplement(\Btob\SejourBundle\Entity\Supplements $supplement)
    {
        $this->supplements[] = $supplement;

        return $this;
    }

    /**
     * Remove supplement
     *
     * @param \Btob\SejourBundle\Entity\Supplements $supplement
     */
    public function removeSupplement(\Btob\SejourBundle\Entity\Supplements $supplement)
    {
        $this->supplements->removeElement($supplement);
    }
    public function __toString()
{
    return $this->libelle;
}

  

    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Sejour
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Sejour
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }
}
