<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SejourRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SejourRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC'));
    }
    
   public function listPriceactive()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.active =:act')
            ->setParameter('act', true)
           ;
        return $query->getQuery()->getResult();
    }
}
