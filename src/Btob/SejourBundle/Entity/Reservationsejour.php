<?php

namespace Btob\SejourBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationsejour
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SejourBundle\Entity\ReservationsejourRepository")
 */
class Reservationsejour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
  /**
     * @var \Date
     *
     * @ORM\Column(name="dated", type="date", nullable =true)
     */
    private $dated;
      /**
     * @var date
     *
     * @ORM\Column(name="datef", type="date" , nullable = true)
     */
    protected $datef;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true)
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", nullable=true)
     */
    private $etat;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbadultes", type="integer", nullable=true)
     */
    private $nbadultes;
     /**
     * @var integer
     *
     * @ORM\Column(name="nbenfants", type="integer", nullable=true)
     */
    private $nbenfants;

    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float", nullable=true)
     */
    private $avance;


    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    

    /**
     * @ORM\ManyToOne(targetEntity="Sejour", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="sejour_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejour;
    
    /**
     * @ORM\ManyToOne(targetEntity="Sejourprice", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="sejourprice_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $sejourprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationsejour")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    // /**
    //  * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationsejour")
    //  * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
    //  */
    // protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Resasejourcomment", mappedBy="reservationsejour", cascade={"remove"})
     */
    protected $resasejourcomment;
   
    /**
     * @ORM\OneToMany(targetEntity="Reservationsdetail", mappedBy="reservationsejour", cascade={"remove"})
     */
    protected $reservationsdetail;

     /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;


   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationsejour
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationsejour
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * Set sejourprice
     *
     * @param \Btob\SejourBundle\Entity\Sejourprice $sejourprice
     * @return Reservationcircuit
     */
    public function setSejourprice(\Btob\SejourBundle\Entity\Sejourprice $sejourprice = null)
    {
        $this->sejourprice = $sejourprice;

        return $this;
    }

    /**
     * Get sejourprice
     *
     * @return \Btob\SejourBundle\Entity\Sejourprice 
     */
    public function getSejourprice()
    {
        return $this->sejourprice;
    }
    
       
    
    /**
     * Set sejour
     *
     * @param \Btob\SejourBundle\Entity\Sejour $sejour
     * @return Reservationsejour
     */
    public function setSejour(\Btob\SejourBundle\Entity\Sejour $sejour = null)
    {
        $this->sejour = $sejour;

        return $this;
    }

    /**
     * Get sejour
     *
     * @return \Btob\SejourBundle\Entity\Sejour 
     */
    public function getSejour()
    {
        return $this->sejour;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationsejour
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    // /**
    //  * Set user
    //  *
    //  * @param \User\UserBundle\Entity\User $user
    //  * @return Reservationsejour
    //  */
    // public function setUser(\User\UserBundle\Entity\User $user = null)
    // {
    //     $this->user = $user;

    //     return $this;
    // }

    // /**
    //  * Get user
    //  *
    //  * @return \User\UserBundle\Entity\User 
    //  */
    // public function getUser()
    // {
    //     return $this->user;
    // }

   


    /**
     * Add reservationsdetail
     *
     * @param \Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail
     * @return Reservationsejour
     */
    public function addReservationsdetail(\Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail)
    {
        $this->reservationsdetail[] = $reservationsdetail;

        return $this;
    }

    /**
     * Remove reservationsdetail
     *
     * @param \Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail
     */
    public function removeReservationsdetail(\Btob\SejourBundle\Entity\Reservationsdetail $reservationsdetail)
    {
        $this->reservationsdetail->removeElement($reservationsdetail);
    }

    /**
     * Get reservationsdetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReservationsdetail()
    {
        return $this->reservationsdetail;
    }

        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }
    
    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }
    
    
    /**
     * Add resasejourcomment
     *
     * @param \Btob\SejourBundle\Entity\Resasejourcomment $resasejourcomment
     * @return Reservationsejour
     */
    public function addResasejourcomment(\Btob\SejourBundle\Entity\Resasejourcomment $resasejourcomment)
    {
        $this->resasejourcomment[] = $resasejourcomment;

        return $this;
    }

    /**
     * Remove resasejourcomment
     *
     * @param \Btob\SejourBundle\Entity\Resasejourcomment $resasejourcomment
     */
    public function removeResasejourcomment(\Btob\SejourBundle\Entity\Resasejourcomment $resasejourcomment)
    {
        $this->resasejourcomment->removeElement($resasejourcomment);
    }

    /**
     * Get resasejourcomment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResasejourcomment()
    {
        return $this->resasejourcomment;
    }


    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Reservationsejour
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Reservationsejour
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return Reservationsejour
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return Reservationsejour
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return Reservationsejour
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }

    /**
     * Set nbadultes
     *
     * @param integer $nbadultes
     *
     * @return Reservationsejour
     */
    public function setNbadultes($nbadultes)
    {
        $this->nbadultes = $nbadultes;

        return $this;
    }

    /**
     * Get nbadultes
     *
     * @return integer
     */
    public function getNbadultes()
    {
        return $this->nbadultes;
    }

    /**
     * Set nbenfants
     *
     * @param integer $nbenfants
     *
     * @return Reservationsejour
     */
    public function setNbenfants($nbenfants)
    {
        $this->nbenfants = $nbenfants;

        return $this;
    }

    /**
     * Get nbenfants
     *
     * @return integer
     */
    public function getNbenfants()
    {
        return $this->nbenfants;
    }
}
