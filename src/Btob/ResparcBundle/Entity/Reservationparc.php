<?php

namespace Btob\ResparcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationparc
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ResparcBundle\Entity\ReservationparcRepository")
 */
class Reservationparc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationparc")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\ParcBundle\Entity\Parc", inversedBy="reservationparc")
     * @ORM\JoinColumn(name="parc_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $parc;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationparc")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;


    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationparc
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;
    
        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }
    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Omra
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Omra
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }



    /**
     * Set omra
     *
     * @param \Btob\ParcBundle\Entity\Parc $parc
     * @return Reservationparc
     */
    public function setParc(\Btob\ParcBundle\Entity\Parc $parc = null)
    {
        $this->parc = $parc;

        return $this;
    }

    /**
     * Get parc
     *
     * @return \Btob\ParcBundle\Entity\Parc
     */
    public function getParc()
    {
        return $this->parc;
    }
}
