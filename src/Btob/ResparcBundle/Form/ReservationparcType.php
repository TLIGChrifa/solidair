<?php

namespace Btob\ResparcBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationparcType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dcr')
            ->add('client')
            ->add('parc')
            ->add('agent')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\ResparcBundle\Entity\Reservationparc'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_resparcbundle_reservationparc';
    }
}
