<?php
namespace Btob\ResparcBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Btob\ResparcBundle\Entity\Reservationparc;

use Btob\ResparcBundle\Form\ReservationparcType;

/**
 * Reservationparc controller.
 *

 */
class ReservationparcController extends Controller
{
    /**
     * Lists all Reservationparc entities.
     *
     */

    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();


        $entities = $em->getRepository('BtobResparcBundle:Reservationparc')->findAll();


        return $this->render('BtobResparcBundle:Reservationparc:index.html.twig', array(

            'entities' => $entities,

        ));

    }

    /**
     * Creates a new Reservationparc entity.
     *

     */

    public function createAction(Request $request)

    {

        $entity = new Reservationparc();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();


            return $this->redirect($this->generateUrl('reservationparc_show', array('id' => $entity->getId())));

        }


        return $this->render('BtobResparcBundle:Reservationparc:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }


    /**
     * Creates a form to create a Reservationparc entity.
     *
     * @param Reservationparc $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createCreateForm(Reservationparc $entity)

    {

        $form = $this->createForm(new ReservationparcType(), $entity, array(

            'action' => $this->generateUrl('reservationparc_create'),

            'method' => 'POST',

        ));


        $form->add('submit', 'submit', array('label' => 'Create'));


        return $form;

    }


    /**
     * Displays a form to create a new Reservationparc entity.
     *

     */

    public function newAction()

    {

        $entity = new Reservationparc();

        $form = $this->createCreateForm($entity);


        return $this->render('BtobResparcBundle:Reservationparc:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }


    /**
     * Finds and displays a Reservationparc entity.
     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobResparcBundle:Reservationparc')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Reservationparc entity.');

        }


        $deleteForm = $this->createDeleteForm($id);


        return $this->render('BtobResparcBundle:Reservationparc:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));

    }


    /**
     * Displays a form to edit an existing Reservationparc entity.
     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobResparcBundle:Reservationparc')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Reservationparc entity.');

        }


        $editForm = $this->createEditForm($entity);

        $deleteForm = $this->createDeleteForm($id);


        return $this->render('BtobResparcBundle:Reservationparc:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }


    /**
     * Creates a form to edit a Reservationparc entity.
     *
     * @param Reservationparc $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createEditForm(Reservationparc $entity)

    {

        $form = $this->createForm(new ReservationparcType(), $entity, array(

            'action' => $this->generateUrl('reservationparc_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));


        $form->add('submit', 'submit', array('label' => 'Update'));


        return $form;

    }

    /**
     * Edits an existing Reservationparc entity.
     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('BtobResparcBundle:Reservationparc')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find Reservationparc entity.');

        }


        $deleteForm = $this->createDeleteForm($id);

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);


        if ($editForm->isValid()) {

            $em->flush();


            return $this->redirect($this->generateUrl('reservationparc_edit', array('id' => $id)));

        }


        return $this->render('BtobResparcBundle:Reservationparc:edit.html.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

            'delete_form' => $deleteForm->createView(),

        ));

    }

    /**
     * Deletes a Reservationparc entity.
     *

     */

    public function deleteAction(Request $request, $id)

    {

        $form = $this->createDeleteForm($id);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('BtobResparcBundle:Reservationparc')->find($id);


            if (!$entity) {

                throw $this->createNotFoundException('Unable to find Reservationparc entity.');

            }


            $em->remove($entity);

            $em->flush();

        }


        return $this->redirect($this->generateUrl('reservationparc'));

    }


    /**
     * Creates a form to delete a Reservationparc entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservationparc_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

}

