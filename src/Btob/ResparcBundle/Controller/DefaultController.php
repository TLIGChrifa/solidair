<?php

namespace Btob\ResparcBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;


use Btob\ParcBundle\Entity\Parc;

use Btob\ResparcBundle\Entity\Reservationparc;


use Btob\HotelBundle\Entity\Clients;


use Btob\ParcBundle\Form\ParcType;

use Btob\HotelBundle\Form\ClientsType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()

    {

        $parcs = $this->getDoctrine()->getRepository("BtobParcBundle:Parc")->findAll();

        
         $request = $this->get('request');
         $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $parcs,
            $request->query->get('page', 1)/*page number*/,
            1/*limit per page*/
        );
       $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobResparcBundle:Default:index.html.twig', array('entities' => $entities,'user' => $user));
    }


    public function detailAction(Parc $parc)

    {

        return $this->render('BtobResparcBundle:Default:detail.html.twig', array('entry' => $parc));

    }

    public function detailreservationparcAction(Reservationparc $Reservationparc)
    {
        return $this->render('BtobResparcBundle:Default:detailreservation.html.twig', array('entry' => $Reservationparc));
    }


    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $this->getDoctrine()->getRepository("BtobResparcBundle:Reservationparc")->find($id);
        // echo $entities->getId();exit;
        $em->remove($entities);
        $em->flush();
        return $this->redirect($this->generateUrl('reservationparcs'));
    }

    public function reservationparcAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $client = new Clients();


        $form = $this->createForm(new ClientsType(), $client);


        $entities = $this->getDoctrine()->getRepository("BtobParcBundle:Parc")->find($id);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            $cin = $post["cin"];

            // echo $defaultd .'/'.$defaulta ;exit;

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->handleRequest($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();

                //    $dated = new \Datetime(Tools::explodedate($request->request->get('dated'), '-'));

                //    $datef = new \Datetime(Tools::explodedate($request->request->get('datef'), '-'));

                //     $message = $request->request->get('message');


                $reservation = new Reservationparc();

                $reservation->setClient($client);

                $reservation->setParc($entities);

                $reservation->setAgent($this->get('security.context')->getToken()->getUser());


                $em->persist($reservation);

                $em->flush();


                $client = new Clients();


                $form = $this->createForm(new ClientsType(), $client);

                $request->getSession()->getFlashBag()->add('notisejour', 'Votre demande a bien été envoyé. Merci.');


                return $this->redirect($this->generateUrl('reservationparc'));
                //  return $this->render('BtobResparcBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }

        }


        return $this->render('BtobResparcBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));

    }


    public function listreservationparcAction()

    {

        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
        if ($user->getRoles()[0] == 'ROLE_SUPER_ADMIN') {
            $entities = $this->getDoctrine()->getRepository("BtobResparcBundle:Reservationparc")->findAll();

        } else {
            $entities = $this->getDoctrine()->getRepository("BtobResparcBundle:Reservationparc")->findBy(array('agent' => $user));

        }

        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }

        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }


        return $this->render('BtobResparcBundle:Default:listreservationparc.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'agence' => $agence,
            'users' => $users,
        ));


    }






}

