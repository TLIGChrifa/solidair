<?php

namespace Btob\AssuranceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\AssuranceBundle\Entity\Assurance;
use Btob\AssuranceBundle\Form\AssuranceType;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\HotelBundle\Common\Tools;

/**
 * Assurance controller.
 *
 */
class AssuranceController extends Controller
{

    /**
     * Lists all Assurance entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobAssuranceBundle:Assurance')->findAll();
        $em = $this->getDoctrine()->getManager();



        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $em->getRepository('BtobAssuranceBundle:Assurance')->findAll();

        }else{
            $entities = $em->getRepository('BtobAssuranceBundle:Assurance')->findBy(array('user' => $user));

        }

        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }
        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatead()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDateaf()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDatead()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDateaf()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobAssuranceBundle:Assurance:index.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));
    }
    /**
     * Creates a new Assurance entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Assurance();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('assurance_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobAssuranceBundle:Assurance:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Assurance entity.
     *
     * @param Assurance $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Assurance $entity)
    {
        $form = $this->createForm(new AssuranceType(), $entity, array(
            'action' => $this->generateUrl('assurance_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Assurance entity.
     *
     */
    public function newAction()
    {
        $entity = new Assurance();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobAssuranceBundle:Assurance:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Assurance entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobAssuranceBundle:Assurance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assurance entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobAssuranceBundle:Assurance:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Assurance entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobAssuranceBundle:Assurance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assurance entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobAssuranceBundle:Assurance:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Assurance entity.
    *
    * @param Assurance $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Assurance $entity)
    {
        $form = $this->createForm(new AssuranceType(), $entity, array(
            'action' => $this->generateUrl('assurance_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Assurance entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobAssuranceBundle:Assurance')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assurance entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('assurance_edit', array('id' => $id)));
        }

        return $this->render('BtobAssuranceBundle:Assurance:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Assurance entity.
     *
     */
    

public function deleteAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobAssuranceBundle:Assurance')->find($id);

        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('assurance'));

    }
    
    
    
    /**
     * Creates a form to delete a Assurance entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('assurance_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }









    public function reservationAction()
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $daten = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));

                $datead = new \Datetime(Tools::explodedate($request->request->get('datead'),'/'));
                $dateaf = new \Datetime(Tools::explodedate($request->request->get('dateaf'),'/'));
                $duree =$request->request->get("duree");
                
                $assurance=new Assurance();
                $assurance->setDated($daten);
                $assurance->setFunction($request->request->get("function"));
                $assurance->setSituation($request->request->get("situation"));
                $assurance->setQuestion1($request->request->get("question1"));
                $assurance->setQuestion2($request->request->get("question2"));
                $assurance->setQuestion3($request->request->get("question3"));
                $assurance->setPass($request->request->get("pass"));
                $assurance->setClient($client);
		$assurance->setAgent($this->get('security.context')->getToken()->getUser());
                $assurance->setDuree($duree);
                $assurance->setDatead($datead);
                $assurance->setDateaf($dateaf);

                $em->persist($assurance);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_assurance_validation'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobAssuranceBundle:Assurance:reservation.html.twig', array('form' => $form->createView(),));
    }

    public function validationAction(){
        return $this->render('BtobAssuranceBundle:Assurance:validation.html.twig', array());
    }




}
