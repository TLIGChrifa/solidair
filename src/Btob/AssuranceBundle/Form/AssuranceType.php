<?php

namespace Btob\AssuranceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AssuranceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('situation', 'text', array('label' => "situation", 'required' => false))
            ->add('function', 'text', array('label' => "function", 'required' => false))
           
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date debut de période",'format' => 'dd/MM/yyyy'))
            ->add('question1', 'text', array('label' => "question1", 'required' => false))
            ->add('question2', 'text', array('label' => "question2", 'required' => false))
            ->add('question3', 'text', array('label' => "question3", 'required' => false))
            ->add('question1rep', 'text', array('label' => "", 'required' => true))
            ->add('question2rep', 'text', array('label' => "", 'required' => true))
            ->add('question3rep', 'text', array('label' => "", 'required' => true))    
            ->add('pass', 'text', array('label' => "Passeport", 'required' => false))

            ->add('client')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\AssuranceBundle\Entity\Assurance'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_assurancebundle_assurance';
    }
}
