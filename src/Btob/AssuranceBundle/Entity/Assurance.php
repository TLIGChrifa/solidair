<?php

namespace Btob\AssuranceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assurance
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\AssuranceBundle\Entity\AssuranceRepository")
 */
class Assurance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationparc")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationomra")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**
     * @var string
     *
     * @ORM\Column(name="situation", type="string", length=10 , nullable=true)
     */
    private $situation;

    /**
     * @var string
     *
     * @ORM\Column(name="function", type="string", length=10 , nullable=true)
     */
    private $function;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=10 , nullable=true)
     */
    private $duree;

    /**
     * @var string
     *
     * @ORM\Column(name="question1", type="string", length=50 , nullable=true)
     */
    private $question1;

    /**
     * @var string
     *
     * @ORM\Column(name="question2", type="string", length=50 , nullable=true)
     */
    private $question2;

    /**
     * @var string
     *
     * @ORM\Column(name="question3", type="string", length=50 , nullable=true)
     */
    private $question3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=40 , nullable=true)
     */
    private $pass;

    
    
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="datead", type="date" , nullable=true)
     */
    private $datead;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateaf", type="date" , nullable=true)
     */
    private $dateaf;
    
    /**
     * @var string
     *
     * @ORM\Column(name="question1rep", type="string", length=50 , nullable=true)
     */
    private $question1rep;

    /**
     * @var string
     *
     * @ORM\Column(name="question2rep", type="string", length=50 , nullable=true)
     */
    private $question2rep;

    /**
     * @var string
     *
     * @ORM\Column(name="question3rep", type="string", length=50 , nullable=true)
     */
    private $question3rep;    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Assurance
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;
    
        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Assurance
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;
    
        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set situation
     *
     * @param string $situation
     * @return Clients
     */
    public function setSituation($situation)
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * Get situation
     *
     * @return string
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Hotelprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set function
     *
     * @param string $function
     * @return Clients
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set question1
     *
     * @param string $question1
     * @return Clients
     */
    public function setQuestion1($question1)
    {
        $this->question1 = $question1;

        return $this;
    }

    /**
     * Get question1
     *
     * @return string
     */
    public function getQuestion1()
    {
        return $this->question1;
    }

    /**
     * Set question2
     *
     * @param string $question2
     * @return Clients
     */
    public function setQuestion2($question2)
    {
        $this->question2 = $question2;

        return $this;
    }

    /**
     * Get question2
     *
     * @return string
     */
    public function getQuestion2()
    {
        return $this->question2;
    }

    /**
     * Set question3
     *
     * @param string $question3
     * @return Clients
     */
    public function setQuestion3($question3)
    {
        $this->question3 = $question3;

        return $this;
    }

    /**
     * Get question3
     *
     * @return string
     */
    public function getQuestion3()
    {
        return $this->question3;
    }

    /**
     * Set pass
     *
     * @param string $pass
     * @return Clients
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }
    
    
    /**
     * Set datead
     *
     * @param \DateTime $datead
     * @return Assurance
     */
    public function setDatead($datead)
    {
        $this->datead = $datead;

        return $this;
    }

    /**
     * Get datead
     *
     * @return \DateTime
     */
    public function getDatead()
    {
        return $this->datead;
    }

    /**
     * Set dateaf
     *
     * @param \DateTime $dateaf
     * @return Assurance
     */
    public function setDateaf($dateaf)
    {
        $this->dateaf = $dateaf;

        return $this;
    }

    /**
     * Get dateaf
     *
     * @return \DateTime
     */
    public function getDateaf()
    {
        return $this->dateaf;
    }
    
    
    /**
     * Set duree
     *
     * @param string $duree
     * @return Assurance
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return string
     */
    public function getDuree()
    {
        return $this->duree;
    }
    
     /**
     * Set question1rep
     *
     * @param string $question1rep
     * @return Clients
     */
    public function setQuestion1rep($question1rep)
    {
        $this->question1rep = $question1rep;

        return $this;
    }

    /**
     * Get question1rep
     *
     * @return string
     */
    public function getQuestion1rep()
    {
        return $this->question1rep;
    }

    /**
     * Set question2rep
     *
     * @param string $question2rep
     * @return Clients
     */
    public function setQuestion2rep($question2rep)
    {
        $this->question2rep = $question2rep;

        return $this;
    }

    /**
     * Get question2rep
     *
     * @return string
     */
    public function getQuestion2rep()
    {
        return $this->question2rep;
    }

    /**
     * Set question3rep
     *
     * @param string $question3rep
     * @return Clients
     */
    public function setQuestion3rep($question3rep)
    {
        $this->question3rep = $question3rep;

        return $this;
    }

    /**
     * Get question3rep
     *
     * @return string
     */
    public function getQuestion3rep()
    {
        return $this->question3rep;
    }   
    
}
