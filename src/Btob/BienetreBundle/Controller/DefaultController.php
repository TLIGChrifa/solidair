<?php

namespace Btob\BienetreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\BienetreBundle\Entity\Bienetre;

use Btob\BienetreBundle\Entity\Imagbien;

use Btob\BienetreBundle\Entity\Reservationbienetre;

use Btob\BienetreBundle\Form\BienetreType;

use Btob\HotelBundle\Common\Tools;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;
use Btob\BienetreBundle\Entity\Tbien;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()

    {

        $biens = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->findby(array('act' => 1));

        $request = $this->get('request');
         $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $biens,
            $request->query->get('page', 1)/*page number*/,
            8/*limit per page*/
        );
         $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobBienetreBundle:Default:index.html.twig', array('entities' => $entities,'user' => $user));


    }



    public function listreservationbienAction()
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();


        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Reservationbienetre")->findAll();

        }else{
            $entities =$this->getDoctrine()->getRepository("BtobBienetreBundle:Reservationbienetre")->findBy(array('agent' => $user));

        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobBienetreBundle:Bienetre:listreservation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));


    }



    public function detailAction(Bienetre $Bienetre)

    {

        return $this->render('BtobBienetreBundle:Default:detail.html.twig', array('entry' => $Bienetre));

    }





  public function detailreservationbienAction(Reservationbienetre $Reservationbienetre)

    {

        return $this->render('BtobBienetreBundle:Bienetre:detailreservation.html.twig', array('entry' => $Reservationbienetre));

    }









    public function reservationbienAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $client = new Clients();



        $form = $this->createForm(new ClientsType(), $client);



        $entities = $this->getDoctrine()->getRepository("BtobBienetreBundle:Bienetre")->find($id);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            $cin = $post["cin"];

            $named = $request->request->get('named');
			 $prenomad = $request->request->get('prenomad');
			
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $reservation = new Reservationbienetre();
                $datea = new \Datetime(Tools::explodedate($request->request->get('dated'),'/'));
                $defaulta = $request->request->get('defaulta');
                

                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setClient($client);
                $reservation->setBienetre($entities);
                $reservation->setDatear($datea);
               
                $reservation->setHeur($defaulta);
                $em->persist($reservation);
                $em->flush();
                
                
                 foreach( $named as $key=>$value){
                    $catg = new Tbien();
                    $catg->setReservationbienetre($reservation);
                    $catg->setNamead($named[$key]);
                    $catg->setPrenomad($prenomad[$key]);
                 
                    $em->persist($catg);
                    $em->flush();

                }

                $client = new Clients();



                $form = $this->createForm(new ClientsType(), $client);

                $request->getSession()->getFlashBag()->add('notiBienetre', 'Votre message a bien été envoyé. Merci.');



                return $this->render('BtobBienetreBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));





            }

        }



        return $this->render('BtobBienetreBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));

    }

}

