<?php

namespace Btob\BienetreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tbien
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\BienetreBundle\Entity\TbienRepository")
 */
class Tbien
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationbienetre", inversedBy="tbien")
     * @ORM\JoinColumn(name="reservationbienetre_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationbienetre;
    /**
     * @var array
     *
     * @ORM\Column(name="namead", type="text",nullable=true)
     */
    private $namead;

    /**
     * @var array
     *
     * @ORM\Column(name="prenomad", type="text",nullable=true)
     */
    private $prenomad;

   
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reservationbienetre
     *
     * @param \Btob\BienetreBundle\Entity\Reservationbienetre $reservationbienetre
     * @return Tbien
     */
    public function setReservationbienetre(\Btob\BienetreBundle\Entity\Reservationbienetre $reservationbienetre = null)
    {
        $this->reservationbienetre = $reservationbienetre;

        return $this;
    }

    /**
     * Get reservationbienetre
     *
     * @return \Btob\BienetreBundle\Entity\Reservationbienetre
     */
    public function getReservationbienetre()
    {
        return $this->reservationbienetre;
    }

    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservation
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string
     */
    public function getNamead()
    {
        return $this->namead;
    }

    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservation
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }

   
}
