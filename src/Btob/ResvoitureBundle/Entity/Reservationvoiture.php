<?php

namespace Btob\ResvoitureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationvoiture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ResvoitureBundle\Entity\ReservationvoitureRepository")
 */
class Reservationvoiture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationvoiture")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\VoitureBundle\Entity\Voiture", inversedBy="reservationvoiture")
     * @ORM\JoinColumn(name="voiture_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $voiture;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationvoiture")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datep", type="date" , nullable=true)
     */
    private $datep;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datec", type="date" , nullable=true)
     */
    private $datec;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datef", type="date" , nullable=true)
     */
    private $datef;
    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $act;

    /**
     * @var string
     *
     * @ORM\Column(name="numc", type="string", length=10 , nullable=true)
     */
    private $numc;
    
     /**
     * @var string
     *
     * @ORM\Column(name="nationalite", type="string", length=50 , nullable=true)
     */
    private $nationalite;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nump", type="string", length=10 , nullable=true)
     */
    private $nump;
    public function __construct(){
        $this->dcr=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Reservationvoiture
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationvoiture
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationvoiture
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set voiture
     *
     * @param \Btob\VoitureBundle\Entity\Voiture $voiture
     * @return Reservationvoiture
     */
    public function setVoiture(\Btob\VoitureBundle\Entity\Voiture $voiture = null)
    {
        $this->voiture = $voiture;

        return $this;
    }

    /**
     * Get voiture
     *
     * @return \Btob\VoitureBundle\Entity\Voiture
     */
    public function getVoiture()
    {
        return $this->voiture;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationvoiture
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservationvoiture
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     * @return Reservationvoiture
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set datep
     *
     * @param \DateTime $datep
     * @return Reservationvoiture
     */
    public function setDatep($datep)
    {
        $this->datep = $datep;

        return $this;
    }

    /**
     * Get datep
     *
     * @return \DateTime
     */
    public function getDatep()
    {
        return $this->datep;
    }
    
    
    
     /**
     * Set datec
     *
     * @param \DateTime $datec
     * @return Reservationvoiture
     */
    public function setDatec($datec)
    {
        $this->datec = $datec;

        return $this;
    }

    /**
     * Get datec
     *
     * @return \DateTime
     */
    public function getDatec()
    {
        return $this->datec;
    }
    
    
    /**
     * Set numc
     *
     * @param string $numc
     * @return Reservationvoiture
     */
    public function setNumc($numc)
    {
        $this->numc = $numc;

        return $this;
    }

    /**
     * Get numc
     *
     * @return string 
     */
    public function getNumc()
    {
        return $this->numc;
    }
    
    
    
    /**
     * Set act
     *
     * @param integer $act
     * @return Reservationvoiture
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return integer
     */
    public function getAct()
    {
        return $this->act;
    }
    
    
    
     /**
     * Set nump
     *
     * @param string $nump
     * @return Reservationvoiture
     */
    public function setNump($nump)
    {
        $this->nump = $nump;

        return $this;
    }

    /**
     * Get nump
     *
     * @return string 
     */
    public function getNump()
    {
        return $this->nump;
    }
    
    
    
     /**
     * Set nationalite
     *
     * @param string $nationalite
     * @return Reservationvoiture
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    /**
     * Get nationalite
     *
     * @return string 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }
    
       
    
}
