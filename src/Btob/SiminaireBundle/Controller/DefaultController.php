<?php


namespace Btob\SiminaireBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\SiminaireBundle\Entity\Siminaire;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\SiminaireBundle\Entity\Reservationsim;
class DefaultController extends Controller
{
    public function indexAction()
    {
        $entities = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Siminaire")->findby(array('pub' => 1));
        return $this->render('BtobSiminaireBundle:Default:index.html.twig', array('entities' => $entities));
    }

    public function detailAction(Siminaire $Siminaire)
    {
        return $this->render('BtobSiminaireBundle:Default:detail.html.twig', array('entry' => $Siminaire));
    }

    public function detailreservationsiminaireAction(Reservationsim $Reservationsim)
    {
        return $this->render('BtobSiminaireBundle:Default:detailreservation.html.twig', array('entry' => $Reservationsim));
    }

    public function listreservationsiminaireAction()
    {


        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();


        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Reservationsim")->findAll();

        }else{
            $entities =$this->getDoctrine()->getRepository("BtobSiminaireBundle:Reservationsim")->findBy(array('agent' => $user));

        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobSiminaireBundle:Default:listreservation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));

    }


    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Reservationsim")->find($id);
       // echo $entities->getId();exit;
        $em->remove($entities);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_siminaire_list_reservation_homepage'));
    }

    public function reservationsiminaireAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $entities = $this->getDoctrine()->getRepository("BtobSiminaireBundle:Siminaire")->find($id);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $cin = $post["cin"];
            // echo $defaultd .'/'.$defaulta ;exit;
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $capacite = $request->request->get('capacite');
                $typesalle = $request->request->get('typesalle');
                $nbrsalle = $request->request->get('nbrsalle');
                $nbrchambre = $request->request->get('nbrchambre');
                $categorie = $request->request->get('categorie');
                $gala = $request->request->get('gala');
                $incentive = $request->request->get('incentive');
                $visites = $request->request->get('visites');
                $rallye = $request->request->get('rallye');
                $remarque = $request->request->get('remarque');

                $reservation = new Reservationsim();
                $reservation->setAgent($this->get('security.context')->getToken()->getUser());
                $reservation->setClient($client);
                $reservation->setSiminaire($entities);
                $reservation->setCapacite($capacite);
                $reservation->setTypesalle($typesalle);
                $reservation->setNbrsalle($nbrsalle);
                $reservation->setCategorie($categorie);
                $reservation->setEtat(0);
                $reservation->setGala($gala);
                $reservation->setIncentive($incentive);
                $reservation->setVisites($visites);
                $reservation->setRallye($rallye);
                $reservation->setRemarque($remarque);
                $reservation->setNbrchambres($nbrchambre);
                $em->persist($reservation);
                $em->flush();
                $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client);
                $request->getSession()->getFlashBag()->add('notiSiminaire', 'Votre message a bien été envoyé. Merci.');

                return $this->render('BtobSiminaireBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));


            }
        }

        return $this->render('BtobSiminaireBundle:Default:reservation.html.twig', array('entry' => $entities, 'form' => $form->createView()));
    }




}
