<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationsim
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\ReservationsimRepository")
 */
class Reservationsim
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationsim")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */

    protected $agent;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationsim")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */

    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Siminaire", inversedBy="reservationsim")
     * @ORM\JoinColumn(name="Siminaire_id", referencedColumnName="id",onDelete="CASCADE")
     */

    protected $siminaire;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbparticipant", type="integer", nullable=true)
     */
    private $nbparticipant;
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchsingle", type="integer", nullable=true)
     */
    private $nbchsingle;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchdouble", type="integer", nullable=true)
     */
    private $nbchdouble;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchtriple", type="integer", nullable=true)
     */
    private $nbchtriple;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbsalle", type="integer", nullable=true)
     */
    private $nbsalle;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbchquadtriple", type="integer", nullable=true)
     */
    private $nbchquadtriple;
    
    
    

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text", nullable=true)
     */
    private $remarque;

    

    /**
     * @var boolean
     *
     * @ORM\Column(name="incentive", type="boolean", nullable=true)
     */
    private $incentive;
   

    /**
     * @var boolean
     *
     * @ORM\Column(name="gala", type="boolean", nullable=true)
     */
    private $gala;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;


   /**
     * @var string
     *
     *
     * @ORM\Column(name="arrangement", type="string", length=255 , nullable=true)
     */
    private $arrangement;
    
    
    /**
     * @var string
     *
     *
     * @ORM\Column(name="lieu", type="string", length=255 , nullable=true)
     */
    private $lieu;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date", nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="date", nullable=true)
     */
    private $dater;
    
    
     /**

     * @ORM\OneToMany(targetEntity="Tsalle", mappedBy="reservationsim")

     */

    protected $reservationsim;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    

    /**
     * Set nbsalle
     *
     * @param integer $nbsalle
     * @return Reservationsim
     */
    public function setNbsalle($nbsalle)
    {
        $this->nbsalle = $nbsalle;

        return $this;
    }

    /**
     * Get nbsalle
     *
     * @return integer 
     */
    public function getNbsalle()
    {
        return $this->nbsalle;
    }

    


    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Reservationsim
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Reservationsim
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    

    /**
     * Set incentive
     *
     * @param boolean $incentive
     * @return Reservationsim
     */
    public function setIncentive($incentive)
    {
        $this->incentive = $incentive;

        return $this;
    }

    /**
     * Get incentive
     *
     * @return boolean 
     */
    public function getIncentive()
    {
        return $this->incentive;
    }

    /**
     * Set gala
     *
     * @param boolean $gala
     * @return Reservationsim
     */
    public function setGala($gala)
    {
        $this->gala = $gala;

        return $this;
    }

    /**
     * Get gala
     *
     * @return boolean 
     */
    public function getGala()
    {
        return $this->gala;
    }

    


    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Reservationsim
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationsim
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {

        $this->dcr=new \DateTime();
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set siminaire
     *
     * @param \Btob\SiminaireBundle\Entity\Siminaire $siminaire
     * @return Reservationsim
     */
    public function setSiminaire(\Btob\SiminaireBundle\Entity\Siminaire $siminaire = null)
    {
        $this->siminaire = $siminaire;

        return $this;
    }

    /**
     * Get siminaire
     *
     * @return \Btob\SiminaireBundle\Entity\Siminaire 
     */
    public function getSiminaire()
    {
        return $this->siminaire;
    }

   

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationsim
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }
    
    
    /**
     * Set nbparticipant
     *
     * @param integer $nbparticipant
     * @return Reservationsim
     */
    public function setNbparticipant($nbparticipant)
    {
        $this->nbparticipant = $nbparticipant;

        return $this;
    }

    /**
     * Get nbparticipant
     *
     * @return integer 
     */
    public function getNbparticipant()
    {
        return $this->nbparticipant;
    }
    
    
    
     /**
     * Set nbchsingle
     *
     * @param integer $nbchsingle
     * @return Reservationsim
     */
    public function setNbchsingle($nbchsingle)
    {
        $this->nbchsingle = $nbchsingle;

        return $this;
    }

    /**
     * Get nbchsingle
     *
     * @return integer 
     */
    public function getNbchsingle()
    {
        return $this->nbchsingle;
    }
    
    
     /**
     * Set nbchdouble
     *
     * @param integer $nbchdouble
     * @return Reservationsim
     */
    public function setNbchdouble($nbchdouble)
    {
        $this->nbchdouble = $nbchdouble;

        return $this;
    }

    /**
     * Get nbchdouble
     *
     * @return integer 
     */
    public function getNbchdouble()
    {
        return $this->nbchdouble;
    }
    
    
     /**
     * Set nbchtriple
     *
     * @param integer $nbchtriple
     * @return Reservationsim
     */
    public function setNbchtriple($nbchtriple)
    {
        $this->nbchtriple = $nbchtriple;

        return $this;
    }

    /**
     * Get nbchtriple
     *
     * @return integer 
     */
    public function getNbchtriple()
    {
        return $this->nbchtriple;
    }
    
    
     /**
     * Set nbchquadtriple
     *
     * @param integer $nbchquadtriple
     * @return Reservationsim
     */
    public function setNbchquadtriple($nbchquadtriple)
    {
        $this->nbchquadtriple = $nbchquadtriple;

        return $this;
    }

    /**
     * Get nbchquadriple
     *
     * @return integer 
     */
    public function getNbchquadtriple()
    {
        return $this->nbchquadtriple;
    }
    
    
     /**
     * Set arrangement 
     *
     * @param string $arrangement
     * @return Reservationsim
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return string 
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }
    
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Reservationsim
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Reservationsim
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }

    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }
    
    
    
    
    /**
     * Add reservationsim
     *
     * @param \Btob\SiminaireBundle\Entity\Tsalle $reservationsim
     * @return Reservationsim
     */

    public function addReservationsim(\Btob\SiminaireBundle\Entity\Tsalle $reservationsim)

    {

        $this->reservationsim[] = $reservationsim;



        return $this;

    }



    /**

     * Remove reservationsim

     *

     * @param \Btob\SiminaireBundle\Entity\Tsalle $reservationsim

     */

    public function removeReservationsim(\Btob\SiminaireBundle\Entity\Tsalle $reservationsim)

    {

        $this->reservationsim->removeElement($reservationsim);

    }



    /**

     * Get reservationsim

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getReservationsim()

    {

        return $this->reservationsim;

    }
  
    
     /**
     * Set lieu 
     *
     * @param string $lieu
     * @return Reservationsim
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }
}
