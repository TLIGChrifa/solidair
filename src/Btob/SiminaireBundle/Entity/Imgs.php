<?php

namespace Btob\SiminaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imgs
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\SiminaireBundle\Entity\ImgsRepository")
 */
class Imgs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;
    /**
     * @ORM\ManyToOne(targetEntity="Siminaire", inversedBy="imgs")
     * @ORM\JoinColumn(name="Siminaire_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $siminaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Imgs
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Imgs
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set siminaire
     *
     * @param \Btob\SiminaireBundle\Entity\Siminaire $siminaire
     * @return Imgs
     */
    public function setSiminaire(\Btob\SiminaireBundle\Entity\Siminaire $siminaire = null)
    {
        $this->siminaire = $siminaire;

        return $this;
    }
    public  function  __construct(){
        $this->dcr = new \DateTime();

    }
    /**
     * Get siminaire
     *
     * @return \Btob\SiminaireBundle\Entity\Siminaire 
     */
    public function getSiminaire()
    {
        return $this->siminaire;
    }
}
