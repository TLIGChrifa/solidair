<?php

namespace Btob\ParcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resapa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ParcBundle\Entity\ResapaRepository")
 */
class Resapa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var integer
     *
     * @ORM\Column(name="adulte", type="integer" , nullable=true)
     */
    private $adulte;

    /**
     * @var integer
     *
     * @ORM\Column(name="enfant", type="integer" , nullable=true)
     */
    private $enfant;

    /**
     * @var integer
     *
     * @ORM\Column(name="bebe", type="integer" , nullable=true)
     */
    private $bebe;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text" , nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="resapa")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="resapa")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="Parc", inversedBy="resapa")
     * @ORM\JoinColumn(name="parc_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $parc;
    /**
     * construct
     */
    public function __construct(){
        $this->dcr=new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Resapa
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Resapa
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set adulte
     *
     * @param integer $adulte
     * @return Resapa
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return integer 
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set enfant
     *
     * @param integer $enfant
     * @return Resapa
     */
    public function setEnfant($enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get enfant
     *
     * @return integer 
     */
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set bebe
     *
     * @param integer $bebe
     * @return Resapa
     */
    public function setBebe($bebe)
    {
        $this->bebe = $bebe;

        return $this;
    }

    /**
     * Get bebe
     *
     * @return integer 
     */
    public function getBebe()
    {
        return $this->bebe;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Resapa
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Resapa
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Resapa
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set parc
     *
     * @param \Btob\ParcBundle\Entity\Parc $parc
     * @return Resapa
     */
    public function setParc(\Btob\ParcBundle\Entity\Parc $parc = null)
    {
        $this->parc = $parc;

        return $this;
    }

    /**
     * Get parc
     *
     * @return \Btob\ParcBundle\Entity\Parc 
     */
    public function getParc()
    {
        return $this->parc;
    }
}
