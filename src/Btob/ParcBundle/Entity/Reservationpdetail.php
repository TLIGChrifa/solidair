<?php

namespace Btob\ParcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationpdetail
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ParcBundle\Entity\ReservationpdetailRepository")
 */
class Reservationpdetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text
     *
     * @ORM\Column(name="chambre", type="text")
     */
    protected $chambre;
    
    /**
     * @ORM\ManyToOne(targetEntity="Reservationparc", inversedBy="Reservationpdetail")
     * @ORM\JoinColumn(name="reservationparc_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationparc;
    
    
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="namead", type="string",length=50 ,nullable=true)
     */
    private $namead;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomad", type="string",length=50 ,nullable=true)
     */
    private $prenomad;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="agead", type="string",length=50 ,nullable=true)
     */
    private $agead;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppad", type="text" ,nullable=true)
     */
    private $suppad;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="reducad", type="text" ,nullable=true)
     */
    private $reducad; 
    
    /**
     * @var string
     *
     * @ORM\Column(name="nameenf", type="string" ,length=50 ,nullable=true)
     */
    private $nameenf;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomenf", type="string" ,length=50 ,nullable=true)
     */
    private $prenomenf;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenf", type="string" ,length=50 ,nullable=true)
     */
    private $ageenf;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppenf", type="text" ,nullable=true)
     */
    private $suppenf;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="reducenf", type="text" ,nullable=true)
     */
    private $reducenf;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="nameb", type="string" ,length=50 ,nullable=true)
     */
    private $nameb;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomb", type="string" ,length=50 ,nullable=true)
     */
    private $prenomb;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageb", type="string" ,length=50 ,nullable=true)
     */
    private $ageb;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="suppb", type="text", nullable=true)
     */
    private $suppb;
    

    /**
     * @var array
     *
     * @ORM\Column(name="reducb", type="text", nullable=true)
     */
    private $reducb;  
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set chambre
     *
     * @param string $chambre
     * @return Reservationpdetail
     */
    public function setChambre($chambre)
    {
        $this->chambre = $chambre;

        return $this;
    }

    /**
     * Get chambre
     *
     * @return string 
     */
    public function getChambre()
    {
        return $this->chambre;
    }



    /**
     * Set reservationparc
     *
     * @param \Btob\ParcBundle\Entity\Reservationparc $reservationparc
     * @return Reservationpdetail
     */
    public function setReservationparc(\Btob\ParcBundle\Entity\Reservationparc $reservationparc = null)
    {
        $this->reservationparc = $reservationparc;

        return $this;
    }

    /**
     * Get reservationparc
     *
     * @return \Btob\ParcBundle\Entity\Reservationparc
     */
    public function getReservationparc()
    {
        return $this->reservationparc;
    }
    
    
    
    /**
     * Set namead
     *
     * @param string $namead
     * @return Reservationpdetail
     */
    public function setNamead($namead)
    {
        $this->namead = $namead;

        return $this;
    }

    /**
     * Get namead
     *
     * @return string 
     */
    public function getNamead()
    {
        return $this->namead;
    }
    
    
    /**
     * Set prenomad
     *
     * @param string $prenomad
     * @return Reservationpdetail
     */
    public function setPrenomad($prenomad)
    {
        $this->prenomad = $prenomad;

        return $this;
    }

    /**
     * Get prenomad
     *
     * @return string 
     */
    public function getPrenomad()
    {
        return $this->prenomad;
    }    
    
    
    /**
     * Set agead
     *
     * @param string $agead
     * @return Reservationpdetail
     */
    public function setAgead($agead)
    {
        $this->agead = $agead;

        return $this;
    }

    /**
     * Get agead
     *
     * @return string 
     */
    public function getAgead()
    {
        return $this->agead;
    }
    

    /**
     * Set suppad
     *
     * @param string $suppad
     * @return Reservationpdetail
     */
    public function setSuppad($suppad)
    {
        $this->suppad = $suppad;

        return $this;
    }

    /**
     * Get suppad
     *
     * @return string 
     */
    public function getSuppad()
    {
        return $this->suppad;
    }    
    
 
    
    /**
     * Set reducad
     *
     * @param string $reducad
     * @return Reservationpdetail
     */
    public function setReducad($reducad)
    {
        $this->reducad = $reducad;

        return $this;
    }

    /**
     * Get reducad
     *
     * @return string 
     */
    public function getReducad()
    {
        return $this->reducad;
    }    
        
    
    
    
    /**
     * Set nameenf
     *
     * @param string $nameenf
     * @return Reservationpdetail
     */
    public function setNameenf($nameenf)
    {
        $this->nameenf = $nameenf;

        return $this;
    }

    /**
     * Get nameenf
     *
     * @return string 
     */
    public function getNameenf()
    {
        return $this->nameenf;
    }
    
    
    /**
     * Set prenomenf
     *
     * @param string $prenomenf
     * @return Reservationpdetail
     */
    public function setPrenomenf($prenomenf)
    {
        $this->prenomenf = $prenomenf;

        return $this;
    }

    /**
     * Get prenomenf
     *
     * @return string 
     */
    public function getPrenomenf()
    {
        return $this->prenomenf;
    }  
    
    
    /**
     * Set ageenf
     *
     * @param string $ageenf
     * @return Reservationpdetail
     */
    public function setAgeenf($ageenf)
    {
        $this->ageenf = $ageenf;

        return $this;
    }

    /**
     * Get ageenf
     *
     * @return string 
     */
    public function getAgeenf()
    {
        return $this->ageenf;
    }    
    

    /**
     * Set suppenf
     *
     * @param string $suppenf
     * @return Reservationpdetail
     */
    public function setSuppenf($suppenf)
    {
        $this->suppenf = $suppenf;

        return $this;
    }

    /**
     * Get suppenf
     *
     * @return string 
     */
    public function getSuppenf()
    {
        return $this->suppenf;
    }        
    

    /**
     * Set reducenf
     *
     * @param string $reducenf
     * @return Reservationpdetail
     */
    public function setReducenf($reducenf)
    {
        $this->reducenf = $reducenf;

        return $this;
    }

    /**
     * Get reducenf
     *
     * @return string 
     */
    public function getReducenf()
    {
        return $this->reducenf;
    }            
    
    
    
    /**
     * Set nameb
     *
     * @param string $nameb
     * @return Reservationpdetail
     */
    public function setNameb($nameb)
    {
        $this->nameb = $nameb;

        return $this;
    }

    /**
     * Get nameb
     *
     * @return string 
     */
    public function getNameb()
    {
        return $this->nameb;
    } 
    
    /**
     * Set prenomb
     *
     * @param string $prenomb
     * @return Reservationpdetail
     */
    public function setPrenomb($prenomb)
    {
        $this->prenomb = $prenomb;

        return $this;
    }

    /**
     * Get prenomb
     *
     * @return string 
     */
    public function getPrenomb()
    {
        return $this->prenomb;
    }            
    
    /**
     * Set ageb
     *
     * @param string $ageb
     * @return Reservationpdetail
     */
    public function setAgeb($ageb)
    {
        $this->ageb = $ageb;

        return $this;
    }

    /**
     * Get ageb
     *
     * @return string 
     */
    public function getAgeb()
    {
        return $this->ageb;
    }
    
    /**
     * Set suppb
     *
     * @param string $suppb
     * @return Reservationpdetail
     */
    public function setSuppb($suppb)
    {
        $this->suppb = $suppb;

        return $this;
    }

    /**
     * Get suppb
     *
     * @return string 
     */
    public function getSuppb()
    {
        return $this->suppb;
    }            
    
    

     /**
     * Set reducb
     *
     * @param string $reducb
     * @return Reservationpdetail
     */
    public function setReducb($reducb)
    {
        $this->reducb = $reducb;

        return $this;
    }

    /**
     * Get reducb
     *
     * @return string 
     */
    public function getReducb()
    {
        return $this->reducb;
    }            
       
    
    
}
