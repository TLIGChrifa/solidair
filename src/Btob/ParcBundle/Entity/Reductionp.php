<?php

namespace Btob\ParcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reductionp
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ParcBundle\Entity\ReductionpRepository")
 */
class Reductionp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float" , nullable=true)
     */
    private $price;

    
    /**
     * @ORM\ManyToOne(targetEntity="Parc", inversedBy="reductionp")
     * @ORM\JoinColumn(name="parc_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $parc;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set name
     *
     * @param string $name
     * @return Reductionp
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Reductionp
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
   

    /**
     * Set parc
     *
     * @param \Btob\ParcBundle\Entity\Parc $parc
     * @return Reductionp
     */
    public function setParc(\Btob\ParcBundle\Entity\Parc $parc = null)
    {
        $this->parc = $parc;

        return $this;
    }

    /**
     * Get parc
     *
     * @return \Btob\ParcBundle\Entity\Parc 
     */
    public function getParc()
    {
        return $this->parc;
    }
}
