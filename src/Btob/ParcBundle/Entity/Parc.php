<?php







namespace Btob\ParcBundle\Entity;







use Doctrine\ORM\Mapping as ORM;







/**



 * Parc



 *



 * @ORM\Table()



 * @ORM\Entity(repositoryClass="Btob\ParcBundle\Entity\ParcRepository")



 */



class Parc



{



    /**



     * @var integer



     *



     * @ORM\Column(name="id", type="integer")



     * @ORM\Id



     * @ORM\GeneratedValue(strategy="AUTO")



     */



    private $id;







    /**



     * @var string



     *



     * @ORM\Column(name="libelle", type="string", length=255 , nullable=true)



     */



    private $libelle;







    /**



     * @var date



     *



     * @ORM\Column(name="dcr", type="date")



     */



    private $dcr;







    /**



     * @var string



     *



     * @ORM\Column(name="ville", type="string", length=255 , nullable=true)



     */



    private $ville;









    /**



     * @var float



     *



     * @ORM\Column(name="prix", type="float" , nullable=true)



     */



    private $prix;



    /**



     * @var float



     *



     * @ORM\Column(name="prixavance", type="float" , nullable=true)



     */



    private $prixavance;    




    /**



     * @var boolean



     *



     * @ORM\Column(name="apartir", type="boolean" , nullable=true)



     */



    private $apartir;



    /**



     * @var boolean



     *



     * @ORM\Column(name="active", type="boolean" , nullable=true)



     */



    private $active;




    /**



     * @ORM\OneToMany(targetEntity="Imgparc", mappedBy="parc")



     */



    protected $imgparc;



    /**



     * @var string



     *



     * @ORM\Column(name="description", type="text" , nullable=true)



     */



    private $description;

     /**
     * @ORM\OneToMany(targetEntity="Supplementp", mappedBy="parc", cascade={"remove"})
     */
    protected $supplementp;
    

     /**
     * @ORM\OneToMany(targetEntity="Reductionp", mappedBy="parc", cascade={"remove"})
     */
    protected $reductionp;    
    
    
     /**
     * @ORM\OneToMany(targetEntity="Parcprice", mappedBy="parc", cascade={"remove"})
     */
    protected $parcprice;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="ageenfmin", type="string", length=255 , nullable=true)
     */

    private $ageenfmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ageenfmax", type="string", length=255 , nullable=true)
     */

    private $ageenfmax;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="agebmin", type="string", length=255 , nullable=true)
     */

    private $agebmin;
    
    /**
     * @var string
     *
     * @ORM\Column(name="agebmax", type="string", length=255 , nullable=true)
     */

    private $agebmax;

    /**
     * @var float
     *
     * @ORM\Column(name="marge", type="float" , nullable=true)
     */

    private $marge; 

    public  function  __construct(){



        $this->dcr = new \DateTime();



        $this->imgparc =new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplementp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reductionp = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parcprice = new \Doctrine\Common\Collections\ArrayCollection();



    }







    /**



     * Get id



     *



     * @return integer 



     */



    public function getId()



    {



        return $this->id;



    }


    /**



     * Set libelle



     *



     * @param string $libelle



     * @return Parc



     */



    public function setLibelle($libelle)



    {



        $this->libelle = $libelle;







        return $this;



    }







    /**



     * Get libelle



     *



     * @return string 



     */



    public function getLibelle()



    {



        return $this->libelle;



    }




    /**



     * Set ville



     *



     * @param string $ville



     * @return Parc



     */



    public function setVille($ville)



    {



        $this->ville = $ville;







        return $this;



    }







    /**



     * Get ville



     *



     * @return string 



     */



    public function getVille()



    {



        return $this->ville;



    }





    /**



     * Set prix



     *



     * @param float $prix



     * @return Parc



     */



    public function setPrix($prix)



    {



        $this->prix = $prix;







        return $this;



    }







    /**



     * Get prix



     *



     * @return float 



     */



    public function getPrix()



    {



        return $this->prix;



    }


    /**



     * Set description



     *



     * @param string $description



     * @return Parc



     */



    public function setDescription($description)



    {



        $this->description = $description;







        return $this;



    }







    /**



     * Get description



     *



     * @return string 



     */



    public function getDescription()



    {



        return $this->description;



    }







    /**



     * Set dcr



     *



     * @param \DateTime $dcr



     * @return Parc



     */



    public function setDcr($dcr)



    {



        $this->dcr = $dcr;







        return $this;



    }







    /**



     * Get dcr



     *



     * @return \DateTime 



     */



    public function getDcr()



    {



        return $this->dcr;



    }






    /**



     * Set apartir



     *



     * @param boolean $apartir



     * @return Parc



     */



    public function setApartir($apartir)



    {



        $this->apartir = $apartir;







        return $this;



    }







    /**



     * Get apartir



     *



     * @return boolean 



     */



    public function getApartir()



    {



        return $this->apartir;



    }







    /**



     * Add imgparc



     *



     * @param \Btob\ParcBundle\Entity\Imgparc $imgparc



     * @return Parc



     */



    public function addImgparc(\Btob\ParcBundle\Entity\Imgparc $imgparc)



    {



        $this->imgparc[] = $imgparc;







        return $this;



    }







    /**



     * Remove imgparc



     *



     * @param \Btob\ParcBundle\Entity\Imgparc $imgparc



     */



    public function removeImgparc(\Btob\ParcBundle\Entity\Imgparc $imgparc)



    {



        $this->imgparc->removeElement($imgparc);



    }







    /**



     * Get imgparc



     *



     * @return \Doctrine\Common\Collections\Collection 



     */



    public function getImgparc()



    {



        return $this->imgparc;



    }







    /**



     * Set active



     *



     * @param boolean $active



     * @return Parc



     */



    public function setActive($active)



    {



        $this->active = $active;







        return $this;



    }







    /**



     * Get active



     *



     * @return boolean 



     */



    public function getActive()



    {



        return $this->active;



    }


     /**
     * Add supplementp
     *
     * @param \Btob\ParcBundle\Entity\Supplementp $supplementp
     * @return Parc
     */
    public function addSupplementp(\Btob\ParcBundle\Entity\Supplementp $supplementp)
    {
        $this->supplementp[] = $supplementp;

        return $this;
    }

    /**
     * Remove supplementp
     *
     * @param \Btob\ParcBundle\Entity\Supplementp $supplementp
     */
    public function removeSupplementp(\Btob\ParcBundle\Entity\Supplementp $supplementp)
    {
        $this->supplementp->removeElement($supplementp);
    }

    /**
     * Get supplementp
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplementp()
    {
        return $this->supplementp;
    }
  
    
     /**
     * Add parcprice
     *
     * @param \Btob\ParcBundle\Entity\Parcprice $parcprice
     * @return Parc
     */
    public function addParcprice(\Btob\ParcBundle\Entity\Parcprice $parcprice)
    {
        $this->parcprice[] = $parcprice;

        return $this;
    }

    /**
     * Remove parcprice
     *
     * @param \Btob\ParcBundle\Entity\Parcprice $parcprice
     */
    public function removeParcprice(\Btob\ParcBundle\Entity\Parcprice $parcprice)
    {
        $this->parcprice->removeElement($parcprice);
    }

    /**
     * Get Parcprice
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParcprice()
    {
        return $this->parcprice;
    }   
    
    
    
     /**
     * Add reductionp
     *
     * @param \Btob\ParcBundle\Entity\Reductionp $reductionp
     * @return Parc
     */
    public function addReductionp(\Btob\ParcBundle\Entity\Reductionp $reductionp)
    {
        $this->reductionp[] = $reductionp;

        return $this;
    }

    /**
     * Remove reductionp
     *
     * @param \Btob\ParcBundle\Entity\Reductionp $reductionp
     */
    public function removeReductionp(\Btob\ParcBundle\Entity\Reductionp $reductionp)
    {
        $this->reductionp->removeElement($reductionp);
    }

    /**
     * Get reductionp
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReductionp()
    {
        return $this->reductionp;
    } 
    
    
    
    /**
     * Set prixavance
     *
     * @param float $prixavance
     * @return Parc
     */

    public function setPrixavance($prixavance)
    {
        $this->prixavance = $prixavance;
        return $this;
    }

    /**
     * Get prixavance
     *
     * @return float 
     */

    public function getPrixavance()
    {
        return $this->prixavance;

    }    
    
    
       
     /**
     * Set ageenfmin
     *
     * @param string $ageenfmin
     * @return Parc
     */

    public function setAgeenfmin($ageenfmin)

    {
        $this->ageenfmin = $ageenfmin;
        return $this;

    }



    /**
     * Get ageenfmin
     *
     * @return string 
     */

    public function getAgeenfmin()

    {
        return $this->ageenfmin;

    }
    
    
     /**
     * Set ageenfmax
     *
     * @param string $ageenfmax
     * @return Parc
     */

    public function setAgeenfmax($ageenfmax)

    {
        $this->ageenfmax = $ageenfmax;
        return $this;

    }



    /**
     * Get ageenfmax
     *
     * @return string 
     */

    public function getAgeenfmax()

    {
        return $this->ageenfmax;

    }
    
    
    
    
    
    
     /**
     * Set agebmin
     *
     * @param string $agebmin
     * @return Parc
     */

    public function setAgebmin($agebmin)

    {
        $this->agebmin = $agebmin;
        return $this;

    }



    /**
     * Get agebmin
     *
     * @return string 
     */

    public function getAgebmin()

    {
        return $this->agebmin;

    }
    
    
     /**
     * Set agebmax
     *
     * @param string $agebmax
     * @return Parc
     */

    public function setAgebmax($agebmax)

    {
        $this->agebmax = $agebmax;
        return $this;

    }



    /**
     * Get agebmax
     *
     * @return string 
     */

    public function getAgebmax()

    {
        return $this->agebmax;

    }
    
    /**
     * Set marge
     *
     * @param float $marge
     * @return Parc
     */

    public function setMarge($marge)
    {
        $this->marge = $marge;
        return $this;
    }

    /**
     * Get marge
     *
     * @return float 
     */

    public function getMarge()
    {
        return $this->marge;

    }        
   


}



