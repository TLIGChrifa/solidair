<?php

namespace Btob\ParcBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ReductionpRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReductionpRepository extends EntityRepository
{
     public function findReductionByParc($parcid)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.parc', 'u')
            ->andWhere('u.id =:id')
            ->setParameter('id', $parcid);
        return $query->getQuery()->getResult();
    }
    
}
