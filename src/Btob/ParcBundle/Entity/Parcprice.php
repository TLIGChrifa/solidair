<?php

namespace Btob\ParcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parcprice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\ParcBundle\Entity\ParcpriceRepository")
 */
class Parcprice {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="pricead", type="float" , nullable=true)
     */
    private $pricead;

     /**
     * @var float
     *
     * @ORM\Column(name="priceenf", type="float" , nullable=true)
     */
    private $priceenf;

     /**
     * @var float
     *
     * @ORM\Column(name="priceb", type="float" , nullable=true)
     */
    private $priceb;   
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="date" , nullable=true)
     */
    private $dated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dates", type="date" , nullable=true)
     */
    private $dates;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dmj", type="datetime", nullable=true)
     */
    private $dmj;

    /**
     * @ORM\ManyToOne(targetEntity="Parc", inversedBy="supplementp")
     * @ORM\JoinColumn(name="parc_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $parc;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricead
     *
     * @param float $pricead
     * @return Parcprice
     */
    public function setPricead($pricead)
    {
        $this->pricead = $pricead;

        return $this;
    }

    /**
     * Get pricead
     *
     * @return float 
     */
    public function getPricead()
    {
        return $this->pricead;
    }

    /**
     * Set priceenf
     *
     * @param float $priceenf
     * @return Parcprice
     */
    public function setPriceenf($priceenf)
    {
        $this->priceenf = $priceenf;

        return $this;
    }

    /**
     * Get priceenf
     *
     * @return float 
     */
    public function getPriceenf()
    {
        return $this->priceenf;
    }   
    
    
    /**
     * Set priceb
     *
     * @param float $priceb
     * @return Parcprice
     */
    public function setPriceb($priceb)
    {
        $this->priceb = $priceb;

        return $this;
    }

    /**
     * Get priceb
     *
     * @return float 
     */
    public function getPriceb()
    {
        return $this->priceb;
    } 
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Parcprice
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }


    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Parcprice
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set dmj
     *
     * @param \DateTime $dmj
     * @return Parcprice
     */
    public function setDmj($dmj)
    {
        $this->dmj = $dmj;

        return $this;
    }

    /**
     * Get dmj
     *
     * @return \DateTime 
     */
    public function getDmj()
    {
        return $this->dmj;
    }

    /**
     * Set parc
     *
     * @param \Btob\ParcBundle\Entity\Parc $parc
     * @return Parcprice
     */
    public function setParc(\Btob\ParcBundle\Entity\Parc $parc = null)
    {
        $this->parc = $parc;

        return $this;
    }

    /**
     * Get parc
     *
     * @return \Btob\ParcBundle\Entity\Parc 
     */
    public function getParc()
    {
        return $this->parc;
    }
    /**
     * Set dates
     *
     * @param \DateTime $dates
     * @return Parcprice
     */
    public function setDates($dates)
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * Get dates
     *
     * @return \DateTime 
     */
    public function getDates()
    {
        return $this->dates;
    }
}
