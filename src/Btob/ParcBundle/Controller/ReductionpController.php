<?php

namespace Btob\ParcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ParcBundle\Entity\Reductionp;
use Symfony\Component\HttpFoundation\Request;
use Btob\ParcBundle\Form\ReductionpType;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReductionpController extends Controller {

    public function indexAction($parcid) {
        $parc = $this->getDoctrine()
                ->getRepository('BtobParcBundle:Parc')
                ->find($parcid);
        return $this->render('BtobParcBundle:Reductionp:index.html.twig', array('entities' => $parc->getReductionp(), "parcid" => $parcid));
    }

    public function addAction($parcid) {
        $parc = $this->getDoctrine()->getRepository('BtobParcBundle:Parc')->find($parcid);
        $Reductionp = new Reductionp();
        $form = $this->createForm(new ReductionpType(), $Reductionp);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Reductionp->setParc($parc);
                $em->persist($Reductionp);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_reductionp_homepage', array("parcid" => $parcid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobParcBundle:Reductionp:form.html.twig', array('form' => $form->createView(), "parcid" => $parcid));
    }

    public function editAction($id, $parcid) {
        $request = $this->get('request');
        $Reductionp = $this->getDoctrine()
                ->getRepository('BtobParcBundle:Reductionp')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReductionpType(), $Reductionp);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_reductionp_homepage', array("parcid" => $parcid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobParcBundle:Reductionp:form.html.twig', array('form' => $form->createView(), 'id' => $id, "parcid" => $parcid)
        );
    }

    public function deleteAction(Reductionp $Reductionp, $parcid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Reductionp) {
            throw new NotFoundHttpException("Reductionp non trouvée");
        }
        $em->remove($Reductionp);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reductionp_homepage', array("parcid" => $parcid)));
    }

}
