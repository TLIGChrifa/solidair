<?php

namespace Btob\ParcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ParcBundle\Entity\Parcprice;
use Symfony\Component\HttpFoundation\Request;
use Btob\ParcBundle\Form\ParcpriceType;
use Symfony\Component\HttpFoundation\JsonResponse;



class ParcpriceController extends Controller
{

    public function indexAction($parcid)
    {
        $parc = $this->getDoctrine()
            ->getRepository('BtobParcBundle:Parc')
            ->find($parcid);
	    $name =  $parc->getLibelle();
        $price = array();
        foreach ($parc->getParcprice() as $value) {
           
                $price[] = $value;
           
        }
        return $this->render('BtobParcBundle:Parcprice:index.html.twig', array(
            "entities" => $price,
            "parcid" => $parcid,
	    "name" => $name,
        ));
    }


    public function addAction($parcid)
    {
        $parc = $this->getDoctrine()->getRepository('BtobParcBundle:Parc')->find($parcid);
      
        $allparcprice = $this->getDoctrine()->getRepository('BtobParcBundle:Parcprice')->findBy(array('parc' => $parc));
      
        

        $parcprice = new Parcprice();
        $form = $this->createForm(new ParcpriceType(), $parcprice);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);


            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $parcprice->setParc($parc);
                $em->persist($parcprice);
                $em->flush();
               
             
              
                   

                return $this->redirect($this->generateUrl('btob_parcprice_homepage', array("parcid" => $parcid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobParcBundle:Parcprice:form.html.twig', array(
            'form' => $form->createView(),
            "parc" => $parc

        ));
    }



    public function editAction($id, $parcid)
    {
        $parc = $this->getDoctrine()->getRepository('BtobParcBundle:Parc')->find($parcid);
        $request = $this->get('request');
        $parcprice = $this->getDoctrine()
            ->getRepository('BtobParcBundle:Parcprice')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ParcpriceType(), $parcprice);
        //Tools::dump($form->getData());
        $form->handleRequest($request);
//************** added by hamza **************
      
        $allparcprice = $this->getDoctrine()->getRepository('BtobParcBundle:Parcprice')->findBy(array('parc' => $parc));
      
    
       
        if ($form->isValid()) {

            $parcprice->setDmj(new \DateTime());
            $em->flush();


            return $this->redirect($this->generateUrl('btob_parcprice_homepage', array("parcid" => $parcid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobParcBundle:Parcprice:edit.html.twig', array('form' => $form->createView(), 'price' => $parcprice, 'id' => $id, "parc" => $parc)
        );
    }

  
    public function deleteAction(Parcprice $parcprice, $parcid)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$parcprice) {
            throw new NotFoundHttpException("Parcprice non trouvée");
        }
        $em->remove($parcprice);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_parcprice_homepage', array("parcid" => $parcid)));
    }



}












