<?php

namespace Btob\ParcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\ParcBundle\Entity\Supplementp;
use Symfony\Component\HttpFoundation\Request;
use Btob\ParcBundle\Form\SupplementpType;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplementpController extends Controller {

    public function indexAction($parcid) {
        $parc = $this->getDoctrine()
                ->getRepository('BtobParcBundle:Parc')
                ->find($parcid);
        return $this->render('BtobParcBundle:Supplementp:index.html.twig', array('entities' => $parc->getSupplementp(), "parcid" => $parcid));
    }

    public function addAction($parcid) {
        $parc = $this->getDoctrine()->getRepository('BtobParcBundle:Parc')->find($parcid);
        $Supplementp = new Supplementp();
        $form = $this->createForm(new SupplementpType(), $Supplementp);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Supplementp->setParc($parc);
                $em->persist($Supplementp);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_supplementp_homepage', array("parcid" => $parcid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobParcBundle:Supplementp:form.html.twig', array('form' => $form->createView(), "parcid" => $parcid));
    }

    public function editAction($id, $parcid) {
        $request = $this->get('request');
        $Supplementp = $this->getDoctrine()
                ->getRepository('BtobParcBundle:Supplementp')
                ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SupplementpType(), $Supplementp);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('btob_supplementp_homepage', array("parcid" => $parcid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobParcBundle:Supplementp:form.html.twig', array('form' => $form->createView(), 'id' => $id, "parcid" => $parcid)
        );
    }

    public function deleteAction(Supplementp $Supplementp, $parcid) {
        $em = $this->getDoctrine()->getManager();

        if (!$Supplementp) {
            throw new NotFoundHttpException("Supplement non trouvée");
        }
        $em->remove($Supplementp);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_supplementp_homepage', array("parcid" => $parcid)));
    }

}
