<?php 

namespace Btob\ParcBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParcpriceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pricead',NULL, array('required' => true, 'label' => "Prix Adulte"))
            ->add('priceenf',NULL, array('required' => true, 'label' => "Prix Enfant"))//,'data' => '0'
            ->add('priceb',NULL, array('required' => true, 'label' => "Prix Bébé"))//,'data' => '0'
            ->add('dated', 'date', array('widget' => 'single_text','label' => "Date",'format' => 'dd/MM/yyyy'))
            ->add('dates', 'date', array('widget' => 'single_text','label' => "Date fin de période",'format' => 'dd/MM/yyyy'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\ParcBundle\Entity\Parcprice'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_parcbundle_parcprice';
    }
}
