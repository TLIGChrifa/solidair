<?php

namespace Btob\CuircuitBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Symfony\Component\Validator\Constraints\DateTime;
use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\CuircuitBundle\Entity\Personalisationcircuit;
use Btob\CuircuitBundle\Entity\Tpcircuit;
use Btob\CuircuitBundle\Entity\Reservationcdetail;
use Btob\CuircuitBundle\Form\PersonalisationcircuitType;
use Btob\CuircuitBundle\Form\PersonalisationcircuitpayerType;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        
        
        
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->listPrice();

       
       
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $circuitprices,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );        
        
       
        
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('BtobCuircuitBundle:Default:index.html.twig', array('entities' => $entities,'circuitprices' => $circuitprices, 'user' => $user));

    }
    
    
    
    

    public function detailAction(Cuircuit $circuit)
    {
        
       $circuitperiode = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());

        return $this->render('BtobCuircuitBundle:Default:detail.html.twig', array('entry' => $circuit,'periods' => $circuitperiode));
    }
    
    
    
    
   public function personalisationAction(Cuircuit $circuit)

    {

       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       
       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        if ($request->getMethod() == 'POST') {
            
            $circuitprice = $request->request->get("circuitprice");
            $session->set('circuitprice', $circuitprice);
            $nbad = $request->request->get("nbad");
            $session->set('nbad', $nbad);
            $nbbebe = $request->request->get("nbbebe");
            $session->set('nbbebe', $nbbebe);
            
            $nbenf = $request->request->get("nbenf");
            $session->set('nbenf', $nbenf);
            
           
            
          return $this->redirect($this->generateUrl('btob_inscrip_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
          
       
    return $this->render('BtobCuircuitBundle:Default:personalisation.html.twig', array('entry' => $circuit,'circuitprices' => $circuitprices));

    
    } 
    
    
    public function inscriptionAction(Cuircuit $circuit)

    {

       $em = $this->getDoctrine()->getManager();
       
       $circuitprices = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->findByCircuit($circuit->getId());
       $circuitsupp = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Supplementc")->findSupplementByCircuit($circuit->getId());
       
       $circuitreduc = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Reductionc")->findReductionByCircuit($circuit->getId());

       $request = $this->get('request');
        $session = $this->getRequest()->getSession();

        $request = $this->get('request');
        $circuitprice = $session->get("circuitprice");
        $nbad = $session->get("nbad");
        $nbchambre = count($nbad);
        $nbbebe = $session->get("nbbebe");
        $nbenf = $session->get("nbenf");
        
        $circuitpricesss = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Cuircuitprice")->find($circuitprice);
        
        $Array = array();
        $aujourdhuib = new \DateTime($circuitpricesss->getDated()->format("Y-m-d"));
       $nbjourbebe = intval($circuit->getAgebmax())*365;
       $aujourdhuib->modify('-'.$nbjourbebe.' day');
        $datedb = $aujourdhuib->format("d/m/Y");

        
        $aujourdhuienf = new \DateTime($circuitpricesss->getDated()->format("Y-m-d"));
       $nbjourenf = intval($circuit->getAgeenfmax())*365;
       $aujourdhuienf->modify('-'.$nbjourenf.' day');
        $datedenf = $aujourdhuienf->format("d/m/Y");
        
        if ($request->getMethod() == 'POST') {
            
             
            for($i=0;$i<$nbchambre;$i++)
            {
               
               //adult
                if(isset($request->request->get("namead")[$i]))
                {
                    $adad=array();
                 for($j=0;$j<count($request->request->get("namead")[$i]);$j++)
                 {
                $adad[$j]['namead']= $request->request->get("namead")[$i][$j];
                $adad[$j]['prenomad']= $request->request->get("prenomad")[$i][$j];
                $adad[$j]['agead']= $request->request->get("agead")[$i][$j];
                
                 if(isset($request->request->get("suppad")[$i][$j]))
                 {
                     $adad[$j]['suppad']= $request->request->get("suppad")[$i][$j];
                 }
                 else{
                   $adad[$j]['suppad']=null;  
                 }
                 
                 
                 if(isset($request->request->get("reducad")[$i][$j]))
                 {
                     $adad[$j]['reducad']= $request->request->get("reducad")[$i][$j];
                 }
                 else{
                   $adad[$j]['reducad']=null;  
                 }
                
                
                
                   
                 }
                
                 $Array[$i]['adult']=$adad;
                 
               
                }
                 //enf
                if(isset($request->request->get("nameenf")[$i]))
                {
                     $enf=array();
                 for($k=0;$k<count($request->request->get("nameenf")[$i]);$k++)
                 {
                $enf[$k]['nameenf']= $request->request->get("nameenf")[$i][$k];
                $enf[$k]['prenomenf']= $request->request->get("prenomenf")[$i][$k];
                $enf[$k]['ageenf']= $request->request->get("ageenf")[$i][$k];
                
                
               
                
                
                if(isset($request->request->get("suppenf")[$i][$k]))
                 {
                     $enf[$k]['suppenf']= $request->request->get("suppenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['suppenf']=null;  
                 }
                
                
                 
                 if(isset($request->request->get("reducenf")[$i][$k]))
                 {
                     $enf[$k]['reducenf']= $request->request->get("reducenf")[$i][$k];
                 }
                 else{
                   $enf[$k]['reducenf']=null;  
                 }
                 
                 }
                 
                 $Array[$i]['enf']=$enf;
                }
                
               
                
                //bebe
                if(isset($request->request->get("nameb")[$i]))
                {
                     $bebe=array();
                 for($l=0;$l<count($request->request->get("nameb")[$i]);$l++)
                 {
                $bebe[$l]['nameb']= $request->request->get("nameb")[$i][$l];
                $bebe[$l]['prenomb']= $request->request->get("prenomb")[$i][$l];
                $bebe[$l]['ageb']= $request->request->get("ageb")[$i][$l];
                
                
                if(isset($request->request->get("suppb")[$i][$l]))
                 {
                     $bebe[$l]['suppb']= $request->request->get("suppb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['suppb']=null;  
                 }
                 
                 if(isset($request->request->get("reducb")[$i][$l]))
                 {
                     $bebe[$l]['reducb']= $request->request->get("reducb")[$i][$l];
                 }
                 else{
                   $bebe[$l]['reducb']=null;  
                 }
                 
                 
                 }
                 
                 $Array[$i]['bebe']=$bebe;
                }
                
                
                 
                
                
                
                
            }
            
            $session->set('array', $Array);
             $session->set('circuitprice', $circuitprice);
            
          

          return $this->redirect($this->generateUrl('btob_resa_reservation_cuircuit', array('id'=>$circuit->getId())));

            
        }
       
		$datedbb = $aujourdhuib->format("Y-m-d");
        $datedenff = $aujourdhuienf->format("Y-m-d");
		$datedb_fin=date($datedbb);
		$datedenf_fin=date($datedenff);
		$auj_fin=date("Y-m-d");
       
    return $this->render('BtobCuircuitBundle:Default:inscription.html.twig', array('datedb' => $datedb,'datedb_fin' => $datedb_fin,'datedenf_fin' => $datedenf_fin,'auj_fin' => $auj_fin,'datedenf' => $datedenf,'entry' => $circuit,'circuitsupp' => $circuitsupp,'circuitreduc' => $circuitreduc,'circuitprices' => $circuitprices,'nbad' => $nbad,'nbchambre' => $nbchambre,'nbbebe' => $nbbebe,'nbenf' => $nbenf));

    
    }
    
      
    
    
    
/*
    public function reservationAction(Cuircuit $circuit)
    {
        $em = $this->getDoctrine()->getManager();
        $client = new Clients();
        $form = $this->createForm(new ClientsType(), $client);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            //Tools::dump($post["cin"],true);
            $cin = $post["cin"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->bind($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                $resa=new Resacircui();

                $date = Tools::explodedate($request->request->get("dated"),'/');
                $resa->setDated(new \DateTime($date));
                $resa->setUser($this->get('security.context')->getToken()->getUser());
                $resa->setClient($client);
                $resa->setBebe($request->request->get("bebe"));
                $resa->setAdulte($request->request->get("adulte"));
                $resa->setEnfant($request->request->get("enfant"));
                $resa->setComment($request->request->get("body"));
                $resa->setCircuit($circuit);
                $em->persist($resa);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_circuit_validation'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$circuit));
    }
    
    
    
    */
    
    
    public function reservationAction(Cuircuit $circuit)

    {

        $em = $this->getDoctrine()->getManager();
      
        $session = $this->getRequest()->getSession();

        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');
        
        $recap = $session->get("array");
        $circuitperiode = $session->get("circuitprice");
        
        
       $pricecirc =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuitprice')->find($circuitperiode);   

       
        $ArrBase = array();
        $total=0;
        
       foreach ($recap as $key => $value) {
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               
                  if(isset($val1['suppad']))
                  {
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                  
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  $reducaddp =0;
               
                  if(isset($val1['reducad']))
                  {
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                  
                  $totalreducad+= $reducaddp;
                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
                  if(isset($vale['suppenf']))
                  {
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
              $totalsuppenf+= $suppenfp;
              
              
              //reduction
              
              
              $reducenfp=0;
                  if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
              $totalreducenf+= $reducenfp;
              
              
              
              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
                  if(isset($valb['suppb']))
                  {
                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              $reducbp=0;
                  if(isset($valb['reducb']))
                  {
                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
              $totalreducb+= $reducbp;
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         $ArrBase[$key]['price'] = $prices;
         $user = $this->get('security.context')->getToken()->getUser();
         if($user->getPrstcircuit()==0){
          $total+= $ArrBase[$key]['price']+$user->getMargecircuit(); 
         }else {
          $total+= $ArrBase[$key]['price']+(($ArrBase[$key]['price']*$user->getMargecircuit())/100); 
         }
           
       }
        
      
        $circuitprice = $session->get("circuitprice");
        
        
       
       // $nbchambre = count($nbad);

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $email = $post["email"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);

            $User = $this->get('security.context')->getToken()->getUser();

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();
                

                
                
                
                
                
                
                
                

                $resa=new Reservationcircuit();



                

                $resa->setUser($User);

                $resa->setClient($client);

                $resa->setCuircuit($circuit);
                $resa->setCuircuitprice($pricecirc);
                $resa->setEtat(1);
                $resa->setTotal($total);
                  
                $em->persist($resa);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Circuit");
                        $hist->setMessage("Ajouter une nouvelle reservation du circuit " . $resa->getCuircuit()->getLibelle()." du " . date_format($resa->getCuircuitprice()->getDated(), 'Y-m-d H:i:s')." au " . date_format($resa->getCuircuitprice()->getDates(), 'Y-m-d H:i:s'));
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);

                $em->flush();
                
                
                
                                
            $ArrBase = array();
            $total=0;
        
        
        
                        
                        

        
       foreach ($recap as $key => $value) {
           
           
           
           
            
           
           //adult
           if(isset($value['adult']))
           {
             $nbad = count($value['adult']);   
           }else{
               
             $nbad=0;  
           }
           
          if(isset($value['enf']))
          {
            $nbenf = count($value['enf']);  
          }else{
              
            $nbenf=0;  
          }
           if(isset($value['bebe']))
           {
               $nbbebe = count($value['bebe']);
           }else{
              $nbbebe = 0; 
           }
               
           $totalsuppad=0;
           $totalsuppenf=0;
           $totalsuppb=0;
           
           $totalreducad=0;
           $totalreducenf=0;
           $totalreducb=0;
           
           
           
           if(isset($value['adult']))
           {
           foreach ($value['adult'] as $k1 => $val1) {
               
               $ArrBase[$key]['adult'][$k1]['namead']=$val1['namead'];
               $ArrBase[$key]['adult'][$k1]['prenomad']=$val1['prenomad'];
               $ArrBase[$key]['adult'][$k1]['agead']=$val1['agead'];
               
               $suppaddp =0;
               $reducaddp =0; 
           
           $resadetail=new Reservationcdetail();
           $resadetail->setReservationcircuit($resa);
           $resadetail->setChambre($key+1);
           
           
           $resadetail->setNamead($val1['namead']);
           $resadetail->setPrenomad($val1['prenomad']);
           $resadetail->setAgead($val1['agead']);
               
           
          
                  if(isset($val1['suppad']))
                  {
                      
                     

 
                     $suppadd=array(); 
                      
                     foreach ($val1['suppad'] as $k2 => $val2) {
                         
                    
                    $supplementad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2);   
                    $suppadd[]= $supplementad->getName();
                    $suppaddp+= $supplementad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['suppad'] = $suppadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['suppadp'] = $suppaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['suppad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['suppadp'] =0;
                  }
                  
                 
                  $resadetail->setSuppad(json_encode($ArrBase[$key]['adult'][$k1]['suppad']));
                  $totalsuppad+= $suppaddp;
                  
                  
                  //reduction
                  
                  if(isset($val1['reducad']))
                  {
                      
                     

 
                     $reducadd=array(); 
                      
                     foreach ($val1['reducad'] as $kr2 => $valr2) {
                         
                    
                    $reductionad =   $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($valr2);   
                    $reducadd[]= $reductionad->getName();
                    $reducaddp+= $reductionad->getPrice();  
                  } 
                  
                  $ArrBase[$key]['adult'][$k1]['reducad'] = $reducadd;
                  
                 
                  $ArrBase[$key]['adult'][$k1]['reducadp'] = $reducaddp;
                    
                  }else{
                      
                   $ArrBase[$key]['adult'][$k1]['reducad'] =NULL; 
                   $ArrBase[$key]['adult'][$k1]['reducadp'] =0;
                  }
                  
                 
                  $resadetail->setReducad(json_encode($ArrBase[$key]['adult'][$k1]['reducad']));
                  $totalreducad+= $reducaddp;
                  
                  
           
           

           $em->persist($resadetail);
           $em->flush();


                  
              
           }
           }
         
          //enf
           if(isset($value['enf']))
           {
           foreach ($value['enf'] as $k1e => $vale) {
               
               
               
               $ArrBase[$key]['enf'][$k1e]['nameenf']=$vale['nameenf'];
               $ArrBase[$key]['enf'][$k1e]['prenomenf']=$vale['prenomenf'];
               $ArrBase[$key]['enf'][$k1e]['ageenf']=$vale['ageenf'];
               $suppenfp=0;
               $reducenfp=0;
               
           $resadetails=new Reservationcdetail();
           $resadetails->setReservationcircuit($resa);
           $resadetails->setChambre($key+1);
           $resadetails->setNameenf($ArrBase[$key]['enf'][$k1e]['nameenf']);
           $resadetails->setPrenomenf($ArrBase[$key]['enf'][$k1e]['prenomenf']);
           $resadetails->setAgeenf($ArrBase[$key]['enf'][$k1e]['ageenf']);
                  if(isset($vale['suppenf']))
                  {
          // $resadetails->setSuppenf(json_encode($vale['suppenf']));
                      $suppenf= array();
                     foreach ($vale['suppenf'] as $k2e => $val2e) {
                     $supplementenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2e);  
                    $suppenf[]=$supplementenf->getName(); 
                    $suppenfp+=$supplementenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['suppenf'] = $suppenf;
                  $ArrBase[$key]['enf'][$k1e]['suppenfp'] = $suppenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['suppenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['suppenfp'] =0;
                  }
                 
                  
              $resadetails->setSuppenf(json_encode($ArrBase[$key]['enf'][$k1e]['suppenf']));
  
              $totalsuppenf+= $suppenfp;
              
              //reduction
              
              if(isset($vale['reducenf']))
                  {
                      $reducenf= array();
                     foreach ($vale['reducenf'] as $k2re => $val2re) {
                     $reductionenf =  $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2re);  
                    $reducenf[]=$reductionenf->getName(); 
                    $reducenfp+=$reductionenf->getPrice(); 
                  } 
                  
                  $ArrBase[$key]['enf'][$k1e]['reducenf'] = $reducenf;
                  $ArrBase[$key]['enf'][$k1e]['reducenfp'] = $reducenfp;
                  }else{
                      
                   $ArrBase[$key]['enf'][$k1e]['reducenf'] =NULL;
                   $ArrBase[$key]['enf'][$k1e]['reducenfp'] =0;
                  }
                 
                  
              $resadetails->setReducenf(json_encode($ArrBase[$key]['enf'][$k1e]['reducenf']));
  
              $totalreducenf+= $reducenfp;
              
              
              
           
          
 
           $em->persist($resadetails);
            $em->flush();

              
           }
           
           }
           
           
           
           
           //bebe
           
           if(isset($value['bebe']))
           {
           foreach ($value['bebe'] as $k1b => $valb) {
               
               
               
               $ArrBase[$key]['bebe'][$k1b]['nameb']=$valb['nameb'];
               $ArrBase[$key]['bebe'][$k1b]['prenomb']=$valb['prenomb'];
               $ArrBase[$key]['bebe'][$k1b]['ageb']=$valb['ageb'];
               $suppbp=0;
               
               $reducbp=0;
               
               $resadetailb=new Reservationcdetail();
           $resadetailb->setReservationcircuit($resa);
           $resadetailb->setChambre($key+1);
           $resadetailb->setNameb($ArrBase[$key]['bebe'][$k1b]['nameb']);
          $resadetailb->setPrenomb($ArrBase[$key]['bebe'][$k1b]['prenomb']);
          $resadetailb->setAgeb($ArrBase[$key]['bebe'][$k1b]['ageb']);
                  if(isset($valb['suppb']))
                  {

                      $suppb=array();
                     foreach ($valb['suppb'] as $k2b => $val2b) {
                     $supplementb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Supplementc')->find($val2b);      
                    $suppb[]= $supplementb->getName();
                    $suppbp+= $supplementb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['suppb'] = $suppb;
                  $ArrBase[$key]['bebe'][$k1b]['suppbp'] = $suppbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['suppb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['suppbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setSuppb(json_encode($ArrBase[$key]['bebe'][$k1b]['suppb']));
              $totalsuppb+= $suppbp;
              
              
              //reduction
              
              if(isset($valb['reducb']))
                  {

                      $reducb=array();
                     foreach ($valb['reducb'] as $k2rb => $val2rb) {
                     $reductionb = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reductionc')->find($val2rb);      
                    $reducb[]= $reductionb->getName();
                    $reducbp+= $reductionb->getPrice();  
                  } 
                  
                  $ArrBase[$key]['bebe'][$k1b]['reducb'] = $reducb;
                  $ArrBase[$key]['bebe'][$k1b]['reducbp'] = $reducbp;
                  }else{
                      
                   $ArrBase[$key]['bebe'][$k1b]['reducb'] =NULL;
                   $ArrBase[$key]['bebe'][$k1b]['reducbp'] =0;
                  }
                 
                  
                  
              $resadetailb->setReducb(json_encode($ArrBase[$key]['bebe'][$k1b]['reducb']));
              $totalreducb+= $reducbp;
              

            $em->persist($resadetailb);
            $em->flush();


              
           }
           }
           
        
          $prices = $this->getDoctrine()
                        ->getRepository('BtobCuircuitBundle:Cuircuitprice')
                        ->calcul($pricecirc,$nbad,$nbenf,$nbbebe,$totalsuppad,$totalsuppenf,$totalsuppb,$totalreducad,$totalreducenf,$totalreducb);
         $ArrBase[$key]['price'] = $prices;
       //   $total+= $ArrBase[$key]['price']; 
         $user = $this->get('security.context')->getToken()->getUser();
         if($user->getPrstcircuit()==0){
          $total+= $ArrBase[$key]['price']+$user->getMargecircuit(); 
         }else {
          $total+= $ArrBase[$key]['price']+(($ArrBase[$key]['price']*$user->getMargecircuit())/100); 
         }
           
       }    
                
                
                
                
                
        
                
               

            } else {
                echo $form->getErrors();
            }

                return $this->redirect($this->generateUrl('btob_circuit_validation'));



        }


//$total=$total+($total*0.5);
        return $this->render('BtobCuircuitBundle:Default:reservation.html.twig', array('form' => $form->createView(),'entry'=>$circuit,'recap'=>$recap,'ArrBase'=>$ArrBase,'total'=>$total));


    }


    
    public function validationAction(){
        return $this->render('BtobCuircuitBundle:Default:validation.html.twig', array());
    }
    
    
    
    
    
    
     public function listpersonalisationcircuitAction()
    {

        $em = $this->getDoctrine()->getManager();





        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
            $agence = $request->request->get('agence');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();


        if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
        {
            $entities = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Personalisationcircuit")->findAll();

        }else{
            $entities =$this->getDoctrine()->getRepository("BtobCuircuitBundle:Personalisationcircuit")->findBy(array('agent' => $user));

        }
        if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getAgent()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }


        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
       
        

        return $this->render('BtobCuircuitBundle:Default:listpersonalisation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
            'agence' => $agence,
            'users' => $users,
        ));






    }   
    
    public function ajoutreservationAction(Request $request){
        $reservation = new Personalisationcircuit();
         $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PersonalisationcircuitType() , $reservation);
        $form->handleRequest($request);
        $user =  $this->get('security.token_storage')->getToken()->getUser();
        $circuit = $em->getRepository('BtobCuircuitBundle:Cuircuit')->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
           
             $reservation->setAgent($user);
            // $reservation->setEtat(1);
            $request = $this->get('request');
           // $request->request->get("dureecircuit");
           $circuitbyid = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($form->get('cuircuit')->getData());
         //  var_dump($circuitbyid).die;
            $montant = $circuitbyid->getPrix();
            $marge = ($circuitbyid->getPrix() * $circuitbyid->getMarge()) / 100 ;
            if(!is_null($marge))
            $montant = $montant + $marge;     
            $reservation->setTotal($montant);
            $em->persist($reservation);
            $em->flush();
            return $this->redirect($this->generateUrl('circuits_personalisation_payerreservation', array("id" => $reservation->getId())));

        }


        return $this->render('BtobCuircuitBundle:Default:ajoutreservation.html.twig', array(
            'reservation' => $reservation,
           // 'arraydater'=> $arraydater,
            'form' => $form->createView(),
           // 'datef'=> max($arraydatefin),
           // 'arr'=>$datenotchecked,
            'circuit'=>$circuit,
        ));
    }

    public function detailpersonalisationAction(Personalisationcircuit $Personalisationcircuit
            )
    {

        return $this->render('BtobCuircuitBundle:Default:showPersonalisation.html.twig', array('entry' => $Personalisationcircuit));

    }

    public function deletepersonalisationAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobCuircuitBundle:Personalisationcircuit')->find($id);

        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('circuits_personalisation'));

    } 
    public function personalizeAction(Cuircuit $circuit)
    {
         $em = $this->getDoctrine()->getManager();
         $client = new Clients();
         $form = $this->createForm(new ClientsType(), $client);
         
        $User = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $post = $request->request->get('btob_hotelbundle_clients');
            $email = $post["email"];
            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneByEmail($email);
            if ($testclient != null) {
                $client = $testclient;
            }
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($client);
                $em->flush();
                 $ville = $request->request->get('ville');
                $dated = $request->request->get('dated');
                
              
                $dater = $request->request->get('dater');
                $typec = $request->request->get('typec');
                $formule = $request->request->get('formule');
                $nbradult = $request->request->get('nbradult');
                $nbrenf = $request->request->get('nbrenf');
                $nbrbebe = $request->request->get('nbrbebe');
                $nbrch = $request->request->get('nbrch');
                $budgetmin = $request->request->get('budgetmin');
                $budgetmax = $request->request->get('budgetmax');
                $remarque = $request->request->get('remarque');

                $personalisation = new Personalisationcircuit();
                $personalisation->setClient($client);
                $personalisation->setCuircuit($circuit);
                $personalisation->setAgent($User);
                
                
                
                $personalisation->setTypec($typec);
                
                $personalisation->setFormule($formule);
                
                $personalisation->setNbradult($nbradult);
                $personalisation->setNbrenf($nbrenf);
                $personalisation->setNbrbebe($nbrbebe);
                $personalisation->setNbrch($nbrch);
                $personalisation->setBudgetmin($budgetmin);
                $personalisation->setBudgetmax($budgetmax);
                $personalisation->setRemarque($remarque);
                $em->persist($personalisation);
                $em->flush();
 foreach( $ville as $key=>$value){
                    $catg = new Tpcircuit();
                    $catg->setPersonalisationcircuit($personalisation);
                    $catg->setVille($ville[$key]);
                    
                    if($request->request->get('dateflex'.$key)=="on")
                {
                    $catg->setDateflex(true);
                }
                else{
                    
                    $catg->setDateflex(false);
                }
                    
                    $catg->setDated(new \Datetime(Tools::explodedate($dated[$key],'/')));
                    $catg->setDater(new \Datetime(Tools::explodedate($dater[$key],'/')));
                    $em->persist($catg);
                    $em->flush();

                }
                
                
                date_default_timezone_set("Europe/Paris");
                mb_internal_encoding("UTF-8");
                $daymonthyear = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%d %B %Y ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));
                $dayonly = iconv("ISO-8859-9","UTF-8",strftime(mb_convert_encoding('%A ','ISO-8859-9','UTF-8') ,strtotime(date('Y M D'))));



                $info =$this->getDoctrine()->getRepository('BtobAgenceBundle:Info')->find(1);
                $img_logo=$this->get('templating.helper.assets')->getUrl($info->getLogo());
                $to = $personalisation->getClient()->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subject =  $personalisation->getAgent()->getName().": réservation circuit personnalisé " ;
                $headers = "From:" .$personalisation->getAgent()->getName()."<" .$personalisation->getAgent()->getEmail(). ">\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";

                $message1 = "--$mime_boundary\n";
                $message1 .= "Content-Type: text/html; charset=UTF-8\n";
                $message1 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message1 .= "<html>\n";
                $message1 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; line-height:25px;"  leftmargin="0">';
                $message1 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td>
							   </tr>
							  </table><br>';
                $message1 .='
				 <b>Bonjour,</b><br />
				 Nous vous remercions pour la confiance renouvelée, nous vous informons que votre demande sera traitée dans l\'heure qui suit.<br />
				 Cordialement.';

                $message1 .= '<table width="90%"  cellspacing="1" border="0">';
                $message1 .= '<tr>';
                $message1 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse().' - <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message1 .= '</tr>';

                $message1 .= '</table>';
                $message1 .= '</body>';


                mail($to, $subject, $message1, $headers);

                $admin = $this->getDoctrine()->getRepository('UserUserBundle:User')->findByRole('ROLE_SUPER_ADMIN');

                $too      = $admin->getEmail();
                $mime_boundary = "----MSA Shipping----" . md5(time());
                $subjects =   "Explore Voyage: réservation  circuit personnalisé " ;
                $header = "From:" .$personalisation->getAgent()->getName().":" .$personalisation->getAgent()->getEmail(). "\n";
                $header .= "Reply-To:" .$personalisation->getAgent()->getName()."<" .$admin->getEmail(). ">\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
                $message2 = "--$mime_boundary\n";
                $message2 .= "Content-Type: text/html; charset=UTF-8\n";
                $message2 .= "Content-Transfer-Encoding: 8bit\n\n";
                $message2 .= "<html>\n";
                $message2 .= '<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px;"  leftmargin="0">';
                $message2 .= '<table width="90%"  cellspacing="1" border="0">
				               <tr>
							    <td><img src="'.$img_logo.'" /><br></td> 
							   </tr>
							  </table><br>';
                $message2 .='<b>Bonjour,</b><br>
				 vous avez reçu une réservation Circuits  , merci de consulter votre backoffice .';

                $message2 .= '<br><table width="90%"  cellspacing="1" border="0">';
                $message2 .= '<tr>';
                $message2 .= '<td height="30" align="center" colspan="3" bgcolor="#183961" style="color:#fff;">'.$info->getAdresse() .'- <a style="color:#fff;">'.$info->getTelephone().'</a>
				</td>';
				$message2 .= '</tr>';

                $message2 .= '</table>';
                $message2 .= '</body>';



                mail($too, $subjects, $message2, $header); 
                
                
                

                return $this->redirect($this->generateUrl('btob_circuit_validation'));

          $client = new Clients();

                $form = $this->createForm(new ClientsType(), $client); 
            }
        }


        return $this->render('BtobCuircuitBundle:Default:personalize.html.twig', array('entry' => $circuit, 'form' => $form->createView()));
    }    
    public function payerReservationpersonnaliseeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobCuircuitBundle:Personalisationcircuit')
            ->find($id);
      
        
      //  
            $form = $this->createForm(new PersonalisationcircuitpayerType(), $reservation);
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
              //  if($reservation->getTotal() == $form->get('montantpaye')->getData())
               // $reservation->setEtat(3);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('circuits_personalisation'));

            }
        
       

       return $this->render('BtobCuircuitBundle:Default:payercircuitpersonnalisee.html.twig', array('form' =>$form->createView(),'reservation'=>$reservation));
    }


}
