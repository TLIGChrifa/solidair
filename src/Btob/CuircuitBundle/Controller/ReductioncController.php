<?php

namespace Btob\CuircuitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Reductionc;
use Symfony\Component\HttpFoundation\Request;
use Btob\CuircuitBundle\Form\ReductioncType;
use Symfony\Component\HttpFoundation\JsonResponse;
use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;

class ReductioncController extends Controller {

    public function indexAction($cuircuitid) {
        $cuircuit = $this->getDoctrine()
                ->getRepository('BtobCuircuitBundle:Cuircuit')
                ->find($cuircuitid);
        return $this->render('BtobCuircuitBundle:Reductionc:index.html.twig', array('entities' => $cuircuit->getReductionc(), "cuircuitid" => $cuircuitid));
    }

    public function addAction($cuircuitid) {
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);
        $Reductionc = new Reductionc();
        $form = $this->createForm(new ReductioncType(), $Reductionc);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $Reductionc->setCuircuit($cuircuit);
                $em->persist($Reductionc);
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Circuit");
                        $hist->setMessage("Ajout: Nouveau réduction  - ".$cuircuit->getLibelle());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
                $em->flush();
                return $this->redirect($this->generateUrl('btob_reductionc_homepage', array("cuircuitid" => $cuircuitid)));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobCuircuitBundle:Reductionc:form.html.twig', array('form' => $form->createView(), "cuircuitid" => $cuircuitid));
    }

    public function editAction($id, $cuircuitid) {
        $request = $this->get('request');
        $Reductionc = $this->getDoctrine()
                ->getRepository('BtobCuircuitBundle:Reductionc')
                ->find($id);
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReductioncType(), $Reductionc);
        $form->handleRequest($request);

        if ($form->isValid()) {
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Circuit");
                        $hist->setMessage("Modification: Réduction du circuit (".$cuircuit->getLibelle().") n° " . $Reductionc->getId()." - " . $Reductionc->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
            $em->flush();

            return $this->redirect($this->generateUrl('btob_reductionc_homepage', array("cuircuitid" => $cuircuitid)));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobCuircuitBundle:Reductionc:form.html.twig', array('form' => $form->createView(), 'id' => $id, "cuircuitid" => $cuircuitid)
        );
    }

    public function deleteAction(Reductionc $Reductionc, $cuircuitid) {
        $em = $this->getDoctrine()->getManager();
        $cuircuit = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Cuircuit')->find($cuircuitid);

        if (!$Reductionc) {
            throw new NotFoundHttpException("Reductionc non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Circuit");
                        $hist->setMessage("Suppression: Réduction du circuit (".$cuircuit->getLibelle().") n° " . $Reductionc->getId()." - " . $Reductionc->getName());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($Reductionc);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_reductionc_homepage', array("cuircuitid" => $cuircuitid)));
    }

}
