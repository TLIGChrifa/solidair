<?php

namespace Btob\CuircuitBundle\Controller;

use Btob\HotelBundle\Common\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Btob\CuircuitBundle\Entity\Cuircuit;
use Btob\CuircuitBundle\Entity\Resacuircuitcomment;
use Btob\HotelBundle\Entity\Clients;
use Btob\HotelBundle\Entity\Facture;
use Btob\HotelBundle\Entity\Tfacture;
use User\UserBundle\Entity\Solde;
use Btob\HotelBundle\Form\ClientsType;
use Btob\CuircuitBundle\Entity\Resacircui;
use Btob\CuircuitBundle\Entity\Reservationcircuit;
use Btob\CuircuitBundle\Form\ReservationcircuitpayerType;
use Btob\CuircuitBundle\Form\ReservationcircuitType;
use DateInterval;
use DatePeriod;
use DateTime;

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Validator\Constraints\DateTime;

use User\UserBundle\Entity\Historique;
use User\UserBundle\Entity\HistoriqueType;
class ReservationController extends Controller
{
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $type = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $agence = "";
        $users = $this->getDoctrine()->getRepository('UserUserBundle:User')->findAll();
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $agence = $request->request->get('agence');
            $type = $request->request->get('type');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }
        $user = $this->get('security.context')->getToken()->getUser();
		if($user->getRoles()[0] =='ROLE_SUPER_ADMIN')
		{
			$entities = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Reservationcircuit")->findAll();

		}else{
			$entities = $this->getDoctrine()->getRepository("BtobCuircuitBundle:Reservationcircuit")->findBy(array('user' => $user), array('id' => 'DESC'));

		}
       if (in_array("AGENCEID", $user->getRoles()) || $agence != '') {

            if ($agence != "") {

                $id = $agence;

            } else {

                $id = $user->getId();

            }

            $tabuser = array();

            foreach ($entities as $value) {

                if ($value->getUser()->getId() == $id) {

                    $tabuser[] = $value;

                }

            }

            $entities = $tabuser;

        }




        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
      
       if ($type != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if ($type=="Circuit") {
                    if($value->getCuircuit()->getType()=="Circuit")
                    {
                      $tabsearch[] = $value;  
                    }
                    
                }
                if ($type=="Excursion") {
                    if($value->getCuircuit()->getType()=="Excursion")
                    {
                      $tabsearch[] = $value;  
                    }
                    
                }
                
                
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobCuircuitBundle:Reservation:index.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'type' => $type,
            'numres' => $numres,
            'datefto' => $datefto,
            'agence' => $agence,
            'users' => $users,
            ));
    }
    public function ajoutreservationAction(Request $request){
        $reservation = new Reservationcircuit();
         $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReservationcircuitType() , $reservation);
        $form->handleRequest($request);
        $user =  $this->get('security.token_storage')->getToken()->getUser();
        $circuit = $em->getRepository('BtobCuircuitBundle:Cuircuit')->findAll();
    //     $arraydater = array();
    //    $arraydatefin = array();
     
    //     foreach($circuit as $c){
    //         $ar[] = $c->getDated()->format('m/d/Y')." ".$c->getDatef()->format('m/d/Y');
    //         // $c->getDatef()
    //         $period = new DatePeriod(
    //             new DateTime($c->getDated()->format('m/d/Y')),
    //             new DateInterval('P1D'),
    //             new DateTime( $c->getDatef()->modify('+ 1 day')->format('m/d/Y'))
    //         );
           
    //         foreach ($period as $key => $value) {
    //             $arraydater[] = $value->modify('-1 month')->format('Y,m,d') ;   
    //             $arraydatefin[] = $value->format('m/d/Y') ;
    //          }
            
    //     }
    //    // var_dump($arraydater).die;
    //     $datefin = max($arraydatefin);
    //     $demain = date('Y-m-d', strtotime('+1 day'));
    //    // var_dump($demain) .die;
    //     $df = new DateTime($datefin);
    //     $datenotchecked = array();
    //     $periodcalandar = new DatePeriod(
    //         new DateTime(),
    //         new DateInterval('P1D'),
    //         new DateTime( $df->modify('+ 1 day')->format('m/d/Y'))
    //     );
    //    // var_dump($arraydater).die;
    //      foreach ($periodcalandar as $key => $valuee) {
    //          if(!in_array($valuee->modify('- 1 month')->format('Y,m,d') , $arraydater ))
    //           $datenotchecked[] = $valuee->modify('+ 1 month')->format('m/d/Y')  ; 

    //      }
        // var_dump($datenotchecked).die;
        if ($form->isSubmitted() && $form->isValid()) {
           
            $reservation->setUser($user);
            $reservation->setEtat(1);
            $request = $this->get('request');
            $request->request->get("dureecircuit");
            //var_dump( ).die;
            $circuitbyid = $em->getRepository('BtobCuircuitBundle:Cuircuit')->find($request->request->get("dureecircuit"));
            $reservation->setCuircuit($circuitbyid);
            $montant = $reservation->getCuircuit()->getPrix();
            $marge = ($reservation->getCuircuit()->getPrix() * $reservation->getCuircuit()->getMarge()) / 100 ;
            if(!is_null($marge))
            $montant = $montant + $marge;     
            $reservation->setTotal($montant);
           // var_dump($montant).die; 
            $em->persist($reservation);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_resaback_payerreservation', array("id" => $reservation->getId())));

        }


        return $this->render('BtobCuircuitBundle:Reservation:ajoutreservation.html.twig', array(
            'reservation' => $reservation,
           // 'arraydater'=> $arraydater,
            'form' => $form->createView(),
           // 'datef'=> max($arraydatefin),
           // 'arr'=>$datenotchecked,
            'circuit'=>$circuit,
        ));
    }



    public function payerreservationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reservation=$this->getDoctrine()
            ->getRepository('BtobCuircuitBundle:Reservationcircuit')
            ->find($id);
      
        
      //  
            $form = $this->createForm(new ReservationcircuitpayerType(), $reservation);
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
                if($reservation->getTotal() == $form->get('montantpaye')->getData())
                $reservation->setEtat(3);
               // var_dump($reservation->getTypepayement(),$reservation->getMontantpaye()).die;
                $em->persist($reservation);
               // $Reservation->setTypepayement($hotel);
               $em->flush();
               $this->addFlash("success", "Le payement à été éffectué avec succès ..");
               return $this->redirect($this->generateUrl('btob_resaback_cuircuit'));

            }
        
       

       return $this->render('BtobCuircuitBundle:Reservation:payer.html.twig', array('form' =>$form->createView(),'reservation'=>$reservation));
    }
    public function actiAction(Reservationcircuit $resacircui, $action)
    {
        $em = $this->getDoctrine()->getManager();
        $facts = $this->getDoctrine()->getRepository('BtobHotelBundle:Facture')->finFacts();
        $factures = array();
        $dr = new \DateTime();
        if (count($facts) > 0) {
            foreach ($facts as $kef) {
                if ($kef->getDcr()->format("Y") == $dr->format("Y")) {
                    array_push($factures, $kef);
                }
            }
        }

        if (count($factures) > 0) {
            $num = $factures[0]->getNum() + 1;
        } else {
            $num = 1;
        }

        if ($action == 2) {
            $ffact = new Facture();
            $ffact->setNum($num);
            $ffact->setEtat(1);
            $ffact->setClient($resacircui->getClient()->getName() . ' ' . $resacircui->getClient()->getPname());
            $ffact->setClientId($resacircui->getClient());
            $ffact->setDcr($dr);
            $ffact->setDate(new \DateTime());
            $ffact->setIdm($resacircui->getId());
            $ffact->setType("Circuit");
            $ffact->setMontant($resacircui->getTotal());
            $em->persist($ffact);
            $em->flush();

            $tfact = new Tfacture();
            $tfact->setFacture($ffact);
            $tfact->setPrixUnitaire($resacircui->getTotal());
            $tfact->setQuantite(1);
            $tfact->setRemise(0);
            $tfact->setTva(20);
            $tfact->setType("Circuit:".$resacircui->getCuircuit()->getLibelle());
            $em->persist($tfact);
            $em->flush();


			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resacircui->setEtat(2);
            $em->persist($resacircui);
            $em->flush();
			
			/*$user = $this->get('security.context')->getToken()->getUser();			
			$solde = new Solde();
            $solde->setObservation("Paiement de la commande Circuit " . Tools::RefResa("", $resacircui->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resacircui->getUser());
            $solde->setSolde(($resacircui->getTotal() * (-1)));
            $em->persist($solde);
			$em->flush();
            $newsolde = $resacircui->getUser()->getSold() - $resacircui->getTotal();
            $currenuser = $resacircui->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();*/
            return $this->redirect($this->generateUrl('btob_resaback_detail_cuircuit', array('id' => $resacircui->getId())));

        }

        if ($action == 3) {

			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation payée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();

            $resacircui->setEtat(3);
            $em->persist($resacircui);
            $em->flush();
						
			$solde = new Solde();
            $solde->setObservation("Paiement de la commande circuit " . Tools::RefResa("", $resacircui->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resacircui->getUser());
            $solde->setSolde(($resacircui->getTotal() * (-1)));
            $em->persist($solde);
			$em->flush();
            $newsolde = $resacircui->getUser()->getSold() - $resacircui->getTotal();
            $currenuser = $resacircui->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();
            return $this->redirect($this->generateUrl('btob_resaback_detail_cuircuit', array('id' => $resacircui->getId())));

        }
        if ($action == 0) {
		
		   if($resacircui->getEtat()==2)
		   {
               $ffact = new Facture();
               $ffact->setNum($num);
               $ffact->setEtat(1);
               $ffact->setClient($resacircui->getClient()->getName() . ' ' . $resacircui->getClient()->getPname());
               $ffact->setClientId($resacircui->getClient());
               $ffact->setDcr($dr);
               $ffact->setDate(new \DateTime());
               $ffact->setIdm($resacircui->getId());
               $ffact->setType("Circuit");
               $ffact->setMontant(-1*$resacircui->getTotal());
               $em->persist($ffact);
               $em->flush();

               $tfact = new Tfacture();
               $tfact->setFacture($ffact);
            $tfact->setPrixUnitaire((-1)*$resacircui->getTotal());
            $tfact->setQuantite(1);
            $tfact->setRemise(0);
            $tfact->setTva(20);
             //  $tfact->setMontant(-1*$resacircui->getTotal());
               $tfact->setType("Circuit:".$resacircui->getCuircuit()->getLibelle());
               $em->persist($tfact);
               $em->flush();

		  /* $user = $this->get('security.context')->getToken()->getUser();			
			$solde = new Solde();
            $solde->setObservation("Annuler de la commande Circuit " . Tools::RefResa("", $resacircui->getId()));
            $solde->setAdminadd($user);
            $solde->setAgence($resacircui->getUser());
            $solde->setSolde(($resacircui->getTotal()));
            $em->persist($solde);
			$em->flush();
            $newsolde = $resacircui->getUser()->getSold() + $resacircui->getTotal();
            $currenuser = $resacircui->getUser();
            $currenuser->setSold($newsolde);
            $em->persist($currenuser);
			$em->flush();*/
		   }
		   
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation annulée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resacircui->setEtat(0);
            $em->persist($resacircui);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_resaback_detail_cuircuit', array('id' => $resacircui->getId())));

        }
        if ($action == 4) {
            
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Demande d'annulation du réservation");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();
            $resacircui->setEtat(4);
            $em->persist($resacircui);
            $em->flush();
            return $this->redirect($this->generateUrl('btob_resaback_detail_cuircuit', array('id' => $resacircui->getId())));

        }
    }

    /**
     * @param Resacircui $resacircui
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Reservationcircuit $resacircui)
    {
        return $this->render('BtobCuircuitBundle:Reservation:detail.html.twig', array('entry' => $resacircui));
    }
    public function voucherAction(Reservationcircuit $resacircui)
    {
		$em = $this->getDoctrine()->getManager();
		        
			$user = $this->get('security.context')->getToken()->getUser();
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $comm = new Resacuircuitcomment();
                            $comm->setReservationcircuit($resacircui);
                            $comm->setUser($user);
                            $comm->setAction("Réservation facturée");
                            $comm->setIp($ip);
                            $em->persist($comm);
                            $em->flush();

        $pdf = $this->get('white_october.tcpdf')->create();
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);

        $pdf->SetFont('helvetica', '', 10, '', true);

        $pdf->AddPage();
		
		$html = $this->renderView('BtobCuircuitBundle:Reservation:voucher.html.twig', array('entry' => $resacircui));

        $img_header=$this->get('templating.helper.assets')->getUrl('front/images/header-vocher-explore-01.png');
        $pdf->Image($img_header, 0, 0, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Image('http://www.digitravel-solution.com/Version_B2B/public_html/front/images/logo.png', 140, 20, 60, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        $pdf->writeHTML($html);
        $img_footer=$this->get('templating.helper.assets')->getUrl('front/images/footer-vocher-explore-01.png');
        $pdf->Image($img_footer, 0, 210, 211, '', 'PNG', '', '', true, 300, '', false, false, 0, false, false, false);
        //$pdf->Output('/pnv.pdf', 'F');
        $nompdf = 'voucher_.pdf';
        $pdf->Output($nompdf);
        return new \Symfony\Component\BrowserKit\Response($pdf->Output($nompdf));
        exit;
    }

    public function deleteAction(Reservationcircuit $resacircui)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$resacircui) {
            throw new NotFoundHttpException("Réservation non trouvée");
        }
                        $hist = new Historique();
                        $hist->setIp($_SERVER['REMOTE_ADDR']);
                        $hist->setType("BO");
                        $hist->setBundle("Circuit");
                        $hist->setMessage("Suppression: Réservation circuit n° " . $resacircui->getId());
                        $hist->setUser($this->get('security.context')->getToken()->getUser());
                        $em->persist($hist);
        $em->remove($resacircui);
        $em->flush();
        return $this->redirect($this->generateUrl('btob_resaback_cuircuit'));
    }
    
    
    public function ajxaddcircuitcommentAction()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $id = $request->request->get("id");
        $action = $request->request->get("action");
        $user = $this->get('security.context')->getToken()->getUser();
        $resacircui = $this->getDoctrine()->getRepository('BtobCuircuitBundle:Reservationcircuit')->find($id);
        $comment = new Resacuircuitcomment();
        $comment->setAction($action);
        $comment->setUser($user);
        $comment->setReservationcircuit($resacircui);
        $comment->setIp($ip);
        $em->persist($comment);
        $em->flush();
        exit;
    }
    
    
}
