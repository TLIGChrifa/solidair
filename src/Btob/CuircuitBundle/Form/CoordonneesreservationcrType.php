<?php

namespace Btob\CuircuitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CoordonneesreservationcrType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('adultes')
            ->add('adulte')
            ->add('email')
            ->add('demande')
            // ->add('cr')
            ->add('mobile')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\CuircuitBundle\Entity\Coordonneesreservationcr'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_cuircuitbundle_coordonneesreservationcr';
    }
}
