<?php





namespace Btob\CuircuitBundle\Form;





use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\OptionsResolver\OptionsResolverInterface;


//use Symfony\Component\Validator\Constraints\Null;





class CuircuitType extends AbstractType


{


    /**


     * @param FormBuilderInterface $builder


     * @param array $options


     */


    public function buildForm(FormBuilderInterface $builder, array $options)


    {


        $builder

         ->add('dated', 'date', ['label' => "Date de Début :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
         ->add('datef', 'date', ['label' => "Date de Fin :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])

            ->add('libelle', 'text', array('label' => "Libelle", 'required' => true))


            ->add('type', 'choice', array(


                'choices' => array('Circuit' => 'Circuit', 'Excursion' => 'Excursion'),


                'required' => true,


              //  'empty_value' => 'Choisissez ',


                'label' => 'Type',


            ))


           // ->add('dcr')


            ->add('ville', 'text', array('label' => "Ville", 'required' => true))


          /*  ->add('nbretoiles', 'choice', array(


                'choices' => array('3' => '3', '4' => '4', '5' => '5', '6' => 'Charme'),


                'required' => true,


                'empty_value' => 'Choisissez ',


                'label' => 'Nombre d\'étoile',


            ))*/


            ->add('prix', NULL, array('label' => ' ', 'required' => true))
            ->add('marge', NULL, array('label' => 'Marge(En pourcentage) ', 'required' => true))
            ->add('prixavance', NULL, array('label' => 'Avance(En pourcentage) ', 'required' => true))
            ->add('apartir', NULL, array('label' =>' ', 'required' => false))


            ->add('active', NULL, array('label' =>' ', 'required' => false))


            ->add('pindex', NULL, array('label' =>' ' , 'required' => false))


            ->add('ageenfmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Enfant  *',
            ))
            ->add('ageenfmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Enfant  *',
            ))
            ->add('agebmin', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Min Bébé  *',
            ))
            ->add('agebmax', 'choice', array(
                'choices' => array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'required' => true,
                'empty_value' => 'Choisissez ',
                'label'=>'Age Max Bébé  *',
            ))      
            ->add('arrangement', NULL, array('label' => "Logement", 'required' => true))
            ->add('maps', NULL, array('label' => "Adresse Maps", 'required' => false))


            //->add('nbrjr', NULL, array('label' => "Nombre des jours", 'required' => true))


          //  ->add('description')


            ->add('description', null, array('label' => "Description longue", 'required' => false))

 


        ;


    }


    


    /**


     * @param OptionsResolverInterface $resolver


     */


    public function setDefaultOptions(OptionsResolverInterface $resolver)


    {


        $resolver->setDefaults(array(


            'data_class' => 'Btob\CuircuitBundle\Entity\Cuircuit'


        ));


    }





    /**


     * @return string


     */


    public function getName()


    {


        return 'btob_cuircuitbundle_cuircuit';


    }


}


