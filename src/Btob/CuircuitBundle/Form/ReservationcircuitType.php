<?php

namespace Btob\CuircuitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReservationcircuitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        // ->add('dcr', 'date', ['label' => "Date de création :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
       // ->add('dated', 'date', ['label' => "Date de Début :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
          //  ->add('datef', 'date', ['label' => "Date de Fin :", 'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])
            ->add('nbnuits' ,'text', ['label' => "Nombre de Nuits :"])
            ->add('nbadultes' ,'text', ['label' => "Nombre des adultes :"])
            ->add('nbenfants' ,'text', ['label' => "Nombre des enfants :"])
            // ->add('total')
            // ->add('etat')
            // ->add('avance')
            // ->add('resultatfinal')
            // ->add('numautoris')
         //   ->add('cuircuit')
            // ->add('cuircuitprice')
            ->add('client')
            // ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\CuircuitBundle\Entity\Reservationcircuit'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_cuircuitbundle_reservationcircuit';
    }
}
