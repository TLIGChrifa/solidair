<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personalisationcircuit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\PersonalisationcircuitRepository")
 */
class Personalisationcircuit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="personalisationcircuit")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    /**
     * @ORM\ManyToOne(targetEntity="Cuircuit", inversedBy="personalisationcircuit")
     * @ORM\JoinColumn(name="cuircuit_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $cuircuit;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="personalisationcircuit")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $agent;

    /**

     * @ORM\OneToMany(targetEntity="Tpcircuit", mappedBy="personalisationcircuit")

     */

    protected $personalisationcircuit;

       /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
        /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;
      
    
    /**
     * @var string
     *
     * @ORM\Column(name="typec", type="string", length=255, nullable=true)
     */
    private $typec;
        /**
     * @var \Date
     *
     * @ORM\Column(name="dated", type="date", nullable =true)
     */
    private $dated;
        /**
     * @var \Date
     *
     * @ORM\Column(name="datef", type="date", nullable =true)
     */
    private $datef;
    
    /**
     * @var string
     *
     * @ORM\Column(name="formule", type="string", length=255, nullable=true)
     */
    private $formule;
    

    /**
     * @var integer
     *
     * @ORM\Column(name="nbradult", type="integer" , nullable=true)
     */
    private $nbradult;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrenf", type="integer" , nullable=true)
     */
    private $nbrenf;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrbebe", type="integer" , nullable=true)
     */
    private $nbrbebe;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrch", type="integer" , nullable=true)
     */
    private $nbrch;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="budgetmin", type="float" , nullable=true)
     */
    private $budgetmin;
    
    
     /**
     * @var float
     *
     * @ORM\Column(name="budgetmax", type="float" , nullable=true)
     */
    private $budgetmax;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime" , nullable=true)
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="remarque", type="text" , nullable=true)
     */
    private $remarque;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

   
    public function __construct(){
        $this->dcr=new \DateTime();
    }


    
     /**
     * Set typec
     *
     * @param string $typec
     * @return Personalisationcircuit
     */
    public function setTypec($typec)
    {
        $this->typec = $typec;

        return $this;
    }

    /**
     * Get typec
     *
     * @return string 
     */
    public function getTypec()
    {
        return $this->typec;
    }
    
    
    /**
     * Set formule
     *
     * @param string $formule
     * @return Personalisationcircuit
     */
    public function setFormule($formule)
    {
        $this->formule = $formule;

        return $this;
    }

    /**
     * Get formule
     *
     * @return string 
     */
    public function getFormule()
    {
        return $this->formule;
    }
   
        
     /**
     * Set nbradult
     *
     * @param integer $nbradult
     * @return Personalisationcircuit
     */
    public function setNbradult($nbradult)
    {
        $this->nbradult = $nbradult;

        return $this;
    }
    
    
    /**
     * Get nbradult
     *
     * @return integer 
     */
    public function getNbradult()
    {
        return $this->nbradult;
    }

    /**
     * Set nbrenf
     *
     * @param integer $nbrenf
     * @return Personalisationcircuit
     */
    public function setNbrenf($nbrenf)
    {
        $this->nbrenf = $nbrenf;

        return $this;
    }

    /**
     * Get nbrenf
     *
     * @return integer 
     */
    public function getNbrenf()
    {
        return $this->nbrenf;
    }

     /**
     * Set nbrbebe
     *
     * @param integer $nbrbebe
     * @return Personalisationcircuit
     */
    public function setNbrbebe($nbrbebe)
    {
        $this->nbrbebe = $nbrbebe;

        return $this;
    }

    /**
     * Get nbrbebe
     *
     * @return integer 
     */
    public function getNbrbebe()
    {
        return $this->nbrbebe;
    }
    
    
    /**
     * Set budgetmin
     *
     * @param float $budgetmin
     * @return Personalisationcircuit
     */
    public function setBudgetmin($budgetmin)
    {
        $this->budgetmin = $budgetmin;

        return $this;
    }

    /**
     * Get budgetmin
     *
     * @return float 
     */
    public function getBudgetmin()
    {
        return $this->budgetmin;
    }
    
    
    
     /**
     * Set budgetmax
     *
     * @param float $budgetmax
     * @return Personalisationcircuit
     */
    public function setBudgetmax($budgetmax)
    {
        $this->budgetmax = $budgetmax;

        return $this;
    }

    /**
     * Get budgetmax
     *
     * @return float 
     */
    public function getBudgetmax()
    {
        return $this->budgetmax;
    }
 
    /**
     * Get nbrch
     *
     * @return integer 
     */
    public function getNbrch()
    {
        return $this->nbrch;
    }
    
    /**
     * Set nbrch
     *
     * @param integer $nbrch
     * @return Personalisationcircuit
     */
    public function setNbrch($nbrch)
    {
        $this->nbrch = $nbrch;

        return $this;
    }
    /**
     * Set remarque
     *
     * @param string $remarque
     * @return Personalisationcircuit
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string 
     */
    public function getRemarque()
    {
        return $this->remarque;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Personalisationcircuit
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Personalisationcircuit
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set cuircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuit $cuircuit
     * @return Personalisationcircuit
     */
    public function setCuircuit(\Btob\CuircuitBundle\Entity\Cuircuit $cuircuit = null)
    {
        $this->cuircuit = $cuircuit;

        return $this;
    }

    /**
     * Get cuircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Cuircuit
     */
    public function getCuircuit()
    {
        return $this->cuircuit;
    }

    /**
     * Set agent
     *
     * @param \User\UserBundle\Entity\User $agent
     * @return Personalisationcircuit
     */
    public function setAgent(\User\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }
    
    /**

     * Add categories

     *

     * @param \Btob\CuircuitBundle\Entity\Tpcircuit $personalisationcircuit

     * @return Personalisationcircuit

     */

    public function addPersonalisationcircuit(\Btob\CuircuitBundle\Entity\Tpcircuit $personalisationcircuit)

    {

        $this->personalisationcircuit[] = $personalisationcircuit;



        return $this;

    }



    /**

     * Remove personalisationcircuit

     *

     * @param \Btob\CuircuitBundle\Entity\Tpcircuit $personalisationcircuit

     */

    public function removePersonalisationcircuit(\Btob\CuircuitBundle\Entity\Tpcircuit $personalisationcircuit)

    {

        $this->personalisationcircuit->removeElement($personalisationcircuit);

    }



    /**

     * Get personalisationcircuit

     *

     * @return \Doctrine\Common\Collections\Collection 

     */

    public function getPersonalisationcircuit()

    {

        return $this->personalisationcircuit;

    }
    
    
        
    

    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Personalisationcircuit
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Personalisationcircuit
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return Personalisationcircuit
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return Personalisationcircuit
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return Personalisationcircuit
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Personalisationcircuit
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }
}
