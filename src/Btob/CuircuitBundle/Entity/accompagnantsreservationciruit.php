<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * accompagnantsreservationciruit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\accompagnantsreservationciruitRepository")
 */
class accompagnantsreservationciruit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbadultes", type="integer")
     */
    private $nbadultes;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbenfants", type="integer")
     */
    private $nbenfants;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbbebe", type="integer")
     */
    private $nbbebe;

    /**
     * @var integer
     *
     * @ORM\Column(name="reservationcircuit", type="integer")
     */
    private $reservationcircuit;

    /**
     * @var integer
     *
     * @ORM\Column(name="ciruit", type="integer")
     */
    private $ciruit;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbadultes
     *
     * @param integer $nbadultes
     *
     * @return accompagnantsreservationciruit
     */
    public function setNbadultes($nbadultes)
    {
        $this->nbadultes = $nbadultes;

        return $this;
    }

    /**
     * Get nbadultes
     *
     * @return integer
     */
    public function getNbadultes()
    {
        return $this->nbadultes;
    }

    /**
     * Set nbenfants
     *
     * @param integer $nbenfants
     *
     * @return accompagnantsreservationciruit
     */
    public function setNbenfants($nbenfants)
    {
        $this->nbenfants = $nbenfants;

        return $this;
    }

    /**
     * Get nbenfants
     *
     * @return integer
     */
    public function getNbenfants()
    {
        return $this->nbenfants;
    }

    /**
     * Set nbbebe
     *
     * @param integer $nbbebe
     *
     * @return accompagnantsreservationciruit
     */
    public function setNbbebe($nbbebe)
    {
        $this->nbbebe = $nbbebe;

        return $this;
    }

    /**
     * Get nbbebe
     *
     * @return integer
     */
    public function getNbbebe()
    {
        return $this->nbbebe;
    }

    /**
     * Set reservationcircuit
     *
     * @param integer $reservationcircuit
     *
     * @return accompagnantsreservationciruit
     */
    public function setReservationcircuit($reservationcircuit)
    {
        $this->reservationcircuit = $reservationcircuit;

        return $this;
    }

    /**
     * Get reservationcircuit
     *
     * @return integer
     */
    public function getReservationcircuit()
    {
        return $this->reservationcircuit;
    }

    /**
     * Set ciruit
     *
     * @param integer $ciruit
     *
     * @return accompagnantsreservationciruit
     */
    public function setCiruit($ciruit)
    {
        $this->ciruit = $ciruit;

        return $this;
    }

    /**
     * Get ciruit
     *
     * @return integer
     */
    public function getCiruit()
    {
        return $this->ciruit;
    }
}

