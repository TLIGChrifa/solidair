<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coordonneesreservationcr
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\CoordonneesreservationcrRepository")
 */
class Coordonneesreservationcr
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adultes", type="string", length=255)
     */
    private $adultes;

    /**
     * @var string
     *
     * @ORM\Column(name="adulte", type="string", length=255)
     */
    private $adulte;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="demande", type="string", length=255)
     */
    private $demande;

    /**
     * @var string
     *
     * @ORM\Column(name="cr", type="string", length=255)
     */
    private $cr;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer")
     */
    private $mobile;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Coordonneesreservationcr
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Coordonneesreservationcr
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adultes
     *
     * @param string $adultes
     *
     * @return Coordonneesreservationcr
     */
    public function setAdultes($adultes)
    {
        $this->adultes = $adultes;

        return $this;
    }

    /**
     * Get adultes
     *
     * @return string
     */
    public function getAdultes()
    {
        return $this->adultes;
    }

    /**
     * Set adulte
     *
     * @param string $adulte
     *
     * @return Coordonneesreservationcr
     */
    public function setAdulte($adulte)
    {
        $this->adulte = $adulte;

        return $this;
    }

    /**
     * Get adulte
     *
     * @return string
     */
    public function getAdulte()
    {
        return $this->adulte;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Coordonneesreservationcr
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set demande
     *
     * @param string $demande
     *
     * @return Coordonneesreservationcr
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return string
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * Set cr
     *
     * @param string $cr
     *
     * @return Coordonneesreservationcr
     */
    public function setCr($cr)
    {
        $this->cr = $cr;

        return $this;
    }

    /**
     * Get cr
     *
     * @return string
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return Coordonneesreservationcr
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }
}

