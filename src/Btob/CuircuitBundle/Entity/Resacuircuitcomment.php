<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resacuircuitcomment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\ResacuircuitcommentRepository")
 */
class Resacuircuitcomment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=30)
     */
    private $ip;
    /**
     * @ORM\ManyToOne(targetEntity="Reservationcircuit", inversedBy="resacuircuitcomment")
     * @ORM\JoinColumn(name="res_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $reservationcircuit;
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="resacuircuitcomment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $user;

    public function __construct()
    {
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Resacuircuitcomment
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Resacuircuitcomment
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Resacuircuitcomment
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set reservationcircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Reservationcircuit $reservationcircuit
     * @return Resacuircuitcomment
     */
    public function setReservationcircuit(\Btob\CuircuitBundle\Entity\Reservationcircuit $reservationcircuit = null)
    {
        $this->reservationcircuit = $reservationcircuit;

        return $this;
    }

    /**
     * Get reservationcircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Reservationcircuit
     */
    public function getReservationcircuit()
    {
        return $this->reservationcircuit;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return Resacuircuitcomment
     */
    public function setUser(\User\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
