<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservationcircuit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\ReservationcircuitRepository")
 */
class Reservationcircuit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="datetime")
     */
    private $dcr;
    
    

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;
    
    /**
     * @var float
     *
     * @ORM\Column(name="avance", type="float" , nullable=true)
     */
    private $avance;
    
    /**
     * @var \Date
     *
     * @ORM\Column(name="dated", type="date", nullable =true)
     */
    private $dated;
      /**
     * @var date
     *
     * @ORM\Column(name="datef", type="date" , nullable = true)
     */
    protected $datef;
          /**
    * @var integer
     *
     * @ORM\Column(name="nbnuits", type="integer")
     */
    private $nbnuits;
         /**
    * @var integer
     *
     * @ORM\Column(name="nbadultes", type="integer")
     */
    private $nbadultes;
       /**
    * @var integer
     *
     * @ORM\Column(name="nbenfants", type="integer")
     */
    private $nbenfants;


    /**
     * @var string
     *
     * @ORM\Column(name="resultatfinal", type="string", nullable=true)
     */
    private $resultatfinal;

    /**
     * @var string
     *
     * @ORM\Column(name="numautoris", type="string", nullable=true)
     */
    private $numautoris;
    

     /**
      * @ORM\ManyToOne(targetEntity="Cuircuit", inversedBy="reservationcircuit")
      * @ORM\JoinColumn(name="cuircuit_id", referencedColumnName="id",onDelete="CASCADE")
      */
     protected $cuircuit;
    
    // /**
    //  * @ORM\ManyToOne(targetEntity="Cuircuitprice", inversedBy="reservationcircuit")
    //  * @ORM\JoinColumn(name="cuircuitprice_id", referencedColumnName="id",onDelete="CASCADE")
    //  */
  //  protected $cuircuitprice;
    
    /**
     * @ORM\ManyToOne(targetEntity="Btob\HotelBundle\Entity\Clients", inversedBy="reservationcircuit")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $client;
    // /**
    //  * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User", inversedBy="reservationcircuit")
    //  * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
    //  */
    // protected $user;

     /**
     * @var float
     *
     * @ORM\Column(name="montantpaye", type="float",nullable=true)
     */
    private $montantpaye;
      /**
     * @var integer
     *
     * @ORM\Column(name="numcheque", type="integer",nullable=true)
     */
    private $numcheque;
     /**
     * @var string
     *
     * @ORM\Column(name="typepayement", type="string",nullable=true)
     */
    private $typepayement;

    // /**
    //  * @ORM\OneToMany(targetEntity="Resacuircuitcomment", mappedBy="reservationcircuit", cascade={"remove"})
    //  */
    // protected $resacuircuitcomment;
   
    // /**
    //  * @ORM\OneToMany(targetEntity="Reservationcdetail", mappedBy="reservationcircuit", cascade={"remove"})
    //  */
    // protected $reservationcdetail;
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resacuircuitcomment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dcr = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Reservationcircuit
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }

    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }

    

    /**
     * Set total
     *
     * @param float $total
     * @return Reservationcircuit
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }


    // /**
    //  * Set cuircuitprice
    //  *
    //  * @param \Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice
    //  * @return Reservationcircuit
    //  */
    // public function setCuircuitprice(\Btob\CuircuitBundle\Entity\Cuircuitprice $cuircuitprice = null)
    // {
    //     $this->cuircuitprice = $cuircuitprice;

    //     return $this;
    // }

    // /**
    //  * Get cuircuitprice
    //  *
    //  * @return \Btob\CuircuitBundle\Entity\Cuircuitprice 
    //  */
    // public function getCuircuitprice()
    // {
    //     return $this->cuircuitprice;
    // }
    
       
    
    /**
     * Set cuircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Cuircuit $cuircuit
     * @return Reservationcircuit
     */
    public function setCuircuit(\Btob\CuircuitBundle\Entity\Cuircuit $cuircuit = null)
    {
        $this->cuircuit = $cuircuit;

        return $this;
    }

    /**
     * Get cuircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Cuircuit 
     */
    public function getCuircuit()
    {
        return $this->cuircuit;
    }

    /**
     * Set client
     *
     * @param \Btob\HotelBundle\Entity\Clients $client
     * @return Reservationcircuit
     */
    public function setClient(\Btob\HotelBundle\Entity\Clients $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Btob\HotelBundle\Entity\Clients 
     */
    public function getClient()
    {
        return $this->client;
    }

    // /**
    //  * Set user
    //  *
    //  * @param \User\UserBundle\Entity\User $user
    //  * @return Reservationcircuit
    //  */
    // public function setUser(\User\UserBundle\Entity\User $user = null)
    // {
    //     $this->user = $user;

    //     return $this;
    // }

    // /**
    //  * Get user
    //  *
    //  * @return \User\UserBundle\Entity\User 
    //  */
    // public function getUser()
    // {
    //     return $this->user;
    // }

   


    // /**
    //  * Add reservationcdetail
    //  *
    //  * @param \Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail
    //  * @return Reservationcircuit
    //  */
    // public function addReservationcdetail(\Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail)
    // {
    //     $this->reservationcdetail[] = $reservationcdetail;

    //     return $this;
    // }

    // /**
    //  * Remove reservationcdetail
    //  *
    //  * @param \Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail
    //  */
    // public function removeReservationcdetail(\Btob\CuircuitBundle\Entity\Reservationcdetail $reservationcdetail)
    // {
    //     $this->reservationcdetail->removeElement($reservationcdetail);
    // }

    // /**
    //  * Get reservationcdetail
    //  *
    //  * @return \Doctrine\Common\Collections\Collection 
    //  */
    // public function getReservationcdetail()
    // {
    //     return $this->reservationcdetail;
    // }

        /**
     * Set etat
     *
     * @param integer $etat
     * @return Reservation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set avance
     *
     * @param float $avance
     * @return Reservation
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return float 
     */
    public function getAvance()
    {
        return $this->avance;
    }

        /**
     * Set resultatfinal
     *
     * @param string $resultatfinal
     * @return Reservation
     */
    public function setResultatfinal($resultatfinal)
    {
        $this->resultatfinal = $resultatfinal;
        return $this;
    }

    /**
     * Get resultatfinal
     *
     * @return string
     */
    public function getResultatfinal()
    {
        return $this->resultatfinal;
    }

    /**
     * Set numautoris
     *
     * @param string $numautoris
     * @return Reservation
     */
    public function setNumautoris($numautoris)
    {
        $this->numautoris = $numautoris;
        return $this;
    }

    /**
     * Get numautoris
     *
     * @return string
     */
    public function getNumautoris()
    {
        return $this->numautoris;
    }

    // /**
    //  * Add resacuircuitcomment
    //  *
    //  * @param \Btob\CuircuitBundle\Entity\Resacuircuitcomment $resacuircuitcomment
    //  * @return Reservationcircuit
    //  */
    // public function addResacuircuitcomment(\Btob\CuircuitBundle\Entity\Resacuircuitcomment $resacuircuitcomment)
    // {
    //     $this->resacuircuitcomment[] = $resacuircuitcomment;

    //     return $this;
    // }

    // /**
    //  * Remove resacuircuitcomment
    //  *
    //  * @param \Btob\CuircuitBundle\Entity\Resacuircuitcomment $resacuircuitcomment
    //  */
    // public function removeResacuircuitcomment(\Btob\CuircuitBundle\Entity\Resacuircuitcomment $resacuircuitcomment)
    // {
    //     $this->resacuircuitcomment->removeElement($resacuircuitcomment);
    // }

    // /**
    //  * Get resacuircuitcomment
    //  *
    //  * @return \Doctrine\Common\Collections\Collection 
    //  */
    // public function getResacuircuitcomment()
    // {
    //     return $this->resacuircuitcomment;
    // }




    /**
     * Set dated
     *
     * @param \DateTime $dated
     *
     * @return Reservationcircuit
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }

    /**
     * Get dated
     *
     * @return \DateTime
     */
    public function getDated()
    {
        return $this->dated;
    }

    /**
     * Set datef
     *
     * @param \DateTime $datef
     *
     * @return Reservationcircuit
     */
    public function setDatef($datef)
    {
        $this->datef = $datef;

        return $this;
    }

    /**
     * Get datef
     *
     * @return \DateTime
     */
    public function getDatef()
    {
        return $this->datef;
    }

    /**
     * Set nbnuits
     *
     * @param integer $nbnuits
     *
     * @return Reservationcircuit
     */
    public function setNbnuits($nbnuits)
    {
        $this->nbnuits = $nbnuits;

        return $this;
    }

    /**
     * Get nbnuits
     *
     * @return integer
     */
    public function getNbnuits()
    {
        return $this->nbnuits;
    }

    /**
     * Set nbadultes
     *
     * @param integer $nbadultes
     *
     * @return Reservationcircuit
     */
    public function setNbadultes($nbadultes)
    {
        $this->nbadultes = $nbadultes;

        return $this;
    }

    /**
     * Get nbadultes
     *
     * @return integer
     */
    public function getNbadultes()
    {
        return $this->nbadultes;
    }

    /**
     * Set nbenfants
     *
     * @param integer $nbenfants
     *
     * @return Reservationcircuit
     */
    public function setNbenfants($nbenfants)
    {
        $this->nbenfants = $nbenfants;

        return $this;
    }

    /**
     * Get nbenfants
     *
     * @return integer
     */
    public function getNbenfants()
    {
        return $this->nbenfants;
    }

    /**
     * Set montantpaye
     *
     * @param float $montantpaye
     *
     * @return Reservationcircuit
     */
    public function setMontantpaye($montantpaye)
    {
        $this->montantpaye = $montantpaye;

        return $this;
    }

    /**
     * Get montantpaye
     *
     * @return float
     */
    public function getMontantpaye()
    {
        return $this->montantpaye;
    }

    /**
     * Set numcheque
     *
     * @param integer $numcheque
     *
     * @return Reservationcircuit
     */
    public function setNumcheque($numcheque)
    {
        $this->numcheque = $numcheque;

        return $this;
    }

    /**
     * Get numcheque
     *
     * @return integer
     */
    public function getNumcheque()
    {
        return $this->numcheque;
    }

    /**
     * Set typepayement
     *
     * @param string $typepayement
     *
     * @return Reservationcircuit
     */
    public function setTypepayement($typepayement)
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    /**
     * Get typepayement
     *
     * @return string
     */
    public function getTypepayement()
    {
        return $this->typepayement;
    }
}
