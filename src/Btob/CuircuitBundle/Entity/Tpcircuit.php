<?php

namespace Btob\CuircuitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tpcircuit
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\CuircuitBundle\Entity\TpcircuitRepository")
 */
class Tpcircuit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Personalisationcircuit", inversedBy="tpcircuit")
     * @ORM\JoinColumn(name="personalisationcircuit_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $personalisationcircuit;
     /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dated", type="datetime" , nullable=true)
     */
    private $dated;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dater", type="datetime" , nullable=true)
     */
    private $dater;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dateflex", type="boolean" , nullable=true)
     */
    private $dateflex;    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personalisationcircuit
     *
     * @param \Btob\CuircuitBundle\Entity\Personalisationcircuit $personalisationcircuit
     * @return Tpcircuit
     */
    public function setPersonalisationcircuit(\Btob\CuircuitBundle\Entity\Personalisationcircuit $personalisationcircuit = null)
    {
        $this->personalisationcircuit = $personalisationcircuit;

        return $this;
    }

    /**
     * Get personalisationcircuit
     *
     * @return \Btob\CuircuitBundle\Entity\Personalisationcircuit
     */
    public function getPersonalisationcircuit()
    {
        return $this->personalisationcircuit;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Tpcircuit
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }
    
    
     /**
     * Set dateflex
     *
     * @param boolean $dateflex
     * @return Tpcircuit
     */
    public function setDateflex($dateflex)
    {
        $this->dateflex = $dateflex;

        return $this;
    }

    /**
     * Get dateflex
     *
     * @return boolean 
     */
    public function getDateflex()
    {
        return $this->dateflex;
    }    
    
    
    /**
     * Set dated
     *
     * @param \DateTime $dated
     * @return Tpcircuit
     */
    public function setDated($dated)
    {
        $this->dated = $dated;

        return $this;
    }
    /**
     * Get dated
     *
     * @return \DateTime 
     */
    public function getDated()
    {
        return $this->dated;
    }

    
    
    /**
     * Set dater
     *
     * @param \DateTime $dater
     * @return Tpcircuit
     */
    public function setDater($dater)
    {
        $this->dater = $dater;

        return $this;
    }
    /**
     * Get dater
     *
     * @return \DateTime 
     */
    public function getDater()
    {
        return $this->dater;
    }    
	
}
