<?php

namespace Btob\OptionvoitureBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\OptionvoitureBundle\Entity\Opvoiture;
use Btob\OptionvoitureBundle\Form\OpvoitureType;

/**
 * Opvoiture controller.
 *
 */
class OpvoitureController extends Controller
{

    /**
     * Lists all Opvoiture entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BtobOptionvoitureBundle:Opvoiture')->findAll();

        return $this->render('BtobOptionvoitureBundle:Opvoiture:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Opvoiture entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Opvoiture();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('opvoiture_show', array('id' => $entity->getId())));
        }

        return $this->render('BtobOptionvoitureBundle:Opvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Opvoiture entity.
     *
     * @param Opvoiture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Opvoiture $entity)
    {
        $form = $this->createForm(new OpvoitureType(), $entity, array(
            'action' => $this->generateUrl('opvoiture_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Opvoiture entity.
     *
     */
    public function newAction()
    {
        $entity = new Opvoiture();
        $form   = $this->createCreateForm($entity);

        return $this->render('BtobOptionvoitureBundle:Opvoiture:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Opvoiture entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOptionvoitureBundle:Opvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Opvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobOptionvoitureBundle:Opvoiture:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Opvoiture entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOptionvoitureBundle:Opvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Opvoiture entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BtobOptionvoitureBundle:Opvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Opvoiture entity.
    *
    * @param Opvoiture $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Opvoiture $entity)
    {
        $form = $this->createForm(new OpvoitureType(), $entity, array(
            'action' => $this->generateUrl('opvoiture_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Opvoiture entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobOptionvoitureBundle:Opvoiture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Opvoiture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('opvoiture_edit', array('id' => $id)));
        }

        return $this->render('BtobOptionvoitureBundle:Opvoiture:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Opvoiture entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BtobOptionvoitureBundle:Opvoiture')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Opvoiture entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('opvoiture'));
    }

    /**
     * Creates a form to delete a Opvoiture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('opvoiture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function delete1Action(Opvoiture $classe) {
        $em = $this->getDoctrine()->getManager();

        if (!$classe) {
            throw new NotFoundHttpException("option non trouvée");
        }
        $em->remove($classe);
        $em->flush();
        return $this->redirect($this->generateUrl('opvoiture'));
    }
    public function add1Action() {
        $pays = new Opvoiture();
        $form = $this->createForm(new OpvoitureType(), $pays);
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            //echo "<pre>";print_r($page);exit;
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($pays);
                $em->flush();
                return $this->redirect($this->generateUrl('opvoiture'));
            } else {
                echo $form->getErrors();
            }
        }
        return $this->render('BtobOptionvoitureBundle:Opvoiture:form.html.twig', array('form' => $form->createView()));
    }

    public function edit1Action($id) {
        $request = $this->get('request');
        $voiture = $this->getDoctrine()
            ->getRepository('BtobOptionvoitureBundle:Opvoiture')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OpvoitureType(), $voiture);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('opvoiture'));
        } else {
            echo $form->getErrors();
        }
        return $this->render('BtobOptionvoitureBundle:Opvoiture:form.html.twig', array('form' => $form->createView(), 'id' => $id,)
        );
    }
}
