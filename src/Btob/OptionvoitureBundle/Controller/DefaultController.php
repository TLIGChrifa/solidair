<?php

namespace Btob\OptionvoitureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BtobOptionvoitureBundle:Default:index.html.twig', array('name' => $name));
    }
}
