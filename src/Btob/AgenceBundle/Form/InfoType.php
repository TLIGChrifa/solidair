<?php

namespace Btob\AgenceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text', array('label' => "Nom d'agence", 'required' => true))
           // ->add('logo', NULL , array('label' => "Logo","attr"=> array(), 'required' => false))
            ->add('adresse', 'text', array('label' => "Adresse", 'required' => false))
            ->add('telephone', 'text', array('label' => "Telephone", 'required' => false))
            ->add('fax', 'text', array('label' => "Fax", 'required' => false))
            ->add('email', 'text', array('label' => "Email", 'required' => false))
            ->add('heurelv', 'text', array('label' => "Heure du travail du lundi au vendredi", 'required' => false))
            ->add('heures', 'text', array('label' => "Heure de travail Samdi", 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Btob\AgenceBundle\Entity\Info'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'btob_agencebundle_info';
    }
}
