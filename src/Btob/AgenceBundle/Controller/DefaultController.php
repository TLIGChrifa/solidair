<?php

namespace Btob\AgenceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BtobAgenceBundle:Default:index.html.twig', array('name' => $name));
    }
}
