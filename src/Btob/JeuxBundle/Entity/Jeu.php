<?php

namespace Btob\JeuxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jeu
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Btob\JeuxBundle\Entity\JeuRepository")
 */
class Jeu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dcr", type="date")
     */
    private $dcr;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255, nullable=true)
     */
    private $question;
    /**
     * @ORM\OneToMany(targetEntity="Choix", mappedBy="qustion")
     */
    protected $choix;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Jeu
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }



    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Jeu
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->choix = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dcr = new \DateTime();

    }

    /**
     * Add choix
     *
     * @param \Btob\JeuxBundle\Entity\Choix $choix
     * @return Jeu
     */
    public function addChoix(\Btob\JeuxBundle\Entity\Choix $choix)
    {
        $this->choix[] = $choix;

        return $this;
    }

    /**
     * Remove choix
     *
     * @param \Btob\JeuxBundle\Entity\Choix $choix
     */
    public function removeChoix(\Btob\JeuxBundle\Entity\Choix $choix)
    {
        $this->choix->removeElement($choix);
    }

    /**
     * Get choix
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * Set dcr
     *
     * @param \DateTime $dcr
     * @return Jeu
     */
    public function setDcr($dcr)
    {
        $this->dcr = $dcr;

        return $this;
    }
public  function  __toString(){
    return $this->question;
}
    /**
     * Get dcr
     *
     * @return \DateTime 
     */
    public function getDcr()
    {
        return $this->dcr;
    }
}
