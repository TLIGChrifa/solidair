<?php 

namespace Btob\JeuxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Btob\HotelBundle\Common\Tools;



use Btob\JeuxBundle\Entity\Choix;

use Btob\JeuxBundle\Entity\Jeu;

use Btob\JeuxBundle\Entity\Reponse;

use Btob\HotelBundle\Entity\Clients;

use Btob\HotelBundle\Form\ClientsType;

use Btob\CuircuitBundle\Entity\Resacircui;

use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $Question = $this->getDoctrine()->getRepository("BtobJeuxBundle:Jeu")->findBy( array(),array(),

                     1 );



        $client = new Clients();

        $form = $this->createForm(new ClientsType(), $client);

        $request = $this->get('request');

        if ($request->getMethod() == 'POST') {

            $post = $request->request->get('btob_hotelbundle_clients');

            //Tools::dump($post["cin"],true);

            $cin = $post["cin"];

            $testclient = $this->getDoctrine()->getRepository('BtobHotelBundle:Clients')->findOneBy(array('cin' => $cin));

            if ($testclient != null) {

                $client = $testclient;

            }

            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($client);

                $em->flush();

                $question = $request->request->get('question');

                $optionsRadios = $request->request->get('optionsRadios');

                if($optionsRadios != null){

                $Reponse = new Reponse();

                    $Reponse->setChoix($optionsRadios);

                    $Reponse->setQuestion($question);

                    $Reponse->setAgent($this->get('security.context')->getToken()->getUser());

                    $Reponse->setClient($client);

                    $em->persist($Reponse);

                    $em->flush();





                    $client = new Clients();



                    $form = $this->createForm(new ClientsType(), $client);

                    $request->getSession()->getFlashBag()->add('notiQuestion', 'Votre message a bien été envoyé. Merci.');





                }

                return $this->redirect($this->generateUrl('btob_jeux_homepage'));

            } else {

                echo $form->getErrors();

            }

        }

        return $this->render('BtobJeuxBundle:Default:reservation.html.twig', array('form' => $form->createView() , 'Question'=>$Question));

    }





    public function listreponseAction()

    {

        $entities = $this->getDoctrine()->getRepository("BtobJeuxBundle:Reponse")->findAll();

        $user = $this->get('security.context')->getToken()->getUser();

        $dt = new \DateTime();
        $dt->modify('-2 month');
        $dcrfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $dcrto = $dt->format("d/m/Y");
        $etat = "";
        $client = "";
        $numres = "";
        $dt = new \DateTime();
        $dt->modify('-2 month');
        $datedfrom = $dt->format("d/m/Y");
        $dt->modify('+4 month');
        $datefto = $dt->format("d/m/Y");
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $dcrto = $request->request->get('dcrto');
            $dcrfrom = $request->request->get('dcrfrom');
            $etat = $request->request->get('etat');
            $client = trim($request->request->get('client'));
            $numres = trim($request->request->get('numres'));
            $datefto = $request->request->get('datefto');
            $datedfrom = $request->request->get('datedfrom');
        }
        $em = $this->getDoctrine()->getManager();
        // reset notification
        $user = $this->get('security.context')->getToken()->getUser();
        $notification = $this->getDoctrine()->getRepository('BtobHotelBundle:Notification')->findBy(array('user' => $user));

        foreach ($notification as $value) {
            $value->setNotif(0);
            $em->persist($value);
            $em->flush();
        }

            $entities = $em->getRepository("BtobJeuxBundle:Reponse")->findAll();




        if ($client != "") {
            $tabsearch = array();
            foreach ($entities as $value) {
                if (strtoupper($value->getClient()->getName()) == strtoupper($client) || $value->getClient()->getCin() == $client || strtoupper($value->getClient()->getPname()) == strtoupper($client) || $value->getClient()->getEmail() == $client) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($dcrto != "") {
            $tabsearch = array();
            $tab = explode('/', $dcrto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datedfrom != "") {
            $tabsearch = array();
            $tab = explode('/', $datedfrom);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dty - $dtx) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }
        if ($datefto != "") {
            $tabsearch = array();
            $tab = explode('/', $datefto);
            $dtx = $tab[2] . $tab[1] . $tab[0];
            foreach ($entities as $value) {
                $dty = $value->getDcr()->format('Ymd');
                if (($dtx - $dty) >= 0) {
                    $tabsearch[] = $value;
                }
            }
            $entities = $tabsearch;
        }

        return $this->render('BtobJeuxBundle:Default:listreservation.html.twig', array(
            'entities' => $entities,
            'dcrto' => $dcrto,
            'dcrfrom' => $dcrfrom,
            'etat' => $etat,
            'client' => $client,
            'numres' => $numres,
            'datefto' => $datefto,
            'datedfrom' => $datedfrom,
        ));

    }



    public function deletereponseAction( $id)

    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BtobJeuxBundle:Reponse')->find($id);



        $em->remove($entity);

        $em->flush();





        return $this->redirect($this->generateUrl('btob_jeux_list_homepage'));

    }







}

