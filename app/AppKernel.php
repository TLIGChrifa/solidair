<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new Btob\HotelBundle\BtobHotelBundle(),
            new Btob\DashBundle\BtobDashBundle(),
         //   new User\UserBundle\UserUserBundle(),
            new Btob\OmraBundle\BtobOmraBundle(),
            new Btob\CuircuitBundle\BtobCuircuitBundle(),
          //  new Btob\SiminaireBundle\BtobSiminaireBundle(),
            new Btob\CroissiereBundle\BtobCroissiereBundle(),
            new Btob\TransfertBundle\BtobTransfertBundle(),
	    new Btob\SpaBundle\BtobSpaBundle(),
            new Btob\CaddyBundle\BtobCaddyBundle(),
            new Btob\SejourBundle\BtobSejourBundle(),
            new Btob\VoleBundle\BtobVoleBundle(),
            new Btob\BannaireBundle\BtobBannaireBundle(),
            new Btob\ActiviteBundle\BtobActiviteBundle(),
	    new Btob\VoitureBundle\BtobVoitureBundle(),
            new Btob\ResvoitureBundle\BtobResvoitureBundle(),
           // new Btob\ResomraBundle\BtobResomraBundle(),
            new Btob\ClasseBundle\BtobClasseBundle(),
            new Btob\OptionvoitureBundle\BtobOptionvoitureBundle(),
	   new Btob\JeuxBundle\BtobJeuxBundle(),
            new Front\BtobBundle\FrontBtobBundle(),
            new Btob\ParcBundle\BtobParcBundle(),
           // new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Btob\EvenementBundle\BtobEvenementBundle(),
            new Btob\SiminaireBundle\BtobSiminaireBundle(),
            new Btob\AssuranceBundle\BtobAssuranceBundle(),
            new Btob\AgenceBundle\BtobAgenceBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            //new FOS\UserBundle\FOSUserBundle(),


        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
