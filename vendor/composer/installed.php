<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'symfony/framework-standard-edition',
  ),
  'versions' => 
  array (
    'doctrine/annotations' => 
    array (
      'pretty_version' => 'v1.2.7',
      'version' => '1.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f25c8aab83e0c3e976fd7d19875f198ccf2f7535',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => 'v1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6f544a20f4807e81f7044d31e679ccbb1866dc3',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c1e4eef75f310ea1b3e30945e9f06e652128b8a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => 'v2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a579557bc689580c19fee4e27487a67fe60defc0',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => 'v2.5.5',
      'version' => '2.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f8c05cd5225a320d56d4bfdb4772f10d045a0c9',
    ),
    'doctrine/doctrine-bundle' => 
    array (
      'pretty_version' => '1.6.4',
      'version' => '1.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dd40b0a7fb16658cda9def9786992b8df8a49be7',
    ),
    'doctrine/doctrine-cache-bundle' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '18c600a9b82f6454d2e81ca4957cdd56a1cf3504',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '90b2128806bfde671b6952ab8bea493942c1fdae',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e884e78f9f0eb1329e445619e04456e64d8051d',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '83893c552fd2045dd78aef794c31e694c37c0b8c',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => 'v2.5.5',
      'version' => '2.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '73e4be7c7b3ba26f96b781a40b33feba4dfa6d45',
    ),
    'friendsofsymfony/user-bundle' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2fc8a023d7ab482321cf7ec810ed49eab40eb50f',
    ),
    'incenteev/composer-parameter-handler' => 
    array (
      'pretty_version' => 'v2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7ce7f06136109e81d1cb9d57066c4d4a99cf1cc',
    ),
    'jdorn/sql-formatter' => 
    array (
      'pretty_version' => 'v1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
    ),
    'knplabs/knp-components' => 
    array (
      'pretty_version' => 'v1.3.10',
      'version' => '1.3.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc1755ba2b77f83a3d3c99e21f3026ba2a1429be',
    ),
    'knplabs/knp-paginator-bundle' => 
    array (
      'pretty_version' => 'v2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f4ece5b347121b9fe13166264f197f90252d4bd2',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bad29cb8d18ab0315e6c477751418a82c850d558',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9b97968bcde1c4de2a5ec6cbd06a0f6c919b46e',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
    'sensio/distribution-bundle' => 
    array (
      'pretty_version' => 'v5.0.14',
      'version' => '5.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e64947de9ebc37732a62f5115164484a9bee7fa6',
    ),
    'sensio/framework-extra-bundle' => 
    array (
      'pretty_version' => 'v3.0.16',
      'version' => '3.0.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '507a15f56fa7699f6cc8c2c7de4080b19ce22546',
    ),
    'sensio/generator-bundle' => 
    array (
      'pretty_version' => 'v3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d3c9c4864142dc6a368fa0d952a8b83215e8740',
    ),
    'sensiolabs/security-checker' => 
    array (
      'pretty_version' => 'v4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '116027b57b568ed61b7b1c80eeb4f6ee9e8c599c',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v5.4.4',
      'version' => '5.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '545ce9136690cea74f98f86fbb9c92dd9ab1a756',
    ),
    'symfony/asset' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/browser-kit' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/class-loader' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/css-selector' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/debug' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/debug-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/dependency-injection' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/doctrine-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/dom-crawler' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/event-dispatcher' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/form' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/framework-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/framework-standard-edition' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'symfony/http-foundation' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/http-kernel' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/inflector' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/intl' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/ldap' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/monolog-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/monolog-bundle' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f2d2d62530cd66be361216107869a3b061045db',
    ),
    'symfony/options-resolver' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/phpunit-bridge' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a56acfdb96be7c82f4bcf16b7c77f1960690b0e2',
    ),
    'symfony/polyfill-apcu' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d4474f447403c3348e37b70acc2b95475b7befa',
    ),
    'symfony/polyfill-intl-icu' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2d6e2b20d457603eefb6e614286c22efca30fdb4',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e79d363049d1c2128f133a2667e4f4190904f7f4',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1dd42b9b89556f18092f3d1ada22cb05ac85383c',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '13ce343935f0f91ca89605a2f6ca6f5c2f3faac2',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '746bce0fca664ac0a575e465f65c6643faddf7fb',
    ),
    'symfony/process' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/property-access' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/property-info' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/proxy-manager-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security-core' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security-csrf' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security-guard' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/security-http' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/serializer' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/stopwatch' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/swiftmailer-bundle' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7b7bd6bb6e9b32ebc5f9778f94d4b4e4af5d069',
    ),
    'symfony/symfony' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b96a144bc875684f3338f697687147a2696d73eb',
    ),
    'symfony/templating' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/twig-bridge' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/twig-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/validator' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/web-profiler-bundle' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/workflow' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/yaml' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.28.2',
      'version' => '1.28.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b22ce0eb070e41f7cba65d78fe216de29726459c',
    ),
  ),
);
