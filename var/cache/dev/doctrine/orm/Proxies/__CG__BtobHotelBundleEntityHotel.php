<?php

namespace Proxies\__CG__\Btob\HotelBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Hotel extends \Btob\HotelBundle\Entity\Hotel implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'id', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'name', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'maps', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'star', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'maxenfant', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'minenfant', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'contrat', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'shortdesc', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'visite', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'longdesc', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'tripadvisor', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'act', 'pays', 'ville', 'hotelchild', 'hotelamg', 'hotelarrangement', 'hotellocalisation', 'hoteloptions', 'hotelthemes', 'hotelroom', 'hotelimg', 'hotelweek', 'hotelprice', 'hotelsupplement', 'promotion', 'responsablehotel', 'stopsales', 'hotelmarge', 'hotelroomsup', 'events', 'reservation', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'baseroom'];
        }

        return ['__isInitialized__', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'id', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'name', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'maps', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'star', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'maxenfant', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'minenfant', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'contrat', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'shortdesc', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'visite', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'longdesc', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'tripadvisor', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'act', 'pays', 'ville', 'hotelchild', 'hotelamg', 'hotelarrangement', 'hotellocalisation', 'hoteloptions', 'hotelthemes', 'hotelroom', 'hotelimg', 'hotelweek', 'hotelprice', 'hotelsupplement', 'promotion', 'responsablehotel', 'stopsales', 'hotelmarge', 'hotelroomsup', 'events', 'reservation', '' . "\0" . 'Btob\\HotelBundle\\Entity\\Hotel' . "\0" . 'baseroom'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Hotel $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setName($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setName', [$name]);

        return parent::setName($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getName', []);

        return parent::getName();
    }

    /**
     * {@inheritDoc}
     */
    public function setStar($star)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStar', [$star]);

        return parent::setStar($star);
    }

    /**
     * {@inheritDoc}
     */
    public function getStar()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStar', []);

        return parent::getStar();
    }

    /**
     * {@inheritDoc}
     */
    public function setMaxenfant($maxenfant)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMaxenfant', [$maxenfant]);

        return parent::setMaxenfant($maxenfant);
    }

    /**
     * {@inheritDoc}
     */
    public function getMaxenfant()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMaxenfant', []);

        return parent::getMaxenfant();
    }

    /**
     * {@inheritDoc}
     */
    public function setMinenfant($minenfant)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMinenfant', [$minenfant]);

        return parent::setMinenfant($minenfant);
    }

    /**
     * {@inheritDoc}
     */
    public function getMinenfant()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMinenfant', []);

        return parent::getMinenfant();
    }

    /**
     * {@inheritDoc}
     */
    public function setContrat($contrat)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setContrat', [$contrat]);

        return parent::setContrat($contrat);
    }

    /**
     * {@inheritDoc}
     */
    public function getContrat()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getContrat', []);

        return parent::getContrat();
    }

    /**
     * {@inheritDoc}
     */
    public function setShortdesc($shortdesc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setShortdesc', [$shortdesc]);

        return parent::setShortdesc($shortdesc);
    }

    /**
     * {@inheritDoc}
     */
    public function getShortdesc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getShortdesc', []);

        return parent::getShortdesc();
    }

    /**
     * {@inheritDoc}
     */
    public function setVisite($visite)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setVisite', [$visite]);

        return parent::setVisite($visite);
    }

    /**
     * {@inheritDoc}
     */
    public function getVisite()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVisite', []);

        return parent::getVisite();
    }

    /**
     * {@inheritDoc}
     */
    public function setLongdesc($longdesc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLongdesc', [$longdesc]);

        return parent::setLongdesc($longdesc);
    }

    /**
     * {@inheritDoc}
     */
    public function getLongdesc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLongdesc', []);

        return parent::getLongdesc();
    }

    /**
     * {@inheritDoc}
     */
    public function setAct($act)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAct', [$act]);

        return parent::setAct($act);
    }

    /**
     * {@inheritDoc}
     */
    public function getAct()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAct', []);

        return parent::getAct();
    }

    /**
     * {@inheritDoc}
     */
    public function setPays(\Btob\HotelBundle\Entity\Pays $pays = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPays', [$pays]);

        return parent::setPays($pays);
    }

    /**
     * {@inheritDoc}
     */
    public function getPays()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPays', []);

        return parent::getPays();
    }

    /**
     * {@inheritDoc}
     */
    public function setVille(\Btob\HotelBundle\Entity\Ville $ville = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setVille', [$ville]);

        return parent::setVille($ville);
    }

    /**
     * {@inheritDoc}
     */
    public function getVille()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVille', []);

        return parent::getVille();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelchild(\Btob\HotelBundle\Entity\Hotelchild $hotelchild)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelchild', [$hotelchild]);

        return parent::addHotelchild($hotelchild);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelchild(\Btob\HotelBundle\Entity\Hotelchild $hotelchild)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelchild', [$hotelchild]);

        return parent::removeHotelchild($hotelchild);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelchild()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelchild', []);

        return parent::getHotelchild();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelamg(\Btob\HotelBundle\Entity\Hotelamg $hotelamg)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelamg', [$hotelamg]);

        return parent::addHotelamg($hotelamg);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelamg(\Btob\HotelBundle\Entity\Hotelamg $hotelamg)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelamg', [$hotelamg]);

        return parent::removeHotelamg($hotelamg);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelamg()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelamg', []);

        return parent::getHotelamg();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelarrangement(\Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelarrangement', [$hotelarrangement]);

        return parent::addHotelarrangement($hotelarrangement);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelarrangement(\Btob\HotelBundle\Entity\Hotelarrangement $hotelarrangement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelarrangement', [$hotelarrangement]);

        return parent::removeHotelarrangement($hotelarrangement);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelarrangement()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelarrangement', []);

        return parent::getHotelarrangement();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotellocalisation(\Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotellocalisation', [$hotellocalisation]);

        return parent::addHotellocalisation($hotellocalisation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotellocalisation(\Btob\HotelBundle\Entity\Hotellocalisation $hotellocalisation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotellocalisation', [$hotellocalisation]);

        return parent::removeHotellocalisation($hotellocalisation);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotellocalisation()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotellocalisation', []);

        return parent::getHotellocalisation();
    }

    /**
     * {@inheritDoc}
     */
    public function addHoteloption(\Btob\HotelBundle\Entity\Hoteloptions $hoteloptions)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHoteloption', [$hoteloptions]);

        return parent::addHoteloption($hoteloptions);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHoteloption(\Btob\HotelBundle\Entity\Hoteloptions $hoteloptions)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHoteloption', [$hoteloptions]);

        return parent::removeHoteloption($hoteloptions);
    }

    /**
     * {@inheritDoc}
     */
    public function getHoteloptions()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHoteloptions', []);

        return parent::getHoteloptions();
    }

    /**
     * {@inheritDoc}
     */
    public function addHoteltheme(\Btob\HotelBundle\Entity\Hotelthemes $hotelthemes)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHoteltheme', [$hotelthemes]);

        return parent::addHoteltheme($hotelthemes);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHoteltheme(\Btob\HotelBundle\Entity\Hotelthemes $hotelthemes)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHoteltheme', [$hotelthemes]);

        return parent::removeHoteltheme($hotelthemes);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelthemes()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelthemes', []);

        return parent::getHotelthemes();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelroom(\Btob\HotelBundle\Entity\Hotelroom $hotelroom)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelroom', [$hotelroom]);

        return parent::addHotelroom($hotelroom);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelroom(\Btob\HotelBundle\Entity\Hotelroom $hotelroom)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelroom', [$hotelroom]);

        return parent::removeHotelroom($hotelroom);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelroom()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelroom', []);

        return parent::getHotelroom();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelimg(\Btob\HotelBundle\Entity\Hotelimg $hotelimg)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelimg', [$hotelimg]);

        return parent::addHotelimg($hotelimg);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelimg(\Btob\HotelBundle\Entity\Hotelimg $hotelimg)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelimg', [$hotelimg]);

        return parent::removeHotelimg($hotelimg);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelimg()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelimg', []);

        return parent::getHotelimg();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelweek(\Btob\HotelBundle\Entity\Week $hotelweek)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelweek', [$hotelweek]);

        return parent::addHotelweek($hotelweek);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelweek(\Btob\HotelBundle\Entity\Week $hotelweek)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelweek', [$hotelweek]);

        return parent::removeHotelweek($hotelweek);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelweek()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelweek', []);

        return parent::getHotelweek();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelprice', [$hotelprice]);

        return parent::addHotelprice($hotelprice);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelprice(\Btob\HotelBundle\Entity\Hotelprice $hotelprice)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelprice', [$hotelprice]);

        return parent::removeHotelprice($hotelprice);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelprice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelprice', []);

        return parent::getHotelprice();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelsupplement(\Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelsupplement', [$hotelsupplement]);

        return parent::addHotelsupplement($hotelsupplement);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelsupplement(\Btob\HotelBundle\Entity\Hotelsupplement $hotelsupplement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelsupplement', [$hotelsupplement]);

        return parent::removeHotelsupplement($hotelsupplement);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelsupplement()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelsupplement', []);

        return parent::getHotelsupplement();
    }

    /**
     * {@inheritDoc}
     */
    public function addPromotion(\Btob\HotelBundle\Entity\Promotion $promotion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addPromotion', [$promotion]);

        return parent::addPromotion($promotion);
    }

    /**
     * {@inheritDoc}
     */
    public function removePromotion(\Btob\HotelBundle\Entity\Promotion $promotion)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removePromotion', [$promotion]);

        return parent::removePromotion($promotion);
    }

    /**
     * {@inheritDoc}
     */
    public function getPromotion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPromotion', []);

        return parent::getPromotion();
    }

    /**
     * {@inheritDoc}
     */
    public function addStopsale(\Btob\HotelBundle\Entity\Stopsales $stopsales)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addStopsale', [$stopsales]);

        return parent::addStopsale($stopsales);
    }

    /**
     * {@inheritDoc}
     */
    public function removeStopsale(\Btob\HotelBundle\Entity\Stopsales $stopsales)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeStopsale', [$stopsales]);

        return parent::removeStopsale($stopsales);
    }

    /**
     * {@inheritDoc}
     */
    public function getStopsales()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStopsales', []);

        return parent::getStopsales();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelmarge', [$hotelmarge]);

        return parent::addHotelmarge($hotelmarge);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelmarge(\Btob\HotelBundle\Entity\Hotelmarge $hotelmarge)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelmarge', [$hotelmarge]);

        return parent::removeHotelmarge($hotelmarge);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelmarge()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelmarge', []);

        return parent::getHotelmarge();
    }

    /**
     * {@inheritDoc}
     */
    public function addHotelroomsup(\Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addHotelroomsup', [$hotelroomsup]);

        return parent::addHotelroomsup($hotelroomsup);
    }

    /**
     * {@inheritDoc}
     */
    public function removeHotelroomsup(\Btob\HotelBundle\Entity\Hotelroomsup $hotelroomsup)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeHotelroomsup', [$hotelroomsup]);

        return parent::removeHotelroomsup($hotelroomsup);
    }

    /**
     * {@inheritDoc}
     */
    public function getHotelroomsup()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHotelroomsup', []);

        return parent::getHotelroomsup();
    }

    /**
     * {@inheritDoc}
     */
    public function addEvent(\Btob\HotelBundle\Entity\Events $events)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addEvent', [$events]);

        return parent::addEvent($events);
    }

    /**
     * {@inheritDoc}
     */
    public function removeEvent(\Btob\HotelBundle\Entity\Events $events)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeEvent', [$events]);

        return parent::removeEvent($events);
    }

    /**
     * {@inheritDoc}
     */
    public function getEvents()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEvents', []);

        return parent::getEvents();
    }

    /**
     * {@inheritDoc}
     */
    public function addReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addReservation', [$reservation]);

        return parent::addReservation($reservation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeReservation(\Btob\HotelBundle\Entity\Reservation $reservation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeReservation', [$reservation]);

        return parent::removeReservation($reservation);
    }

    /**
     * {@inheritDoc}
     */
    public function getReservation()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReservation', []);

        return parent::getReservation();
    }

    /**
     * {@inheritDoc}
     */
    public function addBaseroom(\Btob\HotelBundle\Entity\Baseroom $baseroom)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addBaseroom', [$baseroom]);

        return parent::addBaseroom($baseroom);
    }

    /**
     * {@inheritDoc}
     */
    public function removeBaseroom(\Btob\HotelBundle\Entity\Baseroom $baseroom)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeBaseroom', [$baseroom]);

        return parent::removeBaseroom($baseroom);
    }

    /**
     * {@inheritDoc}
     */
    public function getBaseroom()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getBaseroom', []);

        return parent::getBaseroom();
    }

    /**
     * {@inheritDoc}
     */
    public function addResponsablehotel(\Btob\HotelBundle\Entity\Responsablehotel $responsablehotel)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addResponsablehotel', [$responsablehotel]);

        return parent::addResponsablehotel($responsablehotel);
    }

    /**
     * {@inheritDoc}
     */
    public function removeResponsablehotel(\Btob\HotelBundle\Entity\Responsablehotel $responsablehotel)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeResponsablehotel', [$responsablehotel]);

        return parent::removeResponsablehotel($responsablehotel);
    }

    /**
     * {@inheritDoc}
     */
    public function getResponsablehotel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResponsablehotel', []);

        return parent::getResponsablehotel();
    }

    /**
     * {@inheritDoc}
     */
    public function setTripadvisor($tripadvisor)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTripadvisor', [$tripadvisor]);

        return parent::setTripadvisor($tripadvisor);
    }

    /**
     * {@inheritDoc}
     */
    public function getTripadvisor()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTripadvisor', []);

        return parent::getTripadvisor();
    }

    /**
     * {@inheritDoc}
     */
    public function setMaps($maps)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMaps', [$maps]);

        return parent::setMaps($maps);
    }

    /**
     * {@inheritDoc}
     */
    public function getMaps()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMaps', []);

        return parent::getMaps();
    }

}
