<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_648a17e9d1a6eea6bff48460de868393ba07af4b1ccf20ad862cbae36bf38682 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45a8ae605a703207484292aec48ca46babc6e587a96c080213dda38c76570357 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45a8ae605a703207484292aec48ca46babc6e587a96c080213dda38c76570357->enter($__internal_45a8ae605a703207484292aec48ca46babc6e587a96c080213dda38c76570357_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_24adbb53a56845d26e9bcf3fc91c1a16f2ff1c0d675ca3be543e8dfa74b1d601 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24adbb53a56845d26e9bcf3fc91c1a16f2ff1c0d675ca3be543e8dfa74b1d601->enter($__internal_24adbb53a56845d26e9bcf3fc91c1a16f2ff1c0d675ca3be543e8dfa74b1d601_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_45a8ae605a703207484292aec48ca46babc6e587a96c080213dda38c76570357->leave($__internal_45a8ae605a703207484292aec48ca46babc6e587a96c080213dda38c76570357_prof);

        
        $__internal_24adbb53a56845d26e9bcf3fc91c1a16f2ff1c0d675ca3be543e8dfa74b1d601->leave($__internal_24adbb53a56845d26e9bcf3fc91c1a16f2ff1c0d675ca3be543e8dfa74b1d601_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\traces.txt.twig");
    }
}
