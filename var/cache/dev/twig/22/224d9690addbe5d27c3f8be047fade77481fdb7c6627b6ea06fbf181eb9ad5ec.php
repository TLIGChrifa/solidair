<?php

/* base.html.twig */
class __TwigTemplate_e43f9ebdfad27f9d069a82b7d3b6722d1d2760182d7026cf71598d2dcaec227b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b02cbe9264dfc9724bf9a39bd7c8dc2e785a15048531d23fc0c83758361f832 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b02cbe9264dfc9724bf9a39bd7c8dc2e785a15048531d23fc0c83758361f832->enter($__internal_3b02cbe9264dfc9724bf9a39bd7c8dc2e785a15048531d23fc0c83758361f832_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_2f667489ea114540703f1c8ad09e6c4f118f3609bf00db08b287b385e61e122f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f667489ea114540703f1c8ad09e6c4f118f3609bf00db08b287b385e61e122f->enter($__internal_2f667489ea114540703f1c8ad09e6c4f118f3609bf00db08b287b385e61e122f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,shrink-to-fit=no\"> 
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "       

        
        <div class=\"upper-bar\">
          <div class=\"container\">
              <div class=\"row\">
                  <div class=\"col-lg col-md-6 col-sm-12 \">
                    <i class=\"fa fa-phone\" aria-hidden=\"true\"></i> 72 238 880  
                    <i class=\"fa fa-envelope\" aria-hidden=\"true\"> </i><a href=\"mailto:\">  reservation@solidair.com</a>
                  </div>
                  <div class=\"col-lg col-md-6 col-sm-4 text-md-right reseaux\" >
                    <a href=\"https://www.facebook.com/Solidair.tn/\" target=\"_blank\"><i class=\"fab fa-facebook-f\" style=\"padding: 0.3rem 0.45rem 0.3rem 0.45rem;\"></i></a> 
                    <a href=\"\" target=\"_blank\"> <i class=\"fab fa-instagram \" style=\"padding: 0.3rem 0.3rem 0.299rem 0.3rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-twitter \" style=\"padding: 0.3rem 0.333rem 0.2rem 0.2rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-youtube \" style=\"padding: 0.3rem 0.2rem 0.3rem 0.2rem;\"></i></a>
                  </div>
                  
                </div>
          
          </div>
         
       
        </div>

        <nav class=\"navbar navbar-expand-lg  text-sm-right\">
    
          <a class=\"navbar-brand\" href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
          <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/logo_solidair.png"), "html", null, true);
        echo "\"  style=\"margin-left: 7rem; width: 11rem;\"/>
          </a>
          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-nav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"><i class=\"fas fa-bars\" style=\"color: #fff;margin-top: 4px;\"></i></span>
          </button>
          <div class=\"collapse navbar-collapse animate fadeInTopRight\" id=\"main-nav\">
            <ul class=\"navbar-nav\">
              <li class=\"nav-item \">
                <a class=\"nav-link active\"aria-current=\"page\" href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hotelsentunise");
        echo "\">Hôtels en tunisie<span class=\"sr-only\">(current)</span></a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("voyagesorganisees");
        echo "\">Voyages organisés</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("billettrie");
        echo "\">Billetrie</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Croisières</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("circuit");
        echo "\">Circuits & Excursions </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Location Voitures </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("listemaisondhote");
        echo "\">Maisons d'hôtes</a>
              </li>
              
              
            </ul>
          </div>
              
        </nav>   
    </head>
    <body>
        ";
        // line 77
        $this->displayBlock('body', $context, $blocks);
        // line 78
        echo "
      <div class=\"footer\" >

        <div class=\" container\">
        <img  class=\"logo\"src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/logo_solidair.png"), "html", null, true);
        echo "\"  />
        <div class=\"row\">
          <div class=\"col-md-7 row\">
                <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Inscription Newsletter
            </div>
            <div class=\"text-newsletter\">
                Pour recevoir régulièrement nos meilleures Offres et Promotions
            </div>
            <form >
              <input  placeholder=\"Votre adresse E-mail\" class=\"input-newsletter\">
              <input  style=\"color:#fff\"type=\"submit\" value=\"S'inscrire\" onclick=\"alert('Merci de votre inscription...');\" class=\"submit-newsletter\">
            </form>
            <br>
            
            
          </div>
          <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Navigation
            </div>
            <ul class=\"menu_footer list-unstyled\">
              <li><a href=\"";
        // line 105
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hotelsentunise");
        echo "\"><i class=\"fa fa-angle-right\"></i> Hôtels en Tunisie</a></li>
              <li><a href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("voyagesorganisees");
        echo "\"><i class=\"fa fa-angle-right\"></i> Voyages Organisés</a></li>
              <li><a href=\"";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("circuit");
        echo "\"><i class=\"fa fa-angle-right\"></i> Circuits &amp; Excursions</a></li>
              <li><a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("billettrie");
        echo "\"><i class=\"fa fa-angle-right\"></i> Billetterie</a></li>
              <li><a href=\"";
        // line 109
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("listemaisondhote");
        echo "\"><i class=\"fa fa-angle-right\"></i> Maisons d'hotes</a></li>
              <li><a href=\"\" ><i class=\"fa fa-angle-right\"></i> Location de voitures</a></li>
              <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> Croisières</a></li>
              <li><a href=\"";
        // line 112
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contact");
        echo "\"><i class=\"fa fa-angle-right\"></i> Contact</a></li>
            </ul>
          </div>
          
          
          </div>
          <div class=\"col-md-4\">
            <div class=\"titre-newsletter\">
              Contacts
            </div>
            
            <div class=\"text-footer\" >
              <img  src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f1.png"), "html", null, true);
        echo "\" style=\"width:30px\" >
              <span> Contact :
                Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr</span>

            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f2.png"), "html", null, true);
        echo "\" style=\"width:25px\" > Tél :
              (+216) 72 222 887
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f3.png"), "html", null, true);
        echo "\" style=\"width:25px\"> E-mail :
              reservation@solidair.tn
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f4.png"), "html", null, true);
        echo "\" style=\"width:25px\" > Facebook :
              Solid'air Travel Agency
            </div>
            
            
            
          </div>
        </div>
      

            
      </div>
      </div>
      <div  class=\"footer-end\" style=\"text-align: center; color:#000;  background-color:#BEBEBE\">
          Powered by <a href=\"http://www.digitalgrouperformance.com/public/ \" style=\"color:#000\">
          Digital Group performance</a>
      </div>
        ";
        // line 164
        $this->displayBlock('javascripts', $context, $blocks);
        // line 174
        echo "                
    </body>
</html>














";
        
        $__internal_3b02cbe9264dfc9724bf9a39bd7c8dc2e785a15048531d23fc0c83758361f832->leave($__internal_3b02cbe9264dfc9724bf9a39bd7c8dc2e785a15048531d23fc0c83758361f832_prof);

        
        $__internal_2f667489ea114540703f1c8ad09e6c4f118f3609bf00db08b287b385e61e122f->leave($__internal_2f667489ea114540703f1c8ad09e6c4f118f3609bf00db08b287b385e61e122f_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_e72b1b0b83318cbb3366e9208ec8f26b8f23853bb57c082cc0bce363192d98f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e72b1b0b83318cbb3366e9208ec8f26b8f23853bb57c082cc0bce363192d98f9->enter($__internal_e72b1b0b83318cbb3366e9208ec8f26b8f23853bb57c082cc0bce363192d98f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_cece95fd6f917160b03672ee3e7c0eb28c17e86c02c076e1627e19ef57e8e459 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cece95fd6f917160b03672ee3e7c0eb28c17e86c02c076e1627e19ef57e8e459->enter($__internal_cece95fd6f917160b03672ee3e7c0eb28c17e86c02c076e1627e19ef57e8e459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Solid'air";
        
        $__internal_cece95fd6f917160b03672ee3e7c0eb28c17e86c02c076e1627e19ef57e8e459->leave($__internal_cece95fd6f917160b03672ee3e7c0eb28c17e86c02c076e1627e19ef57e8e459_prof);

        
        $__internal_e72b1b0b83318cbb3366e9208ec8f26b8f23853bb57c082cc0bce363192d98f9->leave($__internal_e72b1b0b83318cbb3366e9208ec8f26b8f23853bb57c082cc0bce363192d98f9_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9c426d18c603e5b2fd67766dd7a1f1e8e4952c10f032d33a49c5c2a4ceacbab0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c426d18c603e5b2fd67766dd7a1f1e8e4952c10f032d33a49c5c2a4ceacbab0->enter($__internal_9c426d18c603e5b2fd67766dd7a1f1e8e4952c10f032d33a49c5c2a4ceacbab0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_ef750308b619d7b99ab7c8cd7cee743798d8d2f6ce573fb06b8264e1e89adf5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef750308b619d7b99ab7c8cd7cee743798d8d2f6ce573fb06b8264e1e89adf5a->enter($__internal_ef750308b619d7b99ab7c8cd7cee743798d8d2f6ce573fb06b8264e1e89adf5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "        <link rel=\"icon\" type=\"image/png\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/icone.png"), "html", null, true);
        echo "\" />
      \t<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t      <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/fontawesome-free-5.15.3-web/css/all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
      \t<link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        ";
        
        $__internal_ef750308b619d7b99ab7c8cd7cee743798d8d2f6ce573fb06b8264e1e89adf5a->leave($__internal_ef750308b619d7b99ab7c8cd7cee743798d8d2f6ce573fb06b8264e1e89adf5a_prof);

        
        $__internal_9c426d18c603e5b2fd67766dd7a1f1e8e4952c10f032d33a49c5c2a4ceacbab0->leave($__internal_9c426d18c603e5b2fd67766dd7a1f1e8e4952c10f032d33a49c5c2a4ceacbab0_prof);

    }

    // line 77
    public function block_body($context, array $blocks = array())
    {
        $__internal_2c346704ab3033f1363dd50074ee6339f7ac2a6a74d7385ef7ec161dbb2c771e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c346704ab3033f1363dd50074ee6339f7ac2a6a74d7385ef7ec161dbb2c771e->enter($__internal_2c346704ab3033f1363dd50074ee6339f7ac2a6a74d7385ef7ec161dbb2c771e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c45a4d779afb4d47ab94a6f20a58415f71ff8c636550b2f8aca32f6a49e016fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c45a4d779afb4d47ab94a6f20a58415f71ff8c636550b2f8aca32f6a49e016fe->enter($__internal_c45a4d779afb4d47ab94a6f20a58415f71ff8c636550b2f8aca32f6a49e016fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c45a4d779afb4d47ab94a6f20a58415f71ff8c636550b2f8aca32f6a49e016fe->leave($__internal_c45a4d779afb4d47ab94a6f20a58415f71ff8c636550b2f8aca32f6a49e016fe_prof);

        
        $__internal_2c346704ab3033f1363dd50074ee6339f7ac2a6a74d7385ef7ec161dbb2c771e->leave($__internal_2c346704ab3033f1363dd50074ee6339f7ac2a6a74d7385ef7ec161dbb2c771e_prof);

    }

    // line 164
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f062709a714723d60e37323b990a72a2a512cb5e17ffbfaf5226945ea54e683c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f062709a714723d60e37323b990a72a2a512cb5e17ffbfaf5226945ea54e683c->enter($__internal_f062709a714723d60e37323b990a72a2a512cb5e17ffbfaf5226945ea54e683c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_0fcbbd827e8f588d005ff99e52f8b113998042bdad79f6509b04b0e248a20aef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fcbbd827e8f588d005ff99e52f8b113998042bdad79f6509b04b0e248a20aef->enter($__internal_0fcbbd827e8f588d005ff99e52f8b113998042bdad79f6509b04b0e248a20aef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 165
        echo "            ";
        if ((((((((((($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "homepage") && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reserver")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "chambre")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "hotelsentunise")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "recherchehotel")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "rechercheht")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservermaisondhote")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "chambres")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "billettrie")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservercircuit"))) {
            // line 166
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-3.2.1.js"), "html", null, true);
            echo "\"></script> 
            ";
        }
        // line 168
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            ";
        // line 169
        if (($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservercircuit")) {
            // line 170
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/main.js"), "html", null, true);
            echo "\"></script>
            ";
        }
        // line 172
        echo "                
        ";
        
        $__internal_0fcbbd827e8f588d005ff99e52f8b113998042bdad79f6509b04b0e248a20aef->leave($__internal_0fcbbd827e8f588d005ff99e52f8b113998042bdad79f6509b04b0e248a20aef_prof);

        
        $__internal_f062709a714723d60e37323b990a72a2a512cb5e17ffbfaf5226945ea54e683c->leave($__internal_f062709a714723d60e37323b990a72a2a512cb5e17ffbfaf5226945ea54e683c_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  384 => 172,  378 => 170,  376 => 169,  371 => 168,  365 => 166,  362 => 165,  353 => 164,  336 => 77,  324 => 12,  320 => 11,  316 => 10,  312 => 9,  307 => 8,  298 => 7,  280 => 6,  253 => 174,  251 => 164,  231 => 147,  221 => 140,  211 => 133,  199 => 124,  184 => 112,  178 => 109,  174 => 108,  170 => 107,  166 => 106,  162 => 105,  136 => 82,  130 => 78,  128 => 77,  115 => 67,  106 => 61,  97 => 55,  91 => 52,  85 => 49,  74 => 41,  70 => 40,  42 => 14,  40 => 7,  36 => 6,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,shrink-to-fit=no\"> 
        <title>{% block title %}Solid'air{% endblock %}</title>
        {% block stylesheets %}
        <link rel=\"icon\" type=\"image/png\" href=\"{{asset('assets/image/icone.png')}}\" />
      \t<link href=\"{{asset('assets/css/main.css')}}\" rel=\"stylesheet\">
        <link href=\"{{asset('assets/css/responsive.css')}}\" rel=\"stylesheet\">
\t      <link href=\"{{asset('assets/fontawesome-free-5.15.3-web/css/all.css')}}\" rel=\"stylesheet\">
      \t<link href=\"{{asset('assets/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
        {% endblock %}
       

        
        <div class=\"upper-bar\">
          <div class=\"container\">
              <div class=\"row\">
                  <div class=\"col-lg col-md-6 col-sm-12 \">
                    <i class=\"fa fa-phone\" aria-hidden=\"true\"></i> 72 238 880  
                    <i class=\"fa fa-envelope\" aria-hidden=\"true\"> </i><a href=\"mailto:\">  reservation@solidair.com</a>
                  </div>
                  <div class=\"col-lg col-md-6 col-sm-4 text-md-right reseaux\" >
                    <a href=\"https://www.facebook.com/Solidair.tn/\" target=\"_blank\"><i class=\"fab fa-facebook-f\" style=\"padding: 0.3rem 0.45rem 0.3rem 0.45rem;\"></i></a> 
                    <a href=\"\" target=\"_blank\"> <i class=\"fab fa-instagram \" style=\"padding: 0.3rem 0.3rem 0.299rem 0.3rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-twitter \" style=\"padding: 0.3rem 0.333rem 0.2rem 0.2rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-youtube \" style=\"padding: 0.3rem 0.2rem 0.3rem 0.2rem;\"></i></a>
                  </div>
                  
                </div>
          
          </div>
         
       
        </div>

        <nav class=\"navbar navbar-expand-lg  text-sm-right\">
    
          <a class=\"navbar-brand\" href=\"{{path('homepage')}}\">
          <img src=\"{{asset('assets/image/logo_solidair.png')}}\"  style=\"margin-left: 7rem; width: 11rem;\"/>
          </a>
          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-nav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"><i class=\"fas fa-bars\" style=\"color: #fff;margin-top: 4px;\"></i></span>
          </button>
          <div class=\"collapse navbar-collapse animate fadeInTopRight\" id=\"main-nav\">
            <ul class=\"navbar-nav\">
              <li class=\"nav-item \">
                <a class=\"nav-link active\"aria-current=\"page\" href=\"{{path('hotelsentunise')}}\">Hôtels en tunisie<span class=\"sr-only\">(current)</span></a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"{{path('voyagesorganisees')}}\">Voyages organisés</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"{{path('billettrie')}}\">Billetrie</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Croisières</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"{{path('circuit')}}\">Circuits & Excursions </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Location Voitures </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"{{path('listemaisondhote')}}\">Maisons d'hôtes</a>
              </li>
              
              
            </ul>
          </div>
              
        </nav>   
    </head>
    <body>
        {% block body %}{% endblock %}

      <div class=\"footer\" >

        <div class=\" container\">
        <img  class=\"logo\"src=\"{{asset('assets/image/logo_solidair.png')}}\"  />
        <div class=\"row\">
          <div class=\"col-md-7 row\">
                <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Inscription Newsletter
            </div>
            <div class=\"text-newsletter\">
                Pour recevoir régulièrement nos meilleures Offres et Promotions
            </div>
            <form >
              <input  placeholder=\"Votre adresse E-mail\" class=\"input-newsletter\">
              <input  style=\"color:#fff\"type=\"submit\" value=\"S'inscrire\" onclick=\"alert('Merci de votre inscription...');\" class=\"submit-newsletter\">
            </form>
            <br>
            
            
          </div>
          <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Navigation
            </div>
            <ul class=\"menu_footer list-unstyled\">
              <li><a href=\"{{path('hotelsentunise')}}\"><i class=\"fa fa-angle-right\"></i> Hôtels en Tunisie</a></li>
              <li><a href=\"{{path('voyagesorganisees')}}\"><i class=\"fa fa-angle-right\"></i> Voyages Organisés</a></li>
              <li><a href=\"{{path('circuit')}}\"><i class=\"fa fa-angle-right\"></i> Circuits &amp; Excursions</a></li>
              <li><a href=\"{{path('billettrie')}}\"><i class=\"fa fa-angle-right\"></i> Billetterie</a></li>
              <li><a href=\"{{path('listemaisondhote')}}\"><i class=\"fa fa-angle-right\"></i> Maisons d'hotes</a></li>
              <li><a href=\"\" ><i class=\"fa fa-angle-right\"></i> Location de voitures</a></li>
              <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> Croisières</a></li>
              <li><a href=\"{{path('contact')}}\"><i class=\"fa fa-angle-right\"></i> Contact</a></li>
            </ul>
          </div>
          
          
          </div>
          <div class=\"col-md-4\">
            <div class=\"titre-newsletter\">
              Contacts
            </div>
            
            <div class=\"text-footer\" >
              <img  src=\"{{asset('assets/image/f1.png')}}\" style=\"width:30px\" >
              <span> Contact :
                Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr</span>

            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f2.png')}}\" style=\"width:25px\" > Tél :
              (+216) 72 222 887
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f3.png')}}\" style=\"width:25px\"> E-mail :
              reservation@solidair.tn
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f4.png')}}\" style=\"width:25px\" > Facebook :
              Solid'air Travel Agency
            </div>
            
            
            
          </div>
        </div>
      

            
      </div>
      </div>
      <div  class=\"footer-end\" style=\"text-align: center; color:#000;  background-color:#BEBEBE\">
          Powered by <a href=\"http://www.digitalgrouperformance.com/public/ \" style=\"color:#000\">
          Digital Group performance</a>
      </div>
        {% block javascripts %}
            {% if  app.request.attributes.get('_route') != 'homepage' and app.request.attributes.get('_route') != 'reserver' and app.request.attributes.get('_route') != 'chambre' and app.request.attributes.get('_route') != 'hotelsentunise' and  app.request.attributes.get('_route') != 'recherchehotel' and  app.request.attributes.get('_route') != 'rechercheht' and  app.request.attributes.get('_route') != 'reservermaisondhote' and   app.request.attributes.get('_route') != 'chambres' and app.request.attributes.get('_route') != 'billettrie' and app.request.attributes.get('_route') != 'reservercircuit' %}
            <script src=\"{{asset('assets/js/jquery-3.2.1.js')}}\"></script> 
            {% endif %}
            <script src=\"{{asset('assets/js/bootstrap.min.js')}}\"></script>
            {% if app.request.attributes.get('_route') != 'reservercircuit' %}
            <script src=\"{{asset('assets/js/main.js')}}\"></script>
            {% endif %}
                
        {% endblock %}
                
    </body>
</html>














", "base.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\base.html.twig");
    }
}
