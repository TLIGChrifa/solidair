<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_b0caa571c2775b52cd1e57fd2dee08299f3acc815bc166ef57e788640bc85a97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5c55426849f6cb30dc89493ce3f805585820e6880b173c0ce6ad382c06bbb75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5c55426849f6cb30dc89493ce3f805585820e6880b173c0ce6ad382c06bbb75->enter($__internal_b5c55426849f6cb30dc89493ce3f805585820e6880b173c0ce6ad382c06bbb75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_91659a7b21b63a36316a529f609603d72b711148d6391a2a393ae3ed1d42ef39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91659a7b21b63a36316a529f609603d72b711148d6391a2a393ae3ed1d42ef39->enter($__internal_91659a7b21b63a36316a529f609603d72b711148d6391a2a393ae3ed1d42ef39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b5c55426849f6cb30dc89493ce3f805585820e6880b173c0ce6ad382c06bbb75->leave($__internal_b5c55426849f6cb30dc89493ce3f805585820e6880b173c0ce6ad382c06bbb75_prof);

        
        $__internal_91659a7b21b63a36316a529f609603d72b711148d6391a2a393ae3ed1d42ef39->leave($__internal_91659a7b21b63a36316a529f609603d72b711148d6391a2a393ae3ed1d42ef39_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_866e0ec926f8fc204a5eacdbe039021cadb0471d4a5c92ad2f3b3bf880a1d863 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_866e0ec926f8fc204a5eacdbe039021cadb0471d4a5c92ad2f3b3bf880a1d863->enter($__internal_866e0ec926f8fc204a5eacdbe039021cadb0471d4a5c92ad2f3b3bf880a1d863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_713989cba51bcff94b8d69e20cdf30af7a40172eb09424abcab5db47ce04860a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_713989cba51bcff94b8d69e20cdf30af7a40172eb09424abcab5db47ce04860a->enter($__internal_713989cba51bcff94b8d69e20cdf30af7a40172eb09424abcab5db47ce04860a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_713989cba51bcff94b8d69e20cdf30af7a40172eb09424abcab5db47ce04860a->leave($__internal_713989cba51bcff94b8d69e20cdf30af7a40172eb09424abcab5db47ce04860a_prof);

        
        $__internal_866e0ec926f8fc204a5eacdbe039021cadb0471d4a5c92ad2f3b3bf880a1d863->leave($__internal_866e0ec926f8fc204a5eacdbe039021cadb0471d4a5c92ad2f3b3bf880a1d863_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
