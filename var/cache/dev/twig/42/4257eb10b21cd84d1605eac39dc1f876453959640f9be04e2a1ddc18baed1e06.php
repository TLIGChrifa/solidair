<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_d02f42119fa9a5c5b0b1441d47cc96a0c2a291350463fd54e228d8cda5b7eb98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d58384f6d50a4594167d01b6d3e783e020be44b50cbe556a63e6d47bfd7b9c7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d58384f6d50a4594167d01b6d3e783e020be44b50cbe556a63e6d47bfd7b9c7c->enter($__internal_d58384f6d50a4594167d01b6d3e783e020be44b50cbe556a63e6d47bfd7b9c7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_8f8d2706184b4213b1869788c338d1e84343de86a8143a4d82b917328188d9ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f8d2706184b4213b1869788c338d1e84343de86a8143a4d82b917328188d9ef->enter($__internal_8f8d2706184b4213b1869788c338d1e84343de86a8143a4d82b917328188d9ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d58384f6d50a4594167d01b6d3e783e020be44b50cbe556a63e6d47bfd7b9c7c->leave($__internal_d58384f6d50a4594167d01b6d3e783e020be44b50cbe556a63e6d47bfd7b9c7c_prof);

        
        $__internal_8f8d2706184b4213b1869788c338d1e84343de86a8143a4d82b917328188d9ef->leave($__internal_8f8d2706184b4213b1869788c338d1e84343de86a8143a4d82b917328188d9ef_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6946fdce6c6ec3efebd18aa2a46b4b058e3e592289ae6655856515577f1f0900 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6946fdce6c6ec3efebd18aa2a46b4b058e3e592289ae6655856515577f1f0900->enter($__internal_6946fdce6c6ec3efebd18aa2a46b4b058e3e592289ae6655856515577f1f0900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_efd437f5486baaf472d64d537c15cfbef372b8bdb1f14d13c4cf71ae66b7b05b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efd437f5486baaf472d64d537c15cfbef372b8bdb1f14d13c4cf71ae66b7b05b->enter($__internal_efd437f5486baaf472d64d537c15cfbef372b8bdb1f14d13c4cf71ae66b7b05b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_efd437f5486baaf472d64d537c15cfbef372b8bdb1f14d13c4cf71ae66b7b05b->leave($__internal_efd437f5486baaf472d64d537c15cfbef372b8bdb1f14d13c4cf71ae66b7b05b_prof);

        
        $__internal_6946fdce6c6ec3efebd18aa2a46b4b058e3e592289ae6655856515577f1f0900->leave($__internal_6946fdce6c6ec3efebd18aa2a46b4b058e3e592289ae6655856515577f1f0900_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d40aa71e74fdf7ce02c1d8c7b0fd860d96c61fc54262a30ff0be6c5196de4311 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d40aa71e74fdf7ce02c1d8c7b0fd860d96c61fc54262a30ff0be6c5196de4311->enter($__internal_d40aa71e74fdf7ce02c1d8c7b0fd860d96c61fc54262a30ff0be6c5196de4311_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_2a54d29a57ad4ff47ce50ac53ae90d8ec3c3ae6026d854c1443eb36155a05d39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a54d29a57ad4ff47ce50ac53ae90d8ec3c3ae6026d854c1443eb36155a05d39->enter($__internal_2a54d29a57ad4ff47ce50ac53ae90d8ec3c3ae6026d854c1443eb36155a05d39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_2a54d29a57ad4ff47ce50ac53ae90d8ec3c3ae6026d854c1443eb36155a05d39->leave($__internal_2a54d29a57ad4ff47ce50ac53ae90d8ec3c3ae6026d854c1443eb36155a05d39_prof);

        
        $__internal_d40aa71e74fdf7ce02c1d8c7b0fd860d96c61fc54262a30ff0be6c5196de4311->leave($__internal_d40aa71e74fdf7ce02c1d8c7b0fd860d96c61fc54262a30ff0be6c5196de4311_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d75df4473a79628834eb09b9f502d6d5ab5933f18377d177f7b2c54dce9b96a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d75df4473a79628834eb09b9f502d6d5ab5933f18377d177f7b2c54dce9b96a9->enter($__internal_d75df4473a79628834eb09b9f502d6d5ab5933f18377d177f7b2c54dce9b96a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_5b7e70f10ca057d288465682d7ee42565b8d3f13d4586738a1021febb0b6646b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b7e70f10ca057d288465682d7ee42565b8d3f13d4586738a1021febb0b6646b->enter($__internal_5b7e70f10ca057d288465682d7ee42565b8d3f13d4586738a1021febb0b6646b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_5b7e70f10ca057d288465682d7ee42565b8d3f13d4586738a1021febb0b6646b->leave($__internal_5b7e70f10ca057d288465682d7ee42565b8d3f13d4586738a1021febb0b6646b_prof);

        
        $__internal_d75df4473a79628834eb09b9f502d6d5ab5933f18377d177f7b2c54dce9b96a9->leave($__internal_d75df4473a79628834eb09b9f502d6d5ab5933f18377d177f7b2c54dce9b96a9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
