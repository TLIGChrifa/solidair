<?php

/* default/chambres.html.twig */
class __TwigTemplate_59e9b1b6a7dbe25a2006ce7d2f76088d03aaefbe0742f4a2f560ab7811f447fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/chambres.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6455c0e962b627d75e3c0999d692c3af7feaaa9479bf08578f355393f7cf9d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6455c0e962b627d75e3c0999d692c3af7feaaa9479bf08578f355393f7cf9d4->enter($__internal_e6455c0e962b627d75e3c0999d692c3af7feaaa9479bf08578f355393f7cf9d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/chambres.html.twig"));

        $__internal_7c321a704763744622765b73de55dd12daafd065741b625a29ad7b62190e08c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c321a704763744622765b73de55dd12daafd065741b625a29ad7b62190e08c8->enter($__internal_7c321a704763744622765b73de55dd12daafd065741b625a29ad7b62190e08c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/chambres.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e6455c0e962b627d75e3c0999d692c3af7feaaa9479bf08578f355393f7cf9d4->leave($__internal_e6455c0e962b627d75e3c0999d692c3af7feaaa9479bf08578f355393f7cf9d4_prof);

        
        $__internal_7c321a704763744622765b73de55dd12daafd065741b625a29ad7b62190e08c8->leave($__internal_7c321a704763744622765b73de55dd12daafd065741b625a29ad7b62190e08c8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d1a7e36bca5e94be9f1eb1f9570b0d9fa725a02e7751f305f0222bfc33f1662b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1a7e36bca5e94be9f1eb1f9570b0d9fa725a02e7751f305f0222bfc33f1662b->enter($__internal_d1a7e36bca5e94be9f1eb1f9570b0d9fa725a02e7751f305f0222bfc33f1662b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e56022a3193221577627647d344f8454df1249b783e409a96536515299b94297 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e56022a3193221577627647d344f8454df1249b783e409a96536515299b94297->enter($__internal_e56022a3193221577627647d344f8454df1249b783e409a96536515299b94297_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

  <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>

     <script>
     
        \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
     
     

  </script>


    <div class=\"slider\"> 
          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                  
                  
                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
                
                  </div>
          
                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
                
                 </div>
                    
                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
          
                  </div>
                 
            
                </div>
                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Previous</span>
                </a>
                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Next</span>
                </a>
          
          </div>
        </div>



      ";
        // line 104
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
 <div class=\"ChoisirChambre container\">
            <ul id=\"menu\">
                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
                <li><a href=\"HotelEnTunisie.html\">Hotel En Tunisie</a></li>
                <li><a class=\"active\">Choisir une Chambre</a></li>
            </ul>
            <div class=\"chambreBox\">
                <h5 >";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "name", array()), "html", null, true);
        echo " </h5> 
                <div class=\"tour-rating\">
                              ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "star", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 115
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoil.png"), "html", null, true);
            echo "\" class=\"\" alt=\"...\">
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 117
        echo "                </div>
            </div>
            <div class=\"chambreBoxContent row\">
                <div class=\"col-md-3 col-12\">
                  ";
        // line 121
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hotelsimg"] ?? $this->getContext($context, "hotelsimg")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 122
            echo "                  ";
            if ($this->getAttribute($context["loop"], "first", array())) {
                // line 123
                echo "                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["i"], "file", array())), "html", null, true);
                echo "\" class=\"w-100\"/>
                    ";
            }
            // line 125
            echo "              ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "                </div>
               <div class=\"col-md-4 col-12\">
                <h5 style=\"color: #F4991C;\">";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "name", array()), "html", null, true);
        echo "</h5>
               <small>";
        // line 129
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "ville", array()), "html", null, true);
        echo "</small>  
                <p>";
        // line 130
        echo twig_slice($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "shortdesc", array()), 0, 500);
        echo "...</p>
                ";
        // line 132
        echo "               </div> 
               <div class=\"col-md-5 col-12\">
                
                     <div  style=\"color: #212529; font-size: 13px;font-weight: bold;padding: 63px 38px;\">
     
      À partir : <input id=\"datedebut\" type=\"text\" style=\"width:43%;border-radius:none;
     height: 1.5rem; font-size:13px;\" class=\"form-control \" autocomplete=\"off\"  value=\"";
        // line 138
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["dated"] ?? $this->getContext($context, "dated")), "d-m-Y"), "html", null, true);
        echo "\" name=\"datedebut\" required/>
       <div style=\"margin-top:14px\">
     Jusqu'au : <input id=\"datefin\" type=\"text\" style=\"width:43%;border-radius:none;
     height: 1.5rem; font-size:13px;\" class=\"form-control \" autocomplete=\"off\"  value=\"";
        // line 141
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["datef"] ?? $this->getContext($context, "datef")), "d-m-Y"), "html", null, true);
        echo "\" name=\"datefin\" required/>
      </div>
                    
        
              </div>

            </div>
           
          <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >
    
    <tr style=\"background-color: #dee2e640; text-align:left;\">
      <th colspan=\"2\"  >Nombre de chambres à reserver</th>
      <th colspan=\"5\" >  ";
        // line 153
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nbchambres", array()), 'row', array("attr" => array("class" => "form-control", "style" => "width: 58px;font-size: 13px;")));
        echo " </th>
    </tr>
    <tr style=\"background-color: #dee2e6b0;\" > 
      <th scope=\"col\" style=\"width: 600px;\">Chambre N°</th>
      <th scope=\"col\" style=\"width: 300px;\">Type de la chambre</th>
      <th scope=\"col\" style=\"width: 300px;\"> Adultes & Enfants</th>
      
      <th scope=\"col\" style=\"width: 400px;\">Avec</th>
      <th scope=\"col\" style=\"width: 25%;\">Suppléments</th>
      
    </tr>
  </thead>
  <tbody id=\"newDiv\">
                                                      
 </tbody>


</table>
<div class=\"btnReserver\"> <button type=\"submit\"  id=\"btsubmit\" class=\"btn \" style=\"color: #fff;
    cursor: not-allowed;
    float: right;
    background-image: linear-gradient(to right, #1e4b22 , #F4991C);\"  disabled ><i class=\"fa fa-edit\"> </i> Tarif & Dispo</button></div>

            
        </div>   













































 


 
    ";
        // line 227
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </form>
    </div>
     </div >
   <script>
 function activer()
        {
            var select = document.getElementById( \"btob_hotelbundle_reservationhotel_typepayement\" )
 
            var otherFields = document.querySelectorAll( \".toDisplay\" )
            //var select = document.querySelectorAll( \".toDisplayselect\" )
            if ( select.value == \"Espèce\" ){
        
             Array.from( otherFields ).forEach( function ( select ) { select.style.display = \"none\" } )
            }
               
            else{
            Array.from( otherFields ).forEach( function ( input ) { input.style.display = \"inline-block\" } )
          //  document.getElementById(\"numcheque\").style.visibility=\"visible\";

            }
        }
        
     function logementchoice() {
       
       var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
       //    \$(\"#type0\").prop('id', 'type'+i);
          
            console.log( event.target.options[event.target.selectedIndex].text);
           
            str2 = event.target.id.replace ( /[^\\d.]/g, '' );
           // alert(str2);
            total = parseInt(str2, 10);
             console.log(total,i);
            if(total == i){
                 \$('#arrval'+i).val(event.target.options[event.target.selectedIndex].text);
       
            }
           
              
              

            }
      }      
        
        
        
        
        
        
        
        
        
        
  var h=0;
   var theIds = [];
  function myFunctione() {
       
       var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
           \$(\"#type0\").prop('id', 'type'+i);
          
            //console.log( event.target.options[event.target.selectedIndex].text);
           
            str2 = event.target.id.replace ( /[^\\d.]/g, '' );
           // alert(str2);
            total = parseInt(str2, 10);
            // alert(total);
            if(total == i){
                 \$('#typeval'+i).val( event.target.options[event.target.selectedIndex].text);
                  var option = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()).text(\$(\"#type\"+i).val());
              \$(\"#adultes\"+i).empty().append(option);
              for(j = \$(\"#type\"+i).val()-1; j > 0; j--){
                \$(\"#adultes\"+i).append(\$('<option>', {
                    value: j,
                    text: j
                }));
              }
             
    
              //\$(\"#adultes\"+i).val(\$(\"#type\"+i).val());
              var optionenfants = \$('<option></option>').attr(\"value\", 0).text(0);
              \$(\"#enfantage\"+i).empty().append(optionenfants);
              for(ef = 1; ef <= \$(\"#type\"+i).val()-1 ; ef++){
              //for(ef = \$(\"#type\"+i).val()-1; ef > 0; ef--){
                \$(\"#enfantage\"+i).append(\$('<option>', {
                    value: ef,
                    text: ef
                }));
              } 
            }
           
              var e = document.getElementById('type'+i);
              var strUser = e.options[e.selectedIndex].text;
              document.getElementById(\"hiddenselect\"+i).value = strUser;
          //  alert(strUser);
              

            }
      } 
function equal() {
     var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
                    var select = \$(\"#type\"+i).val();

                   //console.log(select);
             // \$(\"#adultes\"+i).change(function(){
               
               var valeurad = \$(\"#adultes\"+i).val();
               var valrest = (\$(\"#type\"+i).val()- valeurad) + 1;
               //   console.log( valrest);
               var optionenf = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()- valeurad).text(\$(\"#type\"+i).val() - valeurad);
                 \$(\"#enfantage\"+i).empty().append(optionenf);
                 for(e = valrest; e < \$(\"#type\"+i).val() ; e ++){
                \$(\"#enfantage\"+i).append(\$('<option>', {
                    value: e,
                    text: e
                }));
                 }
               if(valeurad != select){
                    var HTMLl='';
                for(en = 1; en <= \$(\"#enfantage\"+i).val(); en++){
                    HTMLl += ' Âge enfant N° '+en+': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select  name=\"ages'+i+'_'+en+'\" style=\"width:180px;border-radius: 4px; font-size: 13px;\" class=\"form-control\"> ";
        // line 350
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(2, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            echo " <option  value=\"";
            echo twig_escape_filter($this->env, $context["k"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["k"], "html", null, true);
            echo "</option> ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select><br/> '                                                            
                }
     
               \$('#agediv'+i).html(HTMLl);
                 }
           //   })
            }
}
function equalbyenfant() {
     var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
                    var select = \$(\"#type\"+i).val();

                   //console.log(select);
             // \$(\"#adultes\"+i).change(function(){
               
                  var valeuref = \$(\"#enfantage\"+i).val();
                  var valrest = (\$(\"#type\"+i).val()- valeuref) + 1;
                  console.log( \$(\"#type\"+i).val()- valeuref);
                  var optionad = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()- valeuref).text(\$(\"#type\"+i).val() - valeuref);
                \$(\"#adultes\"+i).empty().append(optionad);
                // for(eft = 1; eft <= \$(\"#type\"+i).val() ; eft++){
                 for(eft = valrest; eft <= \$(\"#type\"+i).val() ; eft ++){
                    \$(\"#adultes\"+i).append(\$('<option>', {
                    value: eft,
                    text: eft
                }));
                 }
   // valeur.push( \$(this).val());
    //console.log(valeur);
    
    var HTMLl='';
    for(en = 1; en <= valeuref; en++){
        HTMLl += '  Âge enfant N° '+en+' : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select  name=\"ages'+i+'_'+en+'\" style=\"width:180px;border-radius: 4px; font-size: 13px;\" class=\"form-control\"> ";
        // line 383
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(2, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            echo " <option  value=\"";
            echo twig_escape_filter($this->env, $context["k"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["k"], "html", null, true);
            echo "</option> ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select><br/> '                                                            
      }
     
    \$('#agediv'+i).html(HTMLl);


  //  console.log(\$('#agediv'+\$( \"#enfantage\" ).val()).html(HTMLl));
  
            }
}
 ";
        // line 393
        $context["idenf"] = 0;
        // line 394
        echo "  \$(document).ready(function(){
      //var vals = \$(\"#enfantage\").val();
   var getDates = function(startDate, endDate) {
  var dates = [],
      currentDate = startDate,
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
  while (currentDate <= endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};

// Usage
var dates = getDates(new Date(), new Date(";
        // line 412
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+1 day"), "Y/m/d"), "html", null, true);
        echo "));                                                                                                           
dates.forEach(function(date) {
  console.log(date);
});

  \$('#reservation_hotel_nbchambres').change(function(){
    var num = \$(this).val();
    
    //console.log(vals);
    var HTML='';
   
    for(i = 1; i <= num; i++){
        
         HTML += '<tr style=\"background-color:#dee2e640\"> <td ><strong><br> Chambre N°  '+ i +':</strong> </td><td><br> <select  id=\"type'+i+'\" name=\"type'+i+'\" style=\"border-radius: 4px;width:180px;font-size: 13px;\" onchange=\"myFunctione()\" class=\"form-control\" required=\"required\" >";
        // line 425
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "hotelroom", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            echo " <option  value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "room", array()), "capacity", array()), "html", null, true);
            echo "\" ";
            if (($this->getAttribute($this->getAttribute($context["item"], "room", array()), "name", array()) == "Chambre double")) {
                echo " selected ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "room", array()), "name", array()), "html", null, true);
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select> <input type=\"hidden\" id=\"typeval'+i+'\" name=\"typehidden'+i+'\" value=\"Chambre double\"/></td><td> <select  id=\"adultes'+i+'\" name=\"adultes'+i+'\" style=\"width:180px;border-radius: 4px;font-size: 13px;\" onchange=\"equal()\" class=\"form-control\"> ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "hotelroom", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            if (($this->getAttribute($this->getAttribute($context["item"], "room", array()), "name", array()) == "Chambre double")) {
                echo " ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getAttribute($context["item"], "room", array()), "capacity", array())));
                foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
                    echo " <option  value=\"";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "\" ";
                    if (($context["k"] == $this->getAttribute($this->getAttribute($context["item"], "room", array()), "capacity", array()))) {
                        echo "selected ";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "</option>  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " </select> <br>  <select  name=\"enfants'+i+'\" style=\"width:180px;border-radius: 4px;font-size: 13px;\" id=\"enfantage'+i+'\" onchange=\"equalbyenfant()\" class=\"form-control\"> ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "hotelroom", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            if ($this->getAttribute($context["loop"], "first", array())) {
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(0, ($this->getAttribute($this->getAttribute($context["item"], "room", array()), "capacity", array()) - 1)));
                foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
                    echo "  <option  value=\"";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["k"], "html", null, true);
                    echo "</option>  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo " ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "  </select> <br> <div id=\"agediv'+i+'\"> </div><input type=\"hidden\" value=\"test\" id =\"hiddenselect'+i+'\" name=\"hiddenselect'+i+'\"> </td><td > <br>  <select  id=\"logement'+i+'\" name=\"logement'+i+'\" onchange=\"logementchoice()\" style=\"width:230px;border-radius: 4px;font-size: 13px;\" class=\"form-control\" required=\"required\">'+ ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["allarrangement"] ?? $this->getContext($context, "allarrangement")));
        foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
            // line 426
            echo "            '   ";
            $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
            echo " ";
            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                echo "  ";
                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                echo "  <option  value=\"";
                echo twig_escape_filter($this->env, ($context["totalprice"] ?? $this->getContext($context, "totalprice")), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["arr"], "hotelarrangement", array()), "arrangement", array()), "name", array()), "html", null, true);
                echo "</option> ";
            } else {
                echo "<option  value=\"";
                echo twig_escape_filter($this->env, (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())), "html", null, true);
                echo " \"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["arr"], "hotelarrangement", array()), "arrangement", array()), "name", array()), "html", null, true);
                echo "</option> ";
            }
            echo "   '+
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 428
        echo "             '";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["allarrangement"] ?? $this->getContext($context, "allarrangement")));
        foreach ($context['_seq'] as $context["key"] => $context["arr"]) {
            echo " ";
            if (($context["key"] == 0)) {
                echo "<input type=\"hidden\" id=\"arrval'+i+'\" name=\"arrhidden'+i+'\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["arr"], "hotelarrangement", array()), "arrangement", array()), "name", array()), "html", null, true);
                echo "\"/>";
            }
            echo " ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['arr'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</td><td style=\"text-align: left;\">";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["allsupplement"] ?? $this->getContext($context, "allsupplement")));
        foreach ($context['_seq'] as $context["_key"] => $context["supp"]) {
            $context["totalprice"] = $this->getAttribute($context["supp"], "price", array());
            echo " ";
            if (($this->getAttribute($context["supp"], "persm", array()) == 1)) {
                echo "  ";
                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["supp"], "price", array()) * $this->getAttribute($context["supp"], "marge", array())) / 100));
                echo " ";
            } else {
                echo " ";
                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["supp"], "marge", array()));
                echo " ";
            }
            echo "<input type=\"checkbox\" id=\"supp'+i+'\" name=\"supp'+i+'_";
            echo twig_escape_filter($this->env, $this->getAttribute($context["supp"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, ($context["totalprice"] ?? $this->getContext($context, "totalprice")), "html", null, true);
            echo "\"  style=\"height: 15px;\" class=\"checkbox\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["supp"], "hotelsupplement", array()), "supplement", array()), "name", array()), "html", null, true);
            echo " (+ ";
            echo twig_escape_filter($this->env, ($context["totalprice"] ?? $this->getContext($context, "totalprice")), "html", null, true);
            echo " TND)<br/>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['supp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</td></tr>'
           
    
    
      \$('#newDiv').html(HTML);
   
   \$('#btsubmit').prop('disabled', false);
   \$('#btsubmit').css('cursor', 'pointer');
}  
  })       
         
 // \$(\"form\").submit(function(){
  //  if (\$('#textdatedebut').val() > \$('#textdatefin').val()) {
  //                                   alert('error')
  //   }
 // });
})

       </script>    



";
        
        $__internal_e56022a3193221577627647d344f8454df1249b783e409a96536515299b94297->leave($__internal_e56022a3193221577627647d344f8454df1249b783e409a96536515299b94297_prof);

        
        $__internal_d1a7e36bca5e94be9f1eb1f9570b0d9fa725a02e7751f305f0222bfc33f1662b->leave($__internal_d1a7e36bca5e94be9f1eb1f9570b0d9fa725a02e7751f305f0222bfc33f1662b_prof);

    }

    public function getTemplateName()
    {
        return "default/chambres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  700 => 428,  675 => 426,  583 => 425,  567 => 412,  547 => 394,  545 => 393,  521 => 383,  474 => 350,  348 => 227,  271 => 153,  256 => 141,  250 => 138,  242 => 132,  238 => 130,  234 => 129,  230 => 128,  226 => 126,  212 => 125,  206 => 123,  203 => 122,  186 => 121,  180 => 117,  171 => 115,  167 => 114,  162 => 112,  151 => 104,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}


  <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>

     <script>
     
        \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
     
     

  </script>


    <div class=\"slider\"> 
          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                  
                  
                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
                
                  </div>
          
                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
                
                 </div>
                    
                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
          
                  </div>
                 
            
                </div>
                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Previous</span>
                </a>
                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Next</span>
                </a>
          
          </div>
        </div>



      {{ form_start(form) }}
 <div class=\"ChoisirChambre container\">
            <ul id=\"menu\">
                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
                <li><a href=\"HotelEnTunisie.html\">Hotel En Tunisie</a></li>
                <li><a class=\"active\">Choisir une Chambre</a></li>
            </ul>
            <div class=\"chambreBox\">
                <h5 >{{hotel.name}} </h5> 
                <div class=\"tour-rating\">
                              {% for i in 1..h.star %}
                                <img src=\"{{asset('assets/image/etoil.png')}}\" class=\"\" alt=\"...\">
                                {% endfor %}
                </div>
            </div>
            <div class=\"chambreBoxContent row\">
                <div class=\"col-md-3 col-12\">
                  {% for i in hotelsimg %}
                  {% if loop.first %}
                    <img src=\"{{asset(i.file)}}\" class=\"w-100\"/>
                    {% endif %}
              {% endfor %}
                </div>
               <div class=\"col-md-4 col-12\">
                <h5 style=\"color: #F4991C;\">{{hotel.name}}</h5>
               <small>{{hotel.ville}}</small>  
                <p>{{hotel.shortdesc|slice(0, 500)|raw}}...</p>
                {# <p>Jusqu'a: 20/ 06 / 2021</p> #}
               </div> 
               <div class=\"col-md-5 col-12\">
                
                     <div  style=\"color: #212529; font-size: 13px;font-weight: bold;padding: 63px 38px;\">
     
      À partir : <input id=\"datedebut\" type=\"text\" style=\"width:43%;border-radius:none;
     height: 1.5rem; font-size:13px;\" class=\"form-control \" autocomplete=\"off\"  value=\"{{dated|date(\"d-m-Y\")}}\" name=\"datedebut\" required/>
       <div style=\"margin-top:14px\">
     Jusqu'au : <input id=\"datefin\" type=\"text\" style=\"width:43%;border-radius:none;
     height: 1.5rem; font-size:13px;\" class=\"form-control \" autocomplete=\"off\"  value=\"{{datef|date(\"d-m-Y\")}}\" name=\"datefin\" required/>
      </div>
                    
        
              </div>

            </div>
           
          <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >
    
    <tr style=\"background-color: #dee2e640; text-align:left;\">
      <th colspan=\"2\"  >Nombre de chambres à reserver</th>
      <th colspan=\"5\" >  {{ form_row(form.nbchambres, { 'attr': {'class': 'form-control','style':'width: 58px;font-size: 13px;'}}) }} </th>
    </tr>
    <tr style=\"background-color: #dee2e6b0;\" > 
      <th scope=\"col\" style=\"width: 600px;\">Chambre N°</th>
      <th scope=\"col\" style=\"width: 300px;\">Type de la chambre</th>
      <th scope=\"col\" style=\"width: 300px;\"> Adultes & Enfants</th>
      
      <th scope=\"col\" style=\"width: 400px;\">Avec</th>
      <th scope=\"col\" style=\"width: 25%;\">Suppléments</th>
      
    </tr>
  </thead>
  <tbody id=\"newDiv\">
                                                      
 </tbody>


</table>
<div class=\"btnReserver\"> <button type=\"submit\"  id=\"btsubmit\" class=\"btn \" style=\"color: #fff;
    cursor: not-allowed;
    float: right;
    background-image: linear-gradient(to right, #1e4b22 , #F4991C);\"  disabled ><i class=\"fa fa-edit\"> </i> Tarif & Dispo</button></div>

            
        </div>   













































 


 
    {{ form_end(form) }}
        </form>
    </div>
     </div >
   <script>
 function activer()
        {
            var select = document.getElementById( \"btob_hotelbundle_reservationhotel_typepayement\" )
 
            var otherFields = document.querySelectorAll( \".toDisplay\" )
            //var select = document.querySelectorAll( \".toDisplayselect\" )
            if ( select.value == \"Espèce\" ){
        
             Array.from( otherFields ).forEach( function ( select ) { select.style.display = \"none\" } )
            }
               
            else{
            Array.from( otherFields ).forEach( function ( input ) { input.style.display = \"inline-block\" } )
          //  document.getElementById(\"numcheque\").style.visibility=\"visible\";

            }
        }
        
     function logementchoice() {
       
       var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
       //    \$(\"#type0\").prop('id', 'type'+i);
          
            console.log( event.target.options[event.target.selectedIndex].text);
           
            str2 = event.target.id.replace ( /[^\\d.]/g, '' );
           // alert(str2);
            total = parseInt(str2, 10);
             console.log(total,i);
            if(total == i){
                 \$('#arrval'+i).val(event.target.options[event.target.selectedIndex].text);
       
            }
           
              
              

            }
      }      
        
        
        
        
        
        
        
        
        
        
  var h=0;
   var theIds = [];
  function myFunctione() {
       
       var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
           \$(\"#type0\").prop('id', 'type'+i);
          
            //console.log( event.target.options[event.target.selectedIndex].text);
           
            str2 = event.target.id.replace ( /[^\\d.]/g, '' );
           // alert(str2);
            total = parseInt(str2, 10);
            // alert(total);
            if(total == i){
                 \$('#typeval'+i).val( event.target.options[event.target.selectedIndex].text);
                  var option = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()).text(\$(\"#type\"+i).val());
              \$(\"#adultes\"+i).empty().append(option);
              for(j = \$(\"#type\"+i).val()-1; j > 0; j--){
                \$(\"#adultes\"+i).append(\$('<option>', {
                    value: j,
                    text: j
                }));
              }
             
    
              //\$(\"#adultes\"+i).val(\$(\"#type\"+i).val());
              var optionenfants = \$('<option></option>').attr(\"value\", 0).text(0);
              \$(\"#enfantage\"+i).empty().append(optionenfants);
              for(ef = 1; ef <= \$(\"#type\"+i).val()-1 ; ef++){
              //for(ef = \$(\"#type\"+i).val()-1; ef > 0; ef--){
                \$(\"#enfantage\"+i).append(\$('<option>', {
                    value: ef,
                    text: ef
                }));
              } 
            }
           
              var e = document.getElementById('type'+i);
              var strUser = e.options[e.selectedIndex].text;
              document.getElementById(\"hiddenselect\"+i).value = strUser;
          //  alert(strUser);
              

            }
      } 
function equal() {
     var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
                    var select = \$(\"#type\"+i).val();

                   //console.log(select);
             // \$(\"#adultes\"+i).change(function(){
               
               var valeurad = \$(\"#adultes\"+i).val();
               var valrest = (\$(\"#type\"+i).val()- valeurad) + 1;
               //   console.log( valrest);
               var optionenf = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()- valeurad).text(\$(\"#type\"+i).val() - valeurad);
                 \$(\"#enfantage\"+i).empty().append(optionenf);
                 for(e = valrest; e < \$(\"#type\"+i).val() ; e ++){
                \$(\"#enfantage\"+i).append(\$('<option>', {
                    value: e,
                    text: e
                }));
                 }
               if(valeurad != select){
                    var HTMLl='';
                for(en = 1; en <= \$(\"#enfantage\"+i).val(); en++){
                    HTMLl += ' Âge enfant N° '+en+': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select  name=\"ages'+i+'_'+en+'\" style=\"width:180px;border-radius: 4px; font-size: 13px;\" class=\"form-control\"> {% for k in 2..11 %} <option  value=\"{{k}}\">{{k}}</option> {% endfor %} </select><br/> '                                                            
                }
     
               \$('#agediv'+i).html(HTMLl);
                 }
           //   })
            }
}
function equalbyenfant() {
     var num = \$('#reservation_hotel_nbchambres').val();
            for(i = 1; i <= num; i++){
                    var select = \$(\"#type\"+i).val();

                   //console.log(select);
             // \$(\"#adultes\"+i).change(function(){
               
                  var valeuref = \$(\"#enfantage\"+i).val();
                  var valrest = (\$(\"#type\"+i).val()- valeuref) + 1;
                  console.log( \$(\"#type\"+i).val()- valeuref);
                  var optionad = \$('<option></option>').attr(\"value\", \$(\"#type\"+i).val()- valeuref).text(\$(\"#type\"+i).val() - valeuref);
                \$(\"#adultes\"+i).empty().append(optionad);
                // for(eft = 1; eft <= \$(\"#type\"+i).val() ; eft++){
                 for(eft = valrest; eft <= \$(\"#type\"+i).val() ; eft ++){
                    \$(\"#adultes\"+i).append(\$('<option>', {
                    value: eft,
                    text: eft
                }));
                 }
   // valeur.push( \$(this).val());
    //console.log(valeur);
    
    var HTMLl='';
    for(en = 1; en <= valeuref; en++){
        HTMLl += '  Âge enfant N° '+en+' : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select  name=\"ages'+i+'_'+en+'\" style=\"width:180px;border-radius: 4px; font-size: 13px;\" class=\"form-control\"> {% for k in 2..11 %} <option  value=\"{{k}}\">{{k}}</option> {% endfor %} </select><br/> '                                                            
      }
     
    \$('#agediv'+i).html(HTMLl);


  //  console.log(\$('#agediv'+\$( \"#enfantage\" ).val()).html(HTMLl));
  
            }
}
 {% set idenf = 0 %}
  \$(document).ready(function(){
      //var vals = \$(\"#enfantage\").val();
   var getDates = function(startDate, endDate) {
  var dates = [],
      currentDate = startDate,
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
  while (currentDate <= endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};

// Usage
var dates = getDates(new Date(), new Date({{ \"now\"|date_modify(\"+1 day\")|date(\"Y/m/d\") }}));                                                                                                           
dates.forEach(function(date) {
  console.log(date);
});

  \$('#reservation_hotel_nbchambres').change(function(){
    var num = \$(this).val();
    
    //console.log(vals);
    var HTML='';
   
    for(i = 1; i <= num; i++){
        
         HTML += '<tr style=\"background-color:#dee2e640\"> <td ><strong><br> Chambre N°  '+ i +':</strong> </td><td><br> <select  id=\"type'+i+'\" name=\"type'+i+'\" style=\"border-radius: 4px;width:180px;font-size: 13px;\" onchange=\"myFunctione()\" class=\"form-control\" required=\"required\" >{% for item in hotel.hotelroom  %} <option  value=\"{{ item.room.capacity }}\" {%if item.room.name ==\"Chambre double\" %} selected {%endif%}>{{ item.room.name }}</option>{% endfor %} </select> <input type=\"hidden\" id=\"typeval'+i+'\" name=\"typehidden'+i+'\" value=\"Chambre double\"/></td><td> <select  id=\"adultes'+i+'\" name=\"adultes'+i+'\" style=\"width:180px;border-radius: 4px;font-size: 13px;\" onchange=\"equal()\" class=\"form-control\"> {% for item in hotel.hotelroom  %}{%if item.room.name ==\"Chambre double\" %} {% for k in 1..item.room.capacity %} <option  value=\"{{k}}\" {% if k == item.room.capacity %}selected {%endif%}>{{k}}</option>  {% endfor %}{%endif%}{% endfor %} </select> <br>  <select  name=\"enfants'+i+'\" style=\"width:180px;border-radius: 4px;font-size: 13px;\" id=\"enfantage'+i+'\" onchange=\"equalbyenfant()\" class=\"form-control\"> {% for item in hotel.hotelroom  %}{%if loop.first %}{% for k in 0..item.room.capacity-1 %}  <option  value=\"{{k}}\">{{k}}</option>  {% endfor %} {% endif %}{% endfor %}  </select> <br> <div id=\"agediv'+i+'\"> </div><input type=\"hidden\" value=\"test\" id =\"hiddenselect'+i+'\" name=\"hiddenselect'+i+'\"> </td><td > <br>  <select  id=\"logement'+i+'\" name=\"logement'+i+'\" onchange=\"logementchoice()\" style=\"width:230px;border-radius: 4px;font-size: 13px;\" class=\"form-control\" required=\"required\">'+ {% for arr in allarrangement %}
            '   {%set totalprice = arr.price %} {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %}  <option  value=\"{{totalprice}}\"> {{arr.hotelarrangement.arrangement.name}}</option> {%else %}<option  value=\"{{(totalprice + arr.marge)}} \"> {{arr.hotelarrangement.arrangement.name}}</option> {%endif %}   '+
             {% endfor %}
             '{% for key,arr in allarrangement %} {%if key==0%}<input type=\"hidden\" id=\"arrval'+i+'\" name=\"arrhidden'+i+'\" value=\"{{arr.hotelarrangement.arrangement.name}}\"/>{%endif%} {%endfor%}</td><td style=\"text-align: left;\">{%for supp in allsupplement %}{%set totalprice = supp.price %} {% if supp.persm == 1 %}  {%set totalprice = totalprice + (supp.price * supp.marge)/100  %} {%else%} {%set totalprice = totalprice + supp.marge  %} {%endif%}<input type=\"checkbox\" id=\"supp'+i+'\" name=\"supp'+i+'_{{supp.id}}\" value=\"{{totalprice}}\"  style=\"height: 15px;\" class=\"checkbox\">{{supp.hotelsupplement.supplement.name}} (+ {{totalprice}} TND)<br/>{% endfor %}</td></tr>'
           
    
    
      \$('#newDiv').html(HTML);
   
   \$('#btsubmit').prop('disabled', false);
   \$('#btsubmit').css('cursor', 'pointer');
}  
  })       
         
 // \$(\"form\").submit(function(){
  //  if (\$('#textdatedebut').val() > \$('#textdatefin').val()) {
  //                                   alert('error')
  //   }
 // });
})

       </script>    



{% endblock %}", "default/chambres.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\chambres.html.twig");
    }
}
