<?php

/* default/reserver.html.twig */
class __TwigTemplate_03d2f85232349af33ff540925f06e56cc56e8a80355ce85f5640026c5a0a083d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/reserver.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af9a02db3a1a2c557da3529a255b6bc653052ba95bcdc3c32dd8527662916662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af9a02db3a1a2c557da3529a255b6bc653052ba95bcdc3c32dd8527662916662->enter($__internal_af9a02db3a1a2c557da3529a255b6bc653052ba95bcdc3c32dd8527662916662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/reserver.html.twig"));

        $__internal_747811a017b40f84195ab0514b366b576699bf21e90ce7fdd7e1e469f3404489 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_747811a017b40f84195ab0514b366b576699bf21e90ce7fdd7e1e469f3404489->enter($__internal_747811a017b40f84195ab0514b366b576699bf21e90ce7fdd7e1e469f3404489_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/reserver.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_af9a02db3a1a2c557da3529a255b6bc653052ba95bcdc3c32dd8527662916662->leave($__internal_af9a02db3a1a2c557da3529a255b6bc653052ba95bcdc3c32dd8527662916662_prof);

        
        $__internal_747811a017b40f84195ab0514b366b576699bf21e90ce7fdd7e1e469f3404489->leave($__internal_747811a017b40f84195ab0514b366b576699bf21e90ce7fdd7e1e469f3404489_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dad39c2da3ed979ddd9e4115bf4600e22c538bf13cf92355553326df7f2b637e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dad39c2da3ed979ddd9e4115bf4600e22c538bf13cf92355553326df7f2b637e->enter($__internal_dad39c2da3ed979ddd9e4115bf4600e22c538bf13cf92355553326df7f2b637e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_11906be13afac5fa43e547a358979a5a475261c8baaa17398639a6ea8c7b2445 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11906be13afac5fa43e547a358979a5a475261c8baaa17398639a6ea8c7b2445->enter($__internal_11906be13afac5fa43e547a358979a5a475261c8baaa17398639a6ea8c7b2445_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
       <div class=\"slider\"> 
          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                  
                  
                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
                
                  </div>
          
                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
                
                 </div>
                    
                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
          
                  </div>
                 
            
                </div>
                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Previous</span>
                </a>
                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Next</span>
                </a>
          
          </div>
        </div>

 <form method=\"post\" action=\"\">

        <div class=\"ReservationHotel container\">
            <ul id=\"menu\">
                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
                <li><a href=\"HotelEnTunisie.html\">Hotel En Tunisie</a></li>
                <li><a class=\"active\">Validation de la reservation</a></li>
            </ul>
            <div class=\"chambreBox\">
              <h5 >";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "name", array()), "html", null, true);
        echo " </h5> 
              <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                     ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "star", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 52
            echo "                        <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoil.png"), "html", null, true);
            echo "\" class=\"\" alt=\"...\">
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo " 
                    </li>
                     </ul>
              </div>
          </div>

          <div class=\"chambreBoxContent row\">
              <div class=\"col-md-3 col-12\">
                ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["img"] ?? $this->getContext($context, "img")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 62
            echo "                
                ";
            // line 63
            if ($this->getAttribute($context["loop"], "first", array())) {
                // line 64
                echo "                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["i"], "file", array())), "html", null, true);
                echo "\" class=\"w-100\" />
                ";
            }
            // line 66
            echo "                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "              
              </div>
             <div class=\"col-md col-12\">
              <h5 style=\"color: #F4991C;\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "name", array()), "html", null, true);
        echo "</h5>
             <small>";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "ville", array()), "html", null, true);
        echo "</small>  
              <p>À partir de : ";
        // line 72
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["dated"] ?? $this->getContext($context, "dated")), "d-m-Y"), "html", null, true);
        echo "</p>
              <p>Jusqu'au : ";
        // line 73
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["datef"] ?? $this->getContext($context, "datef")), "d-m-Y"), "html", null, true);
        echo "</p>
             </div> 
             
          </div>
           ";
        // line 77
        $context["difference"] = $this->getAttribute(twig_date_converter($this->env, ($context["datef"] ?? $this->getContext($context, "datef"))), "diff", array(0 => twig_date_converter($this->env, ($context["dated"] ?? $this->getContext($context, "dated")))), "method");
        // line 78
        echo "           ";
        $context["leftDays"] = $this->getAttribute(($context["difference"] ?? $this->getContext($context, "difference")), "days", array());
        // line 79
        echo "          <div class=\"container row\"> 
 
            <h6 style=\"margin-top: 1rem;color: #1f4b22;text-decoration: underline;\"> Récapitulation </h6>
            <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \">

              <thead>
                  <tr >
                      <th> Date de début </th>
                      <th> Date de fin</th>
                      <th> Hôtel </th>
                      <th>Nb.chambres </th>
                    
                  </tr>
                  </thead>
              <tbody>
          
                      <tr style=\"background-color:#fff\">
                         
                          <td data-label=\"Date de début\">";
        // line 97
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["dated"] ?? $this->getContext($context, "dated")), "d-m-Y"), "html", null, true);
        echo "</td>
                          <td data-label=\"Date de fin\">";
        // line 98
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["datef"] ?? $this->getContext($context, "datef")), "d-m-Y"), "html", null, true);
        echo "</td>
                          <td data-label=\"Hotel\" >";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "name", array()), "html", null, true);
        echo "</td>
                          <td data-label=\"Nombre des chambres\">1</td>
     
                      </tr>

              </tbody>


            </table>
                                     <table class=\"table text-center table-bordered \" style=\"margin-top:1rem;\" >
           
                        <tr>
                          <th scope=\"col\" style=\"width: 50px;\">Num</th> 
                          <th scope=\"col\" style=\"width: 600px;\">Type de chambres</th>
                          <th scope=\"col\" style=\"width: 700px;\">Arrangement</th>
                          <th scope=\"col\" style=\"width: 400px;\">Nb</th>
                          <th scope=\"col\" style=\"width: 300px;\">Adultes</th>
                          <th scope=\"col\" style=\"width: 300px;\">Enfants</th>
                          <th scope=\"col\" style=\"width: 400px;\">Disponiblité</th>
                          <th scope=\"col\" style=\"width: 400px;\">Prix pour une nuitée(s)</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                         ";
        // line 126
        echo "                           <td data-label=\"Num\" scope=\"row\">1</td>
                           
                          ";
        // line 129
        echo "                            <td data-label=\"Type de chambres\" scope=\"row\">Chambre Double</td>

                         ";
        // line 131
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 132
            echo "                      ";
            if (($this->getAttribute($context["c"], "hotel", array()) == ($context["hotel"] ?? $this->getContext($context, "hotel")))) {
                // line 133
                echo "                      
                          ";
                // line 134
                if ((twig_length_filter($this->env, $this->getAttribute($context["c"], "pricearr", array())) > 0)) {
                    echo "                
   
                    
                
                       <td data-label=\"Arrangement\"> 
            
                         <select name=\"arr\" id=\"monselect";
                    // line 140
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                    echo "\" class=\"form-control formarrang\" style=\"font-size:14px;\">
                         ";
                    // line 141
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                        // line 142
                        echo "                               ";
                        if (($this->getAttribute($context["arr"], "etat", array()) == 1)) {
                            // line 143
                            echo "                        <option id=\"opt_";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\" value=\"";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\">
                            ";
                            // line 144
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["arr"], "hotelarrangement", array()), "arrangement", array()), "name", array()), "html", null, true);
                            echo "
                        </option>
                        ";
                        }
                        // line 147
                        echo "                         ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 148
                    echo "                        </select>

                        </td>

                       
                   
                            <script type=\"text/javascript\">
    
                                \$(document).ready(function(){
                                
                                \$(\"#monselect";
                    // line 158
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                    echo "\").change(function() {
                                 
                                 ";
                    // line 160
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        echo " 
                                \t//\tvar selected = \$(\"#monselect";
                        // line 161
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                        echo " :selected\").val();
                                \t\tif (\$(\"#monselect";
                        // line 162
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                        echo " :selected\").val() == \$(\"#opt_\"+";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                        echo ").val())
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#monselect";
                        // line 164
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                        echo " :selected\").val();
                                \t    \tvar total = '#total'+\$(\"#monselect";
                        // line 165
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                        echo " :selected\").val();
                                \t\t\t\$(comp).show('slow');
                                \t\t\t\$(total).show('slow');
                                \t\t}
                                \t\telse
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#opt_\"+";
                        // line 171
                        echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                        echo ").val();
                                \t\t\tvar total = '#total'+\$(\"#opt_\"+";
                        // line 172
                        echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                        echo ").val();
                                \t\t\t\$(comp).hide();
                                \t\t\t\$(total).hide();
                                \t\t}
                                // \tvar selected = \$(\"#monselect";
                        // line 176
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["hotel"] ?? $this->getContext($context, "hotel")), "id", array()), "html", null, true);
                        echo " :selected\").val();
                                // \t\$(\"#a\").prop(\"href\", \"https://waytolearnx.com/\");
                                \t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 178
                    echo "\t\t
                                 
                                \t});
                                   })  

                                </script> 
                    ";
                } else {
                    // line 185
                    echo "                     <td data-label=\"Arrangement\"><span class=\"label  ptip_ne\" style=\"    background-color: #cc3300;
                    color: #fff;
                    padding: 3px 12px 4px 15px;
                    border-radius: 4px;
                    font-size: 12px !important;
                    height: 22px;\" > Aucun arrangement ajouté </span> </td>
                                      ";
                }
                // line 192
                echo "                                      ";
            }
            // line 193
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 194
        echo "                      
                      
                      
                      
                      
                   ";
        // line 200
        echo "                   ";
        // line 201
        echo "                            <td data-label=\"Nb\">2</td>
                       <td data-label=\"Adultes\">2</td>
                          ";
        // line 204
        echo "                          ";
        // line 205
        echo "                          <td data-label=\"Enfants\">0</td>
                          <td data-label=\"Disponiblité\"><span class=\"label  ptip_ne\" style=\"    background-color: #28a745;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;\" > Disponible </span></td>
                          <td data-label=\"Total\"> 
                       ";
        // line 213
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 214
            echo "                      ";
            if (($this->getAttribute($context["c"], "hotel", array()) == ($context["hotel"] ?? $this->getContext($context, "hotel")))) {
                // line 215
                echo "                        
               
                  ";
                // line 217
                $context["array"] = array();
                // line 218
                echo "                        ";
                $context["j"] = 0;
                // line 219
                echo "                     ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                    // line 220
                    echo "                    
                
                     ";
                    // line 222
                    $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                    // line 223
                    echo "              ";
                    $context["array"] = array("key" =>                     // line 224
($context["totalprice"] ?? $this->getContext($context, "totalprice")));
                    // line 227
                    echo "                     ";
                    $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                    // line 228
                    echo "                     ";
                    if ((((($context["j"] ?? $this->getContext($context, "j")) == 0) && ($this->getAttribute($context["arr"], "price", array()) == min(($context["array"] ?? $this->getContext($context, "array"))))) && ($this->getAttribute($context["arr"], "etat", array()) == 1))) {
                        // line 229
                        echo "                     <span class=\"label label-warning ptip_ne\"  id=\"comp_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                        echo "\" >  
          ";
                        // line 230
                        if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                            echo "  ";
                            $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                            echo " ";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND ";
                        } else {
                            echo " ";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND";
                        }
                        echo "  
                  </span>
          
          ";
                        // line 233
                        $context["j"] = 1;
                        // line 234
                        echo "                  ";
                    } else {
                        // line 235
                        echo "                        <span class=\"label label-warning ptip_ne\" style=\"display:none;\" id=\"comp_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                        echo "\" >  
          ";
                        // line 236
                        if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                            echo "  ";
                            $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                            echo " ";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND ";
                        } else {
                            echo " ";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND";
                        }
                        echo "  
                  </span>
                  ";
                    }
                    // line 239
                    echo "                 
                     ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 241
                echo "                      ";
                // line 242
                echo "                       
                        ";
            }
            // line 244
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 245
        echo "                    </td>
                          </tr>
                          <tr class=\"disp\">
                         <td> </td>
                         <td></td>
                         <td style=\"text-align:right;\"> <strong> TOTAL : </strong></td> 
                         <td>2</td>
                          <td>2</td>
                           ";
        // line 254
        echo "                          ";
        // line 255
        echo "                           <td>0</td>
                          <td></td>
                            ";
        // line 258
        echo "                          <td>
                            
                              
                                ";
        // line 261
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 262
            echo "                      ";
            if (($this->getAttribute($context["c"], "hotel", array()) == ($context["hotel"] ?? $this->getContext($context, "hotel")))) {
                // line 263
                echo "                        ";
                $context["array"] = array();
                // line 264
                echo "                       ";
                $context["j"] = 0;
                // line 265
                echo "                     ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                    // line 266
                    echo "                    
                
                     ";
                    // line 268
                    $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                    // line 269
                    echo "              ";
                    $context["array"] = array("key" =>                     // line 270
($context["totalprice"] ?? $this->getContext($context, "totalprice")));
                    // line 273
                    echo "                     ";
                    $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                    // line 274
                    echo "                     ";
                    if ((((($context["j"] ?? $this->getContext($context, "j")) == 0) && ($this->getAttribute($context["arr"], "price", array()) == min(($context["array"] ?? $this->getContext($context, "array"))))) && ($this->getAttribute($context["arr"], "etat", array()) == 1))) {
                        // line 275
                        echo "         <span class=\"label label-warning ptip_ne\"  id=\"total";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                        echo "\">  ";
                        if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                            echo "  ";
                            $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                            echo "<input type=\"hidden\" name=\"total\" value=\"";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo "\"/> ";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND ";
                        } else {
                            echo "<input type=\"hidden\" name=\"total\" value=\"";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo "\"/> ";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND";
                        }
                        echo "  </span>
                  ";
                        // line 276
                        $context["j"] = 1;
                        // line 277
                        echo "                   ";
                    } else {
                        // line 278
                        echo "         <span class=\"label label-warning ptip_ne\"  style=\"display:none;\" id=\"total";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                        echo "\">  ";
                        if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                            echo "  ";
                            $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                            echo " <input type=\"hidden\" name=\"total\" value=\"";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo "\"/> ";
                            echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND ";
                        } else {
                            echo "<input type=\"hidden\" name=\"total\" value=\"";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo "\"/> ";
                            echo twig_escape_filter($this->env, (((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2) * ($context["leftDays"] ?? $this->getContext($context, "leftDays"))), "html", null, true);
                            echo " TND";
                        }
                        echo "  </span>

                    ";
                    }
                    // line 281
                    echo "                     ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 282
                echo "                        ";
                // line 283
                echo "                        ";
            }
            // line 284
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "</td>
                          </tr>
                          </tr>
                        
                      </tbody>
                    
                    
                </table>
          </div>
          <div class=\"chambreBox\">
            <h5 >Informations sur le client </h5> 
          </div>
          <div class=\"w-box-content \">
            <div class=\"row\">
                <div class=\"col-md-12\">
  <div id=\"msgpoint\"></div>
</div>

                <div class=\"col-md-6\">
                   <label  class=\"required\"> C.I.N / Passeport* : </label>
                      ";
        // line 304
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "cin", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo "
                        <label >  Civilité : </label>
                      ";
        // line 306
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "civilite", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;font-size:12px; padding: 2px;")));
        echo "
                      <label> Nom : </label>
                      ";
        // line 308
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo "
                    <label>  Prénom :</label>
                       ";
        // line 310
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     
                        <label> Email :</label>
                       ";
        // line 313
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     <br/>
                </div>
                <div class=\"col-md-6\">
                  <label  class=\"required\">   Téléphone :</label>
                       ";
        // line 318
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mobile", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                       
                    <label> Adresse :</label>
                      ";
        // line 321
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "adulte", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                      <label> Code postale :</label>
                      ";
        // line 323
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "adultes", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     <label> Nom de ville : </label>
                      ";
        // line 325
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "demandes", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                       <label> Pays :</label>
                      ";
        // line 327
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "pays", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     <br/>
                </div>
                <input type=\"hidden\" id=\"btob_hotelbundle_clients__token\" name=\"btob_hotelbundle_clients[_token]\" value=\"tsaxezyMkS-Ga6ckebSeqKa4-aiedppR39WDZpWHrRA\">
            </div>
            <div class=\"clearfix\"></div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Liste des personnes / Rooming list </h5> 
          </div>
          <div class=\"w-box-content \">
            <div class=\"\">
                <div class=\"col-md-12\">
                                                                                                                                                                                <div id=\"no-more-tables\">
                                <table class=\"table table-bordered table-condensed cf\" style=\"margin-top: 1rem;\">
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                       
                                        <th> Chambre  </th>
                                        <th> Noms des occupants</th>
                                        <th> Âges </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                       
                                        
                                        <tr >
                                            <td>Adulte (*) </td>
                                             <td data-label=\"Nom\">   <input type=\"text\" name=\"nomadulte\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\">  <input  type=\"hidden\" name=\"idchambre\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\" value=\"\"> </td>
                                     
                                        <td data-label=\"Age\"> <input  type=\"number\" name=\"ageadulte\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                        
                                        <tr >
                                              <td>Enfant ou adulte (*) </td>
                                             <td data-label=\"Nom\">  <input  type=\"text\" name=\"nomenf\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"> </td>
                                      
                                        <td data-label=\"Age\"> <input  type=\"number\" name=\"ageenf\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                          
                                    
                                    </tbody>
                                                                                                                                       </tbody>

                                </table>
                            </div>
                                                                                                                                        </div>
                <div class=\"clearfix\"></div>
            </div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Recommandations </h5> 
          </div>
          <div class=\"w-box-content row \" style=\"padding: 1rem;\">
            <div class=\"col-md-12 row\">
                            <div class=\"col-md-4\">
                                                                        <div class=\"formSep \">
                                                                            <label class=\"checkbox\" > 
                                                                            <input type=\"checkbox\" name=\"options[]\"
                                                                                                           value=\"Séjour de noces\"/>&nbsp;Séjour de noces </label> 
                                                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"De préférence chambre avec 2 lits\"/>&nbsp;De préférence chambre avec 2 lits
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue piscine\"/>&nbsp;Si possible, chambre vue piscine
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue jardin\"/>&nbsp;Si possible, chambre vue jardin
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ tardif\"/>&nbsp;Si possible, départ tardif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Prière prévoir un lit bébé\"/>&nbsp;Prière prévoir un lit bébé
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour infos, séjour de noces\"/>&nbsp;Pour infos, séjour de noces
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour info, arrivée tardive\"/>&nbsp;Pour info, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ actif\"/>&nbsp;Si possible, départ actif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, grand lit\"/>&nbsp;Si possible, grand lit
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres non fumeur\"/>&nbsp;Si possible, chambres non fumeur
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres côte à côte\"/>&nbsp;Si possible, chambres côte à côte
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres communicantes\"/>&nbsp;Si possible, chambres communicantes
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, arrivée tardive\"/>&nbsp;Si possible, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
            </div>
            
            <div class=\"col-md-12\">
                <div>
                    <label for=\"recommandations\" class=\"required\"><strong>Vos remarques /
                            Recommandations :</strong></label>
                    <textarea id=\"recommandations\" name=\"recommandations\" class=\"textarea-interne\"></textarea>

                </div>
            </div>
            <div class=\"clearfix\"></div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Mode de Paiement </h5> 
          </div>
          <div class=\"warpper\">
            <input class=\"radio\" id=\"one\" name=\"group\" type=\"radio\" value=\"espece\" checked >
            <input class=\"radio\" id=\"two\" name=\"group\" type=\"radio\" value=\"Virement\">
            <input class=\"radio\" id=\"three\" name=\"group\" type=\"radio\" value=\"versement\">
            <div class=\"tabs\">
            
             ";
        // line 503
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actagence", array()) == 1)) {
            // line 504
            echo "                <label class=\"tab\" id=\"one-tab\" for=\"one\">Paiement à l'agence</label>
                ";
        }
        // line 506
        echo "                ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actrib", array()) == 1)) {
            // line 507
            echo "                <label class=\"tab\" id=\"two-tab\" for=\"two\">Virement bancaire</label>
                ";
        }
        // line 509
        echo "                    ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actvers", array()) == 1)) {
            // line 510
            echo "                <label class=\"tab\" id=\"three-tab\" for=\"three\">Versement espéces</label>
                ";
        }
        // line 512
        echo "                ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actmond", array()) == 1)) {
            // line 513
            echo "                <label class=\"tab\" id=\"for-tab\" for=\"for\">Mondat</label>
            ";
        }
        // line 515
        echo "         
       
            
        
              </div>
            <div class=\"panels\" >
            <div class=\"panel\" id=\"one-panel\" >
              <div class=\"panel-title\" >Adresses :</div>
              <p style=\"line-height:1.25px;\">
             ";
        // line 524
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescagence", array());
        echo "
          
              </p>
              
            </div>
            <div class=\"panel\" id=\"two-panel\">
              <div class=\"panel-title\">Virement Bancaire</div>
              <p>";
        // line 531
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescrib", array());
        echo "</p>
            </div>
            <div class=\"panel\" id=\"three-panel\">
              <div class=\"panel-title\">Versement en espéces</div>
              <p>";
        // line 535
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescvers", array());
        echo "</p>
            </div>
          
              <div class=\"panel\" id=\"four-panel\">
              <div class=\"panel-title\">Mondat</div>
              <p>";
        // line 540
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescmond", array());
        echo "</p>
            </div>
            </div>
            
          </div>
          <div class=\"btnlisthotel2 w-100\">
                    <button type=\"submit\"  id=\"btsubmit\" class=\"btn btn-primary\" ><i class=\"fa fa-check\"> </i> Réserver</button></div>  


            
            </div> 
        </div>
</form>
";
        
        $__internal_11906be13afac5fa43e547a358979a5a475261c8baaa17398639a6ea8c7b2445->leave($__internal_11906be13afac5fa43e547a358979a5a475261c8baaa17398639a6ea8c7b2445_prof);

        
        $__internal_dad39c2da3ed979ddd9e4115bf4600e22c538bf13cf92355553326df7f2b637e->leave($__internal_dad39c2da3ed979ddd9e4115bf4600e22c538bf13cf92355553326df7f2b637e_prof);

    }

    public function getTemplateName()
    {
        return "default/reserver.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  964 => 540,  956 => 535,  949 => 531,  939 => 524,  928 => 515,  924 => 513,  921 => 512,  917 => 510,  914 => 509,  910 => 507,  907 => 506,  903 => 504,  901 => 503,  722 => 327,  717 => 325,  712 => 323,  707 => 321,  701 => 318,  693 => 313,  687 => 310,  682 => 308,  677 => 306,  672 => 304,  645 => 284,  642 => 283,  640 => 282,  634 => 281,  611 => 278,  608 => 277,  606 => 276,  585 => 275,  582 => 274,  579 => 273,  577 => 270,  575 => 269,  573 => 268,  569 => 266,  564 => 265,  561 => 264,  558 => 263,  555 => 262,  551 => 261,  546 => 258,  542 => 255,  540 => 254,  530 => 245,  524 => 244,  520 => 242,  518 => 241,  511 => 239,  495 => 236,  490 => 235,  487 => 234,  485 => 233,  469 => 230,  464 => 229,  461 => 228,  458 => 227,  456 => 224,  454 => 223,  452 => 222,  448 => 220,  443 => 219,  440 => 218,  438 => 217,  434 => 215,  431 => 214,  427 => 213,  417 => 205,  415 => 204,  411 => 201,  409 => 200,  402 => 194,  396 => 193,  393 => 192,  384 => 185,  375 => 178,  366 => 176,  359 => 172,  355 => 171,  346 => 165,  342 => 164,  335 => 162,  331 => 161,  325 => 160,  320 => 158,  308 => 148,  302 => 147,  296 => 144,  289 => 143,  286 => 142,  282 => 141,  278 => 140,  269 => 134,  266 => 133,  263 => 132,  259 => 131,  255 => 129,  251 => 126,  223 => 99,  219 => 98,  215 => 97,  195 => 79,  192 => 78,  190 => 77,  183 => 73,  179 => 72,  175 => 71,  171 => 70,  166 => 67,  152 => 66,  146 => 64,  144 => 63,  141 => 62,  124 => 61,  114 => 53,  105 => 52,  101 => 51,  94 => 47,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
       <div class=\"slider\"> 
          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                  
                  
                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
                
                  </div>
          
                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
                
                 </div>
                    
                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
          
                  </div>
                 
            
                </div>
                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Previous</span>
                </a>
                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                  <span class=\"sr-only\">Next</span>
                </a>
          
          </div>
        </div>

 <form method=\"post\" action=\"\">

        <div class=\"ReservationHotel container\">
            <ul id=\"menu\">
                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
                <li><a href=\"HotelEnTunisie.html\">Hotel En Tunisie</a></li>
                <li><a class=\"active\">Validation de la reservation</a></li>
            </ul>
            <div class=\"chambreBox\">
              <h5 >{{hotel.name}} </h5> 
              <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                     {% for i in 1..hotel.star %}
                        <img src=\"{{asset('assets/image/etoil.png')}}\" class=\"\" alt=\"...\">
                      {% endfor %} 
                    </li>
                     </ul>
              </div>
          </div>

          <div class=\"chambreBoxContent row\">
              <div class=\"col-md-3 col-12\">
                {% for i in img %}
                
                {% if loop.first %}
                <img src=\"{{asset(i.file)}}\" class=\"w-100\" />
                {% endif %}
                {% endfor %}
              
              </div>
             <div class=\"col-md col-12\">
              <h5 style=\"color: #F4991C;\">{{hotel.name}}</h5>
             <small>{{hotel.ville}}</small>  
              <p>À partir de : {{dated|date('d-m-Y')}}</p>
              <p>Jusqu'au : {{datef|date('d-m-Y')}}</p>
             </div> 
             
          </div>
           {% set difference = date(datef).diff(date(dated)) %}
           {% set leftDays = difference.days %}
          <div class=\"container row\"> 
 
            <h6 style=\"margin-top: 1rem;color: #1f4b22;text-decoration: underline;\"> Récapitulation </h6>
            <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \">

              <thead>
                  <tr >
                      <th> Date de début </th>
                      <th> Date de fin</th>
                      <th> Hôtel </th>
                      <th>Nb.chambres </th>
                    
                  </tr>
                  </thead>
              <tbody>
          
                      <tr style=\"background-color:#fff\">
                         
                          <td data-label=\"Date de début\">{{dated|date('d-m-Y')}}</td>
                          <td data-label=\"Date de fin\">{{datef|date('d-m-Y')}}</td>
                          <td data-label=\"Hotel\" >{{hotel.name}}</td>
                          <td data-label=\"Nombre des chambres\">1</td>
     
                      </tr>

              </tbody>


            </table>
                                     <table class=\"table text-center table-bordered \" style=\"margin-top:1rem;\" >
           
                        <tr>
                          <th scope=\"col\" style=\"width: 50px;\">Num</th> 
                          <th scope=\"col\" style=\"width: 600px;\">Type de chambres</th>
                          <th scope=\"col\" style=\"width: 700px;\">Arrangement</th>
                          <th scope=\"col\" style=\"width: 400px;\">Nb</th>
                          <th scope=\"col\" style=\"width: 300px;\">Adultes</th>
                          <th scope=\"col\" style=\"width: 300px;\">Enfants</th>
                          <th scope=\"col\" style=\"width: 400px;\">Disponiblité</th>
                          <th scope=\"col\" style=\"width: 400px;\">Prix pour une nuitée(s)</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                         {# {% set array = [] %} {% for item in h.hotelroom  %} {% set array = item.room.capacity %}{% endfor %}
                       {{array}} #}
                           <td data-label=\"Num\" scope=\"row\">1</td>
                           
                          {#<td scope=\"row\">{{namebycapacity}}</td>#}
                            <td data-label=\"Type de chambres\" scope=\"row\">Chambre Double</td>

                         {% for c in contrats %}
                      {% if c.hotel == hotel %}
                      
                          {%if c.pricearr|length > 0 %}                
   
                    
                
                       <td data-label=\"Arrangement\"> 
            
                         <select name=\"arr\" id=\"monselect{{hotel.id}}\" class=\"form-control formarrang\" style=\"font-size:14px;\">
                         {%for arr in c.pricearr%}
                               {%if arr.etat == 1%}
                        <option id=\"opt_{{arr.id}}\" value=\"{{arr.id}}\">
                            {{arr.hotelarrangement.arrangement.name}}
                        </option>
                        {%endif%}
                         {%endfor%}
                        </select>

                        </td>

                       
                   
                            <script type=\"text/javascript\">
    
                                \$(document).ready(function(){
                                
                                \$(\"#monselect{{hotel.id}}\").change(function() {
                                 
                                 {%for i in c.pricearr%} 
                                \t//\tvar selected = \$(\"#monselect{{hotel.id}} :selected\").val();
                                \t\tif (\$(\"#monselect{{hotel.id}} :selected\").val() == \$(\"#opt_\"+{{i.id}}).val())
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#monselect{{hotel.id}} :selected\").val();
                                \t    \tvar total = '#total'+\$(\"#monselect{{hotel.id}} :selected\").val();
                                \t\t\t\$(comp).show('slow');
                                \t\t\t\$(total).show('slow');
                                \t\t}
                                \t\telse
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#opt_\"+{{i.id}}).val();
                                \t\t\tvar total = '#total'+\$(\"#opt_\"+{{i.id}}).val();
                                \t\t\t\$(comp).hide();
                                \t\t\t\$(total).hide();
                                \t\t}
                                // \tvar selected = \$(\"#monselect{{hotel.id}} :selected\").val();
                                // \t\$(\"#a\").prop(\"href\", \"https://waytolearnx.com/\");
                                \t{%endfor%}\t\t
                                 
                                \t});
                                   })  

                                </script> 
                    {%else%}
                     <td data-label=\"Arrangement\"><span class=\"label  ptip_ne\" style=\"    background-color: #cc3300;
                    color: #fff;
                    padding: 3px 12px 4px 15px;
                    border-radius: 4px;
                    font-size: 12px !important;
                    height: 22px;\" > Aucun arrangement ajouté </span> </td>
                                      {%endif%}
                                      {%endif%}
                                    {%endfor%}
                      
                      
                      
                      
                      
                   {#{%endif%}#}
                   {# {%endfor%}#}
                            <td data-label=\"Nb\">2</td>
                       <td data-label=\"Adultes\">2</td>
                          {#<td>{{mincapacity}}</td>#}
                          {#<td>{{mincapacity}}</td>#}
                          <td data-label=\"Enfants\">0</td>
                          <td data-label=\"Disponiblité\"><span class=\"label  ptip_ne\" style=\"    background-color: #28a745;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;\" > Disponible </span></td>
                          <td data-label=\"Total\"> 
                       {% for c in contrats %}
                      {% if c.hotel == hotel %}
                        
               
                  {% set array = {}%}
                        {% set j = 0%}
                     {%for arr in c.pricearr%}
                    
                
                     {%set totalprice = arr.price %}
              {% set array = {
                        key : totalprice,
                    }
                    %}
                     {%set totalprice = arr.price %}
                     {%if j==0 and arr.price == min(array) and  arr.etat==1%}
                     <span class=\"label label-warning ptip_ne\"  id=\"comp_{{arr.id}}\" >  
          {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice *2*leftDays}} TND {%else %} {{((totalprice + arr.marge) * 2)*leftDays }} TND{%endif %}  
                  </span>
          
          {% set j = 1%}
                  {%else%}
                        <span class=\"label label-warning ptip_ne\" style=\"display:none;\" id=\"comp_{{arr.id}}\" >  
          {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice *2*leftDays}} TND {%else %} {{((totalprice + arr.marge) * 2)*leftDays }} TND{%endif %}  
                  </span>
                  {% endif %}
                 
                     {% endfor %}
                      {#{{c.price * mincapacity }} TND #}
                       
                        {% endif %}
                    {% endfor %}
                    </td>
                          </tr>
                          <tr class=\"disp\">
                         <td> </td>
                         <td></td>
                         <td style=\"text-align:right;\"> <strong> TOTAL : </strong></td> 
                         <td>2</td>
                          <td>2</td>
                           {#<td>{{mincapacity}}</td>#}
                          {#<td>{{mincapacity}}</td>#}
                           <td>0</td>
                          <td></td>
                            {#  #}
                          <td>
                            
                              
                                {% for c in contrats %}
                      {% if c.hotel == hotel %}
                        {% set array = {}%}
                       {% set j = 0%}
                     {%for arr in c.pricearr%}
                    
                
                     {%set totalprice = arr.price %}
              {% set array = {
                        key : totalprice,
                    }
                    %}
                     {%set totalprice = arr.price %}
                     {%if j==0 and arr.price == min(array) and arr.etat==1%}
         <span class=\"label label-warning ptip_ne\"  id=\"total{{arr.id}}\">  {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %}<input type=\"hidden\" name=\"total\" value=\"{{totalprice * 2*leftDays}}\"/> {{totalprice * 2 *leftDays}} TND {%else %}<input type=\"hidden\" name=\"total\" value=\"{{((totalprice + arr.marge) * 2)* leftDays}}\"/> {{((totalprice + arr.marge) * 2)* leftDays}} TND{%endif %}  </span>
                  {% set j = 1%}
                   {% else %}
         <span class=\"label label-warning ptip_ne\"  style=\"display:none;\" id=\"total{{arr.id}}\">  {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} <input type=\"hidden\" name=\"total\" value=\"{{totalprice * 2*leftDays}}\"/> {{totalprice * 2*leftDays}} TND {%else %}<input type=\"hidden\" name=\"total\" value=\"{{((totalprice + arr.marge) * 2)*leftDays}}\"/> {{((totalprice + arr.marge) * 2)*leftDays}} TND{%endif %}  </span>

                    {%endif%}
                     {% endfor %}
                        {#<span class=\"label label-warning ptip_ne\" >{{c.price * mincapacity }} TND </span>   #}
                        {% endif %}
                    {% endfor %}</td>
                          </tr>
                          </tr>
                        
                      </tbody>
                    
                    
                </table>
          </div>
          <div class=\"chambreBox\">
            <h5 >Informations sur le client </h5> 
          </div>
          <div class=\"w-box-content \">
            <div class=\"row\">
                <div class=\"col-md-12\">
  <div id=\"msgpoint\"></div>
</div>

                <div class=\"col-md-6\">
                   <label  class=\"required\"> C.I.N / Passeport* : </label>
                      {{ form_widget(form.cin, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\",}}) }}
                        <label >  Civilité : </label>
                      {{ form_widget(form.civilite, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;font-size:12px; padding: 2px;\"}}) }}
                      <label> Nom : </label>
                      {{ form_widget(form.nom, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\",}}) }}
                    <label>  Prénom :</label>
                       {{ form_widget(form.prenom, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     
                        <label> Email :</label>
                       {{ form_widget(form.email, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     <br/>
                </div>
                <div class=\"col-md-6\">
                  <label  class=\"required\">   Téléphone :</label>
                       {{ form_widget(form.mobile, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                       
                    <label> Adresse :</label>
                      {{ form_widget(form.adulte, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                      <label> Code postale :</label>
                      {{ form_widget(form.adultes, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     <label> Nom de ville : </label>
                      {{ form_widget(form.demandes, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                       <label> Pays :</label>
                      {{ form_widget(form.pays, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     <br/>
                </div>
                <input type=\"hidden\" id=\"btob_hotelbundle_clients__token\" name=\"btob_hotelbundle_clients[_token]\" value=\"tsaxezyMkS-Ga6ckebSeqKa4-aiedppR39WDZpWHrRA\">
            </div>
            <div class=\"clearfix\"></div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Liste des personnes / Rooming list </h5> 
          </div>
          <div class=\"w-box-content \">
            <div class=\"\">
                <div class=\"col-md-12\">
                                                                                                                                                                                <div id=\"no-more-tables\">
                                <table class=\"table table-bordered table-condensed cf\" style=\"margin-top: 1rem;\">
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                       
                                        <th> Chambre  </th>
                                        <th> Noms des occupants</th>
                                        <th> Âges </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                       
                                        
                                        <tr >
                                            <td>Adulte (*) </td>
                                             <td data-label=\"Nom\">   <input type=\"text\" name=\"nomadulte\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\">  <input  type=\"hidden\" name=\"idchambre\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\" value=\"\"> </td>
                                     
                                        <td data-label=\"Age\"> <input  type=\"number\" name=\"ageadulte\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                        
                                        <tr >
                                              <td>Enfant ou adulte (*) </td>
                                             <td data-label=\"Nom\">  <input  type=\"text\" name=\"nomenf\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"> </td>
                                      
                                        <td data-label=\"Age\"> <input  type=\"number\" name=\"ageenf\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                          
                                    
                                    </tbody>
                                                                                                                                       </tbody>

                                </table>
                            </div>
                                                                                                                                        </div>
                <div class=\"clearfix\"></div>
            </div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Recommandations </h5> 
          </div>
          <div class=\"w-box-content row \" style=\"padding: 1rem;\">
            <div class=\"col-md-12 row\">
                            <div class=\"col-md-4\">
                                                                        <div class=\"formSep \">
                                                                            <label class=\"checkbox\" > 
                                                                            <input type=\"checkbox\" name=\"options[]\"
                                                                                                           value=\"Séjour de noces\"/>&nbsp;Séjour de noces </label> 
                                                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"De préférence chambre avec 2 lits\"/>&nbsp;De préférence chambre avec 2 lits
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue piscine\"/>&nbsp;Si possible, chambre vue piscine
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue jardin\"/>&nbsp;Si possible, chambre vue jardin
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ tardif\"/>&nbsp;Si possible, départ tardif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Prière prévoir un lit bébé\"/>&nbsp;Prière prévoir un lit bébé
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour infos, séjour de noces\"/>&nbsp;Pour infos, séjour de noces
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour info, arrivée tardive\"/>&nbsp;Pour info, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ actif\"/>&nbsp;Si possible, départ actif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, grand lit\"/>&nbsp;Si possible, grand lit
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres non fumeur\"/>&nbsp;Si possible, chambres non fumeur
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres côte à côte\"/>&nbsp;Si possible, chambres côte à côte
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres communicantes\"/>&nbsp;Si possible, chambres communicantes
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, arrivée tardive\"/>&nbsp;Si possible, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
            </div>
            
            <div class=\"col-md-12\">
                <div>
                    <label for=\"recommandations\" class=\"required\"><strong>Vos remarques /
                            Recommandations :</strong></label>
                    <textarea id=\"recommandations\" name=\"recommandations\" class=\"textarea-interne\"></textarea>

                </div>
            </div>
            <div class=\"clearfix\"></div>
          </div>
          <div class=\"chambreBox\">
            <h5 >Mode de Paiement </h5> 
          </div>
          <div class=\"warpper\">
            <input class=\"radio\" id=\"one\" name=\"group\" type=\"radio\" value=\"espece\" checked >
            <input class=\"radio\" id=\"two\" name=\"group\" type=\"radio\" value=\"Virement\">
            <input class=\"radio\" id=\"three\" name=\"group\" type=\"radio\" value=\"versement\">
            <div class=\"tabs\">
            
             {% if payement.actagence == 1 %}
                <label class=\"tab\" id=\"one-tab\" for=\"one\">Paiement à l'agence</label>
                {% endif %}
                {% if payement.actrib == 1 %}
                <label class=\"tab\" id=\"two-tab\" for=\"two\">Virement bancaire</label>
                {% endif %}
                    {% if payement.actvers == 1 %}
                <label class=\"tab\" id=\"three-tab\" for=\"three\">Versement espéces</label>
                {%endif %}
                {% if payement.actmond == 1 %}
                <label class=\"tab\" id=\"for-tab\" for=\"for\">Mondat</label>
            {%endif %}
         
       
            
        
              </div>
            <div class=\"panels\" >
            <div class=\"panel\" id=\"one-panel\" >
              <div class=\"panel-title\" >Adresses :</div>
              <p style=\"line-height:1.25px;\">
             {{payement.longdescagence|raw}}
          
              </p>
              
            </div>
            <div class=\"panel\" id=\"two-panel\">
              <div class=\"panel-title\">Virement Bancaire</div>
              <p>{{payement.longdescrib|raw}}</p>
            </div>
            <div class=\"panel\" id=\"three-panel\">
              <div class=\"panel-title\">Versement en espéces</div>
              <p>{{payement.longdescvers|raw}}</p>
            </div>
          
              <div class=\"panel\" id=\"four-panel\">
              <div class=\"panel-title\">Mondat</div>
              <p>{{payement.longdescmond|raw}}</p>
            </div>
            </div>
            
          </div>
          <div class=\"btnlisthotel2 w-100\">
                    <button type=\"submit\"  id=\"btsubmit\" class=\"btn btn-primary\" ><i class=\"fa fa-check\"> </i> Réserver</button></div>  


            
            </div> 
        </div>
</form>
{% endblock %}", "default/reserver.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\reserver.html.twig");
    }
}
