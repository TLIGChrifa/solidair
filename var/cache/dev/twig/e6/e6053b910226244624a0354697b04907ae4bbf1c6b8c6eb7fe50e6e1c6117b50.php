<?php

/* default/index.html.twig */
class __TwigTemplate_7200de86507193d00cd4b5b16dc756c257d5b8c0a9c33545b76d4034ebcc6d37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fcd881919c237ce809a14387c5f4b9b64165e81bb100a313c691beb29d6797d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fcd881919c237ce809a14387c5f4b9b64165e81bb100a313c691beb29d6797d->enter($__internal_6fcd881919c237ce809a14387c5f4b9b64165e81bb100a313c691beb29d6797d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_cd37ffc7d9b54214fed468b65ac8aeb438027e370976383b724cdd7add0bc21c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd37ffc7d9b54214fed468b65ac8aeb438027e370976383b724cdd7add0bc21c->enter($__internal_cd37ffc7d9b54214fed468b65ac8aeb438027e370976383b724cdd7add0bc21c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6fcd881919c237ce809a14387c5f4b9b64165e81bb100a313c691beb29d6797d->leave($__internal_6fcd881919c237ce809a14387c5f4b9b64165e81bb100a313c691beb29d6797d_prof);

        
        $__internal_cd37ffc7d9b54214fed468b65ac8aeb438027e370976383b724cdd7add0bc21c->leave($__internal_cd37ffc7d9b54214fed468b65ac8aeb438027e370976383b724cdd7add0bc21c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_841151e1192af2d7357f9343bc20fa6918fc446659c59471c374f9aadb3b3272 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_841151e1192af2d7357f9343bc20fa6918fc446659c59471c374f9aadb3b3272->enter($__internal_841151e1192af2d7357f9343bc20fa6918fc446659c59471c374f9aadb3b3272_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bd02e27087d2e79be45da7ed0d2f693c1aafa1eb14ec799f65720d17e16e8d9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd02e27087d2e79be45da7ed0d2f693c1aafa1eb14ec799f65720d17e16e8d9c->enter($__internal_bd02e27087d2e79be45da7ed0d2f693c1aafa1eb14ec799f65720d17e16e8d9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "  <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/ListeEtoiles.js"), "html", null, true);
        echo "\"></script>
  <meta charset=\"utf-8\"> 
  <script>
   \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
  </script>

 <div class=\"slider\">
  <div id=\"slid\" class=\"carousel slide\" data-ride=\"carousel\">
  <ol class=\"carousel-indicators\">
      ";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["bannaire"] ?? $this->getContext($context, "bannaire")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["b"]) {
            // line 65
            echo "    <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"  ";
            if ($this->getAttribute($context["loop"], "first", array())) {
                echo " class=\"active\" ";
            }
            echo "></li>
      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "  </ol>
\t\t  <div class=\"carousel-inner\">
\t\t      ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["bannaire"] ?? $this->getContext($context, "bannaire")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["b"]) {
            // line 70
            echo "\t\t    <div class=\"carousel-item carosel-one ";
            if ($this->getAttribute($context["loop"], "first", array())) {
                echo " active ";
            }
            echo "\"  >
             <a href=\"";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["b"], "lien", array()), "html", null, true);
            echo "\"></a>    
       </div>
\t\t    
          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['b'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "\t\t
  
\t\t\t</div>
      <a class=\"carousel-control-prev\" href=\"#slid\" role=\"button\" data-slide=\"prev\">
        <i class=\"fas fa-chevron-left fa-2x\"></i>
      </a>
      <a class=\"carousel-control-next\" href=\"#slid\" role=\"button\" data-slide=\"next\">
        <i class=\"fas fa-chevron-right fa-2x\"></i>
      </a>
     
\t\t
  </div>
 </div>
 <form method=\"post\" action=\"\">
  <div class=\"filter\">
    <h2 style=\"color:#F4991C\" >Hôtels en tunisie</h2>
    <div class=\"row\">
      <div class=\"col-md-6 col-12\">
        <div class=\"f1 row\">
          <div class=\"col-4\" style=\"padding-left: 0rem;\">
            <span>Déstination</span>
          </div>
          <div class=\"col-8\" style=\"padding-right:0rem;\">
          <select class=\"custom-select form-control form-control-sm\" >
            ";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["villes"] ?? $this->getContext($context, "villes")));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 100
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "name", array()), "html", null, true);
            echo "</option>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "          </select>
          </div>
        </div>

      </div>
      <div class=\"col-md-6 col-12\">
        <div class=\"row\">
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem;\">
                <span>Chambres</span>
              </div>
              <div class=\"col-8\" style=\"padding-right:0rem; padding-left:0rem\">
              <select class=\"custom-select form-control form-control-sm\" >
                ";
        // line 116
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["room"] ?? $this->getContext($context, "room")));
        foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
            // line 117
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "capacity", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["r"], "name", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "              </select>
              </div>
            </div>
            

          </div>
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-5\" style=\"padding-left: 0rem;\">
                <span>Occuppants</span>
              </div>
              <div class=\"col-7\" style=\"padding-right:0rem;\">
              <select class=\"custom-select form-control form-control-sm\" >
                ";
        // line 132
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 133
            echo "                <option>";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 135
        echo "              </select>
              </div>
            </div>
            

          </div>
        </div>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-6 col-12\">
        <div class=\"row\">
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem;\">
                <span>Départ</span>
              </div>
              <div class=\"col-8\" style=\"     BACKGROUND-COLOR: #F3F3F3;padding-right:0rem;padding-left: 0rem;border-left: 1px solid #dad8d8;height: 1.9rem;\">
              <input type=\"date\" id=\"start\" name=\"trip-start\"
              value=\"2018-07-22\"
              min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">
              </div>
            </div>
            

          </div>
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem; \">
                <span>Arrivée</span>
              </div>
              <div class=\"col-8\" style=\"    BACKGROUND-COLOR: #F3F3F3; padding-right:0rem;padding-left: 0rem;border-left: 1px solid #dad8d8;height: 1.9rem;\">
                <input type=\"date\" id=\"start\" name=\"trip-start\"
                value=\"2018-07-22\"
                min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">
              </div>
            </div>
            

          </div>
        </div>
      </div>
      <div class=\"col-md-6 col-12\">
        <div class=\"f1 row\">
          <div class=\"col-4\" style=\"padding-left: 0rem;\">
            <span>Etoiles</span>
          </div>
          <div class=\"col-8\" style=\"padding-right:0rem; border-left: 1px solid #dad8d8;height: 1.9rem;    BACKGROUND-COLOR: #F3F3F3;\" >
            <input type=\"hidden\" name=\"star\" value=\"3\" data-clearable=\"\" id=\"some_id\">
            <div class=\"listeEtoile\" onmouseout=\"GestionHover('A1', -1, '5')\">
              <ul class=\"list-unstyled\" style=\"display: flex; margin-top: 0.5rem;margin-left: 5rem;\">
                <li><a href=\"javascript:ChoixSelection('A1', '1', '5')\" onmouseover=\"GestionHover('A1', '1', '5')\">
                  <img id=\"staroff-A1-1\" src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: none;\">
                  <img id=\"staron-A1-1\" src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: block;\"></a></li><li><a href=\"javascript:ChoixSelection('A1', '2', '5')\" onmouseover=\"GestionHover('A1', '2', '5')\">
              <img id=\"staroff-A1-2\" src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: none;\">
              <img id=\"staron-A1-2\" src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: block;\"></a>
            </li><li><a href=\"javascript:ChoixSelection('A1', '3', '5')\" onmouseover=\"GestionHover('A1', '3', '5')\">
              <img id=\"staroff-A1-3\" src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: none;\">
              <img id=\"staron-A1-3\" src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: block;\"></a></li>
              <li><a href=\"javascript:ChoixSelection('A1', '4', '5')\" onmouseover=\"GestionHover('A1', '4', '5')\">
                <img id=\"staroff-A1-4\" src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: block;\">
                <img id=\"staron-A1-4\" src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: none;\"></a></li><li>
                  <a href=\"javascript:ChoixSelection('A1', '5', '5')\" onmouseover=\"GestionHover('A1', '5', '5')\">
                    <img id=\"staroff-A1-5\" src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: block;\">
                    <img id=\"staron-A1-5\" src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: none;\"></a></li>\t
                  </ul></div>
          </div>
        </div>

      </div>
     <div class=\"col-md-12 col-12\"> <a class=\"btn\" href=\"\" style=\"height: 2rem; background-color: #F6AB43; color: #fff;float: right;margin-right: 0.8rem;\">Rechercher</a></div>
    </div>
   
  
  </div>
</form>

 <div class=\"section-hotel-en-tunisie container\">
  <h2>Nos Sélection d’Hôtels en Tunisie</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>

  

  <div id=\"HotelEnTunisie\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          ";
        // line 224
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["hotels"] ?? $this->getContext($context, "hotels")), 0, 4));
        foreach ($context['_seq'] as $context["key"] => $context["h"]) {
            // line 225
            echo "          <div class=\"col-md-3 col-12  col-xs-12\">
              <div class=\"tour-single\">
              <figure>
                ";
            // line 228
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["img"]);
            foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
                // line 229
                echo "                    ";
                if (($this->getAttribute($context["img"], "hotel", array()) == $context["h"])) {
                    // line 230
                    echo "                        <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["img"], "file", array())), "html", null, true);
                    echo "\" width=\"100%\" height=\"150\" class=\"img-responsive wp-post-image\" alt=\"...\">\t
                        <figcaption></figcaption> 
                    ";
                }
                // line 233
                echo "                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 234
            echo "                          \t    \t    \t            \t\t
              
              </figure>
              <div class=\"tour-details\">
                <h3><a href=\"\"> ";
            // line 238
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "name", array()), "html", null, true);
            echo "</a> </h3>
                <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                      ";
            // line 242
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($context["h"], "star", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 243
                echo "                      <i class=\"fa fa-star active\"></i> 
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 244
            echo " 
                    </li></ul>
                </div>
                <p style=\"font-size: 15px; color: #000; line-height: 24px; font-family: Poppins, sans-serif ;\">";
            // line 247
            echo twig_slice($this->env, $this->getAttribute($context["h"], "shortdesc", array()), 0, 75);
            echo "</p>
                <P style=\"margin-bottom: 0rem;\">";
            // line 248
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "ville", array()), "html", null, true);
            echo "</P>
              </div>
               
                <p class=\"tour-duration\" style=\"font-size: 15px; font-family: Poppins, sans-serif; color: #000; line-height: 24px;\">
                <small style=\"font-weight: bold;\">
                  ";
            // line 253
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 254
                echo "                    ";
                if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                    // line 255
                    echo "                      ";
                    $context["array"] = array();
                    // line 256
                    echo "                        À partir de  :
                        ";
                    // line 257
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["arr"]) {
                        // line 258
                        echo "                            ";
                        if (($this->getAttribute($context["arr"], "etat", array()) == 1)) {
                            // line 259
                            echo "                              ";
                            $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                            // line 260
                            echo "                                ";
                            $context["array"] = twig_array_merge(($context["array"] ?? $this->getContext($context, "array")), array(0 => ($context["totalprice"] ?? $this->getContext($context, "totalprice"))));
                            // line 261
                            echo "                            ";
                        }
                        // line 262
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['arr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 263
                    echo "                      ";
                    if ((twig_length_filter($this->env, ($context["array"] ?? $this->getContext($context, "array"))) > 0)) {
                        echo twig_escape_filter($this->env, min(($context["array"] ?? $this->getContext($context, "array"))), "html", null, true);
                        echo " TND ";
                    }
                    echo " 
                    ";
                }
                // line 265
                echo "                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 266
            echo "                </small> 
                </p>
              <div class=\"plus\">
                <a href=\"";
            // line 269
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailshotel", array("id" => $this->getAttribute($context["h"], "id", array()))), "html", null, true);
            echo "\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
              </div>
            </div>
            
            
           </div>
           ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 276
        echo "         
    
        </div>
      </div>
      ";
        // line 280
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hotels"] ?? $this->getContext($context, "hotels")));
        foreach ($context['_seq'] as $context["key"] => $context["h"]) {
            // line 281
            echo "      ";
            if (($context["key"] > 4)) {
                // line 282
                echo "      <div class=\"carousel-item \">
        <div class=\"row\">
          ";
                // line 284
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["hotels"] ?? $this->getContext($context, "hotels")), ($context["key"] - 1), 4));
                foreach ($context['_seq'] as $context["_key"] => $context["h"]) {
                    // line 285
                    echo "          <div class=\"col-md-3 col-12  col-xs-12\">
              <div class=\"tour-single\">
              <figure>
                ";
                    // line 288
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["img"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
                        // line 289
                        echo "                    ";
                        if (($this->getAttribute($context["img"], "hotel", array()) == $context["h"])) {
                            // line 290
                            echo "                        <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["img"], "file", array())), "html", null, true);
                            echo "\" width=\"100%\" height=\"150\" class=\"img-responsive wp-post-image\" alt=\"...\">\t
                        <figcaption></figcaption> 
                    ";
                        }
                        // line 293
                        echo "                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 294
                    echo "                          \t    \t    \t            \t\t
              
              </figure>
              <div class=\"tour-details\">
                <h3><a href=\"\"> ";
                    // line 298
                    echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "name", array()), "html", null, true);
                    echo "</a> </h3>
                <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                      ";
                    // line 302
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($context["h"], "star", array())));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 303
                        echo "                      <i class=\"fa fa-star active\"></i> 
                      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 304
                    echo " 
                    </li></ul>
                </div>
                <p style=\"font-size: 15px; color: #000; line-height: 24px; font-family: Poppins, sans-serif ;\">";
                    // line 307
                    echo twig_slice($this->env, $this->getAttribute($context["h"], "shortdesc", array()), 0, 75);
                    echo "</p>
                <P style=\"margin-bottom: 0rem;\">";
                    // line 308
                    echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "ville", array()), "html", null, true);
                    echo "</P>
              </div>
               
                <p class=\"tour-duration\" style=\"font-size: 15px; font-family: Poppins, sans-serif; color: #000; line-height: 24px;\">
                <small style=\"font-weight: bold;\">
                  ";
                    // line 313
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
                    foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                        // line 314
                        echo "                    ";
                        if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                            // line 315
                            echo "                      ";
                            $context["array"] = array();
                            // line 316
                            echo "                        À partir de  :
                        ";
                            // line 317
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                            foreach ($context['_seq'] as $context["key"] => $context["arr"]) {
                                // line 318
                                echo "                            ";
                                if (($this->getAttribute($context["arr"], "etat", array()) == 1)) {
                                    // line 319
                                    echo "                              ";
                                    $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                                    // line 320
                                    echo "                                ";
                                    $context["array"] = twig_array_merge(($context["array"] ?? $this->getContext($context, "array")), array(0 => ($context["totalprice"] ?? $this->getContext($context, "totalprice"))));
                                    // line 321
                                    echo "                            ";
                                }
                                // line 322
                                echo "                        ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['key'], $context['arr'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 323
                            echo "                      ";
                            if ((twig_length_filter($this->env, ($context["array"] ?? $this->getContext($context, "array"))) > 0)) {
                                echo twig_escape_filter($this->env, min(($context["array"] ?? $this->getContext($context, "array"))), "html", null, true);
                                echo " TND ";
                            }
                            echo " 
                    ";
                        }
                        // line 325
                        echo "                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 326
                    echo "                </small> 
                </p>
              <div class=\"plus\">
                <a href=\"";
                    // line 329
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailshotel", array("id" => $this->getAttribute($context["h"], "id", array()))), "html", null, true);
                    echo "\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
              </div>
            </div>
            
            
           </div>
           ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['h'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 336
                echo "         
    
        </div>
      </div>
      ";
            }
            // line 341
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 342
        echo "       
     
    </div>
    <a class=\"carousel-control-prev\" href=\"#HotelEnTunisie\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
    </a>
    <a class=\"carousel-control-next\" href=\"#HotelEnTunisie\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>

 </div>

 <div class=\"section-voyages-organises container\">
  <h2>Voyages Organisés</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
  <div id=\"VoyageOrganise\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          ";
        // line 364
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["vo"] ?? $this->getContext($context, "vo")), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 365
            echo "          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    ";
            // line 371
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "dureesj", array()), "html", null, true);
            echo " jours / ";
            echo twig_escape_filter($this->env, ($this->getAttribute($context["v"], "dureesj", array()) - 1), "html", null, true);
            echo " Nuits
                  </div> 
                  ";
            // line 373
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                echo " 
                      ";
                // line 374
                if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                    echo " 
                      <img width=\"100%\" height=\"180\"  src=\"/rustica/public_html/";
                    // line 375
                    echo twig_escape_filter($this->env, ("." . $this->getAttribute($context["item"], "image", array())), "html", null, true);
                    echo "\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      ";
                }
                // line 376
                echo " 
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 378
            echo "                    ";
            $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + $this->getAttribute($context["v"], "marge", array()));
            echo " 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      ";
            // line 380
            echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
            echo " TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"";
            // line 385
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
            echo "</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 393
        echo "         

        </div>
      </div>
      ";
        // line 397
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["vo"] ?? $this->getContext($context, "vo")));
        foreach ($context['_seq'] as $context["key"] => $context["v"]) {
            // line 398
            echo "      ";
            if (($context["key"] > 4)) {
                // line 399
                echo "      <div class=\"carousel-item \">
        <div class=\"row\">
          ";
                // line 401
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["vo"] ?? $this->getContext($context, "vo")), 0, 4));
                foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
                    // line 402
                    echo "          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    ";
                    // line 408
                    echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "dureesj", array()), "html", null, true);
                    echo " jours / ";
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["v"], "dureesj", array()) - 1), "html", null, true);
                    echo " Nuits
                  </div> 
                  ";
                    // line 410
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        echo " 
                      ";
                        // line 411
                        if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                            echo " 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      ";
                        }
                        // line 413
                        echo " 
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 415
                    echo "                    ";
                    $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + $this->getAttribute($context["v"], "marge", array()));
                    echo " 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      ";
                    // line 417
                    echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
                    echo " TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"";
                    // line 422
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
                    echo "</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 430
                echo "         

        </div>
      </div> 
      ";
            }
            // line 435
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " 
      
    </div>
    <a class=\"carousel-control-prev\" href=\"#VoyageOrganise\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
      
    </a>
    <a class=\"carousel-control-next\" href=\"#VoyageOrganise\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>
  <div id=\"VoyageOrganise2\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          ";
        // line 451
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["vo"] ?? $this->getContext($context, "vo")), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 452
            echo "          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    ";
            // line 458
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "dureesj", array()), "html", null, true);
            echo " jours / ";
            echo twig_escape_filter($this->env, ($this->getAttribute($context["v"], "dureesj", array()) - 1), "html", null, true);
            echo " Nuits
                  </div> 
                  ";
            // line 460
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                echo " 
                      ";
                // line 461
                if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                    echo " 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      ";
                }
                // line 463
                echo " 
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 465
            echo "                    ";
            $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + $this->getAttribute($context["v"], "marge", array()));
            echo " 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      ";
            // line 467
            echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
            echo " TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"";
            // line 472
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
            echo "</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 480
        echo "         

        </div>
      </div>
      ";
        // line 484
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["vo"] ?? $this->getContext($context, "vo")));
        foreach ($context['_seq'] as $context["key"] => $context["v"]) {
            // line 485
            echo "      ";
            if (($context["key"] > 4)) {
                // line 486
                echo "      <div class=\"carousel-item \">
        <div class=\"row\">
          ";
                // line 488
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["vo"] ?? $this->getContext($context, "vo")), 0, 4));
                foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
                    // line 489
                    echo "          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    ";
                    // line 495
                    echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "dureesj", array()), "html", null, true);
                    echo " jours / ";
                    echo twig_escape_filter($this->env, ($this->getAttribute($context["v"], "dureesj", array()) - 1), "html", null, true);
                    echo " Nuits
                  </div> 
                  ";
                    // line 497
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        echo " 
                      ";
                        // line 498
                        if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                            echo " 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      ";
                        }
                        // line 500
                        echo " 
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 502
                    echo "                    ";
                    $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + $this->getAttribute($context["v"], "marge", array()));
                    echo " 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      ";
                    // line 504
                    echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
                    echo " TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"";
                    // line 509
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
                    echo "</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 517
                echo "         

        </div>
      </div> 
      ";
            }
            // line 522
            echo "      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo " 
      
    </div>
    <a class=\"carousel-control-prev\" href=\"#VoyageOrganise2\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
      
    </a>
    <a class=\"carousel-control-next\" href=\"#VoyageOrganise2\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>
  


</div>

<div class=\"section-circuit-excursion container\">
  <h2>Circuit & Excursion</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
  <div class=\"row\" style=\"margin-top: 4rem;\">
    
    <div class=\"col-12 col-md-6\" >
      <div class=\"ombre\">

      </div>
      <a href=\"\">
        <img  id=\"photo1\" src=\"";
        // line 549
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/c1.png"), "html", null, true);
        echo "\" style=\"padding: 0.5rem; width: 100%; display:block;position: relative; z-index: 10;height: 31rem;\"/>
        <img id=\"photo2\"src=\"";
        // line 550
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/c5.jpg"), "html", null, true);
        echo "\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 20;height: 31rem;\"/>
        <img id=\"photo3\" src=\"";
        // line 551
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/c6.jpg"), "html", null, true);
        echo "\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 30;height: 31rem;\"/>
        <img id=\"photo4\" src=\"";
        // line 552
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/c7.jpg"), "html", null, true);
        echo "\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 30;height: 31rem;\"/>
     
      </a>   
      <div class=\"pays\">
               
        Italy
      </div>
   </div>

    <div class=\"col-12 col-md-6 row\" style=\"padding-right: 0px;\" >
      ";
        // line 562
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["circuit"] ?? $this->getContext($context, "circuit")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 563
            echo "      ";
            if (($this->getAttribute($context["loop"], "index", array()) <= 4)) {
                // line 564
                echo "      <div class=\"col-6 col-md-6 mid\" style=\"padding:0rem\">
          <div class=\"ombre-interne\" style=\"height: 15.4rem;\">
            <div class=\"cadre-image-interne\">
              <a href=\"\">
                ";
                // line 568
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["imgcuir"] ?? $this->getContext($context, "imgcuir")));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 569
                    echo "                    ";
                    if (($this->getAttribute($context["i"], "cuircuit", array()) == $context["c"])) {
                        // line 570
                        echo "                <img  classe=\"image\" src=\"/rustica/public_html/";
                        echo twig_escape_filter($this->env, ("." . $this->getAttribute($context["i"], "image", array())), "html", null, true);
                        echo "\" style=\"width: 100%;\"/>
                ";
                    }
                    // line 572
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 573
                echo "              </a>
              <div class=\"nbre-jour-interne\">
               
                ";
                // line 576
                echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "ville", array()), "html", null, true);
                echo "
              </div>
            </div>
            <div class=\"cadre-contenue-interne\"  onclick=\"myFunction()\">
              <div class=\"titre-interne\">
                <a href=\"\">
                  ITALIE
                </a>
              </div>
              <div class=\"sejour\">
                
                8 Jours 
                / 7 Nuits
               
              </div>
              
              
           
           
          </div>
         
        </div>
      </div>
      ";
            }
            // line 600
            echo "      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 601
        echo "     
    
      
    </div>
    
    
  
       
 

  </div>
</div>

<div class=\"section-maisons-dHotes container\">
  <h2>Maisons d'Hotes</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
 
        <div class=\"row\">
          ";
        // line 620
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["maison"] ?? $this->getContext($context, "maison")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["m"]) {
            // line 621
            echo "          ";
            if (($this->getAttribute($context["loop"], "index", array()) <= 3)) {
                // line 622
                echo "          <div class=\"col-md-4 col-sm-12  col-xs-12\">          
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-prix\" style=\"font-size:14px; \">
                    ";
                // line 627
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "prix", array()), "html", null, true);
                echo " TND
                </div>
                      ";
                // line 629
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["imgmaison"] ?? $this->getContext($context, "imgmaison")));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 630
                    echo "                      ";
                    if (($context["m"] == $this->getAttribute($context["i"], "maisondhote", array()))) {
                        // line 631
                        echo "                      <img   src=\"/rustica/public_html/";
                        echo twig_escape_filter($this->env, ("." . $this->getAttribute($context["i"], "file", array())), "html", null, true);
                        echo "\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      ";
                    }
                    // line 633
                    echo "                      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "         
                  </figure>
                <div class=\"tour-small-content \">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"\"> ";
                // line 637
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "ville", array()), "html", null, true);
                echo "</a></h5>
                                            <ul class=\"tour-small-price\">
                                              <li>Nombre des personnes: ";
                // line 639
                echo twig_escape_filter($this->env, ($this->getAttribute($context["m"], "nbchambres", array()) * 2), "html", null, true);
                echo "</li>
                                               <li> Nombre des chambres: ";
                // line 640
                echo twig_escape_filter($this->env, $this->getAttribute($context["m"], "nbchambres", array()), "html", null, true);
                echo "</li>
                                                <li>Nombre des lits: ";
                // line 641
                echo twig_escape_filter($this->env, ($this->getAttribute($context["m"], "nbchambres", array()) * 2), "html", null, true);
                echo " </li>
                                                <li>Nombres des salles de bain: 2</li>
                                            </ul>
                            </div>
                                        
                    </div>
                    <div class=\"plus\">
                      <a href=\"\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
                    </div>
            </div>
          </div>
          ";
            }
            // line 653
            echo "          ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['m'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 654
        echo "        </div>
</div> 

<script>
  ";
        // line 658
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hotels"] ?? $this->getContext($context, "hotels")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["h"]) {
            // line 659
            echo "  var div = document.getElementById('cr";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "');

  
var removeBtn = document.getElementById('remove-btn');
var promo = document.getElementById('promo');

removeBtn.addEventListener('click',()=>{
    div.classList.remove('promotion');
    div.classList.add('luxe');
})

promo.addEventListener('click',()=>{
    div.classList.remove('luxe');
  
    div.classList.add('promotion');
})
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 676
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["circuit"] ?? $this->getContext($context, "circuit")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 677
            echo "  ";
            if (($this->getAttribute($context["loop"], "index", array()) <= 4)) {
                // line 678
                echo "function circuitFunction";
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "() {

   var x = document.getElementById(\"photo1\");
   var y = document.getElementById(\"photo2\");
  var z = document.getElementById(\"photo3\");
  var k = document.getElementById(\"photo4\");
  ";
                // line 684
                if ($this->getAttribute($context["loop"], "first", array())) {
                    // line 685
                    echo "  if (x.style.display === \"none\") {
    x.style.display = \"block\";
    y.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
    x.style.display = \"block\";
    y.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";

  }
  ";
                } else {
                    // line 699
                    echo "  
    if (y.style.display === \"none\") {
        console.log(y);
    y.style.display = \"block\";
    x.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
      
    y.style.display = \"block\";
    x.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";


  }
  
    if (z.style.display === \"none\") {
    z.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
      
    z.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    k.style.display=\"none\";


  }
  
  
    if (k.style.display === \"none\") {
    k.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    z.style.display=\"none\";
  } 
  else  {
      
    k.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    z.style.display=\"none\";


  }
  
  ";
                }
                // line 751
                echo "}
 ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 754
        echo "</script>


";
        
        $__internal_bd02e27087d2e79be45da7ed0d2f693c1aafa1eb14ec799f65720d17e16e8d9c->leave($__internal_bd02e27087d2e79be45da7ed0d2f693c1aafa1eb14ec799f65720d17e16e8d9c_prof);

        
        $__internal_841151e1192af2d7357f9343bc20fa6918fc446659c59471c374f9aadb3b3272->leave($__internal_841151e1192af2d7357f9343bc20fa6918fc446659c59471c374f9aadb3b3272_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1537 => 754,  1521 => 751,  1467 => 699,  1451 => 685,  1449 => 684,  1439 => 678,  1436 => 677,  1418 => 676,  1386 => 659,  1369 => 658,  1363 => 654,  1349 => 653,  1334 => 641,  1330 => 640,  1326 => 639,  1321 => 637,  1310 => 633,  1304 => 631,  1301 => 630,  1297 => 629,  1292 => 627,  1285 => 622,  1282 => 621,  1265 => 620,  1244 => 601,  1230 => 600,  1203 => 576,  1198 => 573,  1192 => 572,  1186 => 570,  1183 => 569,  1179 => 568,  1173 => 564,  1170 => 563,  1153 => 562,  1140 => 552,  1136 => 551,  1132 => 550,  1128 => 549,  1094 => 522,  1087 => 517,  1071 => 509,  1063 => 504,  1057 => 502,  1050 => 500,  1044 => 498,  1038 => 497,  1031 => 495,  1023 => 489,  1019 => 488,  1015 => 486,  1012 => 485,  1008 => 484,  1002 => 480,  986 => 472,  978 => 467,  972 => 465,  965 => 463,  959 => 461,  953 => 460,  946 => 458,  938 => 452,  934 => 451,  911 => 435,  904 => 430,  888 => 422,  880 => 417,  874 => 415,  867 => 413,  861 => 411,  855 => 410,  848 => 408,  840 => 402,  836 => 401,  832 => 399,  829 => 398,  825 => 397,  819 => 393,  803 => 385,  795 => 380,  789 => 378,  782 => 376,  777 => 375,  773 => 374,  767 => 373,  760 => 371,  752 => 365,  748 => 364,  724 => 342,  718 => 341,  711 => 336,  698 => 329,  693 => 326,  687 => 325,  678 => 323,  672 => 322,  669 => 321,  666 => 320,  663 => 319,  660 => 318,  656 => 317,  653 => 316,  650 => 315,  647 => 314,  643 => 313,  635 => 308,  631 => 307,  626 => 304,  619 => 303,  615 => 302,  608 => 298,  602 => 294,  596 => 293,  589 => 290,  586 => 289,  582 => 288,  577 => 285,  573 => 284,  569 => 282,  566 => 281,  562 => 280,  556 => 276,  543 => 269,  538 => 266,  532 => 265,  523 => 263,  517 => 262,  514 => 261,  511 => 260,  508 => 259,  505 => 258,  501 => 257,  498 => 256,  495 => 255,  492 => 254,  488 => 253,  480 => 248,  476 => 247,  471 => 244,  464 => 243,  460 => 242,  453 => 238,  447 => 234,  441 => 233,  434 => 230,  431 => 229,  427 => 228,  422 => 225,  418 => 224,  390 => 199,  386 => 198,  381 => 196,  377 => 195,  372 => 193,  368 => 192,  363 => 190,  359 => 189,  355 => 188,  351 => 187,  297 => 135,  288 => 133,  284 => 132,  269 => 119,  258 => 117,  254 => 116,  238 => 102,  227 => 100,  223 => 99,  197 => 75,  179 => 71,  172 => 70,  155 => 69,  151 => 67,  130 => 65,  113 => 64,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
  <script src=\"{{asset('assets/js/ListeEtoiles.js')}}\"></script>
  <meta charset=\"utf-8\"> 
  <script>
   \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
  </script>

 <div class=\"slider\">
  <div id=\"slid\" class=\"carousel slide\" data-ride=\"carousel\">
  <ol class=\"carousel-indicators\">
      {% for key,b in bannaire %}
    <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"{{key}}\"  {% if loop.first %} class=\"active\" {%endif%}></li>
      {% endfor %}
  </ol>
\t\t  <div class=\"carousel-inner\">
\t\t      {% for b in bannaire %}
\t\t    <div class=\"carousel-item carosel-one {% if loop.first %} active {%endif%}\"  >
             <a href=\"{{b.lien}}\"></a>    
       </div>
\t\t    
          {% endfor %}
\t\t
  
\t\t\t</div>
      <a class=\"carousel-control-prev\" href=\"#slid\" role=\"button\" data-slide=\"prev\">
        <i class=\"fas fa-chevron-left fa-2x\"></i>
      </a>
      <a class=\"carousel-control-next\" href=\"#slid\" role=\"button\" data-slide=\"next\">
        <i class=\"fas fa-chevron-right fa-2x\"></i>
      </a>
     
\t\t
  </div>
 </div>
 <form method=\"post\" action=\"\">
  <div class=\"filter\">
    <h2 style=\"color:#F4991C\" >Hôtels en tunisie</h2>
    <div class=\"row\">
      <div class=\"col-md-6 col-12\">
        <div class=\"f1 row\">
          <div class=\"col-4\" style=\"padding-left: 0rem;\">
            <span>Déstination</span>
          </div>
          <div class=\"col-8\" style=\"padding-right:0rem;\">
          <select class=\"custom-select form-control form-control-sm\" >
            {%for v in villes %}
            <option value=\"{{v.id}}\">{{v.name}}</option>
            {%endfor %}
          </select>
          </div>
        </div>

      </div>
      <div class=\"col-md-6 col-12\">
        <div class=\"row\">
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem;\">
                <span>Chambres</span>
              </div>
              <div class=\"col-8\" style=\"padding-right:0rem; padding-left:0rem\">
              <select class=\"custom-select form-control form-control-sm\" >
                {% for r in room %}
                <option value=\"{{r.capacity}}\">{{r.name}}</option>
                {%endfor %}
              </select>
              </div>
            </div>
            

          </div>
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-5\" style=\"padding-left: 0rem;\">
                <span>Occuppants</span>
              </div>
              <div class=\"col-7\" style=\"padding-right:0rem;\">
              <select class=\"custom-select form-control form-control-sm\" >
                {%for i in 1..15 %}
                <option>{{i}}</option>
                {%endfor%}
              </select>
              </div>
            </div>
            

          </div>
        </div>
      </div>
    </div>
    <div class=\"row\">
      <div class=\"col-md-6 col-12\">
        <div class=\"row\">
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem;\">
                <span>Départ</span>
              </div>
              <div class=\"col-8\" style=\"     BACKGROUND-COLOR: #F3F3F3;padding-right:0rem;padding-left: 0rem;border-left: 1px solid #dad8d8;height: 1.9rem;\">
              <input type=\"date\" id=\"start\" name=\"trip-start\"
              value=\"2018-07-22\"
              min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">
              </div>
            </div>
            

          </div>
          <div class=\"col-md-6 col-12\">
            <div class=\"f2 row\">
              <div class=\"col-4\" style=\"padding-left: 0rem; \">
                <span>Arrivée</span>
              </div>
              <div class=\"col-8\" style=\"    BACKGROUND-COLOR: #F3F3F3; padding-right:0rem;padding-left: 0rem;border-left: 1px solid #dad8d8;height: 1.9rem;\">
                <input type=\"date\" id=\"start\" name=\"trip-start\"
                value=\"2018-07-22\"
                min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">
              </div>
            </div>
            

          </div>
        </div>
      </div>
      <div class=\"col-md-6 col-12\">
        <div class=\"f1 row\">
          <div class=\"col-4\" style=\"padding-left: 0rem;\">
            <span>Etoiles</span>
          </div>
          <div class=\"col-8\" style=\"padding-right:0rem; border-left: 1px solid #dad8d8;height: 1.9rem;    BACKGROUND-COLOR: #F3F3F3;\" >
            <input type=\"hidden\" name=\"star\" value=\"3\" data-clearable=\"\" id=\"some_id\">
            <div class=\"listeEtoile\" onmouseout=\"GestionHover('A1', -1, '5')\">
              <ul class=\"list-unstyled\" style=\"display: flex; margin-top: 0.5rem;margin-left: 5rem;\">
                <li><a href=\"javascript:ChoixSelection('A1', '1', '5')\" onmouseover=\"GestionHover('A1', '1', '5')\">
                  <img id=\"staroff-A1-1\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: none;\">
                  <img id=\"staron-A1-1\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: block;\"></a></li><li><a href=\"javascript:ChoixSelection('A1', '2', '5')\" onmouseover=\"GestionHover('A1', '2', '5')\">
              <img id=\"staroff-A1-2\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: none;\">
              <img id=\"staron-A1-2\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: block;\"></a>
            </li><li><a href=\"javascript:ChoixSelection('A1', '3', '5')\" onmouseover=\"GestionHover('A1', '3', '5')\">
              <img id=\"staroff-A1-3\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: none;\">
              <img id=\"staron-A1-3\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: block;\"></a></li>
              <li><a href=\"javascript:ChoixSelection('A1', '4', '5')\" onmouseover=\"GestionHover('A1', '4', '5')\">
                <img id=\"staroff-A1-4\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: block;\">
                <img id=\"staron-A1-4\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: none;\"></a></li><li>
                  <a href=\"javascript:ChoixSelection('A1', '5', '5')\" onmouseover=\"GestionHover('A1', '5', '5')\">
                    <img id=\"staroff-A1-5\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: block;\">
                    <img id=\"staron-A1-5\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: none;\"></a></li>\t
                  </ul></div>
          </div>
        </div>

      </div>
     <div class=\"col-md-12 col-12\"> <a class=\"btn\" href=\"\" style=\"height: 2rem; background-color: #F6AB43; color: #fff;float: right;margin-right: 0.8rem;\">Rechercher</a></div>
    </div>
   
  
  </div>
</form>

 <div class=\"section-hotel-en-tunisie container\">
  <h2>Nos Sélection d’Hôtels en Tunisie</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>

  

  <div id=\"HotelEnTunisie\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          {% for key,h in hotels|slice(0, 4) %}
          <div class=\"col-md-3 col-12  col-xs-12\">
              <div class=\"tour-single\">
              <figure>
                {% for img in img %}
                    {% if img.hotel == h  %}
                        <img src=\"{{ asset( img.file) }}\" width=\"100%\" height=\"150\" class=\"img-responsive wp-post-image\" alt=\"...\">\t
                        <figcaption></figcaption> 
                    {% endif %}
                  {% endfor %}
                          \t    \t    \t            \t\t
              
              </figure>
              <div class=\"tour-details\">
                <h3><a href=\"\"> {{h.name}}</a> </h3>
                <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                      {% for i in 1..h.star %}
                      <i class=\"fa fa-star active\"></i> 
                      {% endfor %} 
                    </li></ul>
                </div>
                <p style=\"font-size: 15px; color: #000; line-height: 24px; font-family: Poppins, sans-serif ;\">{{h.shortdesc|slice(0,75)|raw}}</p>
                <P style=\"margin-bottom: 0rem;\">{{h.ville}}</P>
              </div>
               
                <p class=\"tour-duration\" style=\"font-size: 15px; font-family: Poppins, sans-serif; color: #000; line-height: 24px;\">
                <small style=\"font-weight: bold;\">
                  {% for c in contrats %}
                    {% if c.hotel == h %}
                      {% set array = {}%}
                        À partir de  :
                        {% for key,arr in c.pricearr%}
                            {% if arr.etat == 1%}
                              {% set totalprice = arr.price %}
                                {% set array = array | merge([totalprice]) %}
                            {% endif %}
                        {% endfor %}
                      {%if array|length>0 %}{{min(array)}} TND {%endif%} 
                    {%endif%}
                  {% endfor %}
                </small> 
                </p>
              <div class=\"plus\">
                <a href=\"{{ path('detailshotel', {'id': h.id}) }}\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
              </div>
            </div>
            
            
           </div>
           {% endfor %}
         
    
        </div>
      </div>
      {% for key,h in hotels %}
      {% if key > 4 %}
      <div class=\"carousel-item \">
        <div class=\"row\">
          {% for h in hotels|slice(key-1, 4) %}
          <div class=\"col-md-3 col-12  col-xs-12\">
              <div class=\"tour-single\">
              <figure>
                {% for img in img %}
                    {% if img.hotel == h  %}
                        <img src=\"{{ asset( img.file) }}\" width=\"100%\" height=\"150\" class=\"img-responsive wp-post-image\" alt=\"...\">\t
                        <figcaption></figcaption> 
                    {% endif %}
                  {% endfor %}
                          \t    \t    \t            \t\t
              
              </figure>
              <div class=\"tour-details\">
                <h3><a href=\"\"> {{h.name}}</a> </h3>
                <div class=\"tour-rating\">
                  <ul class=\"list-unstyled clearfix\">
                    <li>
                      {% for i in 1..h.star %}
                      <i class=\"fa fa-star active\"></i> 
                      {% endfor %} 
                    </li></ul>
                </div>
                <p style=\"font-size: 15px; color: #000; line-height: 24px; font-family: Poppins, sans-serif ;\">{{h.shortdesc|slice(0,75)|raw}}</p>
                <P style=\"margin-bottom: 0rem;\">{{h.ville}}</P>
              </div>
               
                <p class=\"tour-duration\" style=\"font-size: 15px; font-family: Poppins, sans-serif; color: #000; line-height: 24px;\">
                <small style=\"font-weight: bold;\">
                  {% for c in contrats %}
                    {% if c.hotel == h %}
                      {% set array = {}%}
                        À partir de  :
                        {% for key,arr in c.pricearr%}
                            {% if arr.etat == 1%}
                              {% set totalprice = arr.price %}
                                {% set array = array | merge([totalprice]) %}
                            {% endif %}
                        {% endfor %}
                      {%if array|length>0 %}{{min(array)}} TND {%endif%} 
                    {%endif%}
                  {% endfor %}
                </small> 
                </p>
              <div class=\"plus\">
                <a href=\"{{ path('detailshotel', {'id': h.id}) }}\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
              </div>
            </div>
            
            
           </div>
           {% endfor %}
         
    
        </div>
      </div>
      {% endif %}
      {% endfor %}
       
     
    </div>
    <a class=\"carousel-control-prev\" href=\"#HotelEnTunisie\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
    </a>
    <a class=\"carousel-control-next\" href=\"#HotelEnTunisie\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>

 </div>

 <div class=\"section-voyages-organises container\">
  <h2>Voyages Organisés</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
  <div id=\"VoyageOrganise\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          {% for v in vo|slice(0, 4) %}
          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    {{v.dureesj}} jours / {{v.dureesj-1}} Nuits
                  </div> 
                  {% for item in sejourimg %} 
                      {% if item.sejour == v  %} 
                      <img width=\"100%\" height=\"180\"  src=\"/rustica/public_html/{{ '.'~item.image}}\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      {% endif %} 
                  {% endfor %}
                    {% set total = v.prix +  v.marge %} 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      {{total}} TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"{{ path('detailsvo', {'id': v.id}) }}\">{{v.titre}}</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          {% endfor %}
         

        </div>
      </div>
      {% for key,v in vo %}
      {% if key > 4 %}
      <div class=\"carousel-item \">
        <div class=\"row\">
          {% for v in vo|slice(0, 4) %}
          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    {{v.dureesj}} jours / {{v.dureesj-1}} Nuits
                  </div> 
                  {% for item in sejourimg %} 
                      {% if item.sejour == v  %} 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      {% endif %} 
                  {% endfor %}
                    {% set total = v.prix +  v.marge %} 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      {{total}} TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"{{ path('detailsvo', {'id': v.id}) }}\">{{v.titre}}</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          {% endfor %}
         

        </div>
      </div> 
      {% endif %}
      {% endfor %} 
      
    </div>
    <a class=\"carousel-control-prev\" href=\"#VoyageOrganise\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
      
    </a>
    <a class=\"carousel-control-next\" href=\"#VoyageOrganise\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>
  <div id=\"VoyageOrganise2\" class=\"carousel slide\" data-ride=\"carousel\">
    <div class=\"carousel-inner\">
    
      <div class=\"carousel-item active\">
        <div class=\"row\">
          {% for v in vo|slice(0, 4) %}
          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    {{v.dureesj}} jours / {{v.dureesj-1}} Nuits
                  </div> 
                  {% for item in sejourimg %} 
                      {% if item.sejour == v  %} 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      {% endif %} 
                  {% endfor %}
                    {% set total = v.prix +  v.marge %} 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      {{total}} TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"{{ path('detailsvo', {'id': v.id}) }}\">{{v.titre}}</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          {% endfor %}
         

        </div>
      </div>
      {% for key,v in vo %}
      {% if key > 4 %}
      <div class=\"carousel-item \">
        <div class=\"row\">
          {% for v in vo|slice(0, 4) %}
          <div class=\"col-md-3 col-sm-6  col-xs-12\">
                      
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-duree\" style=\"font-size:14px; \">
                    {{v.dureesj}} jours / {{v.dureesj-1}} Nuits
                  </div> 
                  {% for item in sejourimg %} 
                      {% if item.sejour == v  %} 
                      <img width=\"100%\" height=\"180\"  src=\"image/Dubaï.png\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      {% endif %} 
                  {% endfor %}
                    {% set total = v.prix +  v.marge %} 
                    <div class=\"tour-prix\" style=\"font-size:14px; \">
                      {{total}} TND
                    </div> \t\t  
                  </figure>
                <div class=\"tour-small-content ptb-10 prl-20\">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"{{ path('detailsvo', {'id': v.id}) }}\">{{v.titre}}</a></h5>
                                            <h5 class=\"tour-small-price\">\$ 50</h5>
                            </div>
                                        
                        </div>
            </div>
          </div>
          {% endfor %}
         

        </div>
      </div> 
      {% endif %}
      {% endfor %} 
      
    </div>
    <a class=\"carousel-control-prev\" href=\"#VoyageOrganise2\" role=\"button\" data-slide=\"prev\">
      <i class=\"fas fa-chevron-left fa-3x\"></i>
      
    </a>
    <a class=\"carousel-control-next\" href=\"#VoyageOrganise2\" role=\"button\" data-slide=\"next\">
      <i class=\"fas fa-chevron-right fa-3x\"></i>
    </a>
  </div>
  


</div>

<div class=\"section-circuit-excursion container\">
  <h2>Circuit & Excursion</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
  <div class=\"row\" style=\"margin-top: 4rem;\">
    
    <div class=\"col-12 col-md-6\" >
      <div class=\"ombre\">

      </div>
      <a href=\"\">
        <img  id=\"photo1\" src=\"{{asset('assets/image/c1.png')}}\" style=\"padding: 0.5rem; width: 100%; display:block;position: relative; z-index: 10;height: 31rem;\"/>
        <img id=\"photo2\"src=\"{{asset('assets/image/c5.jpg')}}\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 20;height: 31rem;\"/>
        <img id=\"photo3\" src=\"{{asset('assets/image/c6.jpg')}}\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 30;height: 31rem;\"/>
        <img id=\"photo4\" src=\"{{asset('assets/image/c7.jpg')}}\" style=\"padding: 0.5rem; width: 100%; display:none;position: relative; z-index: 30;height: 31rem;\"/>
     
      </a>   
      <div class=\"pays\">
               
        Italy
      </div>
   </div>

    <div class=\"col-12 col-md-6 row\" style=\"padding-right: 0px;\" >
      {% for c in circuit %}
      {% if loop.index <=4 %}
      <div class=\"col-6 col-md-6 mid\" style=\"padding:0rem\">
          <div class=\"ombre-interne\" style=\"height: 15.4rem;\">
            <div class=\"cadre-image-interne\">
              <a href=\"\">
                {% for i in imgcuir %}
                    {% if i.cuircuit == c  %}
                <img  classe=\"image\" src=\"/rustica/public_html/{{('.'~i.image) }}\" style=\"width: 100%;\"/>
                {% endif %}
                {% endfor %}
              </a>
              <div class=\"nbre-jour-interne\">
               
                {{c.ville}}
              </div>
            </div>
            <div class=\"cadre-contenue-interne\"  onclick=\"myFunction()\">
              <div class=\"titre-interne\">
                <a href=\"\">
                  ITALIE
                </a>
              </div>
              <div class=\"sejour\">
                
                8 Jours 
                / 7 Nuits
               
              </div>
              
              
           
           
          </div>
         
        </div>
      </div>
      {% endif %}
      {% endfor %}
     
    
      
    </div>
    
    
  
       
 

  </div>
</div>

<div class=\"section-maisons-dHotes container\">
  <h2>Maisons d'Hotes</h2>
  <div class=\"trait\"></div>
  <div class=\"trait2\"></div>
 
        <div class=\"row\">
          {% for m in maison %}
          {% if loop.index <=3%}
          <div class=\"col-md-4 col-sm-12  col-xs-12\">          
            <div class=\"tour-small\">
              
                <figure class=\"tour-small-image\">
                  <div class=\"tour-prix\" style=\"font-size:14px; \">
                    {{m.prix}} TND
                </div>
                      {% for i in imgmaison %}
                      {% if  m == i.maisondhote   %}
                      <img   src=\"/rustica/public_html/{{('.'~i.file) }}\" class=\"attachment-large size-large wp-post-image w-100\" alt=\"\">
                      {% endif %}
                      {% endfor %}         
                  </figure>
                <div class=\"tour-small-content \">
                    <div class=\"tour-small-title\">
                        <h5 class=\"tour-small-head \"><a href=\"\"> {{m.ville}}</a></h5>
                                            <ul class=\"tour-small-price\">
                                              <li>Nombre des personnes: {{m.nbchambres*2 }}</li>
                                               <li> Nombre des chambres: {{m.nbchambres }}</li>
                                                <li>Nombre des lits: {{m.nbchambres*2 }} </li>
                                                <li>Nombres des salles de bain: 2</li>
                                            </ul>
                            </div>
                                        
                    </div>
                    <div class=\"plus\">
                      <a href=\"\" style=\"font-family: Poppins, sans-serif !important;\">Plus</a>
                    </div>
            </div>
          </div>
          {% endif %}
          {% endfor %}
        </div>
</div> 

<script>
  {% for h in hotels %}
  var div = document.getElementById('cr{{loop.index}}');

  
var removeBtn = document.getElementById('remove-btn');
var promo = document.getElementById('promo');

removeBtn.addEventListener('click',()=>{
    div.classList.remove('promotion');
    div.classList.add('luxe');
})

promo.addEventListener('click',()=>{
    div.classList.remove('luxe');
  
    div.classList.add('promotion');
})
{%endfor %}
  {% for c in circuit %}
  {%if loop.index <= 4 %}
function circuitFunction{{loop.index}}() {

   var x = document.getElementById(\"photo1\");
   var y = document.getElementById(\"photo2\");
  var z = document.getElementById(\"photo3\");
  var k = document.getElementById(\"photo4\");
  {% if loop.first %}
  if (x.style.display === \"none\") {
    x.style.display = \"block\";
    y.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
    x.style.display = \"block\";
    y.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";

  }
  {%else%}
  
    if (y.style.display === \"none\") {
        console.log(y);
    y.style.display = \"block\";
    x.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
      
    y.style.display = \"block\";
    x.style.display=\"none\";
    z.style.display=\"none\";
    k.style.display=\"none\";


  }
  
    if (z.style.display === \"none\") {
    z.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    k.style.display=\"none\";
  } 
  else  {
      
    z.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    k.style.display=\"none\";


  }
  
  
    if (k.style.display === \"none\") {
    k.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    z.style.display=\"none\";
  } 
  else  {
      
    k.style.display = \"block\";
    x.style.display=\"none\";
    y.style.display=\"none\";
    z.style.display=\"none\";


  }
  
  {%endif%}
}
 {%endif%}
{%endfor %}
</script>


{% endblock %}

", "default/index.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\index.html.twig");
    }
}
