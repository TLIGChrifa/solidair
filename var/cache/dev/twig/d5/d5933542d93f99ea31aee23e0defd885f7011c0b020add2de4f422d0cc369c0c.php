<?php

/* default/contact.html.twig */
class __TwigTemplate_581cf343b46284f02f40de87319650c17a5f155df4c90298fc83c401ea5bef47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/contact.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25ff72d6346c9d8b5d44fcdd2a183a840df9029e2c33eabe0194a0470ec2b2d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25ff72d6346c9d8b5d44fcdd2a183a840df9029e2c33eabe0194a0470ec2b2d2->enter($__internal_25ff72d6346c9d8b5d44fcdd2a183a840df9029e2c33eabe0194a0470ec2b2d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/contact.html.twig"));

        $__internal_c9419b5fa80867a732ab9bc00b880187086832da3e924abcc89e3b5301a41551 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9419b5fa80867a732ab9bc00b880187086832da3e924abcc89e3b5301a41551->enter($__internal_c9419b5fa80867a732ab9bc00b880187086832da3e924abcc89e3b5301a41551_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25ff72d6346c9d8b5d44fcdd2a183a840df9029e2c33eabe0194a0470ec2b2d2->leave($__internal_25ff72d6346c9d8b5d44fcdd2a183a840df9029e2c33eabe0194a0470ec2b2d2_prof);

        
        $__internal_c9419b5fa80867a732ab9bc00b880187086832da3e924abcc89e3b5301a41551->leave($__internal_c9419b5fa80867a732ab9bc00b880187086832da3e924abcc89e3b5301a41551_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_928fe86ade3819a5d26a98b40b4c8c97b2e7d2b46c4ee5f9bc3e2a049d4b3fba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_928fe86ade3819a5d26a98b40b4c8c97b2e7d2b46c4ee5f9bc3e2a049d4b3fba->enter($__internal_928fe86ade3819a5d26a98b40b4c8c97b2e7d2b46c4ee5f9bc3e2a049d4b3fba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d60dee6349eb0fdd68c285f0934ab54ce6dde8cfe285ba72772419f92a2eb2c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d60dee6349eb0fdd68c285f0934ab54ce6dde8cfe285ba72772419f92a2eb2c8->enter($__internal_d60dee6349eb0fdd68c285f0934ab54ce6dde8cfe285ba72772419f92a2eb2c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

<div class=\"slider\"> 
    <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
  <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
          <div class=\"carousel-inner\">
            
            
            <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
          
            </div>
    
            <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
          
           </div>
              
            <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
    
            </div>
           
      
          </div>
          <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
            <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Previous</span>
          </a>
          <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
            <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Next</span>
          </a>
    
  </div>
  </div>


<div class=\"contact container\" >
    ";
        // line 40
        $context["i"] = 0;
        // line 41
        echo "   ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 42
            echo "         ";
            $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
            // line 43
            echo "         ";
            if ((($context["i"] ?? $this->getContext($context, "i")) == 1)) {
                // line 44
                echo "             <div class=\"alert alert-success\" style=\"width: 441px; color:#fff;\"><center> <i class=\"fa fa-check\"></i> ";
                echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
                echo "</center></div><br/>
        ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        
        <div class=\"\">
            <div class=\"detailsBox\"><h5>Contactez-nous</h5></div>
            <div class=\"row\" >
                <div class=\"col-12 col-md-6 \">
                    <h5 style=\"margin-top: 1rem;
                    text-align: center;
                    color: #f3991c;
                    font-weight: bold;\">Information sur Solid'Air</h5>
                    
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f1.png\" style=\"width:30px\"></div>
                        <div class=\"col-11\">
                          <span style=\"font-weight: bold; color: #1e4b22 ;\"> Contact :</span>  </br>
                          Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr
                        </div>
    
                    </div>
                    <hr style=\"    width: 50%;background: #f3991c;\"/>
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f2.png\" style=\"width:25px\"></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\">  Tél :</span></br>
                            (+216) 72 222 887 </br>
                          
                        </div>
    
                    </div>
                    <hr style=\"    width: 50%;background: #f3991c;\"/>
    
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f3.png\" style=\"width:25px\"></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\"> E-mail :</span> </br>
                            reservation@solidair.tn</br>
                          
                        </div>
    
                    </div>
                   <hr  style=\"    width: 50%;background: #f3991c;\"/>
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"><i class=\"far fa-clock\" style=\"background: #f3991c;
                            color: #fff;
                            font-size: 20px;
                            margin-top: 0.5rem;
                            border-radius: 50%;
                            padding: 3px;\"></i></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\">Horaires :</span> </br>
                           Lundi au vendredi: 9h00 jusqu'à 17h30 </br>
                            Samedi: 9h00 jusqu'à 13h00</br>
                          
                        </div>
    
                    </div>
                 
    
                </div>
               

                <div class=\"col-12 col-md-6 \">
                 <form name=\"contact\" method=\"post\" Action=\"\" >
                    <h5 style=\"margin-top: 1rem;
                    text-align: center;
                    color: #f3991c;
                    font-weight: bold;\">Contactez-nous</h5>
                    <br/>
                   <div class=\"row\">
                       <div class=\"col-6 col-md-6\">
                        <label  >Nom & Prénom * :</label>
                        <input type=\"text\" class=\"form-control\" name=\"nom\">

                       </div>
                       <div class=\"col-6 col-md-6\">
                        <label  >Tel* :</label>
                        <input type=\"text\" class=\"form-control\" name=\"tel\" required>

                       </div>

                   </div> 
                   <div class=\"row\">
                    <div class=\"col-6 col-md-6\">
                     <label  >E-mail* :</label>
                     <input type=\"text\" class=\"form-control\" name=\"email\" required>

                    </div>
                    <div class=\"col-6 col-md-6\">
                     <label  >Sujet * :</label>
                     <input type=\"text\" class=\"form-control\" name=\"sujet\" required>

                    </div>
                    <div class=\"col-12 col-md-12 \">
                        <span >Message</span>
                        <textarea class=\"form-control\" name=\"message\" required style=\"height:3.5rem !important ;\"></textarea>
                        <br/>
                        <button type=\"submit\" class=\"button btn\" >
                          <i class=\"fa fa-envelope\"></i>  Envoyer</button>
                    </div>
   </form> 
                </div>
             
                </div>
               
               
            </div>
        </div>
       

    </div>
    ";
        
        $__internal_d60dee6349eb0fdd68c285f0934ab54ce6dde8cfe285ba72772419f92a2eb2c8->leave($__internal_d60dee6349eb0fdd68c285f0934ab54ce6dde8cfe285ba72772419f92a2eb2c8_prof);

        
        $__internal_928fe86ade3819a5d26a98b40b4c8c97b2e7d2b46c4ee5f9bc3e2a049d4b3fba->leave($__internal_928fe86ade3819a5d26a98b40b4c8c97b2e7d2b46c4ee5f9bc3e2a049d4b3fba_prof);

    }

    public function getTemplateName()
    {
        return "default/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 46,  100 => 44,  97 => 43,  94 => 42,  89 => 41,  87 => 40,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}


<div class=\"slider\"> 
    <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
  <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
          <div class=\"carousel-inner\">
            
            
            <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
          
            </div>
    
            <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
          
           </div>
              
            <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
    
            </div>
           
      
          </div>
          <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
            <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Previous</span>
          </a>
          <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
            <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Next</span>
          </a>
    
  </div>
  </div>


<div class=\"contact container\" >
    {%set i = 0%}
   {% for flashMessage in app.session.flashbag.get('success') %}
         {%set i = i+1%}
         {% if i==1 %}
             <div class=\"alert alert-success\" style=\"width: 441px; color:#fff;\"><center> <i class=\"fa fa-check\"></i> {{ flashMessage }}</center></div><br/>
        {% endif %}{%endfor%}
        
        <div class=\"\">
            <div class=\"detailsBox\"><h5>Contactez-nous</h5></div>
            <div class=\"row\" >
                <div class=\"col-12 col-md-6 \">
                    <h5 style=\"margin-top: 1rem;
                    text-align: center;
                    color: #f3991c;
                    font-weight: bold;\">Information sur Solid'Air</h5>
                    
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f1.png\" style=\"width:30px\"></div>
                        <div class=\"col-11\">
                          <span style=\"font-weight: bold; color: #1e4b22 ;\"> Contact :</span>  </br>
                          Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr
                        </div>
    
                    </div>
                    <hr style=\"    width: 50%;background: #f3991c;\"/>
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f2.png\" style=\"width:25px\"></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\">  Tél :</span></br>
                            (+216) 72 222 887 </br>
                          
                        </div>
    
                    </div>
                    <hr style=\"    width: 50%;background: #f3991c;\"/>
    
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"> <img src=\"/solidair/web/assets/image/f3.png\" style=\"width:25px\"></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\"> E-mail :</span> </br>
                            reservation@solidair.tn</br>
                          
                        </div>
    
                    </div>
                   <hr  style=\"    width: 50%;background: #f3991c;\"/>
                    <div class=\"row\">
                        <div class=\"col-1 col-md-1\"><i class=\"far fa-clock\" style=\"background: #f3991c;
                            color: #fff;
                            font-size: 20px;
                            margin-top: 0.5rem;
                            border-radius: 50%;
                            padding: 3px;\"></i></div>
                        <div class=\"col-11\">
                            <span style=\"font-weight: bold; color: #1e4b22 ;\">Horaires :</span> </br>
                           Lundi au vendredi: 9h00 jusqu'à 17h30 </br>
                            Samedi: 9h00 jusqu'à 13h00</br>
                          
                        </div>
    
                    </div>
                 
    
                </div>
               

                <div class=\"col-12 col-md-6 \">
                 <form name=\"contact\" method=\"post\" Action=\"\" >
                    <h5 style=\"margin-top: 1rem;
                    text-align: center;
                    color: #f3991c;
                    font-weight: bold;\">Contactez-nous</h5>
                    <br/>
                   <div class=\"row\">
                       <div class=\"col-6 col-md-6\">
                        <label  >Nom & Prénom * :</label>
                        <input type=\"text\" class=\"form-control\" name=\"nom\">

                       </div>
                       <div class=\"col-6 col-md-6\">
                        <label  >Tel* :</label>
                        <input type=\"text\" class=\"form-control\" name=\"tel\" required>

                       </div>

                   </div> 
                   <div class=\"row\">
                    <div class=\"col-6 col-md-6\">
                     <label  >E-mail* :</label>
                     <input type=\"text\" class=\"form-control\" name=\"email\" required>

                    </div>
                    <div class=\"col-6 col-md-6\">
                     <label  >Sujet * :</label>
                     <input type=\"text\" class=\"form-control\" name=\"sujet\" required>

                    </div>
                    <div class=\"col-12 col-md-12 \">
                        <span >Message</span>
                        <textarea class=\"form-control\" name=\"message\" required style=\"height:3.5rem !important ;\"></textarea>
                        <br/>
                        <button type=\"submit\" class=\"button btn\" >
                          <i class=\"fa fa-envelope\"></i>  Envoyer</button>
                    </div>
   </form> 
                </div>
             
                </div>
               
               
            </div>
        </div>
       

    </div>
    {% endblock %}", "default/contact.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\contact.html.twig");
    }
}
