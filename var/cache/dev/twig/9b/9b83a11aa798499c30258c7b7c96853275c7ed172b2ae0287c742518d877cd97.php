<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_ccd715d79ec0e30505e93b78d3b08b9f891d8123c4fce2d3d02ea977cfb9fa01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3866344677b935530fd9be491db83a7eed95de4b71fbf2ffe22deae6d9d0f18f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3866344677b935530fd9be491db83a7eed95de4b71fbf2ffe22deae6d9d0f18f->enter($__internal_3866344677b935530fd9be491db83a7eed95de4b71fbf2ffe22deae6d9d0f18f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_2a411202c9bd8227cf52e3c1030e595e9e2a2c5ba138a84ce5c18bce8ca91b13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a411202c9bd8227cf52e3c1030e595e9e2a2c5ba138a84ce5c18bce8ca91b13->enter($__internal_2a411202c9bd8227cf52e3c1030e595e9e2a2c5ba138a84ce5c18bce8ca91b13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3866344677b935530fd9be491db83a7eed95de4b71fbf2ffe22deae6d9d0f18f->leave($__internal_3866344677b935530fd9be491db83a7eed95de4b71fbf2ffe22deae6d9d0f18f_prof);

        
        $__internal_2a411202c9bd8227cf52e3c1030e595e9e2a2c5ba138a84ce5c18bce8ca91b13->leave($__internal_2a411202c9bd8227cf52e3c1030e595e9e2a2c5ba138a84ce5c18bce8ca91b13_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_450a06cdddc852683d04c8e7a129caf6def37496e5cad4d6c22ef16a7eff8598 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_450a06cdddc852683d04c8e7a129caf6def37496e5cad4d6c22ef16a7eff8598->enter($__internal_450a06cdddc852683d04c8e7a129caf6def37496e5cad4d6c22ef16a7eff8598_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_8d882dfc1a7a9e59f371b55511bbe6457d3430c76d71ee11d75cae389ee8a96f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d882dfc1a7a9e59f371b55511bbe6457d3430c76d71ee11d75cae389ee8a96f->enter($__internal_8d882dfc1a7a9e59f371b55511bbe6457d3430c76d71ee11d75cae389ee8a96f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_8d882dfc1a7a9e59f371b55511bbe6457d3430c76d71ee11d75cae389ee8a96f->leave($__internal_8d882dfc1a7a9e59f371b55511bbe6457d3430c76d71ee11d75cae389ee8a96f_prof);

        
        $__internal_450a06cdddc852683d04c8e7a129caf6def37496e5cad4d6c22ef16a7eff8598->leave($__internal_450a06cdddc852683d04c8e7a129caf6def37496e5cad4d6c22ef16a7eff8598_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_03e61be41220917c638bb0ba3b3fb48658e64c437ecf1b5a42441c282ed46b48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03e61be41220917c638bb0ba3b3fb48658e64c437ecf1b5a42441c282ed46b48->enter($__internal_03e61be41220917c638bb0ba3b3fb48658e64c437ecf1b5a42441c282ed46b48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_afcbdf3c57514b2907bdefc42b4bba1dd0983957572e5294c5e82280a28abb1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afcbdf3c57514b2907bdefc42b4bba1dd0983957572e5294c5e82280a28abb1b->enter($__internal_afcbdf3c57514b2907bdefc42b4bba1dd0983957572e5294c5e82280a28abb1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_afcbdf3c57514b2907bdefc42b4bba1dd0983957572e5294c5e82280a28abb1b->leave($__internal_afcbdf3c57514b2907bdefc42b4bba1dd0983957572e5294c5e82280a28abb1b_prof);

        
        $__internal_03e61be41220917c638bb0ba3b3fb48658e64c437ecf1b5a42441c282ed46b48->leave($__internal_03e61be41220917c638bb0ba3b3fb48658e64c437ecf1b5a42441c282ed46b48_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_81366c0379f0579c6a8d25fd6a5df4a8e090173e0077a5be58d2d256d849a9e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81366c0379f0579c6a8d25fd6a5df4a8e090173e0077a5be58d2d256d849a9e5->enter($__internal_81366c0379f0579c6a8d25fd6a5df4a8e090173e0077a5be58d2d256d849a9e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_5b86cc7aadd2303ba380135f6f2c15af9e2a4bd0566f0da9be6b7b088bb18a99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b86cc7aadd2303ba380135f6f2c15af9e2a4bd0566f0da9be6b7b088bb18a99->enter($__internal_5b86cc7aadd2303ba380135f6f2c15af9e2a4bd0566f0da9be6b7b088bb18a99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_5b86cc7aadd2303ba380135f6f2c15af9e2a4bd0566f0da9be6b7b088bb18a99->leave($__internal_5b86cc7aadd2303ba380135f6f2c15af9e2a4bd0566f0da9be6b7b088bb18a99_prof);

        
        $__internal_81366c0379f0579c6a8d25fd6a5df4a8e090173e0077a5be58d2d256d849a9e5->leave($__internal_81366c0379f0579c6a8d25fd6a5df4a8e090173e0077a5be58d2d256d849a9e5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
