<?php

/* default/listehotels.html.twig */
class __TwigTemplate_17be1a1c31adbaf51673c44b1c070a8445b259b90f24f8bddb0ac0bbc2d6ece9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/listehotels.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69ed8e6cc2503bff0a5f8f5ec2da01ae563430c6362cd7ec75a4f7b946653dab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69ed8e6cc2503bff0a5f8f5ec2da01ae563430c6362cd7ec75a4f7b946653dab->enter($__internal_69ed8e6cc2503bff0a5f8f5ec2da01ae563430c6362cd7ec75a4f7b946653dab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/listehotels.html.twig"));

        $__internal_a4b8f6e36a44825ab10e75f54c45a495c06e5240ff2585d68d3d6edd336d4dc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4b8f6e36a44825ab10e75f54c45a495c06e5240ff2585d68d3d6edd336d4dc6->enter($__internal_a4b8f6e36a44825ab10e75f54c45a495c06e5240ff2585d68d3d6edd336d4dc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/listehotels.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_69ed8e6cc2503bff0a5f8f5ec2da01ae563430c6362cd7ec75a4f7b946653dab->leave($__internal_69ed8e6cc2503bff0a5f8f5ec2da01ae563430c6362cd7ec75a4f7b946653dab_prof);

        
        $__internal_a4b8f6e36a44825ab10e75f54c45a495c06e5240ff2585d68d3d6edd336d4dc6->leave($__internal_a4b8f6e36a44825ab10e75f54c45a495c06e5240ff2585d68d3d6edd336d4dc6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_97ae11c69d014b9f28c233824e53592263e1441d1ae3869b5d9d4e3d9063929c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97ae11c69d014b9f28c233824e53592263e1441d1ae3869b5d9d4e3d9063929c->enter($__internal_97ae11c69d014b9f28c233824e53592263e1441d1ae3869b5d9d4e3d9063929c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_febeda8bee4ac7007640a45f8fb27e31760293e1da3f9b48f3c85723e07ef205 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_febeda8bee4ac7007640a45f8fb27e31760293e1da3f9b48f3c85723e07ef205->enter($__internal_febeda8bee4ac7007640a45f8fb27e31760293e1da3f9b48f3c85723e07ef205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo " <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
 <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/ListeEtoile.js"), "html", null, true);
        echo "\"></script>
 <style>
    #ui-datepicker-div{
    font-size:14px !important;
    
}
.ui-datepicker .ui-datepicker-header {
    position: relative;
    padding: .2em 0;
    background: #374e6af5;
    color: #fff;
}
.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
    border: 1px solid #ad256c !important;
    /* background: #fbf9ee url(images/ui-bg_glass_55_fbf9ee_1x400.png) 50% 50% repeat-x; */
    background: #ad256c !important;
    color: #fff !important;
}
</style>


  <script>
 \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
  </script>




 <style>  
 @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200&display=swap');
.k-state-disabled a.k-link 
{    
text-decoration: line-through;    
color: red;
}    
.k-calendar .k-state-selected .k-link {
    border-color: #ffcc33!important ;
    color: #fff;
    background-color: #ffcc33!important ;
     font-family: 'Poppins', sans-serif !important;
  
}
.k-calendar .k-state-selected .k-link:hover {
    border-color: #ffcc33!important ;
    color: #fff;
    background-color: #ffcc33!important ;
}
.k-calendar .k-calendar-content .k-calendar-th, .k-calendar .k-calendar-content th, .k-calendar .k-calendar-view .k-calendar-th .k-meta-header, .k-calendar .k-calendar-view th, .k-calendar .k-content .k-calendar-th, .k-calendar .k-content th, .k-calendar .k-month-header {
    color: #fff !important;
     font-family: 'Poppins', sans-serif !important;
   
}
.k-calendar .k-other-month {
    color: #ffcc33 !important;
}
.k-calendar .k-calendar-footer, .k-calendar .k-footer {
    text-align: center;
    clear: both;
    color: #cc3300;
    background: #f5f5f5;
}
.k-input {
    margin: 0;
    padding: 4px 8px !important;
    width: 100% !important;
    min-width: 0;
    height: calc(1.4285714286em + 8px);
    border: 0 !important;
    height: 25px !important;
    font-weight:100;
}
.k-calendar  {
    color:#0093a9;
}
.k-calendar .k-calendar-table.k-calendar-content, .k-calendar .k-calendar-table.k-content, .k-calendar table.k-calendar-content, .k-calendar table.k-content {
    display: inline-table;
    vertical-align: top;
    color: #fff;
    background: #0093a9;
}

.pagination {
 
    padding-left: 169px;
}

.label-warning {
    background-color: #ffcc33!important;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;
}

</style>  
 <div class=\"HotelEnTunisie\">

          <div class=\" container\">
   ";
        // line 157
        $context["i"] = 0;
        // line 158
        echo "   ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 159
            echo "         ";
            $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
            // line 160
            echo "         ";
            if ((($context["i"] ?? $this->getContext($context, "i")) == 1)) {
                // line 161
                echo "             <div class=\"alert alert-success\" style=\"color:#fff;\"><center> <i class=\"fa fa-check\"></i> ";
                echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
                echo "</center></div><br/>
        ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 163
        echo "              
          <form action=\"\" method=\"post\">
                      <div class=\"filtre-liste-hotel\">

                        <div class=\" col-12 titre-filtre\"><h6>Moteur de recherche</h6></div>

                        <div class=\"content-fltre row\">

                        

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-4\" >

                                    <span>Ville</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">
                                  <select class=\"custom-select form-control form-control-sm\"  name=\"ville\">

                                  <option value=\"Tous\">Tous</option>   ";
        // line 190
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["villes"] ?? $this->getContext($context, "villes")));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            echo " <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "id", array()), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "name", array()), "html", null, true);
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "
                                  </select>

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-6\" >

                                    <span>Nom d'hotel</span>

                                  </div>

                                  <div class=\"col-6\" style=\"padding-right:0rem;\">
                                  
                                  <select class=\"custom-select form-control form-control-sm\" name=\"hotels\">

                                  <option value=\"Tous\">Tous</option>
                                      ";
        // line 219
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["listehotels"] ?? $this->getContext($context, "listehotels")));
        foreach ($context['_seq'] as $context["_key"] => $context["h"]) {
            // line 220
            echo "                                      <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "name", array()), "html", null, true);
            echo "</option>
                                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 222
        echo "                                  </select>

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-5\">

                                    <span>Arrangements</span>

                                  </div>

                                  <div class=\"col-7\" style=\"padding-right:0rem;\">
                                  <select class=\"custom-select form-control form-control-sm \" name=\"arr\" >
                                    <option value=\"Tous\">Tous</option> ";
        // line 254
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["arragement"] ?? $this->getContext($context, "arragement")));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "name", array()), "html", null, true);
            echo "</option>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 255
        echo "                                    </select>
                                  

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f1 row\">

                                  <div class=\"col-4\" >

                                    <span>Etoiles</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">

                                  <input type=\"hidden\" name=\"star\" value=\"3\" data-clearable=\"\" id=\"score-A1\">

                                    <div class=\"listeEtoile\" onmouseout=\"GestionHover('A1', -1, '5')\">

                                      <ul class=\"list-unstyled\" style=\"display: flex;margin-top: 0.7rem;\">

                                        <li><a href=\"javascript:ChoixSelection('A1', '1', '5')\" onmouseover=\"GestionHover('A1', '1', '5')\">

                                          <img id=\"staroff-A1-1\" src=\"";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: none;\">

                                          <img id=\"staron-A1-1\" src=\"";
        // line 290
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: block;\"></a></li><li><a href=\"javascript:ChoixSelection('A1', '2', '5')\" onmouseover=\"GestionHover('A1', '2', '5')\">

                                      <img id=\"staroff-A1-2\" src=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: none;\">

                                      <img id=\"staron-A1-2\" src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: block;\"></a>

                                    </li><li><a href=\"javascript:ChoixSelection('A1', '3', '5')\" onmouseover=\"GestionHover('A1', '3', '5')\">

                                      <img id=\"staroff-A1-3\" src=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: none;\">

                                      <img id=\"staron-A1-3\" src=\"";
        // line 300
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: block;\"></a></li>

                                      <li><a href=\"javascript:ChoixSelection('A1', '4', '5')\" onmouseover=\"GestionHover('A1', '4', '5')\">

                                        <img id=\"staroff-A1-4\" src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: block;\">

                                        <img id=\"staron-A1-4\" src=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: none;\"></a></li><li>

                                          <a href=\"javascript:ChoixSelection('A1', '5', '5')\" onmouseover=\"GestionHover('A1', '5', '5')\">

                                            <img id=\"staroff-A1-5\" src=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej2.png"), "html", null, true);
        echo "\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: block;\">

                                            <img id=\"staron-A1-5\" src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoilej4.png"), "html", null, true);
        echo "\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: none;\"></a></li>\t

                                          </ul></div>

                                  </div>

                                </div>

                    

                              </div>

                                

                    

                              </div>

                            </div>

                        

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-3\" >

                                    <span>Départ</span>

                                  </div>

                                  <div class=\"col-9\" style=\"padding-right:0rem;padding-left: 1rem;\">

                                  <input type=\"date\" id=\"start\" name=\"dated\" value=\"";
        // line 350
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d-m-Y"), "html", null, true);
        echo "\"

                                  style=\"height: 2.5rem; border-color: transparent; background-color: transparent;\">

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-3\">

                                    <span>Arrivée</span>

                                  </div>

                                  <div class=\"col-9\" style=\"padding-right:0rem;padding-left: 1rem;\">

                                    <input type=\"date\" id=\"start\" nname=\"datef\" value=\"";
        // line 376
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "d-m-Y"), "html", null, true);
        echo "\"

                                    min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-4\">

                                    <span>  Adultes</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">
                                <select class=\"custom-select form-control form-control-sm\" name=\"adultes\">
                                            ";
        // line 410
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 411
            echo "                                              <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 413
        echo "
                                </select>
                                

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-6\">

                                    <span>Enfant</span>

                                  </div>

                                  <div class=\"col-6\" style=\"padding-right:0rem;\">
                              <select class=\"custom-select form-control form-control-sm\"  name=\"enfants\" >
                                  ";
        // line 439
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 440
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 442
        echo "                                </select>
                                

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                        <div class=\"col-md-12 col-12\"> 
                        <button type=\"submit\" class=\"btn btn-primary btnfil\" style=\"border-color:#F4991C; background-image: linear-gradient(to right, #5c8e30 , #F4991C); color: #fff;float: right;width:23%\"> <i class=\"fa fa-search \"></i>  Rechercher</button>
                        </div>

                        



                        </div>





                      </div>
          </form>
           ";
        // line 475
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hotels"] ?? $this->getContext($context, "hotels")));
        foreach ($context['_seq'] as $context["_key"] => $context["h"]) {
            // line 476
            echo "            <div class=\"row\">

              <div class=\"col-md-12 col-sm-12 col-xs-12\">

              <div class=\"tour-single row\">

                    <div class=\"tour-image-list col-md-2 col-12\">

                      <figure>
                 ";
            // line 485
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["img"]);
            foreach ($context['_seq'] as $context["_key"] => $context["img"]) {
                // line 486
                echo "                 ";
                if (($this->getAttribute($context["img"], "hotel", array()) == $context["h"])) {
                    // line 487
                    echo "                     <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["img"], "file", array())), "html", null, true);
                    echo "\" width=\"360\" height=\"254\" alt=\"...\" class=\"img-responsive wp-post-image\" alt=\"\">
                   
                 ";
                }
                // line 490
                echo "                 ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['img'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 491
            echo "                 <figcaption>
                     <a href=\"";
            // line 492
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailshotel", array("id" => $this->getAttribute($context["h"], "id", array()))), "html", null, true);
            echo "\">Lire la suite</a>
                        </figcaption>

                      </figure>

                    </div>

                    <div class=\"tour-text col-md-8 col-12\">

                      <div class=\"tour-details\">

                        <h3><a href=\"http://themes.ongoingthemes.com/tripandguide/tours/santorini-islands-tour/\">";
            // line 503
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "name", array()), "html", null, true);
            echo "</a> </h3>

                        <div class=\"tour-rating\">

                          <ul class=\"list-unstyled clearfix\">

                            <li>

                            <i class=\"fa fa-star active\"></i>

                             <i class=\"fa fa-star active\"></i>

                             <i class=\"fa fa-star active\"></i> 

                             <i class=\"fa fa-star active\"></i> 

                             <i class=\"fa fa-star active\"></i> 

                            </li>

                             </ul>

                      \t</div>

                        <p class=\"tour-duration\"> <strong>";
            // line 527
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "ville", array()), "html", null, true);
            echo "</strong> </p>

                        <p style=\"font-size: 14px; \">";
            // line 529
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["h"], "shortdesc", array()), 0, 160), "html", null, true);
            echo "...</p>

                        <i class=\"fas fa-utensils \" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-tv\" style=\"font-size: 15px;color: #5C8E30;\"></i>

                        <i class=\"far fa-snowflake\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-wheelchair\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-wifi\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-medkit\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-spa\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-swimming-pool\" style=\"color: #5C8E30;\"></i>

                      </div>

                    </div>

                    <div class=\"tour-tripadvisor col-md-2 col-12 row\">


                        <div class=\"col-12\">

                          <a href=\"";
            // line 556
            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "tripadvisor", array()), "html", null, true);
            echo "\" target=\"_blank\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/tripadvisor.png"), "html", null, true);
            echo "\" class=\"tripadvisor\"></a>

                        </div>

                             <small style=\"line-height: 1.5rem;margin-top: -1.5rem;\">À partir de : </small>                                                  

                                 ";
            // line 562
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 563
                echo "                   ";
                if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                    // line 564
                    echo "                    ";
                    $context["array"] = array();
                    // line 565
                    echo "                    
                     ";
                    // line 566
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["arr"]) {
                        // line 567
                        echo "                    
                      ";
                        // line 568
                        if (($this->getAttribute($context["arr"], "etat", array()) == 1)) {
                            // line 569
                            echo "                
                        ";
                            // line 570
                            $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                            // line 571
                            echo "                   
                        <h5 class=\"card-title\" style=\"color: #cc3300;display:none;\">  ";
                            // line 572
                            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                                echo " ";
                                echo twig_escape_filter($this->env, ($context["totalprice"] ?? $this->getContext($context, "totalprice")), "html", null, true);
                                echo " TND ";
                            } else {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array()));
                                echo " ";
                                echo twig_escape_filter($this->env, ($context["totalprice"] ?? $this->getContext($context, "totalprice")), "html", null, true);
                                echo " TND";
                            }
                            echo "  </h5>
                   
                    ";
                            // line 574
                            $context["array"] = twig_array_merge(($context["array"] ?? $this->getContext($context, "array")), array(0 => ($context["totalprice"] ?? $this->getContext($context, "totalprice"))));
                            // line 575
                            echo "                   
                    
                    
                     ";
                        }
                        // line 579
                        echo "                     ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['arr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 580
                    echo "                      <br/>      
               <h5 class=\"card-title\" style=\"color: #cc3300;margin-top: -1.5rem;\">";
                    // line 581
                    if ((twig_length_filter($this->env, ($context["array"] ?? $this->getContext($context, "array"))) > 0)) {
                        echo " ";
                        echo twig_escape_filter($this->env, min(($context["array"] ?? $this->getContext($context, "array"))), "html", null, true);
                        echo " TND";
                    }
                    echo "</h5>
                       
                    ";
                }
                // line 584
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 585
            echo "               <div class=\"btnlisthotel w-100\"> 

                          <a href=\"";
            // line 587
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("chambres", array("id" => $this->getAttribute($context["h"], "id", array()), "marcheid" => $this->getAttribute(($context["marcher"] ?? $this->getContext($context, "marcher")), "id", array()), "dated" => twig_date_format_filter($this->env, "now", "d-m-Y"), "datef" => twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+1 day"), "d-m-Y"))), "html", null, true);
            echo "\" class=\"btn btn-primary \">Choisir Chambres</a>

                        </div> 

                          <div class=\"btnlisthotel2 w-100\">

                          <a id=\"a\" href=\"";
            // line 593
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reserver", array("id" => $this->getAttribute($context["h"], "id", array()), "marcheid" => $this->getAttribute(($context["marcher"] ?? $this->getContext($context, "marcher")), "id", array()), "dated" => twig_date_format_filter($this->env, "now", "d-m-Y"), "datef" => twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+1 day"), "d-m-Y"))), "html", null, true);
            echo "\" class=\"btn btn-primary \">Réserver</a>


                          </div>  

                     </div>

                    <table class=\"table text-center table-bordered \" style=\"margin:1rem;margin-top: -1rem;\">

                       <thead>
                        <tr>
                          <th scope=\"col-1\" >Num</th> 
                          <th scope=\"col-2\" >Type de chambres</th>
                          <th scope=\"col-2\" >Arrangement</th>
                          <th scope=\"col-1\" >Nb</th>
                          <th scope=\"col-1\" >Adultes</th>
                          <th scope=\"col-1\" >Enfants</th>
                          <th scope=\"col-2\">Disponiblité</th>
                          <th scope=\"col-2\">Prix pour une nuitée(s)</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        
                           <td scope=\"row\" data-label=\"num\">1</td>
                           
                            <td scope=\"row\" data-label=\"Type de chambres\">Chambre Double</td>

                         ";
            // line 622
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 623
                echo "                      ";
                if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                    // line 624
                    echo "                    ";
                    if ((twig_length_filter($this->env, $this->getAttribute($context["c"], "pricearr", array())) > 0)) {
                        echo "                
   
                    
                 ";
                        // line 627
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                        $context['loop'] = array(
                          'parent' => $context['_parent'],
                          'index0' => 0,
                          'index'  => 1,
                          'first'  => true,
                        );
                        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                            $length = count($context['_seq']);
                            $context['loop']['revindex0'] = $length - 1;
                            $context['loop']['revindex'] = $length;
                            $context['loop']['length'] = $length;
                            $context['loop']['last'] = 1 === $length;
                        }
                        foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                            // line 628
                            echo "                       ";
                            if ($this->getAttribute($context["loop"], "first", array())) {
                                echo "   
                       <td data-label=\"Arrangement\"> 
                         
 
                         <select name=\"arr\" id=\"monselect";
                                // line 632
                                echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
                                echo "\" class=\"form-control formarrang\" style=\"font-size:14px;\">
                         ";
                                // line 633
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                                    // line 634
                                    echo "                               ";
                                    if (($this->getAttribute($context["arr"], "etat", array()) == 1)) {
                                        // line 635
                                        echo "                        <option id=\"opt_";
                                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                                        echo "\" value=\"";
                                        echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                                        echo "\">
                            ";
                                        // line 636
                                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["arr"], "hotelarrangement", array()), "arrangement", array()), "name", array()), "html", null, true);
                                        echo "
                        </option>
                        ";
                                    }
                                    // line 639
                                    echo "                         ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 640
                                echo "                        </select>

                        </td>
                        ";
                            }
                            // line 644
                            echo "                            ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                            if (isset($context['loop']['length'])) {
                                --$context['loop']['revindex0'];
                                --$context['loop']['revindex'];
                                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 645
                        echo "                   
                            <script type=\"text/javascript\">
    
                                \$(document).ready(function(){
                                
                                \$(\"#monselect";
                        // line 650
                        echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
                        echo "\").change(function() {
                                 
                                 ";
                        // line 652
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                            echo " 
                                \t\tif (\$(\"#monselect";
                            // line 653
                            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
                            echo " :selected\").val() == \$(\"#opt_\"+";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                            echo ").val())
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#monselect";
                            // line 655
                            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
                            echo " :selected\").val();
                                \t    \tvar total = '#total'+\$(\"#monselect";
                            // line 656
                            echo twig_escape_filter($this->env, $this->getAttribute($context["h"], "id", array()), "html", null, true);
                            echo " :selected\").val();
                                \t\t\t\$(comp).show('slow');
                                \t\t\t\$(total).show('slow');
                                \t\t}
                                \t\telse
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#opt_\"+";
                            // line 662
                            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                            echo ").val();
                                \t\t\tvar total = '#total'+\$(\"#opt_\"+";
                            // line 663
                            echo twig_escape_filter($this->env, $this->getAttribute($context["i"], "id", array()), "html", null, true);
                            echo ").val();
                                \t\t\t\$(comp).hide();
                                \t\t\t\$(total).hide();
                                \t\t}
                              
                                \t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 668
                        echo "\t\t
                                 
                                \t});
                                   })  

                                </script> 
                    ";
                    } else {
                        // line 675
                        echo "                     <td data-label=\"Arrangement\"><span class=\"label  ptip_ne\" style=\"    background-color: #cc3300;
                    color: #fff;
                    padding: 3px 12px 4px 15px;
                    border-radius: 4px;
                    font-size: 12px !important;
                    height: 22px;\" > Aucun arrangement ajouté </span> </td>
                                      ";
                    }
                    // line 681
                    echo " ";
                }
                // line 682
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 683
            echo "                            <td data-label=\"Nb\">2</td>
                       <td data-label=\"Adultes\">2</td>
                        
                          <td data-label=\"Enfants\">0</td>
                          <td data-label=\"Disponiblité\"><span class=\"label  ptip_ne\" style=\"    background-color: #28a745;
                            color: #fff;
                            padding: 3px 12px 4px 15px;
                            border-radius: 4px;
                            font-size: 12px !important;
                            height: 22px;\" > Disponible </span></td>
                          <td data-label=\"Total\"> 
        
             ";
            // line 695
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 696
                echo "                      ";
                if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                    // line 697
                    echo "                        
               
                  ";
                    // line 699
                    $context["array"] = array();
                    // line 700
                    echo "                        ";
                    $context["j"] = 0;
                    // line 701
                    echo "                     ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                        // line 702
                        echo "                    
                
                     ";
                        // line 704
                        $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                        // line 705
                        echo "              ";
                        $context["array"] = array("key" =>                         // line 706
($context["totalprice"] ?? $this->getContext($context, "totalprice")));
                        // line 709
                        echo "                     ";
                        $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                        // line 710
                        echo "                     ";
                        if ((((($context["j"] ?? $this->getContext($context, "j")) == 0) && ($this->getAttribute($context["arr"], "price", array()) == min(($context["array"] ?? $this->getContext($context, "array"))))) && ($this->getAttribute($context["arr"], "etat", array()) == 1))) {
                            // line 711
                            echo "                     <span class=\"label label-warning ptip_ne\"  id=\"comp_";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\" >  
          ";
                            // line 712
                            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                                echo " ";
                                echo twig_escape_filter($this->env, (($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2), "html", null, true);
                                echo " TND ";
                            } else {
                                echo " ";
                                echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2), "html", null, true);
                                echo " TND";
                            }
                            echo "  
                  </span>
          
          ";
                            // line 715
                            $context["j"] = 1;
                            // line 716
                            echo "                  ";
                        } else {
                            // line 717
                            echo "                        <span class=\"label label-warning ptip_ne\" style=\"display:none;\" id=\"comp_";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\" >  
          ";
                            // line 718
                            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                                echo " ";
                                echo twig_escape_filter($this->env, (($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2), "html", null, true);
                                echo " TND ";
                            } else {
                                echo " ";
                                echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2), "html", null, true);
                                echo " TND";
                            }
                            echo "  
                  </span>
                  ";
                        }
                        // line 721
                        echo "                 
                     ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 723
                    echo "                      ";
                    // line 724
                    echo "                       
                        ";
                }
                // line 726
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 727
            echo "                    </td >
                          </tr>
                          <tr class=\"disp\">
                         <td> </td>
                         <td></td>
                         <td style=\"text-align:right;\"> <strong> TOTAL : </strong></td> 
                         <td>2</td>
                          <td>2</td>
                           ";
            // line 736
            echo "                          ";
            // line 737
            echo "                           <td>0</td>
                          <td></td>
                            ";
            // line 740
            echo "                          <td>  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contrats"] ?? $this->getContext($context, "contrats")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 741
                echo "                      ";
                if (($this->getAttribute($context["c"], "hotel", array()) == $context["h"])) {
                    // line 742
                    echo "                        ";
                    $context["array"] = array();
                    // line 743
                    echo "                       ";
                    $context["j"] = 0;
                    // line 744
                    echo "                     ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["c"], "pricearr", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["arr"]) {
                        // line 745
                        echo "                    
                
                     ";
                        // line 747
                        $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                        // line 748
                        echo "              ";
                        $context["array"] = array("key" =>                         // line 749
($context["totalprice"] ?? $this->getContext($context, "totalprice")));
                        // line 752
                        echo "                     ";
                        $context["totalprice"] = $this->getAttribute($context["arr"], "price", array());
                        // line 753
                        echo "                     ";
                        if ((((($context["j"] ?? $this->getContext($context, "j")) == 0) && ($this->getAttribute($context["arr"], "price", array()) == min(($context["array"] ?? $this->getContext($context, "array"))))) && ($this->getAttribute($context["arr"], "etat", array()) == 1))) {
                            // line 754
                            echo "         <span class=\"label label-warning ptip_ne\"  id=\"total";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\">  ";
                            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                                echo " ";
                                echo twig_escape_filter($this->env, (($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2), "html", null, true);
                                echo " TND ";
                            } else {
                                echo " ";
                                echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2), "html", null, true);
                                echo " TND";
                            }
                            echo "  </span>
                  ";
                            // line 755
                            $context["j"] = 1;
                            // line 756
                            echo "                   ";
                        } else {
                            // line 757
                            echo "         <span class=\"label label-warning ptip_ne\"  style=\"display:none;\" id=\"total";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["arr"], "id", array()), "html", null, true);
                            echo "\">  ";
                            if (($this->getAttribute($context["arr"], "persm", array()) == 1)) {
                                echo "  ";
                                $context["totalprice"] = (($context["totalprice"] ?? $this->getContext($context, "totalprice")) + (($this->getAttribute($context["arr"], "price", array()) * $this->getAttribute($context["arr"], "marge", array())) / 100));
                                echo " ";
                                echo twig_escape_filter($this->env, (($context["totalprice"] ?? $this->getContext($context, "totalprice")) * 2), "html", null, true);
                                echo " TND ";
                            } else {
                                echo " ";
                                echo twig_escape_filter($this->env, ((($context["totalprice"] ?? $this->getContext($context, "totalprice")) + $this->getAttribute($context["arr"], "marge", array())) * 2), "html", null, true);
                                echo " TND";
                            }
                            echo "  </span>

                    ";
                        }
                        // line 760
                        echo "                     ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['arr'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 761
                    echo "                        ";
                    // line 762
                    echo "                        ";
                }
                // line 763
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "</td>
                          </tr>
                          </tr>
                        
                      </tbody>
                    
                    </table>

                  <div class=\"tour-booking\">

                    <a href=\"";
            // line 773
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reserver", array("id" => $this->getAttribute($context["h"], "id", array()), "marcheid" => $this->getAttribute(($context["marcher"] ?? $this->getContext($context, "marcher")), "id", array()), "dated" => twig_date_format_filter($this->env, "now", "d-m-Y"), "datef" => twig_date_format_filter($this->env, twig_date_modify_filter($this->env, "now", "+1 day"), "d-m-Y"))), "html", null, true);
            echo "\">Rèserver</a>

                  </div>

                </div>

                

              </div>

            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['h'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 785
        echo "          </div>



            



        </div>













































  <div class=\"pagination\">

  ";
        // line 841
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["hotels"] ?? $this->getContext($context, "hotels")));
        echo "

</div>   

               

                <br/><br/>
            </div>
            
        </div>
               


     </div>

        </div>
    <div>

    </div>
  </div>

</div>



  



";
        
        $__internal_febeda8bee4ac7007640a45f8fb27e31760293e1da3f9b48f3c85723e07ef205->leave($__internal_febeda8bee4ac7007640a45f8fb27e31760293e1da3f9b48f3c85723e07ef205_prof);

        
        $__internal_97ae11c69d014b9f28c233824e53592263e1441d1ae3869b5d9d4e3d9063929c->leave($__internal_97ae11c69d014b9f28c233824e53592263e1441d1ae3869b5d9d4e3d9063929c_prof);

    }

    public function getTemplateName()
    {
        return "default/listehotels.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1383 => 841,  1325 => 785,  1307 => 773,  1290 => 763,  1287 => 762,  1285 => 761,  1279 => 760,  1260 => 757,  1257 => 756,  1255 => 755,  1238 => 754,  1235 => 753,  1232 => 752,  1230 => 749,  1228 => 748,  1226 => 747,  1222 => 745,  1217 => 744,  1214 => 743,  1211 => 742,  1208 => 741,  1203 => 740,  1199 => 737,  1197 => 736,  1187 => 727,  1181 => 726,  1177 => 724,  1175 => 723,  1168 => 721,  1152 => 718,  1147 => 717,  1144 => 716,  1142 => 715,  1126 => 712,  1121 => 711,  1118 => 710,  1115 => 709,  1113 => 706,  1111 => 705,  1109 => 704,  1105 => 702,  1100 => 701,  1097 => 700,  1095 => 699,  1091 => 697,  1088 => 696,  1084 => 695,  1070 => 683,  1064 => 682,  1061 => 681,  1052 => 675,  1043 => 668,  1031 => 663,  1027 => 662,  1018 => 656,  1014 => 655,  1007 => 653,  1001 => 652,  996 => 650,  989 => 645,  975 => 644,  969 => 640,  963 => 639,  957 => 636,  950 => 635,  947 => 634,  943 => 633,  939 => 632,  931 => 628,  914 => 627,  907 => 624,  904 => 623,  900 => 622,  868 => 593,  859 => 587,  855 => 585,  849 => 584,  839 => 581,  836 => 580,  830 => 579,  824 => 575,  822 => 574,  805 => 572,  802 => 571,  800 => 570,  797 => 569,  795 => 568,  792 => 567,  788 => 566,  785 => 565,  782 => 564,  779 => 563,  775 => 562,  764 => 556,  734 => 529,  729 => 527,  702 => 503,  688 => 492,  685 => 491,  679 => 490,  672 => 487,  669 => 486,  665 => 485,  654 => 476,  650 => 475,  615 => 442,  604 => 440,  600 => 439,  572 => 413,  561 => 411,  557 => 410,  520 => 376,  491 => 350,  450 => 312,  445 => 310,  438 => 306,  433 => 304,  426 => 300,  421 => 298,  414 => 294,  409 => 292,  404 => 290,  399 => 288,  364 => 255,  351 => 254,  317 => 222,  306 => 220,  302 => 219,  272 => 191,  259 => 190,  230 => 163,  220 => 161,  217 => 160,  214 => 159,  209 => 158,  207 => 157,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
 <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">
  <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
  <script src=\"//code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
 <script src=\"{{asset('assets/js/ListeEtoile.js')}}\"></script>
 <style>
    #ui-datepicker-div{
    font-size:14px !important;
    
}
.ui-datepicker .ui-datepicker-header {
    position: relative;
    padding: .2em 0;
    background: #374e6af5;
    color: #fff;
}
.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
    border: 1px solid #ad256c !important;
    /* background: #fbf9ee url(images/ui-bg_glass_55_fbf9ee_1x400.png) 50% 50% repeat-x; */
    background: #ad256c !important;
    color: #fff !important;
}
</style>


  <script>
 \$(function()
        {

            \$(\"#datedebut\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                maxDate: '+1Y+6M',
                onSelect: function (dateStr) {
                    var today = \$(this).datepicker(\"getDate\");
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate()+1);
                    \$(\"#datefin\").datepicker('option', 'minDate', tomorrow || '0');
                }
            });

            \$(\"#datefin\").datepicker({
                altField: \"#datepicker\",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'dd-mm-yy',
                minDate: '0',
                
                onSelect: function (dateStr) {
                    var max = \$(this).datepicker('getDate'); // Get selected date
                    \$('#datepicker').datepicker('option', 'maxDate', max || '+12Y+6M'); // Set other max, default to +18 months

                }
            });



        });
     
  </script>




 <style>  
 @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200&display=swap');
.k-state-disabled a.k-link 
{    
text-decoration: line-through;    
color: red;
}    
.k-calendar .k-state-selected .k-link {
    border-color: #ffcc33!important ;
    color: #fff;
    background-color: #ffcc33!important ;
     font-family: 'Poppins', sans-serif !important;
  
}
.k-calendar .k-state-selected .k-link:hover {
    border-color: #ffcc33!important ;
    color: #fff;
    background-color: #ffcc33!important ;
}
.k-calendar .k-calendar-content .k-calendar-th, .k-calendar .k-calendar-content th, .k-calendar .k-calendar-view .k-calendar-th .k-meta-header, .k-calendar .k-calendar-view th, .k-calendar .k-content .k-calendar-th, .k-calendar .k-content th, .k-calendar .k-month-header {
    color: #fff !important;
     font-family: 'Poppins', sans-serif !important;
   
}
.k-calendar .k-other-month {
    color: #ffcc33 !important;
}
.k-calendar .k-calendar-footer, .k-calendar .k-footer {
    text-align: center;
    clear: both;
    color: #cc3300;
    background: #f5f5f5;
}
.k-input {
    margin: 0;
    padding: 4px 8px !important;
    width: 100% !important;
    min-width: 0;
    height: calc(1.4285714286em + 8px);
    border: 0 !important;
    height: 25px !important;
    font-weight:100;
}
.k-calendar  {
    color:#0093a9;
}
.k-calendar .k-calendar-table.k-calendar-content, .k-calendar .k-calendar-table.k-content, .k-calendar table.k-calendar-content, .k-calendar table.k-content {
    display: inline-table;
    vertical-align: top;
    color: #fff;
    background: #0093a9;
}

.pagination {
 
    padding-left: 169px;
}

.label-warning {
    background-color: #ffcc33!important;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;
}

</style>  
 <div class=\"HotelEnTunisie\">

          <div class=\" container\">
   {%set i = 0%}
   {% for flashMessage in app.session.flashbag.get('success') %}
         {%set i = i+1%}
         {% if i==1 %}
             <div class=\"alert alert-success\" style=\"color:#fff;\"><center> <i class=\"fa fa-check\"></i> {{ flashMessage }}</center></div><br/>
        {% endif %}{%endfor%}
              
          <form action=\"\" method=\"post\">
                      <div class=\"filtre-liste-hotel\">

                        <div class=\" col-12 titre-filtre\"><h6>Moteur de recherche</h6></div>

                        <div class=\"content-fltre row\">

                        

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-4\" >

                                    <span>Ville</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">
                                  <select class=\"custom-select form-control form-control-sm\"  name=\"ville\">

                                  <option value=\"Tous\">Tous</option>   {% for v in villes %} <option value=\"{{v.id}}\" >{{v.name}}</option>{%endfor%}

                                  </select>

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-6\" >

                                    <span>Nom d'hotel</span>

                                  </div>

                                  <div class=\"col-6\" style=\"padding-right:0rem;\">
                                  
                                  <select class=\"custom-select form-control form-control-sm\" name=\"hotels\">

                                  <option value=\"Tous\">Tous</option>
                                      {% for h in listehotels %}
                                      <option value=\"{{h.id}}\">{{h.name}}</option>
                                      {%endfor%}
                                  </select>

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-5\">

                                    <span>Arrangements</span>

                                  </div>

                                  <div class=\"col-7\" style=\"padding-right:0rem;\">
                                  <select class=\"custom-select form-control form-control-sm \" name=\"arr\" >
                                    <option value=\"Tous\">Tous</option> {% for v in arragement %}<option value=\"{{v.id}}\">{{v.name}}</option>{%endfor%}
                                    </select>
                                  

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f1 row\">

                                  <div class=\"col-4\" >

                                    <span>Etoiles</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">

                                  <input type=\"hidden\" name=\"star\" value=\"3\" data-clearable=\"\" id=\"score-A1\">

                                    <div class=\"listeEtoile\" onmouseout=\"GestionHover('A1', -1, '5')\">

                                      <ul class=\"list-unstyled\" style=\"display: flex;margin-top: 0.7rem;\">

                                        <li><a href=\"javascript:ChoixSelection('A1', '1', '5')\" onmouseover=\"GestionHover('A1', '1', '5')\">

                                          <img id=\"staroff-A1-1\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: none;\">

                                          <img id=\"staron-A1-1\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"1\" style=\"border-width: 0px; display: block;\"></a></li><li><a href=\"javascript:ChoixSelection('A1', '2', '5')\" onmouseover=\"GestionHover('A1', '2', '5')\">

                                      <img id=\"staroff-A1-2\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: none;\">

                                      <img id=\"staron-A1-2\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"2\" style=\"border-width: 0px; display: block;\"></a>

                                    </li><li><a href=\"javascript:ChoixSelection('A1', '3', '5')\" onmouseover=\"GestionHover('A1', '3', '5')\">

                                      <img id=\"staroff-A1-3\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: none;\">

                                      <img id=\"staron-A1-3\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"3\" style=\"border-width: 0px; display: block;\"></a></li>

                                      <li><a href=\"javascript:ChoixSelection('A1', '4', '5')\" onmouseover=\"GestionHover('A1', '4', '5')\">

                                        <img id=\"staroff-A1-4\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: block;\">

                                        <img id=\"staron-A1-4\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"4\" style=\"border-width: 0px; display: none;\"></a></li><li>

                                          <a href=\"javascript:ChoixSelection('A1', '5', '5')\" onmouseover=\"GestionHover('A1', '5', '5')\">

                                            <img id=\"staroff-A1-5\" src=\"{{asset('assets/image/etoilej2.png')}}\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: block;\">

                                            <img id=\"staron-A1-5\" src=\"{{asset('assets/image/etoilej4.png')}}\" border=\"0\" title=\"5\" style=\"border-width: 0px; display: none;\"></a></li>\t

                                          </ul></div>

                                  </div>

                                </div>

                    

                              </div>

                                

                    

                              </div>

                            </div>

                        

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-3\" >

                                    <span>Départ</span>

                                  </div>

                                  <div class=\"col-9\" style=\"padding-right:0rem;padding-left: 1rem;\">

                                  <input type=\"date\" id=\"start\" name=\"dated\" value=\"{{\"now\"|date(\"d-m-Y\")}}\"

                                  style=\"height: 2.5rem; border-color: transparent; background-color: transparent;\">

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-3\">

                                    <span>Arrivée</span>

                                  </div>

                                  <div class=\"col-9\" style=\"padding-right:0rem;padding-left: 1rem;\">

                                    <input type=\"date\" id=\"start\" nname=\"datef\" value=\"{{\"now\"|date(\"d-m-Y\")}}\"

                                    min=\"2018-01-01\" max=\"2018-12-31\" style=\"height: 2.5rem; border-color: transparent;background-color: transparent;\">

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                          <div class=\"col-md-6 col-12\">

                            <div class=\"row\">

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-4\">

                                    <span>  Adultes</span>

                                  </div>

                                  <div class=\"col-8\" style=\"padding-right:0rem;\">
                                <select class=\"custom-select form-control form-control-sm\" name=\"adultes\">
                                            {% for i in 1..4 %}
                                              <option value=\"{{i}}\">{{i}}</option>
                                                            {%endfor%}

                                </select>
                                

                                  </div>

                                </div>

                                

                    

                              </div>

                              <div class=\"col-md-6 col-12\">

                                <div class=\"f2 row\">

                                  <div class=\"col-6\">

                                    <span>Enfant</span>

                                  </div>

                                  <div class=\"col-6\" style=\"padding-right:0rem;\">
                              <select class=\"custom-select form-control form-control-sm\"  name=\"enfants\" >
                                  {% for i in 0..4 %}
                                <option value=\"{{i}}\">{{i}}</option>
                                {%endfor%}
                                </select>
                                

                                  </div>

                                </div>

                                

                    

                              </div>

                            </div>

                          </div>

                        <div class=\"col-md-12 col-12\"> 
                        <button type=\"submit\" class=\"btn btn-primary btnfil\" style=\"border-color:#F4991C; background-image: linear-gradient(to right, #5c8e30 , #F4991C); color: #fff;float: right;width:23%\"> <i class=\"fa fa-search \"></i>  Rechercher</button>
                        </div>

                        



                        </div>





                      </div>
          </form>
           {% for h in hotels %}
            <div class=\"row\">

              <div class=\"col-md-12 col-sm-12 col-xs-12\">

              <div class=\"tour-single row\">

                    <div class=\"tour-image-list col-md-2 col-12\">

                      <figure>
                 {% for img in img %}
                 {% if img.hotel == h  %}
                     <img src=\"{{ asset( img.file) }}\" width=\"360\" height=\"254\" alt=\"...\" class=\"img-responsive wp-post-image\" alt=\"\">
                   
                 {% endif %}
                 {% endfor %}
                 <figcaption>
                     <a href=\"{{ path('detailshotel', {'id': h.id}) }}\">Lire la suite</a>
                        </figcaption>

                      </figure>

                    </div>

                    <div class=\"tour-text col-md-8 col-12\">

                      <div class=\"tour-details\">

                        <h3><a href=\"http://themes.ongoingthemes.com/tripandguide/tours/santorini-islands-tour/\">{{h.name}}</a> </h3>

                        <div class=\"tour-rating\">

                          <ul class=\"list-unstyled clearfix\">

                            <li>

                            <i class=\"fa fa-star active\"></i>

                             <i class=\"fa fa-star active\"></i>

                             <i class=\"fa fa-star active\"></i> 

                             <i class=\"fa fa-star active\"></i> 

                             <i class=\"fa fa-star active\"></i> 

                            </li>

                             </ul>

                      \t</div>

                        <p class=\"tour-duration\"> <strong>{{h.ville}}</strong> </p>

                        <p style=\"font-size: 14px; \">{{h.shortdesc|raw|slice(0, 160)}}...</p>

                        <i class=\"fas fa-utensils \" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-tv\" style=\"font-size: 15px;color: #5C8E30;\"></i>

                        <i class=\"far fa-snowflake\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-wheelchair\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-wifi\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-medkit\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-spa\" style=\"color: #5C8E30;\"></i>

                        <i class=\"fas fa-swimming-pool\" style=\"color: #5C8E30;\"></i>

                      </div>

                    </div>

                    <div class=\"tour-tripadvisor col-md-2 col-12 row\">


                        <div class=\"col-12\">

                          <a href=\"{{h.tripadvisor}}\" target=\"_blank\"><img src=\"{{asset('assets/image/tripadvisor.png')}}\" class=\"tripadvisor\"></a>

                        </div>

                             <small style=\"line-height: 1.5rem;margin-top: -1.5rem;\">À partir de : </small>                                                  

                                 {% for c in contrats %}
                   {% if c.hotel == h %}
                    {% set array = {}%}
                    
                     {%for key,arr in c.pricearr%}
                    
                      {%if arr.etat == 1%}
                
                        {%set totalprice = arr.price %}
                   
                        <h5 class=\"card-title\" style=\"color: #cc3300;display:none;\">  {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice }} TND {%else %}  {%set totalprice = totalprice + arr.marge %} {{totalprice }} TND{%endif %}  </h5>
                   
                    {% set array = array | merge([totalprice]) %}
                   
                    
                    
                     {% endif %}
                     {% endfor %}
                      <br/>      
               <h5 class=\"card-title\" style=\"color: #cc3300;margin-top: -1.5rem;\">{%if array|length >0 %} {{min(array)}} TND{%endif%}</h5>
                       
                    {% endif %}
                    {% endfor %}
               <div class=\"btnlisthotel w-100\"> 

                          <a href=\"{{ path('chambres', {'id': h.id,'marcheid':marcher.id,'dated':\"now\"|date('d-m-Y'),'datef': \"now\"|date_modify(\"+1 day\")|date('d-m-Y')}) }}\" class=\"btn btn-primary \">Choisir Chambres</a>

                        </div> 

                          <div class=\"btnlisthotel2 w-100\">

                          <a id=\"a\" href=\"{{ path('reserver', {'id': h.id,'marcheid':marcher.id,'dated':\"now\"|date('d-m-Y'),'datef': \"now\"|date_modify(\"+1 day\")|date('d-m-Y')}) }}\" class=\"btn btn-primary \">Réserver</a>


                          </div>  

                     </div>

                    <table class=\"table text-center table-bordered \" style=\"margin:1rem;margin-top: -1rem;\">

                       <thead>
                        <tr>
                          <th scope=\"col-1\" >Num</th> 
                          <th scope=\"col-2\" >Type de chambres</th>
                          <th scope=\"col-2\" >Arrangement</th>
                          <th scope=\"col-1\" >Nb</th>
                          <th scope=\"col-1\" >Adultes</th>
                          <th scope=\"col-1\" >Enfants</th>
                          <th scope=\"col-2\">Disponiblité</th>
                          <th scope=\"col-2\">Prix pour une nuitée(s)</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        
                           <td scope=\"row\" data-label=\"num\">1</td>
                           
                            <td scope=\"row\" data-label=\"Type de chambres\">Chambre Double</td>

                         {% for c in contrats %}
                      {% if c.hotel == h %}
                    {%if c.pricearr|length > 0 %}                
   
                    
                 {%for arr in c.pricearr%}
                       {%if loop.first %}   
                       <td data-label=\"Arrangement\"> 
                         
 
                         <select name=\"arr\" id=\"monselect{{h.id}}\" class=\"form-control formarrang\" style=\"font-size:14px;\">
                         {%for arr in c.pricearr%}
                               {%if arr.etat == 1%}
                        <option id=\"opt_{{arr.id}}\" value=\"{{arr.id}}\">
                            {{arr.hotelarrangement.arrangement.name}}
                        </option>
                        {%endif%}
                         {%endfor%}
                        </select>

                        </td>
                        {%endif%}
                            {%endfor%}
                   
                            <script type=\"text/javascript\">
    
                                \$(document).ready(function(){
                                
                                \$(\"#monselect{{h.id}}\").change(function() {
                                 
                                 {%for i in c.pricearr%} 
                                \t\tif (\$(\"#monselect{{h.id}} :selected\").val() == \$(\"#opt_\"+{{i.id}}).val())
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#monselect{{h.id}} :selected\").val();
                                \t    \tvar total = '#total'+\$(\"#monselect{{h.id}} :selected\").val();
                                \t\t\t\$(comp).show('slow');
                                \t\t\t\$(total).show('slow');
                                \t\t}
                                \t\telse
                                \t\t{
                                \t\t\tvar comp = '#comp_'+\$(\"#opt_\"+{{i.id}}).val();
                                \t\t\tvar total = '#total'+\$(\"#opt_\"+{{i.id}}).val();
                                \t\t\t\$(comp).hide();
                                \t\t\t\$(total).hide();
                                \t\t}
                              
                                \t{%endfor%}\t\t
                                 
                                \t});
                                   })  

                                </script> 
                    {%else%}
                     <td data-label=\"Arrangement\"><span class=\"label  ptip_ne\" style=\"    background-color: #cc3300;
                    color: #fff;
                    padding: 3px 12px 4px 15px;
                    border-radius: 4px;
                    font-size: 12px !important;
                    height: 22px;\" > Aucun arrangement ajouté </span> </td>
                                      {%endif%} {%endif%}
                                    {%endfor%}
                            <td data-label=\"Nb\">2</td>
                       <td data-label=\"Adultes\">2</td>
                        
                          <td data-label=\"Enfants\">0</td>
                          <td data-label=\"Disponiblité\"><span class=\"label  ptip_ne\" style=\"    background-color: #28a745;
                            color: #fff;
                            padding: 3px 12px 4px 15px;
                            border-radius: 4px;
                            font-size: 12px !important;
                            height: 22px;\" > Disponible </span></td>
                          <td data-label=\"Total\"> 
        
             {% for c in contrats %}
                      {% if c.hotel == h %}
                        
               
                  {% set array = {}%}
                        {% set j = 0%}
                     {%for arr in c.pricearr%}
                    
                
                     {%set totalprice = arr.price %}
              {% set array = {
                        key : totalprice,
                    }
                    %}
                     {%set totalprice = arr.price %}
                     {%if j==0 and arr.price == min(array) and  arr.etat==1%}
                     <span class=\"label label-warning ptip_ne\"  id=\"comp_{{arr.id}}\" >  
          {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice *2}} TND {%else %} {{(totalprice + arr.marge) * 2 }} TND{%endif %}  
                  </span>
          
          {% set j = 1%}
                  {%else%}
                        <span class=\"label label-warning ptip_ne\" style=\"display:none;\" id=\"comp_{{arr.id}}\" >  
          {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice *2}} TND {%else %} {{(totalprice + arr.marge) * 2 }} TND{%endif %}  
                  </span>
                  {% endif %}
                 
                     {% endfor %}
                      {#{{c.price * mincapacity }} TND #}
                       
                        {% endif %}
                    {% endfor %}
                    </td >
                          </tr>
                          <tr class=\"disp\">
                         <td> </td>
                         <td></td>
                         <td style=\"text-align:right;\"> <strong> TOTAL : </strong></td> 
                         <td>2</td>
                          <td>2</td>
                           {#<td>{{mincapacity}}</td>#}
                          {#<td>{{mincapacity}}</td>#}
                           <td>0</td>
                          <td></td>
                            {#  #}
                          <td>  {% for c in contrats %}
                      {% if c.hotel == h %}
                        {% set array = {}%}
                       {% set j = 0%}
                     {%for arr in c.pricearr%}
                    
                
                     {%set totalprice = arr.price %}
              {% set array = {
                        key : totalprice,
                    }
                    %}
                     {%set totalprice = arr.price %}
                     {%if j==0 and arr.price == min(array) and arr.etat==1%}
         <span class=\"label label-warning ptip_ne\"  id=\"total{{arr.id}}\">  {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice * 2}} TND {%else %} {{(totalprice + arr.marge) * 2}} TND{%endif %}  </span>
                  {% set j = 1%}
                   {% else %}
         <span class=\"label label-warning ptip_ne\"  style=\"display:none;\" id=\"total{{arr.id}}\">  {% if arr.persm == 1 %}  {%set totalprice = totalprice + (arr.price * arr.marge)/100  %} {{totalprice * 2}} TND {%else %} {{(totalprice + arr.marge) * 2}} TND{%endif %}  </span>

                    {%endif%}
                     {% endfor %}
                        {#<span class=\"label label-warning ptip_ne\" >{{c.price * mincapacity }} TND </span>   #}
                        {% endif %}
                    {% endfor %}</td>
                          </tr>
                          </tr>
                        
                      </tbody>
                    
                    </table>

                  <div class=\"tour-booking\">

                    <a href=\"{{ path('reserver', {'id': h.id,'marcheid':marcher.id,'dated':\"now\"|date('d-m-Y'),'datef': \"now\"|date_modify(\"+1 day\")|date('d-m-Y')}) }}\">Rèserver</a>

                  </div>

                </div>

                

              </div>

            </div>
            {% endfor %}
          </div>



            



        </div>













































  <div class=\"pagination\">

  {{ knp_pagination_render(hotels) }}

</div>   

               

                <br/><br/>
            </div>
            
        </div>
               


     </div>

        </div>
    <div>

    </div>
  </div>

</div>



  



{% endblock %}", "default/listehotels.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\listehotels.html.twig");
    }
}
