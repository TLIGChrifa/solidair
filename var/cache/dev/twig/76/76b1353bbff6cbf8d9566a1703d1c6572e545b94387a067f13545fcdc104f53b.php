<?php

/* default/voyagesorganiees.html.twig */
class __TwigTemplate_e0acd6ccf6523364ee275c3ac77eb63eb28f2048b7fa2c7278f3a00704b65b00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/voyagesorganiees.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34408e7aaf5f528746da36a815111656d0838b17429f759f508c74c5a7c03ebc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34408e7aaf5f528746da36a815111656d0838b17429f759f508c74c5a7c03ebc->enter($__internal_34408e7aaf5f528746da36a815111656d0838b17429f759f508c74c5a7c03ebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/voyagesorganiees.html.twig"));

        $__internal_00ec075e508ca65801f9547d2625bea43dd8baf679b67aeeb760fb1638bf1638 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00ec075e508ca65801f9547d2625bea43dd8baf679b67aeeb760fb1638bf1638->enter($__internal_00ec075e508ca65801f9547d2625bea43dd8baf679b67aeeb760fb1638bf1638_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/voyagesorganiees.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_34408e7aaf5f528746da36a815111656d0838b17429f759f508c74c5a7c03ebc->leave($__internal_34408e7aaf5f528746da36a815111656d0838b17429f759f508c74c5a7c03ebc_prof);

        
        $__internal_00ec075e508ca65801f9547d2625bea43dd8baf679b67aeeb760fb1638bf1638->leave($__internal_00ec075e508ca65801f9547d2625bea43dd8baf679b67aeeb760fb1638bf1638_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_64361ea5f8b15ff3abdac399db52baa15c37e54f084bf8a453f6fa4034859dd9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64361ea5f8b15ff3abdac399db52baa15c37e54f084bf8a453f6fa4034859dd9->enter($__internal_64361ea5f8b15ff3abdac399db52baa15c37e54f084bf8a453f6fa4034859dd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d46fbf180b9731e742794c7526e9ae9b2af16777ee60a19d8045764375db9dcb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d46fbf180b9731e742794c7526e9ae9b2af16777ee60a19d8045764375db9dcb->enter($__internal_d46fbf180b9731e742794c7526e9ae9b2af16777ee60a19d8045764375db9dcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"slider\"> 
  <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\">
          
          
          <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
        
          </div>
  
          <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
        
         </div>
            
          <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
  
          </div>
         
    
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
          <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
          <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Next</span>
        </a>
  
</div>
</div>
<div class=\"VoyagesOrganises container\">
  ";
        // line 36
        $context["i"] = 0;
        // line 37
        echo "  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "        ";
            $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
            // line 39
            echo "        ";
            if ((($context["i"] ?? $this->getContext($context, "i")) == 1)) {
                // line 40
                echo "            <div class=\"alert alert-success\" style=\" color:#fff;\"><center> <i class=\"fa fa-check\"></i> ";
                echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
                echo "</center></div><br/>
       ";
            }
            // line 42
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "  <div class=\"filtreVoyagesOrganises\">
      <div class=\" col-12 titre-filtre\"><h6>Moteur de recherche</h6></div>
      <div class=\"row\" style=\"margin: 1rem;\">
          <div class=\"col-12 col-md-6\">
              <span>Déstination</span>
              <select class=\"custom-select form-control form-control-sm\" >
                ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["dest"] ?? $this->getContext($context, "dest")));
        foreach ($context['_seq'] as $context["key"] => $context["v"]) {
            // line 50
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "villea", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "villea", array()), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                  </select>
          </div>
          <div class=\"col-12 col-md-6\">
              <span>Periode</span>
              <select class=\"custom-select form-control form-control-sm\" >
                ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 58
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo " jours/";
            echo twig_escape_filter($this->env, ($context["i"] - 1), "html", null, true);
            echo " nuits</option>
                 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "                  </select>
          </div>

      </div>

  </div>
  <div class=\"row\">
    ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["vo"] ?? $this->getContext($context, "vo")), 0, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 68
            echo "      <div class=\"col-sm-4\">\t
        <div class=\"view view-ninth\">
          ";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                echo " 
          ";
                // line 71
                if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                    // line 72
                    echo "                <img src=\"https://rusticavoyages.com/rustica/public_html/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
                    echo "\" class=\"grow\">
          ";
                }
                // line 74
                echo "          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "              <h2 class=\"h2-img\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
            echo " </h2>
  <div class=\"mask mask-1\"></div>
  <div class=\"mask mask-2\"></div>
  <div class=\"content\">
    <h2>";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
            echo "</h2>
    <p>
      ";
            // line 81
            $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + $this->getAttribute($context["v"], "marge", array()));
            // line 82
            echo "      À partir de ";
            echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
            echo " TND<br>
      7 jours / 6 Nuits
    </p>
    <a href=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
            echo "\" class=\"info\">Lire plus</a>
  </div>
</div>
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "      

  </div>
</div>
    
  
 
  

";
        
        $__internal_d46fbf180b9731e742794c7526e9ae9b2af16777ee60a19d8045764375db9dcb->leave($__internal_d46fbf180b9731e742794c7526e9ae9b2af16777ee60a19d8045764375db9dcb_prof);

        
        $__internal_64361ea5f8b15ff3abdac399db52baa15c37e54f084bf8a453f6fa4034859dd9->leave($__internal_64361ea5f8b15ff3abdac399db52baa15c37e54f084bf8a453f6fa4034859dd9_prof);

    }

    public function getTemplateName()
    {
        return "default/voyagesorganiees.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 90,  214 => 85,  207 => 82,  205 => 81,  200 => 79,  192 => 75,  186 => 74,  180 => 72,  178 => 71,  172 => 70,  168 => 68,  164 => 67,  155 => 60,  142 => 58,  138 => 57,  131 => 52,  120 => 50,  116 => 49,  108 => 43,  102 => 42,  96 => 40,  93 => 39,  90 => 38,  85 => 37,  83 => 36,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<div class=\"slider\"> 
  <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\">
          
          
          <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
        
          </div>
  
          <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
        
         </div>
            
          <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
  
          </div>
         
    
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
          <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
          <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Next</span>
        </a>
  
</div>
</div>
<div class=\"VoyagesOrganises container\">
  {%set i = 0%}
  {% for flashMessage in app.session.flashbag.get('success') %}
        {%set i = i+1%}
        {% if i==1 %}
            <div class=\"alert alert-success\" style=\" color:#fff;\"><center> <i class=\"fa fa-check\"></i> {{ flashMessage }}</center></div><br/>
       {% endif %}
  {%endfor%}
  <div class=\"filtreVoyagesOrganises\">
      <div class=\" col-12 titre-filtre\"><h6>Moteur de recherche</h6></div>
      <div class=\"row\" style=\"margin: 1rem;\">
          <div class=\"col-12 col-md-6\">
              <span>Déstination</span>
              <select class=\"custom-select form-control form-control-sm\" >
                {% for key,v in dest %}
                <option value=\"{{v.villea}}\">{{v.villea}}</option>
                {% endfor %}
                  </select>
          </div>
          <div class=\"col-12 col-md-6\">
              <span>Periode</span>
              <select class=\"custom-select form-control form-control-sm\" >
                {% for i in 1..20 %}
                <option value=\"{{i}}\">{{i}} jours/{{i-1}} nuits</option>
                 {% endfor %}
                  </select>
          </div>

      </div>

  </div>
  <div class=\"row\">
    {% for v in vo|slice(0, 3) %}
      <div class=\"col-sm-4\">\t
        <div class=\"view view-ninth\">
          {% for item in sejourimg %} 
          {% if item.sejour == v  %}
                <img src=\"https://rusticavoyages.com/rustica/public_html/{{item.image}}\" class=\"grow\">
          {% endif %}
          {% endfor %}
              <h2 class=\"h2-img\">{{v.titre}} </h2>
  <div class=\"mask mask-1\"></div>
  <div class=\"mask mask-2\"></div>
  <div class=\"content\">
    <h2>{{v.titre}}</h2>
    <p>
      {% set total = v.prix +  v.marge %}
      À partir de {{total}} TND<br>
      7 jours / 6 Nuits
    </p>
    <a href=\"{{ path('detailsvo', {'id': v.id}) }}\" class=\"info\">Lire plus</a>
  </div>
</div>
      </div>
    {% endfor %}
      

  </div>
</div>
    
  
 
  

{% endblock %}", "default/voyagesorganiees.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\voyagesorganiees.html.twig");
    }
}
