<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_6e4c772ba155cdc0b267d63a7179c99998ebe95efc980a02d2d13f77355056b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6216da183a0b5595cf267c47212644f20f3e7fae822535b4ca9cb1898661e0db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6216da183a0b5595cf267c47212644f20f3e7fae822535b4ca9cb1898661e0db->enter($__internal_6216da183a0b5595cf267c47212644f20f3e7fae822535b4ca9cb1898661e0db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_e5d3a78dfa84d8449ac951df9bddf8aebc2bc4a0b1644ccaf4a83c8766008cf9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5d3a78dfa84d8449ac951df9bddf8aebc2bc4a0b1644ccaf4a83c8766008cf9->enter($__internal_e5d3a78dfa84d8449ac951df9bddf8aebc2bc4a0b1644ccaf4a83c8766008cf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6216da183a0b5595cf267c47212644f20f3e7fae822535b4ca9cb1898661e0db->leave($__internal_6216da183a0b5595cf267c47212644f20f3e7fae822535b4ca9cb1898661e0db_prof);

        
        $__internal_e5d3a78dfa84d8449ac951df9bddf8aebc2bc4a0b1644ccaf4a83c8766008cf9->leave($__internal_e5d3a78dfa84d8449ac951df9bddf8aebc2bc4a0b1644ccaf4a83c8766008cf9_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_7f2758c19207cc61019f5cfd7926c437e0726f62926f267991dfc7c99c5f0be4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f2758c19207cc61019f5cfd7926c437e0726f62926f267991dfc7c99c5f0be4->enter($__internal_7f2758c19207cc61019f5cfd7926c437e0726f62926f267991dfc7c99c5f0be4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_115e6e557ca3c36750f8a07c7f0aa04bc693617e1d85a4b77ec5006282225d2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_115e6e557ca3c36750f8a07c7f0aa04bc693617e1d85a4b77ec5006282225d2c->enter($__internal_115e6e557ca3c36750f8a07c7f0aa04bc693617e1d85a4b77ec5006282225d2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_115e6e557ca3c36750f8a07c7f0aa04bc693617e1d85a4b77ec5006282225d2c->leave($__internal_115e6e557ca3c36750f8a07c7f0aa04bc693617e1d85a4b77ec5006282225d2c_prof);

        
        $__internal_7f2758c19207cc61019f5cfd7926c437e0726f62926f267991dfc7c99c5f0be4->leave($__internal_7f2758c19207cc61019f5cfd7926c437e0726f62926f267991dfc7c99c5f0be4_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_66bd33a1d03b70e4ad2deb80a01d99663495df58721e40efe4dad9b515141440 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66bd33a1d03b70e4ad2deb80a01d99663495df58721e40efe4dad9b515141440->enter($__internal_66bd33a1d03b70e4ad2deb80a01d99663495df58721e40efe4dad9b515141440_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_eb85cbbfd31fa129d3186280b783032db5d556239774b4ec08fa054c81287efd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb85cbbfd31fa129d3186280b783032db5d556239774b4ec08fa054c81287efd->enter($__internal_eb85cbbfd31fa129d3186280b783032db5d556239774b4ec08fa054c81287efd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_eb85cbbfd31fa129d3186280b783032db5d556239774b4ec08fa054c81287efd->leave($__internal_eb85cbbfd31fa129d3186280b783032db5d556239774b4ec08fa054c81287efd_prof);

        
        $__internal_66bd33a1d03b70e4ad2deb80a01d99663495df58721e40efe4dad9b515141440->leave($__internal_66bd33a1d03b70e4ad2deb80a01d99663495df58721e40efe4dad9b515141440_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ba34ef4960015d4ee2f762ee406d29472fcf37dd2556407af9e71de2c338c169 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba34ef4960015d4ee2f762ee406d29472fcf37dd2556407af9e71de2c338c169->enter($__internal_ba34ef4960015d4ee2f762ee406d29472fcf37dd2556407af9e71de2c338c169_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_da4a904afd81eb309f663cebf22c15aeabe0a022ddfe0765021c25710d89fa3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da4a904afd81eb309f663cebf22c15aeabe0a022ddfe0765021c25710d89fa3c->enter($__internal_da4a904afd81eb309f663cebf22c15aeabe0a022ddfe0765021c25710d89fa3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_da4a904afd81eb309f663cebf22c15aeabe0a022ddfe0765021c25710d89fa3c->leave($__internal_da4a904afd81eb309f663cebf22c15aeabe0a022ddfe0765021c25710d89fa3c_prof);

        
        $__internal_ba34ef4960015d4ee2f762ee406d29472fcf37dd2556407af9e71de2c338c169->leave($__internal_ba34ef4960015d4ee2f762ee406d29472fcf37dd2556407af9e71de2c338c169_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
