<?php

/* base.html.twig */
class __TwigTemplate_7bfa9f7329cec7eed73720380afca8e2bc52a68a02684c27dd4abcc7d16dd9df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa9086dd5a8a75e5878a946a6664858525b655ce8f493c81c356a62567d489b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa9086dd5a8a75e5878a946a6664858525b655ce8f493c81c356a62567d489b6->enter($__internal_aa9086dd5a8a75e5878a946a6664858525b655ce8f493c81c356a62567d489b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_60625c147c64f70c34fd349629c1f82b0dea88219a59a0fb87421418e6e7a34c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60625c147c64f70c34fd349629c1f82b0dea88219a59a0fb87421418e6e7a34c->enter($__internal_60625c147c64f70c34fd349629c1f82b0dea88219a59a0fb87421418e6e7a34c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,shrink-to-fit=no\"> 
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "       

        
        <div class=\"upper-bar\">
          <div class=\"container\">
              <div class=\"row\">
                  <div class=\"col-lg col-md-6 col-sm-12 \">
                    <i class=\"fa fa-phone\" aria-hidden=\"true\"></i> 72 238 880  
                    <i class=\"fa fa-envelope\" aria-hidden=\"true\"> </i><a href=\"mailto:\">  reservation@solidair.com</a>
                  </div>
                  <div class=\"col-lg col-md-6 col-sm-4 text-md-right reseaux\" >
                    <a href=\"https://www.facebook.com/Solidair.tn/\" target=\"_blank\"><i class=\"fab fa-facebook-f\" style=\"padding: 0.3rem 0.45rem 0.3rem 0.45rem;\"></i></a> 
                    <a href=\"\" target=\"_blank\"> <i class=\"fab fa-instagram \" style=\"padding: 0.3rem 0.3rem 0.299rem 0.3rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-twitter \" style=\"padding: 0.3rem 0.333rem 0.2rem 0.2rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-youtube \" style=\"padding: 0.3rem 0.2rem 0.3rem 0.2rem;\"></i></a>
                  </div>
                  
                </div>
          
          </div>
         
       
        </div>

        <nav class=\"navbar navbar-expand-lg  text-sm-right\">
    
          <a class=\"navbar-brand\" href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
          <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/logo_solidair.png"), "html", null, true);
        echo "\"  style=\"margin-left: 7rem; width: 11rem;\"/>
          </a>
          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-nav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"><i class=\"fas fa-bars\" style=\"color: #fff;margin-top: 4px;\"></i></span>
          </button>
          <div class=\"collapse navbar-collapse animate fadeInTopRight\" id=\"main-nav\">
            <ul class=\"navbar-nav\">
              <li class=\"nav-item \">
                <a class=\"nav-link active\"aria-current=\"page\" href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hotelsentunise");
        echo "\">Hôtels en tunisie<span class=\"sr-only\">(current)</span></a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("voyagesorganisees");
        echo "\">Voyages organisés</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("billettrie");
        echo "\">Billetrie</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Croisières</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("circuit");
        echo "\">Circuits & Excursions </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Location Voitures </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("listemaisondhote");
        echo "\">Maisons d'hôtes</a>
              </li>
              
              
            </ul>
          </div>
              
        </nav>   
    </head>
    <body>
        ";
        // line 77
        $this->displayBlock('body', $context, $blocks);
        // line 78
        echo "
      <div class=\"footer\" >

        <div class=\" container\">
        <img  class=\"logo\"src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/logo_solidair.png"), "html", null, true);
        echo "\"  />
        <div class=\"row\">
          <div class=\"col-md-7 row\">
                <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Inscription Newsletter
            </div>
            <div class=\"text-newsletter\">
                Pour recevoir régulièrement nos meilleures Offres et Promotions
            </div>
            <form >
              <input  placeholder=\"Votre adresse E-mail\" class=\"input-newsletter\">
              <input  style=\"color:#fff\"type=\"submit\" value=\"S'inscrire\" onclick=\"alert('Merci de votre inscription...');\" class=\"submit-newsletter\">
            </form>
            <br>
            
            
          </div>
          <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Navigation
            </div>
            <ul class=\"menu_footer list-unstyled\">
              <li><a href=\"";
        // line 105
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hotelsentunise");
        echo "\"><i class=\"fa fa-angle-right\"></i> Hôtels en Tunisie</a></li>
              <li><a href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("voyagesorganisees");
        echo "\"><i class=\"fa fa-angle-right\"></i> Voyages Organisés</a></li>
              <li><a href=\"";
        // line 107
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("circuit");
        echo "\"><i class=\"fa fa-angle-right\"></i> Circuits &amp; Excursions</a></li>
              <li><a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("billettrie");
        echo "\"><i class=\"fa fa-angle-right\"></i> Billetterie</a></li>
              <li><a href=\"";
        // line 109
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("listemaisondhote");
        echo "\"><i class=\"fa fa-angle-right\"></i> Maisons d'hotes</a></li>
              <li><a href=\"\" ><i class=\"fa fa-angle-right\"></i> Location de voitures</a></li>
              <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> Croisières</a></li>
              <li><a href=\"";
        // line 112
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contact");
        echo "\"><i class=\"fa fa-angle-right\"></i> Contact</a></li>
            </ul>
          </div>
          
          
          </div>
          <div class=\"col-md-4\">
            <div class=\"titre-newsletter\">
              Contacts
            </div>
            
            <div class=\"text-footer\" >
              <img  src=\"";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f1.png"), "html", null, true);
        echo "\" style=\"width:30px\" >
              <span> Contact :
                Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr</span>

            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f2.png"), "html", null, true);
        echo "\" style=\"width:25px\" > Tél :
              (+216) 72 222 887
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f3.png"), "html", null, true);
        echo "\" style=\"width:25px\"> E-mail :
              reservation@solidair.tn
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/f4.png"), "html", null, true);
        echo "\" style=\"width:25px\" > Facebook :
              Solid'air Travel Agency
            </div>
            
            
            
          </div>
        </div>
      

            
      </div>
      </div>
      <div  class=\"footer-end\" style=\"text-align: center; color:#000;  background-color:#BEBEBE\">
          Powered by <a href=\"http://www.digitalgrouperformance.com/public/ \" style=\"color:#000\">
          Digital Group performance</a>
      </div>
        ";
        // line 164
        $this->displayBlock('javascripts', $context, $blocks);
        // line 174
        echo "                
    </body>
</html>














";
        
        $__internal_aa9086dd5a8a75e5878a946a6664858525b655ce8f493c81c356a62567d489b6->leave($__internal_aa9086dd5a8a75e5878a946a6664858525b655ce8f493c81c356a62567d489b6_prof);

        
        $__internal_60625c147c64f70c34fd349629c1f82b0dea88219a59a0fb87421418e6e7a34c->leave($__internal_60625c147c64f70c34fd349629c1f82b0dea88219a59a0fb87421418e6e7a34c_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_6169cb0de21041ea6717bb8e05241fb4763be62efe251ed917297284d4dfa86e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6169cb0de21041ea6717bb8e05241fb4763be62efe251ed917297284d4dfa86e->enter($__internal_6169cb0de21041ea6717bb8e05241fb4763be62efe251ed917297284d4dfa86e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_64fbd9680a4db1a2aa1524082d270654a75f86de79701ee68c678bbf724ff62b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64fbd9680a4db1a2aa1524082d270654a75f86de79701ee68c678bbf724ff62b->enter($__internal_64fbd9680a4db1a2aa1524082d270654a75f86de79701ee68c678bbf724ff62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Solid'air";
        
        $__internal_64fbd9680a4db1a2aa1524082d270654a75f86de79701ee68c678bbf724ff62b->leave($__internal_64fbd9680a4db1a2aa1524082d270654a75f86de79701ee68c678bbf724ff62b_prof);

        
        $__internal_6169cb0de21041ea6717bb8e05241fb4763be62efe251ed917297284d4dfa86e->leave($__internal_6169cb0de21041ea6717bb8e05241fb4763be62efe251ed917297284d4dfa86e_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_2bf44c5702bb6b530a636700fadc53e90264bbc9e9c97c2fdaef4b3889be2365 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bf44c5702bb6b530a636700fadc53e90264bbc9e9c97c2fdaef4b3889be2365->enter($__internal_2bf44c5702bb6b530a636700fadc53e90264bbc9e9c97c2fdaef4b3889be2365_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_74fef1b911603f6cd5b4c78faa741f34097b2a01f4e351721979e919c4fcfebc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74fef1b911603f6cd5b4c78faa741f34097b2a01f4e351721979e919c4fcfebc->enter($__internal_74fef1b911603f6cd5b4c78faa741f34097b2a01f4e351721979e919c4fcfebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "        <link rel=\"icon\" type=\"image/png\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/icone.png"), "html", null, true);
        echo "\" />
      \t<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/main.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t      <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/fontawesome-free-5.15.3-web/css/all.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
      \t<link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        ";
        
        $__internal_74fef1b911603f6cd5b4c78faa741f34097b2a01f4e351721979e919c4fcfebc->leave($__internal_74fef1b911603f6cd5b4c78faa741f34097b2a01f4e351721979e919c4fcfebc_prof);

        
        $__internal_2bf44c5702bb6b530a636700fadc53e90264bbc9e9c97c2fdaef4b3889be2365->leave($__internal_2bf44c5702bb6b530a636700fadc53e90264bbc9e9c97c2fdaef4b3889be2365_prof);

    }

    // line 77
    public function block_body($context, array $blocks = array())
    {
        $__internal_c7ea48aa9f74d77eaf23950119bd12c397352dbd1b0bef011933e76bce7b690d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7ea48aa9f74d77eaf23950119bd12c397352dbd1b0bef011933e76bce7b690d->enter($__internal_c7ea48aa9f74d77eaf23950119bd12c397352dbd1b0bef011933e76bce7b690d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3479ce4003eda515319867ffc416391aeef2b42a55d765be7ff3ce7f7e2a09be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3479ce4003eda515319867ffc416391aeef2b42a55d765be7ff3ce7f7e2a09be->enter($__internal_3479ce4003eda515319867ffc416391aeef2b42a55d765be7ff3ce7f7e2a09be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_3479ce4003eda515319867ffc416391aeef2b42a55d765be7ff3ce7f7e2a09be->leave($__internal_3479ce4003eda515319867ffc416391aeef2b42a55d765be7ff3ce7f7e2a09be_prof);

        
        $__internal_c7ea48aa9f74d77eaf23950119bd12c397352dbd1b0bef011933e76bce7b690d->leave($__internal_c7ea48aa9f74d77eaf23950119bd12c397352dbd1b0bef011933e76bce7b690d_prof);

    }

    // line 164
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7ab6abf9d7f5691d0a992e4ec21c5828ffe54833448a15b0300d8049b6a2530b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ab6abf9d7f5691d0a992e4ec21c5828ffe54833448a15b0300d8049b6a2530b->enter($__internal_7ab6abf9d7f5691d0a992e4ec21c5828ffe54833448a15b0300d8049b6a2530b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_20bdb4f787a11388bc16b6f8e2301ab422a4ef77ce4027ca497551cfb981881b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20bdb4f787a11388bc16b6f8e2301ab422a4ef77ce4027ca497551cfb981881b->enter($__internal_20bdb4f787a11388bc16b6f8e2301ab422a4ef77ce4027ca497551cfb981881b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 165
        echo "            ";
        if ((((((((((($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "homepage") && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reserver")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "chambre")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "hotelsentunise")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "recherchehotel")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "rechercheht")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservermaisondhote")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "chambres")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "billettrie")) && ($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservercircuit"))) {
            // line 166
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-3.2.1.js"), "html", null, true);
            echo "\"></script> 
            ";
        }
        // line 168
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            ";
        // line 169
        if (($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") != "reservercircuit")) {
            // line 170
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/main.js"), "html", null, true);
            echo "\"></script>
            ";
        }
        // line 172
        echo "                
        ";
        
        $__internal_20bdb4f787a11388bc16b6f8e2301ab422a4ef77ce4027ca497551cfb981881b->leave($__internal_20bdb4f787a11388bc16b6f8e2301ab422a4ef77ce4027ca497551cfb981881b_prof);

        
        $__internal_7ab6abf9d7f5691d0a992e4ec21c5828ffe54833448a15b0300d8049b6a2530b->leave($__internal_7ab6abf9d7f5691d0a992e4ec21c5828ffe54833448a15b0300d8049b6a2530b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  384 => 172,  378 => 170,  376 => 169,  371 => 168,  365 => 166,  362 => 165,  353 => 164,  336 => 77,  324 => 12,  320 => 11,  316 => 10,  312 => 9,  307 => 8,  298 => 7,  280 => 6,  253 => 174,  251 => 164,  231 => 147,  221 => 140,  211 => 133,  199 => 124,  184 => 112,  178 => 109,  174 => 108,  170 => 107,  166 => 106,  162 => 105,  136 => 82,  130 => 78,  128 => 77,  115 => 67,  106 => 61,  97 => 55,  91 => 52,  85 => 49,  74 => 41,  70 => 40,  42 => 14,  40 => 7,  36 => 6,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,shrink-to-fit=no\"> 
        <title>{% block title %}Solid'air{% endblock %}</title>
        {% block stylesheets %}
        <link rel=\"icon\" type=\"image/png\" href=\"{{asset('assets/image/icone.png')}}\" />
      \t<link href=\"{{asset('assets/css/main.css')}}\" rel=\"stylesheet\">
        <link href=\"{{asset('assets/css/responsive.css')}}\" rel=\"stylesheet\">
\t      <link href=\"{{asset('assets/fontawesome-free-5.15.3-web/css/all.css')}}\" rel=\"stylesheet\">
      \t<link href=\"{{asset('assets/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
        {% endblock %}
       

        
        <div class=\"upper-bar\">
          <div class=\"container\">
              <div class=\"row\">
                  <div class=\"col-lg col-md-6 col-sm-12 \">
                    <i class=\"fa fa-phone\" aria-hidden=\"true\"></i> 72 238 880  
                    <i class=\"fa fa-envelope\" aria-hidden=\"true\"> </i><a href=\"mailto:\">  reservation@solidair.com</a>
                  </div>
                  <div class=\"col-lg col-md-6 col-sm-4 text-md-right reseaux\" >
                    <a href=\"https://www.facebook.com/Solidair.tn/\" target=\"_blank\"><i class=\"fab fa-facebook-f\" style=\"padding: 0.3rem 0.45rem 0.3rem 0.45rem;\"></i></a> 
                    <a href=\"\" target=\"_blank\"> <i class=\"fab fa-instagram \" style=\"padding: 0.3rem 0.3rem 0.299rem 0.3rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-twitter \" style=\"padding: 0.3rem 0.333rem 0.2rem 0.2rem;\"></i></a>
                    <a href=\"\"target=\"_blank\"><i class=\"fab fa-youtube \" style=\"padding: 0.3rem 0.2rem 0.3rem 0.2rem;\"></i></a>
                  </div>
                  
                </div>
          
          </div>
         
       
        </div>

        <nav class=\"navbar navbar-expand-lg  text-sm-right\">
    
          <a class=\"navbar-brand\" href=\"{{path('homepage')}}\">
          <img src=\"{{asset('assets/image/logo_solidair.png')}}\"  style=\"margin-left: 7rem; width: 11rem;\"/>
          </a>
          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-nav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <span class=\"navbar-toggler-icon\"><i class=\"fas fa-bars\" style=\"color: #fff;margin-top: 4px;\"></i></span>
          </button>
          <div class=\"collapse navbar-collapse animate fadeInTopRight\" id=\"main-nav\">
            <ul class=\"navbar-nav\">
              <li class=\"nav-item \">
                <a class=\"nav-link active\"aria-current=\"page\" href=\"{{path('hotelsentunise')}}\">Hôtels en tunisie<span class=\"sr-only\">(current)</span></a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"{{path('voyagesorganisees')}}\">Voyages organisés</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link\" href=\"{{path('billettrie')}}\">Billetrie</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Croisières</a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"{{path('circuit')}}\">Circuits & Excursions </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"\">Location Voitures </a>
              </li>
              <li class=\"nav-item\">
                <a class=\"nav-link \" href=\"{{path('listemaisondhote')}}\">Maisons d'hôtes</a>
              </li>
              
              
            </ul>
          </div>
              
        </nav>   
    </head>
    <body>
        {% block body %}{% endblock %}

      <div class=\"footer\" >

        <div class=\" container\">
        <img  class=\"logo\"src=\"{{asset('assets/image/logo_solidair.png')}}\"  />
        <div class=\"row\">
          <div class=\"col-md-7 row\">
                <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Inscription Newsletter
            </div>
            <div class=\"text-newsletter\">
                Pour recevoir régulièrement nos meilleures Offres et Promotions
            </div>
            <form >
              <input  placeholder=\"Votre adresse E-mail\" class=\"input-newsletter\">
              <input  style=\"color:#fff\"type=\"submit\" value=\"S'inscrire\" onclick=\"alert('Merci de votre inscription...');\" class=\"submit-newsletter\">
            </form>
            <br>
            
            
          </div>
          <div class=\"col-md-6\">
            <div class=\"titre-newsletter\" >
              Navigation
            </div>
            <ul class=\"menu_footer list-unstyled\">
              <li><a href=\"{{path('hotelsentunise')}}\"><i class=\"fa fa-angle-right\"></i> Hôtels en Tunisie</a></li>
              <li><a href=\"{{path('voyagesorganisees')}}\"><i class=\"fa fa-angle-right\"></i> Voyages Organisés</a></li>
              <li><a href=\"{{path('circuit')}}\"><i class=\"fa fa-angle-right\"></i> Circuits &amp; Excursions</a></li>
              <li><a href=\"{{path('billettrie')}}\"><i class=\"fa fa-angle-right\"></i> Billetterie</a></li>
              <li><a href=\"{{path('listemaisondhote')}}\"><i class=\"fa fa-angle-right\"></i> Maisons d'hotes</a></li>
              <li><a href=\"\" ><i class=\"fa fa-angle-right\"></i> Location de voitures</a></li>
              <li><a href=\"\"><i class=\"fa fa-angle-right\"></i> Croisières</a></li>
              <li><a href=\"{{path('contact')}}\"><i class=\"fa fa-angle-right\"></i> Contact</a></li>
            </ul>
          </div>
          
          
          </div>
          <div class=\"col-md-4\">
            <div class=\"titre-newsletter\">
              Contacts
            </div>
            
            <div class=\"text-footer\" >
              <img  src=\"{{asset('assets/image/f1.png')}}\" style=\"width:30px\" >
              <span> Contact :
                Vente de billets, croisières, séjours et hôtels dans le monde, location de voitures et Omr</span>

            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f2.png')}}\" style=\"width:25px\" > Tél :
              (+216) 72 222 887
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f3.png')}}\" style=\"width:25px\"> E-mail :
              reservation@solidair.tn
            </div>
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"text-footer\">
              <img  src=\"{{asset('assets/image/f4.png')}}\" style=\"width:25px\" > Facebook :
              Solid'air Travel Agency
            </div>
            
            
            
          </div>
        </div>
      

            
      </div>
      </div>
      <div  class=\"footer-end\" style=\"text-align: center; color:#000;  background-color:#BEBEBE\">
          Powered by <a href=\"http://www.digitalgrouperformance.com/public/ \" style=\"color:#000\">
          Digital Group performance</a>
      </div>
        {% block javascripts %}
            {% if  app.request.attributes.get('_route') != 'homepage' and app.request.attributes.get('_route') != 'reserver' and app.request.attributes.get('_route') != 'chambre' and app.request.attributes.get('_route') != 'hotelsentunise' and  app.request.attributes.get('_route') != 'recherchehotel' and  app.request.attributes.get('_route') != 'rechercheht' and  app.request.attributes.get('_route') != 'reservermaisondhote' and   app.request.attributes.get('_route') != 'chambres' and app.request.attributes.get('_route') != 'billettrie' and app.request.attributes.get('_route') != 'reservercircuit' %}
            <script src=\"{{asset('assets/js/jquery-3.2.1.js')}}\"></script> 
            {% endif %}
            <script src=\"{{asset('assets/js/bootstrap.min.js')}}\"></script>
            {% if app.request.attributes.get('_route') != 'reservercircuit' %}
            <script src=\"{{asset('assets/js/main.js')}}\"></script>
            {% endif %}
                
        {% endblock %}
                
    </body>
</html>














", "base.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\base.html.twig");
    }
}
