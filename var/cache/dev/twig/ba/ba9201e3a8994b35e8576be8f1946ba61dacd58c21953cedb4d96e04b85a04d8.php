<?php

/* default/coordonnees.html.twig */
class __TwigTemplate_24a81bb7bc413ee9269fd9fc80e17d4371cf16b97307e3f1d86c44ac43a702dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/coordonnees.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7fa0355553a8b8503ede71da8b3f9a2e212ac811e53874bf597fe1a4294045c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7fa0355553a8b8503ede71da8b3f9a2e212ac811e53874bf597fe1a4294045c->enter($__internal_e7fa0355553a8b8503ede71da8b3f9a2e212ac811e53874bf597fe1a4294045c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/coordonnees.html.twig"));

        $__internal_2025e216242fd315fc84a7052d5f29257537074c081a806a14d621c186e863cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2025e216242fd315fc84a7052d5f29257537074c081a806a14d621c186e863cc->enter($__internal_2025e216242fd315fc84a7052d5f29257537074c081a806a14d621c186e863cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/coordonnees.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e7fa0355553a8b8503ede71da8b3f9a2e212ac811e53874bf597fe1a4294045c->leave($__internal_e7fa0355553a8b8503ede71da8b3f9a2e212ac811e53874bf597fe1a4294045c_prof);

        
        $__internal_2025e216242fd315fc84a7052d5f29257537074c081a806a14d621c186e863cc->leave($__internal_2025e216242fd315fc84a7052d5f29257537074c081a806a14d621c186e863cc_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_9d11d9d97c2bd3db47e743d7978b9774abc597808e48e1ffca118f3f8beb0d9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d11d9d97c2bd3db47e743d7978b9774abc597808e48e1ffca118f3f8beb0d9e->enter($__internal_9d11d9d97c2bd3db47e743d7978b9774abc597808e48e1ffca118f3f8beb0d9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0e42a171012a56ede097c2ca8f0fdefabd65988d294a59dd7181fbfaa89077d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e42a171012a56ede097c2ca8f0fdefabd65988d294a59dd7181fbfaa89077d3->enter($__internal_0e42a171012a56ede097c2ca8f0fdefabd65988d294a59dd7181fbfaa89077d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Gestion des hôtels";
        
        $__internal_0e42a171012a56ede097c2ca8f0fdefabd65988d294a59dd7181fbfaa89077d3->leave($__internal_0e42a171012a56ede097c2ca8f0fdefabd65988d294a59dd7181fbfaa89077d3_prof);

        
        $__internal_9d11d9d97c2bd3db47e743d7978b9774abc597808e48e1ffca118f3f8beb0d9e->leave($__internal_9d11d9d97c2bd3db47e743d7978b9774abc597808e48e1ffca118f3f8beb0d9e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_96a2885f53c490aabf07b8b26f5e60ab9bf9cc5e7c3e6dee052603f7b152cffd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96a2885f53c490aabf07b8b26f5e60ab9bf9cc5e7c3e6dee052603f7b152cffd->enter($__internal_96a2885f53c490aabf07b8b26f5e60ab9bf9cc5e7c3e6dee052603f7b152cffd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4f33dde1dc7391b9f0949dcd23587714c40051b1eb67b97db032779ee821e4c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f33dde1dc7391b9f0949dcd23587714c40051b1eb67b97db032779ee821e4c9->enter($__internal_4f33dde1dc7391b9f0949dcd23587714c40051b1eb67b97db032779ee821e4c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<style>

.label-success{
    background: #0093a9;
    color: #fff;
    border-radius: 5px;
    padding: 2px 6px 4px 6px;
    font-size:13px;
}
.box.box-info {
    border-top-color: #0093a9 !important;
}
 .w-box-header {
     background: #0093a9 !important;
 }
 .table-bordered td, .table-bordered th{
     font-size:14px;
 }
 

@import url('https://fonts.googleapis.com/css?family=Arimo:400,700&display=swap');

h2{
  color:#000;
  text-align:center;
  font-size:2em;
}
.warpper{
  display:flex;
  flex-direction: column;
  align-items: center;
}
.tab{
cursor: pointer;
    padding: 10px 20px;
    margin: 0px 2px;
    background: #1e4b22;
    display: inline-block;
    color: #fff;
    border-radius: 3px 3px 0px 0px;
    box-shadow: 0 0.5rem 0.8rem #00000080;
}
.panels{
  background:#fffffff6;
  box-shadow: 0 2rem 2rem #00000080;
  min-height:200px;
  width:100%;
  max-width:500px;
  border-radius:3px;
  overflow:hidden;
  padding:20px;  
}
.panel{
  display:none;
  animation: fadein .8s;
}
.label-warning {
    background-color: #ffcc33!important;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;
}
.col-6 {
  
    color: #000;
}
@keyframes fadein {
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
}
.warpper {
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 409px;
}
.panel-title{
  font-size:1.5em;
  font-weight:bold
}
.radio{
  display:none;
}
#one:checked ~ .panels #one-panel,
#two:checked ~ .panels #two-panel,
#three:checked ~ .panels #three-panel{
  display:block
}
#one:checked ~ .tabs #one-tab,
#two:checked ~ .tabs #two-tab,
#three:checked ~ .tabs #three-tab{
  background:#fffffff6;
  color:#000;
  border-top: 3px solid #000;
}

.panels {
    background: #f5f5f5;
    box-shadow: 0 2rem 2rem #00000080;
    min-height: 200px;
    width: 100%;
    max-width: 695px;
    border-radius: 3px;
    overflow: hidden;
    padding: 42px;
    font-size: 14px;
}

.panel-title{
    color:#0093a9;
}
#three:checked ~ .tabs #three-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
#two:checked ~ .tabs #two-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
#one:checked ~ .tabs #one-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
</style>

   





 

 <div class=\"tarifReservation container\">
    <div class=\"detailsBox\">
<h5  >  Détails de la réservation</h5>
    </div>
 <div class=\"back \" style=\"margin-bottom: 1rem;\">
  <div class=\" row\">
    <div class=\"col-md-4\">
      ";
        // line 154
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hotelsimg"] ?? $this->getContext($context, "hotelsimg")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 155
            echo "     ";
            if ($this->getAttribute($context["loop"], "first", array())) {
                // line 156
                echo "      <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["i"], "file", array())), "html", null, true);
                echo "\" class=\"w-100\"  />
      ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo " 
    </div>

    
    <div class=\"col-md-7\">
      <h4 style=\"color: rgb(204, 51, 0);\">";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "hotel", array()), "name", array()), "html", null, true);
        echo "</h4>
                               ";
        // line 164
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "hotel", array()), "star", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 165
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoil.png"), "html", null, true);
            echo "\" class=\"\" alt=\"...\">
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 167
        echo "
     <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px\">";
        // line 169
        echo twig_slice($this->env, $this->getAttribute($this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "hotel", array()), "shortdesc", array()), 0, 500);
        echo " ..</h6> <br/>
    
    <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px;;font-weight:bold;\"> Du : ";
        // line 172
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "datedebut", array()), "d/m/Y"), "html", null, true);
        echo " <h6/>
    <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px;;font-weight:bold;\"> Au :  ";
        // line 174
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "datefin", array()), "d/m/Y"), "html", null, true);
        echo "</h6>
    </div>
    

  </div>


 </div>
 <h5 style=\"text-decoration:underline;\">Récapitulation :</h5>
      <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >

                                <thead>
                                    <tr style=\"background-color: #dee2e460;\">
                                        ";
        // line 188
        echo "                                        ";
        // line 189
        echo "                                        <th> Date de début </th>
                                        <th> Date de fin</th>
                                        <th> Hôtel </th>
                        
                                        <th>Nb.chambres </th>
                                      
                                    </tr>
                                    </thead>
\t\t\t\t\t\t\t\t<tbody>
                            
                                        <tr style=\"background-color:#fff\">
                                           
                                            <td>";
        // line 201
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "datedebut", array()), "d/m/Y"), "html", null, true);
        echo "</td>
                                            <td>";
        // line 202
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "datefin", array()), "d/m/Y"), "html", null, true);
        echo "</td>
                                            <td>";
        // line 203
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "hotel", array()), "name", array()), "html", null, true);
        echo "</td>
                                            <td >";
        // line 204
        echo twig_escape_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "nbchambres", array()), "html", null, true);
        echo "</td>
                       
                                        </tr>
                                    </tbody>


                    </table>

             <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                        ";
        // line 216
        echo "                                        ";
        // line 217
        echo "                                        <th> Type </th>
                                        <th> Nbre des adultes</th>
                                        <th> Nbre des enfants </th>
                                        <th> Âges des enfants</th>
                                        <th> Arrangements </th>
                                         <th> Suppléments </th>
                                        ";
        // line 225
        echo "                                    </tr>
                                    </thead>
\t\t\t\t\t\t\t\t  <tbody >
                            
                                     ";
        // line 229
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["chambres"] ?? $this->getContext($context, "chambres")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 230
            echo "                                        <tr style=\"background-color: #fff;\">
                                           
                                            ";
            // line 233
            echo "                                             ";
            // line 234
            echo "                                            <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "type", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 235
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nbadultes", array()), "html", null, true);
            echo "</td>
                                            <td>";
            // line 236
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nbenfants", array()), "html", null, true);
            echo "</td>
                                            <td>
                                                 ";
            // line 238
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($context["c"], "ageenfant", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                // line 239
                echo "                                                  <label class=\"label label-success\">  ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "ageenfant", array()), $context["key"], array(), "array"), "html", null, true);
                echo " ans</label>
                                                
                            
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 243
            echo "                                            </td>
                                            <td ><label class=\"label label-warning\">";
            // line 244
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "arrangement", array()), "html", null, true);
            echo "</label></td>
                                             <td>
                                                    ";
            // line 246
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter($this->getAttribute($context["c"], "supplements", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
                // line 247
                echo "                                                  <label class=\"label label-success\">  ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "supplements", array()), $context["key"], array(), "array"), "html", null, true);
                echo " </label><br/>
                                                
                            
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 251
            echo "                                                  </td> 
                                            
                                          
                                       
                                        </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 257
        echo "                                    </tbody>
            </table>
                                                   ";
        // line 260
        echo "                                                   ";
        // line 261
        echo "    <table border=\"0\" width=\"100%\" >
\t<tr>
\t\t<td width=\"50%\">
\t\t    
      <table class=\"table  \" style=\"margin-top:1rem;display:none;  \" >
\t\t\t    <tr>
\t\t\t        <td width=\"28%\" align=\"center\"><strong></strong></td>
\t\t\t        <td width=\"40%\" align=\"center\"><strong></strong></td>
\t\t\t        <td width=\"32%\" align=\"center\"><strong></strong></td>
\t\t\t    </tr>
\t\t\t 
\t\t\t    <tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t
            </table>    
\t\t</td>
\t\t<td width=\"50%\" >
      <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; background: #dee2e460\" >
\t\t
\t\t\t\t<tr  >
\t\t\t\t\t<td width=\"50%\"  align=\"center\">
\t\t\t\t\t\t<strong>Montant à payer</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"50%\" >
\t\t\t\t\t\t<strong style=\"color :rgb(204, 51, 0)\"> 
\t                     ";
        // line 299
        echo "\t\t\t\t\t\t ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "total", array()), "html", null, true);
        echo " TND

\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t</table>
       </td>
       </tr>
       </table>
       
 </div></div>      
      ";
        // line 311
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo " 

  

 <div class=\" container \" >
  <div class=\" detailsBox\">
    <h5> Informations sur le client</h5>
    </div>
        <div class=\"row\" style=\"font-size:14px;\">
            <div class=\"col col-6\" >
                ";
        // line 322
        echo "           
                    <div class=\"\"  >
                          C.I.N / Passeport * : :
                      ";
        // line 325
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "cin", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo "
                         Civilité :
                      ";
        // line 327
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "civilite", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;font-size:12px; padding: 2px;")));
        echo "
                      Nom :
                      ";
        // line 329
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo "
                      Prénom :
                       ";
        // line 331
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     
                      Email :
                       ";
        // line 334
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     
                    
                    
                
                  </div>
            </div>
            <div class=\"col col-6\" >
                ";
        // line 343
        echo "               
                  
                    <div class=\"form-group\">
                   
                     Téléphone :
                       ";
        // line 348
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "mobile", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                       
                     Adresse :
                      ";
        // line 351
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "adulte", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                      Code postale :
                      ";
        // line 353
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "adultes", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                      Nom de ville : 
                      ";
        // line 355
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "demandes", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                        Pays :
                      ";
        // line 357
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "pays", array()), 'widget', array("attr" => array("class" => "form-control", "style" => "border-radius: 2px !important;height: 25px !important;")));
        echo " 
                     
                     
                
                    
                         
            </div>
           
        </div>
       
    </div>

 

</div>

 
</div></div>


  

 <div class=\" container \" >
 <div class=\"detailsBox\">
    <h5>Liste des personnes / Rooming list</h5>
    </div>
  ";
        // line 383
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, twig_length_filter($this->env, ($context["chambres"] ?? $this->getContext($context, "chambres")))));
        foreach ($context['_seq'] as $context["index"] => $context["c"]) {
            // line 384
            echo " <table class=\"table text-center table-bordered \"  >
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                       
                                        <th> Chambre N° ";
            // line 388
            echo twig_escape_filter($this->env, ($context["index"] + 1), "html", null, true);
            echo "</th>
                                        <th> Noms des occupants</th>
                                        <th> Âges </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                       
                                         ";
            // line 397
            echo "                                        ";
            // line 398
            echo "                                        ";
            // line 399
            echo "                                        ";
            // line 400
            echo "                                         ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["chambres"] ?? $this->getContext($context, "chambres")));
            foreach ($context['_seq'] as $context["key"] => $context["c"]) {
                // line 401
                echo "                                         ";
                if (($context["key"] == $context["index"])) {
                    // line 402
                    echo "                                        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($context["c"], "nbadultes", array())));
                    foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
                        // line 403
                        echo "                                        <tr style=\"background-color: #fff;\">
                                            <td>Adulte (*) ";
                        // line 404
                        echo twig_escape_filter($this->env, $context["a"], "html", null, true);
                        echo "</td>
                                             <td>   <input type=\"text\" name=\"nomadulte[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\">  <input  type=\"hidden\" name=\"idchambre\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\" value=\"";
                        // line 405
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
                        echo "\"> </td>
                                     
                                        <td> <input  type=\"number\" name=\"ageadulte[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 410
                    echo "                                              ";
                }
                // line 411
                echo "                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 412
            echo "                                         ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["chambres"] ?? $this->getContext($context, "chambres")));
            foreach ($context['_seq'] as $context["keyen"] => $context["c"]) {
                // line 413
                echo "                                         ";
                if (($this->getAttribute($context["c"], "nbenfants", array()) > 0)) {
                    // line 414
                    echo "                                         ";
                    if (($context["keyen"] == $context["index"])) {
                        // line 415
                        echo "                                             ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($context["c"], "nbenfants", array())));
                        foreach ($context['_seq'] as $context["keyen"] => $context["e"]) {
                            // line 416
                            echo "                                        <tr style=\"background-color: #fff;\">
                                              <td>Enfant  (*) ";
                            // line 417
                            echo twig_escape_filter($this->env, $context["e"], "html", null, true);
                            echo "</td>
                                             <td>  <input  type=\"text\" name=\"nomenf[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"> </td>
                                      
                                        <td> <input  type=\"number\" name=\"ageenf[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['keyen'], $context['e'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 423
                        echo "                                              ";
                    }
                    // line 424
                    echo "                                              ";
                }
                // line 425
                echo "                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keyen'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 426
            echo "                                    
                                    </tbody>



</table>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['index'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 434
        echo "</div></div>



</div>



  


 <div class=\" container \">
 <div class=\"detailsBox\">
    <h5>Recommandations</h5>
    </div>
                                                 <div class=\"row\" style=\"font-size:14px;\">
                                                                    <div class=\"col-md-4\">
                                                                        <div class=\"formSep \">
                                                                            <label class=\"checkbox\" > 
                                                                            <input type=\"checkbox\" name=\"options[]\"
                                                                                                           value=\"Séjour de noces\"/>&nbsp;Séjour de noces </label> 
                                                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"De préférence chambre avec 2 lits\"/>&nbsp;De préférence chambre avec 2 lits
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue piscine\"/>&nbsp;Si possible, chambre vue piscine
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue jardin\"/>&nbsp;Si possible, chambre vue jardin
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ tardif\"/>&nbsp;Si possible, départ tardif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Prière prévoir un lit bébé\"/>&nbsp;Prière prévoir un lit bébé
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour infos, séjour de noces\"/>&nbsp;Pour infos, séjour de noces
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour info, arrivée tardive\"/>&nbsp;Pour info, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ actif\"/>&nbsp;Si possible, départ actif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, grand lit\"/>&nbsp;Si possible, grand lit
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres non fumeur\"/>&nbsp;Si possible, chambres non fumeur
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres côte à côte\"/>&nbsp;Si possible, chambres côte à côte
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres communicantes\"/>&nbsp;Si possible, chambres communicantes
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, arrivée tardive\"/>&nbsp;Si possible, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                            </div>
                         
                            <div class=\"col-md-6\">
                                <div>
                                    <label for=\"recommandations\" class=\"required\"><strong>Vos remarques /
                                            Recommandations :</strong></label>
                                    <textarea id=\"recommandations\" name=\"recommandations\"
                                              class=\"form-control\"></textarea><br/>

                                </div>










</div></div>



</div>

 
</div></div>



     <div class=\"tarifReservation container \" >
<div class=\"detailsBox\">
    <h5>Mode de Paiement
</h5>
    </div>
<div class=\"warpper\">
  <input class=\"radio\" id=\"one\" name=\"group\" type=\"radio\" value=\"espece\" checked >
  <input class=\"radio\" id=\"two\" name=\"group\" type=\"radio\" value=\"Virement\">
  <input class=\"radio\" id=\"three\" name=\"group\" type=\"radio\" value=\"versement\">
  <div class=\"tabs\">
  ";
        // line 590
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actagence", array()) == 1)) {
            // line 591
            echo "  <label class=\"tab\" id=\"one-tab\" for=\"one\">Paiement à l'agence</label>
  ";
        }
        // line 593
        echo "  ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actrib", array()) == 1)) {
            // line 594
            echo "  <label class=\"tab\" id=\"two-tab\" for=\"two\">Virement bancaire</label>
  ";
        }
        // line 596
        echo "    ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actvers", array()) == 1)) {
            // line 597
            echo "  <label class=\"tab\" id=\"three-tab\" for=\"three\">Versement espéces</label>
  ";
        }
        // line 599
        echo "   ";
        if (($this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "actmond", array()) == 1)) {
            // line 600
            echo "  <label class=\"tab\" id=\"for-tab\" for=\"for\">Mondat</label>
  ";
        }
        // line 602
        echo "    </div>
  <div class=\"panels\" >
  <div class=\"panel\" id=\"one-panel\" >
    <div class=\"panel-title\" style=\"color:#0093a9\">Adresses :</div>
    <p style=\"line-height:1.25px;\">
    ";
        // line 607
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescagence", array());
        echo "

    </p>
    ";
        // line 611
        echo "  </div>
  <div class=\"panel\" id=\"two-panel\">
    <div class=\"panel-title\">Virement Bancaire</div>
    <p>";
        // line 614
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescrib", array());
        echo "</p>
  </div>
  <div class=\"panel\" id=\"three-panel\">
    <div class=\"panel-title\">Versement en espéces</div>
    <p>";
        // line 618
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescvers", array());
        echo "</p>
  </div>

    <div class=\"panel\" id=\"for-panel\">
    <div class=\"panel-title\">Mondat</div>
    <p>";
        // line 623
        echo $this->getAttribute(($context["payement"] ?? $this->getContext($context, "payement")), "longdescmond", array());
        echo "</p>
  </div>
  </div>
  
</div>
  
<div class=\"row\" style=\"margin-bottom:3px;\">
      <div class=\"col-md-6\">
   <a href=\"";
        // line 631
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("deletereservationhotel", array("id" => $this->getAttribute(($context["reservation"] ?? $this->getContext($context, "reservation")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-sm my-0\" style=\"background:#d13300;color:#fff;
  border: #0093a9;
  cursor: pointer;
  width: 120px;
  height: 2rem;
  border-radius: 7px;float:right;    margin-right: -347px;
 \"><i class=\"fa fa-trash\"> </i>  Annuler</a> 
 </div> &nbsp;&nbsp;
    <div class=\"col-md-6\">
  <button type=\"submit\"  id=\"btsubmit\" class=\"btn btn-sm my-0\" style=\"background-color: #0093a9;
  color: white;
  border: #0093a9;
  cursor: pointer;
  width: 120px;
  height: 2rem;
  border-radius: 7px;float:right;    margin-right: -472px;margin-top: -31px !important;
\"><i class=\"fa fa-check\"> </i> Réserver</button><br/></div>  </div>  </div>

</div>

</div>
";
        // line 652
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
<br/><br/><br/><br/><br/>

     
    </div>
   

  </div>

</div>


     
           
      
            
                   
      </div>
       
  
  
";
        
        $__internal_4f33dde1dc7391b9f0949dcd23587714c40051b1eb67b97db032779ee821e4c9->leave($__internal_4f33dde1dc7391b9f0949dcd23587714c40051b1eb67b97db032779ee821e4c9_prof);

        
        $__internal_96a2885f53c490aabf07b8b26f5e60ab9bf9cc5e7c3e6dee052603f7b152cffd->leave($__internal_96a2885f53c490aabf07b8b26f5e60ab9bf9cc5e7c3e6dee052603f7b152cffd_prof);

    }

    public function getTemplateName()
    {
        return "default/coordonnees.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  988 => 652,  964 => 631,  953 => 623,  945 => 618,  938 => 614,  933 => 611,  927 => 607,  920 => 602,  916 => 600,  913 => 599,  909 => 597,  906 => 596,  902 => 594,  899 => 593,  895 => 591,  893 => 590,  735 => 434,  722 => 426,  716 => 425,  713 => 424,  710 => 423,  698 => 417,  695 => 416,  690 => 415,  687 => 414,  684 => 413,  679 => 412,  673 => 411,  670 => 410,  659 => 405,  655 => 404,  652 => 403,  647 => 402,  644 => 401,  639 => 400,  637 => 399,  635 => 398,  633 => 397,  622 => 388,  616 => 384,  612 => 383,  583 => 357,  578 => 355,  573 => 353,  568 => 351,  562 => 348,  555 => 343,  544 => 334,  538 => 331,  533 => 329,  528 => 327,  523 => 325,  518 => 322,  505 => 311,  489 => 299,  450 => 261,  448 => 260,  444 => 257,  433 => 251,  422 => 247,  418 => 246,  413 => 244,  410 => 243,  399 => 239,  395 => 238,  390 => 236,  386 => 235,  381 => 234,  379 => 233,  375 => 230,  371 => 229,  365 => 225,  357 => 217,  355 => 216,  341 => 204,  337 => 203,  333 => 202,  329 => 201,  315 => 189,  313 => 188,  297 => 174,  292 => 172,  286 => 169,  282 => 167,  273 => 165,  269 => 164,  265 => 163,  258 => 158,  240 => 156,  237 => 155,  220 => 154,  68 => 4,  59 => 3,  41 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}
{% block title %}Gestion des hôtels{% endblock %}
{% block body %}

<style>

.label-success{
    background: #0093a9;
    color: #fff;
    border-radius: 5px;
    padding: 2px 6px 4px 6px;
    font-size:13px;
}
.box.box-info {
    border-top-color: #0093a9 !important;
}
 .w-box-header {
     background: #0093a9 !important;
 }
 .table-bordered td, .table-bordered th{
     font-size:14px;
 }
 

@import url('https://fonts.googleapis.com/css?family=Arimo:400,700&display=swap');

h2{
  color:#000;
  text-align:center;
  font-size:2em;
}
.warpper{
  display:flex;
  flex-direction: column;
  align-items: center;
}
.tab{
cursor: pointer;
    padding: 10px 20px;
    margin: 0px 2px;
    background: #1e4b22;
    display: inline-block;
    color: #fff;
    border-radius: 3px 3px 0px 0px;
    box-shadow: 0 0.5rem 0.8rem #00000080;
}
.panels{
  background:#fffffff6;
  box-shadow: 0 2rem 2rem #00000080;
  min-height:200px;
  width:100%;
  max-width:500px;
  border-radius:3px;
  overflow:hidden;
  padding:20px;  
}
.panel{
  display:none;
  animation: fadein .8s;
}
.label-warning {
    background-color: #ffcc33!important;
    color: #fff;
    padding: 3px 12px 4px 15px;
    border-radius: 4px;
    font-size: 12px !important;
    height: 22px;
}
.col-6 {
  
    color: #000;
}
@keyframes fadein {
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
}
.warpper {
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 409px;
}
.panel-title{
  font-size:1.5em;
  font-weight:bold
}
.radio{
  display:none;
}
#one:checked ~ .panels #one-panel,
#two:checked ~ .panels #two-panel,
#three:checked ~ .panels #three-panel{
  display:block
}
#one:checked ~ .tabs #one-tab,
#two:checked ~ .tabs #two-tab,
#three:checked ~ .tabs #three-tab{
  background:#fffffff6;
  color:#000;
  border-top: 3px solid #000;
}

.panels {
    background: #f5f5f5;
    box-shadow: 0 2rem 2rem #00000080;
    min-height: 200px;
    width: 100%;
    max-width: 695px;
    border-radius: 3px;
    overflow: hidden;
    padding: 42px;
    font-size: 14px;
}

.panel-title{
    color:#0093a9;
}
#three:checked ~ .tabs #three-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
#two:checked ~ .tabs #two-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
#one:checked ~ .tabs #one-tab {
    background: #f5f5f5;
    color: #000;
    border-top: 3px solid #ffcc33;
}
</style>

   





 

 <div class=\"tarifReservation container\">
    <div class=\"detailsBox\">
<h5  >  Détails de la réservation</h5>
    </div>
 <div class=\"back \" style=\"margin-bottom: 1rem;\">
  <div class=\" row\">
    <div class=\"col-md-4\">
      {% for i in hotelsimg %}
     {% if loop.first %}
      <img src=\"{{asset(i.file)}}\" class=\"w-100\"  />
      {% endif %}
{% endfor %} 
    </div>

    
    <div class=\"col-md-7\">
      <h4 style=\"color: rgb(204, 51, 0);\">{{reservation.hotel.name}}</h4>
                               {% for i in 1..reservation.hotel.star %}
                                <img src=\"{{asset('assets/image/etoil.png')}}\" class=\"\" alt=\"...\">
                                {% endfor %}

     <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px\">{{reservation.hotel.shortdesc|slice(0, 500)|raw}} ..</h6> <br/>
    
    <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px;;font-weight:bold;\"> Du : {{ reservation.datedebut|date('d/m/Y') }} <h6/>
    <h6 style=\"color: #000; font-size: 13px;
    line-height: 21.25px;;font-weight:bold;\"> Au :  {{ reservation.datefin|date('d/m/Y') }}</h6>
    </div>
    

  </div>


 </div>
 <h5 style=\"text-decoration:underline;\">Récapitulation :</h5>
      <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >

                                <thead>
                                    <tr style=\"background-color: #dee2e460;\">
                                        {# <th>Date de réservation</th> #}
                                        {# <th>Agence</th> #}
                                        <th> Date de début </th>
                                        <th> Date de fin</th>
                                        <th> Hôtel </th>
                        
                                        <th>Nb.chambres </th>
                                      
                                    </tr>
                                    </thead>
\t\t\t\t\t\t\t\t<tbody>
                            
                                        <tr style=\"background-color:#fff\">
                                           
                                            <td>{{ reservation.datedebut|date('d/m/Y') }}</td>
                                            <td>{{ reservation.datefin|date('d/m/Y') }}</td>
                                            <td>{{ reservation.hotel.name }}</td>
                                            <td >{{ reservation.nbchambres }}</td>
                       
                                        </tr>
                                    </tbody>


                    </table>

             <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; \" >
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                        {# <th>Date de réservation</th> #}
                                        {# <th>Agence</th> #}
                                        <th> Type </th>
                                        <th> Nbre des adultes</th>
                                        <th> Nbre des enfants </th>
                                        <th> Âges des enfants</th>
                                        <th> Arrangements </th>
                                         <th> Suppléments </th>
                                        {# <th><strong> Prix Total </strong></th>
                                        <th><strong> Montant payé </strong></th> #}
                                    </tr>
                                    </thead>
\t\t\t\t\t\t\t\t  <tbody >
                            
                                     {% for c in chambres %}
                                        <tr style=\"background-color: #fff;\">
                                           
                                            {# <td>{{ entry.dcr|date('d/m/Y') }} - {{ entry.dcr|date('h:i') }}</td> #}
                                             {# <td>{{ entry.user.name }} </td> #}
                                            <td>{{c.type}}</td>
                                            <td>{{ c.nbadultes }}</td>
                                            <td>{{ c.nbenfants }}</td>
                                            <td>
                                                 {% for key in c.ageenfant|keys %}
                                                  <label class=\"label label-success\">  {{c.ageenfant[key]}} ans</label>
                                                
                            
                                                {% endfor %}
                                            </td>
                                            <td ><label class=\"label label-warning\">{{ c.arrangement }}</label></td>
                                             <td>
                                                    {% for key in c.supplements|keys %}
                                                  <label class=\"label label-success\">  {{c.supplements[key]}} </label><br/>
                                                
                            
                                                {% endfor %}
                                                  </td> 
                                            
                                          
                                       
                                        </tr>
                                        {% endfor %}
                                    </tbody>
            </table>
                                                   {#   <strong style=\" text-decoration: underline;\">  <i class=\"fa fa-hand-point-right mx-3 black-text\" aria-hidden=\"true\"></i> Vous avez réservé {{ reservation.nbchambres }} chambres : </strong>#}
                                                   {#<br>     <br>   #}
    <table border=\"0\" width=\"100%\" >
\t<tr>
\t\t<td width=\"50%\">
\t\t    
      <table class=\"table  \" style=\"margin-top:1rem;display:none;  \" >
\t\t\t    <tr>
\t\t\t        <td width=\"28%\" align=\"center\"><strong></strong></td>
\t\t\t        <td width=\"40%\" align=\"center\"><strong></strong></td>
\t\t\t        <td width=\"32%\" align=\"center\"><strong></strong></td>
\t\t\t    </tr>
\t\t\t 
\t\t\t    <tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<strong>
\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t
            </table>    
\t\t</td>
\t\t<td width=\"50%\" >
      <table class=\"table text-center table-bordered \" style=\"margin-top:1rem; background: #dee2e460\" >
\t\t
\t\t\t\t<tr  >
\t\t\t\t\t<td width=\"50%\"  align=\"center\">
\t\t\t\t\t\t<strong>Montant à payer</strong>
\t\t\t\t\t</td>
\t\t\t\t\t<td width=\"50%\" >
\t\t\t\t\t\t<strong style=\"color :rgb(204, 51, 0)\"> 
\t                     {#{{reservation.total|number_format(3, '.',' ') }} TND#}
\t\t\t\t\t\t {{reservation.total }} TND

\t\t\t\t\t\t</strong>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t
\t\t\t</table>
       </td>
       </tr>
       </table>
       
 </div></div>      
      {{ form_start(form) }} 

  

 <div class=\" container \" >
  <div class=\" detailsBox\">
    <h5> Informations sur le client</h5>
    </div>
        <div class=\"row\" style=\"font-size:14px;\">
            <div class=\"col col-6\" >
                {# <h6 class=\"text-center\" style=\"color: #999966;\">Coordonées</h6> #}
           
                    <div class=\"\"  >
                          C.I.N / Passeport * : :
                      {{ form_widget(form.cin, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\",}}) }}
                         Civilité :
                      {{ form_widget(form.civilite, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;font-size:12px; padding: 2px;\"}}) }}
                      Nom :
                      {{ form_widget(form.nom, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\",}}) }}
                      Prénom :
                       {{ form_widget(form.prenom, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     
                      Email :
                       {{ form_widget(form.email, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     
                    
                    
                
                  </div>
            </div>
            <div class=\"col col-6\" >
                {# <h6 class=\"text-center\" style=\"color:#999966\">Rooming List</h6> #}
               
                  
                    <div class=\"form-group\">
                   
                     Téléphone :
                       {{ form_widget(form.mobile, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                       
                     Adresse :
                      {{ form_widget(form.adulte, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                      Code postale :
                      {{ form_widget(form.adultes, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                      Nom de ville : 
                      {{ form_widget(form.demandes, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                        Pays :
                      {{ form_widget(form.pays, { 'attr': {'class': 'form-control','style':\"border-radius: 2px !important;height: 25px !important;\"}}) }} 
                     
                     
                
                    
                         
            </div>
           
        </div>
       
    </div>

 

</div>

 
</div></div>


  

 <div class=\" container \" >
 <div class=\"detailsBox\">
    <h5>Liste des personnes / Rooming list</h5>
    </div>
  {% for index,c in 1..chambres|length %}
 <table class=\"table text-center table-bordered \"  >
                                     <thead>
                                    <tr style=\"background-color: #dee2e640;\">
                                       
                                        <th> Chambre N° {{index +1 }}</th>
                                        <th> Noms des occupants</th>
                                        <th> Âges </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                       
                                         {#{% for index,c in 1..chambres|length %}#}
                                        {#<tr>#}
                                        {#    <td colspan=2 style=\"font-weight:bold;\"> Chambre N° {{index +1 }}</td>#}
                                        {#</tr>#}
                                         {% for key,c in chambres %}
                                         {%if key == index %}
                                        {%for a  in 1..c.nbadultes %}
                                        <tr style=\"background-color: #fff;\">
                                            <td>Adulte (*) {{a}}</td>
                                             <td>   <input type=\"text\" name=\"nomadulte[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\">  <input  type=\"hidden\" name=\"idchambre\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\" value=\"{{c.id}}\"> </td>
                                     
                                        <td> <input  type=\"number\" name=\"ageadulte[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                            {%endfor%}
                                              {%endif%}
                                              {%endfor%}
                                         {% for keyen,c in chambres %}
                                         {% if c.nbenfants >0%}
                                         {%if keyen == index %}
                                             {%for keyen,e  in 1.. c.nbenfants %}
                                        <tr style=\"background-color: #fff;\">
                                              <td>Enfant  (*) {{e}}</td>
                                             <td>  <input  type=\"text\" name=\"nomenf[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"> </td>
                                      
                                        <td> <input  type=\"number\" name=\"ageenf[]\" class=\"form-control\" style=\"border-radius: 2px !important;height: 25px !important;\"></td>
                                            </tr>
                                            {%endfor%}
                                              {%endif%}
                                              {%endif%}
                                              {%endfor%}
                                    
                                    </tbody>



</table>

    {%endfor%}
</div></div>



</div>



  


 <div class=\" container \">
 <div class=\"detailsBox\">
    <h5>Recommandations</h5>
    </div>
                                                 <div class=\"row\" style=\"font-size:14px;\">
                                                                    <div class=\"col-md-4\">
                                                                        <div class=\"formSep \">
                                                                            <label class=\"checkbox\" > 
                                                                            <input type=\"checkbox\" name=\"options[]\"
                                                                                                           value=\"Séjour de noces\"/>&nbsp;Séjour de noces </label> 
                                                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"De préférence chambre avec 2 lits\"/>&nbsp;De préférence chambre avec 2 lits
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue piscine\"/>&nbsp;Si possible, chambre vue piscine
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambre vue jardin\"/>&nbsp;Si possible, chambre vue jardin
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ tardif\"/>&nbsp;Si possible, départ tardif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Prière prévoir un lit bébé\"/>&nbsp;Prière prévoir un lit bébé
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour infos, séjour de noces\"/>&nbsp;Pour infos, séjour de noces
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Pour info, arrivée tardive\"/>&nbsp;Pour info, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, départ actif\"/>&nbsp;Si possible, départ actif
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, grand lit\"/>&nbsp;Si possible, grand lit
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres non fumeur\"/>&nbsp;Si possible, chambres non fumeur
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres côte à côte\"/>&nbsp;Si possible, chambres côte à côte
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, chambres communicantes\"/>&nbsp;Si possible, chambres communicantes
                                            </label>
                                        </div>
                                    </div>
                                                                    <div class=\"col-md-4\">
                                        <div class=\"formSep \">
                                            <label class=\"checkbox\"><input type=\"checkbox\" name=\"options[]\"
                                                                           value=\"Si possible, arrivée tardive\"/>&nbsp;Si possible, arrivée tardive
                                            </label>
                                        </div>
                                    </div>
                                                            </div>
                         
                            <div class=\"col-md-6\">
                                <div>
                                    <label for=\"recommandations\" class=\"required\"><strong>Vos remarques /
                                            Recommandations :</strong></label>
                                    <textarea id=\"recommandations\" name=\"recommandations\"
                                              class=\"form-control\"></textarea><br/>

                                </div>










</div></div>



</div>

 
</div></div>



     <div class=\"tarifReservation container \" >
<div class=\"detailsBox\">
    <h5>Mode de Paiement
</h5>
    </div>
<div class=\"warpper\">
  <input class=\"radio\" id=\"one\" name=\"group\" type=\"radio\" value=\"espece\" checked >
  <input class=\"radio\" id=\"two\" name=\"group\" type=\"radio\" value=\"Virement\">
  <input class=\"radio\" id=\"three\" name=\"group\" type=\"radio\" value=\"versement\">
  <div class=\"tabs\">
  {% if payement.actagence == 1 %}
  <label class=\"tab\" id=\"one-tab\" for=\"one\">Paiement à l'agence</label>
  {% endif %}
  {% if payement.actrib == 1 %}
  <label class=\"tab\" id=\"two-tab\" for=\"two\">Virement bancaire</label>
  {% endif %}
    {% if payement.actvers == 1 %}
  <label class=\"tab\" id=\"three-tab\" for=\"three\">Versement espéces</label>
  {%endif %}
   {% if payement.actmond == 1 %}
  <label class=\"tab\" id=\"for-tab\" for=\"for\">Mondat</label>
  {%endif %}
    </div>
  <div class=\"panels\" >
  <div class=\"panel\" id=\"one-panel\" >
    <div class=\"panel-title\" style=\"color:#0093a9\">Adresses :</div>
    <p style=\"line-height:1.25px;\">
    {{payement.longdescagence|raw}}

    </p>
    {# <i class=\"fas fa-map-marker-alt\" style=\"color:#ffcc33; font-size: 55px;\"></i> #}
  </div>
  <div class=\"panel\" id=\"two-panel\">
    <div class=\"panel-title\">Virement Bancaire</div>
    <p>{{payement.longdescrib|raw}}</p>
  </div>
  <div class=\"panel\" id=\"three-panel\">
    <div class=\"panel-title\">Versement en espéces</div>
    <p>{{payement.longdescvers|raw}}</p>
  </div>

    <div class=\"panel\" id=\"for-panel\">
    <div class=\"panel-title\">Mondat</div>
    <p>{{payement.longdescmond|raw}}</p>
  </div>
  </div>
  
</div>
  
<div class=\"row\" style=\"margin-bottom:3px;\">
      <div class=\"col-md-6\">
   <a href=\"{{path('deletereservationhotel', {'id': reservation.id})}}\" class=\"btn btn-sm my-0\" style=\"background:#d13300;color:#fff;
  border: #0093a9;
  cursor: pointer;
  width: 120px;
  height: 2rem;
  border-radius: 7px;float:right;    margin-right: -347px;
 \"><i class=\"fa fa-trash\"> </i>  Annuler</a> 
 </div> &nbsp;&nbsp;
    <div class=\"col-md-6\">
  <button type=\"submit\"  id=\"btsubmit\" class=\"btn btn-sm my-0\" style=\"background-color: #0093a9;
  color: white;
  border: #0093a9;
  cursor: pointer;
  width: 120px;
  height: 2rem;
  border-radius: 7px;float:right;    margin-right: -472px;margin-top: -31px !important;
\"><i class=\"fa fa-check\"> </i> Réserver</button><br/></div>  </div>  </div>

</div>

</div>
{{ form_end(form) }}
<br/><br/><br/><br/><br/>

     
    </div>
   

  </div>

</div>


     
           
      
            
                   
      </div>
       
  
  
{% endblock %}























































", "default/coordonnees.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\coordonnees.html.twig");
    }
}
