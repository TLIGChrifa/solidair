<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_e1442f2313d2434d8b896c621bec091b9a3fbc77b083792f226c0292025c59be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22974bfb976b0c1edbd2e3726f981c6fe98a890c695d8ede43a0b3b4c0581b98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22974bfb976b0c1edbd2e3726f981c6fe98a890c695d8ede43a0b3b4c0581b98->enter($__internal_22974bfb976b0c1edbd2e3726f981c6fe98a890c695d8ede43a0b3b4c0581b98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_c8feaa1fc0b8a30a918731cec8419f89ced9f8939d87d200d23b29dde99a38a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8feaa1fc0b8a30a918731cec8419f89ced9f8939d87d200d23b29dde99a38a7->enter($__internal_c8feaa1fc0b8a30a918731cec8419f89ced9f8939d87d200d23b29dde99a38a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22974bfb976b0c1edbd2e3726f981c6fe98a890c695d8ede43a0b3b4c0581b98->leave($__internal_22974bfb976b0c1edbd2e3726f981c6fe98a890c695d8ede43a0b3b4c0581b98_prof);

        
        $__internal_c8feaa1fc0b8a30a918731cec8419f89ced9f8939d87d200d23b29dde99a38a7->leave($__internal_c8feaa1fc0b8a30a918731cec8419f89ced9f8939d87d200d23b29dde99a38a7_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_309d745e447a4ef05bf1df5d6aef94ff5fae6558622c7cbb7bdc8587fbd6ac76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_309d745e447a4ef05bf1df5d6aef94ff5fae6558622c7cbb7bdc8587fbd6ac76->enter($__internal_309d745e447a4ef05bf1df5d6aef94ff5fae6558622c7cbb7bdc8587fbd6ac76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_dd7d3fd13fb02a77b3dfce80cb12d5bef7d3b19b3818c38749e299ba1f8f4f10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd7d3fd13fb02a77b3dfce80cb12d5bef7d3b19b3818c38749e299ba1f8f4f10->enter($__internal_dd7d3fd13fb02a77b3dfce80cb12d5bef7d3b19b3818c38749e299ba1f8f4f10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_dd7d3fd13fb02a77b3dfce80cb12d5bef7d3b19b3818c38749e299ba1f8f4f10->leave($__internal_dd7d3fd13fb02a77b3dfce80cb12d5bef7d3b19b3818c38749e299ba1f8f4f10_prof);

        
        $__internal_309d745e447a4ef05bf1df5d6aef94ff5fae6558622c7cbb7bdc8587fbd6ac76->leave($__internal_309d745e447a4ef05bf1df5d6aef94ff5fae6558622c7cbb7bdc8587fbd6ac76_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_05d21f192c264a636fd4fef0d96e9b5e83ec598dcbc37f5acac40b1fb737ebfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05d21f192c264a636fd4fef0d96e9b5e83ec598dcbc37f5acac40b1fb737ebfd->enter($__internal_05d21f192c264a636fd4fef0d96e9b5e83ec598dcbc37f5acac40b1fb737ebfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_85cbbff0a6a37d8a850cbb324afacfaef911f3716b15d2db65f371e804da4c28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85cbbff0a6a37d8a850cbb324afacfaef911f3716b15d2db65f371e804da4c28->enter($__internal_85cbbff0a6a37d8a850cbb324afacfaef911f3716b15d2db65f371e804da4c28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_85cbbff0a6a37d8a850cbb324afacfaef911f3716b15d2db65f371e804da4c28->leave($__internal_85cbbff0a6a37d8a850cbb324afacfaef911f3716b15d2db65f371e804da4c28_prof);

        
        $__internal_05d21f192c264a636fd4fef0d96e9b5e83ec598dcbc37f5acac40b1fb737ebfd->leave($__internal_05d21f192c264a636fd4fef0d96e9b5e83ec598dcbc37f5acac40b1fb737ebfd_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_cd4f75578defb6de5ce1bac00760c978d18147c32e88aabc750c2bc320267ebd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd4f75578defb6de5ce1bac00760c978d18147c32e88aabc750c2bc320267ebd->enter($__internal_cd4f75578defb6de5ce1bac00760c978d18147c32e88aabc750c2bc320267ebd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_d396d10a2cc38bcce3e40e6e1517a0f559a32a414e6ea8330515ef5f7849ef19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d396d10a2cc38bcce3e40e6e1517a0f559a32a414e6ea8330515ef5f7849ef19->enter($__internal_d396d10a2cc38bcce3e40e6e1517a0f559a32a414e6ea8330515ef5f7849ef19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_d396d10a2cc38bcce3e40e6e1517a0f559a32a414e6ea8330515ef5f7849ef19->leave($__internal_d396d10a2cc38bcce3e40e6e1517a0f559a32a414e6ea8330515ef5f7849ef19_prof);

        
        $__internal_cd4f75578defb6de5ce1bac00760c978d18147c32e88aabc750c2bc320267ebd->leave($__internal_cd4f75578defb6de5ce1bac00760c978d18147c32e88aabc750c2bc320267ebd_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp\\www\\solidair\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
