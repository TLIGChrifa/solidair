<?php

/* default/detailshotel.html.twig */
class __TwigTemplate_990bde55d7168f5082b27406cbce50ee33c5e8140e6c35d280ce621358615d67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/detailshotel.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_747810e028c5ef85adcd7d6aa8f4fc0767690bd872395b5c5c98af138c90afe0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_747810e028c5ef85adcd7d6aa8f4fc0767690bd872395b5c5c98af138c90afe0->enter($__internal_747810e028c5ef85adcd7d6aa8f4fc0767690bd872395b5c5c98af138c90afe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/detailshotel.html.twig"));

        $__internal_4efdd5eee777bf98774127bf061f228d9274864b61f5d68276dedb7e5489e5d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4efdd5eee777bf98774127bf061f228d9274864b61f5d68276dedb7e5489e5d0->enter($__internal_4efdd5eee777bf98774127bf061f228d9274864b61f5d68276dedb7e5489e5d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/detailshotel.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_747810e028c5ef85adcd7d6aa8f4fc0767690bd872395b5c5c98af138c90afe0->leave($__internal_747810e028c5ef85adcd7d6aa8f4fc0767690bd872395b5c5c98af138c90afe0_prof);

        
        $__internal_4efdd5eee777bf98774127bf061f228d9274864b61f5d68276dedb7e5489e5d0->leave($__internal_4efdd5eee777bf98774127bf061f228d9274864b61f5d68276dedb7e5489e5d0_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_60ecc9619291f266fc4df6436f30562af551673d552727edaceb594435131fac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60ecc9619291f266fc4df6436f30562af551673d552727edaceb594435131fac->enter($__internal_60ecc9619291f266fc4df6436f30562af551673d552727edaceb594435131fac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b424b9fa5934c2e11664c074c3393fa26e16746e684e0a0ed4779d941c9b0117 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b424b9fa5934c2e11664c074c3393fa26e16746e684e0a0ed4779d941c9b0117->enter($__internal_b424b9fa5934c2e11664c074c3393fa26e16746e684e0a0ed4779d941c9b0117_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
        <div class=\"slider\"> 

          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->

          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">

                <div class=\"carousel-inner\">

                  

                  

                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">

                

                  </div>

          

                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">

                

                 </div>

                    

                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">

          

                  </div>

                 

            

                </div>

                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">

                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>

                  <span class=\"sr-only\">Previous</span>

                </a>

                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">

                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>

                  <span class=\"sr-only\">Next</span>

                </a>

          

          </div>

        </div>
  <div class=\"detailVoyagesOrganises sliderHotel container\">

            <ul id=\"menu\">

                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>

                <li><a href=\"";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("hotelsentunise");
        echo "\">Hotels En Tunisie</a></li>

                <li><a class=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailshotel", array("id" => $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "id", array()))), "html", null, true);
        echo "\">Détails</a></li>

            </ul>

           <div class=\"detailsBox row \">

              <div class=\"col-12\"><h5> ";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "name", array()), "html", null, true);
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "star", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 82
            echo "                          
                                <img src=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/image/etoil.png"), "html", null, true);
            echo "\" class=\"\" alt=\"...\" >
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 84
        echo " </h5></div> 

               <div class=\"col-12 col-md-12\">

                <div id=\"HotelDetails\" class=\"carousel slide \" data-ride=\"carousel\">

                  

                    <div class=\"carousel-inner\" >

                     \t\t
                    ";
        // line 95
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["img"] ?? $this->getContext($context, "img")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "\t
                       <div class=\"carousel-item ";
            // line 96
            if ($this->getAttribute($context["loop"], "first", array())) {
                echo " active ";
            }
            echo "\">

                             <img    class=\"populair w-100\" src=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["i"], "file", array())), "html", null, true);
            echo "\" alt=\"\" />

                       </div>\t
                     ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "               

                      

                    </div>   

                      

                       

                    <div class=\"carousel-indicators\" style=\"width:100%;\">
              ";
        // line 113
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["img"] ?? $this->getContext($context, "img")));
        foreach ($context['_seq'] as $context["key"] => $context["i"]) {
            // line 114
            echo "                      <div data-target=\"#HotelDetails\" data-slide-to=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\"  class=\"active\"  >

                        <img src=\"";
            // line 116
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["i"], "file", array())), "html", null, true);
            echo "\" >

                      </div>
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "                
                     

                    </div>

                  </div>

                  

                  <div class=\" detail-content \" >

                    <div class=\"btnReserver\"> <a class=\"btn\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reserver", array("id" => $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "id", array()), "marcheid" => $this->getAttribute(($context["marche"] ?? $this->getContext($context, "marche")), "id", array()), "dated" => twig_date_format_filter($this->env, "now", "d-m-Y"), "datef" => twig_date_format_filter($this->env, "now", "d-m-Y"))), "html", null, true);
        echo "\" >Réserver</a></div>

                    

                    <div  style=\"font-size:13px;line-height: 20px;\">

                        <h6 style=\"color: #ef991d;\"><i class=\"fa fa-marker\"></i> Description</h6> 

                        <p style=\"font-weight: bold;\"><i class=\"fas fa-map-marker-alt\"></i> ";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "ville", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "pays", array()), "html", null, true);
        echo "</p>

                       ";
        // line 142
        echo "
                    <div class=\"detail-texte\">

                       <h6>";
        // line 145
        echo $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "shortdesc", array());
        echo "</h6>
                       <p >";
        // line 146
        echo $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "longdesc", array());
        echo "</p>

                    </div>

    

            </div>

            

                  

               </div>

               </div>

               ";
        // line 267
        echo "
            <div class=\"btnReserver col-12\"> <a class=\"btn\" href=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reserver", array("id" => $this->getAttribute(($context["h"] ?? $this->getContext($context, "h")), "id", array()), "marcheid" => $this->getAttribute(($context["marche"] ?? $this->getContext($context, "marche")), "id", array()), "dated" => twig_date_format_filter($this->env, "now", "d-m-Y"), "datef" => twig_date_format_filter($this->env, "now", "d-m-Y"))), "html", null, true);
        echo "\" >Reserver</a></div>      

            



        </div>

        </div>










";
        
        $__internal_b424b9fa5934c2e11664c074c3393fa26e16746e684e0a0ed4779d941c9b0117->leave($__internal_b424b9fa5934c2e11664c074c3393fa26e16746e684e0a0ed4779d941c9b0117_prof);

        
        $__internal_60ecc9619291f266fc4df6436f30562af551673d552727edaceb594435131fac->leave($__internal_60ecc9619291f266fc4df6436f30562af551673d552727edaceb594435131fac_prof);

    }

    public function getTemplateName()
    {
        return "default/detailshotel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  302 => 268,  299 => 267,  280 => 146,  276 => 145,  271 => 142,  264 => 139,  253 => 131,  240 => 120,  230 => 116,  224 => 114,  220 => 113,  207 => 102,  189 => 98,  182 => 96,  163 => 95,  150 => 84,  142 => 83,  139 => 82,  133 => 81,  124 => 75,  119 => 73,  49 => 5,  40 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
  {% extends 'base.html.twig' %}  

{% block body %}

        <div class=\"slider\"> 

          <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->

          <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">

                <div class=\"carousel-inner\">

                  

                  

                  <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">

                

                  </div>

          

                  <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">

                

                 </div>

                    

                  <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">

          

                  </div>

                 

            

                </div>

                <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">

                  <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>

                  <span class=\"sr-only\">Previous</span>

                </a>

                <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">

                  <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>

                  <span class=\"sr-only\">Next</span>

                </a>

          

          </div>

        </div>
  <div class=\"detailVoyagesOrganises sliderHotel container\">

            <ul id=\"menu\">

                <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>

                <li><a href=\"{{path('hotelsentunise')}}\">Hotels En Tunisie</a></li>

                <li><a class=\"{{ path('detailshotel', {'id': h.id}) }}\">Détails</a></li>

            </ul>

           <div class=\"detailsBox row \">

              <div class=\"col-12\"><h5> {{h.name}}    {% for i in 1..h.star %}
                          
                                <img src=\"{{asset('assets/image/etoil.png')}}\" class=\"\" alt=\"...\" >
                                {% endfor %} </h5></div> 

               <div class=\"col-12 col-md-12\">

                <div id=\"HotelDetails\" class=\"carousel slide \" data-ride=\"carousel\">

                  

                    <div class=\"carousel-inner\" >

                     \t\t
                    {% for i in img %}\t
                       <div class=\"carousel-item {% if loop.first %} active {% endif %}\">

                             <img    class=\"populair w-100\" src=\"{{asset(i.file)}}\" alt=\"\" />

                       </div>\t
                     {% endfor %}
               

                      

                    </div>   

                      

                       

                    <div class=\"carousel-indicators\" style=\"width:100%;\">
              {% for key,i in img %}
                      <div data-target=\"#HotelDetails\" data-slide-to=\"{{key}}\"  class=\"active\"  >

                        <img src=\"{{asset(i.file)}}\" >

                      </div>
             {% endfor %}
                
                     

                    </div>

                  </div>

                  

                  <div class=\" detail-content \" >

                    <div class=\"btnReserver\"> <a class=\"btn\" href=\"{{path('reserver',{'id': h.id,'marcheid':marche.id,'dated':\"now\"|date('d-m-Y'),'datef': \"now\"|date('d-m-Y')}) }}\" >Réserver</a></div>

                    

                    <div  style=\"font-size:13px;line-height: 20px;\">

                        <h6 style=\"color: #ef991d;\"><i class=\"fa fa-marker\"></i> Description</h6> 

                        <p style=\"font-weight: bold;\"><i class=\"fas fa-map-marker-alt\"></i> {{h.ville}} - {{h.pays}}</p>

                       {# <p style=\"font-weight: bold;\"> <i class=\"far fa-clock\"></i> Durée : 7 Jours /6 Nuits </p> #}

                    <div class=\"detail-texte\">

                       <h6>{{h.shortdesc|raw}}</h6>
                       <p >{{h.longdesc|raw}}</p>

                    </div>

    

            </div>

            

                  

               </div>

               </div>

               {# <div class=\" col-md-4 col-12\">

                <a href=\"\" class=\" media_bestoff\">

                    <div class=\"card-image\">

                        <img class=\"d-flex mr-2\" src=\"image/paris.jpg\">

                    </div>

                    <div class=\"media-body\">

                        <div class=\"fa Rating Rating4\"></div>

                            <h6>Paris</h6>



                            <div class=\"Destination\">

                                <small>7jours/6nuits </small> </div>

                            <div class=\"BlocPriceHome\">

                                <div class=\"PriceTotal\">

                                    75.000<sup>DT</sup></div>

                                

                            </div>

                    </div>

                </a>

                <a href=\"\" class=\" media_bestoff\">

                    <div class=\"card-image\">

                        <img class=\"d-flex mr-2\" src=\"image/paris.jpg\">

                    </div>

                    <div class=\"media-body\">

                        <div class=\"fa Rating Rating4\"></div>

                            <h6>Paris</h6>



                            <div class=\"Destination\">

                                <small>7jours/6nuits </small> </div>

                            <div class=\"BlocPriceHome\">

                                <div class=\"PriceTotal\">

                                    75.000<sup>DT</sup></div>

                                

                            </div>

                    </div>

                </a>

                <a href=\"\" class=\" media_bestoff\">

                    <div class=\"card-image\">

                        <img class=\"d-flex mr-2\" src=\"image/paris.jpg\">

                    </div>

                    <div class=\"media-body\">

                        <div class=\"fa Rating Rating4\"></div>

                            <h6>Paris</h6>



                            <div class=\"Destination\">

                                <small>7jours/6nuits </small> </div>

                            <div class=\"BlocPriceHome\">

                                <div class=\"PriceTotal\">

                                    75.000<sup>DT</sup></div>

                                

                            </div>

                    </div>

                </a>

            </div> #}

            <div class=\"btnReserver col-12\"> <a class=\"btn\" href=\"{{path('reserver',{'id': h.id,'marcheid':marche.id,'dated':\"now\"|date('d-m-Y'),'datef': \"now\"|date('d-m-Y')}) }}\" >Reserver</a></div>      

            



        </div>

        </div>










{% endblock %}


", "default/detailshotel.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\detailshotel.html.twig");
    }
}
