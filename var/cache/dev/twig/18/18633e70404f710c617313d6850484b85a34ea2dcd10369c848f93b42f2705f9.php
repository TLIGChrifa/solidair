<?php

/* default/detailsvo.html.twig */
class __TwigTemplate_b7d06eb0b5f1dec4b8d00d0359491ed087ac4090e31d930ab1f04c81e9887c97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/detailsvo.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b88eafbf74e2f2ff25439a085628553836036dce7559d82fa274b7bd2a85acc4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b88eafbf74e2f2ff25439a085628553836036dce7559d82fa274b7bd2a85acc4->enter($__internal_b88eafbf74e2f2ff25439a085628553836036dce7559d82fa274b7bd2a85acc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/detailsvo.html.twig"));

        $__internal_02c8e9962f890274eb8c5f257ed0f52d8fc24135306713bb7ec8200f34e914ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02c8e9962f890274eb8c5f257ed0f52d8fc24135306713bb7ec8200f34e914ab->enter($__internal_02c8e9962f890274eb8c5f257ed0f52d8fc24135306713bb7ec8200f34e914ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/detailsvo.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b88eafbf74e2f2ff25439a085628553836036dce7559d82fa274b7bd2a85acc4->leave($__internal_b88eafbf74e2f2ff25439a085628553836036dce7559d82fa274b7bd2a85acc4_prof);

        
        $__internal_02c8e9962f890274eb8c5f257ed0f52d8fc24135306713bb7ec8200f34e914ab->leave($__internal_02c8e9962f890274eb8c5f257ed0f52d8fc24135306713bb7ec8200f34e914ab_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2b1cc706d7621e4c3cfeb69843e691b056b3134e6ecb02a163175f41cfd05600 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b1cc706d7621e4c3cfeb69843e691b056b3134e6ecb02a163175f41cfd05600->enter($__internal_2b1cc706d7621e4c3cfeb69843e691b056b3134e6ecb02a163175f41cfd05600_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e58d8b939dc3ee83ef2b722fa5c109426cea0a60285eadbaad0c6704a9a6c1a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e58d8b939dc3ee83ef2b722fa5c109426cea0a60285eadbaad0c6704a9a6c1a5->enter($__internal_e58d8b939dc3ee83ef2b722fa5c109426cea0a60285eadbaad0c6704a9a6c1a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<head>
   
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    
        <!-- Google -->
    <meta name=\"google-site-verification\" content=\"\">
    <link rel=\"icon\" href=\"https://prod.bravebooking.net/clients/TT69312/media/photos/website/favicon_1.png\" sizes=\"16x16\" type=\"image/png\">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-104156994-1\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104156994-1');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '153865636472391');
fbq('track', 'PageView');
var element = document.querySelector('meta[property~=\"og:image\"]');
var content = element && element.getAttribute(\"content\");
console.log(content);
</script>
<noscript><img height=\"1\" width=\"1\" style=\"display:none\"
src=\"https://www.facebook.com/tr?id=153865636472391&ev=PageView&noscript=1\"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  fbq('track', 'Contact');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P8W6XTV');</script>
<!-- End Google Tag Manager -->
</head>

<body onload='AntiClickDroitImg()'>
    <!-- Google Tag Manager (noscript) -->
<noscript>";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            echo " <iframe src=\"https://rusticavoyages.com/rustica/public_html";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
            echo "\"
height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "</noscript>

<div class=\"slider\"> 
  <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\">
          
          
          <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
        
          </div>
  
          <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
        
         </div>
            
          <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
  
          </div>
         
    
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
          <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
          <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Next</span>
        </a>
  
</div>
</div> 
<div class=\"detailVoyagesOrganises container\">
  <ul id=\"menu\">
      <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
      <li><a href=\"VoyagesOrganises.html\">Voyages Organisés</a></li>
      <li><a class=\"active\">Détails</a></li>
  </ul>
 <div class=\"detailsBox row \">
    <div class=\"col-12\"><h5> ";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "villea", array()), "html", null, true);
        echo " </h5></div> 
     <div class=\"col-12 col-md-8\">
      <div id=\"HotelDetails\" class=\"carousel slide\" data-ride=\"carousel\">
        
        <div class=\"carousel-inner\" >
          ";
        // line 103
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            echo " 
                     ";
            // line 104
            if ($this->getAttribute($context["loop"], "first", array())) {
                // line 105
                echo "            <div class=\"carousel-item active\">
             
                
                  <img   class=\"populair w-100\" src=\"https://rusticavoyages.com/rustica/public_html/";
                // line 108
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
                echo "\"/>
                    
            </div>\t
            ";
            } else {
                // line 112
                echo "            <div class=\"carousel-item \">
             
                
             <img    class=\"populair w-100\" src=\"https://rusticavoyages.com/rustica/public_html/";
                // line 115
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
                echo "\" />
               
            </div>\t
            ";
            }
            // line 119
            echo "              ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "         </div>   
            
             
          <div class=\"carousel-indicators\" style=\"width:100%;\">
            ";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sejourimg"] ?? $this->getContext($context, "sejourimg")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            // line 125
            echo "            <div data-target=\"#HotelDetails\" data-slide-to=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" data-slide-to=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "\" class=\"active\"  >
              <img src=\"https://rusticavoyages.com/rustica/public_html/";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
            echo "\" >
            </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "            
           
          </div>
        </div>
        
        <div class=\" detail-content \" >
          <div class=\"btnReserver\"> <a class=\"btn\" href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reservervoyageo", array("id" => $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "id", array()))), "html", null, true);
        echo "\" >Reserver</a></div>
          
          <div  style=\"font-size:13px;line-height: 20px;\">
              <h6 style=\"color: #ef991d;\"><i class=\"fa fa-marker\"></i> Description</h6> 
              <p style=\"font-weight: bold;\"><i class=\"fas fa-map-marker-alt\"></i> ";
        // line 139
        echo twig_escape_filter($this->env, $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "villea", array()), "html", null, true);
        echo "</p>
             <p style=\"font-weight: bold;\"> <i class=\"far fa-clock\"></i> Durée : ";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "dureesj", array()), "html", null, true);
        echo " Jours /";
        echo twig_escape_filter($this->env, ($this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "dureesj", array()) - 1), "html", null, true);
        echo " Nuits </p>
          <div class=\"detail-texte\">
            <p> ";
        // line 142
        echo $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "description", array());
        echo "      </p> 
          </div>

  </div>
  
        
     </div>
     </div>
     <div class=\" col-md-4 col-12\">
      <h6 style=\"color: #f2981c;\"><i class=\"fa fa-calendar\"></i> Dates disponibles</h6>
      <p style=\"font-size:13px\">Du ";
        // line 152
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "dated", array()), "d-m-Y"), "html", null, true);
        echo " au ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "datef", array()), "d-m-Y"), "html", null, true);
        echo "</p>
      ";
        // line 153
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["datesvo"] ?? $this->getContext($context, "datesvo")));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 154
            echo "       <p style=\"font-size:13px\">Du ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["d"], "dated", array()), "d-m-Y"), "html", null, true);
            echo " au ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["d"], "datef", array()), "d-m-Y"), "html", null, true);
            echo "</p>
       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 156
        echo "       <h6 style=\"color:#f2981c;\"><i class=\"fa fa-list\"></i> À la une </h6>
       ";
        // line 157
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["voyages"] ?? $this->getContext($context, "voyages")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 158
            echo "       ";
            if ((($this->getAttribute($context["v"], "id", array()) != $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "id", array())) && ($this->getAttribute($context["loop"], "index", array()) <= 4))) {
                // line 159
                echo "      <a href=";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("detailsvo", array("id" => $this->getAttribute($context["v"], "id", array()))), "html", null, true);
                echo " class=\" media_bestoff\">
          <div class=\"card-image\">
            ";
                // line 161
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["img"] ?? $this->getContext($context, "img")));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    echo " 
            ";
                    // line 162
                    if (($this->getAttribute($context["item"], "sejour", array()) == $context["v"])) {
                        // line 163
                        echo "              <img class=\"d-flex mr-2\" src=\"https://rusticavoyages.com/rustica/public_html/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "image", array()), "html", null, true);
                        echo "\">
            ";
                    }
                    // line 165
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 166
                echo "          </div>
          <div class=\"media-body\">
                  <h6>";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "titre", array()), "html", null, true);
                echo "</h6>

                  <div class=\"Destination\">
                      <small>";
                // line 171
                echo twig_escape_filter($this->env, $this->getAttribute($context["v"], "dureesj", array()), "html", null, true);
                echo " jours / ";
                echo twig_escape_filter($this->env, ($this->getAttribute($context["v"], "dureesj", array()) - 1), "html", null, true);
                echo " Nuits</small> </div>
                  <div class=\"BlocPriceHome\">
                      <div class=\"PriceTotal\">
                        ";
                // line 174
                $context["total"] = ($this->getAttribute($context["v"], "prix", array()) + (($this->getAttribute($context["v"], "prix", array()) * $this->getAttribute($context["v"], "marge", array())) / 100));
                // line 175
                echo "                        ";
                echo twig_escape_filter($this->env, ($context["total"] ?? $this->getContext($context, "total")), "html", null, true);
                echo "<sup>DT</sup></div>
                      
                  </div>
          </div>
      </a>
      ";
            }
            // line 181
            echo "      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 182
        echo " 
      
  </div>
  <div class=\"btnReserver col-12\"> <a class=\"btn\" href=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reservervoyageo", array("id" => $this->getAttribute(($context["vo"] ?? $this->getContext($context, "vo")), "id", array()))), "html", null, true);
        echo "\" >Reserver</a></div>      
  

</div>
</div>



                
   
       


";
        
        $__internal_e58d8b939dc3ee83ef2b722fa5c109426cea0a60285eadbaad0c6704a9a6c1a5->leave($__internal_e58d8b939dc3ee83ef2b722fa5c109426cea0a60285eadbaad0c6704a9a6c1a5_prof);

        
        $__internal_2b1cc706d7621e4c3cfeb69843e691b056b3134e6ecb02a163175f41cfd05600->leave($__internal_2b1cc706d7621e4c3cfeb69843e691b056b3134e6ecb02a163175f41cfd05600_prof);

    }

    public function getTemplateName()
    {
        return "default/detailsvo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  430 => 185,  425 => 182,  411 => 181,  401 => 175,  399 => 174,  391 => 171,  385 => 168,  381 => 166,  375 => 165,  369 => 163,  367 => 162,  361 => 161,  355 => 159,  352 => 158,  335 => 157,  332 => 156,  321 => 154,  317 => 153,  311 => 152,  298 => 142,  291 => 140,  287 => 139,  280 => 135,  272 => 129,  255 => 126,  248 => 125,  231 => 124,  225 => 120,  211 => 119,  204 => 115,  199 => 112,  192 => 108,  187 => 105,  185 => 104,  166 => 103,  158 => 98,  116 => 58,  104 => 57,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
<head>
   
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    
        <!-- Google -->
    <meta name=\"google-site-verification\" content=\"\">
    <link rel=\"icon\" href=\"https://prod.bravebooking.net/clients/TT69312/media/photos/website/favicon_1.png\" sizes=\"16x16\" type=\"image/png\">

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-104156994-1\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104156994-1');
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '153865636472391');
fbq('track', 'PageView');
var element = document.querySelector('meta[property~=\"og:image\"]');
var content = element && element.getAttribute(\"content\");
console.log(content);
</script>
<noscript><img height=\"1\" width=\"1\" style=\"display:none\"
src=\"https://www.facebook.com/tr?id=153865636472391&ev=PageView&noscript=1\"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  fbq('track', 'Contact');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P8W6XTV');</script>
<!-- End Google Tag Manager -->
</head>

<body onload='AntiClickDroitImg()'>
    <!-- Google Tag Manager (noscript) -->
<noscript>{% for item in sejourimg %} <iframe src=\"https://rusticavoyages.com/rustica/public_html{{item.image}}\"
height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe>{%endfor%}</noscript>

<div class=\"slider\"> 
  <!-- image de slider width: 1111px;aspect-ratio: auto 1111 / 625;height: 625px;-->
<div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\">
          
          
          <div class=\"carousel-item carosel-one active\" style=\"height: 300px;\">
        
          </div>
  
          <div class=\"carousel-item carosel-tow\"  style=\"height: 300px;\">
        
         </div>
            
          <div class=\"carousel-item carosel-three\"  style=\"height: 300px;\">
  
          </div>
         
    
        </div>
        <a class=\"carousel-control-prev\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"prev\">
          <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Previous</span>
        </a>
        <a class=\"carousel-control-next\" href=\"#carouselExampleIndicators\" role=\"button\" data-slide=\"next\">
          <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Next</span>
        </a>
  
</div>
</div> 
<div class=\"detailVoyagesOrganises container\">
  <ul id=\"menu\">
      <li><a href=\"index.html\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i></a></li>
      <li><a href=\"VoyagesOrganises.html\">Voyages Organisés</a></li>
      <li><a class=\"active\">Détails</a></li>
  </ul>
 <div class=\"detailsBox row \">
    <div class=\"col-12\"><h5> {{vo.villea}} </h5></div> 
     <div class=\"col-12 col-md-8\">
      <div id=\"HotelDetails\" class=\"carousel slide\" data-ride=\"carousel\">
        
        <div class=\"carousel-inner\" >
          {% for item in sejourimg %} 
                     {% if loop.first %}
            <div class=\"carousel-item active\">
             
                
                  <img   class=\"populair w-100\" src=\"https://rusticavoyages.com/rustica/public_html/{{item.image}}\"/>
                    
            </div>\t
            {% else %}
            <div class=\"carousel-item \">
             
                
             <img    class=\"populair w-100\" src=\"https://rusticavoyages.com/rustica/public_html/{{item.image}}\" />
               
            </div>\t
            {%endif %}
              {%endfor %}
         </div>   
            
             
          <div class=\"carousel-indicators\" style=\"width:100%;\">
            {% for key,item in sejourimg %}
            <div data-target=\"#HotelDetails\" data-slide-to=\"{{key}}\" data-slide-to=\"{{loop.index}}\" class=\"active\"  >
              <img src=\"https://rusticavoyages.com/rustica/public_html/{{item.image}}\" >
            </div>
            {% endfor %}
            
           
          </div>
        </div>
        
        <div class=\" detail-content \" >
          <div class=\"btnReserver\"> <a class=\"btn\" href=\"{{path('reservervoyageo',{'id': vo.id})}}\" >Reserver</a></div>
          
          <div  style=\"font-size:13px;line-height: 20px;\">
              <h6 style=\"color: #ef991d;\"><i class=\"fa fa-marker\"></i> Description</h6> 
              <p style=\"font-weight: bold;\"><i class=\"fas fa-map-marker-alt\"></i> {{vo.villea}}</p>
             <p style=\"font-weight: bold;\"> <i class=\"far fa-clock\"></i> Durée : {{vo.dureesj}} Jours /{{vo.dureesj-1 }} Nuits </p>
          <div class=\"detail-texte\">
            <p> {{vo.description|raw}}      </p> 
          </div>

  </div>
  
        
     </div>
     </div>
     <div class=\" col-md-4 col-12\">
      <h6 style=\"color: #f2981c;\"><i class=\"fa fa-calendar\"></i> Dates disponibles</h6>
      <p style=\"font-size:13px\">Du {{vo.dated|date('d-m-Y')}} au {{vo.datef|date('d-m-Y')}}</p>
      {% for d in datesvo %}
       <p style=\"font-size:13px\">Du {{d.dated|date('d-m-Y')}} au {{d.datef|date('d-m-Y')}}</p>
       {%endfor%}
       <h6 style=\"color:#f2981c;\"><i class=\"fa fa-list\"></i> À la une </h6>
       {% for v in voyages %}
       {% if v.id != vo.id and loop.index <= 4 %}
      <a href={{ path('detailsvo', {'id': v.id}) }} class=\" media_bestoff\">
          <div class=\"card-image\">
            {% for item in img %} 
            {% if item.sejour == v  %}
              <img class=\"d-flex mr-2\" src=\"https://rusticavoyages.com/rustica/public_html/{{item.image}}\">
            {% endif %}
            {% endfor %}
          </div>
          <div class=\"media-body\">
                  <h6>{{v.titre}}</h6>

                  <div class=\"Destination\">
                      <small>{{v.dureesj}} jours / {{v.dureesj-1}} Nuits</small> </div>
                  <div class=\"BlocPriceHome\">
                      <div class=\"PriceTotal\">
                        {% set total = v.prix +  (v.prix*v.marge)/100 %}
                        {{total}}<sup>DT</sup></div>
                      
                  </div>
          </div>
      </a>
      {% endif %}
      {% endfor %}
 
      
  </div>
  <div class=\"btnReserver col-12\"> <a class=\"btn\" href=\"{{path('reservervoyageo',{'id': vo.id})}}\" >Reserver</a></div>      
  

</div>
</div>



                
   
       


{% endblock %}", "default/detailsvo.html.twig", "C:\\wamp\\www\\solidair\\app\\Resources\\views\\default\\detailsvo.html.twig");
    }
}
