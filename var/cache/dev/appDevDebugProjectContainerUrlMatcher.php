<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // rechercheht
        if (0 === strpos($pathinfo, '/rechercheht') && preg_match('#^/rechercheht/(?P<destination>[^/]++)/(?P<dated>[^/]++)/(?P<datef>[^/]++)/(?P<chambres>[^/]++)/(?P<occ>[^/]++)/\\{(?P<nbstars>[^/]+)\\}$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'rechercheht')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::recherchehtAction',));
        }

        // detailshotel
        if (0 === strpos($pathinfo, '/detailshotel') && preg_match('#^/detailshotel/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detailshotel')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::detailshotelAction',));
        }

        if (0 === strpos($pathinfo, '/c')) {
            // coordonnes
            if (0 === strpos($pathinfo, '/coordonnees') && preg_match('#^/coordonnees/(?P<id>[^/]++)/\\{(?P<dated>[^/]+)\\}/\\{(?P<datef>[^/]+)\\}$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'coordonnes')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::coordonneesAction',));
            }

            // chambres
            if (0 === strpos($pathinfo, '/chambres') && preg_match('#^/chambres/(?P<id>[^/]++)/(?P<marcheid>[^/]++)/\\{(?P<dated>[^/]+)\\}/\\{(?P<datef>[^/]+)\\}$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'chambres')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::chambresAction',));
            }

        }

        // payerreservationhotel
        if (0 === strpos($pathinfo, '/payerreservationhotel') && preg_match('#^/payerreservationhotel/(?P<id>[^/]++)/(?P<coordonnees>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'payerreservationhotel')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::payerAction',));
        }

        // deletereservationhotel
        if (0 === strpos($pathinfo, '/deletereservationhotel') && preg_match('#^/deletereservationhotel/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'deletereservationhotel')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::deletereservationhotelAction',));
        }

        // reserver
        if (0 === strpos($pathinfo, '/reserver') && preg_match('#^/reserver/(?P<id>[^/]++)/(?P<marcheid>[^/]++)/\\{(?P<dated>[^/]+)\\}/\\{(?P<datef>[^/]+)\\}$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'reserver')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reserverAction',));
        }

        // hotelsentunise
        if ($pathinfo === '/hotelsentunisie') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::hotelsAction',  '_route' => 'hotelsentunise',);
        }

        // recherchehotel
        if (0 === strpos($pathinfo, '/recherchehotel') && preg_match('#^/recherchehotel/(?P<nom>[^/]++)/(?P<ville>[^/]++)/(?P<enfants>[^/]++)/(?P<arr>[^/]++)/(?P<stars>[^/]++)/(?P<dated>[^/]++)/(?P<datef>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'recherchehotel')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::rechercheAction',));
        }

        // voyagesorganisees
        if ($pathinfo === '/voyagesorganisees') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::listevoyagesorganiseesAction',  '_route' => 'voyagesorganisees',);
        }

        // detailsvo
        if (0 === strpos($pathinfo, '/detailsvo') && preg_match('#^/detailsvo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detailsvo')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::detailsvoAction',));
        }

        if (0 === strpos($pathinfo, '/re')) {
            // recherchevo
            if (0 === strpos($pathinfo, '/recherchevo') && preg_match('#^/recherchevo/(?P<destination>[^/]++)/(?P<duree>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'recherchevo')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::recherchevoAction',));
            }

            // reservervo
            if (0 === strpos($pathinfo, '/reservervo') && preg_match('#^/reservervo/(?P<id>[^/]++)/(?P<nblignes>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reservervo')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservervoAction',));
            }

        }

        // payerreservationvoyageo
        if (0 === strpos($pathinfo, '/payerreservationvoyageo') && preg_match('#^/payerreservationvoyageo/(?P<id>[^/]++)/(?P<total>[^/]++)/(?P<nblignes>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'payerreservationvoyageo')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::payerreservationvoyageoAction',));
        }

        // reservervoyageo
        if (0 === strpos($pathinfo, '/reservervoyageo') && preg_match('#^/reservervoyageo/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'reservervoyageo')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservervoyageoAction',));
        }

        // listemaisondhote
        if ($pathinfo === '/listemaisondhote') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::listemaisondhoteAction',  '_route' => 'listemaisondhote',);
        }

        // detailsmaisondhote
        if (0 === strpos($pathinfo, '/detailsmaisondhotes') && preg_match('#^/detailsmaisondhotes/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detailsmaisondhote')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::detailsmaisondhoteAction',));
        }

        // reservermaisondhote
        if (0 === strpos($pathinfo, '/reservermaisondhote') && preg_match('#^/reservermaisondhote/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'reservermaisondhote')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservermaisondhoteAction',));
        }

        // billettrie
        if ($pathinfo === '/billettrie') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::billettrieAction',  '_route' => 'billettrie',);
        }

        // circuit
        if ($pathinfo === '/circuit') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::CircuitAction',  '_route' => 'circuit',);
        }

        // detailscircuit
        if (0 === strpos($pathinfo, '/detailscircuit') && preg_match('#^/detailscircuit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'detailscircuit')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::DetailscircuitAction',));
        }

        if (0 === strpos($pathinfo, '/reserverc')) {
            // reservercircuit
            if (0 === strpos($pathinfo, '/reservercircuit') && preg_match('#^/reservercircuit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reservercircuit')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservercircuitAction',));
            }

            // reservercr
            if (0 === strpos($pathinfo, '/reservercr') && preg_match('#^/reservercr/(?P<id>[^/]++)/(?P<nblignes>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reservercr')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservercrAction',));
            }

        }

        // payerreservationcr
        if (0 === strpos($pathinfo, '/payerreservationcr') && preg_match('#^/payerreservationcr/(?P<id>[^/]++)/(?P<total>[^/]++)/(?P<nblignes>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'payerreservationcr')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::payerreservationcrAction',));
        }

        if (0 === strpos($pathinfo, '/c')) {
            // circuitpersonnalisee
            if (0 === strpos($pathinfo, '/circuitpersonnalisee') && preg_match('#^/circuitpersonnalisee/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'circuitpersonnalisee')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::reservercircuitpersonnaliseeAction',));
            }

            // contact
            if ($pathinfo === '/contact') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::contactAction',  '_route' => 'contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
